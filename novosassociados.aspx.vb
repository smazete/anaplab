﻿Imports Newtonsoft.Json
Imports clssessoes
Imports clsSmart
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.Security

Partial Class novosassociados
    Inherits System.Web.UI.Page


    Private Sub novosassociados_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Exit Sub
        End If

        habilitar(True)
    End Sub

    Private Sub txtcep_TextChanged(sender As Object, e As EventArgs) Handles txtcep.TextChanged
        Dim xcep As String = sonumeros(txtcep.Text)
        If xcep = "" Then
            txtcep.Text = "CEP NAO INFORMADO"
            txtcep.Focus()
            Exit Sub

        End If

        Try
            Dim ws As New correios.AtendeClienteService



            'Verificar o nome do endpoint no arquivo Web.config 
            Dim dados = ws.consultaCEP(xcep)

            If dados IsNot Nothing Then

                txtendereco.Text = String.Format("{0}" & vbCr & vbLf & dados.[end], dados.complemento, dados.complemento2, dados.bairro, dados.cidade, dados.uf)


                'System.Threading.Thread.Sleep(2000)
                Dim ender As String = dados.end
                Dim cidade As String = dados.cidade
                Dim bairro As String = dados.bairro
                Dim uf As String = dados.uf

                txtcidade.Text = dados.cidade
                txtbairro.Text = dados.bairro
                txtestado.Text = dados.uf
                txtnumero.Focus()
            Else
                txtcep.Text = "CEP não encontrado."
            End If

        Catch ex As Exception
            txtcidade.Text = "Cidade não encontrada."
            txtendereco.Text = "Endereço não encontrado."
        End Try
    End Sub

    Private Sub limpar()
        'txtnrseq.text = ""
        txtmatricula.Text = ""
        txtnome.Text = ""
        txtnomemae.Text = ""
        '  cbosituacaopb.SelectedValue = ""
        cbosexo.Text = ""
        txtdtnasc.Text = ""
        txtcpf.Text = ""
        txtrg.Text = ""
        txtcep.Text = ""
        txtendereco.Text = ""
        txtnumero.Text = ""
        txtcomplemento.Text = ""
        txtbairro.Text = ""
        txtcidade.Text = ""
        txtestado.Text = ""
        ' txttelddd.Text = ""
        txttelefone.Text = ""
        txtcelular.Text = ""
        txtemail.Text = ""
        txtdtposse.Text = ""
        txtdtaposentadoria.Text = ""
        txtsenha.Text = ""
        txtAgencia.Text = ""
        txtcontacorrente.Text = ""
        'txtautorizacao.text = ""
        'txtdtcad.text = ""
        'txtusercad.text = ""
        'txtAtivo_Old.text = ""
        'txtAtivo.text = ""
        'txtmatriculactrl.text = ""
        'txtcodigo.text = ""
        'txtuserexcluir.text = ""
        'txtdtexcluido.text = ""
        'txtdddcelular.text = ""
        txtcelular.Text = ""
        'txtbloqueado_Old.text = ""
        'txtbloqueado.text = ""
        'txtdtbloqueio.text = ""
        'txtuserbloqueio.text = ""
        'txtdtdesbloqueio.text = ""
        'txtuserdesbloqueio.text = ""
        'txtip.text = ""
        'txtdtsolexc.text = ""
        'txtmotiexc.text = ""
        'txtdtinad.text = ""
        'txtfiliacao.text = ""
        'txtvivomorto.text = ""
        'txtnegociado.text = ""
    End Sub


    Private Sub salvar()

        Dim xclasse As New clsAssociado
        Dim xmensalidades As New clsmensalidades

        'xclasse.Nrseq = txtnrseq.Text

        If txtmatricula.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Matricula}',  showConfirmButton: true})")
            txtmatricula.Focus()
            Exit Sub
        Else
            If validarconta(txtmatricula.Text) = True Then
                xclasse.Matricula = txtmatricula.Text
            Else
                txtmatricula.Focus()
                sm("Swal.fire({  position: 'center',  type: 'error',  title: 'O calculo do dígito dessa matrícula é inválida! Por favor, verifique!',  showConfirmButton: true})")
                Exit Sub
            End If
        End If

        If txtnome.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Nome}',  showConfirmButton: true})")
            txtnome.Focus()
            Exit Sub
        Else
            xclasse.Nome = txtnome.Text.ToUpper
        End If
        If txtnomemae.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Nome Mãe}',  showConfirmButton: true})")
            txtnomemae.Focus()
            Exit Sub
        Else
            xclasse.Nomemae = txtnomemae.Text.ToUpper
        End If

        If cbosituacaopb.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Situação}',  showConfirmButton: true})")
            cbosituacaopb.Focus()
            Exit Sub
        Else
            xclasse.Situacaopb = cbosituacaopb.SelectedItem.Text
        End If

        If cbosituacaopb.SelectedValue = "1" Then
            ' quando aposentado deve informar tanto data posse quanto data aposentadoria
            If txtdtposse.Text = "" Then
                sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Data Posse}',  showConfirmButton: true})")
                txtdtposse.Focus()
                Exit Sub
            Else
                xclasse.Dtposse = txtdtposse.Text
            End If

            If txtdtaposentadoria.Text = "" Then
                sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Data Aposentadoria}',  showConfirmButton: true})")
                txtdtaposentadoria.Focus()
                Exit Sub
            Else
                xclasse.Dtaposentadoria = txtdtaposentadoria.Text
            End If


        ElseIf cbosituacaopb.SelectedValue = "2" Then
            'quando ativo só  precisa informar data posse

            '    xclasse.Dtaposentadoria = ""

            If txtdtposse.Text = "" Then
                sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Data Posse}',  showConfirmButton: true})")
                txtdtposse.Focus()
                Exit Sub
            Else
                xclasse.Dtposse = txtdtposse.Text
            End If

        ElseIf cbosituacaopb.SelectedValue = "3" Then
            ' quando pensionista nao precisa informar a data de aposentadoria 
            '    xclasse.Dtposse = ""
            '    xclasse.Dtaposentadoria = ""


        End If



        If cbosexo.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Sexo}',  showConfirmButton: true})")
            cbosexo.Focus()
            Exit Sub
        Else

            xclasse.Sexo = cbosexo.Text
        End If
        If txtdtnasc.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Data de Nascimento}',  showConfirmButton: true})")
            txtdtnasc.Focus()
            Exit Sub
        Else
            xclasse.Dtnasc = txtdtnasc.Text
        End If
        If txtcpf.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {CPF}',  showConfirmButton: true})")
            txtcpf.Focus()
            Exit Sub
        Else
            If VALIDACPF(txtcpf.Text) = True Then
                If Not xclasse.verificacpfcadastrado Then
                    txtcpf.Focus()
                    sm("Swal.fire({  position: 'center',  type: 'error',  title: 'CPF Não é valido',  showConfirmButton: true})")
                    Exit Sub
                End If


                If Not xclasse.procuracpf Then
                    txtcpf.Focus()
                    sm("Swal.fire({  position: 'center',  type: 'error',  title: 'CPF já esta cadastrado',  showConfirmButton: true})")
                    Exit Sub
                End If


                xclasse.Cpf = sonumeros(txtcpf.Text)
            Else
                txtcpf.Focus()
                sm("Swal.fire({  position: 'center',  type: 'error',  title: 'CPF Não é valido',  showConfirmButton: true})")
                Exit Sub
            End If
        End If
        If txtrg.Text = "" Then
            txtrg.Focus()
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {RG}',  showConfirmButton: true})")
            Exit Sub
        Else
            xclasse.Rg = txtrg.Text
        End If

        If txtcep.Text = "" Then
            txtcep.Focus()
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {CEP}',  showConfirmButton: true})")
            Exit Sub
        Else
            xclasse.Cep = txtcep.Text
        End If
        If txtendereco.Text = "" Then

            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Endereço}',  showConfirmButton: true})")
            txtendereco.Focus()
            Exit Sub
        Else
            xclasse.Endereco = txtendereco.Text
        End If
        If txtnumero.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {nuemro}',  showConfirmButton: true})")
            txtnumero.Focus()
            Exit Sub
        Else
            xclasse.Numero = txtnumero.Text
        End If
        If txtbairro.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Bairro}',  showConfirmButton: true})")
            txtbairro.Focus()
            Exit Sub
        Else
            xclasse.Bairro = txtbairro.Text
        End If
        If txtcidade.Text = "" Then

            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Cidade}',  showConfirmButton: true})")
            txtcidade.Focus()
            Exit Sub
        Else
            xclasse.Cidade = txtcidade.Text
        End If
        If txtestado.Text = "" Then

            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Estado}',  showConfirmButton: true})")
            txtestado.Focus()
            Exit Sub
        Else
            xclasse.Estado = txtestado.Text
        End If
        If txtcomplemento.Text = "" Then

            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Complemento}',  showConfirmButton: true})")
            txtcomplemento.Focus()
            Exit Sub
        Else
            xclasse.Complemento = txtcomplemento.Text
        End If
        'If txttelefone.Text = "" Then

        '    sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Telefone}',  showConfirmButton: true})")
        '    txttelefone.Focus()
        '    Exit Sub
        'Else
        xclasse.Telefone = txttelefone.Text
        ' End If
        If txtcelular.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Celular}',  showConfirmButton: true})")
            txtcelular.Focus()
            Exit Sub
        Else
            xclasse.Celular = txtcelular.Text
        End If
        If txtemail.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {E-mail}',  showConfirmButton: true})")
            txtemail.Focus()
            Exit Sub
        Else
            If validaemail(txtemail.Text) = True Then
                xclasse.Email = txtemail.Text
            Else
                sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor entre com um E-mail valido',  showConfirmButton: true})")
                txtemail.Focus()
                Exit Sub
            End If

        End If
        If txtsenha.Text = "" Then

            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Senha}',  showConfirmButton: true})")
            txtsenha.Focus()
            Exit Sub
        Else
            xclasse.Senha = txtsenha.Text
        End If
        If txtAgencia.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Agencia}',  showConfirmButton: true})")
            txtAgencia.Focus()
            Exit Sub
        Else
            xclasse.Agencia = txtAgencia.Text
            If txtAgencia.Text <> "" Then
                xclasse.Agencia = txtAgencia.Text
            Else

                sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Agencia}',  showConfirmButton: true})")
                txtcontacorrente.Focus()
                Exit Sub
            End If
        End If

        If txtcontacorrente.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Conta Corrente}',  showConfirmButton: true})")
            txtcontacorrente.Focus()
            Exit Sub
        Else
            If validarconta(txtcontacorrente.Text) Then
                xclasse.Contacorrente = txtcontacorrente.Text
            Else
                sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor verificar conta digitada',  showConfirmButton: true})")
                txtcontacorrente.Focus()
                Exit Sub
            End If
        End If

        'xclasse.autorizacao = txtautorizacao.text
        'xclasse.dtcad = txtdtcad.text
        'xclasse.usercad = txtusercad.text
        'xclasse.ativo_old = txtativo_old.text
        'xclasse.ativo = txtativo.text
        'xclasse.matriculactrl = txtmatriculactrl.text
        'xclasse.codigo = txtcodigo.text
        'xclasse.userexcluir = txtuserexcluir.text
        'xclasse.dtexcluido = txtdtexcluido.text
        'xclasse.dddcelular = txtdddcelular.text
        'xclasse.celular = txtcelular.text
        'xclasse.bloqueado_old = txtbloqueado_old.text
        'xclasse.bloqueado = txtbloqueado.text
        'xclasse.dtbloqueio = txtdtbloqueio.text
        'xclasse.userbloqueio = txtuserbloqueio.text
        'xclasse.dtdesbloqueio = txtdtdesbloqueio.text
        'xclasse.userdesbloqueio = txtuserdesbloqueio.text
        'xclasse.ip = txtip.text
        'xclasse.dtsolexc = txtdtsolexc.text
        'xclasse.motiexc = txtmotiexc.text
        'xclasse.dtinad = txtdtinad.text
        'xclasse.filiacao = txtfiliacao.text
        'xclasse.vivomorto = txtvivomorto.text
        'xclasse.negociado = txtnegociado.text
        'xclasse.negociado = txtnegociado.text


        If Not xclasse.procuramatricula Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Matricula ja foi Cadastrada',  showConfirmButton: true})")
            Exit Sub
        End If

        If xclasse.novoassociado() Then
            ''''''''' envia email para  autoatendimento anaplab 
            ''''''''If Not xclasse.emailautoatendimento Then
            ''''''''    sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Não foi possivel enviar o email !',  showConfirmButton: true})")

            ''''''''End If
            ''''''''' envia email para o novo associado
            ''''''''If Not xclasse.emailnovoassociado Then
            ''''''''    sm("Swal.fire({ type: 'error',  title: 'Não foi possivel enviar o email !',  confirmButtonText: 'OK'})")
            ''''''''End If

            xmensalidades.Nrseqcliente = xclasse.Nrseq
            If Not xmensalidades.gerarnovasmensalidades Then
                sm("Swal.fire({ type: 'error',  title: 'Não foi possivel gerar as mensalidades !',  confirmButtonText: 'OK'})")
            End If

            sm("Swal.fire({  position: 'center',  type: 'success',  title: 'Envio realizado com sucesso! Aguarde aprovação',  showConfirmButton: true})")
        Else
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Erro comunicação local web',  showConfirmButton: true})")
        End If


        limpar()
        habilitar(False)


    End Sub

    Public Function CalcularDigitoModulo11(ByVal numero As String, Optional AdmiteDigZero As Boolean = False, Optional pesoinicial As Integer = 2, Optional condicaobradesco As Boolean = False) As Integer


        Dim soma As String = 0
        Dim peso As Integer = pesoinicial
        Dim digito As Integer
        Dim base As Integer = 9
        If condicaobradesco Then base = 7

        For I = numero.Length - 1 To 0 Step -1

            soma = soma + numero.Substring(I, 1) * peso

            If pesoinicial <> 2 Then
                If (peso > 2) Then
                    peso = peso - 1
                Else
                    peso = pesoinicial
                End If
            Else
                If (peso < base) Then
                    peso = peso + 1
                Else
                    peso = 2
                End If
            End If

        Next I


        If condicaobradesco AndAlso (soma Mod 11) = 1 Then
            ' se o resto for 1, retorna "P", represnetado como -1 no digito
            Return -1
        End If

        digito = 11 - (soma Mod 11)

        If digito > 9 Then
            digito = 0
        End If

        If Not AdmiteDigZero AndAlso digito = 0 Then
            digito = 1
        End If

        Return digito

    End Function

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub habilitar(valor As Boolean)

        'txtnrseq.enabled = valor
        txtmatricula.Enabled = valor
        txtnome.Enabled = valor
        txtnomemae.Enabled = valor
        cbosituacaopb.Enabled = valor
        cbosexo.Enabled = valor
        txtdtnasc.Enabled = valor
        txtcpf.Enabled = valor
        txtrg.Enabled = valor
        txtcep.Enabled = valor
        txtendereco.Enabled = valor
        txtnumero.Enabled = valor
        txtcomplemento.Enabled = valor
        txtbairro.Enabled = valor
        txtcidade.Enabled = valor
        txtestado.Enabled = valor
        txtcelular.Enabled = valor
        txttelefone.Enabled = valor
        txtemail.Enabled = valor
        txtdtposse.Enabled = valor
        txtdtaposentadoria.Enabled = valor
        txtsenha.Enabled = valor
        txtAgencia.Enabled = valor
        txtcontacorrente.Enabled = valor
        '  txtmatricula2.Enabled = valor
        'txtautorizacao.enabled = valor
        'txtdtcad.enabled = valor
        'txtusercad.enabled = valor
        'txtativo_old.enabled = valor
        'txtativo.enabled = valor
        'txtmatriculactrl.enabled = valor
        'txtcodigo.enabled = valor
        'txtuserexcluir.enabled = valor
        'txtdtexcluido.enabled = valor
        'txtdddcelular.enabled = valor
        'txtcelular.enabled = valor
        'txtbloqueado_old.enabled = valor
        'txtbloqueado.enabled = valor
        'txtdtbloqueio.enabled = valor
        'txtuserbloqueio.enabled = valor
        'txtdtdesbloqueio.enabled = valor
        'txtuserdesbloqueio.enabled = valor
        'txtip.enabled = valor
        'txtdtsolexc.enabled = valor
        'txtmotiexc.enabled = valor
        'txtdtinad.enabled = valor
        'txtfiliacao.enabled = valor
        'txtvivomorto.enabled = valor
        'txtnegociado.enabled = valor
    End Sub

    Public Function validarconta(ByVal recebe As String) As Boolean

        If recebe <> "" Then
            Dim qtd As String = Len(recebe)
            Dim digitoinformado As String = Mid(recebe, qtd, 1)
            qtd = Len(recebe) - 1
            Dim valor As Integer
            Dim numero As Integer = 9
            Dim digitofinal As Integer

            recebe = Mid(recebe, 1, Len(qtd - 1))
            For x As Integer = 0 To qtd - 1
                If Not IsNumeric(Mid(recebe, qtd, 1)) Then
                    Continue For
                End If
                valor = Mid(recebe, qtd, 1) * numero
                digitofinal = digitofinal + valor


                If numero = 0 Then
                    numero = 9
                End If
                numero -= 1
                qtd = qtd - 1

                If qtd = 0 Then
                    qtd += 1
                End If
            Next

            digitofinal = digitofinal Mod 11

            If digitofinal <> 10 Then

                If IsNumeric(digitoinformado) = False Then
                    Return False
                    Exit Function
                End If

                If CType(digitoinformado, Integer) = digitofinal Then
                    Return True
                Else
                    Return False
                End If

            Else

                If CType(digitoinformado, String) = "X" Or CType(digitoinformado, String) = "x" Then
                    Return True
                Else
                    Return False
                End If
            End If

        Else
            Return False
        End If
    End Function

    Private Sub btnenviar_Click(sender As Object, e As EventArgs) Handles btnenviar.Click
        salvar()
    End Sub

    Private Sub txtemail_TextChanged(sender As Object, e As EventArgs) Handles txtemail.TextChanged

    End Sub

    Private Sub cbosituacaopb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbosituacaopb.SelectedIndexChanged

        If cbosituacaopb.SelectedValue = "1" Then
            txtdtposse.Enabled = True
            txtdtaposentadoria.Enabled = True

        ElseIf cbosituacaopb.SelectedValue = "2" Then
            txtdtposse.Enabled = True
            txtdtaposentadoria.Enabled = False
            txtdtaposentadoria.Text = ""
        ElseIf cbosituacaopb.SelectedValue = "3" Then
            txtdtposse.Enabled = False
            txtdtposse.Text = ""
            txtdtaposentadoria.Enabled = False
            txtdtaposentadoria.Text = ""
        End If

    End Sub
End Class

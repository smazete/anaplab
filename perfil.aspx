﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage.master" CodeFile="perfil.aspx.vb" Inherits="restrito_perfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        @font-face {
            /*     font-family: MyFont; 
                src:url('fonts/veteran_typewriter.ttf');
                src: url('fonts/veteran_typewriter.ttf?') format('embedded-opentype'), 
               url('fonts/veteran_typewriter.ttf') format('woff'), 
               url('fonts/veteran_typewriter.ttf') format('truetype');  */
            font-family: MyFontQuadro;
            src: url('fonts/CoalhandLuke.ttf'); /* this will be applied from IE 9 compatibility mode*/
            src: url('fonts/CoalhandLuke.ttf?') format('embedded-opentype'), /* IE prior versions */
            url('fonts/CoalhandLuke.ttf') format('woff'), /* modern browser that support web open font */
            url('fonts/CoalhandLuke.ttf') format('truetype'); /* browsers that support true type */
        }

        .LabelUsuario {
            font-family: MyFont, Arial;
            color: red;
            font-size: 24px;
        }

        .LabelAvisos {
            font-family: MyFontQuadro, Arial;
            color: yellow;
            font-size: 20px;
        }
    </style>
    <style>
        .imagemperfil {
            border-radius: 100%;
            border: solid;
            border-width: 0.5rem;
            border-color: #990033;
            width: 16rem;
            height: 16rem;
            -webkit-animation-name: example; /* Safari 4.0 - 8.0 */
            -webkit-animation-duration: 4s; /* Safari 4.0 - 8.0 */
            -webkit-animation-delay: 2s; /* Safari 4.0 - 8.0 */
            animation-name: example;
            animation-duration: 4s;
            animation-delay: 2s;
        }

        @-webkit-keyframes example {
            0% {
                border-color: white;
            }

            25% {
                border-color: yellow;
            }

            50% {
                border-color: blue;
            }

            75% {
                border-color: green;
            }

            100% {
                border-color: red;
            }
        }

        @keyframes example {
            0% {
                border-color: white;
            }

            25% {
                border-color: yellow;
            }

            50% {
                border-color: blue;
            }

            75% {
                border-color: green;
            }

            100% {
                border-color: red;
            }
        }

        .imagemperfil:hover {
            border-radius: 50%;
            border: solid;
            border-width: 0.5rem;
            border-color: dodgerblue;
            animation: gira 2s linear infinite;
        }

        @keyframes gira {
            to {
                transform: rotate(360deg);
            }
    </style>
    <style>
        #swal2-content {
            font-size: 12pt;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function exibiralerta1() {




            $('#alerta_conta').show();
            $('#alerta_conta').load();
        }
        function escondeexibiralerta1() {




            $('#alerta_conta').hide();
            $('#alerta_conta').hide();
        }
        function exibiralerta2() {
            $("html, body").animate({
                scrollTop: 0
            }, 1200);



            $('#alerta1').show();
            $('#alerta1').load();
        }
        function escondeexibiralerta2() {




            $('#alerta1').hide();
            $('#alerta1').hide();
        }
    </script>

    <section class="content-header">
        <h1>Perfil de Usuário
        <small>Apenas você consegue ver seu perfil! </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Interno</a></li>
            <li class="active">Perfil</li>
        </ol>
    </section>
    <br />
    <div class="box box-primary" style="margin-right: 1rem; margin-left: 1rem;">


        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <asp:ImageButton ID="imgperfil" runat="server" class="profile-user-img img-responsive img-circle" ImageUrl="~/img/heart01.png"></asp:ImageButton>
                            <h3>
                                <asp:Label ID="lblusuario" runat="server" class="profile-username text-center"></asp:Label></h3>
                            <p class="text-muted text-center">
                                <asp:Label ID="lblpapel" runat="server"></asp:Label>
                            </p>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Negociações</b> <a class="pull-right">
                                        <asp:Label ID="lbltotalbids" runat="server"></asp:Label></a>
                                </li>

                                <asp:Repeater ID="rptresumobids" runat="server">
                                    <ItemTemplate>
                                        <li class="list-group-item">
                                            <b>
                                                <asp:Label ID="lbltituloresumo" runat="server" Text='<%#Bind("titulo") %>'></asp:Label></b> <a class="pull-right">
                                                    <asp:Label ID="lblvalorresumo" runat="server" Text='<%#Bind("valor") %>'></asp:Label></a>
                                            <asp:HiddenField ID="hdpercresumo" runat="server" Value='<%#Bind("percentual") %>' />
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-red" role="progressbar"
                                                    runat="server" id="divbar01">
                                                    <%--<span class="sr-only">20% Complete</span>--%>
                                                </div>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>


                                <%--<li class="list-group-item">
                                    <b>Short-term</b> <a class="pull-right">
                                        <asp:Label ID="lblshortterm" runat="server"></asp:Label></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Mid-Term</b> <a class="pull-right">
                                        <asp:Label ID="lblmidterm" runat="server"></asp:Label></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Long-Term</b> <a class="pull-right">
                                        <asp:Label ID="lbllongterm" runat="server"></asp:Label></a>
                                </li>--%>
                            </ul>
                            <a href="painel.aspx" class="btn btn-primary btn-block"><b>Painel de Acompanhamento</b></a>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sobre mim</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <%--<strong><i class="fa fa-book margin-r-5"></i>Formação</strong>

                            <p class="text-muted">
                                B.S.I deu a bunda na facul
                            </p>

                            <hr>

                            <strong><i class="fa fa-map-marker margin-r-5"></i>ENdereço</strong>

                            <p class="text-muted">São jose dos pinhais, Parana</p>

                            <hr>--%>

                            <%-- <strong><i class="fa fa-pencil margin-r-5"></i>Skills</strong>

                            <p>
                                <span class="label label-danger">UI Design</span>
                                <span class="label label-success">Coding</span>
                                <span class="label label-info">Javascript</span>
                                <span class="label label-warning">PHP</span>
                                <span class="label label-primary">Node.js</span>
                            </p>--%>

                            <hr>

                            <strong><i class="fa fa-file-text-o margin-r-5"></i>Notes</strong>
                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <%--<span class="direct-chat-name pull-right">Sarah Bullock</span>--%>
                                    <%--' <span class="direct-chat-timestamp pull-left"></span>--%>
                                </div>
                                <!-- /.direct-chat-info -->
                                <%--<img class="direct-chat-img" src="../dist/img/user3-128x128.jpg" alt="Message User Image"><!-- /.direct-chat-img -->--%>
                                <div class="direct-chat-text">
                                    <asp:TextBox ID="txtobs" runat="server" CssClass="form-control" Style="background-color: transparent; border: none; border-color: transparent; height: 10rem;" TextMode="MultiLine"></asp:TextBox>
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <p></p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active" id="dadosinternos"><a href="#activity" data-toggle="tab">Informações</a></li>
                            <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
                            <%--<li><a href="#settings" data-toggle="tab">Settings</a></li>--%>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                                <!-- Post -->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="box box-primary">


                                            <div class="box-header with-border">
                                                <h3 class="box-title">Informações Pessoais</h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblCodigo" runat="server" Text="ID"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtCodigo" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-5">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblnome" runat="server" Text="Nome"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtnome" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblcep0" runat="server" Text="Data"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtdtcad" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <br />
                                                        <asp:CheckBox ID="chkAtivo" runat="server" Checked="True" Text="Ativo" />
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label9" runat="server" Text="E-mail"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtemail" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label5" runat="server" Text="Telefone"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txttel1" runat="server" class="form-control" onkeyup="formataTelefone(this,event);"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label6" runat="server" Text="Celular"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txttel2" runat="server" class="form-control" onkeyup="formataTelefone(this,event);"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-3 ">
                                                        CPF
                                                        <br />
                                                        <asp:TextBox ID="txtcpf" runat="server" CssClass="form-control " onkeyup="formataCPF(this,event);"></asp:TextBox>
                                                    </div>
                                                    <div class="col-lg-3 ">
                                                        RG
                                                        <br />
                                                        <asp:TextBox ID="txtrg" runat="server" CssClass="form-control "></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <div class="box box-primary">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Endereço</h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                                <i class="fa fa-minus"></i>
                                                            </button>

                                                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="row">
                                                            <div class="col-lg-2">

                                                                <asp:Label ID="lblcep" runat="server" Text="CEP"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtcep" runat="server" onkeyup="formataCEP(this,event);" MaxLength="9" class="form-control" AutoPostBack="true"></asp:TextBox>

                                                            </div>
                                                            <div class="col-lg-4">

                                                                <asp:Label ID="lblendereco" runat="server" Text="Endereço"></asp:Label>
                                                                <asp:TextBox ID="txtendereco" runat="server" class="form-control"></asp:TextBox>

                                                            </div>
                                                            <div class="col-lg-3">

                                                                <asp:Label ID="lblBairro" runat="server" Text="Bairro"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtBairro" runat="server" class="form-control"></asp:TextBox>

                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4">

                                                                <asp:Label ID="lblCidade" runat="server" Text="Cidade"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtcidade" runat="server" class="form-control"></asp:TextBox>

                                                            </div>
                                                            <div class="col-lg-2">

                                                                <asp:Label ID="lblestado" runat="server" Text="UF"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="txtestado" MaxLength="2" runat="server" class="form-control"></asp:TextBox>

                                                            </div>
                                                            <div class="col-lg-3">

                                                                <asp:Label ID="Label1" runat="server" Text="País"></asp:Label>
                                                                <br />
                                                                <asp:TextBox ID="cbopais" runat="server" class="form-control"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12 ">
                                                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                                                    <ProgressTemplate>
                                                                        <br />
                                                                        <img src="../img/carregando1.gif" width="50px" height="50px" />
                                                                    </ProgressTemplate>
                                                                </asp:UpdateProgress>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Dados da Empresa</h3>
                                                <div class="box-tools pull-right">
                                                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                        Empresa
                    <br />
                                        <asp:DropDownList ID="cboempresa"   runat="server" CssClass="form-control "></asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4">
                                        Cargo
                    <br />
                                        <asp:DropDownList ID="cbocargo"   runat="server" CssClass="form-control "></asp:DropDownList>
                                    </div>

                                    <div class="col-lg-2">
                                        BU
                    <br />
                                        <asp:DropDownList ID="cboBU"   runat="server" CssClass="form-control "></asp:DropDownList>
                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <div id="alerta1" style="display: none;">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                    <strong>Atenção: Verifique os itens abaixo !</strong>
                                                    <asp:GridView ID="gradeerro" BackColor="Transparent" ForeColor="white" runat="server" AutoGenerateColumns="False" BorderColor="White" BorderStyle="none" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None">
                                                        <Columns>
                                                            <asp:BoundField DataField="erro">
                                                                <ItemStyle BackColor="Transparent" ForeColor="White" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>


                                            <div class="box box-primary">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Informações de Acesso</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="box-body">

                                                    <div class="row">

                                                        <div class="col-lg-4 text-center">


                                                            <asp:Label ID="Label2" runat="server" Text="Usuário"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtusuario" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>

                                                        </div>
                                                        
                                                        <div class="col-lg-8  ">
                                                            <div class="row">
                                                                <div class="col-lg-12 text-center">
                                                                    <br />
                                                                    <asp:Button ID="btnalterarsenha" runat="server" Text="Alterar Minha Senha" CssClass="btn btn-warning " />

                                                                </div>
                                                            </div>

                                                            <div class="box box-success " id="divsenhas" runat="server">

                                                              <div class="row">
                                                                <div class="col-lg-6">
                                      <div class="row">
                                          <div class="col-lg-12 ">
                                               Por favor, se atente às condições abaixo:<br />
            <br />
            <asp:Image ID="imgNumerosLetras" runat="server" ImageUrl="~/IMG/button_cancel.png" />
&nbsp;Números e letras minúsculas<br />
            <asp:Image ID="imgSimbolos" runat="server" ImageUrl="~/IMG/button_cancel.png" />
&nbsp;Símbolos especiais<br />
            <asp:Image ID="imgLetraMaiuscula" runat="server" ImageUrl="~/IMG/button_cancel.png" />
&nbsp;Uma letra maíuscula<br />
            <asp:Image ID="imgSenhasConferem" runat="server" ImageUrl="~/IMG/button_cancel.png" />
&nbsp;As senhas conferem<br />
            <asp:Image ID="imgSenhaAntiga" runat="server" ImageUrl="~/IMG/button_cancel.png" />
&nbsp;A senha antiga não confere
                                      </div>
                 </div>
                                                                    </div> 
                                                                  <div class="col-lg-6">
                                                                      <div class="row">
                                                                           <div class="col-lg-12">
                                                                          Senha Antiga
                                                                          <br />
                                                                          <asp:TextBox ID="txtsenhaantiga" TextMode="Password" runat="server" class="form-control" ></asp:TextBox>
                               </div>                                       </div>
                                                                      <div class="row">
                                                                                                                        <div class="col-lg-12">
                                                                          Senha Nova
                                                                          <br />
                                                                          <asp:TextBox ID="txtSenhaNova" TextMode="Password" runat="server" class="form-control" ></asp:TextBox>
                                                                                                                            </div> 
                                                                      </div>
                                                                      <div class="row">
                                                                                                                        <div class="col-lg-12">
                                                                          Confirme a senha
                                                                          <br />
                                                                          <asp:TextBox ID="txtConfirmar" TextMode="Password" runat="server" class="form-control" ></asp:TextBox>
                                                                                                                            </div> 
                                                                      </div>
                                                                    </div> 

                                                                  </div> 
                                                            <div class="row">
                                                                <div class="col-lg-6 text-center ">
                                                                    <br />
                                                                    <asp:LinkButton ID="btnconfirmarnovasenha" runat="server" CssClass="btn btn-success ">Confirmar Nova Senha</asp:LinkButton>
                                                                </div>
                                                                   <div class="col-lg-6 text-center ">
                                                                    <br />
                                                                    <asp:LinkButton ID="btncancelarsenha" runat="server" CssClass="btn btn-danger ">Cancelar Alteração</asp:LinkButton>
                                                                </div>
                                                            </div> 
                                                                
                                                            </div>
                                                            <%--<asp:Label ID="Label4" runat="server" Text="Senha"></asp:Label>--%>
                                                            


                                                        </div>


                                                    </div>



                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12 text-center">



                                                        <asp:Button ID="btnsalvar" runat="server" Text="Salvar" CssClass="btn btn-success" />


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                         <div class="col-lg-12 ">
                                                             <small >Propriedades</small>
                                                             
                                                               <p>
                                <span class="label label-danger"><asp:Label ID="lblgrupo" runat ="server" ></asp:Label></span>
                                <span class="label label-success"> <asp:Label ID="lblmaster" runat ="server" ></asp:Label></span>
                             <%--   <span class="label label-info"><asp:Label ID="lblfilial" runat ="server" ></asp:Label></span>
                                <span class="label label-warning"><asp:Label ID="lblpais" runat ="server" ></asp:Label></span>--%>
                                <span class="label label-primary"><asp:Label ID="lblpermissao" runat ="server" ></asp:Label></span>
                            </p>
                                                         </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <!-- /.post -->



                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">
                                <asp:UpdatePanel ID="updatetimeline" runat="server">
                                    <ContentTemplate>


                                        <!-- The timeline -->
                                        <ul class="timeline timeline-inverse">
                                            <!-- timeline time label -->
                                            <li class="time-label">
                                                <span class="bg-red">
                                                    <asp:Label ID="lblhoje" runat="server"></asp:Label>
                                                </span>
                                            </li>
                                            <!-- /.timeline-label -->
                                            <!-- timeline item -->
                                            <asp:Repeater ID="rpttimeline" runat="server">
                                                <ItemTemplate>
                                                    <%--<i class="fa fa-comment  " aria-hidden="true" runat="server" id="icolog"></i>--%>
                                                    <asp:Image runat="server" ID="imgteste" />
                                                    <li>


                                                        <div class="timeline-item">
                                                            <span class="time"><i class="fa fa-clock-o"></i>
                                                                <asp:Label ID="lblhoratime" runat="server" Text='<%#Bind("hora") %>'> </asp:Label>
                                                            </span>

                                                            <h3 class="timeline-header"><a href="#">
                                                                <asp:Label ID="Label3" runat="server" Text='<%#Bind("tipodelog") %>'></asp:Label></a>   <a class="btn btn-primary btn-xs">
                                                                    <asp:Label ID="lbldtcadtime" runat="server" Text='<%#Bind("dtcad") %>'></asp:Label></a> </h3>

                                                            <div class="timeline-body">
                                                                <asp:Label ID="lbldetalhetime" runat="server" Text='<%#Bind("acao") %>'></asp:Label>
                                                                <br />

                                                            </div>
                                                            <div class="timeline-footer">

                                                                <a class="btn btn-danger btn-xs">IP: 
                                                                    <asp:Label ID="lbliptime" runat="server" Text='<%#Bind("ip") %>'></asp:Label></a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>


                                            <asp:LinkButton ID="btnvertudo" runat="server" Text="View All Logs" Style="margin-left: 5rem;" />
                                            <li>
                                                <i class="fa fa-clock-o bg-gray"></i>
                                            </li>
                                        </ul>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <!-- /.tab-pane -->

                            <%--  <div class="tab-pane" id="settings">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-2 control-label">Name</label>

                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputName" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-2 control-label">Name</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputName" placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputExperience" class="col-sm-2 control-label">Experience</label>

                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputSkills" class="col-sm-2 control-label">Skills</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox">
                                                    I agree to the <a href="#">terms and conditions</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-danger">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->--%>
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->

    </div>

</asp:Content>

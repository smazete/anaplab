﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsSmart
Imports System.IO
Imports System.Data.OleDb

Partial Class restrito_procedimentos
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tabprocedimentos As New clsBanco
    Dim tbprocedimentos As New Data.DataTable
    Dim tabelaEmpresa As String = " tbempresas"
    Dim sql As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        sm("carregaMaskMoney();", "carregaMaskMoney")
        If gradeprocedimentos.Rows.Count > 0 Then
            btnsalvargdv.Visible = True
            btnReajustar.Enabled = True
            btnimprimirtodosproc.Enabled = True
            btnimprimirtodosproc.Visible = True
            divprocedimentos.Visible = True
        Else
            btnsalvargdv.Visible = False
            btnReajustar.Enabled = False
            btnimprimirtodosproc.Enabled = False
            btnimprimirtodosproc.Visible = False
            divprocedimentos.Visible = False
        End If

        If IsPostBack Then Exit Sub

        adicionaConvenio()
        adicionaPlano()
        bloqueio()
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub adicionaPlano()
        ddlPlano.Items.Clear()
        cboplano.Items.Clear()
        tbprocedimentos = tabprocedimentos.conectar("select distinct descricao from tbplanos order by descricao")

        ddlPlano.Items.Insert(0, New ListItem("Selecione", ""))
        cboplano.Items.Insert(0, New ListItem("Selecione", ""))
        If tbprocedimentos.Rows.Count > 0 Then
            For x As Integer = 0 To tbprocedimentos.Rows.Count - 1
                ddlPlano.Items.Add(New ListItem(tbprocedimentos.Rows(x)("descricao").ToString, tbprocedimentos.Rows(x)("descricao").ToString))
                cboplano.Items.Add(New ListItem(tbprocedimentos.Rows(x)("descricao").ToString, tbprocedimentos.Rows(x)("descricao").ToString))
            Next
        End If
    End Sub

    Private Sub adicionaConvenio()
        ddlConvenio.Items.Clear()
        cboconvenio.Items.Clear()
        tbprocedimentos = tabprocedimentos.conectar("select distinct nomefantasia, nrseq from tbconvenios where ativo = true order by nomefantasia")

        ddlConvenio.Items.Insert(0, New ListItem("Selecione", ""))
        cboconvenio.Items.Insert(0, New ListItem("Selecione", ""))
        If tbprocedimentos.Rows.Count > 0 Then
            For x As Integer = 0 To tbprocedimentos.Rows.Count - 1
                ddlConvenio.Items.Add(New ListItem(tbprocedimentos.Rows(x)("nomefantasia").ToString, tbprocedimentos.Rows(x)("nrseq").ToString))
                cboconvenio.Items.Add(New ListItem(tbprocedimentos.Rows(x)("nomefantasia").ToString, tbprocedimentos.Rows(x)("nrseq").ToString))
            Next
        End If
    End Sub

    Private Sub carregarprocedimentos(Optional descricaoconvenios As String = "", Optional plano As String = "", Optional nrseq As Integer = 0, Optional descricaoprocedimentos As String = "", Optional ativo As Boolean = True)
        Dim xprocedimentos As New clsProcedimentos

        Dim sqltext As String = ""
        If nrseq <> 0 Then
            sqltext &= " and nrseq='" & nrseq & "'"
        End If

        If descricaoprocedimentos <> "" Then
            sqltext &= " and descricaoconvenio like '" & descricaoprocedimentos & "%'"
        End If

        If descricaoconvenios <> "" Then
            sqltext &= " and descricaoconvenio like '" & descricaoconvenios & "%'"
        End If

        If plano <> "" Then
            sqltext &= " and plano= '" & plano & "'"
        End If

        If logico(ativo) IsNot Nothing Then
            sqltext &= " and ativo=" & ativo
        End If

        xprocedimentos.Nrseqempresa = Session("idempresaemuso")

        If Not xprocedimentos.carregarprocedimentos() Then
            sm("swal.fire({title: 'Ops!',text: 'Selecione um plano/convênio válidos !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        gradeprocedimentos.DataSource = xprocedimentos.Table
        gradeprocedimentos.DataBind()
    End Sub

    Private Sub btnprocura_Click(sender As Object, e As EventArgs) Handles btnprocura.Click
        If ddlPlano.SelectedValue = Nothing Or ddlConvenio.SelectedValue = Nothing Then
            sm("swal.fire({title: 'Ops!',text: 'Selecione um plano/convênio válidos !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        Dim xproce As New clsProcedimentos

        carregarprocedimentos(ddlConvenio.SelectedItem.Text, ddlPlano.SelectedItem.Text)

        If gradeprocedimentos.Rows.Count > 0 Then
            btnsalvargdv.Visible = True
            btnReajustar.Enabled = True
            btncarregarxls.Enabled = True
            btnimprimirtodosproc.Visible = True
            divprocedimentos.Visible = True
        Else
            btnsalvargdv.Visible = False
            btnReajustar.Enabled = False
            btnimprimirtodosproc.Visible = False
            divprocedimentos.Visible = False
        End If

    End Sub



    Private Sub bloqueio()

        txtdescricao.ReadOnly = True
        txtvalorcusto.ReadOnly = True
        txtvalorfinal.ReadOnly = True
        txtcodamb.ReadOnly = True
        txtcodtuss.ReadOnly = True
        cboincluivalor.Enabled = False
        txt_percente_cob_plano.ReadOnly = True
        'txt_valor_cob_plano.ReadOnly = True
        'txt_percente_paciente.ReadOnly = True
        btnsalvar.Enabled = False

        txtcodigo.Text = ""
        txtdescricao.Text = ""
        txtvalorcusto.Text = ""
        txtvalorfinal.Text = ""
        txtcodamb.Text = ""
        txtcodtuss.Text = ""
        txt_percente_cob_plano.Text = ""
        txt_valor_cob_plano.Text = ""
        txt_percente_paciente.Text = ""
        txt_valor_paciente.Text = ""
        cboincluivalor.Checked = False
    End Sub

    Private Sub habilita()
        txtdescricao.ReadOnly = False
        txtvalorcusto.ReadOnly = False
        txtvalorfinal.ReadOnly = False
        txtcodamb.ReadOnly = False
        txtcodtuss.ReadOnly = False
        cboincluivalor.Enabled = True
        txt_percente_cob_plano.ReadOnly = False
        'txt_valor_cob_plano.ReadOnly = False
        'txt_percente_paciente.ReadOnly = False
        btncancelar.Enabled = True
        btnsalvar.Enabled = True
    End Sub

    Private Function inserirProcedimentos()
        Dim incluir As Boolean = 0
        Dim valor1 As String = "Null"
        Dim valor2 As String = "Null"
        Dim valor3 As String = "Null"
        Dim valor4 As String = "Null"

        If cboincluivalor.Checked = True Then
            incluir = 1
        End If

        valor1 = numeros(txtvalorcusto.Text)
        valor2 = numeros(txtvalorfinal.Text)
        valor3 = numeros(txt_valor_cob_plano.Text)
        valor4 = numeros(txt_percente_paciente.Text)
        Dim sql As String = "UPDATE tbprocedimentos SET   descricao = '" & txtdescricao.Text & "', valorcusto = " & valor1 & ", valorfinal = " & valor2 & ", codigoamb = '" & txtcodamb.Text & "', codigosus = '" & txtcodtuss.Text & "', naoincluircaixa = " & incluir & ", percobplano = '" & txt_percente_cob_plano.Text.Replace("%", "") & "', valorcobplano = " & valor3 & ", valorpaciente = " & valor4 & ", ativo = True, plano = '" & Session("nrseq_plano") & "', convenio = '" & Session("nrseqconvenio") & "' WHERE nrseq = '" & Session("nrseqprocedimento") & "'"

        tbprocedimentos = tabprocedimentos.IncluirAlterarDados(sql)

        Session("nrseqscripts") = ""

        bloqueio()

        If ddlPlano.SelectedValue = Nothing Or ddlConvenio.SelectedValue = Nothing Then
            Dim erro As String = ""
            ' Quantas você quiser dessa coisa... pra cada campo
            erro = verificaCampo(ddlPlano.SelectedValue, "Plano", erro)
            erro = verificaCampo(ddlPlano.SelectedValue, "Convênio", erro)

            If erro <> "" Then
                rowError.Style.Remove("display")
                lblErro.InnerHtml = erro
            End If
        Else
            gradeprocedimentos.DataSource = Nothing
            gradeprocedimentos.DataBind()

            carregarprocedimentos(Session("nrseqconvenio"), Session("nrseq_plano"), ativo:=True)

        End If
    End Function


    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click

        Dim xproce As New clsProcedimentos

        xproce.Nrseq = txtcodigo.Text
        xproce.Plano = ddlPlano.SelectedItem.Text
        xproce.Descricaoconvenio = ddlConvenio.SelectedItem.Text
        xproce.Descricao = txtdescricao.Text
        xproce.Codigoamb = txtcodamb.Text
        xproce.Codigosus = txtcodtuss.Text

        xproce.Valorcobplano = txt_valor_cob_plano.Text
        xproce.Valorcusto = txtvalorcusto.Text
        xproce.Valorfinal = txtvalorfinal.Text
        xproce.Valorpaciente = txt_valor_paciente.Text
        xproce.Percobplano = txt_percente_cob_plano.Text
        xproce.Perccobpaciente = txt_percente_paciente.Text
        If Not xproce.salvar Then
            sm("swal.fire({title: 'Ops!',text: '" & xproce.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        carregarprocedimentos()
        btnnovo.Enabled = True
        bloqueio()

        sm("swal.fire({title: 'Legal !',text: '" & xproce.Mensagemerro & "',type: 'success',confirmButtonText: 'OK'})", "swal")

    End Sub

    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click
        If ddlPlano.SelectedValue = Nothing Or ddlConvenio.SelectedValue = Nothing Then
            sm("swal.fire({title: 'Ops!',text: 'Selecione um plano/convênio válidos !',type: 'error',confirmButtonText: 'OK'})", "swal")
        Else
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tbprocedimentos = tabprocedimentos.IncluirAlterarDados("insert into tbprocedimentos (usercad, dtcad, nrseqctrl, nrseqempresa) values ('" & Session("usuario") & "','" & formatadatamysql(Date.Now.Date) & "','" & wcnrseqctrl & "', '" & Session("idempresaemuso") & "')")
            tbprocedimentos = tabprocedimentos.conectar("select * from tbprocedimentos where nrseqctrl = '" & wcnrseqctrl & "'")

            If tbprocedimentos.Rows.Count <> 0 Then
                Session("nrseqprocedimento") = tbprocedimentos.Rows(0)("nrseq").ToString
            Else
                Session("nrseqprocedimento") = 1
            End If

            Session("nrseq_plano") = ddlPlano.SelectedValue

            Session("nrseqconvenio") = ddlConvenio.SelectedValue

            txtcodigo.Text = Session("nrseqprocedimento")
            btnnovo.Enabled = False
            habilita()
            rowError.Style.Add("display", "none")
        End If
    End Sub

    Private Sub gradeprocedimentos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeprocedimentos.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeprocedimentos.Rows(index)
        Dim xprocedimentos As New clsProcedimentos
        If e.CommandName = "editar" Then
            Dim nrseq As HiddenField = row.FindControl("hdnnrseq")
            xprocedimentos.Nrseq = nrseq.Value
            xprocedimentos.Nrseqempresa = Session("idempresaemuso")
            xprocedimentos.Ativo = True
            If Not xprocedimentos.procurar(True) Then
                sm("swal.fire({title: 'Ops!',text: '" & xprocedimentos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            If xprocedimentos.Contador > 0 Then

                Session("nrseqprocedimento") = xprocedimentos.Nrseq
                txtcodigo.Text = xprocedimentos.Nrseq

                txtdescricao.Text = xprocedimentos.Descricao


                txtvalorcusto.Text = xprocedimentos.Valorcusto

                txtvalorfinal.Text = xprocedimentos.Valorfinal

                txtcodamb.Text = xprocedimentos.Codigoamb

                txtcodtuss.Text = xprocedimentos.Codigosus

                txt_percente_cob_plano.Text = porcentagem(xprocedimentos.Percobplano, True)

                txt_valor_cob_plano.Text = porcentagem(xprocedimentos.Valorcobplano, True)

                txt_percente_paciente.Text = porcentagem(xprocedimentos.Valorpaciente, True)

                If logico(xprocedimentos.Naoincluircaixa) = True Then
                    cboincluivalor.Checked = True
                Else
                    cboincluivalor.Checked = False
                End If


            End If
            habilita()
        End If

        If e.CommandName = "deletar" Then
            Dim nrseq As String = row.Cells(0).Text
            tbprocedimentos = tabprocedimentos.conectar("Select * FROM tbprocedimentos WHERE nrseq = '" & nrseq & "' AND nrseqempresa = '" & Session("idempresaemuso") & "' ")

            tbprocedimentos = tabprocedimentos.IncluirAlterarDados("UPDATE tbprocedimentos SET ativo = NULL WHERE  nrseq = " & tbprocedimentos.Rows(0)("nrseq") & " ")
            If ddlPlano.SelectedValue = Nothing Or ddlConvenio.SelectedValue = Nothing Then
                Dim erro As String = ""
                ' Quantas você quiser dessa coisa... pra cada campo
                erro = verificaCampo(ddlPlano.SelectedValue, "Plano", erro)
                erro = verificaCampo(ddlPlano.SelectedValue, "Convênio", erro)

                If erro <> "" Then
                    rowError.Style.Remove("display")
                    lblErro.InnerHtml = erro
                End If
            Else

                carregarprocedimentos(Session("nrseqconvenio"), Session("nrseq_plano"), ativo:=True)

            End If
        End If

        If e.CommandName = "imprimir" Then
            Dim nrseq As String = row.Cells(0).Text
            Response.Redirect("relatorios/imprimir_procedimentos.aspx?i=" & nrseq)
        End If

    End Sub

    Private Sub ddlPlano_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPlano.SelectedIndexChanged
        Session("nrseq_plano") = ddlPlano.SelectedValue
    End Sub

    Private Sub ddlConvenio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlConvenio.SelectedIndexChanged
        Session("nrseqconvenio") = ddlConvenio.SelectedValue
    End Sub

    Private Sub gradeprocedimentos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeprocedimentos.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        e.Row.Cells(6).Text = FormatDateTime(e.Row.Cells(6).Text, DateFormat.ShortDate)

        If e.Row.Cells(3).Text <> "&nbsp;" And e.Row.Cells(3).Text <> "" Then
            e.Row.Cells(3).Text = "R$ " & FormatNumber(e.Row.Cells(3).Text, 2)
        End If

        If e.Row.Cells(5).Text <> "&nbsp;" And e.Row.Cells(5).Text <> "" Then
            e.Row.Cells(5).Text = "R$ " & FormatNumber(e.Row.Cells(5).Text, 2)
        End If

    End Sub

    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function
    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function

    Private Function carregaEmpresa() As Data.DataTable
        Dim nrseqempresa As String = Session("idempresaemuso")
        Dim sql = "SELECT * FROM " & tabelaEmpresa & " WHERE ativo = true AND nrseq = '" & nrseqempresa & "' "
        Return findSql(sql)
    End Function

    Public Function verificaCampo(input As String, campo As String, erro As String) As String
        If input = "" Then
            If erro <> "" Then
                erro &= ",<br>"
            Else
                erro &= "<br>"
            End If
            erro &= campo
        End If
        Return erro
    End Function

    Private Function converterParaDouble(txt As String) As Double
        If txt.Contains(" ") Then
            txt = txt.Replace(" ", "")
        End If
        If txt.Contains("%") Then
            txt = txt.Replace("%", "")
        End If
        If txt.Contains("R$") Then
            txt = txt.Replace("R$", "")
        End If

        Return txt

    End Function


    Public Sub valoraltera_TextChanged(sender As Object, e As EventArgs)

        Dim valorfinal As Decimal = numeros(txtvalorfinal.Text)

        Dim percentualpaciente As Decimal = numeros(txt_percente_paciente.Text)
        Dim valorplano As Decimal = numeros(txt_valor_cob_plano.Text)
        Dim valorpaciente As Decimal = numeros(txt_valor_paciente.Text)

        Dim percentualplano As Decimal = 0

        If txt_percente_cob_plano.Text = "" Then
            txt_percente_cob_plano.Text = 0
            txt_percente_paciente.Text = 100
            txt_valor_cob_plano.Text = 0
            txt_valor_paciente.Text = valorfinal
        Else
            If numeros(txt_percente_cob_plano.Text) > 100 Then
                txt_percente_cob_plano.Text = "100"
            End If
            percentualplano = porcentagem(txt_percente_cob_plano.Text, True)
            txt_percente_paciente.Text = FormatNumber(100 - percentualplano, 2)
            txt_valor_cob_plano.Text = FormatCurrency((percentualplano * valorfinal) / 100)
            txt_valor_paciente.Text = FormatCurrency(valorfinal - txt_valor_cob_plano.Text)

        End If
        txt_percente_cob_plano.Text = FormatNumber(percentualplano, 2)

        'Dim btn As TextBox = CType(sender, TextBox)

        'If hdnCount.Text <> "" AndAlso hdnCount.Text > 0 Then
        '    hdnCount.Text = hdnCount.Text + 1

        '    If hdnCount.Text = 4 Then
        '        hdnCount.Text = 0
        '        Exit Sub
        '    End If

        'Else
        '    hdnCount.Text = 1
        '    If btn.ID = "txt_percente_paciente" Then
        '        Dim valorfinal As Double = numeros(txtvalorfinal.Text)
        '        Dim percentepaciente As Double = numeros(txt_percente_paciente.Text)
        '        Dim percenteplano As Double = 100 - percentepaciente
        '        Dim valorplano As Double = (valorfinal * percenteplano) / 100

        '        txt_valor_cob_plano.Text = valorplano
        '        txt_percente_cob_plano.Text = percenteplano

        '    ElseIf btn.ID = "txt_valor_cob_plano" Then
        '        Dim valorfinal As Double = numeros(txtvalorfinal.Text)
        '        Dim valorplano As Double = numeros(txt_valor_cob_plano.Text)

        '        Dim percenteplano As Double = (valorplano * 100) / valorfinal
        '        Dim percentepaciente As Double = 100 - percenteplano

        '        txt_percente_paciente.Text = percentepaciente
        '        txt_percente_cob_plano.Text = percenteplano

        '    ElseIf btn.ID = "txt_percente_cob_plano" Then
        '        Dim valorfinal As Double = numeros(txtvalorfinal.Text)
        '        Dim percenteplano As Double = numeros(txt_percente_cob_plano.Text)
        '        Dim percentepaciente As Double = 100 - percenteplano
        '        Dim valorplano As Double = (valorfinal * percenteplano) / 100

        '        txt_valor_cob_plano.Text = valorplano
        '        txt_percente_paciente.Text = percentepaciente
        '    ElseIf btn.ID = "txtvalorfinal" Then
        '        If numeros(txt_percente_cob_plano.Text) = 0 AndAlso numeros(txt_percente_paciente.Text) = 0 Then
        '            txt_percente_paciente.Text = 100
        '        Else
        '            Dim valorfinal As Double = numeros(txtvalorfinal.Text)
        '            Dim valorplano As Double = (valorfinal * numeros(txt_percente_cob_plano.Text)) / 100

        '            txt_valor_cob_plano.Text = valorplano
        '        End If
        '    End If

        'End If



    End Sub

    Private Sub btnsalvargdv_Click(sender As Object, e As EventArgs) Handles btnsalvargdv.Click


        Dim updateValores As List(Of Valores) = New List(Of Valores)

        For Each row As GridViewRow In gradeprocedimentos.Rows
            Dim nrseq As String = row.Cells(0).Text
            Dim oldcusto As String = row.Cells(7).Text
            Dim oldpaciente As String = row.Cells(8).Text
            Dim oldfinal As String = row.Cells(9).Text
            Dim newcusto As String = CType(row.Cells(3).FindControl("valorcusto"), TextBox).Text
            Dim newpaciente As String = CType(row.Cells(4).FindControl("valorpaciente"), TextBox).Text
            Dim newfinal As String = CType(row.Cells(5).FindControl("valorfinal"), TextBox).Text

            newcusto = newcusto.Replace("R$ ", "").Replace(".", "").Replace(" ", "")
            newpaciente = newpaciente.Replace("R$ ", "").Replace(".", "").Replace("%", "").Replace(" ", "")
            newfinal = newfinal.Replace("R$ ", "").Replace(".", "").Replace(" ", "")

            If oldfinal <> newfinal OrElse oldcusto <> newcusto OrElse oldpaciente <> newpaciente Then
                updateValores.Add(New Valores(nrseq, newfinal, newpaciente, newcusto))
            End If
        Next

        For Each valor As Valores In updateValores
            sql = "UPDATE tbprocedimentos SET valorpaciente = " & valor.porcentagempaciente.Replace(",", ".") & ", valorcusto = " & valor.valorcusto.Replace(",", ".") & ", valorfinal = " & valor.valorfinal.Replace(",", ".") & " WHERE nrseq = " & valor.nrseq & ""
            persist(sql)
        Next


        carregarprocedimentos(Session("nrseqconvenio"), Session("nrseq_plano"), ativo:=True)


    End Sub

    Private Sub btnReajustar_Click(sender As Object, e As EventArgs) Handles btnReajustar.Click


        Dim custo As Double = numeros(txtvalorcustoreajuste.Text)
        Dim final As Double = numeros(txtvalorfinalreajuste.Text)

        For Each row As GridViewRow In gradeprocedimentos.Rows

            Dim valorcusto As TextBox = CType(row.FindControl("valorcusto"), TextBox)
            Dim valorfinal As TextBox = CType(row.FindControl("valorfinal"), TextBox)

            valorcusto.Text = numeros(valorcusto.Text) + numeros(valorcusto.Text) * (custo / 100)
            valorfinal.Text = numeros(valorfinal.Text) + numeros(valorfinal.Text) * (final / 100)

        Next

    End Sub

    Private Sub btncarregarxls_Click(sender As Object, e As EventArgs) Handles btncarregarxls.Click



        Dim tbarq As New Data.DataTable
        Dim tabarq As New clsBanco
        Dim uploadedFile As HttpPostedFile = FileUpload1.PostedFile
        Dim FileName As String = "", nomecompleto As String = ""
        Dim FolderPath As String = ConfigurationManager.AppSettings("FolderPath")
        Dim qtdlinhas As Integer = 0
        'Dim FilePath As String '= Server.MapPath("~\importacaocarteira\" & FileName)
        If uploadedFile.FileName = "" Then Exit Sub


        'FilePath = Server.MapPath("~\planilhas")
        FileName = alteranome(Server.MapPath("~") & "planilhas\planilha." & encontrachr(uploadedFile.FileName, ".", 0))
        nomecompleto = FileName
        'FilePath = Server.MapPath("~\planilhas\" & FileName)
        Try
            FileUpload1.SaveAs(nomecompleto)
            '  processarxml(FilePath)
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tbarq = tabarq.IncluirAlterarDados("insert into tbprocedimentos_planilhas (arquivo, dtcad, usercad, ativo, linhas, processado) values ('" & mARQUIVO(FileName) & "', '" & formatadatamysql(data) & "', '" & Session("usuario") & "', true," & qtdlinhas & ", False)")
            listararquivos()
            btnprocessararq.Enabled = True
            'Session("filepath") = FilePath
            'processarxml()

            ' Dim teste As New Threading.Thread(AddressOf processarxml)
            'teste.Start()
        Catch ex As Exception
            Dim lixo As String = ex.Message
            lixo &= "1"
        End Try
    End Sub
    Private Sub listararquivos()
        Dim tbarq As New Data.DataTable
        Dim tabarq As New clsBanco
        tbarq = tabarq.conectar("select * from tbprocedimentos_planilhas where processado = false order by nrseq desc")
        gradearquivos.DataSource = tbarq
        gradearquivos.DataBind()
    End Sub
    Private Sub processar()

        For Each row As GridViewRow In gradearquivos.Rows
            Dim sel As CheckBox = row.FindControl("sel")
            Dim lblnrseq As HiddenField = row.FindControl("lblnrseq")
            If sel.Checked Then

                Dim tb1 As New Data.DataTable
                Dim tab1 As New clsBanco
                Dim qtdlinhas As Integer = processarxml(row.Cells(1).Text, lblnrseq.Value, hdconexao.Value)
                tb1 = tab1.IncluirAlterarDados("update tbprocedimentos_planilhas set linhas = " & qtdlinhas & ", processado = " & IIf(qtdlinhas = -1, "False", "True") & ", dtprocessado = '" & valordatamysql(data) & "', userprocessado = '" & Session("usuario") & "' where nrseq = " & lblnrseq.Value)
            End If
        Next
        listararquivos()

    End Sub
    Private Function processarxml(arquivo As String, nrseqarquivo As Integer, xconexao As String) As Integer
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim tbplanilha As New Data.DataTable
        Dim tbprocs As New Data.DataTable
        Dim tabprocs As New clsBanco
        Dim wclinhaatual As Integer = 0
        Dim wcsessao As String = xconexao
        tb1 = tab1.conectar("select linhaatual from tbprocedimentos_planilhas  where  nrseq = " & nrseqarquivo)
        Dim linhapular As Integer = 0
        If tb1.Rows.Count > 0 Then
            If tb1.Rows(0)("linhaatual").ToString <> "" Then
                linhapular = tb1.Rows(0)("linhaatual").ToString
            End If

        End If
        ''''''''''      Dim conexao_Excel As String = "Provider=Microsoft.Jet.OleDb.4.0;data source=" & Server.MapPath("~") & "planilhas\" & arquivo & ";Extended Properties=Excel 8.0;"



        'Dim conexao As OleDbConnection = New OleDbConnection(conexao_Excel)
        'Dim da As OleDbDataAdapter
        'da = New OleDbDataAdapter("Select * From [importa$]", conexao)



        'da.Fill(tbplanilha)

        'tbplanilha = csvToDatatable(Server.MapPath("~") & "planilhas\" & arquivo, ";", alteranome(Server.MapPath("~") & "logerros\cargaprocedimento.txt"))



        'tbprocs = tabprocs.conectar("select * from tbprocedimentos where plano = '" & cboplano.Text & "' and convenio = " & hdconvenio.Value & " and ativo = true AND nrseqempresa = '" & Session("idempresaemuso") & "' ")

        'For x As Integer = 0 To tbplanilha.Rows.Count - 1
        '    If x < linhapular Then
        '        Continue For
        '    End If
        '    If tbplanilha.Rows(x)("exame").ToString = "" Then
        '        Continue For
        '    End If
        '    Dim tbprocura As Data.DataRow()
        '    tbprocura = tbprocs.Select("descricao = '" & tbplanilha.Rows(x)("exame").ToString & "'")
        '    If tbprocura.Count > 0 Then

        '        tb1 = tab1.IncluirAlterarDados("update tbprocedimentos set valorfinal = " & moeda(tbplanilha.Rows(x)("final").ToString) & ", valorcusto= " & moeda(tbplanilha.Rows(x)("custo").ToString) & " where nrseq = " & tbprocura(0)("nrseq").ToString)
        '    Else
        '        tb1 = tab1.IncluirAlterarDados("insert into tbprocedimentos (plano, convenio, descricao, valorfinal, valorcusto, dtcad, usercad, cod, ativo, nrseqempresa) values ('" & cboplano.SelectedItem.Text & "', " & hdconvenio.Value & ", '" & tratatexto(mLeft(tbplanilha.Rows(x)("exame").ToString, 150)) & "'," & moeda(tbplanilha.Rows(x)("final").ToString) & "," & moeda(tbplanilha.Rows(x)("custo").ToString) & ", '" & hoje() & "', 'Importação Planilha', '" & tbplanilha.Rows(x)("codinterno").ToString & "', true, " & Session("idempresaemuso") & ")")
        '    End If
        '    tb1 = tab1.IncluirAlterarDados("update tbprocedimentos_planilhas set linhaatual = " & x + 1 & ", linhas = " & tbplanilha.Rows.Count & "  where  nrseq = " & nrseqarquivo)
        'Next
    End Function
    Private Sub btnprocessararq_Click(sender As Object, e As EventArgs) Handles btnprocessararq.Click
        hdconexao.Value = Session("conexao")
        Dim thprocessar As New Threading.Thread(AddressOf processar)

        thprocessar.Start()


    End Sub

    Public Sub procuraconvenio()
        hdconvenio.Value = 0
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        tb1 = tab1.conectar("select distinct nomefantasia, nrseq from tbconvenios where nrseqempresa = '" & Session("idempresaemuso") & "' and nomefantasia = '" & cboconvenio.SelectedItem.Text & "' order by nomefantasia")
        If tbprocedimentos.Rows.Count > 0 Then
            hdconvenio.Value = tb1.Rows(0)("nrseq").ToString
        End If
        listararquivos()
    End Sub

    Private Sub gradearquivos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradearquivos.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If

        e.Row.Cells(2).Text = FormatDateTime(e.Row.Cells(2).Text, DateFormat.ShortDate)
    End Sub

    Private Sub btnatualizar_Click(sender As Object, e As EventArgs) Handles btnatualizar.Click
        listararquivos()
    End Sub

    Private Sub btnimprimirtodosproc_Click(sender As Object, e As EventArgs) Handles btnimprimirtodosproc.Click
        Dim listanrseq As String = ""
        For Each gvr As GridViewRow In gradeprocedimentos.Rows
            If gvr.RowType = ListItemType.Item OrElse gvr.RowType = ListItemType.AlternatingItem Then
                Dim hdnnrseq As HiddenField = DirectCast(gvr.FindControl("hdnnrseq"), HiddenField)
                If hdnnrseq.Value <> "" OrElse hdnnrseq.Value IsNot Nothing Then
                    listanrseq &= hdnnrseq.Value + ","
                End If
            End If

        Next
        listanrseq = listanrseq.Remove(listanrseq.Length - 1)

        If listanrseq <> "" Then
            Response.Redirect("relatorios/imprimir_procedimentos.aspx?i=" & listanrseq)
        End If

    End Sub

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click
        limpar(True)
        bloqueio()
    End Sub

    Private Sub limpar(Optional todos As Boolean = False)
        txtdescricao.Text = ""
        txtvalorcusto.Text = ""
        txtvalorfinal.Text = ""
        txtcodamb.Text = ""
        txtcodtuss.Text = ""
        txt_percente_cob_plano.Text = ""
        txt_valor_cob_plano.Text = ""
        txt_percente_paciente.Text = ""
        txt_valor_paciente.Text = ""
        If cboconvenio.SelectedIndex > 0 Then
            cboconvenio.SelectedIndex = 0
        End If
        If cboplano.SelectedIndex > 0 Then
            cboplano.SelectedIndex = 0
        End If

    End Sub

    Private Sub gradearquivos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradearquivos.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradearquivos.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("lblnrseq")
        If e.CommandName.ToLower = "excluir" Then
            tb1 = tab1.IncluirAlterarDados("update tbprocedimentos_planilhas set ativo=False where nrseq = " & nrseq.Value)
            listararquivos()
        End If
        If e.CommandName.ToLower = "naoprocessar" Then
            tb1 = tab1.IncluirAlterarDados("update tbprocedimentos_planilhas set processado=False where nrseq = " & nrseq.Value)
            listararquivos()
        End If
    End Sub
End Class

Public Class Valores

    Public nrseq As String
    Public valorfinal As String
    Public porcentagempaciente As String
    Public valorcusto As String

    Public Sub New(nrseq As String, valorfinal As String, porcentagempaciente As String, valorcusto As String)
        Me.nrseq = nrseq
        Me.valorfinal = valorfinal
        Me.porcentagempaciente = porcentagempaciente
        Me.valorcusto = valorcusto
    End Sub


End Class


﻿Imports clssessoes
Imports clsSmart
Imports System.IO


Partial Class aprovar
    Inherits System.Web.UI.Page


    Private Sub aprovar_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'habilitar(False)
        End If

        divmostradados.Visible = False
        carregaparaaprovar()

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub


    Public Sub carregaparaaprovar(Optional exibirinativos As Boolean = False)
        'Dim xempresas As New clsEmpresas
        Dim xaprovar As New clsaprovacao
        Dim consulta As String

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        If chkrefresh.Checked = True Then
            consulta = "where rejeitado = '1' "
        Else
            consulta = "where rejeitado = '0' and aprovado = '0'"
        End If

        tb1 = tab1.conectar("select * from tbaprovacao " & consulta & "")

        'xplanos.Cliente = xempresas.Nome

        '   If Not xaprovar.carregatodos() Then
        '   sm("swal({title 'Error!',text: '" & xaprovar.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '   Exit Sub
        '   End If


        gradeaprovar.DataSource = tb1
        gradeaprovar.DataBind()

    End Sub




    Private Sub gradeaprovar_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeaprovar.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeaprovar.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "editar" Then

            '    carregarNoFormulario(nrseq.Value)
            '   habilitar(True)
            '  txtidentificador.Enabled = False

            Dim xAprovar As New clsaprovacao

            xAprovar.Nrseq = nrseq.Value



            If Not xAprovar.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAprovar.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            divmostradados.Visible = True

            ' txtnrseq.Text = xAssociados.Nrseq
            txtmatricula.Text = xAprovar.Matricula
            txtnome.Text = xAprovar.Nome
            txtnomemae.Text = xAprovar.Nomemae
            txtsituacaopb.Text = xAprovar.Situacaopb
            txtsexo.Text = xAprovar.Sexo
            txtdtnasc.Text = formatadatamysql(xAprovar.Dtnasc, True, True)
            txtcpf.Text = xAprovar.Cpf
            txtrg.Text = xAprovar.Rg
            txtcep.Text = xAprovar.Cep
            txtendereco.Text = xAprovar.Endereco
            txtnumero.Text = xAprovar.Numero
            txtcomplemento.Text = xAprovar.Complemento
            txtbairro.Text = xAprovar.Bairro
            txtcidade.Text = xAprovar.Cidade
            txtestado.Text = xAprovar.Estado
            txttelddd.Text = xAprovar.Telddd
            txttelefone.Text = xAprovar.Telefone
            txtemail.Text = xAprovar.Email
            txtdtposse.Text = formatadatamysql(xAprovar.Dtposse, True, True)
            txtdtaposentadoria.Text = formatadatamysql(xAprovar.Dtaposentadoria, True, True)
            txtsenha.Text = xAprovar.Senha
            txtAgencia.Text = xAprovar.Agencia
            txtcontacorrente.Text = xAprovar.Contacorrente

            ' dados historico

            '  txtdatainclusao.Text = formatadatamysql(xAprovar.Dtcad, True, True)
            ' txtcadastradopor.Text = xAprovar.Usercad



            carregaparaaprovar()



        End If

        If e.CommandName = "deletar" Then
            '           Dim xplanos As New clsPlanos
            '
            '          xplanos.Nrseq = nrseq.Value

            'If Not xplanos.excluir() Then
            'sm("swal({title: 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            'Exit Sub
            ' End If

            carregaparaaprovar()

        End If
    End Sub







End Class

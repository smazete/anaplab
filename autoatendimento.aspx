﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="autoatendimento.aspx.vb"  MasterPageFile="~/MasterPage3.master" Inherits="autoatendimento" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content" style="height: auto !important; min-height: 0px !important;">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-1 col-xs-12" id="divajuda" runat="server">      
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label Text="0" runat="server" id="txtcaixapostal"/></h3>

              <p>CAIXA POSTAL</p>
            </div>
            <div class="icon">
              <i <span class="iconify" data-icon="carbon-email-new" data-inline="false"></span>></i>
            </div>
            <a href="caixapostal.aspx" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label Text="0" runat="server" id="txtcadastro"/><sup style="font-size: 20px">%</sup></h3>

              <p>MEU CADASTRO</p>
            </div>
            <div class="icon">
              <i class="iconify" data-icon="bx:bxs-user-detail" data-inline="false"></i>
            </div>
            <a href="meucadastro.aspx" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label Text="0" runat="server" id="txtdocumentos"/></h3>

              <p>MEUS DOCUMENTOS</p>
            </div>
            <div class="icon">
              <i class="iconify" data-icon="mdi-light:folder-multiple" data-inline="false""></i>
            </div>
            <a href="meusdocumentos.aspx" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label Text="0" runat="server" id="txtacoes"/></h3>

              <p>MINHAS AÇÕES</p>
            </div>
            <div class="icon">
              <i class="iconify" data-icon="mdi-light:spellcheck" data-inline="false""></i>
            </div>
            <a href="minhasacoes.aspx" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
            <!-- ./col --> 
          <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label Text="0" runat="server" id="txtmensalidades"/></h3>

              <p>MENSALIDADES</p>
            </div>
            <div class="icon">
              <i  class="iconify" data-icon="fa-regular:money-bill-alt" data-inline="false"></i>
            </div>
            <a href="minhasmensalidades.aspx" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
            <div id="divjoiasmenu" runat="server" visible="false">
            <div class="col-lg-2 col-xs-6" >
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label Text="0" runat="server" id="txtjoias"/></h3>

              <p>JOIAS</p>
            </div>
            <div class="icon">
              <i  class="iconify" data-icon="mdi:diamond-stone" data-inline="false"></i>
            </div>
            <a href="minhasjoias.aspx" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
        
        </section>


</asp:Content>
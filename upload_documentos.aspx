﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="upload_documentos.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="upload_documentos" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Arquivo Balanço / Balancete </b>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-2">
                    Nome
                    <br>
                    <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Arquivo
                    <br>
                    <asp:TextBox ID="txtarquivo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Ativo
                    <br>
                    <asp:TextBox ID="txtativo" runat="server" Text="ATIVO" CssClass="form-control" Enabled="false"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Tipo
                    <br>
                    <asp:DropDownList ID="cbotipo" runat="server" CssClass="form-control">
                        <asp:ListItem Text="Alvará" />
                        <asp:ListItem Text="Balancete" />
                        <asp:ListItem Text="Balanço" />
                        <asp:ListItem Text="Outros" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-8 ">
                    Obs
                    <br>
                    <asp:TextBox ID="txtobs" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="col-lg-3 ">
                    <asp:FileUpload ID="upload01" runat="server" />
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 text-center">
                    <br>
                    <asp:LinkButton ID="btnupload" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i>Salvar</asp:LinkButton>
                </div>
            </div>
            <div class="row">
                </hr>
            </div>
            <div class="row">
                <br>
                <asp:GridView ID="grade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma doca cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="nrseq" HeaderText="Código" />
                        <asp:BoundField DataField="tipo" HeaderText="Tipo" />
                        <asp:BoundField DataField="nome" HeaderText="Nome" />
                        <asp:BoundField DataField="arquivo" HeaderText="Arquivo" />
                        <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="excluir" runat="server" CssClass="btn btn-danger btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                <i class="fa fa-trash-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
</asp:Content>

﻿Imports clsSmart


Partial Class acoes
    Inherits System.Web.UI.Page

    Private Sub acoes_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
        End If

        habilitar(False)
        carregagradeacoes()

    End Sub


    Private Sub limpar()
        txtnrseq.Text = ""
        txtnome.Text = ""
        txtnumero.Text = ""
        ' cbojustica.SelectedValue = ""
        ' txtconteudo.Text = ""
        txtdata.Text = ""
        ' txtusercad.Text = ""
        ' txtativo_old.text = ""
        ' txtnrseqctrl.text = ""
        txtlink.Text = ""
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub habilitar(valor As Boolean)
        txtnrseq.Enabled = valor
        txtnome.Enabled = valor
        txtnumero.Enabled = valor
        cbojustica.Enabled = valor
        ' txtconteudo.Enabled = valor
        txtdata.Enabled = valor
        ' txtusercad.Enabled = valor
        ' txtativo_old.enabled = valor
        ' chkativo.Enabled = valor
        ' txtnrseqctrl.enabled = valor
        txtlink.Enabled = valor
    End Sub

    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click
        Dim informar As String
        informar = ""
        Dim xAcoes As New clsacoes

        If txtnome.Text = "" Then
            informar = informar + " [Nome] "
        End If

        If txtnumero.Text = "" Then
            informar = informar + " [Numero] "
        End If

        If txtlink.Text = "" Then
            informar = informar + " [link] "
        End If
        If cbojustica.Text = "" Then
            informar = informar + " [Justiça] "
        End If
        If txtdata.Text = "" Then
            informar = informar + " [Data] "
        End If

        If informar <> "" Then
            sm("Swal.fire({   type: 'warning',  title: 'Preencha os campos " & informar & "',  confirmButtonText: 'OK'})")
            btnnovo.Visible = False
            habilitar(True)
            Exit Sub
        End If

        xAcoes.Nrseq = hdnrseq.Value
        xAcoes.Nome = txtnome.Text
        xAcoes.Numero = txtnumero.Text
        xAcoes.Justica = cbojustica.Text
        xAcoes.Data = txtdata.Text
        ' xAcoes.Ativo = chkativo.Checked
        xAcoes.Link = txtlink.Text

        If Not xAcoes.salvar Then
            sm("Swal.fire({ type: 'error',  title: 'Não foi possivel salvar esta ação preencha os campos assinalados',  confirmButtonText: 'OK'})")
            Exit Sub
        End If

        sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Ação salva com sucesso',  showConfirmButton: false,  timer: 2000})")

        limpar()
        habilitar(False)
        btnnovo.Visible = True
        carregagradeacoes()

    End Sub

    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click

        Dim xAcoes As New clsacoes
        If Not xAcoes.novo Then
            sm("swal({title: 'Atenção!',text: '" & xAcoes.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        txtnrseq.Text = xAcoes.Nrseq
        hdnrseq.Value = xAcoes.Nrseq


        habilitar(True)
        txtnrseq.Enabled = False
        btnnovo.Visible = False

    End Sub

    Public Sub carregagradeacoes(Optional exibirinativos As Boolean = False)
        Dim consulta As String

        consulta = ""

        If txtbusca.Text = "" Then
            consulta = " where ativo = 1"
        Else
            consulta = "where " & cbobusca.SelectedValue & " = '" & txtbusca.Text & "'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbacoes " & consulta & "")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        gradeacoes.DataSource = tb1
        gradeacoes.DataBind()

    End Sub



    Private Sub gradeacoes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeacoes.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeacoes.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "selecionar" Then
            habilitar(False)
            Dim xAcoes As New clsacoes
            xAcoes.Nrseq = nrseq.Value
            Dim xAcoesDetalhes As New clsacoesdth

            xAcoesDetalhes.Nrseqacoes = nrseq.Value

            If Not xAcoesDetalhes.participantescount() Then
                txtqtd.Text = "0"
            End If

            txtqtd.Text = xAcoesDetalhes.Participantes

            If Not xAcoes.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAcoes.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            hdnrseq.Value = xAcoes.Nrseq
            txtnrseq.Text = xAcoes.Nrseq
            txtnome.Text = xAcoes.Nome
            txtnumero.Text = xAcoes.Numero
            txtlink.Text = xAcoes.Link
            txtdata.Text = xAcoes.Data
            txtdata.Text = xAcoes.Finalizada_descrifim

            If xAcoes.Justica = "" Then
                cbojustica.Text = "JUSTIÇA FEDERAL"
            Else
                cbojustica.Text = xAcoes.Justica
            End If



            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Ação Carregada com Sucesso',  showConfirmButton: false,  timer: 2000})")

            carregagradeacoes()


        End If
        If e.CommandName = "editar" Then
            '    carregarNoFormulario(nrseq.Value)
            habilitar(True)
            '  txtidentificador.Enabled = False

            Dim xAcoes As New clsacoes

            xAcoes.Nrseq = nrseq.Value

            Dim xAcoesDetalhes As New clsacoesdth

            xAcoesDetalhes.Nrseqacoes = nrseq.Value

            If Not xAcoesDetalhes.participantescount() Then

            End If

            txtqtd.Text = xAcoesDetalhes.Participantes

            If Not xAcoes.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAcoes.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            hdnrseq.Value = xAcoes.Nrseq
            txtnrseq.Text = xAcoes.Nrseq
            txtnome.Text = xAcoes.Nome
            txtnumero.Text = xAcoes.Numero
            txtlink.Text = xAcoes.Link

            If xAcoes.Justica = "" Then
                cbojustica.Text = "JUSTIÇA FEDERAL"
            Else
                cbojustica.Text = xAcoes.Justica
            End If

            txtdata.Text = formatadatamysql(xAcoes.Data, True, True)


            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Ação Carregada com Sucesso',  showConfirmButton: false,  timer: 2000})")

            carregagradeacoes()


        End If



        If e.CommandName = "ativar" Then
            Dim xAcoes As New clsacoes

            xAcoes.Nrseq = nrseq.Value

            If Not xAcoes.excluir() Then

                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Ação Ativada',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregagradeacoes()
        End If

        If e.CommandName = "cancelar" Then
            Dim xAcoes As New clsacoes

            xAcoes.Nrseq = nrseq.Value

            If Not xAcoes.excluir() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Ação cancelada com sucesso',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregagradeacoes()
        End If

        If e.CommandName = "nfim" Then
            Dim xAcoes As New clsacoes

            xAcoes.Nrseq = nrseq.Value

            If Not xAcoes.nfim() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel recuperar esta ação',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregagradeacoes()
        End If
        If e.CommandName = "encerrar" Then
            Dim xAcoes As New clsacoes

            xAcoes.Nrseq = nrseq.Value

            If Not xAcoes.encerrar() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel recuperar esta ação',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregagradeacoes()
        End If
    End Sub

    Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click
        carregagradeacoes()
    End Sub

    Private Sub gradeacoes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeacoes.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        Dim ativar As HtmlGenericControl = e.Row.FindControl("divativar")
        Dim fim As HtmlGenericControl = e.Row.FindControl("divfim")
        Dim cancelar As HtmlGenericControl = e.Row.FindControl("divcancelado")
        Dim cancelar2 As HtmlGenericControl = e.Row.FindControl("divcancelar2")
        Dim selecionar As HtmlGenericControl = e.Row.FindControl("divselecionafim")


        If e.Row.Cells(6).Text = "1" Then
            e.Row.Cells(6).Text = "ATIVO"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(6).Text = "0" Then
            e.Row.Cells(6).Text = "CANCELADO"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray

        End If

        If e.Row.Cells(7).Text = "1" Then
            e.Row.Cells(7).Text = "OK"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.Green
        End If



        If e.Row.Cells(7).Text = "OK" Or e.Row.Cells(7).Text = "1" Then
            ativar.Visible = False
            cancelar.Visible = False
            cancelar2.Visible = False
            selecionar.Visible = True

        ElseIf e.Row.Cells(6).Text = "ATIVO" Then
            ativar.Visible = True
            cancelar.Visible = False
            cancelar2.Visible = True
            selecionar.Visible = False

        ElseIf e.Row.Cells(6).Text = "CANCELADO" Then

            ativar.Visible = False
            cancelar.Visible = True
            cancelar2.Visible = False
            selecionar.Visible = False
        End If


    End Sub
End Class
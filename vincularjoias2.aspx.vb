﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports clssessoes
Imports clsSmart


Partial Class vincularjoias2
    Inherits System.Web.UI.Page

    Private Sub vincularjoias2_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack Then
            Exit Sub
        End If


    End Sub



    Public Sub carregajoias(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.nrsequser = '" & hdnruser.Value & "' and tbjoias_2.ativo = true order by tbjoias_2.ano, tbjoias_2.mes asc  ")

        gradejoias.DataSource = tb1
        gradejoias.DataBind()

    End Sub

    Public Sub carregaassociados(Optional exibirinativos As Boolean = False)
        'Dim xempresas As New clsEmpresas
        Dim xAssociados As New clsAssociado
        Dim consulta As String

        consulta = ""

        If txtbusca.Text = "" Then
            consulta = ""
            sm("swal({title: 'Error!',text: 'Essa Busca pode demorar',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        Else
            consulta = "where " & cbobusca.SelectedValue & " like '%" & txtbusca.Text & "%'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbAssociados " & consulta & "")

        gradeassociados.DataSource = tb1
        gradeassociados.DataBind()

    End Sub


    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub



    Private Sub btnbuscarassociado_Click(sender As Object, e As EventArgs) Handles btnbuscarassociado.Click

        divgradeassociados.Visible = True
        divjoiascliente.Visible = False
        carregaassociados()
        divgradeassociados.Visible = True
        divjoiascliente.Visible = False

    End Sub



    Private Sub gradeassociados_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeassociados.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeassociados.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "usar" Then

            Dim xAssociados As New clsAssociado


            xAssociados.Nrseq = nrseq.Value


            If Not xAssociados.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If


            hdnruser.Value = xAssociados.Nrseq
            lblcodigo.Text = xAssociados.Nrseq
            lblmatricula.Text = xAssociados.Matricula
            lblnome.Text = xAssociados.Nome
            lblcpf.Text = xAssociados.Cpf

            carregajoias()

            divgerarjoias.Visible = True
            divassociado.Visible = True
            divgradeassociados.Visible = False
            divjoiascliente.Visible = True

            ' botoes parte clliente solicitado
            btngerajoias.Enabled = True

        End If
    End Sub

    Private Sub btngerajoias_Click(sender As Object, e As EventArgs) Handles btngerajoias.Click

        If cbomes.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Mês}',  showConfirmButton: true})")
            cbomes.Focus()
            Exit Sub
        End If
        If txtano.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Ano}',  showConfirmButton: true})")
            txtano.Focus()
            Exit Sub
        End If

        If txtparcela.Text = "" Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Por favor preencha o campo {Parcelas}',  showConfirmButton: true})")
            txtparcela.Focus()
            Exit Sub
        End If
        Dim xjoia2 As New clsjoia2

        xjoia2.Nrsequser = hdnruser.Value
        xjoia2.Mes = cbomes.SelectedValue
        xjoia2.Ano = txtano.Text
        xjoia2.Parcelas = txtparcela.Text

        If Not xjoia2.gerarlote() Then
            sm("swal({title: 'Error!',text: 'Erro ao tentar gerar as Joias 2',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        carregajoias()

        sm("Swal.fire({  position: 'center',  type: 'success',  title: 'Associado vinculado a joias 2.',  showConfirmButton: true})")

    End Sub


    Private Sub gradejoias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradejoias.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(4).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightSalmon
        ElseIf e.Row.Cells(4).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If

        If e.Row.Cells(5).Text = "1" Then
            e.Row.Cells(5).Text = "Janeiro"
        ElseIf e.Row.Cells(5).Text = "2" Then
            e.Row.Cells(5).Text = "Fevereiro"
        ElseIf e.Row.Cells(5).Text = "3" Then
            e.Row.Cells(5).Text = "Março"
        ElseIf e.Row.Cells(5).Text = "4" Then
            e.Row.Cells(5).Text = "Abril"
        ElseIf e.Row.Cells(5).Text = "5" Then
            e.Row.Cells(5).Text = "Maio"
        ElseIf e.Row.Cells(5).Text = "6" Then
            e.Row.Cells(5).Text = "Junho"
        ElseIf e.Row.Cells(5).Text = "7" Then
            e.Row.Cells(5).Text = "Julho"
        ElseIf e.Row.Cells(5).Text = "8" Then
            e.Row.Cells(5).Text = "Agosto"
        ElseIf e.Row.Cells(5).Text = "9" Then
            e.Row.Cells(5).Text = "Setembro"
        ElseIf e.Row.Cells(5).Text = "10" Then
            e.Row.Cells(5).Text = "Outubro"
        ElseIf e.Row.Cells(5).Text = "11" Then
            e.Row.Cells(5).Text = "Novembro"
        ElseIf e.Row.Cells(5).Text = "12" Then
            e.Row.Cells(5).Text = "Dezembro"
        End If

        If e.Row.Cells(4).Text = "0" Then
            e.Row.Cells(4).Text = "Aberto"
        ElseIf e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Pago"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Estornado"
        Else
            e.Row.Cells(4).Text = "Aberto"
        End If

        If e.Row.Cells(2).Text = "0" Then
            e.Row.Cells(2).Text = "Excluido"
            e.Row.Cells(4).Text = "Excluido"
        ElseIf e.Row.Cells(2).Text = "3" Then
            e.Row.Cells(2).Text = "Ativo"
        End If


        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If



    End Sub

    Private Sub btntodos_Click(sender As Object, e As EventArgs) Handles btntodos.Click

        Dim xjoia2 As New clsjoia2

        xjoia2.Nrsequser = hdnruser.Value

        If Not xjoia2.excluir() Then
            sm("Swal.fire({  position: 'center',  type: 'error',  title: 'Não foi possivel desvincular o associado!',  showConfirmButton: true})")

            Exit Sub
        End If

        carregajoias()
        sm("Swal.fire({  position: 'center',  type: 'success',  title: 'Associado desvinculado com sucesso!',  showConfirmButton: true})")

    End Sub

    Private Sub chkciente_CheckedChanged(sender As Object, e As EventArgs) Handles chkciente.CheckedChanged
        If chkciente.Checked = False Then
            btntodos.Enabled = False
        Else
            btntodos.Enabled = True
            sm("Swal.fire({  position: 'center',  type: 'warning',  title: 'Esta ciente que excluindo voce pode afetar os valores e ira retirar este associado do vinculo a {Joias 2]',  showConfirmButton: true})")
            Exit Sub
        End If
    End Sub
End Class

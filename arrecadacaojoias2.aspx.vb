﻿Imports clssessoes
Imports clsSmart
Imports System.Web.UI.DataVisualization.Charting

Partial Class arrecadacaojoias2
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    Private Sub arrecadacaojoias_Load(sender As Object, e As EventArgs) Handles Me.Load

        'cbomes.SelectedValue = Date.Now.Month
        'txtbuscajoias.Text = DateTime.Now.ToString("yyyy")

        carregaarrecadaçãojoias()
        carregaextratoconsolidado()
    End Sub

    Public Sub carregaextratoconsolidado(Optional exibirinativos As Boolean = False)
        Dim joiasmes As String = cbomes.SelectedValue
        Dim joiasano As String = txtbuscajoias.Text

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.statuspg =  1 and tbAssociados.ativo = 1 ")

        lblpagamentosextrato.Text = tb1.Rows.Count.ToString()

        lbltotalgeral.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 200)


        ' lblanaplab.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 20)
        ' lblstamato.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 50)
        '  lblnelson.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 30)
        lblanaplab.Text = FormatCurrency(numeros(lbltotalgeral.Text) * (20 / 100))
        lblstamato.Text = FormatCurrency(numeros(lbltotalgeral.Text) * (50 / 100))
        lblnelson.Text = FormatCurrency(numeros(lbltotalgeral.Text) * (30 / 100))


        '   lblcomissao.Text = FormatCurrency(CDbl(lblanaplab.Text) + CDbl(lblstamato.Text) + CDbl(lblnelson.Text))
        lblgeralarrecadado.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 200)

    End Sub

    Public Sub carregaarrecadaçãojoias(Optional exibirinativos As Boolean = False)
        Dim joiasmes As String = cbomes.SelectedValue
        Dim joiasano As String = txtbuscajoias.Text

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        'tb1 = tab1.conectar("select * from tbjoias_2 where ativo = '1'")

        'lblparticipantesgerais.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.ano =  '" & joiasano & "' and tbjoias_2.mes = '" & joiasmes & "'  and tbAssociados.ativo = 1 ")

        lblparticipantes.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.ano =  '" & joiasano & "' and tbjoias_2.mes = '" & joiasmes & "'  and tbjoias_2.statuspg = 1 and tbAssociados.ativo = 1 ")

        lblarrecadajoias.Text = tb1.Rows.Count.ToString()


        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.ano =  '" & joiasano & "' and tbjoias_2.mes = '" & joiasmes & "'  and tbjoias_2.statuspg = 3 and tbAssociados.ativo = 1 ")

        lblarrecadaisentos.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.ano =  '" & joiasano & "' and tbjoias_2.mes = '" & joiasmes & "'  and tbjoias_2.statuspg = 0 and tbAssociados.ativo = 1 ")

        lblinadimplentes.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.ano =  '" & joiasano & "' and tbjoias_2.mes = '" & joiasmes & "'  and tbjoias_2.statuspg = 1 and tbAssociados.ativo = 0 ")

        lbldesfiliadosadiplentes.Text = tb1.Rows.Count.ToString()


        lblvalorfinal.Text = FormatCurrency(numeros(lblarrecadajoias.Text) * 200)

        'lblvalorfinal.Text = lblarrecadajoias.Text * 100


    End Sub


    Private Sub btnbuscandojoias_Click(sender As Object, e As EventArgs) Handles btnbuscandojoias.Click

        carregaarrecadaçãojoias()
    End Sub


End Class

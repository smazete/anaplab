﻿Imports clsSmart
Imports clssessoes
' *******************************************
' Desenvolvido por Claudio Smart
' Data:  09/09/2019
' Atualizado em: 09/09/2019
Partial Class restrito_perfil
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tberro As New Data.DataTable


    Private Sub restrito_perfil_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub

        divsenhas.Style.Add("display", "none")
        Dim tbfun As New Data.DataTable
        Dim tabfun As New clsBanco





        Dim xuser As New clsusuarios

        xuser.Nrseq = Session("idusuario")

        If Not xuser.procurausuario Then
            sm("swal({title: 'Atenção!',text: '" & xuser.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        If System.IO.File.Exists(Request.MapPath("~/social/" & xuser.Imagemperfil)) Then
            imgperfil.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & xuser.Imagemperfil)
        Else
            imgperfil.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/img/semimagem01.png")
        End If
        With xuser

            txtnome.Text = .Colaboradores(0).Nome
            txttel1.Text = .Colaboradores(0).Telefone
            txttel2.Text = .Colaboradores(0).Celular
            txtusuario.Text = .Usuario
            txtemail.Text = .Colaboradores(0).Email
            txtBairro.Text = .Colaboradores(0).Bairro
            txtcep.Text = .Colaboradores(0).Cep
            txtcidade.Text = .Colaboradores(0).Cidade
            txtCodigo.Text = .Nrseq
            txtdtcad.Text = FormatDateTime(.Dtcad, DateFormat.ShortDate)
            lblgrupo.Text = .Permissao
            cbopais.Text = "Brasil"
            txtestado.Text = .Colaboradores(0).Uf
            lblpermissao.Text = .Permissao
            lblmaster.Text = IIf(.Master, "Master", "Normal")
        End With

        'tb1 = tab1.conectar("select * from vwusuarios where ativo = true and nrseq = " & Session("idusuario"))


        'txtCodigo.Text = tb1.Rows(0)("nrseq").ToString
        'txtemail.Text = tb1.Rows(0)("email").ToString
        'txtusuario.Text = tb1.Rows(0)("usuario").ToString
        'lblusuario.Text = Session("usuario")
        'txtnome.Text = tb1.Rows(0)("nome").ToString
        'txtdtcad.Text = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)

        'txtsenha.Text = tb1.Rows(0)("senha").ToString
        '' lblagente.Text = tb1.Rows(0)("agente").ToString
        'lblgrupo.Text = tb1.Rows(0)("grupo").ToString
        '' lblfilial.Text = tb1.Rows(0)("filial").ToString
        'cbopais.Text = tb1.Rows(0)("pais").ToString
        'lblpermissao.Text = tb1.Rows(0)("permissao").ToString
        'txtendereco.Text = tb1.Rows(0)("endereco").ToString
        'txtBairro.Text = tb1.Rows(0)("bairro").ToString
        'txtcidade.Text = tb1.Rows(0)("cidade").ToString
        'txtestado.Text = tb1.Rows(0)("uf").ToString
        'txtcep.Text = tb1.Rows(0)("cep").ToString
        'txtcpf.Text = tb1.Rows(0)("cpf").ToString
        'txtrg.Text = tb1.Rows(0)("rg").ToString
        'txttel1.Text = tb1.Rows(0)("tel1").ToString
        'txttel2.Text = tb1.Rows(0)("tel2").ToString

        verlogs()

    End Sub

    Private Sub verlogs(Optional vertudo As Boolean = False
                        )
        tb1 = tab1.conectar("select * from tblog where nrsequsuario = " & Session("idusuario") & " order by nrseq desc " & IIf(vertudo, "", "limit 10"))
        If vertudo Then
            btnvertudo.Text = "View last 10 logs"
        Else
            btnvertudo.Text = "View All Logs"
        End If
        rpttimeline.DataSource = tb1
        rpttimeline.DataBind()
        lblhoje.Text = data(True)
    End Sub

    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click

        Dim xuser As New clsusuarios
        Dim xcolabora As New clscolaboradores
        xcolabora.Procurarpor = "nrsequsuario"
        xcolabora.Nome = txtnome.Text
        xcolabora.Bairro = txtBairro.Text
        xcolabora.Cidade = txtcidade.Text
        xcolabora.Uf = txtestado.Text
        xcolabora.Email = txtemail.Text
        xcolabora.Endereco = txtendereco.Text
        xcolabora.Telefone = txttel1.Text
        xcolabora.Celular = txttel2.Text
        xcolabora.Cep = txtcep.Text
        xcolabora.Gravarusuario = False
        'xcolabora.Complemento 
        'xcolabora.Horista 

        If xcolabora.Nrseq <> 0 Then
            xcolabora.Salvar()
        End If

        xuser.Nrseq = txtCodigo.Text


        xuser.Usuario = txtusuario.Text
        xuser.Senha = ""

        xuser.Alterado = True

        xuser.Ativo = True

        xuser.Email = txtemail.Text

        xuser.Master = IIf(lblmaster.Text = "Master", True, False)

        If Not xuser.alterarusuario Then
            sm("swal({title: 'Atenção!',text: '" & xuser.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        Else
            sm("swal({title: 'Legal!',text: 'Seus dados foram atualizados com sucesso !',type: 'success',confirmButtonText: 'OK'})", "swal")
        End If

    End Sub

    Private Sub rpttimeline_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rpttimeline.ItemDataBound
        If e.Item.ItemIndex < 0 Then
            Exit Sub
        End If
        Dim lbldtcadtime As Label = e.Item.FindControl("lbldtcadtime")
        Dim imgteste As Image = e.Item.FindControl("imgteste")

        'imgteste.Attributes.Remove("class")
        'imgteste.Attributes.Add("class", retornaimagemlog(e.Item.DataItem("tipodelog")))
        lbldtcadtime.Text = formatadatamysql(e.Item.DataItem("dtcad").ToString, True)
    End Sub

    Private Sub btnvertudo_Click(sender As Object, e As EventArgs) Handles btnvertudo.Click
        If btnvertudo.Text = "View All Logs" Then
            verlogs(True)
        Else
            verlogs(False)
        End If

    End Sub

    Private Sub rptresumobids_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptresumobids.ItemDataBound
        If e.Item.ItemIndex < 0 Then Exit Sub

        Dim divbar01 As Object = e.Item.FindControl("divbar01")
        Dim lbltituloresumo As Label = e.Item.FindControl("lbltituloresumo")
        Dim lblvalorresumo As Label = e.Item.FindControl("lblvalorresumo")
        Dim hdpercresumo As HiddenField = e.Item.FindControl("hdpercresumo")

        divbar01.style.add("width", moeda(FormatNumber(hdpercresumo.Value, 0) & "%", True))


        'divbar01.Style.Add("width", "50%")
        ''aria-valuenow = "20" = "0" aria-valuemax="100"
        'lblexpirados.Text = 10

    End Sub

    Private Sub imgperfil_Click(sender As Object, e As ImageClickEventArgs) Handles imgperfil.Click
        Session("chave") = txtCodigo.Text
        Session("origem") = "usuarios"
        Response.Redirect("/loadimage.aspx")
    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub txtcep_TextChanged(sender As Object, e As EventArgs) Handles txtcep.TextChanged
        Dim xcep As New clsCEP

        xcep.Cep = txtcep.Text

        If Not xcep.buscarcep Then
            sm("swal({title: 'Atenção!',text: '" & xcep.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        txtendereco.Text = xcep.Endereco
        txtBairro.Text = xcep.Bairro
        txtcidade.Text = xcep.Cidade
        txtestado.Text = xcep.Uf
        cbopais.Text = xcep.Pais
    End Sub

    Private Sub btnconfirmarnovasenha_Click(sender As Object, e As EventArgs) Handles btnconfirmarnovasenha.Click
        versessao()

        Dim erro As Boolean = False
        Dim tblista As New Data.DataTable
        Dim tablista As New clsBanco
        Dim senhaantiga As String = ""
        Dim condchrespeciais As Boolean, condletramaius As Boolean, condnumeros As Boolean, condletrasminus As Boolean
        tblista = tablista.conectar("Select * from tbusuarios  where nrseq = " & Session("idusuario"))
        If tblista.Rows.Count > 0 Then
            senhaantiga = tblista.Rows(0)("senha").ToString
        End If
        If txtSenhaAntiga.Text.ToLower = senhaantiga.ToLower Then
            imgSenhaAntiga.ImageUrl = ("~\img\btn_gravar.png")
        Else
            imgSenhaAntiga.ImageUrl = ("~\img\button_cancel.png")
            erro = True
        End If
        If txtConfirmar.Text <> txtSenhaNova.Text Then
            erro = True
            imgSenhasConferem.ImageUrl = ("~\img\button_cancel.png")
        Else
            imgSenhasConferem.ImageUrl = ("~\img\btn_gravar.png")
        End If
        condchrespeciais = False
        condletramaius = False
        condnumeros = False
        condletrasminus = False
        For X As Integer = 33 To 47
            If txtConfirmar.Text.Contains(Chr(X)) Then
                condchrespeciais = True
            End If
        Next
        For X As Integer = 58 To 64
            If txtConfirmar.Text.Contains(Chr(X)) Then
                condchrespeciais = True
            End If
        Next
        For X As Integer = 65 To 90
            If txtConfirmar.Text.Contains(Chr(X)) Then
                condletramaius = True
            End If
        Next
        For X As Integer = 48 To 57
            If txtConfirmar.Text.Contains(Chr(X)) Then
                condnumeros = True
            End If
        Next
        For X As Integer = 48 To 57
            If txtConfirmar.Text.Contains(Chr(X)) Then
                condletrasminus = True
            End If
        Next
        If condletrasminus AndAlso condnumeros Then
            imgNumerosLetras.ImageUrl = ("~\img\btn_gravar.png")
        Else
            erro = True
            imgNumerosLetras.ImageUrl = ("~\img\button_cancel.png")

        End If
        If condletramaius Then
            imgLetraMaiuscula.ImageUrl = ("~\img\btn_gravar.png")
        Else
            erro = True
            imgLetraMaiuscula.ImageUrl = ("~\img\button_cancel.png")
        End If
        If condchrespeciais Then
            imgSimbolos.ImageUrl = ("~\img\btn_gravar.png")
        Else
            erro = True
            imgSimbolos.ImageUrl = ("~\img\button_cancel.png")
        End If

        If erro Then
            Exit Sub
        End If




        tblista = tablista.IncluirAlterarDados("update tbusuarios  set senha = '" & txtConfirmar.Text.Trim & "', alterado = true, dtalterado = '" & formatadatamysql(data()) & "', hralterado = '" & hora() & "' where nrseq = " & Session("idusuario"))

        btnalterarsenha.Visible = True
        divsenhas.Style.Add("display", "none")
    End Sub

    Private Sub btnalterarsenha_Click(sender As Object, e As EventArgs) Handles btnalterarsenha.Click
        btnalterarsenha.Visible = False
        divsenhas.Style.Remove("display")

    End Sub

    Private Sub btncancelarsenha_Click(sender As Object, e As EventArgs) Handles btncancelarsenha.Click
        btnalterarsenha.Visible = True
        divsenhas.Style.Add("display", "none")
    End Sub

    'Private Sub listafunc(Optional soativos As Boolean = True)
    '    Dim tbfun As New Data.DataTable
    '    Dim tabfun As New clsBanco
    '    Dim sql As String = "Select * from tbcolaboradores where usuario = '" & Session("usuario") & "'"

    '    tbfun = tabfun.conectar(sql)
    '    txtnomec.Text = tb1.Rows(0)("nome").ToString

    'End Sub



End Class

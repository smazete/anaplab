﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage.master" CodeFile="relatorioprocedimentos.aspx.vb" Inherits="relatorioprocedimentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary">
        <div class="box-header">
            Relatório de Procedimentos
        </div>
        <div class="box-body">
            <asp:UpdatePanel ID="update1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:Label runat="server" for="txtdtinicial">Data Inicial</asp:Label>
                            <asp:TextBox runat="server" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdtinicial" ClientIDMode="Static" />
                        </div>
                        <div class="col-lg-6">
                            <asp:Label runat="server" for="txtdtinicial">Data Final</asp:Label>
                            <asp:TextBox runat="server" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdtfinal" ClientIDMode="Static" />

                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Label runat="server" for="cboPlano">Plano</asp:Label>
                                    <asp:DropDownList ID="cboPlano" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label runat="server" for="cboConvenio">Convênio</asp:Label>
                                    <asp:DropDownList ID="cboConvenio" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label runat="server" for="cboProcedimento">Procedimento</asp:Label>
                                    <asp:DropDownList ID="cboProcedimento" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label runat="server" for="cboSolicitadoPor">Solicitado Por</asp:Label>
                                    <asp:DropDownList ID="cboSolicitadoPor" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">

                            <div class="row">
                                <div class="col-lg-7">
                                    <asp:CheckBox runat="server" ID="chkOcultarValrCobradoDoPaciente" CssClass="checkbox" Text="Ocultar o valor cobrado do paciente" />

                                    <asp:CheckBox runat="server" ID="chkExibirReceita" CssClass="checkbox" Text="Exibir Receita" />

                                    <asp:CheckBox runat="server" ID="chkAgruparPorPaciente" CssClass="checkbox" Text="Agrupar por paciente" />

                                </div>

                                <div class="col-lg-5">
                                    <div class="box box-primary">
                                        <div class="box-header">
                                            Exibir Procedimentos
                                        </div>
                                        <div class="box-body">
                                            <asp:RadioButtonList ID="rblProcedimentos" CssClass="radio-inline" runat="server">
                                                <asp:ListItem Text="Todos" Value="1" Selected="true"></asp:ListItem>
                                                <asp:ListItem Text="Baixados" Value="2" />
                                                <asp:ListItem Text="Em aberto" Value="3" />
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="row text-align-center">
                <asp:UpdatePanel ID="upBtnSalvar" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-12">
                            <asp:Button runat="server" CssClass="btn btn-primary btn-lg" ID="btnImprimir" Text="Imprimir" AutoPostBack="true" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
            </div>
        </div>
    </div>

<%--    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <div id="enviaremail" class="windowstatusmodelo">
                <div class="btfecharholder">
                    <%--<div class="btfechar" onclick="fecharmodalfichaatendimento()">
                    <a href="#" class="fechar" onclick="fecharmodelo()">X</a>
                    <br />
                    <br />
                </div>
                <div>
                    <iframe src="dependentes.aspx" class="frameemail"></iframe>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>--%>


    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.maskMoney.min.js"></script>
    <script type="text/javascript">
        $(function () {
            // Mascara


            $(".moeda").maskMoney()
            $(".moedaGdv").maskMoney({ prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: true, allowZero: true });
            //  $("#txtcep").mask("99999-999");
        });

    </script>
</asp:Content>

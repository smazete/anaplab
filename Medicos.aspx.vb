﻿Imports Newtonsoft.Json


Imports clssessoes
Imports clsSmart
Imports System.IO

Partial Class Medicos
    Inherits System.Web.UI.Page
    Dim xcontrole As New clscontroleagenda
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tabela As String = "tbmedicos"
    Dim xhoras As New clsmedicos("usuario")

    Private Sub Medicos_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If
        divmostradados.Visible = False
        divhorarios.Style.Add("display", "none")
        Session("dadosenviaragenda") = ""
        lblnovomedico.Text = "Cadastro Novo Profissional"
        carregarmedicos()
        buscapermissoes()





    End Sub

    Private Sub carregarmedicos(Optional procura As String = "", Optional inativos As Boolean = False)

        Dim tbprofissionais As New Data.DataTable
        Dim tabprofissionais As New clsBanco
        Dim Qwhere As String
        Dim status As String
        Dim busca As String

        Qwhere = " where ativo = '1'"
        If chkexibirsuspensos.Checked = True Then
            Qwhere = " where ativo = '0'"
        End If
        If chkexibirexcluidos.Checked = True Then

            Qwhere = " where ativo = '0' " + status
        End If

        If txtprocura.Text <> "" Then
            Qwhere = Qwhere + " and " + cbobusca.Text + " like '%" + txtprocura.Text + "%'"
        End If

        busca = Qwhere

        tbprofissionais = tabprofissionais.conectar("Select * FROM tbmedicos " & busca)

        gradeprofissionais.DataSource = tbprofissionais
        gradeprofissionais.DataBind()
    End Sub

    Private Sub limpacampos()

        lblnovomedico.Text = "Cadastro Novo Profissional"
        hidcod.Value = ""
        lblstatusmedico.Text = ""
        txtnome.Text = ""
        txtEndereco.Text = ""
        txtBairro.Text = ""
        txtEstado.Text = ""
        txtCep.Text = ""
        txtcontato.Text = ""
        txtCelular.Text = ""
        txtCidade.Text = ""
        txtCpf.Text = ""
        txtCrm.Text = ""
        txtLogin.Text = ""
        txtEmail.Text = ""
        txtCepcomercial.Text = ""
        txtDtnasc.Text = ""
        txtUfcrm.Text = ""

    End Sub


    Private Sub Gradeprofissionais_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeprofissionais.RowCommand
        Dim existe As Boolean = False

        '  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "escondeexibiralerta2()", "escondeexibiralerta2()", True)

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)

        Dim xMedicos As New clsmedicos
        Dim row As GridViewRow = gradeprofissionais.Rows(index)
        Dim hdnrseq As HiddenField = CType(row.FindControl("hdnrseq"), HiddenField)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Select Case e.CommandName.ToString.ToLower

            Case Is = "editar"
                'mostrardadosmedicoounovo.Visible = False
                divmostradados.Visible = True
                xMedicos.Cod = hdnrseq.Value

                If Not xMedicos.procurar() Then
                    sm("Swal.fire({icon: 'error',title: 'Oops...',text: 'Algo deu errado.',})")
                    Exit Sub
                End If

                hidcod.Value = hdnrseq.Value
                cbominutosagenda.Text = xMedicos.Horasem
                hdnrseq.Value = xMedicos.Cod
                txtnome.Text = xMedicos.Nome
                txtCpf.Text = xMedicos.Cpf
                txtDtnasc.Text = xMedicos.Dtnasc
                txtEmail.Text = xMedicos.Email
                txtCelular.Text = xMedicos.Celular
                txtCep.Text = xMedicos.Cep
                txtEstado.Text = xMedicos.Estado
                txtEndereco.Text = xMedicos.Endereco
                txtBairro.Text = xMedicos.Bairro
                txtCidade.Text = xMedicos.Cidade
                'Area medica dados para informações profissionais 
                txtCrm.Text = xMedicos.Crm
                txtUfcrm.Text = xMedicos.Ufcrm
                txtcontato.Text = xMedicos.Tel1
                txtCepcomercial.Text = xMedicos.Cepcomercial

                'dados para acesso medico 
                txtLogin.Text = xMedicos.Login
                '  txtsenha.Text = xMedicos.Senha
                '  cbopermissoes.Text = xMedicos.Permitir
                '  chkativouser.Text = xMedicos.Ativo
                '  chkexibirsuspensos.Text = xMedicos.blo


                ' txtTel2.Text = tb1.Rows(0)("tel2").ToString
                ' txtSalario.Text = tb1.Rows(0)("salario").ToString
                ' txtDtinclusao.Text = FormatDateTime(tb1.Rows(0)("dtinclusao").ToString, DateFormat.ShortDate)
                ' txtUserinc.Text = tb1.Rows(0)("userinc").ToString
                ' txtHorasem.Text = tb1.Rows(0)("horasem").ToString
                ' txtRg.Text = tb1.Rows(0)("rg").ToString
                ' txtPermitir.Text = tb1.Rows(0)("permitir").ToString
                ' txtCidadecomercial.Text = tb1.Rows(0)("cidadecomercial").ToString
                ' txtEstadocomercial.Text = tb1.Rows(0)("estadocomercial").ToString
                ' txtBairrocomercial.Text = tb1.Rows(0)("bairrocomercial").ToString
                ' txtEnderecocomercial.Text = tb1.Rows(0)("enderecocomercial").ToString
                ' txtVacinas.Text = tb1.Rows(0)("vacinas").ToString

                ' txtPorcclinica.Text = tb1.Rows(0)("porcclinica").ToString
                ' txtPorcprofissional.Text = tb1.Rows(0)("porcprofissional").ToString
                ' txtNometipon.Text = tb1.Rows(0)("nometipon").ToString
                ' txtEnviadosite.Text = tb1.Rows(0)("enviadosite").ToString
                ' txtDtexclui.Text = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
                ' txtUserexclui.Text = tb1.Rows(0)("userexclui").ToString
                ' txtTipon_gerenciapropria.Text = tb1.Rows(0)("tipon_gerenciapropria").ToString
                ' txtTipon_hora.Text = tb1.Rows(0)("tipon_hora").ToString
                ' txtNaoenviartipon.Text = tb1.Rows(0)("naoenviartipon").ToString
                ' txtArqassinatura.Text = tb1.Rows(0)("arqassinatura").ToString
                ' txtImpassinaturaaso.Text = tb1.Rows(0)("impassinaturaaso").ToString
                ' txtUsercad.Text = tb1.Rows(0)("usercad").ToString
                ' txtDtcad.Text = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
                '   txtCbo.Text = tb1.Rows(0)("cbo").ToString
                '   txtIgnorarqtdconsultasplano.Text = tb1.Rows(0)("ignorarqtdconsultasplano").ToString
                '  txtLimitesconsultas.Text = tb1.Rows(0)("limitesconsultas").ToString
                '  txtNrseqctrl.Text = tb1.Rows(0)("nrseqctrl").ToString
                ' txtNrseqempresa.Text = tb1.Rows(0)("nrseqempresa").ToString
                ' txtNumero.Text = tb1.Rows(0)("numero").ToString
                lblstatusmedico.Text = "(Ativo)"
                lblnovomedico.Text = " Cadastro Profissional:" & hdnrseq.Value & " - "


            Case Is = "envioagenda"

                sm("Swal.fire({icon: 'info',title: 'Envio agenda esta em manutençao',text: 'Aguardando novas Atualizações',})")

            Case Is = "horario"

                hdnrseqmedico.Value = hdnrseq.Value
                'carregarhoras()
                Session("telamedicosnrseqmedico") = hdnrseq.Value


                Dim xcalendario As New clsmontarcalendario

                gradehoras.DataSource = xcalendario.Retornarhoras
                gradehoras.DataBind()
                divhorarios.Style.Remove("display")
              '  executarmodal()

             '   sm("Swal.fire({icon: 'info',title: 'Horario Médico esta em manutençao',text: 'Aguardando novas Atualizações',})")

            Case Is = "especialidades"
                '   Response.Redirect("medicoespecialidades.aspx")
                sm("Swal.fire({icon: 'info',title: 'Redirecionamento aguardando finalização da pagina',text: 'Aguardando novas Atualizações',})")
            Case Is = "agenda"

                sm("Swal.fire({icon: 'info',title: 'Agenda esta em manutençao',text: 'Aguardando novas Atualizações',})")

            Case Is = "excluir"

                xMedicos.Cod = hdnrseq.Value

                If Not xMedicos.excluir() Then
                    sm("Swal.fire({icon: 'error',title: '" & xMedicos.Mensagemerro & "', text: 'Algo deu errado.',})")
                    Exit Sub
                End If

                sm("Swal.fire({icon: 'info',title: 'Bloqueio Efetuado',text: ' x ',})")

                carregarmedicos()

            Case Is = "suspenso"
                ' tb1 = tab1.IncluirAlterarDados("update " & tabela & " set suspenso = false, qtdtentativas = 0 where nrseq = " & hdnrseq.Value)

                sm("Swal.fire({icon: 'info',title: 'Suspenso nao habilitado',text: ' Aguardando novas Atualizações ',})")
       '         carregarusuarios()


            Case Is = "reativar"


                sm("Swal.fire({icon: 'info',title: 'M',text: 'Aguardando novas Atualizações',})")
                '    tb1 = tab1.IncluirAlterarDados("update " & tabela & " set suspenso = false, qtdtentativas = 0 where nrseq = " & hdnrseq.Value)
                '         carregarusuarios()

            Case Is = "resetsenha"


                sm("Swal.fire({icon: 'info',title: 'nao foi possivel concluir esta ação',text: 'Aguardando novas Atualizações',})")
                '    tb1 = tab1.IncluirAlterarDados("update " & tabela & " set suspenso = false, qtdtentativas = 0 where nrseq = " & hdnrseq.Value)
                '         carregarusuarios()

            Case Is = "senhapemail"


                sm("Swal.fire({icon: 'erro',title: 'nao foi possivel concluir esta ação',text: 'Aguardando novas Atualizações',})")
                '    tb1 = tab1.IncluirAlterarDados("update " & tabela & " set suspenso = false, qtdtentativas = 0 where nrseq = " & hdnrseq.Value)
                '         carregarusuarios()

        End Select


    End Sub


    Private Sub buscapermissoes()

        Dim tbplanos As New Data.DataTable
        Dim tabplanos As New clsBanco

        tbplanos = tabplanos.conectar("select descricao from tbpermissoes where ativo = true order by descricao")
        cbopermissoes.Items.Clear()
        cbopermissoes.Items.Add("")
        For x As Integer = 0 To tbplanos.Rows.Count - 1
            cbopermissoes.Items.Add(tbplanos.Rows(x)("descricao").ToString)
        Next


    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

    Private Sub btnnovomedico_Click(sender As Object, e As EventArgs) Handles btnnovomedico.Click
        Dim xMedicos As New clsmedicos

        If Not xMedicos.novo() Then
            sm("Swal.fire({icon 'error',title: 'Não foi possivel Evetuar esta ação...',text: 'Contate o Administrador do Sistema',})")
            Exit Sub

        End If
        divmostradados.Visible = True
        limpacampos()
        hidcod.Value = xMedicos.Cod

        lblstatusmedico.Text = "(Novo)"
        lblnovomedico.Text = " Cadastro Profissional:" & hidcod.Value & " - "
        'mostrardadosmedicoounovo.Visible = False
    End Sub

    Private Sub btnprocurar_Click(sender As Object, e As EventArgs) Handles btnprocurar.Click
        carregarmedicos()
    End Sub

    Private Sub btnsalvarmedico_Click(sender As Object, e As EventArgs) Handles btnsalvarmedico.Click
        Dim informar As String
        informar = ""

        If txtnome.Text = "" Then
            informar = informar + " [Nome] "
        End If
        If txtCpf.Text = "" Then
            informar = informar + " [Cpf] "
        End If

        If txtEmail.Text = "" Then
            informar = informar + " [Email] "
        End If

        If txtCep.Text = "" Then
            informar = informar + " [Cep] "
        End If

        '  Dados Proficionaisc

        If txtCrm.Text = "" Then
            informar = informar + " [CRM] "
        End If

        If txtUfcrm.Text = "" Then
            informar = informar + " [UF CRM] "
        End If

        'If txtcontato.Text = "" Then
        '    informar = informar + " [Contato] "
        'End If

        If txtCepcomercial.Text = "" Then
            informar = informar + " [Cep Comercial] "
        End If


        If informar <> "" Then
            sm("Swal.fire({icon: 'error',title: 'O Campo " & informar & "',text: 'Preencha os campos que estão faltando ',})")
            Exit Sub
        End If

        Dim xMedicos As New clsmedicos

        xMedicos.Cod = hidcod.Value

        xMedicos.Nome = txtnome.Text
        xMedicos.Cpf = txtCpf.Text
        ' xMedicos.Dtnasc = txtDtnasc.Text
        xMedicos.Email = txtEmail.Text
        xMedicos.Celular = txtCelular.Text
        xMedicos.Cep = txtCep.Text
        xMedicos.Estado = txtEstado.Text
        xMedicos.Endereco = txtEndereco.Text
        xMedicos.Bairro = txtBairro.Text
        xMedicos.Cidade = txtCidade.Text
        'dados do profissional

        xMedicos.Crm = txtCrm.Text
        xMedicos.Ufcrm = txtUfcrm.Text
        xMedicos.Tel2 = txtcontato.Text
        xMedicos.Cepcomercial = txtCepcomercial.Text
        xMedicos.Horasem = cbominutosagenda.SelectedItem.Text
        ' dados para acesso medico
        ' xMedicos.Login = txtLogin.Text
        ' xMedicos.senha = txtsenha.Text
        ' xMedicos.permissoes = txtpermissoes.Text
        ' xMedicos.senha = txtsenha.Text


        If Not xMedicos.salvar() Then
            sm("Swal.fire({icon: 'error',title: 'Não foi possivel Evetuar esta ação...',text: 'Contate o Administrador do Sistema',})")
            Exit Sub
        Else
            divmostradados.Visible = False
            sm("swal({title: 'Muito Bom!',text: 'Cadastro salvo com sucesso !',type: 'success',confirmButtonText: 'OK'})", "swal")
        End If
        'mostrardadosmedicoounovo.Visible = False
        carregarmedicos()
        limpacampos()

    End Sub










    Private Sub gradehoras_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradehoras.RowCommand
        'Dim existe As Boolean = False
        'If hdagenda.Value = "" Then
        '    Exit Sub
        'End If
        'If txthorasel.Text = "Já preenchido" Then
        '    Exit Sub
        'End If
        Dim linha As GridViewRow
        txthorasel.Text = e.CommandArgument
        txtdocasel.Text = e.CommandName - 1
        linha = gradehoras.Rows(txthorasel.Text)


        Dim btnver As LinkButton = linha.FindControl("btnver" & (txtdocasel.Text))
        Dim xpanel As Panel = linha.FindControl("painel" & (txtdocasel.Text))
        Dim lbltexto As Label = linha.FindControl("lbltexto" & (txtdocasel.Text))
        Dim hdstatus As HiddenField = CType(btnver.Controls(1), HiddenField) ' linha.FindControl("hdstatus" & (txtdocasel.Text))
        '  lblhora.Text = linha.Cells(0).Text
        lbldiasemana.Text = CType(gradehoras.HeaderRow.Cells(txtdocasel.Text + 1).Controls(0), Label).Text

        Dim existelista As Boolean = False

        Dim xcont As New List(Of clscontroleagenda)
        If Session("dadosenviaragenda") <> "" Then
            Dim rangenovo As New List(Of String)
            rangenovo.AddRange(Split(Session("dadosenviaragenda"), ";"))

            For y As Integer = 0 To rangenovo.Count - 1
                Dim xtemp As New clscontroleagenda
                xtemp = JsonConvert.DeserializeObject(Of clscontroleagenda)(rangenovo(y))

                xcont.Add(xtemp)
            Next
            existelista = False
            For y As Integer = 0 To xcont.Count - 1
                If xcont(y).Doca = lbldiasemana.Text AndAlso xcont(y).Hora = linha.Cells(0).Text Then
                    existelista = True
                End If
                xcontrole.Data = data()
                xcontrole.Doca = xcont(y).Doca
                xcontrole.Hora = xcont(y).Hora
                xcontrole.Valor = xcont(y).Valor
                xcontrole.Status = xcont(y).Status
                xcontrole.adicionastatus()
            Next
            For y As Integer = 0 To xcont.Count - 1
                If xcont(y).Doca = lbldoca.Text AndAlso xcont(y).Hora = linha.Cells(0).Text AndAlso xcont(y).Data = data() Then
                    xcontrole.Data = data()
                    xcontrole.Doca = xcont(y).Doca
                    xcontrole.Hora = xcont(y).Hora
                    xcontrole.Valor = xcont(y).Valor
                    xcontrole.Status = xcont(y).Status
                    xcontrole.adicionastatus()
                    existelista = True
                End If


            Next
        End If

        xcontrole.Doca = CType(gradehoras.HeaderRow.Cells(txtdocasel.Text + 1).Controls(0), Label).Text
        xcontrole.Hora = linha.Cells(0).Text
        xcontrole.Data = data()

        xcontrole.Valor = True
        If Not existelista Then
            xcontrole.Status = "Vazio"
            xcontrole.adicionastatus(True)
        End If

        If lbltexto.Text = "Vazio" Then

            xcontrole.verstatus()

            If xcontrole.Status = "Marcado" Then
                xcontrole.Status = "Vazio"
                xpanel.BackColor = System.Drawing.Color.Transparent
                btnver.BackColor = System.Drawing.Color.Transparent
                lbltexto.ForeColor = System.Drawing.Color.Blue
                linha.Cells(txtdocasel.Text + 1).BackColor = System.Drawing.Color.Transparent

            Else
                xcontrole.Status = "Marcado"
                btnver.BackColor = System.Drawing.Color.AntiqueWhite
                xpanel.BackColor = System.Drawing.Color.AntiqueWhite
                lbltexto.ForeColor = System.Drawing.Color.Black
                linha.Cells(txtdocasel.Text + 1).BackColor = System.Drawing.Color.AntiqueWhite

            End If
            xcontrole.adicionastatus()

            'If hdagenda.Value = "" Then

            '    sm("swal({title: 'Atenção!',text: 'Nenhuma carga selecionada !',type: 'error',confirmButtonText: 'OK'})", "swal")

            '    Exit Sub
            'End If
            'divconfirma.Style.Remove("display")
            'Dim xpadrao As New clsconfig_padroes
            'xpadrao.Nome = "Confirmação de agendamento"
            'xpadrao.Nrseqcarga = hdagenda.Value
            'xpadrao.Datatemp = lbldata.Text
            'xpadrao.Hortemp = lblhora.Text
            'xpadrao.tratatextoemail()
            'txtresumo.Text = xpadrao.Textoemail
            'divhoras.Style.Add("display", "none")
            'divsetas.Style.Add("display", "none")

        Else
            'hdagenda.Value = hdstatus.Value

            'Dim xcarga As New clscargas
            'xcarga.Nrseq = hdagenda.Value
            'If Not xcarga.procurar() Then
            '    sm("swal({title: 'Atenção!',text: '" & xcarga.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            '    Exit Sub
            'End If

            'rpthoras03.DataSource = xcarga.Listahoras
            'rpthoras03.DataBind()

            'xcontrole.Status = lbltexto.Text

            'lbldata02.Text = lbldata.Text

            ''  lblhora02.Text = linha.Cells(0).Text
            'cbodoca02.Text = CType(gradehoras.HeaderRow.Cells(txtdocasel.Text + 1).Controls(0), Label).Text
            'divalterahorario01.Style.Remove("display")
            'divhoras.Style.Add("display", "none")
            'divsetas.Style.Add("display", "none")
            Exit Sub
        End If


        'For x As Integer = 0 To xcontrole.Lista.Count - 1
        Session("dadosenviaragenda") = ""

        For x As Integer = 0 To xcontrole.Lista.Count - 1
            Dim dadosenviar As String = JsonConvert.SerializeObject(xcontrole.Lista(x))


            If Session("dadosenviaragenda") = "" Then
                Session("dadosenviaragenda") = dadosenviar
            Else
                Session("dadosenviaragenda") = Session("dadosenviaragenda") & ";" & dadosenviar
            End If
        Next

        '' Next

        'Dim index As Integer = Convert.ToInt32(e.CommandArgument)

        'Dim row As GridViewRow = gradehoras.Rows(index)

    End Sub

    Private Sub gradehoras_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradehoras.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            For x As Integer = 1 To gradehoras.Columns.Count - 1

                Dim xcont As New List(Of clscontroleagenda)
                If Session("dadosenviaragenda") <> "" Then
                    Dim rangenovo As New List(Of String)
                    rangenovo.AddRange(Split(Session("dadosenviaragenda"), ";"))

                    For y As Integer = 0 To rangenovo.Count - 1
                        Dim xtemp As New clscontroleagenda
                        xtemp = (JsonConvert.DeserializeObject(Of clscontroleagenda)(rangenovo(y)))

                        xcont.Add(xtemp)
                    Next
                    For y As Integer = 0 To xcont.Count - 1
                        xcontrole.Doca = xcont(y).Doca
                        xcontrole.Data = xcont(y).Data
                        xcontrole.Hora = xcont(y).Hora
                        xcontrole.Valor = xcont(y).Valor
                        xcontrole.Status = xcont(y).Status
                        xcontrole.adicionastatus()
                    Next
                End If
                Dim btnver As LinkButton = e.Row.FindControl("btnver" & x - 1)
                Dim hdstatus As HiddenField = btnver.Controls(1) 'e.Row.FindControl("hdstatus" & zeros(x, 2))

                For y As Integer = 0 To xcontrole.Lista.Count - 1
                    'If lbldata.Text = xcontrole.Lista(y).Data AndAlso e.Row.Cells(0).Text = xcontrole.Lista(y).Hora AndAlso xcontrole.Lista(y).Doca = CType(gradehoras.HeaderRow.Cells(x).Controls(0), Label).Text Then
                    '    'If xcontrole.Lista(y).Status = "Marcado" Then
                    '    '    btnver.BackColor = System.Drawing.Color.Gold
                    '    'Else
                    '    '    btnver.BackColor = System.Drawing.Color.Transparent
                    '    'End If
                    'End If
                Next

                btnver.CommandArgument = e.Row.RowIndex
                btnver.CommandName = x
                btnver.OnClientClick = "clicahoras('" & x - 1 & "','" & e.Row.RowIndex & "')"
                Dim lbltexto As Label = e.Row.FindControl("lbltexto" & x - 1)
                '        Dim coluna As String = CType(gradehoras.HeaderRow.Cells(e.Row.RowIndex).Controls(0), Label).Text
                For y As Integer = 0 To xhoras.Listahorarios.Count - 1

                    If xhoras.Listahorarios(y).Hora = e.Row.Cells(0).Text AndAlso xhoras.Listahorarios(y).Ativo = True AndAlso xhoras.Listahorarios(y).Diasemana = x Then

                        'xpanel.BackColor = System.Drawing.Color.Transparent
                        'btnver.BackColor = System.Drawing.Color.Transparent
                        e.Row.Cells(x).BackColor = System.Drawing.Color.AntiqueWhite
                        'Else
                        '    e.Row.Cells(x).BackColor = System.Drawing.Color.Transparent
                    End If

                Next
                'If txthorasel.Text <> "" AndAlso txtdocasel.Text <> "" Then
                '    If txthorasel.Text <> "Já preenchido" AndAlso txtdocasel.Text = x - 1 AndAlso txthorasel.Text = e.Row.RowIndex Then
                '        lbltexto.Text = "Selecionado"
                '    End If
                'End If
            Next
            ' ElseIf e.Row.RowType = DataControlRowType.Header Then


            '      If Session("telamedicosnrseqmedico") = "" Then Session("telamedicosnrseqmedico") = "0"
            '     xhoras.Cod = Session("telamedicosnrseqmedico")
            '      xhoras.procurar()

            'xagenda.Data = lbldata.Text
            'xagenda.carregaragenda()

            ' .OnClientClick = "clicahoras('" & _FieldArgument & "','" & .CommandArgument & "')"
        End If

    End Sub

    Private Sub gradehoras_Init(sender As Object, e As EventArgs) Handles gradehoras.Init
        carregarhoras()
    End Sub
    Private Sub carregarhoras()
        Dim xcalendario As New clsmontarcalendario


        Dim semanadias As New List(Of String)
        semanadias.Add("Domingo")
        semanadias.Add("Segunda")
        semanadias.Add("Terça")
        semanadias.Add("Quarta")
        semanadias.Add("Quinta")
        semanadias.Add("Sexta")
        semanadias.Add("Sábado")



        For y As Integer = gradehoras.Columns.Count - 1 To 2 Step -1
            gradehoras.Columns.Remove(gradehoras.Columns(y))
        Next

        For y As Integer = 0 To semanadias.Count - 1



            Dim xcampo As New TemplateField
            xcalendario.Controlehoras = gradehoras
            xcampo.HeaderTemplate = New clsmontarcalendario(ListItemType.Header, semanadias(y), "")


            xcampo.ItemTemplate = New clsmontarcalendario(ListItemType.Item, semanadias(y), y)
            gradehoras.Columns.Add(xcampo)
            '  gradehoras.Columns.Add(xcampo2)
        Next

        gradehoras.DataSource = xcalendario.Retornarhoras
        gradehoras.DataBind()
    End Sub

    Private Sub btncarregar_Click(sender As Object, e As EventArgs) Handles btncarregar.Click
        carregarhoras()
    End Sub

    Private Sub Medicos_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        If IsPostBack Then Exit Sub
        Session("dadosenviaragenda") = ""
    End Sub

    Private Sub btnsalvarhoras_Click(sender As Object, e As EventArgs) Handles btnsalvarhoras.Click
        Dim xcont As New clscontroleagenda
        Dim xmedicos As New clsmedicos
        xmedicos.Nrseq = Session("telamedicosnrseqmedico")


        For Each linha As GridViewRow In gradehoras.Rows

            Dim btnver As LinkButton = linha.FindControl("btnver" & (txtdocasel.Text))
            Dim xpanel As Panel = linha.FindControl("painel" & (txtdocasel.Text))
            Dim lbltexto As Label = linha.FindControl("lbltexto" & (txtdocasel.Text))
            'lblhora.Text = linha.Cells(0).Text
            'lbldoca.Text =
            For y As Integer = 1 To gradehoras.Columns.Count - 1
                Dim diasemana As String = retornacodsemana(CType(gradehoras.HeaderRow.Cells(y).Controls(0), Label).Text)
                If linha.Cells(y).BackColor = System.Drawing.Color.AntiqueWhite Then
                    ' lbldoca.Text = CType(gradehoras.HeaderRow.Cells(txtdocasel.Text + 1).Controls(0), Label).Text
                    ' xcont.Lista.Add(New clscontroleagenda With {.Data = data(), .Hora = linha.Cells(0).Text, .Status = "Marcado", .Doca = CType(gradehoras.HeaderRow.Cells(txtdocasel.Text + 1).Controls(0), Label).Text})
                    xmedicos.Listahorarios.Add(New clsmedicos_horarios With {.Diasemana = diasemana, .Hora = linha.Cells(0).Text, .Ativo = True, .Nrseqmedico = xmedicos.Nrseq, .Dtcad = data()})
                End If
            Next

        Next

            If xMedicos.Listahorarios.Count = 0 Then
            sm("swal({title: 'Atenção!',text: 'Selecione ao menos um horário para prosseguir !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        If Not xMedicos.salvar Then
            sm("swal({title: 'Atenção!',text: '" & xMedicos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        divhorarios.Style.Add("display", "none")
    End Sub

    Private Sub executarmodal()
        sm("$(""#myModal"").modal()")
    End Sub

    Private Sub btnvoltarhoras_Click(sender As Object, e As EventArgs) Handles btnvoltarhoras.Click
        divhorarios.Style.Add("display", "none")
    End Sub

    Private Sub txtCep_TextChanged(sender As Object, e As EventArgs) Handles txtCep.TextChanged
        Dim xcep As New clsCEP

        xcep.Cep = txtCep.Text

        If Not xcep.buscarcep Then
            sm("swal({title: 'Atenção!',text: '" & xcep.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        txtBairro.Text = xcep.Bairro
        txtCidade.Text = xcep.Cidade
        txtEstado.Text = xcep.Uf
        txtEndereco.Text = xcep.Endereco
        txtEndereco.Focus()
    End Sub

    Private Sub btnCancelarnovo_Click(sender As Object, e As EventArgs) Handles btnCancelarnovo.Click
        divmostradados.Visible = False
    End Sub

    Private Sub btnsalvaruser_Click(sender As Object, e As EventArgs) Handles btnsalvaruser.Click
        Dim xUsuario As New clsusuarios
        Dim informar As String
        informar = ""

        If txtLogin.Text = "" Then
            informar = informar + " [Login] "
        End If

        If txtEmail.Text = "" Then
            informar = informar + " [E-mail] "
        End If

        If txtsenha.Text = "" Then
            informar = informar + " [Senha] "
        End If
        If cbopermissoes.SelectedValue = "" Then
            informar = informar + " [Permissões] "
        End If

        If informar <> "" Then
            sm("Swal.fire({icon: 'error',title: 'O Campo " & informar & "',text: 'Para criar o acesso ao Sistema  ',})")
            Exit Sub
        End If

        xUsuario.Usuario = txtLogin.Text
        ' xMedicos.Dtnasc = txtDtnasc.Text
        xUsuario.Email = txtEmail.Text
        xUsuario.Senha = txtsenha.Text
        xUsuario.Permissao = cbopermissoes.SelectedValue
        xUsuario.Nrseqmedico = hidcod.Value


        If Not xUsuario.salvarmedicousuario Then
            sm("swal({title: 'Atenção!',text: '" & xUsuario.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If




    End Sub
End Class

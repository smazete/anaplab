﻿Imports clssessoes
Imports System.IO
Imports clsSmart


Partial Class caixadeemail
    Inherits System.Web.UI.Page


    Private Sub caixadeemail_Load(sender As Object, e As EventArgs) Handles Me.Load
        DirectCast(DirectCast(Page.Master, MasterPage).FindControl("panelprincipal"), Panel).DefaultButton = ""
        If IsPostBack Then
            Exit Sub
        End If
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub


    Public Sub carregaassociados(Optional exibirinativos As Boolean = False)
        'Dim xempresas As New clsEmpresas
        Dim xAssociados As New clsAssociado
        Dim consulta As String

        consulta = ""

        If txtbusca.Text = "" Then
            consulta = ""
            sm("swal({title: 'Error!',text: 'Essa Busca pode demorar',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        Else
            consulta = "where " & cbobusca.SelectedValue & " = '" & txtbusca.Text & "'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbAssociados " & consulta & "")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        gradeassociados.DataSource = tb1
        gradeassociados.DataBind()

    End Sub

    Private Sub btnenviar_Click(sender As Object, e As EventArgs) Handles btnenviar.Click

        Dim xCaixapostal As New clscaixapostal

        '  xCaixapostal.Cxpstatus = txtcxpstatus.text
        '   xCaixapostal.Msgp = txtmsgp.text
        '  xCaixapostal.Msga = txtmsga.text
        ' xCaixapostal.Msgi = txtmsgi.text

        If hddadosuser.Value = "" Then
            xCaixapostal.Nrseqassociado = "0"
        Else
            xCaixapostal.Nrseqassociado = hddadosuser.Value
        End If
        xCaixapostal.Assunto = txtassunto.Text
        xCaixapostal.Texto = txtcorpo.Text

        If Not xCaixapostal.salvar Then
            sm("swal({title: 'Atenção!',text: '" & xCaixapostal.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'envio realizado com sucesso',  showConfirmButton: false,  timer: 2000})")

    End Sub


    Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click
        carregaassociados()
    End Sub

    Private Sub gradeassociados_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeassociados.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeassociados.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "usar" Then
            '    carregarNoFormulario(nrseq.Value)
            '  habilitar(True)
            '  txtidentificador.Enabled = False

            Dim xAssociados As New clsAssociado

            xAssociados.Nrseq = nrseq.Value
            hddadosuser.Value = nrseq.Value


            If Not xAssociados.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If


            hddadosuser.Value = nrseq.Value
            lblcodigo.Text = xAssociados.Nrseq
            lblmatricula.Text = xAssociados.Matricula
            lblnome.Text = xAssociados.Nome
            lblcpf.Text = xAssociados.Cpf
            chkenvmod.Checked = True
            carregaassociados()



        End If
    End Sub

End Class

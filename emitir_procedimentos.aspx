﻿<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="emitir_procedimentos.aspx.vb" Inherits="restrito_emitir_procedimentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <script src="../js/JScriptmascara.js" type="text/javascript"></script>
    <script type="text/javascript">
        function test(cidade) {



            alert(cidade);

        }

        function irlink(linkativo) {



            window.open('/restrito/imprimir_exames.aspx?i=' + linkativo, '_blank');

        }

        function irlinkmesmpagina(linkativo) {

            window.location.href = linkativo;

            //window.open(linkativo);

        }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanel3" runat="server">
        <ContentTemplate>



            <div class="box box-success" style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
                <div class="box box-header"><b>Emissão de Procedimentos</b></div>
                <div class="box-body">

                    <div class="row ">
                        <div class="col-lg-12 text-center">
                            <asp:LinkButton runat="server" CssClass="btn btn-primary  " ID="btnnovo"> <asp:Image runat="server" style="width:2rem ;height:2rem;" ImageUrl="~/img/agendar.png" />Novo</asp:LinkButton>
                        </div>
                        <asp:HiddenField runat="server" ID="hdnrseq" />
                    </div>
                    <div class="row">
                        <div class=" col-lg-4">
                            <label for="ddlPlano">Plano</label>
                            <asp:DropDownList runat="server" ID="ddlPlano" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                        <div class=" col-lg-4">
                            <label for="ddlConvenio">Convênio</label>
                            <asp:DropDownList runat="server" ID="ddlConvenio" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class=" col-lg-1">
                            <br />
                            <asp:LinkButton runat="server" CssClass="btn btn-success  " ID="btntrava"> <asp:Image runat="server" style="width:2rem ;height:2rem;" ImageUrl="~/img/imgokboll.png" />Abrir</asp:LinkButton>

                        </div>
                        <div class=" col-lg-2">
                            <br />
                            <asp:LinkButton runat="server" CssClass="btn btn-success  " ID="btngrupos"> <asp:Image runat="server" style="width:2rem ;height:2rem;" ImageUrl="~/img/abrir01.png" />Abrir Grupos Procedimentos</asp:LinkButton>

                        </div>
                    </div>

                    <div runat="server" id="rowGrupos" class="row" style="display: none;">
                        <div class="form-group col-lg-4">
                            <label for="ddlGrupos">Grupos</label>
                            <asp:DropDownList runat="server" ID="ddlGrupos" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="form-group col-lg-3">
                            <label for="ddlProcedimentos">Procedimentos</label>
                            <asp:DropDownList runat="server" ID="ddlProcedimentos" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="txtvalorprocedimento">Valor Procedimento</label>
                            <input runat="server" type="text" class="form-control" id="txtvalorprocedimento" onkeyup="formataValor(this,event);" clientidmode="Static" readonly />
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="txtvalorplano">Valor Plano</label>
                            <input runat="server" type="text" class="form-control" id="txtvalorplano" onkeyup="formataValor(this,event);" clientidmode="Static" />
                        </div>
                        <div class="form-group col-lg-2">
                            <label for="txtvalorpaciente">Valor Paciente</label>
                            <input runat="server" type="text" class="form-control" id="txtvalorpaciente" onkeyup="formataValor(this,event);" clientidmode="Static" />
                        </div>
                        <div class="form-group col-lg-1 text-center">
                            <br />
                            <asp:Button runat="server" CssClass="btn btn-info " ID="btnOK" Text="OK" />
                        </div>
                        <div class="form-group col-lg-2">
                            <br />
                            <asp:Button runat="server" CssClass="btn btn-danger " ID="btnTrocar" Text="Trocar de convênio" Enabled="false" />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                                <ProgressTemplate>
                                    <center>
                                              <img src="../img/loadred.gif" style="height:5rem;width:5rem;" />
                                                    </center>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>

                    <div class="row" id="rowError" runat="server" style="display: none">

                        <br />
                        <center>
                                <div class="col-lg-3">
                                </div>
                                <div class="alert alert-danger col-lg-6" role="alert">

                                    <p>
                                        <center>
                                                <strong>Aconteceu algo de errado<br /></strong> 
                                                Os campos: <strong id="lblErro" runat="server"></strong><br /> são de preenchimento obrigatório!
                                            </center>
                                    </p>
                                </div>
                                    </center>
                    </div>


                </div>

                <br />
                <br />
                <div class="container-fluid">
                    <div class="box box-danger">
                        <div class="box-header"><b>Lista de Procedimentos</b></div>
                        <div class="box-body">
                            <asp:GridView ID="gdvListaExames" runat="server" CssClass="table " AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum procedimento adicionado !">
                                <Columns>
                                    <asp:BoundField DataField="nrseqexame" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                    <asp:BoundField DataField="nomeempresa" HeaderText="Laboratório" />
                                    <asp:BoundField DataField="descricao" HeaderText="Procedimentos" />
                                    <asp:BoundField DataField="valorproced" HeaderText="Valor" />
                                    <asp:BoundField DataField="valorplano" HeaderText="Valor Plano" />
                                    <asp:BoundField DataField="valorcliente" HeaderText="Valor Paciente" />
                                    <asp:TemplateField HeaderText="Opções">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="deletar" runat="server" CssClass="light-blue-text text-darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Daletar" CommandName="deletar" CommandArgument='<%#Container.DataItemIndex%>'>
                                            <i class="material-icons">delete</i> 
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>
                            <div class="form-group col-lg-2">
                                <label for="txtvalortotalpaciente">Valor Total Paciente</label>
                                <input runat="server" type="text" class="form-control" id="txtvalortotalpaciente" clientidmode="Static" readonly />
                                <asp:HiddenField ID="hdvlparcial" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="updatepanel1" runat="server">
                    <ContentTemplate>
                        <div class="container-fluid">
                            <div class="box box-warning ">
                                <div class="box-header text-center"><b>Procura de clientes</b></div>
                                <div class="box-body">


                                    <div class="row">
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-3 ">
                                            <asp:DropDownList runat="server" ID="ddpListarPor" CssClass="form-control"></asp:DropDownList>

                                        </div>
                                        <div class="col-lg-4 ">
                                            <asp:TextBox ID="txtbusca" runat="server" CssClass="form-control " ClientIDMode="Static"></asp:TextBox>

                                            <%-- <input runat="server" type="text" class="form-control col-lg-2" id="txtbusca2" clientidmode="Static" />--%>
                                        </div>
                                        <div class="col-lg-2 ">
                                            <asp:LinkButton runat="server" ID="btnprocura" CssClass="btn btn-primary" ClientIDMode="Static"><i class="fa fa-search"></i> Localizar</asp:LinkButton>
                                        </div>



                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-12">
                            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <center>
                                              <img src="../img/loadred.gif" style="width:3rem;height:3rem;" />
                                                    </center>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                        </div>
                        <div class="row" style="margin-left: .5rem; margin-right: 1rem;">
                            <div>

                                <asp:GridView ID="gdvListaClientes" runat="server" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sel">
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="sel" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sel">
                                            <ItemTemplate>
                                                <span class="badge" style="background: #1E90FF">
                                                    <asp:Label ID="lblitem" runat="server"></asp:Label></span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="nrseq" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                        <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                        <asp:BoundField DataField="nome" HeaderText="Nome" />
                                        <asp:BoundField DataField="cpf" HeaderText="CPF" />
                                        <asp:BoundField DataField="email" HeaderText="E-mail" />
                                        <asp:BoundField DataField="telresidencia" HeaderText="Telefone Res." />
                                        <asp:BoundField DataField="telcelular" HeaderText="Celular" />
                                        <asp:BoundField DataField="logradouro" HeaderText="Logradouro" />
                                        <asp:BoundField DataField="numero" HeaderText="Numero" />
                                        <asp:BoundField DataField="cidade" HeaderText="Cidade" />
                                        <asp:BoundField DataField="uf" HeaderText="Estado" />
                                        <asp:BoundField DataField="edependente" HeaderText="dependente" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                        <asp:BoundField DataField="nrseqdependente" HeaderText="dependente" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
               
                <asp:UpdatePanel runat="server" ID="updategerar">
                    <ContentTemplate>
                        <div class="row" style="margin-left: .5rem; margin-right: 1rem;">
                            
                                <div class="box box-success">
                                    <div class="box-body">
                                           <div class="row">
                                        <div class=" col-lg-2">
                                            <label for="txtsolicitadopor">Solicitado por</label>
                                            <input runat="server" type="text" class="form-control" id="txtsolicitadopor" clientidmode="Static" />
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="ddlPagamento">Forma de pagamento</label>
                                            <asp:DropDownList runat="server" ID="ddlPagamento" CssClass="form-control" AutoPostBack="true" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <br />

                                            <asp:LinkButton runat="server" CssClass="btn btn-success  " ID="btngerar"> <asp:Image runat="server" style="width:2rem ;height:2rem;" ImageUrl="~/img/ligado.png" />Gerar</asp:LinkButton>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <br />

                                            <asp:LinkButton runat="server" CssClass="btn btn-primary  " ID="btnimprimir"> <asp:Image runat="server" style="width:2rem ;height:2rem;" ImageUrl="~/img/print01.png" />Imprimir</asp:LinkButton>
                                        </div>
                                        <div class="col-lg-2 text-center">
                                            <br />

                                            <asp:LinkButton runat="server" CssClass="btn btn-danger  " ID="btncancelar"> <asp:Image runat="server" style="width:2rem ;height:2rem;" ImageUrl="~/img/imgnao.png" />Cancelar</asp:LinkButton>
                                        </div>
                                               </div> 
                                        <div class="row">
                                        <div class="col-lg-12 text-center">

                                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updategerar">
                                                <ProgressTemplate>
                                                    <img src="../img/carregando1.gif" style="width:3rem;height:3rem;" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>



                                        </div>
                                        </div>

                                        <div class="row" id="rowErro2" runat="server" style="display: none">
                                            <br />
                                            <div class="col-lg-3">
                                            </div>
                                            <div class="alert alert-danger col-lg-6" role="alert">
                                                <p>
                                                    <center>
                                                        <strong>Aconteceu algo de errado<br /></strong> 
                                                        <strong id="lblErro2" runat="server"></strong><br /> 
                                                    </center>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


            </div>

        </ContentTemplate>
    </asp:UpdatePanel>






</asp:Content>

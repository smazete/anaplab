﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="minhasmensalidades.aspx.vb" MasterPageFile="~/MasterPage3.master" Inherits="minhasmensalidades" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
<div class="box box-header">
<b> Minhas mensalidades. </b>
</div>
     <div class="box box-body">   
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
 <asp:GridView ID = "grademensalidades" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma mensalidade encontrada !" CssClass="table " GridLines="none" AutoGenerateColumns="false"   >
<columns>
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
             <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                        <asp:BoundField DataField="nome" HeaderText="Cliente" />
                        <asp:BoundField DataField="statuspg" HeaderText="Status" />
                        <asp:BoundField DataField="mes" HeaderText="Mês" />
                        <asp:BoundField DataField="ano" HeaderText="Ano" />

 </columns>
</asp:GridView >

</div>
        
                <div class="col-lg-2"></div>

</div>
</div>




</asp:Content>
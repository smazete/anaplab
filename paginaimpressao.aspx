﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="paginaimpressao.aspx.vb" Inherits="paginaimpressao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script  src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script  src="js/jquery.min.js" type="text/javascript"></script>
    <style type="text/css">

*{margin: 0; padding: 0; font-size: 100%; border: none; outline: none; font-weight: 300; box-sizing: border-box; font-family: "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", "DejaVu Sans", Verdana, "sans-serif";}


            .grade th {
                font: bold 11px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
                color: #fbfbfb;
                border-right: 1px solid #C1DAD7;
                border-bottom: 1px solid #C1DAD7;
                border-top: 1px solid #C1DAD7;
                letter-spacing: 2px;
                text-transform: uppercase;
                text-align: left;
                padding: 3px 3px 3px 6px;
                background: #990033;
                height: 5px;
            }

            .grade td {
                border-right: 1px solid #C1DAD7;
                border-bottom: 1px solid #C1DAD7;
                background: #fff;
                padding: 3px 3px 3px 6px;
                
            
            }

                .botaored 
        
        {
            background-color :#fff;
            color :#990033;
           
         border-radius :10px
        }

        .botaoredondo{
            background-color : #990033  ;
            color :white;
           
         border-radius :10px
        }
         </style>
</head>
<body onload="self.print();" class="corpo2">
    <form id="form1" runat="server">
        <div>
            <center>
                <asp:Label ID="Label1" runat="server" Text=" - RELATÓRIO DE LOTES CARREGADOS - "></asp:Label>
                <br />

            </center>
            <asp:Label ID="lblData" runat="server" Text="xxxx"></asp:Label>
            <br />
            <asp:Label ID="Label2" runat="server" Text="Período: "> </asp:Label><asp:Label ID="lbldtinicial" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Label3" runat="server" Text=" à "> </asp:Label><asp:Label ID="lbldatafinal" runat="server" Text="Label"></asp:Label>
            <br />
            <br />
                    
                     
                     <br />
            <asp:Label ID="Label4" runat="server" Text="Total líquido:"></asp:Label>&nbsp;<asp:Label ID="lblvltotal" runat="server" Text="Total líquido:"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label5" runat="server" Text="Qtd Impresso:"></asp:Label>
            <asp:Label ID="lblqtd" runat="server" Text="Total líquido:"></asp:Label>
            <br />
                         <asp:GridView ID="GradeLotes" runat="server" AutoGenerateColumns="False" CellPadding="4" Width="100%"  ShowHeaderWhenEmpty="True" CssClass="grade" ForeColor="Black" GridLines="Horizontal" style="text-align: center; margin-bottom: 0px;" >
                         <Columns>
                            
                             <asp:BoundField DataField="dtemissao" HeaderText="Data Emissão" />
                             <asp:BoundField DataField="usercad" HeaderText="Filial" />
                             <asp:BoundField DataField="nrcte" HeaderText="Nr CTe" />
                           <asp:BoundField DataField="loteempresa" HeaderText="Nr Fat Cliente" />
                              <asp:BoundField DataField="nomecliente" HeaderText="Cliente" />
                             <asp:BoundField DataField="refcliente" HeaderText="Ref Cliente" />
                             <asp:BoundField DataField="refceva" HeaderText="Ref Ceva" />
                             <asp:BoundField DataField="refdossie" HeaderText="Dossie" />
                              <asp:BoundField DataField="despesa" HeaderText="Despesa" />
                             <asp:BoundField DataField="moeda" HeaderText="Moeda" />
                             <%--<asp:BoundField DataField="valorpeso" HeaderText="Valor" />--%>
                              <%--<asp:BoundField DataField="desconto" HeaderText="Desconto" />--%>
                                  <asp:BoundField DataField="valorfinal" HeaderText="VLR TL SERV" />
                             <asp:BoundField DataField="vencimento" HeaderText="Vencimento" />
                             <asp:BoundField DataField="fretepeso" HeaderText="Frete Peso" />
                             <asp:TemplateField HeaderText="% comissão">
                                 <ItemTemplate >
                                     <asp:Label ID="lblcomissaoporc" runat="server" Text="7%"></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:BoundField DataField="valorcomissao" HeaderText="% comissão" />
                              <asp:TemplateField HeaderText="% Impostos">
                                 <ItemTemplate >
                                     <asp:Label ID="lblcomissaoporc" runat="server" Text="13%"></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:BoundField DataField="valorcomissao" HeaderText="VLR. BRT. COM" />
                             <%-- <asp:BoundField DataField="valorimposto" HeaderText="% Impostos" />
                              <asp:BoundField DataField="valorimposto" HeaderText="Valor Impostos" />
                              <asp:BoundField DataField="valorliquido" HeaderText="Valor Líquido" />--%>
                             
                         </Columns>
                         <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                         <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                         <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                         <RowStyle BackColor="White" ForeColor="#330099" />
                         <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                         <SortedAscendingCellStyle BackColor="#FEFCEB" />
                         <SortedAscendingHeaderStyle BackColor="#AF0101" />
                         <SortedDescendingCellStyle BackColor="#F6F0C0" />
                         <SortedDescendingHeaderStyle BackColor="#7E0000" />
                     </asp:GridView>  
            <br />

        </div>
    </form>
</body>
</html>

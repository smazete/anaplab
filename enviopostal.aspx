﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="enviopostal.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="enviopostal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        section {
            padding-top: 4rem;
            padding-bottom: 5rem;
            background-color: #f1f4fa;
        }

        .wrap {
            display: flex;
            background: white;
            padding: 1rem 1rem 1rem 1rem;
            border-radius: 0.5rem;
            box-shadow: 7px 7px 30px -5px rgba(0,0,0,0.1);
            margin-bottom: 2rem;
        }

            .wrap:hover {
                background: linear-gradient(135deg,#6394ff 0%,#0a193b 100%);
                color: white;
            }

        .ico-wrap {
            margin: auto;
        }

        .mbr-iconfont {
            font-size: 4.5rem !important;
            color: #313131;
            margin: 1rem;
            padding-right: 1rem;
        }

        .vcenter {
            margin: auto;
        }

        .mbr-section-title3 {
            text-align: left;
        }

        h2 {
            margin-top: 0.5rem;
            margin-bottom: 0.5rem;
        }

        .display-5 {
            font-family: 'Source Sans Pro',sans-serif;
            font-size: 1.4rem;
        }

        .mbr-bold {
            font-weight: 500;
            font-weight: 500;
        }

        p {
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: 25px;
        }

        .display-6 {
            font-family: 'Source Sans Pro',sans-serif;
            font-size: 1re
        }
    </style>

    
     <!-- Font Awesome -->
  <link rel="stylesheet" href="../../editor/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
<%-- <link rel="stylesheet" href="../../editor/dist/css/adminlte.css">--%>
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../../editor/summernote/summernote-bs4.css">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>CAIXA POSTAL  </b>
            <asp:LinkButton Text="Voltar" ID="btnvoltar" runat="server" CssClass="btn btn-default btn-sm right"><i class="fa fa-reply"></i></asp:LinkButton>
        </div>
        <%-- envia para somente um dos clientes  --%>
        <div class="box-body" runat="server" id="divescolha">
            <section>
                <div class="container">
                    <div class="row mbr-justify-content-center">
                        <div class="col-lg-2 mbr-col-md-10">
                            <asp:LinkButton ID="btncaixadeentrada" Text="text" runat="server">
                    <div class="row">Caixa de entrada</div>
                <div class="wrap">
                    <div class="ico-wrap">
                        <span class="mbr-iconfont fa-envelope-o fa"></span>
                    </div>
                    <div class="text-wrap vcenter">
                        <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5"><b></b><span></span>
                        </h2>
                        <p class="mbr-fonts-style text1 mbr-text display-6">Envios: 
                          <asp:Label Text="0" runat="server" /><br /> Vistos: 
                          <asp:Label Text="0" runat="server" /><br /> Modelo: 
                          <asp:Label Text="0" runat="server" /></p>
                    </div>
                </div></asp:LinkButton>
                        </div>
                        <div class="col-lg-4 mbr-col-md-10">
                            <asp:LinkButton ID="btnpublicas" Text="text" runat="server">
                   <div class="row">PÚBLICAS</div>
                <div class="wrap">
                    <div class="ico-wrap fa fa-envelope-o fa fa-envelope-o fa fa-envelope-o">
                        <span class="mbr-iconfont fa-users fa"></span>
                    </div>
                    <div class="text-wrap vcenter">
                        <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5"><span><b>PÚBLICAS</b></span> Todos Associados</h2>
                        <p class="mbr-fonts-style text1 mbr-text display-6">Envio de mensagens para todos Associados ativos entro do sistema </p>
                    </div>
                </div></asp:LinkButton>
                        </div>
                        <div class="col-lg-4 mbr-col-md-10">
                            <asp:LinkButton ID="btnprivadas" Text="text" runat="server">   <div class="row">PRIVADAS</div>
                <div class="wrap">
                    <div class="ico-wrap fa fa-envelope-o"">
                        <span class="mbr-iconfont fa-user fa"></span>
                    </div>
                    <div class="text-wrap vcenter">
                        <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5"><b>PRIVADAS</b>
                            <span>Unico Associado</span>
                        </h2>
                        <p class="mbr-fonts-style text1 mbr-text display-6">Envio exclusivo de mensagem para a caixa de entrada do associado selecionado</p>
                    </div>
                </div></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </section>

        </div>

        <%-- Caixa de entrada usuario   --%>
        <div class="box-body" runat="server" id="divescolha1" visible="false">
            <section class="content">
                <div class="row">
                    <div class="col-md-3">
                        <a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>

                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Folders</h3>

                                <div class="box-tools">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-pills nav-stacked">
                                    <li class="active"><a href="#"><i class="fa fa-inbox"></i>Inbox
                  <span class="label label-primary pull-right">12</span></a></li>
                                    <li><a href="#"><i class="fa fa-envelope-o"></i>Sent</a></li>
                                    <li><a href="#"><i class="fa fa-file-text-o"></i>Drafts</a></li>
                                    <li><a href="#"><i class="fa fa-filter"></i>Junk <span class="label label-warning pull-right">65</span></a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-trash-o"></i>Trash</a></li>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /. box -->
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Labels</h3>

                                <div class="box-tools">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-pills nav-stacked">
                                    <li><a href="#"><i class="fa fa-circle-o text-red"></i>Important</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i>Promotions</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i>Social</a></li>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-9">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Inbox</h3>

                                <div class="box-tools pull-right">
                                    <div class="has-feedback">
                                        <input type="text" class="form-control input-sm" placeholder="Search Mail">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <div class="mailbox-controls">
                                    <!-- Check all button -->
                                    <button type="button" class="btn btn-default btn-sm checkbox-toggle">
                                        <i class="fa fa-square-o"></i>
                                    </button>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                                    </div>
                                    <!-- /.btn-group -->
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                                    <div class="pull-right">
                                        1-50/200
                  <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                      <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                                        <!-- /.btn-group -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                                <div class="row">
                                    <br>
                                    <asp:GridView ID="GridView2" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma doca cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
                                                    <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblnrseqgrade" runat="server" Text='<%#Bind("nrseq")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcxpstatusgrade" runat="server" Text='<%#Bind("cxpstatus")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblmsgpgrade" runat="server" Text='<%#Bind("msgp")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblmsgagrade" runat="server" Text='<%#Bind("msga")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblmsgigrade" runat="server" Text='<%#Bind("msgi")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblid_pessoagrade" runat="server" Text='<%#Bind("id_pessoa")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblassuntograde" runat="server" Text='<%#Bind("assunto")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltextograde" runat="server" Text='<%#Bind("texto")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldtcadgrade" runat="server" Text='<%#Bind("dtcad")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblvistograde" runat="server" Text='<%#Bind("visto")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <!-- /.mail-box-messages -->
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer no-padding">
                                <div class="mailbox-controls">
                                    <!-- Check all button -->
                                    <button type="button" class="btn btn-default btn-sm checkbox-toggle">
                                        <i class="fa fa-square-o"></i>
                                    </button>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                                    </div>
                                    <!-- /.btn-group -->
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                                    <div class="pull-right">
                                        1-50/200
                  <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                      <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                                        <!-- /.btn-group -->
                                    </div>
                                    <!-- /.pull-right -->
                                </div>
                            </div>
                        </div>
                        <!-- /. box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>

        </div>

        <%-- Enviar mensagem publica    --%>

        <div class="box-body" runat="server" id="divescolha2" visible="false">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <asp:Label ID="lblheader2" Text="Envio" runat="server" /></h3>
                    </div>
                    <%-- envio privatvo --%>
                    <div class="row" runat="server" id="divprivado" visible="false">
                        <div class="row">
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control">
                                    <asp:ListItem Text="Codigo" />
                                    <asp:ListItem Text="Nome" />
                                    <asp:ListItem Text="Matricula" />
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-6">
                                <asp:TextBox runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-md-3">
                                <asp:LinkButton Text="btnbuscarcliente" runat="server" CssClass="btn btn-primary " />
                            </div>
                        </div>
                        <div class="row">
                            <asp:HiddenField runat="server" ID="hddadosuser" />
                            <div class="col-md-2">
                                Código
                        <asp:Label Text="0" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-md-2">
                                Matricula
                        <asp:Label Text="0" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-md-4">
                                Nome
                        <asp:Label Text="nome sobrenome" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-md-3">
                                CPF
                        <asp:Label Text="000.000.000-00" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                    </div>
                    <%-- enviopublico --%>
                    <div class="row" runat="server" id="divpublica" visible="false">
                        <div class="col-lg-3">
                            <b>
                                <asp:Label Text="Somente Ativos" runat="server" /><br />
                            </b>
                            <asp:CheckBox runat="server" Checked="true" />
                            Associados Ativos
                        </div>
                        <div class="col-lg-3">
                            <b>
                                <asp:Label Text="Somente Uma " runat="server" /><br />
                            </b>
                            <asp:CheckBox runat="server" />
                            Visualização
                        </div>
                        <div class="col-lg-3">
                            <b>
                                <asp:Label Text="Disponivel até" runat="server" /></b>
                            <asp:TextBox runat="server" CssClass="form-control" type="date"></asp:TextBox>
                        </div>
                    </div>

                    <%-- compartilhado com os demais --%>
                    <div class="row">
                 

                        <div class="col-lg-12">
                            <%-- começo--%>
                            <div class="row">
                                   <div class="col-md-12">
          <div class="card card-outline ">
            <div class="card-header">
           <%--   <h3 class="card-title">
                Envio Caixa Postal
                <small></small>
              </h3>--%>
              <!-- tools box -->
         <%--     <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>--%>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
                       <div class="col-lg-12">
                            <asp:Label Text="ASSUNTO:" runat="server" />
                            <asp:TextBox ID="txtassunto" runat="server" class="form-control" placeholder="Assunto:" />
                        </div>
                
                       <div class="col-lg-12">
                            <asp:Label Text="MENSAGEM:" runat="server" /><br />
              <div class="mb-3">
                <textarea class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  <asp:TextBox runat="server" TextMode="MultiLine" ID="teste"></asp:TextBox>
                      </div>
</div>
            </div>
          </div>
        </div></div>
                            <%-- fim --%>
                        </div>
                        <br />
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <br />
                        <center>
                        <asp:LinkButton ID="btnenvioprivado" Text="Enviar" runat="server" class="btn btn-primary"><i class="fa fa-envelope-o">Enviar</i></asp:LinkButton>
                        <asp:LinkButton ID="btnenviopublico" Text="Enviar" runat="server" class="btn btn-primary"><i class="fa fa-envelope-o">Enviar</i></asp:LinkButton></center>
                  
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /. box -->
        </div>


        <%-- Enviar mensagem Privada    --%>
    </div>
    <!-- jQuery -->
<script src="../../editor/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../editor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../editor/dist/js/demo.js"></script>
<!-- Summernote -->
<script src="../../editor/summernote/summernote-bs4.min.js"></script>
<script>
    $(function () {
        // Summernote
        $('.textarea').summernote()

    })
</script>
</asp:Content>

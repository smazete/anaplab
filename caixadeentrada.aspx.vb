﻿Imports clsSmart
Partial Class caixadeentrada
    Inherits System.Web.UI.Page
    Dim tbmensagens As New Data.DataTable
    Dim tbteste As New Data.DataTable
    Dim tabteste As New clsBanco
    Private Sub caixadeentrada_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack Then Exit Sub
        Dim resultado As String = valida(Session("usuario"), mARQUIVO(Request.Path, True), Session("usuariomaster")).ToLower
        Select Case resultado
            Case Is = "login"
                Session("urlretorno") = Request.Path
                Response.Redirect("login.aspx")
                Exit Sub
            Case Is = "erro"
                Response.Redirect("ops2.aspx")
                Exit Sub
        End Select

        tbmensagens.Rows.Clear()
        tbmensagens.Columns.Clear()
        tbmensagens.Columns.Add("mensagem")
        tbmensagens.Columns.Add("assunto")
        tbmensagens.Columns.Add("remetente")
        tbmensagens.Columns.Add("data")
        tbmensagens.Columns.Add("codigo")
        tbmensagens.Columns.Add("nrseq")

        Dim email As New clsRecebeEMail
        Dim i As Integer
        Dim msg As clsRecebeEMail.Message
        Dim msgstring As String
        Dim messages1 As Integer


        messages1 = email.connect

        If messages1 = -1 Then

            Exit Sub
        End If
        Dim originalCaption As String = ""
        Dim linhas As Integer = 0
        tbmensagens.Rows.Clear()
        ' lblteste.Text = "Qtd mensagens: " & messages1
        For i = messages1 To 1 Step -1

            messages1.ToString()

            msgstring = email.GetMessage(i)
            Dim newString As String = leremailhtml(msgstring.Replace(vbCr, "").Replace(vbLf, ""))
            msg = email.CreateFromText(msgstring)

            If Not msg._From.Contains("VPS.mailer@accenture.com") Then
                Continue For
            End If

            tbmensagens.Rows.Add()
            tbmensagens.Rows(linhas)("data") = msg._Date
            tbmensagens.Rows(linhas)("assunto") = msg._Subject
            tbmensagens.Rows(linhas)("mensagem") = msg._Body
            tbmensagens.Rows(linhas)("remetente") = msg._From
            tbmensagens.Rows(linhas)("codigo") = msg._Received
            tbmensagens.Rows(linhas)("nrseq") = i

            tbteste = tabteste.conectar("Select * from tbemails where remetente = '" & msg._From.Trim & "' and receive = '" & msg._Received.Trim & "'")
            If tbteste.Rows.Count = 0 Then
                tbteste = tabteste.IncluirAlterarDados("insert into tbemails (data, assunto, mensagem, remetente, nrmensagem, receive) values ('" & formatadatamysql(msg._Date) & "','" & msg._Subject & "','" & tratatexto(newString) & "','" & msg._From.Trim & "'," & i & ",'" & msg._Received.Trim & "')")

            End If



            linhas += 1

            '    msgitem.Text = msg._From
            '    msgitem.SubItems.Add(msg._Subject)
            '  msgitem.SubItems.Add(msg._Date)
            ' ListView1.Items.Add(msgitem)
            '  TextBox1.AppendText(msg._Body & vbCrLf)
        Next
        Grade.DataSource = tbmensagens
        Grade.DataBind()
    End Sub
End Class

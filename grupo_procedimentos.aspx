﻿<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="grupo_procedimentos.aspx.vb" Inherits="restrito_grupo_procedimentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <script src="../js/JScriptmascara.js" type="text/javascript"></script>

    <style>
        .lupa {
            margin-top: 26px;
            margin-left: 10px;
        }

        .btngerar {
            margin-top: 25px;
            margin-left: 32px;
        }

        .btnok {
            margin-top: 25px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanel3" runat="server">
        <ContentTemplate>


            <div class="box box-primary "  style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
                <div class="box box-header "><b>Cadastro de Grupos de Procedimentos</b></div>
                <div class="box-body">
                    <br />
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="txtgrupo">Grupo</label>
                            <input runat="server" id="txtgrupo" class="form-control" type="text" />
                        </div>
                        <div class="form-group col-lg-2">
                            <asp:Button CssClass="btn btn-primary btnok" runat="server" ID="btncriar" Text="Criar" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="ddlConvenio">Convênio</label>
                            <asp:DropDownList runat="server" ID="ddlConvenio" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-2">
                            <asp:Button CssClass="btn btn-primary btnok" runat="server" ID="btnaddconv" Text="Adicionar" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-4 col-lg-offset-2">
                            <label for="ddlProcedimentos">Procedimentos</label>
                            <asp:DropDownList runat="server" ID="ddlProcedimentos" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-2">
                            <asp:Button CssClass="btn btn-primary btnok" runat="server" ID="btnaddproc" Text="Adicionar" />
                        </div>
                    </div>


                    <br />
                    <br />
                    <br />

                    <div class="container-fluid">
                        <div class="box box-success ">
                            <div class="box box-header "><b>Lista</b></div>
                            <div class="box-body">
                                <div class="container">
                                    <asp:GridView ID="gdvListaExames" runat="server" CssClass="table " AutoGenerateColumns="false" GridLines="None">
                                        <Columns>
                                            <asp:BoundField DataField="nrseq" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                            <asp:BoundField DataField="descricao" HeaderText="Procedimento" />
                                            <asp:BoundField DataField="nome" HeaderText="Convênio" />
                                            <asp:TemplateField HeaderText="Opções">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="deletar" runat="server" CssClass="light-blue-text text-darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Daletar" CommandName="deletar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                        <i class="material-icons">delete</i> 
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <center>
                                        <div class="col-lg-3 col-lg-offset-3">
                                            <asp:Button runat="server" CssClass="btn btn-success" ID="btnsalvar" Text="Salvar" />
                                        </div>
                                        <div class="col-lg-3">
                                             <asp:Button runat="server" CssClass="btn btn-danger" ID="btncancelar" Text="Cancelar" />
                                        </div>
                                    </center>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="rowError" runat="server" style="display: none">
                        <br />
                        <div class="col-lg-3">
                        </div>
                        <div class="alert alert-danger col-lg-6" role="alert">
                            <p>
                                <center>
                                        <strong>Aconteceu algo de errado<br /></strong> 
                                        Os campos: <strong id="lblErro" runat="server"></strong><br /> são de preenchimento obrigatório!
                                    </center>
                            </p>
                        </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <center>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label for="txtprocura">Buscar</label>
                                            <input runat="server" type="text" class="form-control" id="txtprocura" clientidmode="Static" placeholder="Digite sua busca aqui" />
                                        </div>
                                    </div>
                                    <asp:LinkButton runat="server" ID="btnprocura" CssClass="btn btn-default lupa" ClientIDMode="Static"><i class="fas fa-search"></i></asp:LinkButton>
                                </center>
                            </div>
                        </div>
                    </div>

                    <br />
                    <br />
                    <br />

                    <div class="row">
                        <div class="container">
                            <div class="box box-success">
                                <div class="box box-header "><b>Lista de Grupos Cadastrados</b></div>
                                <div class="box-body">
                                    <asp:GridView ID="gdvListaGrupos" runat="server" CssClass="table" AutoGenerateColumns="false" GridLines="None">
                                        <Columns>
                                            <asp:BoundField DataField="nrseq" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                            <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                            <asp:TemplateField HeaderText="Ações" runat="server">
                                                <ItemTemplate runat="server">
                                                    <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                Alterar
                                                    </asp:LinkButton>

                                                    <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="deletar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                Deletar
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

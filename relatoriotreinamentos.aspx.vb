﻿Imports System.IO
Imports clsSmart

Partial Class relatoriotreinamentos
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Private Sub painel_Load(sender As Object, e As EventArgs) Handles Me.Load


        If IsPostBack Then Exit Sub


        carregagestores()
        carregaravaliacoes()
        If Session("gestor") = "Sim" Then
            divgestor.Style.Add("display", "none")
        End If
        '   txtano.Text = data.Year
    End Sub
    Private Sub carregaravaliacoes()
        cbocursos.Items.Clear()
        Dim xsql As String
        xsql = "select distinct descricao from tbcursos where ativo = true  order by descricao "

        tb1 = tab1.conectar(xsql)
        cbocursos.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbocursos.Items.Add(tb1.Rows(x)("descricao").ToString)
        Next
    End Sub
    Private Sub carregagestores()
        Dim xsql As String
        xsql = "select distinct nome from vwcolaboradores where gestor = true   "
        If Not Session("master") = "Sim" Then
            xsql &= " And nrseqgestor = " & Session("idusuario")
        End If
        xsql &= " order by nome"
        tb1 = tab1.conectar(xsql)
        cbogestor.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbogestor.Items.Add(tb1.Rows(x)("nome").ToString)
        Next
    End Sub

    Private Sub btnProcurar_Click(sender As Object, e As EventArgs) Handles btnProcurar.Click

        'If sonumeros(txtano.Text).Length = 0 Then
        '    sm("swal.fire({title: 'Ops!',text: 'O campo ano é de preenchimento obrigatório !',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        Dim xsql As String = "select * from vwrelpad_cursos where 1=1 "
        'Dim xsql As String = "select * from vwpad_cursos where 1=1 "
        If Session("gestor") = "Sim" AndAlso Session("master") = "Não" Then
            xsql &= " and nrseqgestor = " & Session("nrseqcolaborador")

        End If

        If cbogestor.Text <> "" Then
            xsql &= " and nomegestor = '" & cbogestor.Text & "'"
        End If
        If cbocursos.Text <> "" Then
            xsql &= " and descricao = '" & cbocursos.Text & "'"
        End If
        If txtcolaborador.Text <> "" Then
            xsql &= " and nomecolaborador like '%" & tratatexto(txtcolaborador.Text) & "%'"
        End If
        If sonumeros(txtano.Text).Length <> 0 Then
            xsql &= " and anobase = " & sonumeros(txtano.Text)
        End If

        Select Case cbostatus.SelectedItem.Text.ToLower
            Case Is = "Realizados".ToLower
                xsql &= " and concluido = 'Sim' "
            Case Is = "Em aberto".ToLower
                xsql &= " and concluido = 'Não' "

        End Select
        'If optordernome.Checked Then
        '    xsql &= " order by nomecolaborador, descricao"
        'Else
        '    xsql &= " order by descricao, nomecolaborador"
        'End If


        tb1 = tab1.conectar(xsql)
        If tb1.Rows.Count = 0 Then
            sm("swal.fire({title: 'Ops!',text: 'Desculpe, nenhum PAD encontrado com essas infromações !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        grade.DataSource = tb1
        grade.DataBind()

        dgexcel.DataSource = tb1
        dgexcel.DataBind()
    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        Dim lblitem As Label = e.Row.FindControl("lblitem")
        Dim lblconcluidograde As Label = e.Row.FindControl("lblconcluidograde")
        Dim imgconcluidograde As Image = e.Row.FindControl("imgconcluidograde")

        lblitem.Text = e.Row.RowIndex + 1
        imgconcluidograde.Visible = False
        If lblconcluidograde.Text = "Sim" Then
            imgconcluidograde.Visible = True
        End If



    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        Dim xsql As String = "select * from vwrelpad_cursos where 1=1 "
        If Session("gestor") = "Sim" Then
            xsql &= " and nrseqgestor = " & Session("nrseqcolaborador")

        End If

        If cbogestor.Text <> "" Then
            xsql &= " and nomegestor = '" & cbogestor.Text & "'"
        End If
        If cbocursos.Text <> "" Then
            xsql &= " and descricao = '" & cbocursos.Text & "'"
        End If
        If txtcolaborador.Text <> "" Then
            xsql &= " and nomecolaborador like '%" & tratatexto(txtcolaborador.Text) & "%'"
        End If
        If sonumeros(txtano.Text).Length <> 0 Then
            xsql &= " and anobase = " & sonumeros(txtano.Text)
        End If

        Select Case cbostatus.SelectedItem.Text.ToLower
            Case Is = "Realizados".ToLower
                xsql &= " and concluido = 'Sim' "
            Case Is = "Em aberto".ToLower
                xsql &= " and concluido = 'Não' "

        End Select
        If optordernome.Checked Then
            xsql &= " order by nomecolaborador, descricao"
        Else
            xsql &= " order by descricao, nomecolaborador"
        End If
        dgexcel.DataSource = tb1
        dgexcel.DataBind()
        Session("sql") = xsql
        clssessoes.gravarcookie("sql", xsql)
        clssessoes.versessao()
        Dim link As String
        link = "irlink('" & Request.Url.OriginalString.ToString.Replace(Request.Url.PathAndQuery, "") & "/ImprimirRelTreina.aspx');"
        sm(link, link)
    End Sub

    Private Sub dgexcel_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgexcel.ItemDataBound
        If e.Item.ItemIndex < 0 Then Exit Sub

        e.Item.Cells(6).Text = retiraCaracteresEspeciais(e.Item.Cells(6).Text)
        e.Item.Cells(5).Text = retiraCaracteresEspeciais(e.Item.Cells(5).Text)

    End Sub
End Class

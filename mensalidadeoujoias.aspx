﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mensalidadeoujoias.aspx.vb" MasterPageFile="~/MasterPage3.master" Inherits="mensalidadeoujoias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <section class="content" style="height: auto !important; min-height: 0px !important;">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-1 col-xs-12" id="divajuda" runat="server">
                    </div>
                    <!-- ./col -->
                    <!-- ./col -->
                    <!-- ./col -->
                    <div class="col-lg-2 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>
                                    <asp:Label Text="0" runat="server" ID="txtmensalidades" /></h3>
                                <p>MENSALIDADES</p>
                            </div>
                            <div class="icon">
                                <i class="iconify" data-icon="fa-regular:money-bill-alt" data-inline="false"></i>
                            </div>
                            <asp:linkbutton  ID="btnmensalidades" runat="server" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></asp:linkbutton>
                        </div>
                    </div>

                    <div id="divjoiasmenu" runat="server" visible="false">
                        <div class="col-lg-2 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        <asp:Label Text="0" runat="server" ID="txtjoias" /></h3>
                                    <p>JOIAS</p>
                                </div>
                                <div class="icon">
                                    <i class="iconify" data-icon="mdi:diamond-stone" data-inline="false"></i>
                                </div>
                                  <asp:linkbutton  ID="btnjoias" runat="server" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></asp:linkbutton>
                            </div>
                        </div>
                    </div>      
                    <div id="divjoias2menu" runat="server" visible="false">
                        <div class="col-lg-2 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        <asp:Label Text="0" runat="server" ID="txtjoias2" /></h3>
                                    <p>JOIAS 2</p>
                                </div>
                                <div class="icon">
                                    <i class="iconify" data-icon="mdi:diamond-stone" data-inline="false"></i>
                                </div>
                                  <asp:linkbutton  ID="btnjoias2" runat="server" class="small-box-footer">Entrar <i class="fa fa-arrow-circle-right"></i></asp:linkbutton>
                            </div>
                        </div>
                    </div>                    <!-- ./col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="row" runat="server" id="divdemensalidades" visible="false">
                        <asp:GridView ID="grademensalidades" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma mensalidade encontrada !" CssClass="table " GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                <asp:BoundField DataField="nome" HeaderText="Cliente" />
                                <asp:BoundField DataField="statuspg" HeaderText="Status" />
                                <asp:BoundField DataField="mes" HeaderText="Mês" />
                                <asp:BoundField DataField="ano" HeaderText="Ano" />

                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div class="row" id="divdejoias" runat="server" visible="false">
                    <asp:GridView ID="gradejoias" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma joia existente para este cliente !" CssClass="table " GridLines="none" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="matricula" HeaderText="matricula" />
                            <asp:BoundField DataField="nome" HeaderText="Cliente" />
                            <asp:BoundField DataField="statuspg" HeaderText="Status" />
                            <asp:BoundField DataField="mes" HeaderText="Mes" />
                            <asp:BoundField DataField="ano" HeaderText="Ano" />
                        </Columns>
                    </asp:GridView>

                </div>
                <div class="row" id="divdejoias2" runat="server" visible="false">
                    <asp:GridView ID="gradejoias2" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma joia existente para este cliente !" CssClass="table " GridLines="none" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="matricula" HeaderText="matricula" />
                            <asp:BoundField DataField="nome" HeaderText="Cliente" />
                            <asp:BoundField DataField="statuspg" HeaderText="Status" />
                            <asp:BoundField DataField="mes" HeaderText="Mes" />
                            <asp:BoundField DataField="ano" HeaderText="Ano" />
                        </Columns>
                    </asp:GridView>

                </div>
            </section>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


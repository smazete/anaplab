﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="resultados.aspx.vb" Inherits="resultados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>


            <div class="box box-primary " style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
                <div class="box box-header  ">

                    <b>Tabela de Resultados
                    </b>

                </div>
                <div class="box-body ">
                    <div class="row">
                        <div class="col-lg-6 ">
                            <div class="box box-warning  ">
                                <div class="box box-header  ">

                                    <b>Pontuações Individuais
                                    </b>

                                </div>
                                <div class="box-body ">
                                    <div class="row">
                                        <div class="col-lg-4 ">
                                            Descrição
                                  <br />
                                            <asp:TextBox ID="txtdescricao" runat="server" CssClass="form-control "></asp:TextBox>
                                        </div>
                                        <div class="col-lg-4 ">
                                            Pontuação
                                  <br />
                                            <asp:TextBox ID="txtpontua" runat="server" CssClass="form-control "></asp:TextBox>
                                        </div>
                                           <div class="col-lg-4 ">
                                            Pontuação
                                  <br />
                                            <asp:DropDownList ID="cbousarem" runat="server" CssClass="form-control ">
                                                <asp:ListItem >Objetivos</asp:ListItem>
                                                <asp:ListItem >Competências</asp:ListItem>
                                                <asp:ListItem >Ambos</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        </div>
                                    <div class="row">
                                        <div class="col-lg-12 text-center">
                                            Título da legenda
                                            <br />
                                            <asp:TextBox ID="txttitulolegenda" runat="server" CssClass="form-control "></asp:TextBox>
                                        </div>
                                    </div>
                                       <div class="row">
                                        <div class="col-lg-12 text-center">
                                            Texto da legenda
                                            <br />
                                            <asp:TextBox ID="txttextolegenda" TextMode="MultiLine" style="height:10rem;"  runat="server" CssClass="form-control "></asp:TextBox>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-lg-12 text-center">
                                            <br />
                                            <asp:LinkButton ID="btnaddresult" runat="server" CssClass="btn btn-success "><asp:Image runat="server" ImageUrl ="~/img/add.png" style="width:2rem;height:2rem;" />Adicionar</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 ">
                                            <br />
                                            <asp:GridView ID="grade" runat="server" CssClass="table table-hover table-striped" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum colaborador localizado!" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblitemgrade" CssClass="badge "></asp:Label>
                                                            <asp:HiddenField runat="server" ID="hdnrseqgrade" Value='<%#Bind("nrseq")%>' />
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                                    <asp:BoundField DataField="peso" HeaderText="Pontuação" />
                                                     <asp:BoundField DataField="usarem" HeaderText="Usar em" />
                                                    <asp:TemplateField HeaderText="Ações" runat="server">
                                                        <ItemTemplate runat="server">


                                                            <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Deletar">
                                           <i class="fas fa-trash-alt"></i>
                                                            </asp:LinkButton>
                                                               <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Alterar">
                                           <i class="fas fa-edit "></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 ">
                            <div class="box box-success  ">
                                <div class="box box-header  ">

                                    <b>Avaliação Final
                                    </b>

                                </div>
                                <div class="box-body ">
                                    <div class="row">
                                        <div class="col-lg-4 ">
                                        </div>

                                        <div class="col-lg-3 text-center">
                                            Pontuação
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-lg-3 ">
                                            Descrição
                                  <br />
                                            <asp:TextBox ID="txtdescricaofinal" runat="server" CssClass="form-control "></asp:TextBox>
                                            <asp:HiddenField ID="hdnrseqfinal" runat="server" value="0"/>
                                        </div>
                                        <div class="col-lg-2 ">
                                            Inicial
                                  <br />
                                            <asp:TextBox ID="txtpontualcaoini" runat="server" CssClass="form-control "></asp:TextBox>
                                        </div>
                                        <div class="col-lg-2 ">
                                            Final
                                  <br />
                                            <asp:TextBox ID="txtpontualcaofin" runat="server" CssClass="form-control "></asp:TextBox>
                                        </div>
                                          <div class="col-lg-2 ">
                                            Rating %
                                  <br />
                                            <asp:TextBox ID="txtmetas" runat="server" CssClass="form-control "></asp:TextBox>
                                        </div>
                                         <div class="col-lg-3 ">
                                            Pontuação
                                  <br />
                                            <asp:DropDownList ID="cbousaremfinal" runat="server" CssClass="form-control ">
                                                <asp:ListItem >Objetivos</asp:ListItem>
                                                <asp:ListItem >Competências</asp:ListItem>
                                                <asp:ListItem >Ambos</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                       
                                        </div>
                                    <div class="row">
                                        <div class="col-lg-12 text-center">
                                            <br />
                                            <asp:LinkButton ID="btnaddfinal" runat="server" CssClass="btn btn-success "><asp:Image runat="server" ImageUrl ="~/img/add.png" style="width:2rem;height:2rem;" />Adicionar</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 ">
                                            <br />
                                            <asp:GridView ID="gradefinal" runat="server" CssClass="table table-hover table-striped" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum colaborador localizado!" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblitemgrade" CssClass="badge "></asp:Label>
                                                            <asp:HiddenField runat="server" ID="hdnrseqgrade" Value='<%#Bind("nrseq")%>' />
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                                    <asp:BoundField DataField="pesoinicial" HeaderText="Inicial" />
                                                    <asp:BoundField DataField="pesofinal" HeaderText="Final" />
                                                    <asp:BoundField DataField="metageral" HeaderText="Rating %" />
                                                      <asp:BoundField DataField="usarem" HeaderText="Usar em" />
                                                    <asp:TemplateField HeaderText="Ações" runat="server">
                                                        <ItemTemplate runat="server">


                                                            <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Deletar">
                                           <i class="fas fa-trash-alt"></i>
                                                            </asp:LinkButton>
                                                              <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Alterar">
                                           <i class="fas fa-edit "></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>


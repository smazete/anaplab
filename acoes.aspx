﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="acoes.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="acoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Cadastro de Ações Judiciais </b>
        </div>
        <div class="box-body">

            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:HiddenField runat="server" ID="hdnrseq"         />
                    <asp:LinkButton ID="btnnovo" runat="server" CssClass="btn btn-warning "> <i class="fas fa-plus-circle"></i>Novo</asp:LinkButton>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2">
                    <asp:Label Text="Nrseq" runat="server" />
                    <br>
                    <asp:TextBox ID="txtnrseq" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-4">
                    <asp:Label Text="Nome" runat="server" />
                    <br>
                    <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-3">
                    <asp:Label Text="Numero" runat="server" />
                    <br>
                    <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
      
                <div class="col-lg-2">
                    <asp:Label Text="Data" runat="server" />
                    <br>
                    <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdata" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="col-lg-3">
                    <asp:Label Text="Justica" runat="server" />

                    <br>
                    <asp:DropDownList ID="cbojustica" runat="server" CssClass="form-control">
                        <asp:ListItem Value="JUSTIÇA FEDERAL">JUSTIÇA FEDERAL</asp:ListItem>
                        <asp:ListItem Value="TJ AC">TJ AC</asp:ListItem>
                        <asp:ListItem Value="TJ AL">TJ AL</asp:ListItem>
                        <asp:ListItem Value="TJ AP">TJ AP</asp:ListItem>
                        <asp:ListItem Value="TJ AM">TJ AM</asp:ListItem>
                        <asp:ListItem Value="TJ BA">TJ BA</asp:ListItem>
                        <asp:ListItem Value="TJ CE">TJ CE</asp:ListItem>
                        <asp:ListItem Value="TJ DF">TJ DF</asp:ListItem>
                        <asp:ListItem Value="TJ ES">TJ ES</asp:ListItem>
                        <asp:ListItem Value="TJ EX">TJ EX</asp:ListItem>
                        <asp:ListItem Value="TJ GO">TJ GO</asp:ListItem>
                        <asp:ListItem Value="TJ MA">TJ MA</asp:ListItem>
                        <asp:ListItem Value="TJ MT">TJ MT</asp:ListItem>
                        <asp:ListItem Value="TJ MS">TJ MS</asp:ListItem>
                        <asp:ListItem Value="TJ MG">TJ MG</asp:ListItem>
                        <asp:ListItem Value="TJ PA">TJ PA</asp:ListItem>
                        <asp:ListItem Value="TJ PB">TJ PB</asp:ListItem>
                        <asp:ListItem Value="TJ PR">TJ PR</asp:ListItem>
                        <asp:ListItem Value="TJ PE">TJ PE</asp:ListItem>
                        <asp:ListItem Value="TJ PI">TJ PI</asp:ListItem>
                        <asp:ListItem Value="TJ RJ">TJ RJ</asp:ListItem>
                        <asp:ListItem Value="TJ RN">TJ RN</asp:ListItem>
                        <asp:ListItem Value="TJ RS">TJ RS</asp:ListItem>
                        <asp:ListItem Value="TJ RO">TJ RO</asp:ListItem>
                        <asp:ListItem Value="TJ RR">TJ RR</asp:ListItem>
                        <asp:ListItem Value="TJ SC">TJ SC</asp:ListItem>
                        <asp:ListItem Value="TJ SP">TJ SP</asp:ListItem>
                        <asp:ListItem Value="TJ SE">TJ SE</asp:ListItem>
                        <asp:ListItem Value="TJ TO">TJ TO</asp:ListItem>
                        <asp:ListItem Value="TRT RIO">TRT RIO</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-lg-7">
                    <asp:Label Text="Link" runat="server" />
                    <br>
                    <asp:TextBox ID="txtlink" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                   <div class="col-lg-1">
                    <asp:Label Text="particiantes" runat="server" />
                    <br>
                    <asp:TextBox ID="txtqtd" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                </div>
            </div>

            <div class="row" runat="server" div="divdescrifim">
                <div class="col-lg-12">
    <asp:Label Text="Descricao Encerramento" runat="server" />
<br>
<asp:TextBox id="txtdescrifim" runat="server" cssclass="form-control" TextMode="MultiLine" Enabled="false"></asp:TextBox>
</div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <br>
                    <asp:LinkButton ID="btnsalvar" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i>Salvar</asp:LinkButton>
                </div>
            </div>

            <div class="col-lg-4">
                <asp:Label Text="Buscar Por" runat="server" />
                <asp:DropDownList runat="server" CssClass="form-control" ID="cbobusca">
                    <asp:ListItem Text="Codigo" Value="nrseq" />
                    <asp:ListItem Text="Nome" Value="nome" />
                    <asp:ListItem Text="Numero" Value="numero" />
                </asp:DropDownList>
            </div>
            <div class="col-lg-6">
                <br />
                <asp:TextBox runat="server" ID="txtbusca" CssClass="form-control" />
            </div>

            <div class="col-lg-2 text-center">
                <br>
                <asp:LinkButton ID="btnbuscando" runat="server" CssClass="btn btn-primary "> <i class="fa fa-searc"></i> Buscar </asp:LinkButton>
            </div>

            <div class="row">
                <br>

                <asp:GridView ID="gradeacoes" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma doca cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                <div runat="server" id="divcancelar2" >
                                   <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                    </div>     
                                <div runat="server" id="divselecionafim" >

                                   <asp:LinkButton ID="selecionar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="selecionar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                    
                                      <asp:LinkButton ID="nfim" runat="server" CssClass="btn btn-warning btn-xs" CommandName="nfim" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-unlock"></i></asp:LinkButton>

                                    </div>
      
</ItemTemplate>
</asp:TemplateField>
                        
                                <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                     
                                <asp:BoundField DataField="nome" HeaderText="Nome" />
                                <asp:BoundField DataField="numero" HeaderText="Numero" />
                                <asp:BoundField DataField="Justica" HeaderText="Justiça" />
                                <asp:BoundField DataField="data" HeaderText="Data" />
                                <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                <asp:BoundField DataField="finalizada" HeaderText="Finalizar" />
                        <asp:TemplateField HeaderText="Opções">
                            <ItemTemplate>
                                <div runat="server" id="divativar">

                                       <asp:LinkButton ID="encerrar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="encerrar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-thumbs-o-up"></i></asp:LinkButton>
                                
                                 <asp:HyperLink ID="HlFile" runat="server" 
         NavigateUrl='<%# Eval("link") %>'  Target="_blank"> <i class="fa fa-chrome"> </i> </asp:HyperLink>
                                     
                                   <asp:LinkButton ID="cancelar" runat="server" CssClass="btn btn-danger btn-xs" CommandName="cancelar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-close"></i></asp:LinkButton>
                                </div>
                                <div runat="server" id="divcancelado">
                                      <asp:LinkButton ID="ativar" runat="server" CssClass="btn btn-success btn-xs" CommandName="ativar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-check"></i></asp:LinkButton>

                                </div>

                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                </asp:GridView>
            </div>

            </div>
        </div>
</asp:Content>

﻿Imports clssessoes
Imports clsSmart
Imports System.Web.UI.DataVisualization.Charting
Partial Class _Default
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    'Private Sub carregardivs()
    '    divgradeconsultas.Style.Add("display", "none")
    '    divgradegrafconsultas.Style.Remove("display")
    '    carregargraficos(hddataref.Value)
    'End Sub

    Private Sub _Default_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If

        Dim wcdata As Date = data()
        hddataref.Value = wcdata
        lblmesref.Text = MesExtenso(CDate(hddataref.Value).Month) & "/" & CDate(hddataref.Value).Year

        '  carregardivs()
        Exit Sub


        If IsDate(Session("novasenhaem")) Then
            lblnovasenha.Text = "Sua senha expirará em " & FormatDateTime(Session("novasenhaem"))
        End If
        divdados.Style.Add("display", "none")
        divdesbloqueio.Style.Add("display", "none")
        If exibirsolicitacoes() > 0 AndAlso Session("usuariomaster") = "Sim" Then
            divdesbloqueio.Style.Remove("display")

        End If


        If sonumeros(Session("usuario")).Length > 0 Then

            Dim xcol As New clscolaboradores
            xcol.Procurarpor = "matricula"
            xcol.Matricula = Session("usuario")

            If xcol.carregar Then
                txtnome.Text = xcol.Nome
                txtemail.Text = xcol.Email
            End If
            If sonumeros(xcol.Nome).Count > 1 OrElse xcol.Email = "" Then
                divdados.Style.Remove("display")


            End If

        End If

    End Sub


    Private Sub carregargraficos(mes As DateTime)


        'Dim tbg As New Data.DataTable, tabg As New clsBanco

        'Dim tbgraf As New Data.DataTable
        'tbgraf.Columns.Add("status")
        'tbgraf.Columns.Add("valor")

        ''baixado e nao foi baixa
        ''pago e nao foi pago
        'tbgraf.Rows.Clear()
        'tbg = tabg.conectar("select sum(if(atendido = true, 1,0)) as atendidas,  sum(if(cancelado,1,0)) as canceladas, sum(if(atendido = false and cancelado = false and data < now(),1,0)) as ausencias from tbagendamedica  where ativo = true and month(data) = " & mes.Month & " and year(data) = " & mes.Year)
        'tbgraf.Rows.Add()
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Atendidos"
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("atendidas").ToString
        'tbgraf.Rows.Add()
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Cancelados"
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("canceladas").ToString
        'tbgraf.Rows.Add()
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Ausências"
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("ausencias").ToString
        'grafconsultas.DataSource = tbgraf
        'grafconsultas.DataBind()

        'tbgraf.Columns.Clear()

        'tbgraf.Columns.Add("status")
        'tbgraf.Columns.Add("valor")

        ''conciliado
        ''emitido
        'tbgraf.Rows.Clear()
        'tbg = tabg.conectar("select sum(if(emitido = true, 1,0)) as emitidos,  sum(if(conciliado,1,0)) as conciliados from tbguias where ativo = true and month(data) = " & mes.Month & " and year(data) = " & mes.Year)
        'tbgraf.Rows.Add()
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Emitidos"
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("emitidos").ToString
        'tbgraf.Rows.Add()
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Conciliados"
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("conciliados").ToString
        ''tbgraf.Rows.Add()
        ''tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Ausências"
        ''tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("ausencias").ToString
        'grafprocedimentos.DataSource = tbgraf
        'grafprocedimentos.DataBind()

        'tbgraf.Columns.Clear()

        'tbgraf.Columns.Add("status")
        'tbgraf.Columns.Add("valor")

        ''debitos e creditos
        'tbgraf.Rows.Clear()
        'tbg = tabg.conectar("select sum(if(operacao = 'C', 1,0)) as creditos,sum(if(operacao<>'C' ,1,0)) as debitos from tbcaixasdth  where ativo = true and month(dtcad) = " & mes.Month & " and year(dtcad) = " & mes.Year)
        'tbgraf.Rows.Add()
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Créditos"
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("creditos").ToString
        'tbgraf.Rows.Add()
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Débitos"
        'tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("debitos").ToString
        ''tbgraf.Rows.Add()
        ''tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = "Ausências"
        ''tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(0)("ausencias").ToString
        'grafcaixas.DataSource = tbgraf
        'grafcaixas.DataBind()


        'tbgraf.Rows.Clear()
        'tbg = tabg.conectar("Select   descricaoresultado, count(*) As total FROM vwpad where ativo = true and anobase = " & ano & " group by descricaoresultado")
        'For x As Integer = 0 To tbg.Rows.Count - 1
        '    tbgraf.Rows.Add()
        '    If tbg.Rows(0)("descricaoresultado").ToString = "" Then
        '        tbg.Rows(0)("descricaoresultado") = "Sem Resultados"
        '    End If
        '    tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = tbg.Rows(x)("descricaoresultado").ToString
        '    tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(x)("total").ToString
        'Next




        'tbg = tabg.conectar("Select   nomegestor as nomecolaborador, pontosresultado, round(( select if(count(*) > 0 , sum(pontosresultado)/count(*) ,0) from vwpad group by anobase, ativo having  ativo = true  and anobase = " & ano & "),2) as media FROM  vwpad where ativo = true   and anobase = " & ano & " group by nomegestor")
        ''For x As Integer = 0 To tbg.Rows.Count - 1
        ''    tbgraf.Rows.Add()
        ''    If tbg.Rows(0)("descricaoresultado").ToString = "" Then
        ''        tbg.Rows(0)("descricaoresultado") = "Sem Resultados"
        ''    End If
        ''    tbgraf.Rows(tbgraf.Rows.Count - 1)("status") = tbg.Rows(x)("descricaoresultado").ToString
        ''    tbgraf.Rows(tbgraf.Rows.Count - 1)("valor") = tbg.Rows(x)("total").ToString
        ''Next
        'grafmedia.Titles(0).Text = "Média das pontuações das equipes"
        'If tbg.Rows.Count > 0 Then
        '    Dim novotitulo As New DataVisualization.Charting.Title
        '    novotitulo.Text = "Média das notas das equipes:" & tbg.Rows(0)("media") & " pontos"
        '    grafmedia.Titles.Add(novotitulo)
        'End If

        'grafmedia.DataSource = tbg
        'grafmedia.DataBind()


    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub btnsalvarinformacoes_Click(sender As Object, e As EventArgs) Handles btnsalvarinformacoes.Click

        Dim xcol As New clscolaboradores
        xcol.Nrseq = Session("idusuario")
        xcol.Nome = txtnome.Text
        xcol.Email = txtemail.Text
        xcol.Gestor = True
        xcol.Horista = False
        If Not xcol.Salvar Then
            sm("swal.fire({title: 'Ops!',text: 'Algo deu errado: " & xcol.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

        Else
            sm("swal.fire({title: 'Ótimo!',text: 'Obrigado! Atualizamos suas informações !',type: 'success',confirmButtonText: 'OK'})", "swal")

            divdados.Style.Add("display", "none")
        End If
    End Sub

    Private Sub btnaqui_Click(sender As Object, e As EventArgs) Handles btnaqui.Click
        Response.Redirect("perfil.aspx")
    End Sub

    Private Sub btnMesAnterior_Click(sender As Object, e As EventArgs) Handles btnMesAnterior.Click
        If Not IsDate(hddataref.Value) Then
            hddataref.Value = data()
        End If
        hddataref.Value = CDate(hddataref.Value).AddMonths(-1)

        lblmesref.Text = MesExtenso(CDate(hddataref.Value).Month) & "/" & CDate(hddataref.Value).Year
        carregargraficos(hddataref.Value)
    End Sub

    Private Sub btnProximoMes_Click(sender As Object, e As EventArgs) Handles btnProximoMes.Click
        If Not IsDate(hddataref.Value) Then hddataref.Value = data()
        hddataref.Value = CDate(hddataref.Value).AddMonths(1)
        lblmesref.Text = MesExtenso(CDate(hddataref.Value).Month) & "/" & CDate(hddataref.Value).Year
        carregargraficos(hddataref.Value)
    End Sub

    'Private Sub chartstatusgeral1_DataBound(sender As Object, e As EventArgs) Handles chartstatusgeral1.DataBound
    '    ' chartstatusgeral1.Series(0).LegendText = chartstatusgeral1.Series(0).XValueMember

    '    For x As Integer = 0 To chartstatusgeral1.Series(0).Points.Count - 1

    '        chartstatusgeral1.Series(0).Points(x).LabelToolTip = FormatNumber(chartstatusgeral1.Series(0).Points(x).YValues(0), 0)
    '        chartstatusgeral1.Series(0).Points(x).Label = FormatNumber(chartstatusgeral1.Series(0).Points(x).YValues(0), 0)
    '        chartstatusgeral1.Series(0).Points(x).LegendText = chartstatusgeral1.Series(0).Points(x).AxisLabel

    '    Next

    'End Sub

    Private Sub grafmedia_Click(sender As Object, e As ImageMapEventArgs) Handles grafmedia.Click

    End Sub

    Private Sub btnpainelgestor_Click(sender As Object, e As EventArgs) Handles btnpainelgestor.Click
        Response.Redirect("painelgestor.aspx")
    End Sub

    Private Sub _Default_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete



    End Sub

    Private Sub gradesol_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradesol.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradesol.Rows(index)
        Dim nrseqsol As HiddenField = row.FindControl("nrseqsol")
        '    Dim hdnrseqgrade As HiddenField = row.FindControl("hdnrseqgrade")
        Dim xpad As New clspad
        Select Case e.CommandName.ToLower
            Case Is = "feito"
                Dim xsol As New clssolicitacoes
                xsol.Nrseq = nrseqsol.Value
                xsol.Nrsequsuariorespondido = Session("idusuario")
                If Not xsol.baixar Then
                    sm("swal.fire({title: 'Ops!',text: 'Algo deu errado: " & xsol.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                    Exit Sub
                End If
                exibirsolicitacoes()


        End Select
    End Sub
    Private Function exibirsolicitacoes() As Integer

        tb1 = tab1.conectar("select * from vwsolicitacoes where " & IIf(chkexibirliberados.Checked, " 1=1 ", " liberado = false  ") & " order by nrseq desc")
        gradesol.DataSource = tb1
        gradesol.DataBind()
        Return tb1.Rows.Count
    End Function
    Private Sub gradesol_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradesol.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.ForeColor = System.Drawing.Color.White
            Dim lblliberadograde As Label = e.Row.FindControl("lblliberadograde")
            Dim divliberado As Object = e.Row.FindControl("divliberado")
            If lblliberadograde.Text = "" Then
                e.Row.BackColor = System.Drawing.Color.Red
                divliberado.style.add("display", "none")
            Else
                e.Row.BackColor = System.Drawing.Color.Green
                divliberado.style.remove("display")
            End If
        End If
    End Sub

    Private Sub chkexibirliberados_CheckedChanged(sender As Object, e As EventArgs) Handles chkexibirliberados.CheckedChanged
        exibirsolicitacoes()
    End Sub

    Private Sub grafconsultas_DataBound(sender As Object, e As EventArgs) Handles grafconsultas.DataBound
        For x As Integer = 0 To grafconsultas.Series(0).Points.Count - 1
            grafconsultas.Series(0).Points(x).PostBackValue = grafconsultas.Series(0).Points(x).AxisLabel
        Next
    End Sub
    Private Sub grafprocedimentos_DataBound(sender As Object, e As EventArgs) Handles grafprocedimentos.DataBound
        For x As Integer = 0 To grafprocedimentos.Series(0).Points.Count - 1
            grafprocedimentos.Series(0).Points(x).PostBackValue = grafprocedimentos.Series(0).Points(x).AxisLabel
        Next
    End Sub
    Private Sub grafcaixas_DataBound(sender As Object, e As EventArgs) Handles grafcaixas.DataBound
        For x As Integer = 0 To grafcaixas.Series(0).Points.Count - 1
            grafcaixas.Series(0).Points(x).PostBackValue = grafcaixas.Series(0).Points(x).AxisLabel
        Next
    End Sub

    Private Sub grafconsultas_Click(sender As Object, e As ImageMapEventArgs) Handles grafconsultas.Click
        sm("swal.fire({title: 'Ops!',text: 'Voce clicou em : " & e.PostBackValue & "',type: 'error',confirmButtonText: 'OK'})", "swal")
    End Sub
    Private Sub grafprocedimentos_Click(sender As Object, e As ImageMapEventArgs) Handles grafprocedimentos.Click
        sm("swal.fire({title: 'Ops!',text: 'Voce clicou em : " & e.PostBackValue & "',type: 'error',confirmButtonText: 'OK'})", "swal")
    End Sub
    Private Sub grafcaixas_Click(sender As Object, e As ImageMapEventArgs) Handles grafcaixas.Click
        sm("swal.fire({title: 'Ops!',text: 'Voce clicou em : " & e.PostBackValue & "',type: 'error',confirmButtonText: 'OK'})", "swal")
    End Sub

    '    Dim tb1 As New Data.DataTable
    '    Dim tab1 As New clsBanco
    '    Private Sub carregagraficos(dataref As Date)
    '        lblmesref.Text = MesExtenso(dataref.Month) & " / " & dataref.Year()
    '        lblpermissao.Text = Session("permissao")
    '        Dim tb1 As New Data.DataTable
    '        Dim tab1 As New clsBanco
    '        Dim tb2 As New Data.DataTable
    '        Dim tab2 As New clsBanco
    '        Dim tb3 As New Data.DataTable
    '        Dim tab3 As New clsBanco
    '        tb1 = tab1.conectar("select * from tbpermissoes where descricao = '" & Session("permissao") & "'")
    '        If tb1.Rows.Count <> 0 Then
    '            If tb1.Rows(0)("vertodas").ToString = "True" OrElse tb1.Rows(0)("vertodas").ToString = "1" Then
    '                Session("filtropermissao") = "1=1"
    '                tb1 = tab1.conectar("select status,count(*) as total FROM tblotes where month(dtcad) = " & dataref.Month & " and year(dtcad) = " & dataref.Year & " group by status, ativo having not status is null and ativo = True order by status")
    '                tb2 = tab2.conectar("select nomecliente,count(*) as total FROM vwlotes where month(dtcad) = " & dataref.Month & " and year(dtcad) = " & dataref.Year & " group by nomecliente, nrseqtransportadora, ativo having not nomecliente is null  and ativo = True  order by nomecliente")
    '                '    tb3 = tab3.conectar("select status,count(*) as total FROM tblotes where (month(dtcad) >= " & dataref.AddMonths(-12).Month & " and year(dtcad) >= " & dataref.AddMonths(-12).Year & ") and (month(dtcad) <= " & dataref.Month & " and year(dtcad) <= " & dataref.Year & ") group by status, nrseqtransportadora, ativo having not status is null  and ativo = True  order by dtcad")
    '            Else
    '                Session("filtropermissao") = ""
    '                tb1 = tab1.conectar("select status,count(*) as total FROM tblotes where month(dtcad) = " & dataref.Month & " and year(dtcad) = " & dataref.Year & " group by status, nrseqtransportadora, ativo having not status is null and nrseqtransportadora = " & Session("usertransp") & " and ativo = True  order by status")
    '                tb2 = tab2.conectar("select nomecliente,count(*) as total FROM vwlotes where month(dtcad) = " & dataref.Month & " and year(dtcad) = " & dataref.Year & " group by nomecliente, nrseqtransportadora, ativo having not nomecliente is null and nrseqtransportadora = " & Session("usertransp") & " and ativo = True  order by nomecliente")
    '                '  tb3 = tab3.conectar("select status,count(*) as total FROM tblotes where (month(dtcad) >= " & dataref.AddMonths(-12).Month & " and year(dtcad) >= " & dataref.AddMonths(-12).Year & ") and (month(dtcad) <= " & dataref.Month & " and year(dtcad) <= " & dataref.Year & ") group by status, nrseqtransportadora, ativo having not status is null  and ativo = True  order by status")
    '            End If
    '        End If

    '        ' tb1 = tab1.conectar("select status,count(*) as total FROM tblotes group by status having not status is null " & IIf(Session("permissao") = "", "", "") & " order by status")

    '        Chart3.Series("Default").LegendText = "#AXISLABEL"

    '        Chart3.Series("Default")("PieLabelStyle") = "Outside"
    '        Chart3.Series("Default")("3DLabelLineSize") = "200"
    '        Chart3.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True
    '        Chart3.Titles(0).Text = "Status - " & MesExtenso(dataref.Month) & "/" & dataref.Year
    '        Chart3.Titles(0).Alignment = Drawing.ContentAlignment.MiddleCenter
    '        Chart3.Titles(0).Font = New Drawing.Font("Arial", 16, Drawing.FontStyle.Bold)
    '        Chart3.DataSource = tb1
    '        Chart3.DataBind()




    '        grafclientes.Series("Default").LegendText = "#AXISLABEL"

    '        grafclientes.Series("Default")("PieLabelStyle") = "Outside"
    '        grafclientes.Series("Default")("3DLabelLineSize") = "200"
    '        grafclientes.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True
    '        grafclientes.Titles(0).Text = "Clientes - " & MesExtenso(dataref.Month) & "/" & dataref.Year
    '        grafclientes.Titles(0).Alignment = Drawing.ContentAlignment.MiddleCenter
    '        grafclientes.Titles(0).Font = New Drawing.Font("Arial", 16, Drawing.FontStyle.Bold)
    '        grafclientes.DataSource = tb2
    '        grafclientes.DataBind()


    '        Dim tbgraf As New Data.DataTable
    '        Dim tabgraf As New clsBanco

    '        tbgraf = tabgraf.conectar("select concat( month(dtcad),'/', year(dtcad)) as data, sum(case when (status = 'Finalizado') then 1 else 0 end) as finalizado, sum(case when (status = 'Em análise') then 1 else 0 end) as abertos from tblotes where ativo = true group by  month(dtcad), year(dtcad) order by  year(dtcad), month(dtcad)")
    '        Chart1.DataSource = tbgraf
    '        Chart1.DataBind()

    '        'Dim tbgraf As New Data.DataTable
    '        'Dim tabgraf As New clsBanco
    '        'Dim tbgraf1 As New Data.DataTable
    '        'tbgraf = tabgraf.conectar("select   status, ativo, nomecliente,  month(dtcad), year(dtcad) from vwlotes where dtcad >= '2018/01/10' and  dtcad <= '2019/01/10' and ativo = true where status='Finalizado' group by status, nomecliente, ativo, month(dtcad), year(dtcad) order by dtcad, nomecliente")

    '        'Dim wcmesanoinicial As String = zeros(CDate(dataref).AddMonths(-1).Year, 4) & "/" & zeros(CDate(dataref).AddMonths(-1).Month, 2)


    '        ''Date = CDate("01/" & zeros(CDate(txtdtinicial.Text).AddMonths(-1).Month, 2) & "/" & zeros(CDate(txtdtinicial.Text).AddMonths(-1).Year, 4))
    '        'For y As Integer = 0 To tbgraf.Rows.Count - 1
    '        '    tbgraf1.Columns.Add("valormedia" & y)
    '        '    tbgraf1.Columns("valormedia" & y).DataType = System.Type.GetType("System.Decimal")
    '        'Next
    '        'tbgraf1.Rows.Add()

    '        'tbgraf1.Rows(tbgraf1.Rows.Count - 1)("MesAno") = wcmesanoinicial
    '        'For y As Integer = 0 To tbgraf.Rows.Count - 1
    '        '    tbgraf1.Rows(tbgraf1.Rows.Count - 1)("valormedia" & y) = FormatNumber(0, 2)
    '        'Next
    '        'For y As Integer = 0 To tbgraf.Rows.Count - 1
    '        '    'tbgraf1.Columns.Add("valormedia" & y)
    '        '    'tbgraf1.Columns("valormedia" & y).DataType = System.Type.GetType("System.Decimal")
    '        '    Dim novaserie As New DataVisualization.Charting.Series
    '        '    novaserie.ChartArea = "ChartArea1"
    '        '    novaserie.Font = New Drawing.Font("Microsoft Sans Serif", 12)
    '        '    novaserie.Name = "Default" & y
    '        '    novaserie.IsValueShownAsLabel = True
    '        '    novaserie.XValueMember = "MesAno"
    '        '    novaserie.YValueMembers = "valormedia" & y
    '        '    novaserie.LegendText = tbgraf.Rows(y)("nomecliente").ToString
    '        '    novaserie.ChartType = DataVisualization.Charting.SeriesChartType.FastLine
    '        '    novaserie.IsValueShownAsLabel = True
    '        '    ' Chart2.ChartAreas("ChartArea1").Area3DStyle.Enable3D = True
    '        '    Chart2.Series.Add(novaserie)


    '        '    For h As Integer = 0 To DateDiff(DateInterval.Month, CDate(dataref).AddMonths(-1), CDate(dataref)) - 1
    '        '        For x As Integer = 0 To tbgraf.Rows.Count - 1
    '        '            If tbgeral.Rows(y)("questao").ToString <> tbquestoes.Rows(x)("questao").ToString Then
    '        '                Continue For
    '        '            End If
    '        '            Dim novadata As String = zeros(CDate(txtdtinicial.Text).AddMonths(h).Year, 4) & "/" & zeros(CDate(txtdtinicial.Text).AddMonths(h).Month, 2)


    '        '            Dim tbacha As Data.DataRow()
    '        '            Dim wcvalor As Decimal = 0
    '        '            Dim existedata As Boolean = False
    '        '            tbacha = tbquestoes.Select(" mesano = '" & novadata & "' and questao = '" & tbgeral.Rows(y)("questao").ToString & "'")
    '        '            If tbacha.Count > 0 Then

    '        '                wcvalor = tbacha(0)("media").ToString

    '        '                'tbgraf1.Rows(tbgraf1.Rows.Count - 1)("MesAno") = novadata
    '        '                ''tbgraf1.Rows(tbgraf1.Rows.Count - 1)("mes") = novadata.Month
    '        '                ''tbgraf1.Rows(tbgraf1.Rows.Count - 1)("ano") = novadata.Year
    '        '                ''tbgraf1.Rows(tbgraf1.Rows.Count - 1)("dia") = novadata.Day
    '        '                'tbgraf1.Rows(tbgraf1.Rows.Count - 1)("valormedia" & y) += FormatNumber(tbacha(0)("media").ToString, 2)

    '        '            End If
    '        '            For w As Integer = 0 To tbgraf1.Rows.Count - 1
    '        '                If tbgraf1.Rows(w)("MesAno").ToString = novadata Then
    '        '                    tbgraf1.Rows(w)("valormedia" & y) = FormatNumber(wcvalor, 2)
    '        '                    existedata = True
    '        '                End If
    '        '            Next
    '        '            If Not existedata Then
    '        '                tbgraf1.Rows.Add()
    '        '                tbgraf1.Rows(tbgraf1.Rows.Count - 1)("MesAno") = novadata
    '        '                tbgraf1.Rows(tbgraf1.Rows.Count - 1)("valormedia" & y) = FormatNumber(wcvalor, 2)
    '        '            End If
    '        '        Next
    '        '    Next
    '        'Next
    '        'tbgraf1.DefaultView.Sort = "MesAno"
    '        'tbgraf1 = tbgraf1.DefaultView.ToTable()


    '        'Chart1.Legends(Chart2.Legends.Count - 1).Font = New Drawing.Font("Arial", 12, Drawing.FontStyle.Regular)
    '        'Chart1.DataSource = tbgraf1
    '        'Chart1.DataBind()

    '    End Sub
    '    Private Sub _Default_Load(sender As Object, e As EventArgs) Handles Me.Load
    '        If IsPostBack Then Exit Sub

    '        Dim tb2 As New Data.DataTable, tbsal As New Data.DataTable 
    '        Dim tab2 As New clsBanco, tabsal As New clsBanco 

    '        tb2 = tab2.conectar("select * from tbusuarios")

    '        tb1 = tab1.conectar("select * from tblotes where nrsequsercad is null")
    '        For x As Integer = 0 To tb1.Rows.Count - 1
    '            Dim tbprox As Data.DataRow()
    '            tbprox = tb2.Select(" usuario = '" & tb1.Rows(x)("usercad").ToString & "'")
    '            If tbprox.Count > 0 Then
    '                tbsal = tabsal.IncluirAlterarDados("update tblotes set nrsequsercad = " & tbprox(0)("nrseq").ToString & " where nrseq = " & tb1.Rows(x)("nrseq").ToString)
    '            End If
    '        Next

    '        versessao()

    '        hddataref.value = data()
    '        carregagraficos(hddataref.value)

    '    End Sub

    '    Private Sub btnMesAnterior_Click(sender As Object, e As EventArgs) Handles btnMesAnterior.Click
    '        If Not IsDate(hddataref.Value) Then hddataref.Value = data()
    '        hddataref.Value = CDate(hddataref.Value).AddMonths(-1)
    '        carregagraficos(hddataref.Value)
    '    End Sub

    '    Private Sub btnProximoMes_Click(sender As Object, e As EventArgs) Handles btnProximoMes.Click
    '        If Not IsDate(hddataref.Value) Then hddataref.Value = data()
    '        hddataref.Value = CDate(hddataref.Value).AddMonths(1)
    '        carregagraficos(hddataref.Value)
    '    End Sub
End Class

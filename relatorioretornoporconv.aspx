﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="relatorioretornoporconv.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="relatorioretornoporconv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="margem">
        <br />
        <div class="container-fluid">
            <div class="box box-primary">
                <div class="box-header">
                    Relatório Retorno Por Plano
                </div>
                <div class="box-body">

                    <asp:UpdatePanel ID="update1" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <asp:Label runat="server" for="txtdtinicial">Data Inicial</asp:Label>
                                            <asp:TextBox runat="server" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdtinicial" ClientIDMode="Static" />
                                            <asp:Label runat="server" for="txtdtinicial">Data Final</asp:Label>
                                            <asp:TextBox runat="server" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdtfinal" ClientIDMode="Static" />
                                        </div>
                                        <div class="col-lg-6">
                                            <asp:Label runat="server" for="ddlPlano">Plano</asp:Label>
                                            <asp:DropDownList ID="ddlPlano" CssClass="form-control" runat="server"></asp:DropDownList>

                                            <asp:Label runat="server" for="ddlConvenio">Convênio</asp:Label>
                                            <asp:DropDownList ID="ddlConvenio" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-lg-2 fix-check">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <asp:CheckBox Text="Somente Retorno" CssClass="checkbox-inline" ID="chksomenteretorno" runat="server"></asp:CheckBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <asp:CheckBox Text="Exibir Receita" CssClass="checkbox-inline" ID="chkExibirReceita" runat="server" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <asp:CheckBox Text="Listar Paciente" CssClass="checkbox-inline" ID="chkListarPaciente" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--             <div class="row">
                                    
                                        <div class="col-lg-2">
                                            <div class="container-fluid">
                                                <div class="box box-primary">
                                                    <div class="box-header center-block">
                                                        <center>Exibir Procedimentos</center>
                                                    </div>
                                                    <div class="box-body">
                                                        <asp:RadioButtonList ID="rblProcedimentos" runat="server">
                                                            <asp:ListItem Text="Todos" Value="1" Selected="true"></asp:ListItem>
                                                            <asp:ListItem Text="Atendidos" Value="2" />
                                                            <asp:ListItem Text="Cancelados" Value="3" />
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <div class="row text-align-center">
                        <asp:UpdatePanel ID="upBtnSalvar" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:Button runat="server" CssClass="btn btn-primary btn-lg" ID="btnImprimir" Text="Imprimir" AutoPostBack="true" />
                                    </div>
                                </div>
                                
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.maskMoney.min.js"></script>
    <script type="text/javascript">
        $(function () {
            // Mascara


            $(".moeda").maskMoney()
            $(".moedaGdv").maskMoney({ prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: true, allowZero: true });
            //  $("#txtcep").mask("99999-999");
        });

    </script>
</asp:Content>

﻿Imports System.IO
Imports clsSmart

Partial Class mensalidades

    Inherits System.Web.UI.Page

    Private Sub mensalidades_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If
        abastecer()

    End Sub


    Private Sub abastecer()

        Dim dateInMay As New System.DateTime(1993, 5, 31, 12, 14, 0)

        txtdtbaixa.Text = FormatDateTime(hoje())

    End Sub
    Private Sub limpar()
        txtnrseq.Text = ""
        txtnrseqcliente.Text = ""
        txtmes.Text = ""
        txtano.Text = ""
        txtstatuspg.Text = ""
        txtfrmpg.Text = ""
        txtdtpg.Text = ""
        txtstatusfiliacao.Text = ""
        txtdtemissao.Text = ""
        txtdtvencimente.Text = ""
        '   txtarquivo.Text = ""
        txtpago.Text = ""
        txtautomatico.Text = ""
        '  txtativo.text = ""
        '  txtuserexclui.text = ""
        '  txtdtexclui.text = ""
        '  txtuseraltera.text = ""
        '  txtdtaltera.text = ""
        '  txtusercad.text = ""
        '  txtdtcad.text = ""
        txtsegundavia.Text = ""
        txtestornado.Text = ""
    End Sub


    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click

        Dim xMensalidade As New clsmensalidades
        xMensalidade.Nrseq = txtnrseq.Text
        xMensalidade.nrseqcliente = txtnrseqcliente.Text
        xMensalidade.mes = txtmes.Text
        xMensalidade.ano = txtano.Text
        xMensalidade.statuspg = txtstatuspg.Text
        xMensalidade.frmpg = txtfrmpg.Text
        xMensalidade.dtpg = txtdtpg.Text
        xMensalidade.statusfiliacao = txtstatusfiliacao.Text
        xMensalidade.dtemissao = txtdtemissao.Text
        xMensalidade.dtvencimente = txtdtvencimente.Text
        ' xMensalidade.Arquivo = txtarquivo.Text
        xMensalidade.pago = txtpago.Text
        xMensalidade.automatico = txtautomatico.Text
        ' xMensalidade.Ativo = txtativo.text
        ' xMensalidade.userexclui = txtuserexclui.text
        ' xMensalidade.dtexclui = txtdtexclui.text
        ' xMensalidade.useraltera = txtuseraltera.text
        ' xMensalidade.dtaltera = txtdtaltera.text
        ' xMensalidade.Usercad = txtusercad.text
        ' xMensalidade.Dtcad = txtdtcad.text
        ' xMensalidade.segundavia = txtsegundavia.Text
        xMensalidade.Estornado = txtestornado.Text


        If Not xMensalidade.salvar Then
            sm("swal({title: 'Atenção!',text: 'Não foi possivel alterar',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        limpar()
        habilitar(False)
        ' Fim do salvar

    End Sub


    Public Sub carregaassociados(Optional exibirinativos As Boolean = False)
        'Dim xempresas As New clsEmpresas
        Dim xAssociados As New clsAssociado
        Dim consulta As String

        consulta = ""

        If txtbusca.Text = "" Then
            consulta = ""
            sm("swal({title: 'Error!',text: 'Essa Busca pode demorar',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        Else
            consulta = "where " & cbobusca.SelectedValue & " = '" & txtbusca.Text & "'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbAssociados " & consulta & "")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        gradeassociados.DataSource = tb1
        gradeassociados.DataBind()

    End Sub
    ' Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click
    '     carregaassociados()
    '  End Sub



    Private Sub grademensalidade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grademensalidade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grademensalidade.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "visualizar" Then
            '    carregarNoFormulario(nrseq.Value)
            habilitar(True)
            '  txtidentificador.Enabled = False

            Dim xMensalidade As New clsmensalidades

            xMensalidade.Nrseq = nrseq.Value



            If Not xMensalidade.procurar() Then
                sm("swal({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            txtnrseq.Text = xMensalidade.Nrseq
            txtnrseqcliente.Text = xMensalidade.Nrseqcliente
            txtmes.Text = xMensalidade.Mes
            txtano.Text = xMensalidade.Ano
            txtstatuspg.Text = xMensalidade.Statuspg
            txtfrmpg.Text = xMensalidade.Frmpg
            txtdtpg.Text = formatadatamysql(xMensalidade.Dtpg, True, True)
            txtstatusfiliacao.Text = xMensalidade.Statusfiliacao
            txtdtemissao.Text = formatadatamysql(xMensalidade.Dtemissao, True, True)
            txtdtvencimente.Text = formatadatamysql(xMensalidade.Dtvencimente, True, True)
            '  txtarquivo.Text = xMensalidade.Arquivo
            txtpago.Text = xMensalidade.Pago
            txtautomatico.Text = xMensalidade.Automatico

            ' dados historico

            'txtdtcad.Text = formatadatamysql(xAcoes.Dtcad, True, True)
            'txtusercad.Text = xAcoes.Usercad

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Carregada ',  showConfirmButton: false,  timer: 1200})")
            carregamensalidades()


        End If

        If e.CommandName = "excluir" Then
            Dim xMensalidade As New clsmensalidades

            xMensalidade.Nrseq = nrseq.Value


            If Not xMensalidade.excluir() Then
                sm("swal({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            If xMensalidade.Ativo = "1" Then

                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Reativada com Sucesso',  showConfirmButton: false,  timer: 2000})")
                carregamensalidades()
            Else
                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Cancelado com Sucesso',  showConfirmButton: false,  timer: 2000})")

                carregamensalidades()
            End If


        End If

        If e.CommandName = "aberto" Then
            Dim xMensalidade As New clsmensalidades

            Dim valor As HiddenField = row.FindControl("hdvalor")
            xMensalidade.Nrseq = nrseq.Value
            xMensalidade.Valorpg = valor.Value

            If Not xMensalidade.aberto() Then
                sm("swal({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade em Aberto',  showConfirmButton: false,  timer: 2000})")

            carregamensalidades()


        End If

        If e.CommandName = "pagar" Then
            Dim xMensalidade As New clsmensalidades

            Dim valor As HiddenField = row.FindControl("hdvalor")
            xMensalidade.Nrseq = nrseq.Value
            xMensalidade.Valorpg = valor.Value

            If txtdtbaixa.Text <> "" Then
                xMensalidade.Dtpg = txtdtbaixa.Text
            End If

            If Not xMensalidade.pagar() Then
                sm("swal({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Paga',  showConfirmButton: false,  timer: 2000})")

            carregamensalidades()


        End If

        If e.CommandName = "estornar" Then
            Dim xMensalidade As New clsmensalidades

            xMensalidade.Nrseq = nrseq.Value


            If Not xMensalidade.estornar() Then
                sm("swal({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Estorno Realizado com Sucesso',  showConfirmButton: false,  timer: 2000})")

            carregamensalidades()

        End If
        If e.CommandName = "isento" Then
            Dim xMensalidade As New clsmensalidades

            xMensalidade.Nrseq = nrseq.Value


            If Not xMensalidade.isento() Then
                sm("Swal.fire({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Isenção Realizada com Sucesso',  showConfirmButton: false,  timer: 2000})")

            carregamensalidades()

        End If
    End Sub

    Private Sub grademensalidade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grademensalidade.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(4).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue
        Else
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If
        'controle mes 
        If e.Row.Cells(5).Text = "1" Then
            e.Row.Cells(5).Text = "Janeiro"
        ElseIf e.Row.Cells(5).Text = "2" Then
            e.Row.Cells(5).Text = "Fevereiro"
        ElseIf e.Row.Cells(5).Text = "3" Then
            e.Row.Cells(5).Text = "Março"
        ElseIf e.Row.Cells(5).Text = "4" Then
            e.Row.Cells(5).Text = "Abril"
        ElseIf e.Row.Cells(5).Text = "5" Then
            e.Row.Cells(5).Text = "Maio"
        ElseIf e.Row.Cells(5).Text = "6" Then
            e.Row.Cells(5).Text = "Junho"
        ElseIf e.Row.Cells(5).Text = "7" Then
            e.Row.Cells(5).Text = "Julho"
        ElseIf e.Row.Cells(5).Text = "8" Then
            e.Row.Cells(5).Text = "Agosto"
        ElseIf e.Row.Cells(5).Text = "9" Then
            e.Row.Cells(5).Text = "Setembro"
        ElseIf e.Row.Cells(5).Text = "10" Then
            e.Row.Cells(5).Text = "Outubro"
        ElseIf e.Row.Cells(5).Text = "11" Then
            e.Row.Cells(5).Text = "Novembro"
        ElseIf e.Row.Cells(5).Text = "12" Then
            e.Row.Cells(5).Text = "Dezembro"

        End If


        If e.Row.Cells(4).Text = "0" Then
            e.Row.Cells(4).Text = "Aberto"
        ElseIf e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Pago"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Estornado"
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.Cells(4).Text = "Isento"
        End If



        If e.Row.Cells(3).Text = "0" Then
            '  e.Row.Cells(7).Text = "Ativar"
            div.Visible = False
            texto.Visible = True
        Else
            div.Visible = True
            texto.Visible = False
        End If


        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If


    End Sub



    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

    Private Sub habilitar(valor As Boolean)

        txtnrseq.Enabled = valor
        txtnrseqcliente.Enabled = valor
        txtmes.Enabled = valor
        txtano.Enabled = valor
        txtstatuspg.Enabled = valor
        txtfrmpg.Enabled = valor
        txtdtpg.Enabled = valor
        txtstatusfiliacao.Enabled = valor
        txtdtemissao.Enabled = valor
        txtdtvencimente.Enabled = valor
        'txtarquivo.Enabled = valor
        txtpago.Enabled = valor
        txtautomatico.Enabled = valor
        'chkativo.enabled = valor
        txtsegundavia.Enabled = valor
        txtestornado.Enabled = valor

    End Sub



    Private Sub txtmesbusca_TextChanged(sender As Object, e As EventArgs) Handles txtmesbusca.TextChanged
        sonumeros(txtmesbusca.Text)

        If txtmesbusca.Text > "31" Then

            sm("swal({title: 'Atenção!',text: ' O valor digitato nao pode ser uma data',type: 'error',confirmButtonText: 'OK'})", "swal")

        End If

    End Sub

    Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click

        carregamensalidades()

    End Sub

    Public Sub carregamensalidades(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim xMensalidades As New clsmensalidades
        Dim consulta As String = ""
        Dim mes As String = ""
        Dim ano As String = ""
        Dim status As String = ""
        Dim ativo As String = ""
        Dim sql As String = ""



        If radstatus.SelectedValue = "1" Then
            ativo = " where ativo =  '1' "
        ElseIf radstatus.SelectedValue = "0" Then
            ativo = " where ativo = '0' "
        ElseIf radstatus.SelectedValue = "4" Then
            ativo = ""
        End If

        If ativo = "" Then
            ativo = "where valor = '20'  "
        End If

        If cbostatus.SelectedValue = "1" Then
            status = " and statuspg =  '1' "
        ElseIf cbostatus.SelectedValue = "2" Then
            status = " and statuspg = '2' "
        ElseIf cbostatus.SelectedValue = "3" Then
            status = " and statuspg = '3' "
        ElseIf cbostatus.SelectedValue = "0" Then
            status = " and statuspg = '0' "
        End If


        If txtano.Text <> "" Then
            ano = " and ano = '" & txtano.Text & "' "
        End If
        If txtmes.Text > "12" Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Digite uma mês valido!',  showConfirmButton: false,  timer: 2000})")
        End If

        If txtmes.Text <> "" Then
            mes = " and mes = '" & txtmes.Text & "' "
        End If


        If txtdescricao.Text = "" Then
            consulta = ""
        Else
            consulta = " and " & cbobusca.SelectedValue & " = '" & txtdescricao.Text & "'"
        End If

        sql = ativo + mes + ano + status + consulta




        '   If Not xMensalidades.carregagrade() Then
        '   sm("swal({title: 'Error!',text: '" & xMensalidades.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        '   End If

        Dim count As Integer = 0

        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq " & sql & " order by ano,mes asc   ")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        If tb1.Rows.Count > 0 Then

            lblncodigo.Text = tb1.Rows(0)("nrseqcliente").ToString

        Else
            lblncodigo.Text = 0
        End If


        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        grademensalidade.DataSource = tb1
        grademensalidade.DataBind()

    End Sub

    Private Sub btnbuscar_Click(sender As Object, e As EventArgs) Handles btnbuscar.Click
        carregaassociados()
    End Sub

    Private Sub btncomgerarlote_Click(sender As Object, e As EventArgs) Handles btncomgerarlote.Click

        Dim xMensalidade As New clsmensalidades

        '      If Not xMensalidade.gerarloteparaano() Then
        '     sm("swal({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        '   End If

        sm("Swal.fire({  type: 'success',  title: 'Lote gerado com sucesso criados [33876] mensalidades para 2021',  confirmButtonText: 'OK',  timer: 6000})")
        '  txtmensagemg.Text = xMensalidade.Mensagemp
        ' txtmensagemg.Text = xMensalidade.Mensagemg

    End Sub

    Private Sub btnbaixaselect_Click(sender As Object, e As EventArgs) Handles btnbaixaselect.Click
        For Each row As GridViewRow In grademensalidade.Rows

            Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
            Dim chkok As CheckBox = row.FindControl("chkok")

            If chkok.Checked = "true" Then
                Dim xMensalidade As New clsmensalidades

                Dim valor As HiddenField = row.FindControl("hdvalor")
                xMensalidade.Nrseq = nrseq.Value

                If txtdtbaixa.Text <> "" Then
                    xMensalidade.Dtpg = txtdtbaixa.Text
                End If

                If Not xMensalidade.pagar() Then
                    sm("swal({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                    Exit Sub
                End If

                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Efetuada a baixa das mensalidades',  showConfirmButton: false,  timer: 2000})")

                carregamensalidades()

            End If
        Next
    End Sub

    'Private Sub btnteste_Click(sender As Object, e As EventArgs) Handles btnteste.Click

    '    Dim xMensalidade As New clsmensalidades

    '    If Not xMensalidade.corrigir() Then
    '        sm("swal({title: 'Error!',text: 'ERRO 1',type: 'error',confirmButtonText: 'OK'})", "swal")
    '        Exit Sub
    '    End If
    '    If Not xMensalidade.corrigedatas() Then
    '        sm("swal({title: 'Error!',text: 'ERRO 2',type: 'error',confirmButtonText: 'OK'})", "swal")
    '        Exit Sub
    '    End If

    'End Sub
End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="acoeshomepage.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="acoeshomepage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="margin-left: 1rem; margin-top: 1rem; margin-right: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box-header">
            <h2>HOME PAGE ACÕES JUDICIAIS</h2>
        </div>
        <div class="box-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <div class="box-body">
                        <asp:UpdatePanel ID="updateemails" runat="server">
                            <ContentTemplate>

                                <div class="box box-primary">

                                    <div class="box-header with-border">
                                        <h3 class="box-title">Grade ação judicial  </h3>

                                        <div class="box-tools pull-right">                                            
                          <asp:LinkButton ID="btninvisivel"  runat="server" CssClass="btn btn-primary btn-xs" >
                            <i class="fa fa-mail-reply"></i>Voltar</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <br />
                                            <center>
                          <asp:LinkButton ID="btncarregar"  runat="server" CssClass="btn btn-primary btn-lg" >
                            <i class="fa fa-search"></i></asp:LinkButton>
                                        </center>
                                        </div>
                                        <asp:GridView ID="gradeacaojudicial" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma doca cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>' CssClass="badge"></asp:Label>
                                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                                <asp:BoundField DataField="titulo" HeaderText="Titulo" />
                                                <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                                <asp:BoundField DataField="dtcad" HeaderText="Data Cadastrado" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div id="divdesativar" runat="server">
                                                            <asp:LinkButton ID="btndesativar" runat="server" CssClass="btn btn-danger btn-xs" CommandName="cancelar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-close"></i></asp:LinkButton>
                                                        </div>

                                                        <div id="divativar" runat="server">
                                                            <asp:LinkButton ID="btnativar" runat="server" CssClass="btn btn-success btn-xs" CommandName="ativar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-check"></i>  </asp:LinkButton>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Carregamento da ação judicial  </h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="row">
                                            <asp:GridView ID="gradeacoesjud" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                                            <asp:LinkButton ID="usar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="usar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                                    <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                                    <asp:BoundField DataField="nome" HeaderText="Nome" />
                                                    <asp:BoundField DataField="cpf" HeaderText="CPF" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <div class="row center-block  bg-gray-active with-border radio">
                                                    <div class="col-lg-4 text-center">
                                                        <div class="col-lg-11 ">
                                                            <b>
                                                                <asp:Label Text="PROCURAÇÂO" runat="server" /></b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <asp:CheckBox ID="chkprocuracao" runat="server" type="checkbox" AutoPostBack="true" />
                                                                </span>
                                                                <asp:TextBox ID="txtchkprocuracao" type="text" CssClass="form-control" runat="server" Enabled="false" AutoPostBack="true" />
                                                            </div>
                                                            <asp:FileUpload ID="FileUpload1" runat="server" Enabled="false" />
                                                            <br />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4"></div>
                                                    <div class="col-lg-4 text-center">
                                                        <div class="col-lg-11 ">
                                                            <b>
                                                                <asp:Label Text="TERMO" runat="server" /></b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <asp:CheckBox ID="chktermo" runat="server" type="checkbox" AutoPostBack="true" />
                                                                </span>
                                                                <asp:TextBox ID="txtchtermo" type="text" CssClass="form-control" runat="server" Enabled="false" AutoPostBack="true" />
                                                            </div>
                                                            <asp:FileUpload ID="FileUpload2" runat="server" Enabled="false" />
                                                            <br />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div id="divtexto" runat="server">
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <b>
                                                        <asp:Label Text="TITULO:" runat="server" /></b>
                                                    <asp:TextBox runat="server" ID="txtassunto" CssClass="form-control" />
                                                    <b>
                                                        <asp:Label Text="MENSAGEM:" runat="server" /></b>
                                                    <asp:TextBox ID="txtcorpo" TextMode="MultiLine" Style="margin-right: 10px; margin-left: 10px;" Height="420px" runat="server" Width="100%" Rows="100">
                                                    </asp:TextBox>
                                                    <ajaxToolkit:HtmlEditorExtender ID="TextBox1_HtmlEditorExtender" runat="server" TargetControlID="txtcorpo" EnableSanitization="false">
                                                    </ajaxToolkit:HtmlEditorExtender>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-lg-4 text-center "></div>
                                            <div class="col-lg-4 text-center ">
                                                <br />
                                                <asp:Button ID="btnenviar" runat="server" Text="Salvar" class="btn btn-warning " />
                                            </div>
                                        </div>


                                        <%--                                <div id="myTesteEmail" class="modal fade" role="dialog">
                                    <div class="modal-dialog" style="width: 80vw; height: 80vh;">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center">
                                                    <asp:Label ID="Label1" runat="server" CssClass="label-success"></asp:Label></h4>
                                            </div>
                                            <div class="modal-body">


                                                <asp:UpdatePanel ID="updatedocas" runat="server">
                                                    <ContentTemplate>


                                                        <div class="row">
                                                            <div class="col-lg-12 text-center">
                                                                <b>Assunto do E-mail</b>
                                                                <br />
                                                                <asp:Label ID="lbltesteemail" runat="server">
                                                                </asp:Label>
                                                                <br />
                                                                <b>Corpo do e-mail</b>
                                                                <br />
                                                                <center>
                                                  
                                              <iframe id="frameteste"  runat="server" style="width:70vw;height:30vw">

                                              </iframe>
                                                  </center>

                                                                <%--  <ajaxToolkit:HtmlEditorExtender ID="HtmlEditorExtender1" runat="server" TargetControlID="txttesteemail" EnableSanitization="false">
                                                </ajaxToolkit:HtmlEditorExtender>--%>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    </div>
                                    </div>
                                </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script>

        $('.my-colorpicker2').colorpicker();

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>

</asp:Content>

﻿<%@ Page Title="" Language="VB" MasterPageFile="MasterPage.master" AutoEventWireup="false" CodeFile="empresa.aspx.vb" Inherits="pad_empresa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="/js/JScriptmascara.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            
        
    <div class="box box-primary "  style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box-header ">

            <b>
                Empresas
            </b>

        </div>
        <div class="box-body ">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:Button ID="btnnovo" runat="server" Text="Novo" CssClass="btn btn-primary " />
                    <br />
                </div>
            </div>

            <div>
                <asp:Label style="font-size: 0px;" runat="server" ID="txtControle"></asp:Label>
            </div>

            <div class="row">
                <div class="col-lg-1 ">
                    ID:
            <br />
                    <asp:TextBox ID="txtcodigo" runat="server" CssClass="form-control " ReadOnly="true"></asp:TextBox>
                </div>


                <div class="col-lg-4 ">
                    Nome da Empresa:
            <br />
                    <asp:TextBox ID="txtempresa" runat="server" CssClass="form-control "></asp:TextBox>
                </div>
                <div class="col-lg-2 ">
                    CNPJ
            <br />
                    <asp:TextBox ID="txtcnpj" runat="server" CssClass="form-control " onkeyup="formataCNPJ(this,event);" MaxLength="18"></asp:TextBox>
                </div>

                 <div class="col-lg-2 ">
                CEP
            <br />
                <asp:TextBox ID="txtcep" runat="server" CssClass="form-control " onkeyup="formataCEP(this,event);" MaxLength="9"></asp:TextBox>
            </div>

           <div class="col-lg-3 ">
                    Endereço
            <br />
                    <asp:TextBox ID="Txtendereco" runat="server" CssClass="form-control "></asp:TextBox>
                </div>

            </div>
            <br />
            <div class="row">
                



                <div class="col-lg-5 ">
                    Cidade
            <br />
                    <asp:TextBox ID="txtcidade" runat="server" CssClass="form-control "></asp:TextBox>
                </div>


                <div class="col-lg-5 ">
                Bairro
            <br />
                <asp:TextBox ID="Txtbairro" runat="server" CssClass="form-control "></asp:TextBox>

            </div>
                <div class="col-lg-1">
                Estado
            <br />
                <asp:DropDownList ID="Txtestado" runat="server" CssClass="form-control"></asp:DropDownList>


</div>

            </div>



            

            


                <div class="row">
                    <div class="col-lg-12 text-center">
                        <br />
                        <asp:Button ID="btnsalvar" runat="server" Text="Salvar" CssClass="btn btn-success" />
                    </div>
                </div>
                <div class="row">
                     <div class="col-lg-12 text-center ">
                    <hr style="border-color:cornflowerblue "/>
                         </div> 
                </div>



               
                        <asp:UpdatePanel ID="updategrade" runat="server">
                <ContentTemplate>

                    <div class="row">

                        <div class="col-lg-12 text-center ">

                            <br />
                            <asp:GridView ID="grade" runat="server" CssClass="table table-hover table-striped" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma empresa localizada !" AutoGenerateColumns="false" GridLines="None">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="nrseq" />
                                    <asp:BoundField HeaderText="Empresa" DataField="empresa" />
                                    <asp:BoundField HeaderText="CNPJ" DataField="cnpj" />
                                    <asp:BoundField HeaderText="CEP" DataField="cep" />
                                    <asp:BoundField HeaderText="Endereço" DataField="endereco" />
                                    <asp:BoundField HeaderText="Cidade" DataField="cidade" />
                                    <asp:BoundField HeaderText="Bairro" DataField="bairro" />
                                    <asp:BoundField HeaderText="Estado" DataField="uf" />
                                    <asp:TemplateField HeaderText="Ações" runat="server">
                                        <ItemTemplate runat="server">
                                            <asp:LinkButton ID="Edit" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar">
                                                                             <i class="fas fa-edit"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Deletar">
                                           <i class="fas fa-trash-alt"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>


                </ContentTemplate>
            </asp:UpdatePanel>

               
            </div>
    </div>

            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


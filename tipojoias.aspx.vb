﻿Imports clssessoes
Imports System.IO
Imports clsSmart

Partial Class tipojoias
    Inherits System.Web.UI.Page


    Private Sub tipojoias_Load(sender As Object, e As EventArgs) Handles Me.Load
        carregatipojoias()
    End Sub

    Private Sub limpar()
        txtnrseq.Text = ""
        txtdescricao.Text = ""
        txtvalor.Text = ""
        txtdtcad.Text = ""
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub habilitar(valor As Boolean)
        txtnrseq.Enabled = valor
        txtdescricao.Enabled = valor
        txtvalor.Enabled = valor
        txtdtcad.Enabled = valor
    End Sub

    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click

        Dim xTjoias As New clstipojoias

        If Not xTjoias.novo Then
            sm("Swal.fire({   type: 'warning',  title: 'Preencha os campos " & xTjoias.Mensagemerro & "',  confirmButtonText: 'OK'})")
            Exit Sub
        End If


        txtnrseq.Text = xTjoias.Nrseq
        hdnrseq.Value = xTjoias.Nrseq
        txtnrseq.Enabled = False
        btnnovo.Visible = False
        btnsalvar.Visible = True
    End Sub

    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click

        Dim xclasse As New clstipojoias
        Dim informar As String
        informar = ""
        Dim xAcoes As New clsacoes

        If txtdescricao.Text = "" Then
            informar = informar + " [Descricao] "
        End If

        If txtvalor.Text = "" Then
            informar = informar + " [Valor] "
        End If

        If informar <> "" Then
            sm("Swal.fire({   type: 'warning',  title: 'Preencha os campos " & informar & "',  confirmButtonText: 'OK'})")

            Exit Sub
        End If

        xclasse.Descricao = txtdescricao.Text
        xclasse.Nrseq = hdnrseq.Value
        xclasse.Valor = txtvalor.Text

        If Not xclasse.salvar Then
            sm("Swal.fire({   type: 'warning',  title: '" & xclasse.Mensagemerro & "',  confirmButtonText: 'OK'})")
            limpar()
            btnnovo.Visible = True
            btnsalvar.Visible = False
            Exit Sub
        End If

        limpar()
        carregatipojoias()

    End Sub

    Public Sub carregatipojoias()
        Dim consulta As String

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco



        tb1 = tab1.conectar("select * from tbtipojoias ")


        grade.DataSource = tb1
        grade.DataBind()

    End Sub


    Private Sub grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grade.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "editar" Then
            Dim xclasse As New clstipojoias
            xclasse.Nrseq = nrseq.Value

            xclasse.Nrseq = nrseq.Value

            If Not xclasse.procurar() Then
                sm("swal({title: 'Error!',text: '" & xclasse.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            hdnrseq.Value = xclasse.Nrseq
            txtnrseq.Text = xclasse.Nrseq
            txtdescricao.Text = xclasse.Descricao
            txtvalor.Text = xclasse.Valor
            txtdtcad.Text = xclasse.Dtcad


            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Ação Carregada com Sucesso',  showConfirmButton: false,  timer: 2000})")

            carregatipojoias()


        End If
        If e.CommandName = "ativar" Then
            Dim xclasse As New clstipojoias

            xclasse.Nrseq = nrseq.Value

            If Not xclasse.excluir() Then
                sm("swal({title: 'Error!',text: '" & xclasse.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            carregatipojoias()
        End If

        If e.CommandName = "cancelar" Then
            Dim xclasse As New clstipojoias

            xclasse.Nrseq = nrseq.Value

            If Not xclasse.excluir() Then
                sm("swal({title: 'Error!',text: '" & xclasse.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            carregatipojoias()
        End If

    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        Dim ativar As HtmlGenericControl = e.Row.FindControl("divativar")
        Dim fim As HtmlGenericControl = e.Row.FindControl("divfim")
        Dim cancelar As HtmlGenericControl = e.Row.FindControl("divcancelado")
        Dim cancelar2 As HtmlGenericControl = e.Row.FindControl("divcancelar2")
        Dim selecionar As HtmlGenericControl = e.Row.FindControl("divselecionafim")


        If e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "ATIVO"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(4).Text = "0" Then
            e.Row.Cells(4).Text = "CANCELADO"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray

        End If


        If e.Row.Cells(4).Text = "ATIVO" Then
            ativar.Visible = True
            cancelar.Visible = False
        Else
            ativar.Visible = False
            cancelar.Visible = True
        End If

    End Sub
End Class

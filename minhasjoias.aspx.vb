﻿
Partial Class minhasjoias
    Inherits System.Web.UI.Page




    Private Sub minhasjoias_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("idassociado") = "" Then

            Response.Redirect("loginassociados.aspx")
        End If

        If IsPostBack Then

            Exit Sub
        End If

        carregajoias()

    End Sub

    Private Sub gradejoias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradejoias.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(3).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightSalmon
        Else
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If
        'controle mes 
        If e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Janeiro"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Fevereiro"
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.Cells(4).Text = "Março"
        ElseIf e.Row.Cells(4).Text = "4" Then
            e.Row.Cells(4).Text = "Abril"
        ElseIf e.Row.Cells(4).Text = "5" Then
            e.Row.Cells(4).Text = "Maio"
        ElseIf e.Row.Cells(4).Text = "6" Then
            e.Row.Cells(4).Text = "Junho"
        ElseIf e.Row.Cells(4).Text = "7" Then
            e.Row.Cells(4).Text = "Julho"
        ElseIf e.Row.Cells(4).Text = "8" Then
            e.Row.Cells(4).Text = "Agosto"
        ElseIf e.Row.Cells(4).Text = "9" Then
            e.Row.Cells(4).Text = "Setembro"
        ElseIf e.Row.Cells(4).Text = "10" Then
            e.Row.Cells(4).Text = "Outubro"
        ElseIf e.Row.Cells(4).Text = "11" Then
            e.Row.Cells(4).Text = "Novembro"
        ElseIf e.Row.Cells(4).Text = "12" Then
            e.Row.Cells(4).Text = "Dezembro"

        End If


        If e.Row.Cells(3).Text = "0" Then
            e.Row.Cells(3).Text = "Aberto"
        ElseIf e.Row.Cells(3).Text = "1" Then
            e.Row.Cells(3).Text = "Pago"
        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.Cells(3).Text = "Estornado"
        Else
            e.Row.Cells(3).Text = ""
        End If




        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If


    End Sub


    Public Sub carregajoias(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.nrsequser = " & Session("idassociado") & " ")



        gradejoias.DataSource = tb1
        gradejoias.DataBind()

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

End Class

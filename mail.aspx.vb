﻿Imports System.IO
Imports clsSmart
Imports clssessoes
Imports AjaxControlToolkit

Enum OpçõesMensagens
    Entradas
    Enviadas
    Rascunhos
    Excluídas
End Enum
Enum TipoCaixasMensagens
    Nova
    Resposta
End Enum
Partial Class mail

    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Public Shared listausuarios As New List(Of clsusuarios)
    Dim listaemails As New List(Of clsusuarios)
    Public Sub New()

    End Sub

    Private Sub mail_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack Then Exit Sub

        Dim xsql As String = ""

        xsql = "select * from vwusuarios where ativo = true   "

        xsql &= " order by usuario"
        tb1 = tab1.conectar(xsql) ' where nrseqgestor = " & Session("idusuario"))

        listausuarios.Clear()
        For x As Integer = 0 To tb1.Rows.Count - 1
            Dim xcol As New clsusuarios With {
                    .Usuario = tb1.Rows(x)("usuario").ToString,
                    .Nrseq = tb1.Rows(x)("nrseq").ToString,
                    .Email = tb1.Rows(x)("email").ToString}
            listausuarios.Add(xcol)


            ' cbonome.Items.Add(tb1.Rows(x)("nome").ToString)
        Next
        divprepararmensagem.Style.Add("display", "none")
        divcorpo.Style.Remove("display")
        divbotoesfinalizarmensagem.Style.Remove("display")
        divlista.Style.Remove("display")
        divpainel.Style.Remove("display")
        divnova.Style.Add("display", "none")
        divaddusers.Style.Add("display", "none")

        tb1 = tab1.conectar("select * from tbusuarios where ativo = true order by usuario")

        cbousuario.Items.Clear()
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbousuario.Items.Add(tb1.Rows(x)("usuario").ToString)
        Next
        If Session("anexos") <> "" Then

        End If

        hdcaixasel.Value = OpçõesMensagens.Entradas

        listagrade()

        If Request.QueryString("opera") = "nova" Then
            hdtabela.Value = Session("tabela")
            txtassunto.Text = Session("assunto")
            txtcorpo.Text = Session("texto")
            txtemail.Text = Session("emails")
            divlista.Style.Add("display", "none")
            divpainel.Style.Add("display", "none")
            divnova.Style.Remove("display")
            divprepararmensagem.Style.Remove("display")
            divcorpo.Style.Add("display", "none")
            divbotoesfinalizarmensagem.Style.Add("display", "none")
            exibircaixa(TipoCaixasMensagens.Nova)
        End If

        'Dim tbcaixas As New Data.DataTable
        'tbcaixas.Columns.Add("caixas")

        'tbcaixas.Rows.Add()
        'tbcaixas.Rows(tbcaixas.Rows.Count - 1)("caixas") = "Entrada"
        'tbcaixas.Rows.Add()
        'tbcaixas.Rows(tbcaixas.Rows.Count - 1)("caixas") = "Enviadas"
        'tbcaixas.Rows.Add()
        'tbcaixas.Rows(tbcaixas.Rows.Count - 1)("caixas") = "Rascunhos"
        'gradecaixas.DataSource = tbcaixas
        'gradecaixas.DataBind()
    End Sub
    Private Sub listagrade(Optional opcao As OpçõesMensagens = OpçõesMensagens.Entradas)
        Select Case opcao
            Case Is = OpçõesMensagens.Entradas
                tb1 = tab1.conectar("select * from vwcaixaemails where nrseqdestinatario = " & Session("idusuario") & " and excluida = false order by  nrseq desc")
                Grade.DataSource = tb1
                Grade.DataBind()
            Case Is = OpçõesMensagens.Enviadas
                tb1 = tab1.conectar("select * from vwcaixaemails where nrseqremetente = " & Session("idusuario") & " and excluida = false order by nrseq desc")
                Grade.DataSource = tb1
                Grade.DataBind()
            Case Is = OpçõesMensagens.Excluídas
                tb1 = tab1.conectar("select * from vwcaixaemails where nrseqremetente = " & Session("idusuario") & " and excluida = true order by nrseq desc")
                Grade.DataSource = tb1
                Grade.DataBind()
            Case Is = OpçõesMensagens.Rascunhos
                tb1 = tab1.conectar("select * from vwcaixaemails where nrseqdestinatario = " & Session("idusuario") & " and excluida = false and rascunho = true order by  nrseq desc")
                Grade.DataSource = tb1
                Grade.DataBind()
        End Select

    End Sub

    Private Sub btnnova_Click(sender As Object, e As EventArgs) Handles btnnova.Click
        divlista.Style.Add("display", "none")
        divpainel.Style.Add("display", "none")
        divnova.Style.Remove("display")
        exibircaixa(TipoCaixasMensagens.Nova)
    End Sub

    Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click

        Dim enviar As New clscaixamensagens
        enviar.Assunto = txtassunto.Text
        enviar.Corpo = txtcorpo.Text.Replace("{.}", hdtabela.Value)

        If Session("anexos") <> "" Then
            Dim _listaanexos As String() = separaremails(Session("anexos"))
            For x As Integer = 0 To _listaanexos.Count - 1
                Dim xanexo As New clscaixamensagens_anexos
                xanexo.Arquivo = mARQUIVO(_listaanexos(x))
                xanexo.Caminho = mPASTA(_listaanexos(x))
                enviar.anexos.Add(xanexo)
            Next
        End If

        Dim _listaemails As String() = separaremails(txtemail.Text)

        For y As Integer = 0 To _listaemails.Length - 1
            If Not _listaemails(y) Is Nothing Then
                Dim destinos As New clscaixamensagens_destinatarios
                destinos.Usuario = _listaemails(y)
                enviar.destinatarios.Add(destinos)
            End If
        Next
        Dim _listaemailscco As String() = separaremails(txtcco.Text)
        For y As Integer = 0 To _listaemailscco.Length - 1
            If Not _listaemailscco(y) Is Nothing Then
                Dim destinos As New clscaixamensagens_destinatarios
                destinos.Usuario = _listaemailscco(y)
                destinos.Oculto = True
                enviar.destinatarios.Add(destinos)
            End If
        Next
        'enviar.Origemdeemailenviado = True
        enviar.enviar()


        divnova.Style.Add("display", "none")

        divpainel.Style.Remove("display")
        divlista.Style.Remove("display")

        If Request.QueryString("opera") = "nova" Then
            Response.Redirect("loteaprovado.aspx")
        End If
    End Sub

    Private Sub btncancelnova_Click(sender As Object, e As EventArgs) Handles btncancelnova.Click
        divnova.Style.Add("display", "none")

        divpainel.Style.Remove("display")
        divlista.Style.Remove("display")
        listagrade()
    End Sub

    Private Sub btnenderecos_Click(sender As Object, e As EventArgs) Handles btnenderecos.Click

        hdemails.Value &= txtemail.Text & ";"
        For x As Integer = 0 To separaremails(hdemails.Value).Length - 1
            If separaremails(hdemails.Value)(x) Is Nothing Then Continue For
            Dim xdest As New clsusuarios
            xdest.Usuario = separaremails(hdemails.Value)(x)
            listaemails.Add(xdest)
        Next
        rptdestinatarios.DataSource = listaemails
        rptdestinatarios.DataBind()
        txtemail.Text = ""


        'divaddusers.Style.Remove("display")
    End Sub

    Private Sub btnadddest_Click(sender As Object, e As EventArgs) Handles btnadddest.Click
        If txtemail.Text = "" Then
            txtemail.Text = cbousuario.Text
        Else
            txtemail.Text &= (";" & cbousuario.Text)
        End If

    End Sub

    Private Sub btnadddestoculto_Click(sender As Object, e As EventArgs) Handles btnadddestoculto.Click
        If txtcco.Text = "" Then
            txtcco.Text = cbousuario.Text
        Else
            txtcco.Text &= (";" & cbousuario.Text)
        End If
    End Sub

    Private Sub btnFecharViaEnergia_Click(sender As Object, e As EventArgs) Handles btnFecharViaEnergia.Click
        divaddusers.Style.Add("display", "none")

    End Sub

    Private Sub Grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Grade.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If

        Dim hdimagemremetente As HiddenField = e.Row.FindControl("hdimagemremetente")
        Dim imagemremetente As Image = e.Row.FindControl("imagemremetente")
        Dim hdimagemdestinatario As HiddenField = e.Row.FindControl("hdimagemdestinatario")
        Dim imagemdestinatario As Image = e.Row.FindControl("imagemdestinatario")
        Dim hdlida As HiddenField = e.Row.FindControl("hdlida")
        Dim imganexosgrade As Image = e.Row.FindControl("imganexosgrade")
        If IsDate(e.Row.Cells(3).Text) Then e.Row.Cells(3).Text = FormatDateTime(e.Row.Cells(3).Text, DateFormat.ShortDate)
        If hdlida.Value = "1" OrElse hdlida.Value = "True" Then
            e.Row.ForeColor = System.Drawing.Color.Black
        Else
            e.Row.ForeColor = System.Drawing.Color.Green
            e.Row.Font.Bold = True
        End If

        imganexosgrade.Visible = False
        If e.Row.DataItem("totalanexos").ToString > 0 Then
            imganexosgrade.Visible = True
        End If
        If hdimagemremetente.Value <> "" Then
            If System.IO.File.Exists(Request.MapPath("~/social/" & hdimagemremetente.Value)) Then
                imagemremetente.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & hdimagemremetente.Value)
            End If
        End If

        If hdimagemdestinatario.Value <> "" Then
            If System.IO.File.Exists(Request.MapPath("~/social/" & hdimagemdestinatario.Value)) Then
                imagemdestinatario.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & hdimagemdestinatario.Value)
            End If
        End If
    End Sub

    Private Sub Grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Grade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = Grade.Rows(index)
        Dim hdnrseq As HiddenField = row.FindControl("hdnrseq")
        Dim hdnrseqdest As HiddenField = row.FindControl("hdnrseqdest")
        Dim lblremetente As Label = row.FindControl("lblremetente")
        If e.CommandName = "excluir" Then
            Dim excluimens As New clscaixamensagens(True)

            excluimens.apagar(hdnrseqdest.Value)
            exibircaixa(TipoCaixasMensagens.Resposta)

        End If
        If e.CommandName = "Abrir" Then

            Dim excluimens As New clscaixamensagens(True)

            excluimens.abrir(True, hdnrseqdest.Value)

            gradeanexos.DataSource = excluimens.Tbanexos
            gradeanexos.DataBind()
            txtassunto.Text = excluimens.Assunto
            txtcorpo.Text = excluimens.Corpo
            txtemail.Text = Session("usuario")
            lbllremetente.Text = lblremetente.Text
            divnova.Style.Remove("display")
            exibircaixa(TipoCaixasMensagens.Resposta)
          
        End If
    End Sub
    Private Sub exibircaixa(Optional tipo As TipoCaixasMensagens = TipoCaixasMensagens.Nova)
        If tipo = TipoCaixasMensagens.Nova Then
            divnovamensagem.Style.Remove("display")
            divenviar.Style.Remove("display")
            divresponder.Style.Add("display", "none")
            divencaminhar.Style.Add("display", "none")
            divremetente.Style.Add("display", "none")
            txtemail.ReadOnly = False
            txtcco.ReadOnly = False
            btnenderecos.Visible = True
            lblfrasenovamensagem.Text = "Nova Mensagem"
        Else
            divresponder.Style.Remove("display")
            divencaminhar.Style.Remove("display")
            divnovamensagem.Style.Add("display", "none")
            divenviar.Style.Add("display", "none")
            divremetente.Style.Remove("display")
            txtemail.ReadOnly = True
            txtcco.ReadOnly = True
            btnenderecos.Visible = False
            lblfrasenovamensagem.Text = "Mensagem"
        End If


    End Sub
    Private Sub btncaixa_entrada_Click(sender As Object, e As EventArgs) Handles btncaixa_entrada.Click
        hdcaixasel.Value = OpçõesMensagens.Entradas

        listagrade()

    End Sub

    Private Sub btncaixa_enviadas_Click(sender As Object, e As EventArgs) Handles btncaixa_enviadas.Click
        hdcaixasel.Value = OpçõesMensagens.Enviadas

        listagrade(hdcaixasel.Value)

    End Sub

    Private Sub btncaixa_excluidas_Click(sender As Object, e As EventArgs) Handles btncaixa_excluidas.Click
        hdcaixasel.Value = OpçõesMensagens.Excluídas

        listagrade(hdcaixasel.Value)
    End Sub

    Private Sub btncaixa_rascunho_Click(sender As Object, e As EventArgs) Handles btncaixa_rascunho.Click
        hdcaixasel.Value = OpçõesMensagens.Rascunhos

        listagrade(hdcaixasel.Value)
    End Sub

    Private Sub btnresponder_Click(sender As Object, e As EventArgs) Handles btnresponder.Click
        txtemail.Text = lbllremetente.Text
        exibircaixa(TipoCaixasMensagens.Nova)
    End Sub

    Private Sub btnEncaminhar_Click(sender As Object, e As EventArgs) Handles btnEncaminhar.Click
        txtemail.Text = lbllremetente.Text
        exibircaixa(TipoCaixasMensagens.Nova)
    End Sub

    Public Sub saveimage(sender As Object, e As AjaxFileUploadEventArgs)
        Dim fullPath As String = "/assets/images/homepage_image/" + e.FileName

        TextBox1_HtmlEditorExtender.AjaxFileUpload.SaveAs(MapPath("/assets/images/homepage_image/" + e.FileName))



        e.PostedUrl = Page.ResolveUrl(fullPath)
    End Sub

    Private Sub TextBox1_HtmlEditorExtender_ResolveControlID(sender As Object, e As ResolveControlEventArgs) Handles TextBox1_HtmlEditorExtender.ResolveControlID
        MsgBox(e.Control.ToString & " - " & e.ControlID.ToString)
    End Sub

    Private Sub btnanexar_Click(sender As Object, e As EventArgs) Handles btnanexar.Click

    End Sub

    Private Sub gradeanexos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeanexos.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim lblitem As Label = e.Row.FindControl("lblitem")
        lblitem.Text = e.Row.RowIndex + 1

        If logico(e.Row.DataItem("anexoativo")) = True Then
            e.Row.ForeColor = System.Drawing.Color.Green
        Else
            e.Row.ForeColor = System.Drawing.Color.Red
        End If
    End Sub

    Private Sub gradeanexos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeanexos.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeanexos.Rows(index)
        Dim hdcaminho As HiddenField = row.FindControl("hdcaminho")
        Select Case e.CommandName.ToLower
            Case Is = "Baixar".ToLower
                Dim arquivo As String = hdcaminho.Value & "\" & row.Cells(1).Text
                Response.ContentType = "application/octet-stream"

                Response.Clear()
                Response.AddHeader("content-disposition", "attachment; filename=" & arquivo)
                Response.TransmitFile(arquivo)
                Response.End()
            Case Is = "Excluir".ToLower
        End Select

    End Sub

    Private Sub mail_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If IsPostBack Then Exit Sub
        If Request.QueryString("opera") = "nova" Then

            txtcorpo.Text = Session("texto")

        End If
    End Sub

    Private Sub btnprepararmensagem_Click(sender As Object, e As EventArgs) Handles btnprepararmensagem.Click
        divcorpo.Style.Remove("display")
        divbotoesfinalizarmensagem.Style.Remove("display")
        divprepararmensagem.Style.Add("display", "none")
        txtcorpo.Text = Session("texto")
    End Sub
    <System.Web.Script.Services.ScriptMethod(),
   System.Web.Services.WebMethod()>
    Public Shared Function getdestinatarios(ByVal prefixText As String, ByVal count As Integer) As List(Of String)
        Dim cliente As New List(Of String)
        'Dim tb1 As New Data.DataTable
        'Dim tab1 As New clsBanco

        'tb1 = tab1.conectar("select * from tbcolaboradores where nome like '" & prefixText & "%' order by nome")

        'For x As Integer = 0 To tb1.Rows.Count - 1
        '    cliente.Add(String.Format("{0}", tb1.Rows(x)("nome").ToString))
        'Next
        If listausuarios.Count = 0 Then
            Dim xsql As String
            Dim tb1 As New Data.DataTable
            Dim tab1 As New clsBanco
            xsql = "select * from vwusuarios where ativo = true   "

            xsql &= " order by usuario"
            tb1 = tab1.conectar(xsql) ' where nrseqgestor = " & Session("idusuario"))


            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim xcol As New clsusuarios With {
                    .Usuario = tb1.Rows(x)("usuario").ToString,
                    .Nrseq = tb1.Rows(x)("nrseq").ToString,
                    .Email = tb1.Rows(x)("email").ToString}
                listausuarios.Add(xcol)


                ' cbonome.Items.Add(tb1.Rows(x)("nome").ToString)
            Next
        End If
        For x As Integer = 0 To listausuarios.Count - 1
            If listausuarios(x) Is Nothing Then Continue For
            If listausuarios(x).Usuario.Length >= prefixText.Length AndAlso listausuarios(x).Usuario.ToLower.Substring(0, prefixText.Length) = prefixText.ToLower Then
                cliente.Add(listausuarios(x).Usuario)
            End If

        Next

        Return cliente
    End Function
End Class

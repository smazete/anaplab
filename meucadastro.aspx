﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="meucadastro.aspx.vb"  MasterPageFile="~/MasterPage3.master" Inherits="meucadastro" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
    <script src="js/JScriptmascara.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
   <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
       
                    <div class="box-header with-border">
                        <h3 class="box-title">Dados Pessoais</h3>
                       
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="Matricula" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox ID="txtmatricula" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            </div>
                            <div class="col-lg-4">
                                <asp:Label Text="Nome" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="E-mail" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Nascimento" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtnasc" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="CPF" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox runat="server" type="text" CssClass="form-control cnpjcpf" ID="txtcpf" ClientIDMode="Static"  onkeyup="formataCnpjCpf(this,event);" MaxLength="13"      ></asp:TextBox>        
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="RG" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox ID="txtrg" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Celular" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox ID="txtcelular" runat="server" CssClass="form-control"  ClientIDMode="Static" onkeyup="formataTelefoneC(this,event);"  MaxLength="15"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Telefone" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox ID="txttelefone" runat="server" CssClass="form-control"  ClientIDMode="Static" onkeyup="formataTelefone(this,event);" MaxLength="15"></asp:TextBox>

                            </div>
                            <div class="col-lg-1">
                                <asp:Label Text="Sexo" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:DropDownList runat="server" ID="txtsexo" CssClass="form-control" ClientIDMode="Static">
                                                <asp:ListItem></asp:ListItem>
                                                <asp:ListItem>M</asp:ListItem>
                                                <asp:ListItem>F</asp:ListItem>
                                            </asp:DropDownList>
                            </div>
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                            <div class="col-lg-2">
                                <asp:Label Text="CEP" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                            <asp:TextBox runat="server" type="text" AutoPostBack="true" CssClass="form-control cep" ID="txtcep" ClientIDMode="Static"  MaxLength="9" onkeyup="formataCEP(this,event);" />
                                                  </div>
                                    
                            </ContentTemplate> 
                            </asp:UpdatePanel>
                            <div class="col-lg-1">
                                <asp:Label Text="Estado" runat="server"></asp:Label>
                                <asp:TextBox ID="txtestado" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label Text="Cidade" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcidade" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Endereço" runat="server"></asp:Label>
                                <asp:TextBox ID="txtendereco" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Bairro" runat="server"></asp:Label>
                                <asp:TextBox ID="txtbairro" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:Label Text="Numero" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Complemento" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcomplemento" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <asp:Label Text="Nome Mãe" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                <asp:TextBox ID="txtnomemae" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <b>Dados de Acesso </b>
                                    </div>
                                    <div class="card-body">
                                        <br />
                                        <div class="col-lg-4" visible="false" runat="server">
                                            <b>
                                                <asp:Label Text="USUARIO:" runat="server"></asp:Label></b>
                                        </div>
                                        <div class="col-lg-8" visible="false" runat="server">
                                            <asp:TextBox ID="txtmatricula2" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-4">
                                            <b>
                                                <asp:Label Text="SENHA:" runat="server"></asp:Label></b>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:TextBox  ID="txtsenha" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <b>Dados do Associado </b>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label ID="lblsituaçao" Text="Situação" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox ID="txtsituacaopb" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label Text="Data posse" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtposse" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label Text="Data Aposentadoria" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtaposentadoria" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <b>Dados Bancarios</b>
                                        </div>
                                        <div class="card-body">
                                            <br />
                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label Text="Agencia" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox ID="txtAgencia" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label Text="Conta corrente" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox ID="txtcontacorrente" runat="server" CssClass="form-control"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                     
                        </div>

                               <div class="row">
                                   <br />
                            <center>
                            <asp:LinkButton CssClass="btn btn-primary" ID="btnatualizar" Text="Atualizar" runat="server" />
                                </center>
                                </div>

                    </div>
                </div>
   

</asp:Content>
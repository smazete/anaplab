﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <title>Anaplab - Login</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script 
        src="https://code.jquery.com/jquery-3.4.1.slim.js"
        integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
</head>
<body>
    <form runat="server">
        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
        <div class="row" style="width: 100%;">
            <style>
                #slideshow > div img {
                    width: 100%;
                    height: 800px;
                    position: absolute;
                    object-fit: cover;
                }
            </style>



            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-4">
                 
               <div runat="server" id="slideshow">
                </div>


            </div>

            <script type="text/javascript">

                $(document).ready(function () {
                    $("#slideshow > div:gt(0)").hide();

                    var count = $("#slideshow").children().length;

                    if (count > 1) {

                        setInterval(function () {
                            $('#slideshow > div:first')
                                .fadeOut(2000)
                                .next()
                                .fadeIn(2000)
                                .end()
                                .appendTo('#slideshow');
                        }, 8000);
                    }
                });

                function executa() {
                    $("#slideshow > div:gt(0)").hide();

                    var count = $("#slideshow").children().length;

                    if (count > 1) {

                        setInterval(function () {
                            $('#slideshow > div:first')
                                .fadeOut(2000)
                                .next()
                                .fadeIn(2000)
                                .end()
                                .appendTo('#slideshow');
                        }, 8000);
                    }
                };

            </script>


            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-8" style="background-color: white; position: absolute; top: 0; bottom: 0; right: 0; background-color: white;">
                <div class="container" style="padding: 8%;">


                    <div style="margin-bottom: 5%; margin-top: 5%;" class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                            <center>
                            <img style="width: 200px;" src="img/logo.png" />
                         <%--  <asp:Image ID="imlogo" runat="server" />--%> 
                                </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 text-center">
                            <p>Bem-vindo ao sistema Anaplab</p>
                            <br />
                            <b>Gerenciamento Anaplab</b>
                        </div>
                        <br />

                    </div>
                    <div id="divdadosentrada" runat="server">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 text-center">
                                <br />
                                <p>Por favor, acesse com seu login</p>
                            </div>
                            <br />

                        </div>
                        <div class="row">
                            <%--<div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                            <p class="alert-warning">Enter your user ID in the format "domain\User" or "user@domain".</p>
                        </div>--%>
                        </div>

                        <div class="row">

                            <div class="col-lg-11 col-md-11 col-sm-11 col-xl-11">

                             <center>Login</center>   
                             
                                <asp:TextBox runat="server" ID="txtuser" CssClass="form-control" style="text-align:center;"></asp:TextBox>
                            </div>

                            <div class="col-lg-11 col-md-11 col-sm-11 col-xl-11">
                                 <center>Senha</center>     
                                <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" CssClass="form-control" style="text-align:center;"></asp:TextBox>
                            </div>

                        </div>

                        <div class="row">
                            <%-- <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12">
                            <asp:CheckBox runat="server" CssClass="custom-checkbox" Text="Keep me signed in" />
                        </div>--%>
                        </div>
                        <br />
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-12 text-center">
                                <asp:LinkButton type="submit" runat="server" CssClass="btn btn-primary" Text="Entrar" ID="btnentrar"></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="btnesqueci" runat="server"> Esqueci minha senha</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                    <%--<div style="display:none">--%>


                    <div id="divemail" runat="server">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <br />
                                <br />
                                <asp:Label runat="server" Text="Por favor, digite seu email:" ID="Label4"></asp:Label>

                                <br />
                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control "></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-6 text-center">

                                <br />
                                <br />
                                <asp:LinkButton ID="btnsend" runat="server" CssClass="btn btn-success "><asp:Image runat="server" ImageUrl="~/img/email2.png" style="width:1.5rem;height:1.5rem"/>Enviar</asp:LinkButton>
                            </div>
                            <div class="col-lg-6 text-center">

                                <br />
                                <br />
                                <asp:LinkButton ID="btnCancelar" runat="server" CssClass="btn btn-danger "><asp:Image runat="server" ImageUrl="~/img/imgnao.png" style="width:1.5rem;height:1.5rem"/>Cancelar</asp:LinkButton>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-center ">
                                <br />
                                <asp:LinkButton ID="btnnotenhoemail" runat="server" CssClass="btn btn-warning " Style="background-color: antiquewhite; color: red;">
                                    <asp:Label runat="server" Text="Não tenho e-mail cadastrado!" ID="Label3"></asp:Label>
                                </asp:LinkButton>
                            </div>
                            <div class="col-lg-2 ">
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="divnatal" runat="server">
                        <%-- <div class="col-lg-10 ">
    </div>--%>
                        <div class="col-lg-12 text-center">
                            <asp:Image runat="server" Style="width: 8rem; height: 8rem;" ImageUrl="~/img/papai-noel.png" />

                        </div>
                    </div>

                    <%--</div>--%>
                </div>
            </div>

        </div>
    </form>
</body>
</html>

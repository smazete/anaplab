﻿Imports System.Data
Imports clsSmart

Partial Class relatoriojoias2
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim wcvalortotal As Decimal = 0
    Dim linhas As Integer = 0
    Dim tab1 As New clsBanco

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub


    Private Sub limpar()
        '  txtdtfinal.Text = ""
        '    txtdtinicial.Text = ""
    End Sub

    Private Sub relatoriojoias_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If
    End Sub

    '  Private Sub btnexport_Click(sender As Object, e As EventArgs) Handles btnexport.Click

    'Dim tb1 As New Data.DataTable
    'Dim tab1 As New clsBanco
    'Dim xMensalidades As New clsmensalidades
    'Dim consulta As String = ""
    'Dim mes As String = ""
    'Dim ano As String = ""
    'Dim status As String = ""
    'Dim ativo As String = ""
    'Dim sql As String = ""

    'If ativo = "" Then
    '    ativo = "where tbmensalidade_joias.ativo = true  "
    'End If

    'If cbostatus.SelectedValue = "1" Then
    '    status = " and tbmensalidade_joias.statuspg =  '1' "
    'ElseIf cbostatus.SelectedValue = "2" Then
    '    status = " and tbmensalidade_joias.statuspg = '2' "
    'ElseIf cbostatus.SelectedValue = "3" Then
    '    status = " and tbmensalidade_joias.statuspg = '3' "
    'ElseIf cbostatus.SelectedValue = "0" Then
    '    status = " and tbmensalidade_joias.statuspg = '0' "
    'End If

    'ano = " and tbmensalidade_joias.ano = '" & txtano.Text & "'"
    'mes = " and tbmensalidade_joias.mes = '" & cbomes.SelectedValue & "'"

    'sql = ativo + mes + ano + status

    ''   If Not xMensalidades.carregagrade() Then
    ''   sm("swal({title: 'Error!',text: '" & xMensalidades.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
    ''    Exit Sub
    ''   End If

    'Dim count As Integer = 0

    'tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq " & sql & " order by ano,mes asc   ")

    ''If Not xempresas.Contador > 0 The

    'gradeex.DataSource = tb1
    'gradeex.DataBind()

    '     sm("exportarh", "exportarh")

    '  End Sub

    Private Sub btnver_Click(sender As Object, e As EventArgs) Handles btnver.Click

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim consulta As String = ""
        Dim mes As String = ""
        Dim ano As String = ""
        Dim status As String = ""
        Dim ativo As String = ""
        Dim sql As String = ""
        Dim userid As String = ""

        If ativo = "" Then
            ativo = " where tbjoias_2.ativo = true   "
        End If
        If txtid.Text <> "" Then
            userid = " and tbjoias_2.nrsequser  = '" & txtid.Text & "' "
        End If

        If cbostatus.SelectedValue = "1" Then
            status = " and tbjoias_2.statuspg =  '1' "
        ElseIf cbostatus.SelectedValue = "2" Then
            status = " and tbjoias_2.statuspg = '2' "
        ElseIf cbostatus.SelectedValue = "3" Then
            status = " and tbjoias_2.statuspg = '3' "
        ElseIf cbostatus.SelectedValue = "0" Then
            status = " and tbjoias_2.statuspg = '0' "
        Else
            status = ""
        End If
        If chkdt.Checked = True Then
            mes = ""
            ano = ""
        Else

            ano = " and tbjoias_2.ano = '" & txtano.Text & "' "
            mes = " and tbjoias_2.mes = '" & cbomes.SelectedValue & "' "
        End If



        sql = ativo + userid + mes + ano + status

        '   If Not xMensalidades.carregagrade() Then
        '   sm("swal({title: 'Error!',text: '" & xMensalidades.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        '   End If

        '   Dim count As Integer = 0

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq " & sql & " order by tbjoias_2.nrseq, tbjoias_2.ano, tbjoias_2.mes asc   ")

        ' linhas = tb1.Rows.Count

        'If Not xempresas.Contador > 0 The

        grademensalidade.DataSource = tb1
        grademensalidade.DataBind()

        gradeex.DataSource = tb1
        gradeex.DataBind()

        btnexport.Visible = True
    End Sub


    Private Sub grademensalidade_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles grademensalidade.ItemDataBound


        Select Case e.Item.ItemType

            Case Is = ListItemType.Header
                wcvalortotal = 0
            Case Is = ListItemType.Item
                Dim lblvalordg As Label = e.Item.FindControl("lblvalordg")
                If e.Item.Cells(5).Text <> "" Then
                    wcvalortotal -= numeros(lblvalordg.Text)
                    e.Item.ForeColor = System.Drawing.Color.Red
                Else
                    wcvalortotal += numeros(lblvalordg.Text)
                    e.Item.ForeColor = System.Drawing.Color.Green
                End If
            Case Is = ListItemType.AlternatingItem
                Dim lblvalordg As Label = e.Item.FindControl("lblvalordg")
                If e.Item.Cells(5).Text <> "" Then
                    wcvalortotal -= numeros(lblvalordg.Text)
                    e.Item.ForeColor = System.Drawing.Color.Red
                Else
                    wcvalortotal += numeros(lblvalordg.Text)
                    e.Item.ForeColor = System.Drawing.Color.Green
                End If
            Case Is = ListItemType.Footer
                Dim lblvalordg As Label = e.Item.FindControl("lbltotvalordg")
                lblvalordg.Text = FormatCurrency(wcvalortotal)
        End Select

        If e.Item.ItemType = ListItemType.Item Then

        End If


        If e.Item.Cells(3).Text = "1" Then
            e.Item.Cells(3).Text = "Janeiro"
        ElseIf e.Item.Cells(3).Text = "2" Then
            e.Item.Cells(3).Text = "Fevereiro"
        ElseIf e.Item.Cells(3).Text = "3" Then
            e.Item.Cells(3).Text = "Março"
        ElseIf e.Item.Cells(3).Text = "4" Then
            e.Item.Cells(3).Text = "Abril"
        ElseIf e.Item.Cells(3).Text = "5" Then
            e.Item.Cells(3).Text = "Maio"
        ElseIf e.Item.Cells(3).Text = "6" Then
            e.Item.Cells(3).Text = "Junho"
        ElseIf e.Item.Cells(3).Text = "7" Then
            e.Item.Cells(3).Text = "Julho"
        ElseIf e.Item.Cells(3).Text = "8" Then
            e.Item.Cells(3).Text = "Agosto"
        ElseIf e.Item.Cells(3).Text = "9" Then
            e.Item.Cells(3).Text = "Setembro"
        ElseIf e.Item.Cells(3).Text = "10" Then
            e.Item.Cells(3).Text = "Outubro"
        ElseIf e.Item.Cells(3).Text = "11" Then
            e.Item.Cells(3).Text = "Novembro"
        ElseIf e.Item.Cells(3).Text = "12" Then
            e.Item.Cells(3).Text = "Dezembro"
        End If


        If e.Item.Cells(6).Text.ToString = "1" Then
            e.Item.Cells(6).Text = "Pago"
        ElseIf e.Item.Cells(6).Text.ToString = "0" Then
            e.Item.Cells(6).Text = "Aberto"
        Else
            e.Item.Cells(6).Text = ""
        End If

    End Sub

    'Private Sub btnvalor_Click(sender As Object, e As EventArgs) Handles btnvalor.Click


    '    tb1 = tab1.IncluirAlterarDados("update tbjoias_2 set valor = '200'")


    'End Sub

    Private Sub gradeex_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles gradeex.ItemDataBound




        Select Case e.Item.ItemType

            Case Is = ListItemType.Header
                wcvalortotal = 0
            Case Is = ListItemType.Item
                Dim lblvalordg As Label = e.Item.FindControl("lblvalordg")
                If e.Item.Cells(5).Text <> "" Then
                    wcvalortotal -= numeros(lblvalordg.Text)
                    e.Item.ForeColor = System.Drawing.Color.Red
                Else
                    wcvalortotal += numeros(lblvalordg.Text)
                    e.Item.ForeColor = System.Drawing.Color.Green
                End If
            Case Is = ListItemType.AlternatingItem
                Dim lblvalordg As Label = e.Item.FindControl("lblvalordg")
                If e.Item.Cells(5).Text <> "" Then
                    wcvalortotal -= numeros(lblvalordg.Text)
                    e.Item.ForeColor = System.Drawing.Color.Red
                Else
                    wcvalortotal += numeros(lblvalordg.Text)
                    e.Item.ForeColor = System.Drawing.Color.Green
                End If
            Case Is = ListItemType.Footer
                Dim lblvalordg As Label = e.Item.FindControl("lbltotvalordg")
                lblvalordg.Text = FormatCurrency(wcvalortotal)
        End Select

        If e.Item.ItemType = ListItemType.Item Then

        End If

        If e.Item.Cells(3).Text = "1" Then
            e.Item.Cells(3).Text = "Janeiro"
        ElseIf e.Item.Cells(3).Text = "2" Then
            e.Item.Cells(3).Text = "Fevereiro"
        ElseIf e.Item.Cells(3).Text = "3" Then
            e.Item.Cells(3).Text = "Março"
        ElseIf e.Item.Cells(3).Text = "4" Then
            e.Item.Cells(3).Text = "Abril"
        ElseIf e.Item.Cells(3).Text = "5" Then
            e.Item.Cells(3).Text = "Maio"
        ElseIf e.Item.Cells(3).Text = "6" Then
            e.Item.Cells(3).Text = "Junho"
        ElseIf e.Item.Cells(3).Text = "7" Then
            e.Item.Cells(3).Text = "Julho"
        ElseIf e.Item.Cells(3).Text = "8" Then
            e.Item.Cells(3).Text = "Agosto"
        ElseIf e.Item.Cells(3).Text = "9" Then
            e.Item.Cells(3).Text = "Setembro"
        ElseIf e.Item.Cells(3).Text = "10" Then
            e.Item.Cells(3).Text = "Outubro"
        ElseIf e.Item.Cells(3).Text = "11" Then
            e.Item.Cells(3).Text = "Novembro"
        ElseIf e.Item.Cells(3).Text = "12" Then
            e.Item.Cells(3).Text = "Dezembro"
        End If



        If e.Item.Cells(6).Text.ToString = "1" Then
            e.Item.Cells(6).Text = "Pago"
        ElseIf e.Item.Cells(6).Text.ToString = "0" Then
            e.Item.Cells(6).Text = "Aberto"
        Else
            e.Item.Cells(6).Text = ""
        End If



    End Sub
End Class

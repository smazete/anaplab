﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Especialidades.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="Especialidades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Cadastro de Especialidades</b>
        </div>

        <div class="box-body">
            <div class="row">

                <div class="col-lg-4">
                    <asp:Label ID="Label20" runat="server" Text="Plano "></asp:Label>
                    <asp:DropDownList ID="cboplano" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
                <div class="col-lg-2">
                    <asp:LinkButton ID="btnconfirmar" runat="server" CssClass="btn btn-primary fix-btn" autopostback="true">Selecionar</asp:LinkButton>
                    <asp:LinkButton ID="btntrocar" runat="server" CssClass="btn btn-info fix-btn" Enabled="false" Visible="false">Trocar</asp:LinkButton>
                </div>
                <div class="col-lg-2">
                    <asp:HiddenField ID="hidcod" runat="server"></asp:HiddenField>
                </div>
            </div>

            <div id="divmostrardados" runat="server">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <asp:LinkButton ID="btnnovo" runat="server" CssClass="btn btn-warning "> <i class="fas fa-plus-circle"></i>Novo</asp:LinkButton>
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        Codigo
                    <br>
                        <asp:TextBox Enabled="false" ReadOnly="true" ID="txtcod" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-lg-6">
                        Descricao
                    <br>
                        <asp:TextBox Enabled="false" ID="txtespecialidade" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div class="col-lg-2">
                        Valor
                    <br>
                        <asp:TextBox Enabled="false" ID="txtvalor" runat="server" CssClass="form-control">0</asp:TextBox>
                    </div>
                    <div class="col-lg-2">
                        Nova Consulta Em                       
                        <br>
                        <asp:TextBox Enabled="false" ID="txtnovaconsultaem" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">


                            <div class="col-lg-6">
                                Padrão de prontuário
                                <asp:DropDownList Enabled="false" ID="cbopadraoprontuario" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="col-lg-2">
                                <br>
                                <asp:LinkButton Enabled="false" ID="btnacao" runat="server" CssClass="btn btn-primary"> + </asp:LinkButton>
                            </div>
                            <div class="col-lg-4">
                                CBO
                    <br>
                                <asp:TextBox Enabled="false" ID="txtcbo" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-6">
                                Descrição padrão: tela de Recebimento
                    <br>
                                <asp:DropDownList Enabled="false" ID="cbodescripadrao" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="col-lg-6">
                                Tabela Tuss
                    <br>
                                <asp:DropDownList Enabled="false" ID="txtTuss" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        Ações por Especialidade
                    <br>
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:CheckBox Enabled="false" CssClass="checkbox-inline" ID="chkIgnoraRetorno" runat="server" Text="Iguinora Retorno"></asp:CheckBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:CheckBox Enabled="false" CssClass="checkbox-inline" ID="chkctrlFinan" runat="server" Text="Controle Financeiro"></asp:CheckBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:CheckBox Enabled="false" CssClass="checkbox-inline" ID="chkignorarvalor" runat="server" Text="Ignorar Valor"></asp:CheckBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:CheckBox Enabled="false" CssClass="checkbox-inline" ID="chkretorno" runat="server" Text="Retorno Automático"></asp:CheckBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:CheckBox Enabled="false" CssClass="checkbox-inline" ID="chkimprimirguia" runat="server" Text="Imprimir Guia "></asp:CheckBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:CheckBox Enabled="false" CssClass="checkbox-inline" ID="chkmedtrab" runat="server" Text="Medicina Trabalho"></asp:CheckBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:CheckBox Enabled="false" CssClass="checkbox-inline" ID="chkpreparatorio" runat="server" Text=" Peparatorio de consulta" AutoPostBack="true"></asp:CheckBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:CheckBox Enabled="false" CssClass="checkbox-inline" ID="chkalterarespecialidade" runat="server" Text=" Alterar especialidade ao confirmar consulta"></asp:CheckBox>
                            </div>

                        </div>

                    </div>
                </div>

                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="row" runat="server" id="divdescricaopreparatorios">
                            <div class="col-lg-12">
                                Descrição Preparatorios
                            <asp:TextBox CssClass="form-control" Rows="5" Enabled="false" runat="server" ID="txtdescricaopreparatorio" Style="resize: vertical" TextMode="MultiLine"></asp:TextBox>
                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <div class="row">
                    <div class="col-lg-12 text-center">

                        <asp:LinkButton ClientIDMode="Static" ID="btnsalvar" runat="server" CssClass="btn btn-success "> <i class="fas fa-plus"></i> Salvar</asp:LinkButton>

                        <asp:LinkButton ClientIDMode="Static" ID="btncancelar" runat="server" CssClass="btn btn-danger "> <i class="fas fa-eraser"></i> Cancelar</asp:LinkButton>

                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hdndecision" />
                        <asp:Button runat="server" ID="btndecision" Style="visibility: hidden;" ClientIDMode="Static" Visible="true" />
                        <br>
                    </div>
                </div>

            </div>
            <br />

            <%--            <div class="row">
                <div class="col-lg-12">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div id="divgradeespecialidades" runat="server" class="row">
                                <br>
                                Carrega Grade
                <asp:GridView ID="gradeespecialidades" Style="width: 100%;" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro encontrado no momento!" CssClass="table-bordered" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("cod")%>'></asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="cod" HeaderText="Cod" />
                        <asp:BoundField DataField="Descricao" HeaderText="Descricao" />
                        <asp:BoundField DataField="plano" HeaderText="Plano" />
                        <asp:BoundField DataField="valor" HeaderText="Valor" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton CommandArgument="<%#Container.DataItemIndex %>" Text="Selecionar" runat="server" CssClass="btn btn-primary" ID="btnselectespec" CommandName="btnselect"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>--%>


            <div class="box box-primary" visible="false" id="divprocurarespec" runat="server">
                <div class="box-header">
                    Procurar caixa
                </div>
                <div class="box-body">
                    <div class="row">



                        <div class="col-lg-12" runat="server" id="mgradep">


                            <asp:GridView ID="grade" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" EmptyDataText="Não possui nenhuma especialidade para essa consulta" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                <AlternatingRowStyle BackColor="AntiqueWhite" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                            <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("cod") %>' />

                                            <asp:LinkButton ID="selecionar" runat="server" CssClass="btn btn-default btn-xs" CommandName="selecionar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Selecionar"> 
                                            <i class="fas fa-edit"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton CommandName="excluir" title="Excluir" CommandArgument='<%#Container.DataItemIndex%>' ClientIDMode="Static" ID="btnexcluir" runat="server" CssClass="btn btn-default btn-xs">
                                        <i class="fas fa-close"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton CommandName="reativar" title="Reativar" Visible="false" CommandArgument='<%#Container.DataItemIndex%>' ClientIDMode="Static" ID="btnreativar" runat="server" CssClass="btn btn-default btn-xs">
                                        <i class="fa fa-undo"></i>
                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="descricao" HeaderText="descricao" />
                                    <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                    <asp:BoundField DataField="plano" HeaderText="plano" />
                                    <asp:BoundField DataField="Valor" HeaderText="Valor" />

                                </Columns>

                            </asp:GridView>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-12">
                              <asp:CheckBox runat="server" Visible="false" Text="Inativo" AutoPostBack="true" CssClass="checkbox-inline" Checked="false" ID="chkexibirinativos" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).on('change', '#hdndecision', function () {
            if ($("#hdndecision").val() == "bloquear") {
                __doPostBack('UpdatePanel1', 'bloquear');
            }

            if ($("#hdndecision").val() == "reativar") {
                __doPostBack('UpdatePanel1', 'reativar');
            }

        });


        $(".btnreativar").click(function (e) {
            e.preventDefault();
            Swal.fire({
                title: 'Deseja reativar essa especialidade?',
                icon: 'warning',
                //text: "You won't be able to revert this!",
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Reativar',
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {
                    $("#hdndecision").val("reativar").trigger('change');
                }

            });
        });

        $(".btnbloquear").click(function (e) {
            e.preventDefault();
            Swal.fire({
                title: 'Deseja bloquear essa especialidade?',
                icon: 'warning',
                //text: "You won't be able to revert this!",
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Bloquear',
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {

                    $("#hdndecision").val("bloquear").trigger('change');

                }

            });

        });

    </script>
</asp:Content>

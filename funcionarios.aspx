﻿<%@ Page Title="" Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="funcionarios.aspx.vb" Inherits="restrito_funcionarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="style1.css" rel="stylesheet" />
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
    <script type="text/javascript" src="jquery-1.9.1.js"></script>
    <script type="text/javascript" src="core.js"></script>
    <script type="text/javascript" src="jquery.cookie.js"></script>
       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.min.js" type="text/javascript"></script>
    <script src="../js/jquery.Jcrop.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#<%=imgcrop.ClientID %>').Jcrop({
                onSelect: storeCoords,
                //Set Image Box Height & Width
                boxWidth: 800, boxHeight: 600
            });
        });

        //Function to store coordinates
        function storeCoords(c) {
            jQuery('#<%=hdnX.ClientID %>').val(c.x);
        jQuery('#<%=hdnY.ClientID %>').val(c.y);
        jQuery('#<%=hdnW.ClientID %>').val(c.w);
        jQuery('#<%=hdnH.ClientID %>').val(c.h);
        };

    </script>

    <script language="javascript" type="text/javascript">
        function fecharimagem(caminho) {


            $(".windowstatusmodelo").hide();
            $(".windowstatusmodelo").closest;

            //$("#imgperfil").attr("src", caminho);
            document.getElementById('imgperfil').src = caminho;
            //$("#imgperfil").show;
            debugger;

        }
        function fecharimagemvazio() {


            $(".windowstatusmodelo").hide();
            $(".windowstatusmodelo").closest;

            //$("#imgperfil").attr("src", caminho);
            //document.getElementById('imgperfil').src = caminho;
            //$("#imgperfil").show;
            debugger;

        }
        function exibirimagem() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);
            $('#imagemperfil').show();
            $('#imagemperfil').load();

        }
        function fecharmodelo(caminho) {


            $(".windowstatusmodelo").hide();
            $(".windowstatusmodelo").closest;

            //$("#imgperfil").attr("src", caminho);
            document.getElementById('imgperfil').src = caminho;
            //$("#imgperfil").show;
            debugger;

        }

        function exibirmodelo() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);
            $('#enviaremail').show();
            $('#enviaremail').load();

        }
        function exibirmodelo2() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);
            $('#imagemlegal').show();
            $('#imagemlegal').load();

        }
    </script>
    <style>
        .frame {
            height: 580px;
            width: 680px;
            border: none;
        }

        .frameemail {
            width: 1240px;
            height: 820px;
            border: none;
        }

        .windowsdth {
            border-width: 2px;
            border-style: solid;
            border-color: #800000;
            display: none;
            width: 1400px;
            height: 650px;
            position: absolute;
            left: 45px;
            top: 65px;
            background: white;
            z-index: 9900;
            padding: 5px;
            text-align: left;
            /*margin-top: 430px;*/
            -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
        }

        .windowsdocs {
            border-width: 2px;
            border-style: solid;
            border-color: #800000;
            display: none;
            width: 1400px;
            height: 650px;
            position: absolute;
            left: 45px;
            top: 65px;
            background: white;
            z-index: 9900;
            padding: 5px;
            text-align: left;
            /*margin-top: 430px;*/
            -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
        }

        .windowstatusmodelo {
            border-width: 2px;
            border-style: solid;
            border-color: #800000;
            display: none;
            width: 1400px;
            height: 650px;
            position: absolute;
            left: 65px;
            top: 65px;
            background: white;
            z-index: 9900;
            padding: 5px;
            text-align: left;
            /*margin-top: 430px;*/
            -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
        }
    </style>
    <br />

    <br />
    <div class="panel panel-primary">
        <div class="panel-heading">
            <center>Colaboradores</center>
        </div>
        <div class="panel-body">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <center>
                     <asp:Button ID="btnnovo" runat="server" Text="Novo" class="btn btn-primary" />
                                    </center>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <center>
                                    
                    <asp:ImageButton ID="imgperfil" runat="server" ImageUrl="~/img/semimagem01.png" clientidmode="static"  class="img-responsive img-circle" style="  -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);"></asp:ImageButton>
                                 
                      <%--ImageUrl="~/img/sem_foto.gif"--%>
                        </center>
                            </div>
                        </div>
                        <div class="col-lg-10">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <asp:Label ID="lblCodigo" runat="server" Text="Matrícula"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCodigo" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <asp:Label ID="lblnome" runat="server" Text="Nome"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtNome" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <br />
                                            <asp:CheckBox ID="chkAtivo" runat="server" Checked="True" Text="Ativo" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <asp:Label ID="lblcep0" runat="server" Text="Data de Cadastro"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtdtcad" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblcargo" runat="server" Text="Função"></asp:Label>
                                            <br />
                                            <asp:DropDownList ID="cbocargo" runat="server" class="form-control">
                                                <asp:ListItem>Programador</asp:ListItem>
                                                <asp:ListItem>Estagiário</asp:ListItem>
                                                <asp:ListItem>Suporte</asp:ListItem>
                                                <asp:ListItem>Administrativo</asp:ListItem>
                                                <asp:ListItem>Vendedor</asp:ListItem>
                                                <asp:ListItem>Diretor</asp:ListItem>
                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" Text="Usuario"></asp:Label>
                                            <asp:TextBox ID="txtusuario" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                       <div class="col-lg-6">
                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server" Text="Senha"></asp:Label>
                                            <asp:TextBox ID="txtsenha" runat="server" type="password" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <asp:Label ID="lblendreco" runat="server" Text="Endereço"></asp:Label>
                                            <asp:TextBox ID="txtendereco" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblBairro" runat="server" Text="Bairro"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtBairro" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblCidade" runat="server" Text="Cidade"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtcidade" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <asp:Label ID="lblestado" runat="server" Text="UF"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtestado" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblcep" runat="server" Text="CEP"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtcep" runat="server" onkeyup="formataCEP(this,event);" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <asp:Label ID="lbltelefone1" runat="server" Text="Telefone fixo"></asp:Label>
                                <br />
                                <asp:TextBox ID="txttel1" runat="server" onkeyup="formataTelefone(this,event);" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <asp:Label ID="lbltel2" runat="server" Text="Telefone celular"></asp:Label>
                                <br />
                                <asp:TextBox ID="txttel2" runat="server" onkeyup="formataTelefone(this,event);" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <asp:Label ID="lbemail" runat="server" Text="Email Pessoal"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtemail" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <asp:Label ID="lblemailsmart" runat="server" Text="Email Empresa"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtemailsmart" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <asp:Label ID="lblCPF" runat="server" Text="CPF"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtcpf" runat="server" onkeyup="formataCPF(this,event);" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <asp:Label ID="lblrg" runat="server" Text="RG"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtrg" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <asp:Label ID="lblusuario0" runat="server" Text="Admissão"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtdtadm" runat="server" onkeyup="formataData(this,event);" class="form-control" type="date"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <asp:Label ID="lblusuario1" runat="server" Text="Demissão"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtdtdem" runat="server" onkeyup="formataData(this,event);" class="form-control" type="date"></asp:TextBox>
                            </div>
                        </div>

                    </div>



                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <center>Dados Financeiros</center>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server" Text="Porcentagem comissão implantação"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtcomimp" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <asp:Label ID="lblcomissaomensa" runat="server" Text="Porcentagem comissão mensalidades"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtcompmens" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <asp:Label ID="lblcomissaotreinamento" runat="server" Text="Porcentagem comissão treinamento"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtcomtreino" runat="server" class="form-control"></asp:TextBox>
                                        <br />
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <asp:Label ID="lblbanco" runat="server" Text="Banco"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtbanco" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <asp:Label ID="lblagencia" runat="server" Text="Agência"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtagencia" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <asp:Label ID="lblconta" runat="server" Text="Conta"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtconta" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <center>Meus Arquivos Carregados</center>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblconta0" runat="server" Text="Descrição do arquivo"></asp:Label>

                                        <br />
                                        <asp:TextBox ID="txtdescarq" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <br />
                                        <asp:FileUpload ID="FileUpload1" runat="server" Height="32px" Width="400px" />



                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button ID="btnADDArq" runat="server" CssClass="btn btn-info" Text="      Adicionar      " />
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-lg-12">

                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="GradeArqs" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ShowHeaderWhenEmpty="True" Width="714px" ForeColor="Black" GridLines="Horizontal">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sel">
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="sel2" runat="server" />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="sel2" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="nrseq" HeaderText="Codigo" />
                                                    <asp:BoundField DataField="arquivo" HeaderText="Arquivo" />
                                                    <asp:BoundField DataField="descricao" HeaderText="Tipo de Arquivo" />

                                                    <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                                    <asp:ButtonField ButtonType="Button" CommandName="Excluir" Text="  Remover  ">

                                                        <ControlStyle CssClass="botaoredondo" />
                                                        <ItemStyle CssClass="botaored" />
                                                    </asp:ButtonField>
                                                    <asp:ButtonField ButtonType="Button" CommandName="Baixar" Text="  Baixar  ">

                                                        <ControlStyle CssClass="botaoredondo" />
                                                        <ItemStyle CssClass="botaored" />
                                                    </asp:ButtonField>
                                                </Columns>
                                                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                                <HeaderStyle BackColor="#333333" BorderStyle="Solid" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" ForeColor="Black" HorizontalAlign="Right" />
                                                <RowStyle BorderStyle="Solid" BorderWidth="1px" />
                                                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                                <SortedDescendingHeaderStyle BackColor="#242121" />
                                            </asp:GridView>
                                            <br />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <center>Minhas Competências</center>
                        </div>
                        <div class="panel-body">
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <center>
                                 <asp:Button ID="btnsalvar" runat="server" Text="Salvar" CssClass="btn btn-danger"  />
                                    </center>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <center>Time de Ouro</center>
                        </div>
                        <div class="panel-body">

                            <div class="row" style="margin-left: .5rem; margin-right: 1rem;">
                                <asp:UpdatePanel ID="updatepanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grade" runat="server" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sel">
                                                    <EditItemTemplate>
                                                        <asp:CheckBox ID="sel" runat="server" />
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="sel" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="nrseq" HeaderText="Codigo" />
                                                <asp:BoundField DataField="nome" HeaderText="Nome" />
                                                <asp:BoundField DataField="cargo" HeaderText="Cargo" />
                                                <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                                <asp:BoundField DataField="dtadm" HeaderText="Admissão" />
                                                <asp:BoundField DataField="dtdem" HeaderText="Demissão" />

                                                <asp:TemplateField HeaderText="Ações" runat="server">
                                                    <ItemTemplate runat="server">
                                                        <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                <i class="fas fa-pencil-alt"></i>
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="deletar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                <i class="fas fa-trash-alt"></i>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <asp:CheckBox ID="chkIgnorarInativos" runat="server" Checked="True" Text="Ignorar Inativos" />
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- InstanceEndEditable -->
                    </div>
                    <div id="imagemlegal" class="windowstatusmodelo">
                        <div class="panel panel-primary" style="margin-left: 5px; margin-right: 5px;">

                            <div class="panel-heading">
                                <center>
                Vamos carregar sua foto? Selecione a imagem desejada e clique em carregar!
               </center>
                            </div>
                            <div class="panel-body">
                                <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="alerta1" style="display: none;">


                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <strong>Atenção:</strong>
                                <asp:Label ID="lblerroconta1" runat="server" Text="Por favor, selecione uma carteira de clientes válida !"></asp:Label>

                            </div>
                        </div>
                       
                       
                   </ContentTemplate>
                </asp:UpdatePanel>--%>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <asp:FileUpload ID="FileUpload2" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <asp:UpdatePanel ID="UpdatePanelj" runat="server">
                                                <ContentTemplate>
                                                    <asp:Button ID="Button1" runat="server" Text="Carregar !" CssClass="btn btn-primary" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="Button1" />
                                                    <asp:PostBackTrigger ControlID="Button3" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>--%>
                                            <asp:Button ID="btnsair" runat="server" Text="Sair" CssClass="btn btn-warning" />
                                            <%--  </ContentTemplate> 
                                         </asp:UpdatePanel> --%>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <center>
                               <asp:Image ID="imgcrop" runat="server" ImageUrl="~/img/amigo.png" />
                               </center>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <asp:Button ID="Button2" runat="server" Text="Carregar" class="btn btn-primary" />
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:Button ID="Button3" runat="server" Text="Salvar" class="btn btn-primary" />
                                    </div>
                                </div>

                            </div>
                        </div>

                        <asp:Image ID="Image1" runat="server" />
                        <asp:HiddenField ID="hdnX" runat="server" />
                        <asp:HiddenField ID="hdnY" runat="server" />
                        <asp:HiddenField ID="hdnW" runat="server" />
                        <asp:HiddenField ID="hdnH" runat="server" />
                        <asp:HiddenField ID="imagemescondida" runat="server" />

                        <asp:Image ID="imgCropped" runat="server" />


                    </div>

                </ContentTemplate>

            </asp:UpdatePanel>
        </div>

        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>

                <div id="imagemperfil" class="windowstatusmodelo">


                    <div class="btfecharholder">
                        <%--<div class="btfechar" onclick="fecharmodalfichaatendimento()">--%>

                        <a href="#" class="btn btn-close btn-round btn-default" onclick="fecharimagemvazio()"><i
                            class="glyphicon glyphicon-remove "></i></a>
                        <br />
                        <br />
                    </div>
                    <div>

                        <iframe src="/socialnetwork/carregarimagem.aspx" class="frameemail"></iframe>


                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>


    </div>

</asp:Content>


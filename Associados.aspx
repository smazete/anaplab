﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage.master" CodeFile="Associados.aspx.vb" Inherits="Associados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <script type="text/javascript">

        function irlink(linkativo) {

            window.open(linkativo, '_blank');

        }

        function irlinkmesmpagina(linkativo) {

            window.location.href = linkativo;

            //window.open(linkativo);

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Dados de Associados </b>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <asp:Label Text="Buscar Por" runat="server" />
                <asp:DropDownList runat="server" CssClass="form-control" ID="cbobusca">
                    <asp:ListItem Text="Codigo" Value="nrseq" />
                    <asp:ListItem Text="Conta Corrente" Value="contacorrente" />
                    <asp:ListItem Text="Nome" Value="nome" />
                    <asp:ListItem Text="Matricula" Value="matricula" />
                    <asp:ListItem Text="CPF" Value="cpf" />
                </asp:DropDownList>
            </div>
            <div class="col-lg-6">
                <br />
                <asp:TextBox runat="server" ID="txtbusca" CssClass="form-control" />
            </div>
            <div class="col-lg-2 text-center">
                <br>
                <asp:LinkButton ID="btnbuscando" runat="server" CssClass="btn btn-primary "> <i class="fa fa-searc"></i> Buscar</asp:LinkButton>
            </div>
            <br>
            <div runat="server" id="divgradeassociado">
            <asp:GridView ID="gradeassociados" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                            <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="nrseq" HeaderText="Código" />
                    <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                    <asp:BoundField DataField="nome" HeaderText="Nome" />
                    <asp:BoundField DataField="cpf" HeaderText="CPF" />
                    <asp:BoundField DataField="telefone" HeaderText="Telefone" />
                </Columns>
            </asp:GridView></div>
        </div>
        <div class="box-body">
            <%--   <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:LinkButton ID="btnnovo" runat="server" CssClass="btn btn-warning " Visible="false"> <i class="fa fa-plus-circle"></i> Novo</asp:LinkButton>
                    <br>
                </div>
            </div>--%>

            <div class="row">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Dados Pessoais: 
                            <asp:Label ID="nomedoassociado" runat="server" />
                            <span class="label label-primary">
                                <asp:Label ID="txtativodesativo" runat="server" Style="font-size: 18px;">...</asp:Label></span></h3>
                        <div class="box-tools pull-right">
                            <!-- Buttons, labels, and many other things can be placed here! -->

                            <!-- Here is a label for example -->
                            <span class="label label-primary">
                                <asp:HiddenField runat="server" ID="hdnrseq" />
                                <asp:HiddenField runat="server" ID="hdmatricula" />
                                <asp:Label ID="txtnrseq" Text="Nrseq" runat="server" Style="font-size: 18px;">0</asp:Label></span>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <center>
                            <asp:LinkButton ID="btndadosassociados" runat="server" CssClass="btn btn-info btn-lg"  >
                            <i class="fa fa-pencil-square-o"></i> Associado</asp:LinkButton>
                            <asp:LinkButton ID="btnmensalidade" runat="server" CssClass="btn btn-success btn-lg"  >
                            <i class="fa fa-pencil-square-o"></i> Mensalidade</asp:LinkButton> 
                              <asp:LinkButton ID="btnjoias" runat="server" CssClass="btn btn-primary btn-lg" >
                            <i class="fa fa-diamond"></i> Joias </asp:LinkButton>
                                <asp:LinkButton ID="btnjoias2" runat="server" CssClass="btn btn-primary btn-lg" >
                            <i class="fa fa-diamond"></i> Joias 2 </asp:LinkButton>
                              <asp:LinkButton ID="btndocumentos" runat="server" CssClass="btn btn-warning btn-lg"  >
                            <i class="fa fa-files-o"></i> Documentos </asp:LinkButton>
                              <asp:LinkButton ID="btnacoes" runat="server" CssClass="btn btn-info btn-lg" >
                            <i class="fa fa-balance-scale"></i> Ações</asp:LinkButton>
                                   <asp:LinkButton ID="btndesfiliar" runat="server" CssClass="btn btn-danger btn-lg" >
                            <i class="fa fa-close" visible="false" ></i> Desfiliar</asp:LinkButton>                                
                                   <asp:LinkButton ID="btnreativar" runat="server" CssClass="btn btn-success btn-lg" >
                            <i class="fa fa-check" visible="false" ></i> Reativar</asp:LinkButton>
                                </center>
                        </div>
                        <%-- mostra dados do associados --%>
                        <div id="divdadosassociado" runat="server" visible="false">
                            <div class="row">
                                <div class="col-lg-2">
                                    <asp:Label Text="Matricula" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox ID="txtmatricula" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="col-lg-4">
                                    <asp:Label Text="Nome" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label Text="E-mail" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label Text="Nascimento" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtnasc" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-2">
                                    <asp:Label Text="CPF" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox runat="server" type="text" CssClass="form-control cnpjcpf" ID="txtcpf" ClientIDMode="Static" onkeyup="formataCnpjCpf(this,event);" MaxLength="13"></asp:TextBox>
                                </div>
                                <div class="col-lg-2">
                                    <asp:Label Text="RG" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox ID="txtrg" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-2">
                                    <asp:Label Text="Celular" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox ID="txtcelular" runat="server" CssClass="form-control" ClientIDMode="Static" onkeyup="formataTelefoneC(this,event);" MaxLength="15"></asp:TextBox>
                                </div>
                                <div class="col-lg-2">
                                    <asp:Label Text="Telefone" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox ID="txttelefone" runat="server" CssClass="form-control" ClientIDMode="Static" onkeyup="formataTelefone(this,event);" MaxLength="15"></asp:TextBox>

                                </div>
                                <div class="col-lg-1">
                                    <asp:Label Text="Sexo" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:DropDownList runat="server" ID="txtsexo" CssClass="form-control" ClientIDMode="Static">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>M</asp:ListItem>
                                        <asp:ListItem>F</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="col-lg-2">
                                            <asp:Label Text="CEP" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                            <asp:TextBox runat="server" type="text" AutoPostBack="true" CssClass="form-control cep" ID="txtcep" ClientIDMode="Static" MaxLength="9" onkeyup="formataCEP(this,event);" />
                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="col-lg-1">
                                    <asp:Label Text="Estado" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtestado" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <asp:Label Text="Cidade" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtcidade" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label Text="Endereço" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtendereco" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label Text="Bairro" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtbairro" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-1">
                                    <asp:Label Text="Numero" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-2">
                                    <asp:Label Text="Complemento" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtcomplemento" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <asp:Label Text="Nome Mãe" runat="server"></asp:Label><span class=" text-danger"><b>*</b></span>
                                    <asp:TextBox ID="txtnomemae" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <b>Dados de Acesso </b>
                                        </div>
                                        <div class="card-body">
                                            <br />
                                            <div class="col-lg-4" visible="false" runat="server">
                                                <b><asp:Label Text="USUARIO:" runat="server"></asp:Label></b>
                                            </div>
                                            <div class="col-lg-8" visible="false" runat="server">
                                                <asp:TextBox ID="txtmatricula2" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label Text="SENHA:" runat="server"></asp:Label></b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox ID="txtsenha" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card">
                                        <div class="card-header">
                                            <b>Dados do Associado </b>
                                        </div>
                                        <div class="card-body">
                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label ID="lblsituaçao" Text="Situação" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox ID="txtsituacaopb" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label Text="Data posse" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtposse" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label Text="Data Aposentadoria" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtaposentadoria" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <b>Dados Bancários</b>
                                            </div>
                                            <div class="card-body">
                                                <br />
                                                <div class="col-lg-12">
                                                    <div class="col-lg-4">
                                                        <b>
                                                            <asp:Label Text="Agencia" runat="server"></asp:Label>
                                                        </b>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <asp:TextBox ID="txtAgencia" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-4">
                                                        <b>
                                                            <asp:Label Text="Conta corrente" runat="server"></asp:Label>
                                                        </b>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <asp:TextBox ID="txtcontacorrente" runat="server" CssClass="form-control"></asp:TextBox>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <br />
                                <center>
                            <asp:LinkButton CssClass="btn btn-primary" ID="btnatualizar" Text="Atualizar" runat="server" />
                                </center>
                            </div>



                            <%--hisotirico--%>
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row text-align-center">
                                        Historico de alterações
                                          
                                    </div>

                                </div>

                                <div class="box-body" id="rowPosVenda" runat="server">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <asp:Label runat="server" for="txtmatricula">Data de Inclusão</asp:Label>
                                            <asp:TextBox runat="server" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdatainclusao" ClientIDMode="Static" Enabled="false" />

                                        </div>

                                        <div class="col-lg-3">
                                            <asp:Label runat="server" for="txtcadastrado">Cadastrado por</asp:Label>
                                            <asp:TextBox runat="server" type="text" CssClass="form-control" ID="txtcadastradopor" ClientIDMode="Static" Enabled="false" />

                                        </div>
                                        <div class="col-lg-1">
                                            <asp:Label runat="server" for="txtmatricula">Liberado</asp:Label>
                                            <div class="row">
                                                <asp:RadioButton runat="server" ID="liberadoS" GroupName="liberado" Text="Sim" />
                                                <asp:RadioButton runat="server" ID="liberadoN" GroupName="liberado" Text="Não" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-lg-2">
                                            <asp:Label runat="server" for="txtdtbaixa">Data da baixa</asp:Label>
                                            <asp:TextBox runat="server" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdtbaixa" ClientIDMode="Static" Enabled="false" />

                                        </div>

                                        <div class="col-lg-3">
                                            <asp:Label runat="server" for="txtbaixadopor">Baixado por</asp:Label>
                                            <asp:TextBox runat="server" type="text" CssClass="form-control dtmascara" ID="txtbaixadopor" ClientIDMode="Static" Enabled="false" />
                                        </div>

                                        <div class="col-lg-2">
                                            <asp:Label runat="server" for="txtdtalterado">Data da alteração</asp:Label>
                                            <asp:TextBox runat="server" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control " ID="txtdtalterado" ClientIDMode="Static" Enabled="false" />
                                        </div>

                                        <div class="col-lg-3">
                                            <asp:Label runat="server" for="txtalterado">Alterado por</asp:Label>
                                            <asp:TextBox runat="server" type="text" CssClass="form-control" ID="txtalterado" ClientIDMode="Static" Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-lg-2">
                                            <asp:Label runat="server" for="txtdtsuspensao">Data suspensão</asp:Label>
                                            <asp:TextBox runat="server" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdtsuspensao" ClientIDMode="Static" Enabled="false" />
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label runat="server" for="txtsuspenso">Suspenso por</asp:Label>
                                            <asp:TextBox runat="server" type="text" CssClass="form-control" ID="txtsuspenso" ClientIDMode="Static" Enabled="false" />
                                        </div>
                                        <div class="col-lg-2">
                                            <asp:Label runat="server" for="txtreativacao">Data reativação</asp:Label>
                                            <asp:TextBox runat="server" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtreativacao" ClientIDMode="Static" Enabled="false" />
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label runat="server" for="txtreativado">Reativado por</asp:Label>
                                            <asp:TextBox runat="server" type="text" CssClass="form-control" ID="txtreativado" ClientIDMode="Static" Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- mostradado mensalidades -->
                    <div id="divmensalidade" runat="server" visible="false">
                    
                        <div class="col-lg-3">
                            <b>
                                <asp:Label Text="Data Baixa" runat="server" /></b>
                            <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                            <div class="col-lg-3">
                                <br />
                                <asp:LinkButton ID="btnpdfmensaldiade" CssClass="btn btn-warning " Text="PDF" runat="server" />
                            </div>

                        <asp:GridView ID="grademensalidade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma mensalidade encontrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdvalor" runat="server" Value='<%#Bind("valor")%>'></asp:HiddenField>
                                        <asp:LinkButton ID="visualizar" runat="server" CssClass="btn btn-info " CommandName="visualizar" CommandArgument='<%#Container.DataItemIndex%>' Visible="false">
                            <i class="fa fa-eye"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                <asp:BoundField DataField="nome" HeaderText="Cliente" />
                                <asp:BoundField DataField="ativo" HeaderText="Ativo" Visible="false" />
                                <asp:BoundField DataField="statuspg" HeaderText="Status" />
                                <asp:BoundField DataField="mes" HeaderText="Mês" />
                                <asp:BoundField DataField="ano" HeaderText="Ano" />
                                <asp:BoundField DataField="Valor" HeaderText="Valor" />
                                <asp:BoundField DataField="dtpg" HeaderText="Data" />

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <p>
                                            <asp:CheckBox ID="chkok" runat="server" />
                                        </p>
                                        <div runat="server" id="divtexto">
                                            <asp:LinkButton ID="btnativo" runat="server" CssClass="btn btn-primary btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>Ativar
                            <i class="fa fa-check"></i> </asp:LinkButton>
                                        </div>
                                        <div runat="server" id="divbuttons">
                                            <asp:LinkButton ID="aberto" runat="server" CssClass="btn btn-default " CommandName="aberto" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-ban"></i> Aberto</asp:LinkButton>
                                            <%--                                    <asp:LinkButton ID="excluir" runat="server" CssClass="btn btn-danger " CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-ban"></i> Cancelar</asp:LinkButton>--%>
                                            <asp:LinkButton ID="pagar" runat="server" CssClass="btn btn-success  " CommandName="pagar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-dollar"></i> Baixa</asp:LinkButton>
                                            <asp:LinkButton ID="estornar" runat="server" CssClass="btn btn-warning " CommandName="estornar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-step-backward"></i> Estornar</asp:LinkButton>
                                            <asp:LinkButton ID="isento" runat="server" CssClass="btn btn-primary" CommandName="isento" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-step-backward"></i> Isento</asp:LinkButton>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <center>
        <asp:LinkButton Text="Baixar selecionados" runat="server" CssClass="btn btn-success btn-lg" ID="btnbaixaselect" />
            </center>
                    </div>
                    <div id="divjoias" runat="server" visible="false">


                        <asp:GridView ID="gradejoias" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma joia existente para este associado !" CssClass="table " GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdnrsequser" runat="server" Value='<%#Bind("nrsequser")%>'></asp:HiddenField>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                <asp:BoundField DataField="nome" HeaderText="Cliente" />
                                <asp:BoundField DataField="statuspg" HeaderText="Status" />
                                <asp:BoundField DataField="mes" HeaderText="Mes" />
                                <asp:BoundField DataField="ano" HeaderText="Ano" />

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <p>
                                            <asp:CheckBox ID="chkok" runat="server" />
                                        </p>
                                        <div runat="server" id="divtexto">
                                            <asp:LinkButton ID="btnativo" runat="server" CssClass="btn btn-primary btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>Ativar
                            <i class="fa fa-check"></i> </asp:LinkButton>
                                        </div>
                                        <div runat="server" id="divbuttons">
                                            <asp:LinkButton ID="aberto" runat="server" CssClass="btn btn-default " CommandName="aberto" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-ban"></i> Aberto</asp:LinkButton>
                                            <%--                                    <asp:LinkButton ID="excluir" runat="server" CssClass="btn btn-danger " CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-ban"></i> Cancelar</asp:LinkButton>--%>
                                            <asp:LinkButton ID="pagar" runat="server" CssClass="btn btn-success  " CommandName="pagar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-dollar"></i> Baixa</asp:LinkButton>
                                            <asp:LinkButton ID="estornar" runat="server" CssClass="btn btn-warning " CommandName="estornar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-step-backward"></i> Estornar</asp:LinkButton>
                                            <%--                            <asp:LinkButton ID="isento" runat="server" CssClass="btn btn-primary" CommandName="isento" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-step-backward"></i> Isento</asp:LinkButton>--%>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <center>
        <asp:LinkButton Text="Baixar selecionados" runat="server" CssClass="btn btn-success btn-lg" ID="btngradejoiasselect" />
            </center>
                    </div>


                    <div id="divjoias2" runat="server" visible="false">
                        <asp:GridView ID="gradejoias2" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma joia 2 existente para este associado !" CssClass="table " GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdnrsequser" runat="server" Value='<%#Bind("nrsequser")%>'></asp:HiddenField>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                <asp:BoundField DataField="nome" HeaderText="Cliente" />
                                <asp:BoundField DataField="statuspg" HeaderText="Status" />
                                <asp:BoundField DataField="mes" HeaderText="Mes" />
                                <asp:BoundField DataField="ano" HeaderText="Ano" />

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <p>
                                            <asp:CheckBox ID="chkok" runat="server" />
                                        </p>
                                        <div runat="server" id="divtexto">
                                            <asp:LinkButton ID="btnativo" runat="server" CssClass="btn btn-primary btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>Ativar
                            <i class="fa fa-check"></i> </asp:LinkButton>
                                        </div>
                                        <div runat="server" id="divbuttons">
                                            <asp:LinkButton ID="aberto" runat="server" CssClass="btn btn-default " CommandName="aberto" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-ban"></i> Aberto</asp:LinkButton>
                                            <%--                                    <asp:LinkButton ID="excluir" runat="server" CssClass="btn btn-danger " CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-ban"></i> Cancelar</asp:LinkButton>--%>
                                            <asp:LinkButton ID="pagar" runat="server" CssClass="btn btn-success  " CommandName="pagar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-dollar"></i> Baixa</asp:LinkButton>
                                            <asp:LinkButton ID="estornar" runat="server" CssClass="btn btn-warning " CommandName="estornar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-step-backward"></i> Estornar</asp:LinkButton>
                                            <%--                            <asp:LinkButton ID="isento" runat="server" CssClass="btn btn-primary" CommandName="isento" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-step-backward"></i> Isento</asp:LinkButton>--%>
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <center>
        <asp:LinkButton Text="Baixar selecionados" runat="server" CssClass="btn btn-success btn-lg" ID="btndarbaixaj2" />
            </center>
                    </div>






                    <div id="divacoes" runat="server" visible="false">
                        <asp:GridView ID="gradeaceos" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma ação vinculada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="nome" HeaderText="Nome" />
                                <asp:BoundField DataField="numero" HeaderText="Número" />
                                <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                <asp:TemplateField HeaderText="Consultar Processo">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdlink" runat="server" Value='<%#Bind("link")%>'></asp:HiddenField>
                                        <a runat="server" href='<%# Eval("link") %>' target="_blank" dir="auto">
                                            <asp:Label Text='<%# Eval("link") %>' runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Opções">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdmatricula" runat="server" Value='<%#Bind("matricula")%>'></asp:HiddenField>
                                        <div runat="server" id="divretirar">
                                            <asp:LinkButton ID="retirar" runat="server" CssClass="btn btn-danger btn-xs" CommandName="retirar" CommandArgument='<%#Container.DataItemIndex%>'>
                             <i class="fa fa-close"></i></asp:LinkButton>
                                        </div>
                                        <div runat="server" id="divativar">
                                            <asp:LinkButton ID="ativar" runat="server" CssClass="btn btn-success btn-xs" CommandName="ativar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-check-square-o"></i></asp:LinkButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                    <div id="divdocumentos" runat="server" visible="false">
                        <div class="row">

                            <div class="col-lg-1"></div>
                            <div class="col-lg-4">
                                <asp:Label runat="server" Text="Descricão"></asp:Label>
                                <asp:TextBox ID="txtdescridocassoc" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <%--                <div class="col-lg-2">
                    Arquivo
                    <br>
                    <asp:TextBox ID="txtarquivo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>--%>
                            <div class="col-lg-1">
                                <br>
                                <asp:CheckBox ID="chkativo" runat="server" Text="ATIVO" Checked="true" Enabled="false"></asp:CheckBox>
                            </div>
                            <div class="col-lg-3">
                                <br />
                                <asp:FileUpload ID="upload01" runat="server" CssClass="btn btn-default" autopostback="true" />
                                <br />
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-2">
                                <br />
                                <asp:LinkButton ID="btnupload" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i>Salvar</asp:LinkButton>
                            </div>
                        </div>

                        <asp:GridView ID="gradedocumentos" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum documento cadastrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdarquivo" runat="server" Value='<%#Bind("arquivo")%>'></asp:HiddenField>
                                        <asp:LinkButton ID="baixar" runat="server" CssClass="btn btn-info btn-xs" CommandName="baixar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-download" ></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                <asp:BoundField DataField="nome" HeaderText="Descricão" />

                                <asp:BoundField DataField="matricula" HeaderText="matricula" />
                                <asp:BoundField DataField="arquivo" HeaderText="Arquivo" />
                                <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div runat="server" id="divmostrabtn">
                                            <asp:LinkButton ID="excluir" runat="server" CssClass="btn btn-danger btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                <i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </div>
</asp:Content>


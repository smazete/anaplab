﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoesautoatendimento
Imports System.IO
Imports System.Data

Partial Class autoatendimento
    Inherits System.Web.UI.Page

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Private Sub autoatendimento_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("idassociado") = "" Then

            Response.Redirect("loginassociados.aspx")
        End If

        verificajoias()
        caixapostal()
        cadastroperc()
        acoesativas()
        documentosativos()
        mensalidades()
        joias()

    End Sub


    Private Sub verificajoias()

        Dim count As Integer = 0

        tb1 = tab1.conectar("select * from tbmensalidade_joias where nrsequser = '" & Session("idassociado") & "' ")
        If tb1.Rows.Count > 0 Then
            divajuda.Visible = False
            divjoiasmenu.Visible = True
        End If

        txtjoias.Text = tb1.Rows.Count.ToString()
    End Sub

    Private Sub cadastroperc()

        Dim count As Integer = 0
        Dim valor As String
        Dim x As String = "4"


        tb1 = tab1.conectar("select * from tbAssociados where nrseq = '" & Session("idassociado") & "'")

        If tb1.Rows(0)("matricula").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("nome").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("email").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("dtnasc").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("cpf").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("rg").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("celular").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("nomemae").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("situacaopb").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("sexo").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("cep").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("endereco").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("numero").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("complemento").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("bairro").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("cidade").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("estado").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("telefone").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("agencia").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("contacorrente").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("senha").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("dtposse").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("dtaposentadoria").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("ativo").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If
        If tb1.Rows(0)("dtcad").ToString <> "" Then
            count = count + 1
            valor = valor + Convert.ToDecimal(x)
        End If


        txtcadastro.Text = valor
    End Sub


    Private Sub acoesativas()

        Dim count As Integer

        tb1 = tab1.conectar("select * from tbacoes_detalhes inner join tbacoes on tbacoes_detalhes.nrseqacoes = tbacoes.nrseq where tbacoes_detalhes.ativo = 1 and tbacoes_detalhes.matricula = '" & Session("idmatricula") & "' and  tbacoes.ativo = '1' order by  tbacoes.nrseq desc")

        txtacoes.Text = tb1.Rows.Count.ToString()

    End Sub

    Private Sub caixapostal()

        Dim count As Integer

        tb1 = tab1.conectar("select * from tbcaixapostal where nrseqassociado = '" & Session("idassociado") & "' and ativo = '1'")

        txtcaixapostal.Text = tb1.Rows.Count.ToString()

    End Sub

    Private Sub documentosativos()

        Dim count As Integer

        tb1 = tab1.conectar("select * from tbdocumentosassociados where matricula = '" & Session("idmatricula") & "' and ativo = '1'")

        txtdocumentos.Text = tb1.Rows.Count.ToString()

    End Sub

    Private Sub mensalidades()
        Dim count As Integer

        tb1 = tab1.conectar("select * from tbmensalidades where nrseqcliente = '" & Session("idassociado") & "'")

        txtmensalidades.Text = tb1.Rows.Count.ToString()
    End Sub

    Private Sub joias()

        Dim count As Integer

        tb1 = tab1.conectar("select * from tbmensalidade_joias where nrsequser = '" & Session("idassociado") & "'")

        txtjoias.Text = tb1.Rows.Count.ToString()

        If tb1.Rows.Count.ToString() <> "0" Then
            divjoiasmenu.Visible = True
        End If
    End Sub

End Class

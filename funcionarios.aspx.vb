﻿Imports System.Drawing
Imports System.Drawing.Drawing2D

Imports clsSmart
Imports System.IO
Partial Class restrito_funcionarios
    Inherits System.Web.UI.Page
    Dim tbvendedores As New Data.DataTable
    Dim tabvendedores As New clsBanco
    Dim tbsalvar As New Data.DataTable
    Dim tabsalvar As New clsBanco
    Dim m_sImageNameUserUpload As String
    Dim m_sImageNameGenerated As String
    Private Sub restrito_funcionarios_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = True Then
            Exit Sub
        End If
        'imagemescondida.Value = "~/img/sem_foto.gif"
        If Session("usuario") = "" Then
            Response.Redirect("default.aspx")
            Exit Sub
        End If
        listafunc()

        habilitar(False)

        btnsalvar.Enabled = False
    End Sub
    Private Sub habilitar(Optional valor As Boolean = True)

        Dim valor2 As Boolean = True
        If valor Then
            valor2 = False
        End If
        txtdescarq.ReadOnly = valor
        FileUpload1.Enabled = valor
        btnADDArq.Enabled = valor2
        txtendereco.ReadOnly = valor
        txtBairro.ReadOnly = valor
        txtcidade.ReadOnly = valor
        txtestado.ReadOnly = valor
        txtcep.ReadOnly = valor
        txtNome.ReadOnly = valor
        txttel1.ReadOnly = valor
        txttel2.ReadOnly = valor
        txtcomimp.ReadOnly = valor
        txtcompmens.ReadOnly = valor
        txtcomtreino.ReadOnly = valor
        txtbanco.ReadOnly = valor
        txtcpf.ReadOnly = valor
        txtrg.ReadOnly = valor
        txtconta.ReadOnly = valor
        txtagencia.ReadOnly = valor
        txtemail.ReadOnly = valor
        txtemailsmart.ReadOnly = valor
        txtdtadm.ReadOnly = valor
        txtdtdem.ReadOnly = valor
        txtCodigo.ReadOnly = valor
        txtusuario.ReadOnly = valor
        txtsenha.ReadOnly = valor
        FileUpload1.Enabled = valor2
    End Sub
    Private Sub limpa()
        txtNome.Text = ""
        txttel1.Text = ""
        txttel2.Text = ""
        txtcomimp.Text = ""
        txtcompmens.Text = ""
        txtcomtreino.Text = ""
        txtbanco.Text = ""
        txtcpf.Text = ""
        txtrg.Text = ""
        txtconta.Text = ""
        txtagencia.Text = ""
        cbocargo.Text = ""
        txtemail.Text = ""
        txtemailsmart.Text = ""
        txtdtadm.Text = ""
        txtusuario.Text = ""
        txtsenha.Text = ""
        txtdtdem.Text = ""
        txtCodigo.Text = ""
        txtendereco.Text = ""
        txtBairro.Text = ""
        txtcidade.Text = ""
        txtestado.Text = ""
        txtcep.Text = ""

    End Sub
    Private Sub listafunc()
        Dim tbfun As New Data.DataTable
        Dim tabfun As New clsBanco
        Dim sql As String = "Select * from tbcolaboradores where " & IIf(chkIgnorarInativos.Checked, "  (ativo = true  or dtdem <> '1900/01/01')", " 1=1 ") & IIf(Session("grupo") = "Adm", "", " and usuario = '" & Session("usuario") & "'") & " and nrseqempresa = " & Session("idempresaemuso") & " order by nome"

        tbfun = tabfun.conectar(sql)
        Grade.DataSource = tbfun
        Grade.DataBind()
    End Sub
    Private Sub listaarquivos()
        Dim wcnrlote As Integer = 0
        If txtCodigo.Text = "" Then Exit Sub
        wcnrlote = txtCodigo.Text
        Dim tbarq As New Data.DataTable
        Dim tabarq As New clsBanco

        tbarq = tabarq.conectar("select * from tbcolaboradores_arquivos where ativo = true and nrseqvendedor = " & wcnrlote & " order by nrseq desc")

        GradeArqs.DataSource = tbarq
        GradeArqs.DataBind()
    End Sub

    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click
        Dim nrseqctrl As String = gerarnrseqcontrole()



        tbvendedores = tabvendedores.IncluirAlterarDados("insert into tbcolaboradores (dtcad,ativo,nrseqctrl, salvo) values('" & formatadatamysql(Date.Now.Date) & "',false,'" & nrseqctrl & "', false)")




        tbvendedores = tabvendedores.conectar("select * from tbcolaboradores where nrseqctrl='" & nrseqctrl & "'")
        txtCodigo.Text = tbvendedores.Rows(0)("nrseq").ToString
        txtdtcad.Text = tbvendedores.Rows(0)("dtcad").ToString
        btnnovo.Enabled = False
        txtdtcad.Enabled = False
        habilitar(False)

        btnsalvar.Enabled = True
        txtNome.Focus()
    End Sub

    Private Sub btnADDArq_Click(sender As Object, e As EventArgs) Handles btnADDArq.Click
        If sonumeros(txtCodigo.Text) = "" Then


            Exit Sub
        End If
        Dim tbarq As New Data.DataTable
        Dim tabarq As New clsBanco
        Dim uploadedFile As HttpPostedFile = FileUpload1.PostedFile
        Dim FileName As String = "", nomecompleto As String = ""
        Dim FolderPath As String = ConfigurationManager.AppSettings("FolderPath")

        Dim FilePath As String = Server.MapPath("~\docfuncionarios\" & FileName)

        FilePath = Server.MapPath("~\docfuncionarios\")
        FileName = "arqfunc" & sonumeros(txtCodigo.Text) & "_" & Directory.GetFiles(FilePath).Length + 1 & "." & encontrachr(FileUpload1.FileName.ToString, ".", 0)
        nomecompleto = FilePath & FileName
        FilePath = Server.MapPath("~\docfuncionarios\" & FileName)

        FileUpload1.SaveAs(FilePath)


        tbarq = tabarq.IncluirAlterarDados("insert into tbvendedores_arquivos (nrseqvendedor, arquivo, arqcompleto, dtcad, usercad, ativo, descricao) values (" & txtCodigo.Text & ",'" & FileName & "','" & nomecompleto & "','" & formatadatamysql(data) & "','" & Session("usuario").ToString & "',true,'" & txtdescarq.Text & "')")
        listaarquivos()
    End Sub

    Private Sub imgperfil_Click(sender As Object, e As ImageClickEventArgs) Handles imgperfil.Click
        If Session("nrseqcolaborador") = "" Then

            Exit Sub
        End If

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibirimagem()", "exibirimagem()", True)



    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sImageName As String = ""
        Dim sImagePathImages As String = Server.MapPath("~\social")
        Session("ImageName") = ""
        If (FileUpload1.HasFile) Then

            sImageName = Guid.NewGuid().ToString().Substring(0, 8)
            Dim sFileExt As String = Path.GetExtension(FileUpload1.FileName)
            m_sImageNameUserUpload = sImageName + sFileExt
            m_sImageNameGenerated = Path.Combine(sImagePathImages, m_sImageNameUserUpload)
            FileUpload1.PostedFile.SaveAs(m_sImageNameGenerated)
            Session("ImageName") = m_sImageNameUserUpload
        End If

        imgcrop.ImageUrl = "/social/" + Session("ImageName")
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibirmodelo2()", "exibirmodelo2()", True)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Session("ImageName") = "" Then Exit Sub
        '

        Dim m_sImageNameUserUpload As String = Session("ImageName")
        If (hdnW.Value <> "") Then


            Dim sImagePathImages As String = Server.MapPath("~\social\")
            Dim iHeight As Integer = Convert.ToInt32(hdnH.Value)
            Dim iWidth As Integer = Convert.ToInt32(hdnW.Value)
            Dim Int As Integer = Convert.ToInt32(hdnH.Value)
            Dim iXCoord As Integer = Convert.ToInt32(hdnX.Value)
            Dim iYCoord As Integer = Convert.ToInt32(hdnY.Value)
            '//Call CropImage method defined below
            Dim byt_CropImage = CropImage(sImagePathImages + m_sImageNameUserUpload, iWidth, iHeight, iXCoord, iYCoord)
            Dim oMemoryStream As MemoryStream = New MemoryStream(byt_CropImage, 0, byt_CropImage.Length)
            oMemoryStream.Write(byt_CropImage, 0, byt_CropImage.Length)

            Dim oCroppedImage As System.Drawing.Image = System.Drawing.Image.FromStream(oMemoryStream, True)
            Dim sSaveTo As String = sImagePathImages + "T" & m_sImageNameUserUpload
            oCroppedImage.Save(sSaveTo, oCroppedImage.RawFormat)

            imgCropped.ImageUrl = "images/" + m_sImageNameUserUpload
            Session("imgret") = "images/" + m_sImageNameUserUpload
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "fecharmodaldocsparent1()", "fecharmodaldocsparent1()", True)
        Else


        End If
    End Sub

    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click
        Dim sql As String = "update tbcolaboradores set nome = '" & txtNome.Text & "', tel1 = '" & txttel1.Text & "', tel2 = '" & txttel2.Text & "', endereco = '" & txtendereco.Text & "', bairro = '" & txtbanco.Text & "', cidade = '" & txtcidade.Text & "', uf = '" & txtestado.Text & "', cep = '" & txtcep.Text & "', emailsmart = '" & txtemailsmart.Text & "', email = '" & txtemail.Text & "', agencia = '" & txtagencia.Text & "', conta = '" & txtconta.Text & "', banco = '" & txtbanco.Text & "', rg = '" & txtrg.Text & "', cpf = '" & txtcpf.Text & "', dtadm = '" & formatadatamysql(txtdtadm.Text) & "', cargo = '" & cbocargo.Text & "', comissaovenda = " & moeda(txtcomimp.Text) & ", comissaomanu = " & moeda(txtcompmens.Text) & ", comissaotreinamento = " & moeda(txtcomtreino.Text) & ", nrseqempresa = " & Session("idempresaemuso") & ", ativo = true, salvo = true, usuario= '" & txtusuario.Text & "', senha= '" & txtsenha.Text & "' where nrseq = " & txtCodigo.Text
        tbsalvar = tabsalvar.IncluirAlterarDados(sql)
        limpa()
        habilitar(True)
        btnnovo.Enabled = True
        btnsalvar.Enabled = True

    End Sub

    Private Sub btnnovo_Command(sender As Object, e As CommandEventArgs) Handles btnnovo.Command

    End Sub

    Private Function CropImage(sImagePath As String, iWidth As Integer, iHeight As Integer, iX As Integer, iY As Integer)
        Try
            Dim oOriginalImage As System.Drawing.Image = System.Drawing.Image.FromFile(sImagePath)
            Dim oBitmap As System.Drawing.Image = New System.Drawing.Bitmap(iWidth, iHeight)
            ' oBitmap.SetResolution(oOriginalImage.HorizontalResolution, oOriginalImage.VerticalResolution)
            Dim Graphic As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(oBitmap)
            Graphic.SmoothingMode = SmoothingMode.AntiAlias
            Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic
            Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality
            Graphic.DrawImage(oOriginalImage, New System.Drawing.Rectangle(0, 0, iWidth, iHeight), iX, iY, iWidth, iHeight, System.Drawing.GraphicsUnit.Pixel)
            Dim oMemoryStream As MemoryStream = New MemoryStream()
            oBitmap.Save(oMemoryStream, oOriginalImage.RawFormat)
            Return oMemoryStream.GetBuffer()

        Catch ex As Exception
            Dim teste As String = ex.Message
            teste &= "1"
        End Try

    End Function

    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function
    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function

    Private Sub Grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Grade.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = Grade.Rows(index)

        Dim nrseq As String = row.Cells(1).Text

        If e.CommandName = "editar" Then

            carregaFunc(nrseq)

        End If

        If e.CommandName = "deletar" Then

            Dim sqlSalvar = "UPDATE tbcolaboradores SET ativo = " & False & ", userexclui = '" & Session("usuario") & "', dtexclui = '" & formatadatamysql(Date.Now.Date) & "' WHERE nrseq = " & nrseq

            Dim datatable As Data.DataTable = persist(sqlSalvar)

            '  procura()
            listafunc()

        End If

    End Sub

    Private Sub carregaFunc(nrseq As String)
        Dim datatable As Data.DataTable = findSql("SELECT * FROM tbcolaboradores WHERE nrseq = '" & nrseq & "'")
        If datatable.Rows.Count <> 0 Then
            Session("nrseqirregulariedade") = datatable.Rows(0)("nrseq").ToString
        Else
            Session("nrseqirregulariedade") = 1
        End If
        habilitar(False)
        txtCodigo.Text = datatable.Rows(0)("nrseq").ToString
        txtdtcad.Text = datatable.Rows(0)("dtcad").ToString

        txtNome.Text = datatable.Rows(0)("nome").ToString
        cbocargo.SelectedValue = datatable.Rows(0)("cargo").ToString
        txtusuario.Text = datatable.Rows(0)("usuario").ToString
        txtsenha.Text = datatable.Rows(0)("senha").ToString

        txtendereco.Text = datatable.Rows(0)("endereco").ToString
        txtBairro.Text = datatable.Rows(0)("bairro").ToString
        txtcidade.Text = datatable.Rows(0)("cidade").ToString
        txtestado.Text = datatable.Rows(0)("uf").ToString
        txtcep.Text = datatable.Rows(0)("cep").ToString

        txttel1.Text = datatable.Rows(0)("tel1").ToString
        txttel2.Text = datatable.Rows(0)("tel2").ToString
        txtemail.Text = datatable.Rows(0)("email").ToString
        txtemailsmart.Text = datatable.Rows(0)("emailsmart").ToString

        txtcpf.Text = datatable.Rows(0)("cpf").ToString
        txtrg.Text = datatable.Rows(0)("rg").ToString
        txtdtadm.Text = datatable.Rows(0)("dtadm").ToString
        txtdtdem.Text = datatable.Rows(0)("dtdem").ToString

        txtcomimp.Text = datatable.Rows(0)("comissaovenda").ToString
        txtcompmens.Text = datatable.Rows(0)("comissaomanu").ToString
        txtcomtreino.Text = datatable.Rows(0)("comissaotreinamento").ToString
        txtbanco.Text = datatable.Rows(0)("banco").ToString
        txtagencia.Text = datatable.Rows(0)("agencia").ToString
        txtconta.Text = datatable.Rows(0)("conta").ToString
        btnsalvar.Enabled = True
    End Sub
End Class

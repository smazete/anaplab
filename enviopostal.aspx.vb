﻿Imports System.IO
Imports clsSmart

Partial Class enviopostal
    Inherits System.Web.UI.Page


    Private Sub enviopostal_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If


    End Sub

    Private Sub btncaixadeentrada_Click(sender As Object, e As EventArgs) Handles btncaixadeentrada.Click
        divescolha.Visible = False
        divescolha1.Visible = True
        divescolha2.Visible = False
        divpublica.Visible = False
        divprivado.Visible = False
    End Sub

    Private Sub btnprivadas_Click(sender As Object, e As EventArgs) Handles btnprivadas.Click
        divescolha.Visible = False
        divescolha1.Visible = False
        divescolha2.Visible = True
        divpublica.Visible = False
        divprivado.Visible = True
        lblheader2.Text = "ENVIO MENSAGEM PRIVADA"
    End Sub

    Private Sub btnpublicas_Click(sender As Object, e As EventArgs) Handles btnpublicas.Click
        divescolha.Visible = False
        divescolha1.Visible = False
        divescolha2.Visible = True
        divprivado.Visible = False
        divpublica.Visible = True
        lblheader2.Text = "ENVIO MENSAGEM PUBLICA"
    End Sub

    Private Sub btnvoltar_Click(sender As Object, e As EventArgs) Handles btnvoltar.Click

        divescolha1.Visible = False
        divescolha2.Visible = False
        divpublica.Visible = False
        divprivado.Visible = False
        divescolha.Visible = True
    End Sub
End Class

﻿Imports System.Data
Imports clsSmart

Partial Class relatorioclientes
    Inherits System.Web.UI.Page
    Dim wcregistro As Integer = 0
    Dim tb1 As New Data.DataTable, tbdep As New Data.DataTable
    Dim tab1 As New clsBanco, tabdep As New clsBanco
    Private Sub relatorioclientes_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        carregar()
    End Sub

    Private Sub carregar()

        Dim tbx As New Data.DataTable
        tbx.Columns.Add("matricula")
        tbx.Columns.Add("nome")
        tbx.Columns.Add("status")
        tbx.Columns.Add("plano")
        tbx.Columns.Add("cpf")
        tbx.Columns.Add("dtnasc")
        tbx.Columns.Add("dtcad")

        Dim xsql As String = "select * from tbclientes where (nome <> '' or not nome is null)"
        Dim wcdata As String = ""

        If optdata_cadastro.Checked Then
            wcdata = "dtcad"
        End If

        If optdata_venda.Checked Then
            wcdata = "dtvenda"
        End If

        If optdata_negativacao.Checked Then
            wcdata = "dtnegativado"
        End If
        If optdata_suspensao.Checked Then
            wcdata = "dtsuspenso"
        End If

        If IsDate(txtdtinicial.Text) Then
            xsql &= " And " & wcdata & " >= '" & formatadatamysql(txtdtinicial.Text) & "'"
        End If
        If IsDate(txtdtfinal.Text) Then
            xsql &= " and " & wcdata & " <= '" & formatadatamysql(txtdtfinal.Text) & "'"
        End If

        If optsituacao_ativos.Checked Then
            xsql &= " and liberado = true "
        End If
        If optsituacao_cancelados.Checked Then
            xsql &= " and liberado = false "
        End If

        If chknegativados.Checked Then
            xsql &= " and negativado = true"
        End If

        If chkSuspensos.Checked Then
            xsql &= " and suspenso = true"
        End If

        If optordem_matricula.Checked Then

            xsql &= " order by matricula"
        End If

        If optordem_nome.Checked Then

            xsql &= " order by nome"
        End If

        If optordem_data.Checked Then

            xsql &= " order by dtcad"
        End If

        tb1 = tab1.conectar(xsql)
        tbdep = tabdep.conectar("select * from tbdependentes_map where (nome <> '' or not nome is null)")

        For x As Integer = 0 To tb1.Rows.Count - 1
            tbx.Rows.Add()
            tbx.Rows(tbx.Rows.Count - 1)("matricula") = tb1.Rows(x)("matricula").ToString
            tbx.Rows(tbx.Rows.Count - 1)("nome") = tb1.Rows(x)("nome").ToString
            tbx.Rows(tbx.Rows.Count - 1)("dtcad") = tb1.Rows(x)("dtcad").ToString
            tbx.Rows(tbx.Rows.Count - 1)("dtnasc") = tb1.Rows(x)("dtnasc").ToString
            tbx.Rows(tbx.Rows.Count - 1)("plano") = tb1.Rows(x)("plano").ToString
            tbx.Rows(tbx.Rows.Count - 1)("cpf") = tb1.Rows(x)("cpf").ToString
            tbx.Rows(tbx.Rows.Count - 1)("status") = "Titular"

            If chkexibirdependentes.Checked Then
                Dim tbproc As Data.DataRow()
                tbproc = tbdep.Select(" matricula = '" & tb1.Rows(x)("matricula").ToString & "'")
                If tbproc.Count > 0 Then
                    For y As Integer = 0 To tbproc.Count - 1
                        tbx.Rows.Add()
                        tbx.Rows(tbx.Rows.Count - 1)("matricula") = tbproc(y)("matricula").ToString
                        tbx.Rows(tbx.Rows.Count - 1)("nome") = tbproc(y)("nome").ToString
                        tbx.Rows(tbx.Rows.Count - 1)("dtcad") = tbproc(y)("dtcad").ToString
                        tbx.Rows(tbx.Rows.Count - 1)("dtnasc") = tbproc(y)("dtnasc").ToString
                        tbx.Rows(tbx.Rows.Count - 1)("plano") = tbproc(y)("plano").ToString
                        tbx.Rows(tbx.Rows.Count - 1)("cpf") = tbproc(y)("cpf").ToString
                        tbx.Rows(tbx.Rows.Count - 1)("status") = "Dependente"
                    Next
                End If
            End If
        Next

        gdvProcuraDeCarnes.DataSource = tbx
        gdvProcuraDeCarnes.DataBind()

    End Sub

    Private Sub gdvProcuraDeCarnes_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles gdvProcuraDeCarnes.ItemDataBound
        If e.Item.ItemIndex < 0 Then
            wcregistro = 0
        Else
            Dim lblitemgrade As Label = e.Item.FindControl("lblitemgrade")

            If e.Item.Cells(3).Text = "Titular" Then
                wcregistro += 1
                lblitemgrade.Text = wcregistro
                e.Item.Cells(3).BackColor = System.Drawing.Color.Green
                e.Item.Cells(3).ForeColor = System.Drawing.Color.White
            Else
                e.Item.Cells(3).BackColor = System.Drawing.Color.Blue
                e.Item.Cells(3).ForeColor = System.Drawing.Color.White
            End If


            If e.Item.Cells(8).Text = "Sim" Then
                e.Item.ForeColor = System.Drawing.Color.Green
                If e.Item.Cells(10).Text = "Sim" Then
                    e.Item.ForeColor = System.Drawing.Color.Black
                    e.Item.Font.Overline = True
                End If
                If e.Item.Cells(9).Text = "Sim" Then
                    e.Item.ForeColor = System.Drawing.Color.Black
                    e.Item.Font.Overline = True
                End If
            Else
                e.Item.ForeColor = System.Drawing.Color.Red
            End If

        End If


    End Sub





    'Dim tabelaIrregulariedades As String = " tbirregularidades_map"
    'Dim tabelaIrregulariedadesdth As String = " tbirregularidadesdth_map"
    'Dim tabelaPlanos As String = " tbplanos_map"
    'Dim tabelaEmpresa As String = " tbempresas"
    'Dim tabelaClientes As String = " tbclientes_map"

    'Private Sub btnexport_Click(sender As Object, e As EventArgs) Handles btnexport.Click

    '    Dim fdata As String = rblFiltroData.SelectedValue
    '    Dim irr As String = rblServico.SelectedValue
    '    Dim sit As String = rdlIrregulares.SelectedValue
    '    Dim ord As String = rblOrdernar.SelectedValue
    '    Dim i As String = txtdtinicial.Value
    '    Dim f As String = txtdtfinal.Value

    '    Dim xsql As String = ""
    '    Dim wccampo As String = ""
    '    Dim ordena As String = ""

    '    ' operador busca entre tipo de data

    '    If fdata = 5 Then
    '        wccampo = "dtnegativado"
    '    ElseIf fdata = 4 Then
    '        wccampo = "dtprimeiraparcela"
    '    ElseIf fdata = 3 Then
    '        wccampo = "dtvenda"
    '    ElseIf fdata = 2 Then
    '        wccampo = "dtsuspenso"
    '    Else
    '        wccampo = "dtcad"
    '    End If

    '    'seleciona uma  das opções buscar no periodo datas'

    '    If IsDate(txtdtinicial.Value) Then

    '        xsql &= "" & wccampo & " >= '" & formatadatamysql(txtdtinicial.Value) & "'"
    '    End If

    '    If IsDate(txtdtfinal.Value) Then

    '        xsql &= " and " & wccampo & " <= '" & formatadatamysql(txtdtfinal.Value) & "'"
    '    End If

    '    ' situação do cliente
    '    If sit = 3 Then
    '        xsql &= " and ativos = 0"
    '    ElseIf sit = 2 Then
    '        xsql &= " and ativos = 1"
    '    Else
    '        ordena = ""
    '    End If

    '    ' irregularidade do cliente do cliente
    '    If sit = 5 Then
    '        xsql &= " and negativado = 0 and supenso = 0"
    '    ElseIf sit = 4 Then
    '        xsql &= " and negativado = 1 and supenso = 1"
    '    ElseIf sit = 3 Then
    '        xsql &= " and negativado = 1"
    '    ElseIf sit = 2 Then
    '        xsql &= " and suspenso = 1"
    '    Else
    '    End If

    '    ' ordena por
    '    If ord = 3 Then
    '        ordena = "" & wccampo & ""
    '    ElseIf ord = 2 Then
    '        ordena = "matricula"
    '    Else
    '        ordena = "nome"
    '    End If



    '    Session("consultasql") = xsql

    '    Dim tb1 As New Data.DataTable
    '    Dim tab1 As New clsBanco

    '    tb1 = tab1.conectar("SELECT matricula, nome, plano, cpf, rg,  DATE_FORMAT(dtnasc, '%d/%m/%Y') As dtnasc, telresidencia, telcelular,  DATE_FORMAT(dtcad, '%d/%m/%Y') As dtcad, liberado  FROM tbclientes_map where " & xsql & " ORDER BY " & ordena & " ")
    '    If tb1.Rows.Count = 0 Then
    '        Exit Sub
    '    End If

    '    Session("datainicial") = FormatDateTime(txtdtinicial.Value, vbShortDate)
    '    Session("datafinal") = FormatDateTime(txtdtfinal.Value, vbShortDate)


    '    gdvProcuraDeCarnes.DataSource = tb1
    '    gdvProcuraDeCarnes.DataBind()


    'End Sub


    'Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click

    '    Dim fdata As String = rblFiltroData.SelectedValue
    '    Dim irr As String = rblServico.SelectedValue
    '    Dim sit As String = rdlIrregulares.SelectedValue
    '    Dim ord As String = rblOrdernar.SelectedValue
    '    Dim i As String = txtdtinicial.Value
    '    Dim f As String = txtdtfinal.Value
    '    Dim link As String = ""

    '    Dim xsql As String = ""
    '    Dim wccampo As String = ""
    '    Dim ordena As String = ""

    '    ' operador busca entre tipo de data

    '    If fdata = 5 Then
    '        wccampo = "dtnegativado"
    '    ElseIf fdata = 4 Then
    '        wccampo = "dtprimeiraparcela"
    '    ElseIf fdata = 3 Then
    '        wccampo = "dtvenda"
    '    ElseIf fdata = 2 Then
    '        wccampo = "dtsuspenso"
    '    Else
    '        wccampo = "dtcad"
    '    End If

    '    'seleciona uma  das opções buscar no periodo datas'

    '    If IsDate(txtdtinicial.Value) Then

    '        xsql &= "" & wccampo & " >= '" & formatadatamysql(txtdtinicial.Value) & "'"
    '    End If

    '    If IsDate(txtdtfinal.Value) Then

    '        xsql &= " and " & wccampo & " <= '" & formatadatamysql(txtdtfinal.Value) & "'"
    '    End If

    '    ' situação do cliente
    '    If sit = 3 Then
    '        xsql &= " and ativos = 0"
    '    ElseIf sit = 2 Then
    '        xsql &= " and ativos = 1"
    '    Else
    '        ordena = ""
    '    End If

    '    ' irregularidade do cliente do cliente
    '    If sit = 5 Then
    '        xsql &= " and negativado = 0 and supenso = 0"
    '    ElseIf sit = 4 Then
    '        xsql &= " and negativado = 1 and supenso = 1"
    '    ElseIf sit = 3 Then
    '        xsql &= " and negativado = 1"
    '    ElseIf sit = 2 Then
    '        xsql &= " and suspenso = 1"
    '    Else
    '    End If

    '    ' ordena por
    '    If ord = 3 Then
    '        ordena = "" & wccampo & ""
    '    ElseIf ord = 2 Then
    '        ordena = "matricula"
    '    Else
    '        ordena = "nome"
    '    End If



    '    Session("consultasql") = xsql

    '    Dim tb1 As New Data.DataTable
    '    Dim tab1 As New clsBanco

    '    tb1 = tab1.conectar("SELECT matricula, nome, plano, cpf, rg,  DATE_FORMAT(dtnasc, '%d/%m/%Y') As dtnasc, telresidencia, telcelular,  DATE_FORMAT(dtcad, '%d/%m/%Y') As dtcad, liberado  FROM tbclientes_map where " & xsql & " ORDER BY " & ordena & " ")
    '    If tb1.Rows.Count = 0 Then
    '        Exit Sub
    '    End If

    '    Session("datainicial") = FormatDateTime(txtdtinicial.Value, vbShortDate)
    '    Session("datafinal") = FormatDateTime(txtdtfinal.Value, vbShortDate)


    '    gdvProcuraDeCarnes.DataSource = tb1
    '    gdvProcuraDeCarnes.DataBind()


    '    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), link, link, True)

    'End Sub
    ''Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click

    ''    Dim nome As String = ""
    ''    Dim matricula As String = ""
    ''    Dim plano As String = ""
    ''    Dim planoOrigem As String = ""
    ''    Dim cidade As String = ""
    ''    Dim clienteEmpresa As String = ""
    ''    Dim situacao As String = ""
    ''    Dim ordenar As String = ""
    ''    Dim irregulares As String = ""
    ''    Dim ignorarSuspensos As String = ""
    ''    Dim cadastrosInvalidos As String = ""
    ''    Dim exibirCobrancaSuspensa As String = ""
    ''    Dim dadosCompletos As String = ""
    ''    Dim exibirDependentes As String = ""
    ''    Dim clientesGravacao As String = ""
    ''    Dim clientesNegativo As String = ""
    ''    Dim clienteSemGravacao As String = ""
    ''    Dim exibirData As String = ""
    ''    Dim dtInicio As String = ""
    ''    Dim dtFinal As String = ""
    ''    Dim tipo As String = ""
    ''    Dim planoO As String = ""

    ''    If txtnome.Value <> "" Then
    ''        nome = txtnome.Value
    ''    End If
    ''    If txtmatricula.Value <> "" Then
    ''        matricula = txtmatricula.Value
    ''    End If
    ''    If ddlPlano.SelectedValue <> "" Then
    ''        plano = ddlPlano.SelectedValue
    ''    End If
    ''    If ddlOrigem.SelectedValue <> "" Then
    ''        planoOrigem = ddlOrigem.SelectedValue
    ''    End If
    ''    If ddlCidade.SelectedValue <> "" Then
    ''        cidade = ddlCidade.SelectedValue
    ''    End If
    ''    If ddlempresa.SelectedValue <> "" Then
    ''        clienteEmpresa = ddlempresa.SelectedValue
    ''    End If
    ''    If rblServico.SelectedValue <> "" Then
    ''        situacao = rblServico.SelectedValue
    ''    End If
    ''    If rblOrdernar.SelectedValue <> "" Then
    ''        ordenar = rblOrdernar.SelectedValue
    ''    End If
    ''    If rdlIrregulares.SelectedValue <> "" Then
    ''        irregulares = rdlIrregulares.SelectedValue
    ''    End If
    ''    If chkClientesSuspensas.Checked Then
    ''        ignorarSuspensos = "1"
    ''    End If
    ''    If chkCobrancasSuspensas.Checked Then
    ''        exibirCobrancaSuspensa = "1"
    ''    End If
    ''    If chkDependentes.Checked Then
    ''        exibirDependentes = "1"
    ''    End If
    ''    If chkIgnorarNegativados.Checked Then
    ''        clientesNegativo = "1"
    ''    End If
    ''    If chkCadastrosInvalidos.Checked Then
    ''        cadastrosInvalidos = "1"
    ''    End If
    ''    If chkDadosCompletos.Checked Then
    ''        dadosCompletos = "1"
    ''    End If
    ''    If chkComGravacoes.Checked Then
    ''        clientesGravacao = "1"
    ''    End If
    ''    If chkSemGravacoes.Checked Then
    ''        clienteSemGravacao = "1"
    ''    End If

    ''    If chkExibirPeriodo.Checked Then
    ''        exibirData = "1"
    ''    End If

    ''    If txtdtinicial.Value <> "" Then
    ''        dtInicio = txtdtinicial.Value
    ''    End If
    ''    If txtdtfinal.Value <> "" Then
    ''        dtFinal = txtdtfinal.Value
    ''    End If
    ''    If rblVendas.SelectedValue <> "" Then
    ''        tipo = rblVendas.SelectedValue
    ''    End If
    ''    If ddlProdutos.SelectedValue <> "" Then
    ''        planoO = ddlProdutos.SelectedValue
    ''    End If



    ''    Response.Redirect("print_cliente.aspx?nome=" & nome & "&matricula=" & matricula & "&plano=" & plano & "&planoOrigem=" & planoOrigem & "&cidade=" & cidade & "&clienteEmpresa=" & clienteEmpresa & "&situacao=" & situacao & "&ordenar=" & ordenar & "&irregulares=" & irregulares & "&ignorarSuspensos=" & ignorarSuspensos & "&cadastrosInvalidos=" & cadastrosInvalidos & "&exibirCobrancaSuspensa=" & exibirCobrancaSuspensa & "&dadosCompletos=" & dadosCompletos & "&exibirDependentes=" & exibirDependentes & "&clientesGravacao=" & clientesGravacao & "&clientesNegativo=" & clientesNegativo & "&clienteSemGravacao=" & clienteSemGravacao & "&exibirData=" & exibirData & "&dtInicio=" & dtInicio & "&dtFinal=" & dtFinal & "&tipo=" & tipo & "&planoO=" & planoO)

    ''End Sub



End Class

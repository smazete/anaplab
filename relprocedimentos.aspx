﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPrint.master" CodeFile="relprocedimentos.aspx.vb" Inherits="restrito_print_cliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">--%>
    <script src="../js/JScriptmascara.js" type="text/javascript"></script>

    <meta charset="utf-8" />


    <style type="text/css" media="all">
        .texto-titulo {
            font-size: 15pt;
            font-weight: bold;
        }

        .normal {
            font-weight: normal;
        }

        .negrito {
            font-weight: bold !important;
        }

        .negrito2 {
            font-weight: bold;
            color: #000;
        }

        hr {
            border-color: #aaa;
            box-sizing: border-box;
            width: 100%;
        }

        .table-lastline>tbody>tr:last-child>td {
            font-weight: 900;
        }

    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h4 class="texto-titulo ">Relatório de Procedimentos</h4>
            </div>
        </div>
        <br />
        <hr />
        <br />
        <!-- NÃO AGRUPADO -->
        <div class="row">
            <!-- GDV DE DATAS -->
            <asp:GridView ID="gdvNaoAgrupado_Data" runat="server" CssClass="table " AutoGenerateColumns="false" GridLines="None" OnRowDataBound="data_OnRowDataBound">
                <Columns>
                    <asp:BoundField DataField="Data" HeaderText="Data" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <!-- GDV DE CLIENTES -->
                            <asp:GridView ID="gdvNaoAgrupado_Clientes" runat="server" 
                                CssClass="table " AutoGenerateColumns="false" GridLines="None" OnRowDataBound="cliente_OnRowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="data" HeaderText="Date" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide"/>
                                    <asp:BoundField DataField="nomecliente" HeaderText="Cliente" />
                                    <asp:TemplateField >
                                        <ItemTemplate>
                                            <!-- GDV DE EXAMES -->
                                            <asp:GridView ID="gdvNaoAgrupado_exames" runat="server" clientidmode="static"
                                                CssClass="table table-lastline" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>
                                                    <asp:BoundField DataField="nrseq" HeaderText="nrseq" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                    <asp:BoundField DataField="nomeempresa" HeaderText="Convênio" />
                                                    <asp:BoundField DataField="cod" HeaderText="COD"  ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide"  />
                                                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                                    <asp:BoundField DataField="nomecliente" HeaderText="Cliente"  ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                    <asp:BoundField DataField="valorcusto" HeaderText="Valor Custo" />
                                                    <asp:BoundField DataField="valorcliente" HeaderText="Valor Cliente" />
                                                    <asp:BoundField DataField="valorplano" HeaderText="Valor Plano" />
                                                    <asp:BoundField DataField="valortotal" HeaderText="Valor Total" />
                                                    <asp:BoundField DataField="receita" HeaderText="Receita" />
                                                    <asp:BoundField DataField="baixado" HeaderText="Baixado" />
                                                    <asp:BoundField DataField="solicitadopor" HeaderText="Solicitado Por" />
                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
        <!-- AGRUPADO POR CLIENTE -->
          <div class="row">
            <!-- GDV DE DATAS -->
            <asp:GridView ID="gdvAgrupadoPorCliente" runat="server" CssClass="table " AutoGenerateColumns="false" GridLines="None" OnRowDataBound="clienteAgp_OnRowDataBound">
                <Columns>
                    <asp:BoundField DataField="nomecliente" HeaderText="Cliente" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <!-- GDV DE CLIENTES -->
                            <asp:GridView ID="gdAgrupado_Data" runat="server" 
                                CssClass="table " AutoGenerateColumns="false" GridLines="None" OnRowDataBound="dataAgp_OnRowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="nomecliente" HeaderText="Cliente" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide"/>
                                    <asp:BoundField DataField="data" HeaderText="Data"/>
                                    <asp:TemplateField >
                                        <ItemTemplate>
                                            <!-- GDV DE EXAMES -->
                                            <asp:GridView ID="gdvNaoAgrupado_exames" runat="server" clientidmode="static"
                                                CssClass="table table-lastline" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>
                                                    <asp:BoundField DataField="nrseq" HeaderText="nrseq" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                    <asp:BoundField DataField="nomeempresa" HeaderText="Convênio" />
                                                    <asp:BoundField DataField="cod" HeaderText="COD"  ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide"  />
                                                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                                    <asp:BoundField DataField="nomecliente" HeaderText="Cliente"  ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                    <asp:BoundField DataField="valorcusto" HeaderText="Valor Custo" />
                                                    <asp:BoundField DataField="valorcliente" HeaderText="Valor Cliente" />
                                                    <asp:BoundField DataField="valorplano" HeaderText="Valor Plano" />
                                                    <asp:BoundField DataField="valortotal" HeaderText="Valor Total" />
                                                    <asp:BoundField DataField="receita" HeaderText="Receita" />
                                                    <asp:BoundField DataField="baixado" HeaderText="Baixado" />
                                                    <asp:BoundField DataField="solicitadopor" HeaderText="Solicitado Por" />
                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
        <br />
        <hr />
        <br />
        <div class="row">
            <div class="col-lg-12">
                <center>
                    <div class="col-xs-2">
                        <h5>Total Valor do Custo:<br />
                            <asp:Label runat="server" ID="lblValorCusto"></asp:Label></h5>
                    </div>
                      <div class="col-xs-2">
                        <h5>Total Valor do Plano:<br />
                            <asp:Label runat="server" ID="lblValorPlano"></asp:Label></h5>
                    </div>
                    <div class="col-xs-2">
                        <h5>Total Valor do Cliente:<br />
                            <asp:Label runat="server" ID="lblValorCliente"></asp:Label></h5>
                    </div>
                   <div class="col-xs-2">
                        <h5>Total Valor do Total:<br />
                            <asp:Label runat="server" ID="lblValorTotal"></asp:Label></h5>
                    </div>
                    <div class="col-xs-4">
                        <h5>Total Valor da Receita:<br />
                           <strong><asp:Label runat="server" ID="lblValorReceita"></asp:Label></strong></h5>
                    </div>
                </center>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-12">
                <center>
                    <div class="col-xs-4">
                        <h5>Total de Impresso:
                            <asp:Label runat="server" ID="totalimpresso"></asp:Label></h5>
                    </div>
                    <div class="col-xs-4">
                        <h5>Data da Impressão:
                            <asp:Label runat="server" ID="dtimpressao"></asp:Label></h5>
                    </div>
                    <div class="col-xs-4">
                        <h5>Usuário:
                            <asp:Label runat="server" ID="lblusuario"></asp:Label></h5>
                    </div>
                </center>
            </div>

        </div>
        <div class="row">
            <div class="alert alert-primary col-xs-12" role="alert">
                <p>
                    <center>
                       <strong style="color: #333;">Filtros Utilizados</strong> 
                       <label id="lblFiltrosUtilizados" runat="server" style="color: #808080;"></label>
                    </center>
                </p>
            </div>
        </div>
    </div>



    <script>
</script>
</asp:Content>

﻿Imports System.Data
Imports System.IO
Imports Barcode

Partial Class restrito_imprimir_exames
    Inherits System.Web.UI.Page
    Dim tabexames As New clsBanco
    Dim tbexames As New Data.DataTable
    Dim tabexames1 As New clsBanco
    Dim tbexames1 As New Data.DataTable
    Dim tabexames2 As New clsBanco
    Dim tbexames2 As New Data.DataTable
    Dim sql As String

    Private Sub restrito_imprimir_exames_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim nrseq As String = Request.QueryString("i")

        If nrseq Is Nothing Then
            Response.Redirect("/restrito/emitir_procedimentos.aspx")
        End If

        obterDados(nrseq)


    End Sub

    Private Sub gdvListaExames_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvListaExames.RowDataBound

    End Sub

    Private Function obterDados(nrseq As String)

        Dim sql As String = ""

        If Request.QueryString("d") = True Then
            sql = "SELECT tbguias.nrseq AS nrseq, solicitadopor, dep.nome AS nome, dep.sexo AS sexo, cli.plano AS plano, DATE_FORMAT(dep.dtnasc, '%d/%m/%Y') AS dtnasc, dep.rg AS rg, dep.cpf AS cpf, emissor, DATE_FORMAT(tbguias.dtemitido, '%d/%m/%Y') AS dtemitido, serial, imgcodbarras FROM tbguias "
            sql &= "LEFT JOIN tbclientes AS cli ON tbguias.nrseqcliente = cli.nrseq "
            sql &= "LEFT JOIN tbdependentes AS dep ON tbguias.nrseqdependente = dep.nrseq "
            sql &= " WHERE tbguias.nrseq = '" & nrseq & "'"

        Else
            sql = "SELECT tbguias.nrseq AS nrseq, solicitadopor, nome, plano, sexo, DATE_FORMAT(dtnasc, '%d/%m/%Y') AS dtnasc, rg, cpf, emissor, DATE_FORMAT(tbguias.dtemitido, '%d/%m/%Y') AS dtemitido, serial, imgcodbarras FROM tbguias LEFT JOIN tbclientes ON tbguias.nrseqcliente = tbclientes.nrseq WHERE tbguias.nrseq = '" & nrseq & "'"
        End If


        tbexames = tabexames.conectar(sql)

        lblserial.Text = tbexames.Rows(0)("serial").ToString
        lbldata.Text = FormatDateTime(DateTime.Now)

        lblpaciente.Text = tbexames.Rows(0)("nome").ToString
        lblplano.Text = tbexames.Rows(0)("plano").ToString
        lblsexo.Text = tbexames.Rows(0)("sexo").ToString
        lbldtnascimento.Text = tbexames.Rows(0)("dtnasc").ToString
        lblcpf.Text = tbexames.Rows(0)("cpf").ToString
        lblrg.Text = tbexames.Rows(0)("rg").ToString
        lblemissor.Text = tbexames.Rows(0)("emissor").ToString
        lbldtexpedicao.Text = Left(tbexames.Rows(0)("dtemitido").ToString, 10)
        lblserialctrl.Text = tbexames.Rows(0)("serial").ToString

        Dim curFileSMap = Server.MapPath("~/imgcodbarras/") & tbexames.Rows(0)("imgcodbarras").ToString

        If File.Exists(curFileSMap) Then

            codbarras.ImageUrl = "/imgcodbarras/" & tbexames.Rows(0)("imgcodbarras").ToString

        End If






        Dim tabela As New DataTable()


        gdvListaExames.DataSource = Nothing

        Dim sql2 As String = "SELECT * FROM tbguias_exames LEFT JOIN tbconvenios ON tbguias_exames.nrseqconvenio = tbconvenios.nrseq WHERE nrseqagenda = '" & tbexames.Rows(0)("nrseq").ToString & "' AND tbguias_exames.ativo = true AND nrseqconvenio ='" & Request.QueryString("c") & "'"

        tbexames1 = tabexames1.conectar(sql2)

        tabela.Columns.Add("codamb")
        tabela.Columns.Add("procedimento")

        lblconveniada.Text = tbexames1.Rows(0)("nomefantasia").ToString
        lblendereco.Text = tbexames1.Rows(0)("endereco").ToString
        lblbairro.Text = tbexames1.Rows(0)("bairro").ToString
        lblcidade.Text = tbexames1.Rows(0)("cidade").ToString
        lbltelefone.Text = tbexames1.Rows(0)("telefonefixo").ToString
        lblemail.Text = tbexames1.Rows(0)("email").ToString
        lblsolicitado.Text = tbexames.Rows(0)("solicitadopor").ToString

        For x As Integer = 0 To tbexames1.Rows.Count - 1

            tbexames2 = tabexames2.conectar("select * from tbprocedimentos where nrseq = '" & tbexames1.Rows(x)("nrseqproced").ToString & "'")

            Dim proced As String = tbexames2.Rows(0)("descricao").ToString
            Dim codigoamb As String = tbexames2.Rows(0)("codigoamb").ToString

            tabela.Rows.Add(codigoamb, proced)
        Next

        gdvListaExames.DataSource = tabela
        gdvListaExames.DataBind()


        lblqtdexames.Text = gdvListaExames.Rows.Count
        Return True
    End Function

End Class

﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="relatoriotreinamentos.aspx.vb" Inherits="relatoriotreinamentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
     <script runat="server">

         Sub exportarh(ByVal Source As Object, ByVal E As EventArgs)

             If dgexcel.Items.Count = 0 Then
                 Exit Sub
             End If

             Dim wcarquivo As String = "Treina" & Date.Now.ToString.Replace("/", "").Replace(":", "").Replace(" ", "").Trim()

             exportarExcel(dgexcel, wcarquivo)
         End Sub

         Sub exportarExcel(grid As DataGrid, saveAsFile As String)

             ' O linite de linhas do Excel é  65536
             If grid.Items.Count + 1 < 65536 Then
                 HttpContext.Current.Response.Clear()
                 HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                 HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & saveAsFile & ".xls")
                 ' Remover caracteres do header - Content-Type
                 HttpContext.Current.Response.Charset = ""
                 'HttpContext.Current.Response.WriteFile("style.txt")
                 ' desabilita o  view state.
                 grid.EnableViewState = False
                 Dim tw As New System.IO.StringWriter()
                 Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                 grid.RenderControl(hw)
                 ' Escrever o html no navegador
                 HttpContext.Current.Response.Write(tw.ToString())
                 ' termina o response
                 HttpContext.Current.Response.End()

             Else
                 HttpContext.Current.Response.Write("Muitas linhas para exportar para o Excel !!!")
             End If

         End Sub
    </script>
      <script type="text/javascript">

        function irlink(linkativo) {



            window.open(linkativo, '_blank');

        }

        function irlinkmesmpagina(linkativo) {

            window.location.href = linkativo;

            //window.open(linkativo);

        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    
    <asp:UpdatePanel ID="updategeral" runat="server">
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="grade" EventName="RowCommand" />--%>
            <asp:PostBackTrigger ControlID="grade" />
            <asp:PostBackTrigger ControlID="btnexcel" />
        </Triggers>
        <ContentTemplate>



            <div class="box box-primary" style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);" id="divprincipal" runat="server">

                <div class="box box-header ">
                    <b>Relatório de Treinamentos</b>
                </div>
                <div class="box-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>

                            <div class="row">
                                <div class="col-lg-3" runat ="server" id="divgestor">
                                    <div class="form-group">

                                        <asp:Label ID="Label1" runat="server" Text="Gestor"></asp:Label>
                                        <br />
                                        <asp:DropDownList ID="cbogestor" runat="server" class="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <asp:Label ID="Label2" runat="server" Text="Colaborador"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtcolaborador" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <asp:Label ID="Label4" runat="server" Text="Ano"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="txtano" runat="server" class="form-control" Style="text-align: center;">
                                        </asp:TextBox>
                                    </div>
                                </div>


                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button ID="btnProcurar" runat="server" Text="     Procurar     " CssClass="btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">

                                        <asp:Label ID="Label3" runat="server" Text="Treinamento/Curso"></asp:Label>
                                        <br />
                                        <asp:DropDownList ID="cbocursos" runat="server" class="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">

                                        <asp:Label ID="Label5" runat="server" Text="Status"></asp:Label>
                                        <br />
                                        <asp:DropDownList ID="cbostatus" runat="server" class="form-control">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Realizados</asp:ListItem>
                                            <asp:ListItem>Em aberto</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>
                                 <div class="col-lg-4">
                                     <div class="box box-warning ">
                                         <div class="box box-header ">
                                             Ordenar por
                                         </div>
                                         <div class="box-body ">
                                             <div class="row">
                                                 <div class="col-lg-12 ">
                                                     <asp:RadioButton ID="optordernome" Text="Nome colaborador" runat="server" GroupName ="ordem" Checked="true"/>
                                                     <br />
                                                     <asp:RadioButton ID="optordertreina" Text="Nome treinamento" runat="server" GroupName ="ordem"/>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                     </div> 
                            </div>

                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                                        <ProgressTemplate>
                                            Carregando...
                                                <br />
                                            <img src="../img/loaddouble01.gif" style="width: 9rem; height: 9rem;" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">

                                    <br />
                                    <div style="overflow: auto" runat="server" id="divgrade">


                                        <asp:GridView ID="grade" runat="server" AutoGenerateColumns="False" CellPadding="4" Width="100%" PageSize="200" ShowHeaderWhenEmpty="True" CssClass="table" ForeColor="Black" GridLines="Horizontal" AllowPaging="True">
                                            <Columns>
                                                <asp:TemplateField HeaderText="It" ItemStyle-HorizontalAlign="Center">

                                                    <ItemTemplate>

                                                        <span class="badge" style="background: #1E90FF">
                                                            <asp:Label ID="lblitem" runat="server"></asp:Label>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:BoundField DataField="anobase" HeaderText="Ano Base" />
                                                <asp:BoundField DataField="descricao" HeaderText="Treinamento" />
                                                <asp:TemplateField HeaderText="Colaborador">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lblcolaborador" runat="server" Font-Size="Large" Text='<%# Bind("matriculacolaborador") %>'></asp:Label>
                                                        <br />
                                                        <asp:Label ID="lblnomecola" runat="server" Font-Size="Large" Text='<%# Bind("nomecolaborador") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Gestor ">
                                                    <ItemTemplate>



                                                        <asp:Label ID="lblgestor" runat="server" Font-Size="Large" Text='<%# Bind("matriculagestor") %>'></asp:Label>

                                                        <asp:Label ID="lblnomegestor" runat="server" Font-Size="Large" Text='<%# Bind("nomegestor") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                              

                                                 <asp:TemplateField HeaderText="Concluído ">
                                                    <ItemTemplate>



                                                        <asp:Label ID="lblconcluidograde" runat="server" Font-Size="Large" Text='<%# Bind("concluido") %>'></asp:Label>

                                                       <asp:Image ID="imgconcluidograde" runat="server" ImageUrl="~/img/ligado.png" style="width:3rem;height:3rem;" />

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            
                                                <%--<asp:BoundField HeaderText="Concluído" DataField="concluido" />--%>


                                            </Columns>


                                        </asp:GridView>

                                    </div>


                                </div>
                            </div>
                            <div class="row" style="display: none">
                                <asp:DataGrid ID="dgexcel" runat="server" BackColor="WhiteSmoke" BorderColor="Black"
                                    CellPadding="0" CellSpacing="1" Font-Name="Arial" Font-Names="Arial"
                                    Font-Size="8pt" HeaderStyle-BackColor="#0000FF"
                                    Headerstyle-Font-Name="Arial" HeaderStyle-Font-Size="10"
                                    HeaderStyle-ForeColor="#FFFFFF" Height="148px" Style="margin-right: 0px"
                                    AutoGenerateColumns="False" ShowFooter="True">
                                    <AlternatingItemStyle BackColor="White" />
                                    <Columns>

                                        <asp:BoundColumn DataField="matriculacolaborador" HeaderText="Matricula Colaborador" />
                                        <asp:BoundColumn DataField="nomecolaborador" HeaderText="Nome Colaborador" />

                                        <asp:BoundColumn DataField="matriculagestor" HeaderText="Matricula Gestor" />
                                        <asp:BoundColumn DataField="nomegestor" HeaderText="Nome Gestor" />

                                        <asp:BoundColumn DataField="anobase" HeaderText="Ano Base" />
                                        <asp:BoundColumn DataField="descricao" HeaderText="Treinamento" />
                                        <asp:BoundColumn HeaderText="Concluído" DataField="concluido" />
                                          <asp:TemplateColumn  HeaderText="Concluído ">
                                                    <ItemTemplate>



                                                        <asp:Label ID="lblconcluidograde" runat="server" Font-Size="Large" Text='<%# Bind("concluido") %>'></asp:Label>

                                                       <asp:Image ID="imgconcluidograde" runat="server" ImageUrl="~/img/ligado.png" style="width:3rem;height:3rem;" />

                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 text-center">

                                    <br />
                                    <asp:LinkButton ID="btnImprimir" runat="server" CssClass="btn btn-primary"> <asp:Image runat="server" style="width:2rem;height:2rem;" ImageUrl="~/img/print01.png" /> Imprimir</asp:LinkButton>

                                </div>
                                <div class="col-lg-6 text-center">

                                    <br />
                                    <asp:LinkButton ID="btnexcel" runat="server" CssClass="btn btn-success" OnClick="exportarh"> <asp:Image runat="server" style="width:2rem;height:2rem;"  ImageUrl="~/img/excel.png" /> Excel</asp:LinkButton>

                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>



</asp:Content>


﻿<%@ Page Language="VB" MasterPageFile="~/MasterPrint.master" AutoEventWireup="false" CodeFile="imprimir_exames.aspx.vb" Inherits="restrito_imprimir_exames" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">--%>
    <script src="../js/JScriptmascara.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>


    <style type="text/css" media="all">
        .texto-titulo {
            font-size: 11pt;
            font-weight: bold;
        }

        .negrito {
            font-weight: bold;
        }

        .negrito2 {
            font-weight: bold;
            color: #000;
        }

        .pull-right {
            padding-left: 100px;
        }

        .font_menor {
            font-size: 8pt;
        }

        .font_menor2 {
            font-size: 11pt;
        }

        .font_menor3 {
            font-size: 8pt;
            font-weight: normal;
        }

        footer {
            position: fixed;
            height: 100px;
            bottom: 0;
            width: 100%;
        }

        .container {
            width: 96% !important;
        }

        .tabeladados {
            margin-bottom: -24px;
            margin-top: -24px;
        }

        .tabeladados1 {
            padding: 0px;
        }

        .rowmenor {
            margin-top: -25px;
        }

        .pagebreak {
            page-break-after: always;
        }
        /*
        @media all {
            @page {
                size: A4 portrait;
            }

            div.content-print {
                margin-top: 3cm;
                border: 1px solid #000;
                max-height: 16cm;
            }

            div.divFixed {
                clear: both;
                display: block !important;
                position: fixed;
                margin: 1px;
                left: 0;
                right: 0;
            }

            div.divHeader {
                top: 0;
            }

            div.divFooter {
                bottom: 0;
            }
        }
            */
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div class="container" id="renderPDF">
        <div class="row divFixed divHeader" id="divHeader">

            <div class="panel panel-primary ">
                <div class="panel-body tabeladados1">
                    <div class="row">
                        <div class="col-xs-3">
                        </div>
                        <div class="col-xs-6 text-center">
                            <h4 class="texto-titulo">SPA - Solicitação de Pronto Atendimento</h4>
                            <h5>Guia de Encaminhamento</h5>
                        </div>
                        <div class="col-xs-3 ">
                            <h6>SERIAL GUIA:</h6>
                            <asp:Label runat="server" ID="lblserial" CssClass="negrito">00000000123123</asp:Label>
                            <h6>
                                <asp:Label runat="server" ID="lbldata" CssClass="negrito">00/00/0000</asp:Label></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-print">
            <div class="row">
                <div class="col-xs-4">
                    <h6>Conveniada:
                    <asp:Label runat="server" ID="lblconveniada" CssClass="negrito">LAB ANEL LABORMED</asp:Label></h6>
                    <h6>Paciente:
                    <asp:Label runat="server" ID="lblpaciente" CssClass="negrito">João da Silva</asp:Label></h6>
                    <h6>Plano:
                    <asp:Label runat="server" ID="lblplano" CssClass="negrito">TesPla</asp:Label></h6>
                </div>
                <div class="col-xs-4"></div>
                <div class="col-xs-4">
                    <h6>Sexo:
                    <asp:Label runat="server" ID="lblsexo" CssClass="negrito">Masculino</asp:Label></h6>
                    </h6>
                <h6>Data de Nascimento:
                    <asp:Label runat="server" ID="lbldtnascimento" CssClass="negrito">00/00/0000</asp:Label></h6>
                    </h6>
                </div>
            </div>

            <div class="row">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="container row">
                            <table class="table tabeladados">
                                <thead>
                                    <tr>
                                        <th>
                                            <label class="negrito2">CPF</label></th>
                                        <th>
                                            <label class="negrito2">RG</label></th>
                                        <th>
                                            <label class="negrito2">UF Emissor</label></th>
                                        <th>
                                            <label class="negrito2">Data Expedição</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Label runat="server" ID="lblcpf">000.000.000-00</asp:Label></td>
                                        <td>
                                            <asp:Label runat="server" ID="lblrg">00.000.000-02</asp:Label></td>
                                        <td>
                                            <asp:Label runat="server" ID="lblemissor">SSP</asp:Label></td>
                                        <td>
                                            <asp:Label runat="server" ID="lbldtexpedicao">00/00/0000</asp:Label></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: -25px;">
                <h6 class="text-center negrito font_menor">Autorizamos o portador desta guia de encaminhamento a realizar o(s) exame(s) abaixo relacionado(s).</h6>
            </div>

            <br />
            <br />

            <div class="row">
                <asp:GridView ID="gdvListaExames" runat="server" CssClass="table " AutoGenerateColumns="false" GridLines="None">
                    <Columns>


                        <asp:BoundField DataField="codamb" HeaderText="Cod AMB" />
                        <asp:BoundField DataField="procedimento" HeaderText="Procedimento" />
                    </Columns>
                </asp:GridView>
            </div>
            <div class="row hide">
                <div class="col-xs-12">
                    <div class="row">
                        <p>
                    1<br />
                    2<br />
                    3<br />
                    4<br />
                    5<br />
                    6<br />
                    7<br />
                    8<br />
                    9<br />
                    10<br />
                    11<br />
                    12<br />
                    13<br />
                    14<br />
                    15<br />
                    16<br />
                </p>
                <p>
                    18<br />
                    19<br />
                    20<br />
                    21<br />
                    22<br />
                    23<br />
                    24<br />
                    25<br />
                    26<br />
                    27<br />
                    28<br />
                    29<br />
                    30<br />
                    31<br />
                    32<br />
                    33<br />
                </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer divFixed divFooter" id="divFooter">

            <div class="row rowmenor">
                <h6 class="text-center font_menor">Atenção: Guia válida por 30 dias a partir da emissão</h6>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-body tabeladados1">
                            <div class="row container">
                                <h5 class="negrito">Endereço para realização do(s) exames(s):</h5>
                                <h6 class="font_menor3">Endereço:
                                    <asp:Label runat="server" ID="lblendereco" CssClass="negrito">Rua Alfredo</asp:Label></h6>
                                <h6 class="font_menor3">Bairro:
                                    <asp:Label runat="server" ID="lblbairro" CssClass="negrito">Afonso Pena</asp:Label></h6>
                                <h6 class="font_menor3">Cidade:
                                    <asp:Label runat="server" ID="lblcidade" CssClass="negrito">São José dos Pinhais</asp:Label></h6>
                                <h6 class="font_menor3">Telefone:
                                    <asp:Label runat="server" ID="lbltelefone" CssClass="negrito">41 99999-9999</asp:Label></h6>
                                <h6 class="font_menor3">E-mail:
                                    <asp:Label runat="server" ID="lblemail" CssClass="negrito">teste@mail.com</asp:Label></h6>
                                <h6 class="font_menor3">Solicitado por:
                                    <asp:Label runat="server" ID="lblsolicitado" CssClass="negrito">Fulano</asp:Label></h6>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-body tabeladados1">
                            <div class="row">
                                <h5 class="negrito text-center">Data Agendada:
                                    <asp:Label runat="server" ID="lbldtagendada"></asp:Label></h5>
                                <h5 class="negrito text-center">Horário:
                                    <asp:Label runat="server" ID="lblhorario"></asp:Label></h5>
                                <h5 class="negrito text-center">Horário de Atendimento</h5>
                                <h6 class="negrito text-center">Segunda à Sexta:
                                    <asp:Label runat="server" ID="lbldiasuteis"></asp:Label></h6>
                                <h6 class="negrito text-center">Sábado:
                                    <asp:Label runat="server" ID="lblsabado"></asp:Label></h6>
                                <h5 class="negrito text-center">Ctrl:
                                    <asp:Label runat="server" ID="lblserialctrl"></asp:Label></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-body tabeladados1">
                            <asp:Image runat="server" ID="codbarras" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="panel panel-primary">
                        <div class="panel-body tabeladados1">
                            <h6 class="text-center">Qtd Exames:
                                <asp:Label runat="server" ID="lblqtdexames">000</asp:Label></h6>
                            <h6 class="text-center font_menor">À Faturar ? (    ) Sim     (    )Não</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h6 class="text-center font_menor2">Chegar ao local do(s) exame(s) com 15 minutos de antecedência !</h6>
            </div>
        </div>

    </div>


    <script>

        function imprimir() {
            var doc = new jsPDF('portrait', 'pt', 'a4');
            doc.addHTML($('html'), function () {
                doc.save("teste.pdf");
            });
        }

        //$(document).ready(function() {

        //    var doc = new jsPDF('portrait', 'pt', 'a5');
        //    doc.addHTML($('html'), function () {
        //        doc.save("guia.pdf");
        //    });  

        //})

    </script>
</asp:Content>

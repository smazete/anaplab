﻿Imports clsSmart
Partial Class restrito_tiposdocumentos
    Inherits System.Web.UI.Page

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub restrito_tiposdocumentos_Load(sender As Object, e As EventArgs) Handles Me.Load
        sm("carregaMaskMoney()", "carregaMaskMoney")
        If IsPostBack Then Exit Sub
        'verificarusuario2()
        txtcodigo.ReadOnly = True
        liberar(False)
        carregargrade()
    End Sub

    Private Sub verificarusuario2()
        Dim resultado As String = valida(Session("usuario"), mARQUIVO(Request.Path, True), Session("usuariomaster")).ToLower
        Select Case resultado
            Case Is = "login"
                Session("urlretorno") = Request.Path
                Response.Redirect("login.aspx")
                Exit Sub
            Case Is = "erro"
                Response.Redirect("ops2.aspx")
                Exit Sub
        End Select
    End Sub

    Private Sub carregargrade()

        Dim xdocumentos As New clsDocumentos

        If Not xdocumentos.consultarativo() Then
            sm("swal({title: 'Error!',text: '" & xdocumentos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        GradeArqs.DataSource = xdocumentos.Table
        GradeArqs.DataBind()

    End Sub

    Private Sub btnNovo_Click(sender As Object, e As EventArgs) Handles btnNovo.Click
        Dim xdocumentos As New clsDocumentos

        If Not xdocumentos.Novo() Then
            sm("swal({title: 'Error!',text: '" & xdocumentos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        txtcodigo.Text = xdocumentos.Nrseq

        btnNovo.Enabled = False
        liberar(True)
        btnSalvar.Enabled = True

    End Sub
    Private Sub limpar()
        txtcodigo.Text = ""
        txtdescricao.Text = ""
        txttarifa.Text = ""
        txttempo.Text = ""
        chkExigir.Checked = False
    End Sub

    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click


        Dim xdocumentos As New clsDocumentos
        Dim taxa As String
        xdocumentos.Descricao = txtdescricao.Text
        xdocumentos.Compensa = moeda(txttempo.Text)
        taxa = moeda(txttarifa.Text)
        taxa = taxa.Replace("%", "")
        xdocumentos.Taxa = taxa
        xdocumentos.Exigirdth = chkExigir.Checked
        xdocumentos.Contacorrente = chkcontacorrente.Checked
        xdocumentos.Nrseq = txtcodigo.Text

        If Not xdocumentos.salvar() Then
            sm("swal({title: 'Error!',text: '" & xdocumentos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If



        liberar(False)
        limpar()
        carregargrade()

    End Sub

    Private Function converterParaDouble(txt As String) As String
        If txt.Contains(" ") Then
            txt = txt.Replace(" ", "")
        End If
        If txt.Contains(".") Then
            txt = txt.Replace(".", "")
        End If
        If txt.Contains(",") Then
            txt = txt.Replace(",", ".")
        End If
        If txt.Contains("R$") Then
            txt = txt.Replace("R$", "")
        End If
        If txt.Contains("%") Then
            txt = txt.Replace("%", "")
        End If

        Return txt
    End Function

    Private Sub GradeArqs_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GradeArqs.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        If e.Row.Cells(5).Text = True Then
            e.Row.Cells(5).Text = "Sim"
        Else
            e.Row.Cells(5).Text = "Não"
        End If
        If e.Row.Cells(4).Text = True Then
            e.Row.Cells(4).Text = "Sim"
        Else
            e.Row.Cells(4).Text = "Não"
        End If
    End Sub

    Private Sub GradeArqs_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GradeArqs.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim txtlinha As Integer
        ' Retrieve the row that contains the button clicked 
        ' by the user from the Rows collection.
        Dim row As GridViewRow = GradeArqs.Rows(index)
        txtlinha = index
        ' Create a new ListItem object for the contact in the row.     
        'Dim item As New ListItem()
        'item.Text = Server.HtmlDecode(row.Cells(2).Text) & " " & _
        '  Server.HtmlDecode(row.Cells(3).Text)

        Dim xyz As Integer = 0
        Dim linha As GridViewRow = GradeArqs.Rows(txtlinha)

        Select Case e.CommandName.ToLower
            Case Is = "excluir"
                Dim xdocumentos As New clsDocumentos
                xdocumentos.Nrseq = linha.Cells(0).Text

                If Not xdocumentos.excluir() Then
                    sm("swal({title: 'Error!',text: '" & xdocumentos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                    Exit Sub
                End If

            Case Is = "alterar"
                Dim xdocumentos As New clsDocumentos
                xdocumentos.Nrseq = linha.Cells(0).Text
                If Not xdocumentos.consultar() Then
                    sm("swal({title: 'Error!',text: '" & xdocumentos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                    Exit Sub
                End If
                If xdocumentos.Contador <> 0 Then
                    txtcodigo.Text = xdocumentos.Nrseq
                    txtdescricao.Text = xdocumentos.Descricao
                    txttarifa.Text = xdocumentos.Taxa
                    txttempo.Text = xdocumentos.Compensa
                    chkcontacorrente.Checked = xdocumentos.Contacorrente
                    chkExigir.Checked = xdocumentos.Exigirdth
                    liberar(True)
                End If
        End Select
        carregargrade()
    End Sub

    Private Function liberar(valor As Boolean) As Integer
        txtdescricao.Enabled = valor
        txttarifa.Enabled = valor
        txttempo.Enabled = valor
        chkExigir.Enabled = valor
        btnSalvar.Enabled = valor
        btnNovo.Enabled = Not valor
    End Function

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click
        limpar()
        liberar(False)
    End Sub
End Class

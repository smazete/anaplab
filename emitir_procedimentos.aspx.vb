﻿Imports SelectPdf
Imports System.Data
Imports System.Drawing
Imports System.IO
Imports clsSmart
Imports clsCaixa_new
Imports clssessoes

Partial Class restrito_emitir_procedimentos
    Inherits System.Web.UI.Page
    Dim tabprocedimentos As New clsBanco
    Dim tbprocedimentos As New Data.DataTable
    Dim wctotalpaciente As Decimal = 0
    Dim tabps As New clsBanco
    Dim tbps As New Data.DataTable
    Dim tabelaEmpresa As String = " tbempresas"
    Dim sql As String
    Dim plano As String
    Dim procedimentos As String

    Private Sub restrito_emitir_procedimentos_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub

        adicionaPlano()
        adicionaBusca()
        adicionaProcedimentos()
        adicionaConvenio()
        adicionaFormaPagamento()
        adcionaGrupoProced()
        bloqueio()

        tbprocedimentos = tabprocedimentos.conectar("Select * from vwagenda_exames where nrseq = 0")
        gdvListaExames.DataSource = tbprocedimentos
        gdvListaExames.DataBind()

    End Sub

    Private Function adicionaBusca()
        ddpListarPor.Items.Clear()

        ddpListarPor.Items.Add(New ListItem("Procurar por Nome Cliente", 1))
        ddpListarPor.Items.Add(New ListItem("Procurar por Matricula Cliente", 2))
        ddpListarPor.Items.Add(New ListItem("Procurar por CPF Cliente", 3))
        ddpListarPor.Items.Add(New ListItem("Procurar pelo Nome do Dependente", 4))

    End Function

    Private Function adicionaPlano()
        ddlPlano.Items.Clear()

        tbprocedimentos = tabprocedimentos.conectar("select * from tbplanos where ativo = true ORDER BY descricao")

        ddlPlano.Items.Insert(0, New ListItem("Selecione", ""))

        If tbprocedimentos.Rows.Count > 0 Then
            For x As Integer = 0 To tbprocedimentos.Rows.Count - 1
                ddlPlano.Items.Add(New ListItem(tbprocedimentos.Rows(x)("descricao").ToString, tbprocedimentos.Rows(x)("nrseq").ToString))
            Next
        End If
    End Function

    Private Function adcionaGrupoProced()
        ddlGrupos.Items.Clear()

        sql = "SELECT * FROM tbprocedimentos_grupos WHERE ativo = true   ORDER BY descricao"
        Dim tb As DataTable = findSql(sql)

        ddlGrupos.Items.Insert(0, New ListItem("Selecione", ""))

        If tb.Rows.Count > 0 Then
            For x As Integer = 0 To tb.Rows.Count - 1
                ddlGrupos.Items.Add(New ListItem(tb.Rows(x)("descricao").ToString, tb.Rows(x)("nrseq").ToString))
            Next
        End If
    End Function

    Private Function adicionaFormaPagamento()
        ddlPagamento.Items.Clear()

        tbprocedimentos = tabprocedimentos.conectar("select * from tbdocumentos where ativo = true  ORDER BY descricao")

        ddlPagamento.Items.Insert(0, New ListItem("Selecione", ""))

        If tbprocedimentos.Rows.Count > 0 Then
            For x As Integer = 0 To tbprocedimentos.Rows.Count - 1
                ddlPagamento.Items.Add(New ListItem(tbprocedimentos.Rows(x)("descricao").ToString, tbprocedimentos.Rows(x)("nrseq").ToString))
            Next
        End If
    End Function

    Private Function adicionaConvenio()
        ddlConvenio.Items.Clear()

        tbprocedimentos = tabprocedimentos.conectar("select * from tbconvenios where ativo = True   ORDER BY nome")

        ddlConvenio.Items.Insert(0, New ListItem("Selecione", ""))

        If tbprocedimentos.Rows.Count > 0 Then
            For x As Integer = 0 To tbprocedimentos.Rows.Count - 1
                ddlConvenio.Items.Add(New ListItem(tbprocedimentos.Rows(x)("nome").ToString, tbprocedimentos.Rows(x)("nrseq").ToString))
            Next
        End If
    End Function

    Private Function adicionaProcedimentos()
        ddlProcedimentos.Items.Clear()

        If ddlPlano.SelectedValue <> "" Then
            Dim sql As String = "select * from tbplanos where nrseq = '" & plano & "' AND ativo = true   ORDER BY descricao"
            tbprocedimentos = tabprocedimentos.conectar(sql)
            sql = "select * from tbprocedimentos where plano = '" & tbprocedimentos.Rows(0)("descricao").ToString & "' AND ativo = true  ORDER BY descricao" 'AND nrseqempresa = '" & Session("idempresaemuso") & "'
            tbprocedimentos = tabprocedimentos.conectar(sql)
        Else
            tbprocedimentos = tabprocedimentos.conectar("select * from tbprocedimentos where ativo = true")
        End If

        ddlProcedimentos.Items.Insert(0, New ListItem("Selecione", ""))

        If tbprocedimentos.Rows.Count > 0 Then
            For x As Integer = 0 To tbprocedimentos.Rows.Count - 1
                ddlProcedimentos.Items.Add(New ListItem(tbprocedimentos.Rows(x)("descricao").ToString, tbprocedimentos.Rows(x)("nrseq").ToString))
            Next
        End If
    End Function

    'Private Sub ddlPlano_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPlano.SelectedIndexChanged
    '    plano = ddlPlano.SelectedValue
    '    adicionaProcedimentos()
    ' FrontEnd Frameworks BackEnd Mobile
    'End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        If ddlGrupos.SelectedValue = "" Then
            versessao()
            Dim lab As String = ddlConvenio.SelectedValue
            Dim procedimento As String = ddlProcedimentos.SelectedValue

            hdvlparcial.Value = ""

            If lab = "" Or procedimento = "" Or txtvalorprocedimento.Value = "" Or txtvalorplano.Value = "" Or txtvalorpaciente.Value = "" Then
                Dim erro As String = ""
                ' Quantas você quiser dessa coisa... pra cada campo
                erro = verificaCampo(lab, "Laboratório", erro)
                erro = verificaCampo(procedimento, "Procedimento", erro)
                erro = verificaCampo(txtvalorprocedimento.Value, "Valor Procedimento", erro)
                erro = verificaCampo(txtvalorplano.Value, "Valor Plano", erro)
                erro = verificaCampo(txtvalorpaciente.Value, "Valor Paciente", erro)

                If erro <> "" Then
                    rowError.Style.Remove("display")
                    lblErro.InnerHtml = erro
                End If
                Exit Sub
            End If
            hdnrseq.Value = sonumeros(hdnrseq.Value)
            tbprocedimentos = tabprocedimentos.IncluirAlterarDados("insert into tbguias_exames (nrseqconvenio, nrseqagenda, nrseqproced, nrseqplano, valorproced, valorplano, valorcliente, ativo, nrseqempresa) values (" & ddlConvenio.SelectedValue & ", " & sonumeros(hdnrseq.Value) & ", " & ddlProcedimentos.SelectedValue & ", " & ddlPlano.SelectedValue & ", " & moeda(txtvalorprocedimento.Value) & ", " & moeda(txtvalorplano.Value) & ", " & moeda(txtvalorpaciente.Value) & ", TRUE, '" & Session("idempresaemuso") & "')")

            carregargrade(hdnrseq.Value)

            'Dim tabela As New DataTable()


            'gdvListaExames.DataSource = tabela
            'gdvListaExames.DataBind()

            'tabela.Columns.Add("nrseq")
            'tabela.Columns.Add("lab")
            'tabela.Columns.Add("procedimento")
            'tabela.Columns.Add("valor")
            'tabela.Columns.Add("vlplano")
            'tabela.Columns.Add("vlpaciente")

            'For x As Integer = 0 To tbprocedimentos.Rows.Count - 1

            '    tbps = tabps.conectar("select distinct nomefantasia from tbconvenios where  nrseq = '" & tbprocedimentos.Rows(x)("nrseqconvenio").ToString & "'")

            '    Dim convenio As String = tbps.Rows(0)("nomefantasia").ToString

            '    tbps = tabps.conectar("select distinct descricao from tbprocedimentos where nrseq = '" & tbprocedimentos.Rows(x)("nrseqproced").ToString & "'")

            '    Dim proced As String = tbps.Rows(0)("descricao").ToString

            '    tabela.Rows.Add(tbprocedimentos.Rows(x)("nrseq").ToString, convenio, proced, tbprocedimentos.Rows(x)("valorproced").ToString, tbprocedimentos.Rows(x)("valorplano").ToString, tbprocedimentos.Rows(x)("valorcliente").ToString)
            'Next



            rowError.Style.Add("display", "none")
        Else

            sql = "select c.nrseq as nrseqconvenio, p.nrseq as nrseqprocedimento, p.valorfinal, p.valorcobplano, p.valorpaciente, p.percobplano from tbprocedimentos_grupos as pg Inner join tbprocedimentos_grupos_conv as pgc on  pg.nrseq = pgc.nrseqgrupo inner join tbprocedimentos_grupo_dth as pgd on pgd.nrseqgrupoconv = pgc.nrseq inner join tbprocedimentos as p on p.nrseq = pgd.nrseqprocedimento inner join tbconvenios as c on c.nrseq = pgc.nrseqconveniado where  pgd.ativo = true and pg.nrseq = " & ddlGrupos.SelectedValue
            'pg.nrseqempresa = '" & Session("idempresaemuso") & "' AND
            Dim tb2 As DataTable = findSql(sql)

            If tb2.Rows.Count > 0 Then
                For x As Integer = 0 To tb2.Rows.Count - 1

                    Dim percentualplano As Double = 0
                    Dim percentualpaciente As Double = 0
                    Dim valorfinal As Double = 0

                    Dim valorprocedimento As Double = 0
                    Dim valorpaciente As Double = 0
                    Dim valorplano As Double = 0

                    valorfinal = tb2.Rows(x)("valorfinal").ToString
                    percentualplano = IIf(tb2.Rows(x)("percobplano").ToString = "", 0, tb2.Rows(x)("percobplano").ToString)
                    percentualpaciente = IIf(tb2.Rows(x)("valorpaciente").ToString = "", 0, tb2.Rows(x)("valorpaciente").ToString)

                    valorprocedimento = valorfinal
                    valorplano = valorfinal * (percentualplano / 100)

                    If valorplano = 0 Then
                        valorpaciente = valorprocedimento
                    Else

                        valorpaciente = valorfinal * (percentualpaciente / 100)
                    End If

                    sql = "insert into tbguias_exames (nrseqconvenio, nrseqagenda, nrseqproced, nrseqplano, valorproced, valorplano, valorcliente, ativo, nrseqempresa ) values (" & tb2.Rows(x)("nrseqconvenio").ToString & ", " & hdnrseq.Value & ", " & tb2.Rows(x)("nrseqprocedimento").ToString & ", " & ddlPlano.SelectedValue & ", " & moeda(valorprocedimento) & ", " & moeda(valorplano) & ", " & moeda(valorpaciente) & ", TRUE, '" & Session("idempresaemuso") & "')"
                    persist(sql)

                Next
                carregargrade(hdnrseq.Value)
            Else
                Dim erro As String = "Não é possivel utilizar um grupo que esteja sem procedimentos!"
                ' Quantas você quiser dessa coisa... pra cada campo

                If erro <> "" Then
                    rowErro2.Style.Remove("display")
                    lblErro2.InnerHtml = erro
                End If
                Exit Sub
            End If




        End If

    End Sub

    Private Sub carregargrade(loteexames As Integer)
        Dim sql As String = "Select * from vwagenda_exames where nrseq = '" & hdnrseq.Value & "' and exameativo = true " 'and nrseqempresa = '" & Session("idempresaemuso") & "'"

        tbprocedimentos = tabprocedimentos.conectar(sql)
        gdvListaExames.DataSource = tbprocedimentos
        gdvListaExames.DataBind()
        wctotalpaciente = 0
        For Each row As GridViewRow In gdvListaExames.Rows
            wctotalpaciente += row.Cells(5).Text
        Next

        txtvalortotalpaciente.Value = FormatCurrency(wctotalpaciente)

        'sql = "select sum(valorcliente) as totalvalorcliente from vwagenda_exames group by nrseq, exameativo, nrseqempresa having nrseq = '" & hdnrseq.Value & "' and exameativo = true  and nrseqempresa = '" & Session("idempresaemuso") & "'"

        'tbprocedimentos = tabprocedimentos.conectar(sql)
        'If tbprocedimentos.Rows.Count > 0 AndAlso tbprocedimentos.Rows(0)("totalvalorcliente").ToString <> "" Then
        '    txtvalortotalpaciente.Value = FormatCurrency(tbprocedimentos.Rows(0)("totalvalorcliente").ToString)
        'Else
        '    txtvalortotalpaciente.Value = FormatCurrency(0)
        'End If


    End Sub

    Public Function verificaCampo(input As String, campo As String, erro As String) As String
        If input = "" Then
            If erro <> "" Then
                erro &= ",<br>"
            Else
                erro &= "<br>"
            End If
            erro &= campo
        End If
        Return erro
    End Function

    Private Sub ddlProcedimentos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProcedimentos.SelectedIndexChanged
        procedimentos = ddlProcedimentos.SelectedValue
        adcionaValores()
    End Sub

    Private Function adcionaValores()
        Dim percentualplano As Double = 0
        Dim percentualpaciente As Double = 0
        Dim valorfinal As Double = 0

        Dim sql As String = "select * from vwprocedimentos where descricao = '" & ddlProcedimentos.SelectedItem.Text & "'" '  AND nrseqempresa = '" & Session("idempresaemuso") & "' "

        If ddlConvenio.Text <> "" Then
            sql &= " and nomeempresa = '" & ddlConvenio.SelectedItem.Text & "'"
        End If

        If ddlPlano.Text <> "" Then
            sql &= " and plano = '" & ddlPlano.SelectedItem.Text & "'"
        End If




        tbprocedimentos = tabprocedimentos.conectar(sql)

        If tbprocedimentos.Rows.Count = 0 Then
            sm("swal.fire({title: 'Ops!',text: 'Esse procedimento não existe para esse plano !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Function
        End If

        valorfinal = tbprocedimentos.Rows(0)("valorfinal").ToString
        percentualplano = IIf(tbprocedimentos.Rows(0)("percobplano").ToString = "", 0, tbprocedimentos.Rows(0)("percobplano").ToString)
        percentualpaciente = IIf(tbprocedimentos.Rows(0)("valorpaciente").ToString = "", 0, tbprocedimentos.Rows(0)("valorpaciente").ToString)

        txtvalorprocedimento.Value = FormatCurrency(valorfinal) ' "R$ " + valorfinal.ToString


        txtvalorplano.Value = FormatCurrency(valorfinal * (percentualplano / 100)) '"R$ " & FormatNumber((valorfinal * (percentualplano / 100)).ToString, 2)

        If CType(txtvalorplano.Value, Decimal) = 0 Then
            txtvalorpaciente.Value = txtvalorprocedimento.Value
        Else

            txtvalorpaciente.Value = FormatCurrency(valorfinal * (percentualpaciente / 100))  '"R$ " & FormatNumber((valorfinal * 
        End If

        '  (percentualpaciente / 100)).ToString, 2)

    End Function

    Private Sub gdvListaExames_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gdvListaExames.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gdvListaExames.Rows(index)

        If e.CommandName = "deletar" Then

            Dim nrseq As String = row.Cells(0).Text
            'tbps = tabps.conectar("SELECT * FROM tbguias_exames WHERE nrseq = '" & nrseq & "'")

            tbps = tabps.IncluirAlterarDados("UPDATE tbguias_exames SET ativo = false WHERE  nrseq = " & row.Cells(0).Text & " ")



            carregargrade(hdnrseq.Value)







        End If
    End Sub

    Private Sub gdvListaReuniao_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvListaExames.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            If Server.HtmlDecode(e.Row.Cells(5).Text).Trim = "" Then e.Row.Cells(5).Text = 0

            If Server.HtmlDecode(e.Row.Cells(3).Text).Trim = "" Then e.Row.Cells(3).Text = 0
            If Server.HtmlDecode(e.Row.Cells(4).Text).Trim = "" Then e.Row.Cells(4).Text = 0

            e.Row.Cells(3).Text = "R$ " & FormatNumber(e.Row.Cells(3).Text, 2)
            e.Row.Cells(4).Text = "R$ " & FormatNumber(e.Row.Cells(4).Text, 2)
            e.Row.Cells(5).Text = "R$ " & FormatNumber(e.Row.Cells(5).Text, 2)
        End If



    End Sub

    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click
        Dim wcnrseqctrl As String = gerarnrseqcontrole()
        tbprocedimentos = tabprocedimentos.IncluirAlterarDados("insert into tbguias (usercad, dtcad, nrseqctrl, ativo, emitido, nrseqempresa) values ('" & Session("usuario") & "','" & formatadatamysql(Date.Now.Date) & "','" & wcnrseqctrl & "', true, false, '" & Session("idempresaemuso") & "')")
        tbprocedimentos = tabprocedimentos.conectar("select * from tbguias where nrseqctrl = '" & wcnrseqctrl & "'")

        If tbprocedimentos.Rows.Count <> 0 Then
            hdnrseq.Value = tbprocedimentos.Rows(0)("nrseq").ToString
        Else
            hdnrseq.Value = 1
        End If
        btnnovo.Enabled = False

        habilita()
    End Sub

    Private Function bloqueio()
        ddlPlano.Enabled = False
        ddlConvenio.Enabled = False
        ddlProcedimentos.Enabled = False
        txtvalorprocedimento.Disabled = True
        txtvalorplano.Disabled = True
        txtvalorpaciente.Disabled = True
        btnOK.Enabled = False
        btngrupos.Enabled = False
        ddlGrupos.Enabled = False

        ddlPlano.SelectedValue = ""
        ddlConvenio.SelectedValue = ""
        ddlProcedimentos.SelectedItem.Text = ""
        txtvalorprocedimento.Value = ""
        txtvalorplano.Value = ""
        txtvalorpaciente.Value = ""
    End Function

    Private Function habilita()
        ddlPlano.Enabled = True
        ddlConvenio.Enabled = True
        btngrupos.Enabled = True
    End Function

    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function
    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function



    Private Sub btngerar_Click(sender As Object, e As EventArgs) Handles btngerar.Click
        versessao()
        Dim sss As List(Of String) = New List(Of String)
        Dim nrseqdefinitivo As String = ""
        Dim nome As String = ""
        Dim edependente As Boolean = False
        Dim nrseqdependente As String = "0"
        For Each row As GridViewRow In gdvListaClientes.Rows
            Dim cb As CheckBox = CType(row.FindControl("sel"), CheckBox)
            If cb.Checked Then
                Dim clientenrseq As String = ""
                clientenrseq = row.Cells(2).Text
                nrseqdefinitivo = row.Cells(2).Text
                nome = row.Cells(4).Text
                If row.Cells(13).Text = 1 Then
                    edependente = True
                    If row.Cells(14).Text <> 0 Then
                        nrseqdependente = row.Cells(14).Text
                    End If
                End If
                sss.Add(clientenrseq)
            End If
        Next

        If sss.Count <= 0 Then
            Dim erro As String = "Por favor selecione ao menos 1 cliente"
            ' Quantas você quiser dessa coisa... pra cada campo


            If erro <> "" Then
                rowErro2.Style.Remove("display")
                lblErro2.InnerHtml = erro
            End If
        ElseIf sss.Count > 1 Then

            Dim erro As String = "Por favor selecione apenas 1 cliente"
            ' Quantas você quiser dessa coisa... pra cada campo

            If erro <> "" Then
                rowErro2.Style.Remove("display")
                lblErro2.InnerHtml = erro
            End If
        Else
            'Session("nrseqagenda")
            If txtvalortotalpaciente.Value = "" Then
                Dim erro As String = "O valor total não pode ser zero"
                ' Quantas você quiser dessa coisa... pra cada campo

                If erro <> "" Then
                    rowErro2.Style.Remove("display")
                    lblErro2.InnerHtml = erro
                End If
            Else
                Dim barra As New Barcode
                Dim serial As String = gerarserialprocedimento(hdnrseq.Value)


                Dim img As Drawing.Image = barra.gerarbarra(mRight(serial, 10), 100, 500)

                img.Save(Server.MapPath("~/imgcodbarras/" & serial & ".png"), Imaging.ImageFormat.Png)


                Dim caixa As New clsCaixas
                Dim dth As New clscaixasdth
                caixa.Funcionario = Session("usuario")
                caixa.Nrsequsuario = Session("idusuario")
                dth.Descricao = "Pagamento Procedimentos - " & nome & "-" & ddlConvenio.SelectedItem.Text
                dth.Tipolancamento = "C"
                dth.Usercad = Session("usuario")
                dth.Valor = txtvalortotalpaciente.Value
                dth.Qtdparcelas = 1
                dth.Operacao = "C"

                caixa.Detalhes.Add(dth)
                caixa.entrada()


                sql = "UPDATE tbguias SET ativo = true, serial = '" & serial & "',data = '" & formatadatamysql(data) & "', hora = '" & hora(True) & "', nrseqcliente = '" & nrseqdefinitivo & "', solicitadopor = '" & txtsolicitadopor.Value & "', imgcodbarras = '" & serial & ".png'  ,  valortotal = " & txtvalortotalpaciente.Value.Replace("R$", "").Replace(".", "").Replace(",", ".") & ", emitido = true, dtemitido = '" & formatadatamysql(data) & "', useremitido = '" & Session("usuario") & "', gerado = 1, edependente = " & edependente & ", nrseqdependente = '" & nrseqdependente & "', nrseqcaixa = " & caixa.Nrseq & " WHERE  nrseq = " & hdnrseq.Value
                persist(sql)

                'Dim caixa As New clsCaixa_new(Session("idusuario"), Session("idempresaemuso"))

                'caixa.inserirLancamento("Pagamento Procedimentos - " & nome & "-" & ddlConvenio.SelectedItem.Text, "C", txtvalortotalpaciente.Value.Replace("R$", "").Replace(".", "").Replace(",", "."), "", 0, ddlPagamento.SelectedItem.Text, tplancamento.Exames, hdnrseq.Value)

                ' Response.Redirect("/restrito/imprimir_exames.aspx?i=" & hdnrseq.Value)
                tbprocedimentos = tabprocedimentos.conectar("Select * from vwagenda_exames where nrseq = 0")
                gdvListaExames.DataSource = tbprocedimentos
                gdvListaExames.DataBind()


                Dim raiz As String = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
                Dim r As String = ""
                Dim salvarEm As String = Server.MapPath("~/")

                Dim arquivo As String = "guiaexame" & hdnrseq.Value & "_" & DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString & ".pdf"
                salvarEm &= "restrito\guias\" & arquivo
                Dim FullPath = Request.Url.OriginalString.Replace("emitir_procedimentos.aspx", "imprimir_exames.aspx?i=" & hdnrseq.Value & "&d=" & edependente)

                sql = "SELECT DISTINCT(nrseqconvenio) AS idconvenio FROM tbguias_exames WHERE nrseqagenda ='" & hdnrseq.Value & "'"

                Dim lista As List(Of String) = converteDatatableToListOfString(findSql(sql), "idconvenio")
                Dim inteiro As Integer = 0
                For Each c As String In lista
                    inteiro = inteiro + 1
                    sm("window.open('" & FullPath & "&c=" & c & "', '_blank')", "imprimir_exames" & inteiro)
                Next


                '  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "irlink(" & hdnrseq.Value & ");", "irlink(" & hdnrseq.Value & ");", True)
                rowErro2.Style.Add("display", "none")

                ddlPlano.Enabled = True
                ddlConvenio.Enabled = True
                ddlProcedimentos.Enabled = False
                btnOK.Enabled = True
                'ddlPlano.Enabled = True
                'ddlConvenio.Enabled = True

                btnTrocar.Enabled = False
                btntrava.Enabled = True
                btnnovo.Enabled = True
                btngerar.Enabled = False

                limpar()
            End If
        End If


    End Sub
    Private Function converteDatatableToListOfString(datatable As DataTable, Optional campo As String = "nrseq") As List(Of String)
        Dim listaS As List(Of String) = New List(Of String)

        For x As Integer = 0 To datatable.Rows.Count - 1
            If datatable(x)(campo).GetType IsNot GetType(DBNull) Then
                listaS.Add(datatable(x)(campo).ToString)
            End If
        Next

        Return listaS
    End Function
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub limpar()
        txtsolicitadopor.Value = ""
        txtvalorpaciente.Value = ""
        txtvalorplano.Value = ""
        txtvalorprocedimento.Value = ""
        txtvalortotalpaciente.Value = ""
        carregargrade(0)
        Dim tbclientes As New Data.DataTable
        Dim tabclientes As New clsBanco
        tbclientes = tabclientes.conectar("select * from tbclientes where 1<>1 limit 1")
        gdvListaClientes.DataSource = tbclientes
        gdvListaClientes.DataBind()
    End Sub
    Private Sub btnprocura_Click(sender As Object, e As EventArgs) Handles btnprocura.Click
        versessao()
        If ddpListarPor.SelectedValue = 1 Then
            sql = "select *, 0 AS edependente, 0 AS nrseqdependente from tbclientes where nome LIKE '%" & txtbusca.Text & "%'   "
            gdvListaClientes.DataSource = findSql(sql)
            gdvListaClientes.DataBind()

        ElseIf ddpListarPor.SelectedValue = 2 Then
            sql = "select *, 0 AS edependente, 0 AS nrseqdependente from tbclientes where matricula LIKE  '%" & txtbusca.Text & "%'  "
            gdvListaClientes.DataSource = findSql(sql)
            gdvListaClientes.DataBind()

        ElseIf ddpListarPor.SelectedValue = 3 Then
            sql = "select *, 0 AS edependente, 0 AS nrseqdependente from tbclientes where cpf LIKE  '%" & txtbusca.Text.Replace(".", "") & "%'   "
            gdvListaClientes.DataSource = findSql(sql)
            gdvListaClientes.DataBind()

        ElseIf ddpListarPor.SelectedValue = 4 Then
            '  sql = " SELECT cli.nrseq AS nrseq, cli.matricula AS matricula, 1 AS edependente, dep.nome AS nome, dep.cpf AS cpf, dep.email AS email, "
            sql = " SELECT cli.nrseq AS nrseq, cli.matricula AS matricula, dep.nrseq AS nrseqdependente, 1 AS edependente, dep.nome AS nome, dep.cpf AS cpf, dep.email AS email, "
            sql &= " dep.telcelular AS telcelular, telresidencial as telresidencia, endereco as logradouro, dep.numero AS numero, dep.cidade AS cidade, dep.uf AS uf "
            sql &= " FROM tbdependentes AS dep "
            sql &= " LEFT JOIN tbclientes AS cli ON dep.matricula = cli.matricula "
            sql &= " WHERE dep.nome LIKE '%" & txtbusca.Text & "%'   "
            ' sql = "select *, telresidencial as telresidencia, endereco as logradouro from tbdependentes where nome LIKE  '%" & txtbusca.text & "%' AND cliente = '" & Session("empresaemuso") & "' "
            gdvListaClientes.DataSource = findSql(sql)



            gdvListaClientes.DataBind()
        End If
    End Sub

    Public Function gerarserialprocedimento(ByVal wcnrseq As Integer) As String

        Dim wchoraatual As Integer = Date.Now.TimeOfDay.Seconds + Date.Now.Hour + Date.Now.Minute
        Dim serial1 As Integer = Date.Now.Year + Date.Now.Day + Date.Now.Month + Date.Now.DayOfWeek + Date.Now.DayOfYear + wchoraatual
        Dim serial2 As Integer = wchoraatual + (5000 * wcnrseq)
        Dim serial3 As String = zeros(wcnrseq.ToString & serial1.ToString & serial2.ToString, 20)

        Return serial3
    End Function



    Private Sub btntrava_Click(sender As Object, e As EventArgs) Handles btntrava.Click
        ddlProcedimentos.Items.Clear()
        Dim sql As String = "select  nrseq, descricao from vwprocedimentos where 1=1 " ' AND nrseqempresa = '" & Session("idempresaemuso") & "' "

        If ddlConvenio.Text <> "" Then
            sql &= " and nomeempresa = '" & ddlConvenio.SelectedItem.Text & "'"
        End If

        If ddlPlano.Text <> "" Then
            sql &= " and plano = '" & ddlPlano.SelectedItem.Text & "'"
        End If

        sql &= " order by descricao"


        tbprocedimentos = tabprocedimentos.conectar(sql)

        ddlProcedimentos.Items.Add("")
        ddlProcedimentos.Items(0).Value = 0
        For x As Integer = 0 To tbprocedimentos.Rows.Count - 1
            ddlProcedimentos.Items.Add(tbprocedimentos.Rows(x)("descricao").ToString)
            ddlProcedimentos.Items(x + 1).Value = tbprocedimentos.Rows(x)("nrseq").ToString
        Next


        'ddlProcedimentos.DataSource = tbprocedimentos
        'ddlProcedimentos.DataMember = "nrseq"
        'ddlProcedimentos.DataTextField = "descricao"
        'ddlProcedimentos.DataValueField = "nrseq"
        'ddlProcedimentos.DataBind()



        ddlPlano.Enabled = False
        ddlConvenio.Enabled = False
        ddlProcedimentos.Enabled = True
        btnOK.Enabled = True
        'ddlPlano.Enabled = True
        'ddlConvenio.Enabled = True
        txtvalorprocedimento.Disabled = True
        txtvalorplano.Disabled = True
        txtvalorpaciente.Disabled = True
        btnTrocar.Enabled = True
        btnTrocar.Enabled = True
        btngerar.Enabled = True
    End Sub

    Private Sub btnTrocar_Click(sender As Object, e As EventArgs) Handles btnTrocar.Click
        ddlPlano.Enabled = True
        ddlConvenio.Enabled = True
        ddlProcedimentos.Enabled = False
        btnOK.Enabled = True
        'ddlPlano.Enabled = True
        'ddlConvenio.Enabled = True

        btnTrocar.Enabled = False
        btntrava.Enabled = True
        btnnovo.Enabled = True
        btngerar.Enabled = False
    End Sub

    Private Sub gdvListaExames_DataBound(sender As Object, e As EventArgs) Handles gdvListaExames.DataBound

    End Sub

    Private Sub btnimprimir_Click(sender As Object, e As EventArgs) Handles btnimprimir.Click
        versessao()
        Dim sss As List(Of String) = New List(Of String)
        Dim nrseqdefinitivo As String = ""
        Dim nome As String = ""
        Dim edependente As Boolean = False
        Dim nrseqdependente As String = "0"
        For Each row As GridViewRow In gdvListaClientes.Rows
            Dim cb As CheckBox = CType(row.FindControl("sel"), CheckBox)
            If cb.Checked Then
                Dim clientenrseq As String = ""
                clientenrseq = row.Cells(2).Text
                nrseqdefinitivo = row.Cells(2).Text
                nome = row.Cells(4).Text
                If row.Cells(13).Text = 1 Then
                    edependente = True
                    If row.Cells(14).Text <> 0 Then
                        nrseqdependente = row.Cells(14).Text
                    End If
                End If

                sss.Add(clientenrseq)
            End If
        Next

        If sss.Count <= 0 Then
            Dim erro As String = "Por favor selecione ao menos 1 cliente"
            ' Quantas você quiser dessa coisa... pra cada campo


            If erro <> "" Then
                rowErro2.Style.Remove("display")
                lblErro2.InnerHtml = erro
            End If
        ElseIf sss.Count > 1 Then

            Dim erro As String = "Por favor selecione apenas 1 cliente"
            ' Quantas você quiser dessa coisa... pra cada campo

            If erro <> "" Then
                rowErro2.Style.Remove("display")
                lblErro2.InnerHtml = erro
            End If
        Else
            'Session("nrseqagenda")
            If txtvalortotalpaciente.Value = "" Then
                Dim erro As String = "O valor total não pode ser zero"
                ' Quantas você quiser dessa coisa... pra cada campo

                If erro <> "" Then
                    rowErro2.Style.Remove("display")
                    lblErro2.InnerHtml = erro
                End If
            Else


                sql = "UPDATE tbguias SET ativo = true ,data = '" & formatadatamysql(data) & "', hora = '" & hora(True) & "', nrseqcliente = '" & nrseqdefinitivo & "', solicitadopor = '" & txtsolicitadopor.Value & "', valortotal = " & txtvalortotalpaciente.Value.Replace("R$", "").Replace(".", "").Replace(",", ".") & ", emitido = false, gerado = 0, edependente = " & edependente & ", nrseqdependente = '" & nrseqdependente & "' WHERE  nrseq = " & hdnrseq.Value
                persist(sql)

                Dim FullPath = Request.Url.OriginalString.Replace("emitir_procedimentos.aspx", "imprimir_orcamento.aspx?i=" & hdnrseq.Value)
                '  sql = "SELECT DISTINCT(nrseqconvenio) AS idconvenio FROM tbguias_exames WHERE nrseqagenda ='" & hdnrseq.Value & "'"

                '  Dim lista As List(Of String) = converteDatatableToListOfString(findSql(sql), "idconvenio")
                '  Dim inteiro As Integer = 0
                '   For Each c As String In lista
                '  inteiro = inteiro + 1
                '   sm("window.open('" & FullPath & "&c=" & c & "', '_blank')", "imprimir_orcamento" & inteiro)
                '    Next

                Response.Redirect("/restrito/imprimir_orcamento.aspx?i=" & hdnrseq.Value & "&d=" & edependente)
            End If
        End If
    End Sub

    Private Sub btngrupos_Click(sender As Object, e As EventArgs) Handles btngrupos.Click
        If ddlPlano.SelectedValue <> "" Then
            ddlConvenio.Enabled = False
            btntrava.Enabled = False
            ddlProcedimentos.Enabled = False
            txtvalorprocedimento.Disabled = True
            txtvalorplano.Disabled = True
            txtvalorpaciente.Disabled = True
            btnOK.Enabled = True
            btnTrocar.Enabled = False
            rowError.Style.Add("display", "none")
            rowGrupos.Style.Remove("display")
            ddlGrupos.Enabled = True
        Else
            Dim erro As String = ""
            erro = verificaCampo(ddlPlano.SelectedValue, "Plano", erro)

            If erro <> "" Then
                rowError.Style.Remove("display")
                lblErro.InnerHtml = erro
            End If
        End If
    End Sub

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click
        bloqueio()
        hdnrseq.Value = ""
        btnnovo.Enabled = True
        rowGrupos.Style.Add("display", "none")
        txtvalortotalpaciente.Value = ""
        gdvListaExames.DataSource = Nothing
        gdvListaExames.DataBind()
    End Sub

    Private Sub gdvListaClientes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvListaClientes.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim lblitem As Label = e.Row.FindControl("lblitem")
        lblitem.Text = e.Row.RowIndex + 1
    End Sub
End Class

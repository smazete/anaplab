﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage2.master" CodeFile="novosassociados.aspx.vb" Inherits="novosassociados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <%-- sweetalert2 monitorando qual vou usar  --%>

    <%-- sweetalert2 monitorando qual vou usar  --%>
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
    <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="/dist/js/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>

    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />

    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/JScriptmascara.js" type="text/javascript"></script>
    <style>
        @media screen and (max-width: 600px) {
            .texto-mobile {
                padding: 0px 15px;
            }
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="card" style="background-image: url('img/odd_background.png');">
                <br>
                <div class="card-header">
                    <br />
                    <center><h3><asp:label text="FORMULÁRIO DE FILIAÇÃO" runat="server" style="font-size:20px;" /></h3></center>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <center><p class="texto-mobile">Os campos <span style="color: red;">*</span> são de preenchimento obrigatorios a pos o envio a equipe Anaplab ira verificar e aprovar seu cadastro!</p> </center>
                                <br />
                                <div class="box-tools pull-right">
                                    <!-- Here is a label for example -->
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <%-- dados de documentos profissionais  primeiro formulario --%>
                                    <div class="col-lg-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <b>DADOS E DOCUMENTOS PROFISSIONAIS </b>
                                            </div>
                                            <div class="card-body bg-aqua">
                                                <br />
                                                <div class="row" style="margin-top: 7px;">
                                                    <div class="col-lg-4"><span style="color: red; font-variant-east-asian: simplified">*</span>MATRICULA</div>
                                                    <div class="col-lg-6">
                                                        <asp:TextBox ID="txtmatricula" runat="server" CssClass="form-control" placeholder="Matricula"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 7px;">
                                                    <div class="col-lg-4"><span style="color: red; font-variant-east-asian: simplified">*</span>SITUAÇÃO</div>
                                                    <div class="col-lg-7">
                                                        <asp:DropDownList ID="cbosituacaopb" runat="server" CssClass="form-control" placeholder="Situação" AutoPostBack="true">
                                                            <asp:ListItem Text="Aposentado" Value="1" />
                                                            <asp:ListItem Text="Ativo" Value="2" />
                                                            <asp:ListItem Text="Pensionista" Value="3" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 7px;">
                                                    <div class="col-lg-4"><span style="color: red; font-variant-east-asian: simplified">*</span>DATA POSSE</div>
                                                    <div class="col-lg-7">
                                                        <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtposse" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 7px;">
                                                    <div class="col-lg-4"><span style="color: red; font-variant-east-asian: simplified">*</span>DATA APOSENTADORIA</div>
                                                    <div class="col-lg-7">
                                                        <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtaposentadoria" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--INFORMAÇÔES segundo formulario --%>
                                    <div class="col-lg-6">
                                    </div>
                                    <%--DADOS E DOCUMENTOS PESSOAIS segundo formulario --%>
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <br />
                                            <br />
                                            <div class="card">
                                                <div class="card-header">
                                                    <b>DADOS E DOCUMENTOS PESSOAIS </b>
                                                </div>
                                                <div class="card-body bg-aqua">
                                                    <br />
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>CPF</div>
                                                        <div class="col-lg-5">
                                                            <asp:TextBox runat="server" type="text" CssClass="form-control cnpjcpf" ID="txtcpf" ClientIDMode="Static"  onkeyup="formataCnpjCpf(this,event);" MaxLength="14"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>RG</div>
                                                        <div class="col-lg-5">
                                                            <asp:TextBox ID="txtrg" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>NOME</div>
                                                        <div class="col-lg-9">
                                                            <asp:TextBox ID="txtnome" runat="server" CssClass="form-control" placeholder="Nome"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>NOME MÃE</div>
                                                        <div class="col-lg-9">
                                                            <asp:TextBox ID="txtnomemae" runat="server" CssClass="form-control" placeholder="Nome MÃE"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>E-MAIL</div>
                                                        <div class="col-lg-7">
                                                            <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" placeholder="E-mail"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>CELULAR</div>
                                                        <div class="col-lg-7">
                                                            <asp:TextBox ID="txtcelular" runat="server" CssClass="form-control" onkeyup="formataTelefoneC(this,event);" MaxLength="15" placeholder="(DDD) 90000-0000"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3">TELEFONE</div>
                                                        <div class="col-lg-7">
                                                            <asp:TextBox ID="txttelefone" runat="server" CssClass="form-control" onkeyup="formataTelefone(this,event);" MaxLength="14" placeholder="(DDD) 0000-0000"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>DATA DE NASCIMENTO </div>
                                                        <div class="col-lg-7">
                                                            <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtnasc" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>SEXO</div>
                                                        <div class="col-lg-5">
                                                            <asp:DropDownList ID="cbosexo" runat="server" CssClass="form-control">
                                                                <asp:ListItem Text="Masculino" />
                                                                <asp:ListItem Text="Feminino" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <%-- endereços segundo formulario --%>

                                        <asp:UpdatePanel runat="server">                                         
                                            <ContentTemplate>
                                        <div class="col-lg-6">
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <div class="card">
                                                <div class="card-header">
                                                    <b>ENDEREÇO </b>
                                                </div>
                                                <div class="card-body bg-aqua">
                                                    <br />
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"><span style="color: red; font-variant-east-asian: simplified">*</span>CEP</div>
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtcep" runat="server" CssClass="form-control" placeholder="00000-000" AutoPostBack="true" MaxLength="9" onkeyup="formataCEP(this,event);"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"> <span style="color: red; font-variant-east-asian: simplified">*</span>UF</div>
                                                        <div class="col-lg-3">
                                                            <asp:TextBox ID="txtestado" runat="server" CssClass="form-control" placeholder="SC"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"> <span style="color: red; font-variant-east-asian: simplified">*</span>CIDADE</div>
                                                        <div class="col-lg-9">
                                                            <asp:TextBox ID="txtcidade" runat="server" CssClass="form-control" placeholder="Cidade"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"> <span style="color: red; font-variant-east-asian: simplified">*</span>ENDEREÇO</div>
                                                        <div class="col-lg-9">
                                                            <asp:TextBox ID="txtendereco" runat="server" CssClass="form-control" placeholder="Endereço"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"> <span style="color: red; font-variant-east-asian: simplified">*</span>BAIRRO</div>
                                                        <div class="col-lg-9">
                                                            <asp:TextBox ID="txtbairro" runat="server" CssClass="form-control" placeholder="Bairro"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"> <span style="color: red; font-variant-east-asian: simplified">*</span>COMPLEMENTO</div>
                                                        <div class="col-lg-9">
                                                            <asp:TextBox ID="txtcomplemento" runat="server" CssClass="form-control" placeholder="Casa"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top: 7px;">
                                                        <div class="col-lg-3"> <span style="color: red; font-variant-east-asian: simplified">*</span>NUMERO</div>
                                                        <div class="col-lg-3">
                                                            <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control" placeholder="N°"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                    <%-- DADOS ACESSO segundo formulario --%>

                                    <div class="row"></div>
                                    <div class="row">

                                        <br />
                                        <div class="col-lg-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <b>DADOS PARA ACESSO AO AUTOATENDIMENTO</b>
                                                </div>
                                                <div class="card-body">
                                                    <div class="row">O acesso ao autoatendimento é efetuado com sua matrícula e uma senha, exclusiva, que deve ser cadastrada no campo abaixo.</div>
                                                    <%--  <div class="col-lg-8" style="margin-top: 7px;">
                                                <asp:TextBox ID="txtmatricula2" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>--%>
                                                    <div class="col-lg-2">
                                                        <br />
                                                        <span style="color: red; font-variant-east-asian: simplified">*</span>SENHA:</div>

                                                    <div class="col-lg-10" style="margin-top: 7px;">
                                                        <asp:TextBox type="password" ID="txtsenha" runat="server" CssClass="form-control"></asp:TextBox><span style="color: red">*Não utilize sua senha bancária.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <b>Dados Bancários</b>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="col-lg-5">
                                                            <span style="color: red; font-variant-east-asian: simplified">*</span>
                                                            AGENCIA
                                                        </div>
                                                        <div class="col-lg-7">
                                                            <asp:TextBox ID="txtAgencia" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-5" style="margin-top: 7px;">
                                                            <span style="color: red; font-variant-east-asian: simplified">*</span>
                                                            CONTA CORRENTE
                                                        </div>
                                                        <div class="col-lg-7" style="margin-top: 7px;">
                                                            <asp:TextBox ID="txtcontacorrente" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <br />
                                <div class="row">
                                    <br />
                                    <div class="card">
                                        <div class="card-header">
                                            <b></b>
                                        </div>
                                        <div class="card-body bg-aqua">
                                            <p>* Manifesto meu interesse em associar-me à ANAPLAB, a partir desta data, e providenciarei a confirmação da autorização das TRANSFERÊNCIAS AUTOMÁTICAS das mensalidades em minha conta corrente acima identificada, conforme orientações expedidas pela TESOURARIA.</p>
                                            <p>* Valor da Mensalidade: R$ 20,00 (vinte reais)</p>
                                            <p>* Ao clicar em "ENVIAR" você terá completado o cadastro e receberá, em até 48 horas, um e-mail com as instruções para aderir ao débito da mensalidade em conta corrente. Depois de concluída essa etapa, os benefícios estarão disponíveis.</p>
                                            <%--  <asp:CheckBox Style="left:auto" Text=" Estou Ciente" runat="server"  Checked="true"/>--%>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-2"></div>
                                <br />
                                <br />
                                <center>                                
                        <asp:LinkButton   runat="server" id="btnenviar" class="btn btn-primary btn-lg"> ENVIAR </asp:LinkButton> 
                    <%--    <asp:LinkButton   runat="server" id="btnteste" class="btn btn-primary"> Teste </asp:LinkButton> 
                                <asp:label id="teste3" text="text" runat="server" />
                                <asp:label id="teste" text="text" runat="server" />
                                <asp:label id="teste2" text="text" runat="server" />--%>
                        </center>
                            </div>
                        </div>

                    </div>
                </div>




                <!-- /.box -->
            </div>
            <br />
            <br />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>


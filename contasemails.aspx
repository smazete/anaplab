﻿<%@ Page Title="" Language="VB" MasterPageFile="MasterPage.master" AutoEventWireup="false" CodeFile="contasemails.aspx.vb" Inherits="pad_empresa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="/js/JScriptmascara.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            
        
    <div class="panel panel-primary " style="margin-left: 1rem; margin-right: 1rem;">
        <div class="panel-heading ">

            <center>
                Cadastro de Contas e Emails
            </center>

        </div>
        <div class="panel-body ">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:Button ID="btnnovo" runat="server" Text="Novo" CssClass="btn btn-primary " />
                    <br />
                </div>
            </div>


            <div class="row">
               
                <div class="col-md-1">
                    <Asp:Label runat="server" Text="ID:" />
                    <asp:TextBox runat="server" placeholder="ID" CssClass="form-control" ID="txtNrSeq" />
                </div>

                 <div class="col-md-3">
                    <Asp:Label runat="server" Text="Usuário:" />
                    <asp:TextBox runat="server" placeholder="Usuário" CssClass="form-control" ID="txtUsuario" />
                </div>

                 <div class="col-md-3">
                    <Asp:Label runat="server" Text="Senha:" />
                    <asp:TextBox runat="server" placeholder="Senha" CssClass="form-control" ID="txtSenha" />
                </div>

                <div class="col-md-3">
                    <Asp:Label runat="server" Text="SSL:" />
                    <asp:TextBox runat="server" placeholder="SSL" CssClass="form-control" ID="txtSSL" />
                </div>

                 <div class="col-md-2">
                    <Asp:Label runat="server" Text="Padrão:" />
                    <asp:TextBox runat="server" placeholder="Padrao" CssClass="form-control" ID="txtPadrao" />
                </div>
              
               
            </div>
               <br />
            <div class="row">
                <div class="col-md-4">
                    <Asp:Label runat="server" Text="Servidor:" />
                    <asp:TextBox runat="server" placeholder="Servidor" CssClass="form-control" ID="txtServidor" />
                </div>
                <div class="col-md-3">
                    <Asp:Label runat="server" Text="Porta:" />
                    <asp:TextBox runat="server" placeholder="Porta" CssClass="form-control" ID="txtPorta" />
                </div>

            </div>

            <br />
          
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <br />
                        <asp:Button ID="btnsalvar" runat="server" Text="Salvar" CssClass="btn btn-success" />
                    </div>
                </div>
                <div class="row">
                    <hr />
                </div>



                <div class="row">
                        <asp:UpdatePanel ID="updategrade" runat="server">
                <ContentTemplate>

                    <div class="row">

                        <div class="col-md-12 text-center ">

                            <br />
                            <asp:GridView ID="grade" runat="server" CssClass="table table-hover table-striped" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma empresa localizada !" AutoGenerateColumns="false" GridLines="None">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="nrseq" />
                                    <asp:BoundField HeaderText="Usuário" DataField="usuario" />
                                    <asp:BoundField HeaderText="Senha" DataField="senha" />
                                    <asp:BoundField HeaderText="Servidor" DataField="servidor" />
                                    <asp:BoundField HeaderText="Porta" DataField="porta" />
                                    <asp:BoundField HeaderText="SSL" DataField="ssl" />
                                    <asp:TemplateField HeaderText="Ações" runat="server">
                                        <ItemTemplate runat="server">
                                            <asp:LinkButton ID="Edit" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar">
                                                                             <i class="fas fa-edit"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Deletar">
                                           <i class="fas fa-trash-alt"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>


                </ContentTemplate>
            </asp:UpdatePanel>

                </div>
            </div>
    </div>

            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


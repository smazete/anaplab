﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="quem-somos.aspx.vb"   MasterPageFile="~/MasterPage2.master" Inherits="quem_somos" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




<div class="container">
    <br><br><br>
      <div style="height"60"="">
          <div style="text-align:center"><h3>Associação Nacional dos Participantes do PB1 da PREVI - ANAPLAB</h3></div>

    
    <br><br><>
    
        <p dir="ltr" style="line-height:2.4;margin-top:0pt;margin-bottom:0pt">
            <span><h3 >QUEM SOMOS</h3></span></p>
        <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">Associação Nacional dos Participantes do PB1 da PREVI - ANAPLAB <br>
        A ANAPLAB foi idealizada por um grupo de aposentados e pensionistas do Plano de Benefícios número 1 da PREVI, com a finalidade de somar esforços na defesa dos direitos de todos os integrantes do PB1 da PREVI.</p>
       <br  />
            <h3 >NOSSA MISSÃO </h3></p>
          <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">DESENVOLVER um trabalho VOLUNTÁRIO com COMPROMISSO, ÉTICA, RESPONSABILIDADE, SOLIDARIEDADE e TRANSPARÊNCIA, em prol dos aposentados e pensionistas participantes do Plano de Benefícios 1 (PB1) da PREVI, defendendo seus direitos de forma generalizada de modo a CORRIGIR irregularidades cometidas pelo BB/PREVI e PRESERVAR OS VALORES DOS COMPLEMENTOS E/OU PENSÕES.</p>
          <br />    
            <h3>OBJETIVOS</h3></p>
           <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">RESGATAR OS DIREITOS DOS APOSENTADOS E PENSIONISTAS DO PB1 da PREVI através da proposição de um leque de ações, já disponíveis por nossa Assessoria Jurídica, com base no cumprimento da legislação vigente.

BUSCAR, constantemente, dentro dos fatos atuais ocorridos com o BB/PREVI, novas propostas de ações judiciais que busquem minimizar as perdas dos complementos de aposentadorias e/ou pensões.</p>
          <br />    

        <p>    <span style="color:#0000cd;"><h3 >Diretoria Executiva (CONAD)</h3></span></p>
           <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">ARI ZANELLA <br />

Presidente Administrativo<br /> 
               <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">presidencia@anaplab.com.br</span></p>
         
<br />     
          <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">JANE TORRES DE MELO <br />

Vice-Presidente Administrativo<br /> 
               <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">vicepresidencia@anaplab.com.br</span></p>
         

          <br />     
          <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">LÁZARA RABELO DE ARAÚJO <br />

Vice-Presidente para Assuntos Previdenciários<br />
               <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">previdencia@anaplab.com.br</span></p>
               <br />     
          <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">JOSÉ GILVAN PEREIRA REBOUÇAS <br />

Vice-Presidente Financeiro <br />
               <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">tesouraria@anaplab.com.br</span></p>
         

          <br />
         <p>   <span style="color:#0000cd;"><h3>Conselho Fiscal (CONFI)</h3></span></p>
        <p><span style="color:#0000cd;"><h3>Efetivos</h3></span></p>
        
                <br />
            <p dir="ltr" style="line-height:2.4;margin-top:0pt;margin-bottom:0pt">
            <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">MARIA ELIZABETH GONÇALVES CHAGAS</p>
        <p>
            <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">bethconfi@anaplab.com.br</span></p>
       <br />   
        <p dir="ltr" style="line-height:2.4;margin-top:0pt;margin-bottom:0pt">
            <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">ANA MARIA MOREIRA MOHAMED</p>
        <p>
            <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">cristalconfi@anaplab.com.br</span></p>
       <br />   
        <p>
           <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">MÁXIMO MONTENEGRO ZAMBONI</p>
        <p>
            <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">montenegro@anaplab.com.br</span></p>
        <p>
            &nbsp;</p>
        <p>
            <h3>CONFI - SUPLENTES</h3></p>

        <p dir="ltr" style="line-height:2.4;margin-top:0pt;margin-bottom:0pt">
            <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">CELSO ANTÔNIO BERNARDES DA SILVA</span><span style="font-size: 16px; font-family: Verdana; vertical-align: baseline; white-space: pre-wrap;"> </span></p>
        <p dir="ltr" style="font-size:13.3333px;line-height:2.4;margin-top:0pt;margin-bottom:0pt">
            <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">celsobernardes@anaplab.com.br</span></p>
        <p dir="ltr" style="font-size:13.3333px;line-height:2.4;margin-top:0pt;margin-bottom:0pt">
            &nbsp;</p>
        <p dir="ltr" style="font-size:13.3333px;line-height:2.4;margin-top:0pt;margin-bottom:0pt">
            <p style="font-family: Verdana; font-size:16px; color:darkslateblue;">IVO RITZMANN</span><span style="font-size: 16px; font-family: Verdana; vertical-align: baseline; white-space: pre-wrap;"> </span></p>
        <div>
            <p dir="ltr" style="font-size:13.3333px;line-height:2.4;margin-top:0pt;margin-bottom:0pt">
                <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">ivoritzmannconfi@anaplab.com.br</span></p>
            <p dir="ltr" style="font-size:13.3333px;line-height:2.4;margin-top:0pt;margin-bottom:0pt">
                &nbsp;</p>
            <p dir="ltr" style="font-size:13.3333px;line-height:2.4;margin-top:0pt;margin-bottom:0pt"><p style="font-family: Verdana; font-size:16px; color:darkslateblue;">MANOEL DOS PASSOS MACHADO</p>
                <span style="color: rgb(61, 133, 198); font-family: Verdana; font-size: 16px; white-space: pre-wrap;">manoelmachado@anaplab.com.br</span></p>
        </div>
    
          </asp:Content>
  
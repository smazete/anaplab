﻿Imports System.IO
Imports clsSmart
Imports clssessoes
Imports System.Data

Partial Class restrito_print_cliente
    Inherits System.Web.UI.Page
    Dim tabelaAgenda As String = " vwagenda_exames"
    Dim tabelaEmpresa As String = " tbempresas"
    Dim dtGlobal As Data.DataTable = New Data.DataTable

    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function
    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function

    Private Function carregaEmpresa() As Data.DataTable
        Dim nrseqempresa As String = Session("idempresaemuso")
        Dim sql = "SELECT * FROM " & tabelaEmpresa & " WHERE ativo = true AND nrseq = '" & nrseqempresa & "' "
        Return findSql(sql)
    End Function

    Public Function replaceForSqlMoeda(campo As String) As String
        Return "CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(COALESCE(" & campo & ", 0), 2), ',', ' '), '.', ','), ' ', '.'))"
    End Function

    Public Function replaceForSqlMoedacomSum(campo As String) As String
        Return "CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(SUM(COALESCE(" & campo & ", 0)), 2), ',', ' '), '.', ','), ' ', '.'))"
    End Function

    Private Function converterParaDouble(txt As String) As Double

        If txt <> "" AndAlso txt <> "&nbsp;" Then

            If txt.Contains(" ") Then
                txt = txt.Replace(" ", "")
            End If
            If txt.Contains("%") Then
                txt = txt.Replace("%", "")
            End If
            If txt.Contains("R$") Then
                txt = txt.Replace("R$", "")
            End If

        Else
            Return 0
        End If

        Return txt
    End Function

    Public Function getTabelaAgpClientes(dt As Data.DataTable, dtCliente As DataTable) As Data.DataTable
        Dim tb As New Data.DataTable
        tb.Columns.Add("nomecliente")
        Dim i As Integer = 0
        dtGlobal = dt
        For Each row As Data.DataRow In dtCliente.Rows
            If row("nomecliente").GetType() IsNot GetType(DBNull) Then
                tb.Rows.Add(row("nomecliente"))
            End If
        Next
        Return tb
    End Function


    Private Function getTabelaDataAgp(dt As Data.DataTable, nomecliente As String) As Data.DataTable
        Dim tb As New Data.DataTable
        tb.Columns.Add("nomecliente")
        tb.Columns.Add("data")

        Dim expression As String = "nomecliente = '" & nomecliente & "'"
        Dim rows As DataRow() = dt.Select(expression)

        For Each row As Data.DataRow In rows
            tb.Rows.Add(nomecliente, row("data"))
            getTabelaExames(dt, row("data"), nomecliente)
        Next
        Return tb
    End Function

    Public Function getTabelaData(dt As Data.DataTable, dtData As DataTable) As Data.DataTable
        'gdvNaoAgrupado_Clientes
        'gdvNaoAgrupado_exames
        Dim tb As New Data.DataTable
        tb.Columns.Add("data")
        Dim i As Integer = 0
        dtGlobal = dt
        For Each row As Data.DataRow In dtData.Rows
            If row("data").GetType() IsNot GetType(DBNull) Then
                tb.Rows.Add(row("data"))
            End If

        Next
        Return tb
    End Function

    Private Function getTabelaClientes(dt As Data.DataTable, data As String) As Data.DataTable
        'gdvNaoAgrupado_Clientes
        'gdvNaoAgrupado_exames
        Dim tb As New Data.DataTable
        tb.Columns.Add("data")
        tb.Columns.Add("nomecliente")

        Dim expression As String = "data = '" & data & "'"
        Dim rows As DataRow() = dt.Select(expression)

        For Each row As Data.DataRow In rows
            tb.Rows.Add(data, row("nomecliente"))
            'getTabelaExames(dt, data, row("nomecliente"))
        Next
        Return tb
    End Function

    Private Function getTabelaExames(dt As Data.DataTable, data As String, cliente As String) As Data.DataTable
        'gdvNaoAgrupado_Clientes
        'gdvNaoAgrupado_exames
        Dim tb As New Data.DataTable
        tb.Columns.Add("nrseq")
        tb.Columns.Add("nomeempresa")
        tb.Columns.Add("cod")
        tb.Columns.Add("descricao")
        tb.Columns.Add("nomecliente")
        tb.Columns.Add("valorcusto")
        tb.Columns.Add("valorcliente")
        tb.Columns.Add("valorplano")
        tb.Columns.Add("valortotal")
        tb.Columns.Add("receita")
        tb.Columns.Add("baixado")
        tb.Columns.Add("solicitadopor")

        Dim expression As String = "data = '" & data & "' AND nomecliente = '" & cliente & "'"
        Dim rows As DataRow() = dt.Select(expression)

        Dim valorcusto As Double = 0
        Dim valorcliente As Double = 0
        Dim valorplano As Double = 0
        Dim valortotal As Double = 0
        Dim receita As Double = 0

        For Each row As Data.DataRow In rows
            tb.Rows.Add(row("nrseq"), row("nomeempresa"), row("cod"), row("descricao"), row("nomecliente"), row("valorcusto"), row("valorcliente"), row("valorplano"), row("valortotal"), row("receita"), row("baixado"), row("solicitadopor"))
            valorcusto = valorcusto + converterParaDouble(row("valorcusto"))
            valorcliente = valorcliente + converterParaDouble(row("valorcliente"))
            valorplano = valorplano + converterParaDouble(row("valorplano"))
            valortotal = valortotal + converterParaDouble(row("valortotal"))
            receita = receita + converterParaDouble(row("receita"))
        Next

        tb.Rows.Add("", "Sub Totais do Paciente", "", "", "", FormatCurrency(valorcusto, 2), FormatCurrency(valorcliente, 2), FormatCurrency(valorplano, 2), FormatCurrency(valortotal, 2), FormatCurrency(receita, 2), "", "")
        Return tb
    End Function

    Public Sub data_OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gdvNaoAgrupado_Clientes As GridView = CType(e.Row.FindControl("gdvNaoAgrupado_Clientes"), GridView)

            gdvNaoAgrupado_Clientes.DataSource = getTabelaClientes(dtGlobal, e.Row.Cells(0).Text.ToString)
            gdvNaoAgrupado_Clientes.DataBind()
        End If
    End Sub

    Public Sub cliente_OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gdvNaoAgrupado_exames As GridView = CType(e.Row.FindControl("gdvNaoAgrupado_exames"), GridView)

            gdvNaoAgrupado_exames.DataSource = getTabelaExames(dtGlobal, e.Row.Cells(0).Text.ToString, e.Row.Cells(1).Text.ToString)
            gdvNaoAgrupado_exames.DataBind()
        End If
    End Sub

    Public Sub clienteAgp_OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gdAgrupado_Data As GridView = CType(e.Row.FindControl("gdAgrupado_Data"), GridView)

            gdAgrupado_Data.DataSource = getTabelaDataAgp(dtGlobal, e.Row.Cells(0).Text.ToString)
            gdAgrupado_Data.DataBind()
        End If
    End Sub

    Public Sub dataAgp_OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim gdvNaoAgrupado_exames As GridView = CType(e.Row.FindControl("gdvNaoAgrupado_exames"), GridView)

            gdvNaoAgrupado_exames.DataSource = getTabelaExames(dtGlobal, e.Row.Cells(1).Text.ToString, e.Row.Cells(0).Text.ToString)
            gdvNaoAgrupado_exames.DataBind()
        End If
    End Sub


    Private Sub resumos(sqlWhere As String, quantidadeLinhas As Integer, impressao As String, usuario As String, receita As String, ocultar As String, filtroUtilizado As String)
        dtimpressao.Text = impressao
        totalimpresso.Text = quantidadeLinhas
        lblusuario.Text = usuario
        lblFiltrosUtilizados.InnerText = filtroUtilizado

        If ocultar = "" OrElse ocultar = "0" Then
            Dim sql As String = "SELECT " & replaceForSqlMoedacomSum("valorcliente") & " AS valor FROM " & tabelaAgenda & sqlWhere
            Try
                lblValorCliente.Text = findSql(sql).Rows(0)("valor")
            Catch ex As Exception

            End Try

            Dim sql2 As String = "SELECT " & replaceForSqlMoedacomSum("valorplano") & " AS valor FROM " & tabelaAgenda & sqlWhere
            Try
                lblValorPlano.Text = findSql(sql2).Rows(0)("valor")

            Catch ex As Exception

            End Try
        End If

        If receita <> "" AndAlso receita <> "0" Then
            Dim sql As String = "SELECT " & replaceForSqlMoedacomSum("(valortotal - valorcusto)") & " AS valor FROM " & tabelaAgenda & sqlWhere
            Try
                lblValorReceita.Text = findSql(sql).Rows(0)("valor")
            Catch ex As Exception

            End Try
        End If

        Try
            Dim sql3 As String = "SELECT " & replaceForSqlMoedacomSum("valorcusto") & " AS valor FROM " & tabelaAgenda & sqlWhere
            lblValorCusto.Text = findSql(sql3).Rows(0)("valor")
        Catch ex As Exception

        End Try

        Try
            Dim sql4 As String = "SELECT " & replaceForSqlMoedacomSum("valortotal") & " AS valor FROM " & tabelaAgenda & sqlWhere
            lblValorTotal.Text = findSql(sql4).Rows(0)("valor")
        Catch ex As Exception

        End Try

    End Sub
    Public Sub buscaNaoAgrupado(dateInit As String, dateEnd As String, receita As String, plano As String, ocultar As String, convenio As String, procedimento As String, solicitadoPor As String, exibirProcedimento As String, filtroUtilizado As String)

        Dim sql As String = "SELECT DATE_FORMAT(data, '%d/%m/%Y') AS data, nrseq, nomeempresa, cod, descricao, nomecliente, " & replaceForSqlMoeda("valorcusto") & " AS valorcusto,  IF('1' <> '" & ocultar & "'," & replaceForSqlMoeda("valorcliente") & ", '') AS valorcliente,  IF('1' <> '" & ocultar & "'," & replaceForSqlMoeda("valorplano") & ", '') AS valorplano, " & replaceForSqlMoeda("valortotal") & " AS valortotal, IF(baixado = 1, 'Sim', 'Não') AS baixado, solicitadopor, IF('1' = '" & receita & "'," & replaceForSqlMoeda("( valortotal - valorcusto)") & ", '') AS receita FROM " & tabelaAgenda & ""
        Dim sqlWhere As String = " WHERE 1=1 AND nrseqempresa = '" & Session("idempresaemuso") & "' AND nomecliente <> '' "
        If dateInit <> "" AndAlso dateEnd <> "" Then
            sqlWhere &= " AND data BETWEEN '" & dateInit & "' AND '" & dateEnd & "'"
        End If
        If plano <> "" AndAlso plano <> "0" Then
            sqlWhere &= " AND plano = '" & plano & "'"
        End If
        If convenio <> "" AndAlso convenio <> "0" Then
            sqlWhere &= " AND nrseqconvenio = '" & convenio & "'"
        End If
        If procedimento <> "" AndAlso procedimento <> "0" Then
            sqlWhere &= " AND nrseqproc = '" & procedimento & "'"
        End If

        sql = sql & sqlWhere & " ORDER BY data"

        Dim sql2 As String = "SELECT DISTINCT(DATE_FORMAT(data, '%d/%m/%Y')) AS data FROM " & tabelaAgenda & "" & sqlWhere & " ORDER BY data"

        Dim dt As Data.DataTable = findSql(sql)

        resumos(sqlWhere, dt.Rows.Count, DateTime.Now.ToString("dd/MM/yyyy hh:MM"), Session("usuario"), receita, ocultar, filtroUtilizado)

        gdvNaoAgrupado_Data.DataSource = getTabelaData(dt, findSql(sql2))
        gdvNaoAgrupado_Data.DataBind()

    End Sub

    Public Sub buscaAgrupado(dateInit As String, dateEnd As String, receita As String, plano As String, ocultar As String, convenio As String, procedimento As String, solicitadoPor As String, exibirProcedimento As String, filtroUtilizado As String)

        Dim sql As String = "SELECT DATE_FORMAT(data, '%d/%m/%Y') AS data, nrseq, nomeempresa, cod, descricao, nomecliente, " & replaceForSqlMoeda("valorcusto") & " AS valorcusto,  IF('1' <> '" & ocultar & "'," & replaceForSqlMoeda("valorcliente") & ", '') AS valorcliente,  IF('1' <> '" & ocultar & "'," & replaceForSqlMoeda("valorplano") & ", '') AS valorplano, " & replaceForSqlMoeda("valortotal") & " AS valortotal, IF(baixado = 1, 'Sim', 'Não') AS baixado, solicitadopor, IF('1' = '" & receita & "'," & replaceForSqlMoeda("( valortotal - valorcusto)") & ", '') AS receita FROM " & tabelaAgenda & ""
        Dim sqlWhere As String = " WHERE 1=1 AND nrseqempresa = '" & Session("idempresaemuso") & "' AND nomecliente <> '' "
        If dateInit <> "" AndAlso dateEnd <> "" Then
            sqlWhere &= " AND data BETWEEN ('" & dateInit & "' AND '" & dateEnd & "')"
        End If
        If plano <> "" AndAlso plano <> "0" Then
            sqlWhere &= " AND nrseqproc = '" & plano & "'"
        End If
        If convenio <> "" AndAlso convenio <> "0" Then
            sqlWhere &= " AND nrseqconvenio = '" & convenio & "'"
        End If
        If procedimento <> "" AndAlso procedimento <> "0" Then
            sqlWhere &= " AND nrseqproc = '" & procedimento & "'"
        End If

        sql = sql & sqlWhere & " ORDER BY nomecliente, data"

        Dim sql2 As String = "SELECT DISTINCT(nomecliente) AS nomecliente FROM " & tabelaAgenda & "" & sqlWhere & " ORDER BY nomecliente"

        Dim dt As Data.DataTable = findSql(sql)

        resumos(sqlWhere, dt.Rows.Count, DateTime.Now.ToString("dd/MM/yyyy hh:MM"), Session("usuario"), receita, ocultar, filtroUtilizado)

        gdvAgrupadoPorCliente.DataSource = getTabelaAgpClientes(dt, findSql(sql2))
        gdvAgrupadoPorCliente.DataBind()

    End Sub

    Private Sub restrito_print_cliente_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim dateInit As String = Request.QueryString("init")
        Dim dateEnd As String = Request.QueryString("end")
        Dim agrupar As String = Request.QueryString("agp")
        Dim receita As String = Request.QueryString("rct")
        Dim plano As String = Request.QueryString("pl")
        Dim ocultar As String = Request.QueryString("oc")
        Dim convenio As String = Request.QueryString("co")
        Dim procedimento As String = Request.QueryString("pr")
        Dim solicitadoPor As String = Request.QueryString("sp")
        Dim exibirProcedimento As String = Request.QueryString("ep")
        Dim filtroUtilizado As String = Request.QueryString("fu")

        If agrupar <> 0 AndAlso agrupar <> "" Then
            buscaAgrupado(dateInit, dateEnd, receita, plano, ocultar, convenio, procedimento, solicitadoPor, exibirProcedimento, filtroUtilizado)
        Else
            buscaNaoAgrupado(dateInit, dateEnd, receita, plano, ocultar, convenio, procedimento, solicitadoPor, exibirProcedimento, filtroUtilizado)
        End If


    End Sub

End Class

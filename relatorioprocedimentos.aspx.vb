﻿Imports System.Data
Imports clsSmart
Partial Class relatorioprocedimentos
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tabelaPlanos As String = " tbplanos"
    Dim tabelaespecialidades As String = "especialidades"
    Dim tabelamedico As String = " tbmedicos"
    Dim tabelamedicoespecialidade As String = " tbmedicos"

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click

        novaImpressao()

    End Sub

    Private Sub carregasolicitadopor()

        Dim cont As Integer

        tb1 = tab1.conectar("select cod, nome from tbmedicos where ativo = true")
        cboSolicitadoPor.Items.Clear()
        cboSolicitadoPor.Items.Add("")

        cont = cboSolicitadoPor.Items.Count
        For x As Integer = 0 To tb1.Rows.Count - 1
            cboSolicitadoPor.Items.Add(New ListItem(tb1.Rows(x)("nome").ToString, tb1.Rows(x)("cod").ToString))
        Next

    End Sub


    Private Sub carregaprocedimento()

        Dim tbplano As New Data.DataTable
        Dim tabplano As New clsBanco

        Dim cont As Integer

        tb1 = tab1.conectar("select nrseq, descricao from tbprocedimentos where ativo = true")
        cboProcedimento.Items.Clear()
        cboProcedimento.Items.Add("")

        cont = cboProcedimento.Items.Count
        For x As Integer = 0 To tb1.Rows.Count - 1
            cboProcedimento.Items.Add(New ListItem(tb1.Rows(x)("descricao").ToString, tb1.Rows(x)("nrseq").ToString))
        Next

    End Sub

    Private Sub carregaplano()

        Dim tbplano As New Data.DataTable
        Dim tabplano As New clsBanco

        Dim cont As Integer

        tbplano = tabplano.conectar("select nrseq, descricao from tbplanos where ativo = true")
        cboPlano.Items.Clear()
        cboPlano.Items.Add("")

        cont = cboPlano.Items.Count
        For x As Integer = 0 To tbplano.Rows.Count - 1
            cboPlano.Items.Add(New ListItem(tbplano.Rows(x)("descricao").ToString, tbplano.Rows(x)("nrseq").ToString))
        Next

    End Sub
    Private Sub carregaconv()

        Dim tbconv As New Data.DataTable
        Dim tabconv As New clsBanco

        Dim cont As Integer

        tbconv = tabconv.conectar("select nrseq, nome from tbconvenios where ativo = true")
        cboConvenio.Items.Clear()
        cboConvenio.Items.Add("")

        cont = cboConvenio.Items.Count
        For x As Integer = 0 To tbconv.Rows.Count - 1
            cboConvenio.Items.Add(New ListItem(tbconv.Rows(x)("nome").ToString, tbconv.Rows(x)("nrseq").ToString))
        Next
    End Sub

    Private Sub novaImpressao()

        Dim dtinicial = ""
        Dim dtfinal = ""
        Dim agrupar As String = "0"
        Dim receita As String = "0"
        Dim ocultar As String = "0"
        Dim plano As String = "0"
        Dim convenio As String = "0"
        Dim procedimento As String = "0"
        Dim solicitadoPor As String = "0"
        Dim exibirproced As String = "1"

        Dim filtrosUtilizados As String = ""

        If txtdtfinal.Text <> "" OrElse txtdtinicial.Text <> "" Then
            If txtdtinicial.Text <> "" Then
                dtinicial = txtdtinicial.Text
                If txtdtfinal.Text <> "" Then
                    dtfinal = txtdtfinal.Text
                Else
                    dtfinal = DateTime.Now.ToString("yyyy/MM/dd")
                End If
                filtrosUtilizados = "Data Inicial, "
            End If
            If txtdtfinal.Text <> "" Then
                dtfinal = txtdtfinal.Text
                If txtdtinicial.Text <> "" Then
                    dtinicial = txtdtinicial.Text
                Else
                    dtinicial = DateTime.Now.ToString("yyyy/MM/dd")
                End If
                filtrosUtilizados &= "Data Final, "
            End If

        End If

        If chkAgruparPorPaciente.Checked Then
            agrupar = "1"
            filtrosUtilizados &= "Agrupamento Por Paciente, "
        End If

        If chkExibirReceita.Checked Then
            receita = "1"
            filtrosUtilizados &= "Exibição de Receita, "
        End If

        If chkOcultarValrCobradoDoPaciente.Checked Then
            ocultar = "1"
            filtrosUtilizados &= "Ocultação do Valor Cobrado do Paciente, "
        End If

        If cboPlano.SelectedValue <> "" Then
            plano = cboPlano.SelectedValue
            filtrosUtilizados &= "Por Plano: " & cboPlano.SelectedItem.Text & ", "
        End If

        If cboConvenio.SelectedValue <> "" Then
            convenio = cboProcedimento.SelectedValue
            filtrosUtilizados &= "Por convênio: " & cboConvenio.SelectedItem.Text & ", "
        End If

        If cboProcedimento.SelectedValue <> "" Then
            procedimento = cboProcedimento.SelectedValue
            filtrosUtilizados &= "Por Procedimento: " & cboProcedimento.SelectedItem.Text & ", "
        End If

        If cboSolicitadoPor.SelectedValue <> "" Then
            solicitadoPor = cboSolicitadoPor.SelectedValue
            filtrosUtilizados &= "Solicitado Por: " & cboSolicitadoPor.SelectedItem.Text & ", "
        End If


        If rblProcedimentos.SelectedValue = 1 Then
            exibirproced = rblProcedimentos.SelectedValue
            filtrosUtilizados &= "Exibindo Procedimentos Baixados e Em Aberto"
        ElseIf rblProcedimentos.SelectedValue = 2 Then
            exibirproced = rblProcedimentos.SelectedValue
            filtrosUtilizados &= "Exibindo Procedimentos Baixados"
        Else
            exibirproced = rblProcedimentos.SelectedValue
            filtrosUtilizados &= "Exibindo Procedimentos Em Aberto"

        End If

        Response.Redirect("/relprocedimentos.aspx?init=" & dtinicial & "&end=" & dtfinal & "&agp=" & agrupar & "&rct=" & receita & "&oc=" & ocultar & "&pl=" & plano & "&co=" & convenio & "&pr=" & procedimento & "&sp=" & solicitadoPor & "ep=" & exibirproced & "&fu=" & filtrosUtilizados)

    End Sub

    Private Sub relatorioprocedimentos_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        carregaconv()
        carregaplano()
        carregasolicitadopor()
        carregaprocedimento()
    End Sub

    'Private Sub impressaOld()
    '    Dim sql As String = "SELECT * FROM tbclientes WHERE liberado = 0 AND (cliente IS NULL OR cliente = '') "

    '    Dim dtinicial = ""
    '    Dim dtfinal = ""
    '    If txtdtfinal.Value <> "" OrElse txtdtinicial.Value <> "" Then
    '        If txtdtinicial.Value <> "" Then
    '            dtinicial = txtdtinicial.Value
    '            If txtdtfinal.Value <> "" Then
    '                dtfinal = txtdtfinal.Value
    '            Else
    '                dtfinal = DateTime.Now.ToString("yyyy/MM/dd")
    '            End If
    '        End If
    '        If txtdtfinal.Value <> "" Then
    '            dtfinal = txtdtfinal.Value
    '            If txtdtinicial.Value <> "" Then
    '                dtinicial = txtdtinicial.Value
    '            Else
    '                dtinicial = DateTime.Now.ToString("yyyy/MM/dd")
    '            End If
    '        End If
    '        sql &= " And dtbaixa BETWEEN " & formatadatamysql(dtinicial) & " And " & formatadatamysql(dtfinal) & " "
    '    End If

    '    If cboPlano.SelectedValue <> "" Then
    '        sql &= " And plano = '" & cboPlano.SelectedValue & "' "
    '    End If



    '    Dim c As String = "ERA NOME EMPRESA"
    '    Dim u As String = Session("usuario")
    '    Dim s As String = sql
    '    Dim r As String = "Sem Endereço"
    '    Dim t As String = "Sem Telefone"


    '    Response.Redirect("/relatorios/procedimentos.aspx?c=" & Server.UrlEncode(c) & "&u=" & Server.UrlEncode(u) & "&s=" & Server.UrlEncode(s) & "&r=" & Server.UrlEncode(r) & "&t=" & Server.UrlEncode(t))
    'End Sub
End Class

﻿Imports System.IO
Imports clsSmart

Partial Class login
    Inherits System.Web.UI.Page

    Private Sub login_Load(sender As Object, e As EventArgs) Handles Me.Load
        '  If IsPostBack Then Exit Sub

        divdadosentrada.Style.Remove("display")
        divemail.Style.Add("display", "none")
        divnatal.Style.Add("display", "none")

        '  divemail01.Style.Add("display", "none")

        If Month(Date.Now.Date) = 12 Then
            divnatal.Style.Remove("display")
        End If

        Dim Files = SearchForFiles(Server.MapPath("~/imagenspublicas/"), {"*.png", "*.jpg", "*.jpeg", "*.gif"})

        For i As Integer = 0 To Files.Count - 1
            Files.Item(i) = Files.Item(i).Replace(Files.Item(i).Substring(0, Files.Item(i).IndexOf("\imagenspublicas") + 1), "")
        Next

        If Files.Count > 1 Then

            For i As Integer = 0 To Files.Count - 1
                Dim divcontrol As HtmlGenericControl = New HtmlGenericControl("div")
                Dim imgcontrol As HtmlGenericControl = New HtmlGenericControl("img")
                slideshow.Controls.Add(divcontrol)
                imgcontrol.Attributes.Add("src", Files.Item(i))
                divcontrol.Controls.Add(imgcontrol)
            Next
        Else
            Dim divcontrol As HtmlGenericControl = New HtmlGenericControl("div")
            Dim imgcontrol As HtmlGenericControl = New HtmlGenericControl("img")
            slideshow.Controls.Add(divcontrol)
            imgcontrol.Attributes.Add("src", "/imagenspublicas/family-01.jpg")
            divcontrol.Controls.Add(imgcontrol)


        End If

        Dim xconfig As New clsconfig
        xconfig.carregar()


        If File.Exists(Server.MapPath("~") & "img\logo.png") Then
            'imlogo.ImageUrl = Server.MapPath("~") & "img\cevanovo.png" Request.Url.OriginalString.Replace(Request.Path, "/social/" & Session("logocliente"))

            Dim xred As New clsredimensiona

            xred.Arquivo = (Server.MapPath("~") & "img\logo.png")

            If xred.redimensionar Then
                '      imlogo.Style.Remove("height")
                '     imlogo.Style.Remove("width")
                '      imlogo.Style.Add("height", xred.Novaaltura & "rem")
                ''      imlogo.Style.Add("width", xred.Novalargura & "rem")
            End If
            '  imlogo.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & Session("logocliente"))

            'Dim img As System.Drawing.Image = System.Drawing.Image.FromFile(Server.MapPath("~") & "social\" & Session("logocliente"))
            'Dim imgaltura As Decimal = img.PhysicalDimension.Height
            'Dim imglargura As Decimal = img.PhysicalDimension.Width
            'Dim fator As Decimal = Int((imgaltura / imglargura) * 16)
            'Dim novaaltura As Decimal = (16 * 100) / imgaltura
            'If (img.Width >= img.Height) Then
            '    fator = (16 / img.Width)
            'Else
            '    fator = (16 / img.Height)
            'End If

            'newWidth = CInt(img.Width * fator)
            'newHeight = CInt(img.Height * fator)

            'logo.Style.Add("height", moeda(FormatNumber(novaaltura, 2)) & "%")
            'logo.Style.Add("width", moeda(FormatNumber(novaaltura, 2)) & "%")

        Else '
            ' imlogo.Visible = False
        End If
    End Sub

    Private Sub btnentrar_Click(sender As Object, e As EventArgs) Handles btnentrar.Click
        Dim login As New clslogin

        login.Usuario = txtuser.Text
        login.Senha = txtpassword.Text


        If Not login.logar Then
            sm("swal({title: 'Atenção!',text: '" & login.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            'sm("executa()", "executa()")
            Exit Sub
        End If

        Response.Redirect(login.Urlretorno)
    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub btnesqueci_Click(sender As Object, e As EventArgs) Handles btnesqueci.Click

        divdadosentrada.Style.Add("display", "none")
        divemail.Style.Remove("display")

        sm("executa()", "executa()")
        '  divemail01.Style.Remove("display")
    End Sub

    Private Sub btnnotenhoemail_Click(sender As Object, e As EventArgs) Handles btnnotenhoemail.Click
        sm("swal({title: 'Atenção!',text: 'Digite seu login no campo e-mail e clique em enviar!',type: 'success',confirmButtonText: 'OK'})", "swal")
    End Sub

    Private Sub btnsend_Click(sender As Object, e As EventArgs) Handles btnsend.Click
        Dim usuario As New clsusuarios("usuario")

        usuario.Email = txtemail.Text

        If Not usuario.EnviarSenha() Then
            sm("swal({title: 'Atenção!',text: '" & usuario.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        sm("swal({title: 'Oba!!!',text: '" & usuario.Mensagemerro & "',type: 'success',confirmButtonText: 'OK'})", "swal")

        divemail.Style.Add("display", "none")
    End Sub
End Class

﻿Imports SelectPdf
Imports System.Data
Imports System.Drawing
Imports System.IO
Imports clsSmart
Imports clsCaixa_new
Imports clssessoes

Partial Class restrito_emitir_procedimentos
    Inherits System.Web.UI.Page
    Dim tabprocedimentos As New clsBanco
    Dim tbprocedimentos As New Data.DataTable

    Dim tabps As New clsBanco
    Dim tbps As New Data.DataTable
    Dim tabelaEmpresa As String = " tbempresas"
    Dim tabelaAgenda As String = " tbagenda"
    Dim tabelaExames As String = " tbagenda_exames"
    Dim tabelaProcedimentos As String = " tbprocedimento_map"
    Dim tabelaConvenios As String = " tbconvenios"
    Dim sql As String
    Dim plano As String
    Dim procedimentos As String

    Private Sub restrito_emitir_procedimentos_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        versessao()
        carregaAgenda(Session("ClienteID"))
        '      carregaAgenda(467)
        Session("empresaemuso") = carregaEmpresa().Rows(0)("nome").ToString
    End Sub

    Private Sub carregaAgenda(nrseq As String)
        Dim sql As String = "SELECT nrseq, serial, DATE_FORMAT(data, '%d/%m/%Y') AS data, hora, REPLACE(CONCAT('R$ ', FORMAT(valortotal, 2)), '.', ',') AS valortotal, IF(emitido = 1, 'Sim', 'Não') As emitido, IF(gerado = 1, 'Sim', 'Não') As gerado FROM " & tabelaAgenda
        sql &= " WHERE emitido = 1 AND ativo = 1 AND nrseqcliente = '" & nrseq & "' "
        gdvAgenda.DataSource = findSql(sql)
        gdvAgenda.DataBind()
    End Sub

    Private Sub carregaExames(nrseq As String)
        Dim sql As String = "SELECT c.nome AS cnome, p.descricao AS pdescricao, REPLACE(CONCAT('R$ ', FORMAT(ae.valorproced, 2)), '.', ',') AS aevalorproced, ae.nrseq AS aenrseq,"
        sql &= " REPLACE(CONCAT('R$ ', FORMAT(ae.valorplano, 2)), '.', ',') AS aevalorplano,  REPLACE(CONCAT('R$ ', FORMAT(ae.valorcliente, 2)), '.', ',') AS aevalorcliente"
        sql &= " FROM " & tabelaExames & " AS ae"
        sql &= " INNER JOIN " & tabelaProcedimentos & " AS p ON ae.nrseqproced = p.nrseq "
        sql &= " INNER JOIN " & tabelaConvenios & " AS c ON ae.nrseqconvenio = c.nrseq "
        sql &= " WHERE ae.ativo = 1 AND ae.nrseqagenda = '" & nrseq & "' "
        gdvExames.DataSource = findSql(sql)
        gdvExames.DataBind()
    End Sub

    Private Sub gdvAgenda_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gdvAgenda.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gdvAgenda.Rows(index)

        If e.CommandName = "editar" Then

            Dim nrseq As String = row.Cells(0).Text

            carregaExames(nrseq)

        End If
    End Sub

    Public Function verificaCampo(input As String, campo As String, erro As String) As String
        If input = "" Then
            If erro <> "" Then
                erro &= ",<br>"
            Else
                erro &= "<br>"
            End If
            erro &= campo
        End If
        Return erro
    End Function


    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function
    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function

    Private Function carregaEmpresa() As Data.DataTable
        Dim nrseqempresa As String = Session("idempresaemuso")
        Dim sql = "SELECT * FROM " & tabelaEmpresa & " WHERE ativo = true AND nrseq = '" & nrseqempresa & "' "
        Return findSql(sql)
    End Function



End Class

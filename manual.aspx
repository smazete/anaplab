﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="manual.aspx.vb" Inherits="manual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="box box-danger " style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); margin-left: 1rem; margin-right: 1rem;">
        <div class="box-header ">
            <b>Manual do Sistema</b>

        </div>
        <div class="box-body ">
            <div class="row">
                <div class="col-lg-4 ">
                    O que deseja fazer?
                    <br />
                     <asp:TextBox runat="server" ID="txtprocurar" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2 ">
                    <br />
                    <asp:LinkButton ID="btnprocurar" runat="server" CssClass="btn btn-warning "><asp:Image runat="server" style="width:2rem;height:2rem;" ImageUrl="~/img/lupa02.png" /> Procurar</asp:LinkButton>
                    </div> 
            </div>
            <div class="row">
                <div class="col-lg-12 ">
                    <asp:TreeView ID="treemanuais" runat="server" NodeWrap="true"  ></asp:TreeView>
                </div>
                </div> 
            <div class="row">
                <div class="col-lg-12 ">
                    <hr style="border-color:cadetblue" />
                    </div> 
                </div> 
            <div class="row">
                <div class="col-lg-12 ">
                    <iframe  runat="server" id="frame01" class="col-lg-12 " style="height:50rem; border:none;"></iframe>
                </div>
            </div>
             <div class="row">
                <div class="col-lg-12 ">
                    <hr style="border-color:cadetblue" />
                </div>
            </div>
            <asp:UpdatePanel ID="updatemanual" runat="server">
                <ContentTemplate >

            
           <div class="row">
               <div class="col-lg-6 ">
                   <div class="text-center">
                       Esse tutorial te auxiliou?
                   </div>
                   
                   <br />
                   <asp:RadioButton ID="optsim" runat="server" Text="Sim, ajudou!" AutoPostBack="true" GroupName ="opcoes01" Checked="true"/>
                   <br />
                   <asp:RadioButton ID="optnao" runat="server" Text="Não, ainda preciso de suporte!" AutoPostBack="true" GroupName ="opcoes01"/>
               </div>
               <div class="col-lg-6 " id="divmensagem" runat="server">
                   Por favor, descreva o problema
                   <br />
                   <asp:TextBox id="txtobs" runat="server" TextMode="MultiLine" style="height:15rem;" CssClass="form-control"></asp:TextBox>
                   <br />
                   <i>Ou entre em contato com nosso suporte: +55 (41) 3385-1270</i><asp:Image runat="server" ImageUrl="~/img/simbolo.jpg" style="height:2rem;width:2rem" />
               </div>
           </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <br />
                    <asp:LinkButton ID="btnenviar" runat="server" CssClass="btn btn-success "><asp:Image runat="server" ImageUrl="~/img/ligado.png" style="height:2rem;width:2rem" /> Enviar </asp:LinkButton>
                </div>
            </div>
                        </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>


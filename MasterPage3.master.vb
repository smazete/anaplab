﻿Imports clsSmart
Imports clssessoesautoatendimento
Imports System.IO

Partial Class MasterPage3
    Inherits System.Web.UI.MasterPage
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Private Sub MasterPage3_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' If IsPostBack Then Exit Sub

        'versessao()
        Dim nomeuser As String
        nomeuser = Session("associado")

        If Session("associado") = "" Then
            Response.Redirect("loginassociados.aspx")
        ElseIf Session("idassociado") = "" Then
            Response.Redirect("loginassociados.aspx")
        ElseIf Session("idmatricula") = "" Then
            Response.Redirect("loginassociados.aspx")
        End If

        If Session("idassociado") = "" Then

            Response.Redirect("loginassociados.aspx")
        End If

        txtnomeuser.Text = nomeuser

    End Sub


    'Private Sub imgperfilmenu_Click(sender As Object, e As ImageClickEventArgs) Handles imgperfilmenu.Click
    '    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibirimagem()", "exibirimagem()", True)
    'End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub linkcadastro_ServerClick(sender As Object, e As EventArgs) Handles linkcadastro.ServerClick
        Response.Redirect("meucadastro.aspx")
    End Sub

    Private Sub linkcaixapostal_ServerClick(sender As Object, e As EventArgs) Handles linkcaixapostal.ServerClick
        Response.Redirect("caixapostal.aspx")
    End Sub

    Private Sub linkmeusdocumentos_ServerClick(sender As Object, e As EventArgs) Handles linkmeusdocumentos.ServerClick
        Response.Redirect("meusdocumentos.aspx")
    End Sub


    Private Sub linkminhasmensalidadeoujoias_ServerClick(sender As Object, e As EventArgs) Handles linkminhasmensalidadeoujoias.ServerClick
        Response.Redirect("mensalidadeoujoias.aspx")
    End Sub

    Private Sub linkminhasacoes_ServerClick(sender As Object, e As EventArgs) Handles linkminhasacoes.ServerClick
        Response.Redirect("minhasacoes.aspx")
    End Sub

End Class


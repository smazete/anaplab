﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="associadodocumento.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="associadodocumento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Arquivos Associados </b>
        </div>
        <div class="box-body">



            <div class="row" runat="server" id="divprivado" visible="true">
                <div class="row">
                    <div class="col-lg-4">
                        <asp:Label Text="Buscar Por" runat="server" />
                        <asp:DropDownList runat="server" CssClass="form-control" ID="cbobusca">
                            <asp:ListItem Text="Codigo" Value="nrseq" />
                            <asp:ListItem Text="Nome" Value="nome" />
                            <asp:ListItem Text="Matricula" Value="matricula" />
                            <asp:ListItem Text="CPF" Value="cpf" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-lg-6">
                        <br />
                        <asp:TextBox runat="server" ID="txtbusca" CssClass="form-control" />
                    </div>

                    <div class="col-lg-2 text-center">
                        <br>
                        <asp:LinkButton ID="btnbuscando" runat="server" CssClass="btn btn-primary "> <i class="fa fa-searc"></i> Buscar</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="row">
                <asp:GridView ID="gradeassociados" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdmatricula" runat="server" Value='<%#Bind("matricula")%>'></asp:HiddenField>
                                <asp:LinkButton ID="usar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="usar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="nrseq" HeaderText="Código" />
                        <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                        <asp:BoundField DataField="nome" HeaderText="Nome" />
                        <asp:BoundField DataField="cpf" HeaderText="CPF" />
                    </Columns>
                </asp:GridView>
            </div>

            <div class="row" id="divassociado" runat="server">
                <asp:HiddenField runat="server" ID="hddadosuser" />
                <div class="col-md-2">
                    Código
                        <asp:Label ID="lblcodigo" Text="0" runat="server" CssClass="form-control" />
                </div>
                <div class="col-md-2">
                    Matricula
                        <asp:Label ID="lblmatricula" Text="0" runat="server" CssClass="form-control" />
                </div>
                <div class="col-md-4">
                    Nome
                        <asp:Label ID="lblnome" Text="nome sobrenome" runat="server" CssClass="form-control" />
                </div>
                <div class="col-md-3">
                    CPF
                        <asp:Label ID="lblcpf" Text="000.000.000-00" runat="server" CssClass="form-control" />
                </div>
            </div>


            <div class="row">

                <div class="col-lg-4">
                    Descricão
                    <br>
                    <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
<%--                <div class="col-lg-2">
                    Arquivo
                    <br>
                    <asp:TextBox ID="txtarquivo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>--%>
                <div class="col-lg-1">
                    Ativo
                    <br>
                    <asp:CheckBox ID="chkativo" runat="server" Text="ATIVO" Checked="true" Enabled="false"></asp:CheckBox>
                </div>
                <div class="col-lg-3 ">
                    <br />
                    <asp:FileUpload ID="upload01" runat="server" CssClass="btn btn-default" />
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 text-center">
                    <br>
                    <asp:LinkButton ID="btnupload" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i>Salvar</asp:LinkButton>
                </div>
            </div>
            <div class="row">
                </hr>
            </div>
            <div class="row" id="divgrade" runat="server">
                <br>
                <asp:GridView ID="gradedoc" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum documento cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdarquivo" runat="server" Value='<%#Bind("arquivo")%>'></asp:HiddenField>
                                <asp:LinkButton ID="baixar" runat="server" CssClass="btn btn-info btn-xs" CommandName="baixar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-download" ></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="nrseq" HeaderText="Código" />
                        <asp:BoundField DataField="nome" HeaderText="Nome" />
                    <asp:TemplateField>
                            <ItemTemplate>
                                <div runat="server" id="divmostrabtn">
                                    <asp:LinkButton ID="excluir" runat="server" CssClass="btn btn-danger btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                <i class="fa fa-trash-o"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
</asp:Content>

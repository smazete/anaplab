﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="chapaseleicoes.aspx.vb" Inherits="chapaseleicoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
                <div class="box box-header">
                    <b>Cadastro de Chapas</b>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <asp:LinkButton ID="btnnova" runat="server" CssClass="btn btn-warning "> <i class="fas fa-plus-circle"></i> Nova</asp:LinkButton>
                            <asp:HiddenField runat="server" ID="hdnrseq" />
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            Número
                    <br>
                            <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-10">
                            Nome
                    <br>
                            <asp:TextBox ID="txtdescricao" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <br>
                            <asp:LinkButton ID="btnsalvar" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i> Salvar</asp:LinkButton>
                        </div>
                    </div>
                    <div class="row">
                        <hr />
                    </div>        
                    <div class="row">
                        <asp:UpdatePanel ID="updatepanel" runat="server">
                            <ContentTemplate>                              
                                <div class="col-lg-6" id="divbusca" runat="server">
                                    <asp:Label Text="Buscar por nome" runat="server" ID="lblbusca"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtbusca" CssClass="form-control" />
                                </div>
                                <div class="col-lg-2">
                                    <br>
                                    <asp:LinkButton ID="btnbuscar" runat="server" CssClass="btn btn-primary "> <i class="fa fa-searc"></i> Buscar </asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="row">
                        <br>
                        <asp:GridView ID="gradeacoes" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma doca cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="Opções">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                        <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'><i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                        <asp:LinkButton ID="excluir" runat="server" CssClass="btn btn-danger btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'><i class="fa fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="numero" HeaderText="Número" />
                                <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

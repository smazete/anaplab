﻿Imports System.IO
Imports clsSmart

Partial Class imprimirRelPad
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Private Sub imprimirRelPad_Load(sender As Object, e As EventArgs) Handles Me.Load
        If File.Exists(Server.MapPath("~") & "img\Cevanovo.png") Then

            Dim xred As New clsredimensiona
            xred.Arquivo = Server.MapPath("~") & "img\Cevanovo.png"
            xred.Max = 9
            If xred.redimensionar Then
                imlogo.Style.Remove("Height")
                imlogo.Style.Remove("Width")
                imlogo.Style.Add("Height", xred.Novaaltura & "rem")
                imlogo.Style.Add("Width", xred.Novalargura & "rem")
            End If

        End If

        lblemissao.Text = data()
        lblusuario.Text = clssessoes.buscarsessoes("usuario")
        Dim xsql As String = IIf(mLeft(clssessoes.buscarsessoes("sql"), 1) = ",", clssessoes.buscarsessoes("sql").Substring(1), clssessoes.buscarsessoes("sql"))
        tb1 = tab1.conectar(xsql)

        grade.DataSource = tb1
        grade.DataBind()


    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        'Dim lblitem As Label = e.Row.FindControl("lblitem")
        'lblitem.Text = e.Row.RowIndex + 1
        Dim dtadmissao As Label = e.Row.FindControl("dtadmissao")

        dtadmissao.Text = e.Row.DataItem("dtadmissao")

        If e.Row.Cells(5).Text > 0 Then
            e.Row.Cells(5).Text = "Sim"
        Else
            e.Row.Cells(5).Text = "Não"
        End If
        If e.Row.Cells(6).Text > 0 Then
            e.Row.Cells(6).Text = "Sim"
        Else
            e.Row.Cells(6).Text = "Não"
        End If
        If e.Row.Cells(7).Text > 0 Then
            e.Row.Cells(7).Text = "Sim"
        Else
            e.Row.Cells(7).Text = "Não"
        End If
    End Sub
End Class

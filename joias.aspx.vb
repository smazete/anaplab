﻿
Imports Microsoft.VisualBasic
Imports System.Data
Imports clssessoes
Imports clsSmart


Partial Class autoatendimento_joias
    Inherits System.Web.UI.Page

    'tipo de busca é a maneira que foi efetuada a origem da busca assim podendo reutilizar o selctanterior

    ' tipo = 0 nao exise tipo busca 
    'tipo = 1 busca feita pelo botao usar
    '
    '
    '


    Private Sub autoatendimento_joias_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If
        carregacbotipojoias()
        carregacbojoiastipo()
        divassociado.Visible = False
        divgerarjoias.Visible = False
    End Sub

    Public Sub carregaassociados(Optional exibirinativos As Boolean = False)
        'Dim xempresas As New clsEmpresas
        Dim xAssociados As New clsAssociado
        Dim consulta As String

        consulta = ""

        If txtbusca.Text = "" Then
            consulta = ""
            sm("swal({title: 'Error!',text: 'Essa Busca pode demorar',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        Else
            consulta = "where " & cbobusca.SelectedValue & " like '%" & txtbusca.Text & "%'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        tb1 = tab1.conectar("select * from tbAssociados " & consulta & "")


        gradeassociados.DataSource = tb1
        gradeassociados.DataBind()

    End Sub

    Private Sub carregacbotipojoias()
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Dim xTJoias As New clstipojoias
        Dim contador As String
        contador = tb1.Rows.Count

        tb1 = tab1.conectar("select * from tbtipojoias where ativo = '1' ")

        For x As Integer = 0 To tb1.Rows.Count - 1
            cbotipojoias.Items.Add(tb1.Rows(x)("descricao").ToString)
        Next

    End Sub
    Private Sub carregacbojoiastipo()
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Dim xTJoias As New clstipojoias
        Dim contador As String
        contador = tb1.Rows.Count

        tb1 = tab1.conectar("select * from tbtipojoias where ativo = '1' ")

        For x As Integer = 0 To tb1.Rows.Count - 1
            cbojoiastipo.Items.Add(tb1.Rows(x)("descricao").ToString)
        Next

    End Sub

    Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click

        '   gradeassociados.Visible = True
        divassociado.Visible = False
        hdnruser.Value = ""
        lblcodigo.Text = "0"
        lblmatricula.Text = "0"
        lblnome.Text = ""
        lblcpf.Text = "0"
        tipodabusca.Value = "1"
        '    carregaassociados()

        Dim xAssociados As New clsAssociado

        xAssociados.Nrseq = txtbusca.Text


        If Not xAssociados.procurar() Then
            sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        gradeassociados.Visible = False
        divassociado.Visible = True
        divgerarjoias.Visible = False

        hdnruser.Value = xAssociados.Nrseq
        lblcodigo.Text = xAssociados.Nrseq
        lblmatricula.Text = xAssociados.Matricula
        lblnome.Text = xAssociados.Nome
        lblcpf.Text = xAssociados.Cpf

        carregajoias()

    End Sub


    Private Sub gradeassociados_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeassociados.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeassociados.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "usar" Then

            Dim xAssociados As New clsAssociado

            xAssociados.Nrseq = nrseq.Value


            If Not xAssociados.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            gradeassociados.Visible = False
            divassociado.Visible = True
            divgerarjoias.Visible = True

            hdnruser.Value = xAssociados.Nrseq
            lblcodigo.Text = xAssociados.Nrseq
            lblmatricula.Text = xAssociados.Matricula
            lblnome.Text = xAssociados.Nome
            lblcpf.Text = xAssociados.Cpf
            carregajoias()

        End If
    End Sub


    Public Sub carregajoias(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        If tipodabusca.Value = "1" Then

            'If Not xempresas.carregarempresa() Then
            '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            '    Exit Sub
            'End If

            tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.nrsequser = " & hdnruser.Value & "")

            'If Not xempresas.Contador > 0 Then
            '    Exit Sub
            'End If

            'xplanos.Cliente = xempresas.Nome

            'If Not xAssociados.carregatodos() Then
            '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            '    Exit Sub
            'End If

        ElseIf tipodabusca.Value = "2" Then
            Dim sqlb As String = " tbmensalidade_joias.statusfiliacao = '1' "
            If txtbusca.Text <> "" Then
                sqlb = sqlb + " and " + cbobusca.Text = " = '" + txtbusca.Text + "'"
            End If
            If cboano.Text <> "" Then
                sqlb = sqlb + "and tbmensalidade_joias.ano = '" + cboano.SelectedValue + "'"
            End If
            If cbomes.Text <> "" Then
                sqlb = sqlb + "and tbmensalidade_joias.mes  = '" + cbomes.SelectedValue + "'"
            End If

            If cbostatus.Text <> "" Then
                sqlb = sqlb + "and tbmensalidade_joias.statuspg  = '" + cbostatus.SelectedValue + "'"
            End If

            If cbotipojoias.Text <> "" Then
                sqlb = sqlb + "and tbmensalidade_joias.descricaotipojoia ano = '" + cbotipojoias.Text + "'"
            End If


            tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where " & sqlb & "")



        End If

        gradejoias.DataSource = tb1
        gradejoias.DataBind()

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub


    Private Sub gradejoias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradejoias.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(4).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightSalmon
        ElseIf e.Row.Cells(4).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If

        If e.Row.Cells(5).Text = "1" Then
            e.Row.Cells(5).Text = "Janeiro"
        ElseIf e.Row.Cells(5).Text = "2" Then
            e.Row.Cells(5).Text = "Fevereiro"
        ElseIf e.Row.Cells(5).Text = "3" Then
            e.Row.Cells(5).Text = "Março"
        ElseIf e.Row.Cells(5).Text = "4" Then
            e.Row.Cells(5).Text = "Abril"
        ElseIf e.Row.Cells(5).Text = "5" Then
            e.Row.Cells(5).Text = "Maio"
        ElseIf e.Row.Cells(5).Text = "6" Then
            e.Row.Cells(5).Text = "Junho"
        ElseIf e.Row.Cells(5).Text = "7" Then
            e.Row.Cells(5).Text = "Julho"
        ElseIf e.Row.Cells(5).Text = "8" Then
            e.Row.Cells(5).Text = "Agosto"
        ElseIf e.Row.Cells(5).Text = "9" Then
            e.Row.Cells(5).Text = "Setembro"
        ElseIf e.Row.Cells(5).Text = "10" Then
            e.Row.Cells(5).Text = "Outubro"
        ElseIf e.Row.Cells(5).Text = "11" Then
            e.Row.Cells(5).Text = "Novembro"
        ElseIf e.Row.Cells(5).Text = "12" Then
            e.Row.Cells(5).Text = "Dezembro"
        End If

        If e.Row.Cells(4).Text = "0" Then
            e.Row.Cells(4).Text = "Aberto"
        ElseIf e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Pago"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Estornado"
        Else
            e.Row.Cells(4).Text = "Aberto"
        End If

        If e.Row.Cells(2).Text = "0" Then
            e.Row.Cells(2).Text = "Excluido"
            e.Row.Cells(4).Text = "Excluido"
        ElseIf e.Row.Cells(2).Text = "3" Then
            e.Row.Cells(2).Text = "Ativo"
        End If

        If e.Row.Cells(2).Text = "Excluido" Then
            '  e.Row.Cells(7).Text = "Ativar"
            div.Visible = False
            texto.Visible = True
        Else
            div.Visible = True
            texto.Visible = False
        End If


        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If



    End Sub

    Private Sub gradejoias_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradejoias.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradejoias.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim nrsequser As HiddenField = row.FindControl("hdnrsequser")

        If e.CommandName = "excluir" Then

            Dim xJoias As New clsmJoias

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value


            If Not xJoias.excluir() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel excluir a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias()

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")

        End If

        If e.CommandName = "estornar" Then

            Dim xJoias As New clsmJoias

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value


            If Not xJoias.estorno() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias()


            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")
        End If

        If e.CommandName = "aberto" Then

            Dim xJoias As New clsmJoias

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value

            If Not xJoias.aberto() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If


            carregajoias()

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")

        End If

        If e.CommandName = "pagar" Then

            Dim xJoias As New clsmJoias

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value

            If Not xJoias.baixar() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel dar baixa a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias()

        End If

    End Sub

    Private Sub btntodos_Click(sender As Object, e As EventArgs) Handles btntodos.Click

        Dim xJoias As New clsmJoias

        xJoias.Nrsequser = hdnruser.Value

        If Not xJoias.excluirtodas() Then

            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel Excluir as joias',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        carregajoias()



    End Sub

    Private Sub btnbaixaselect_Click(sender As Object, e As EventArgs) Handles btnbaixaselect.Click
        For Each row As GridViewRow In gradejoias.Rows

            Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
            Dim chkok As CheckBox = row.FindControl("chkok")

            If chkok.Checked = "true" Then
                Dim xMensalidade As New clsmJoias

                Dim xJoias As New clsmJoias

                xJoias.Nrseq = nrseq.Value

                If Not xJoias.baixar() Then
                    sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia',  showConfirmButton: false,  timer: 2000})")
                    Exit Sub
                End If
                carregajoias()

                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Efetuada a baixa das joias',  showConfirmButton: false,  timer: 2000})")


            End If
        Next
    End Sub


    Private Sub btnprocrar_Click(sender As Object, e As EventArgs) Handles btnprocrar.Click
        tipodabusca.Value = "2"
        carregajoias()
    End Sub
End Class

﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports System.Data

Public Class clsconfig_atrasos
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _status As String
    Dim _nrseq As Integer = 0
    Dim _hrinicial As Integer = 0
    Dim _hrvalidar As Decimal = 0
    Dim _dtvalidar As Date
    Dim _hrfinal As Integer = 0
    Dim _mensagem As String = ""
    Dim _ativo As Boolean = False
    Dim _permitecontinuar As Boolean = False
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim tblista As New Data.DataTable
    Public Sub New()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub
    Public Sub New(xuser As String)
        _usercad = xuser
        _dtcad = data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Hrinicial As Integer
        Get
            Return _hrinicial
        End Get
        Set(value As Integer)
            _hrinicial = value
        End Set
    End Property

    Public Property Hrfinal As Integer
        Get
            Return _hrfinal
        End Get
        Set(value As Integer)
            _hrfinal = value
        End Set
    End Property

    Public Property Mensagem As String
        Get
            Return _mensagem
        End Get
        Set(value As String)
            _mensagem = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Permitecontinuar As Boolean
        Get
            Return _permitecontinuar
        End Get
        Set(value As Boolean)
            _permitecontinuar = value
        End Set
    End Property

    Public Property Tblista1 As DataTable
        Get
            Return tblista
        End Get
        Set(value As DataTable)
            tblista = value
        End Set
    End Property

    Public Property Hrvalidar As Decimal
        Get
            Return _hrvalidar
        End Get
        Set(value As Decimal)
            _hrvalidar = value
        End Set
    End Property

    Public Property Status As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property

    Public Property Dtvalidar As Date
        Get
            Return _dtvalidar
        End Get
        Set(value As Date)
            _dtvalidar = value
        End Set
    End Property
End Class

Partial Public Class clsconfig_atrasos

    Public Function procurar() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbconfig_atrasos where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Hrinicial = tb1.Rows(0)("hrinicial").ToString
        Hrfinal = tb1.Rows(0)("hrfinal").ToString
        Mensagem = tb1.Rows(0)("mensagem").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        Usercad = tb1.Rows(0)("usercad").ToString

        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function validahora() As Boolean
        If _dtvalidar < data() Then
            _status = "Data Inválida "
            _mensagem = "Essa entrada não é do dia programado"
            _permitecontinuar = False
            Return True
        End If
        If _dtvalidar > data() Then
            _status = "No prazo"
            _mensagem = ""
            _permitecontinuar = True
            Return True
        End If
        If _hrvalidar < 1 Then
            _status = "No prazo"
            _mensagem = ""
            _permitecontinuar = True
            Return True
        End If

        ' tb1 = tab1.conectar("select * from tbconfig_atrasos where  " & _hrvalidar & " >= hrinicial and " & _hrvalidar & " <= hrfinal and ativo = true ")
        tb1 = tab1.conectar("select * from tbconfig_atrasos where  ativo = true order by hrinicial")
        If tb1.Rows.Count <> 0 Then
            For x As Integer = 0 To tb1.Rows.Count - 1
                If tb1.Rows(x)("hrinicial").ToString >= _hrvalidar AndAlso _hrvalidar <= tb1.Rows(x)("hrfinal").ToString Then
                    _status = tb1.Rows(x)("status").ToString
                    _mensagem = tb1.Rows(x)("mensagem").ToString
                    _permitecontinuar = tb1.Rows(x)("permitecontinuar").ToString
                    Exit For
                End If
            Next
            '_status = "Atrasado"


            Return True
        Else
            _status = "No prazo"
            _permitecontinuar = True
            _mensagem = ""
        End If


        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = "select * from tbconfig_atrasos where hrinicial = " & _hrinicial & " and ativo = true"

            tb1 = tab1.conectar(xsql)
            If tb1.Rows.Count = 0 Then
                tb1 = tab1.IncluirAlterarDados("insert into tbconfig_atrasos (ativo, usercad, dtcad, hrinicial, hrfinal, mensagem, permitecontinuar) values (true, '" & _usercad & "', '" & hoje() & "', " & Hrinicial & "," & Hrfinal & ", '" & Mensagem & "'," & logico(Permitecontinuar) & ")")
            Else
                xsql = " update tbconfig_atrasos set ativo = True, permitecontinuar = " & _permitecontinuar

                If Hrinicial <> 0 Then
                    xsql &= ",hrinicial = " & moeda(Hrinicial)
                End If
                If Hrfinal <> 0 Then
                    xsql &= ",hrfinal = " & moeda(Hrfinal)
                End If
                If Mensagem <> "" Then
                    xsql &= ",mensagem = '" & tratatexto(Mensagem) & "'"
                End If


                xsql &= " where nrseq = " & tb1.Rows(0)("nrseq").ToString
                tb1 = tab1.IncluirAlterarDados(xsql)
            End If




            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole("")
            tb1 = tab1.IncluirAlterarDados("insert into tbconfig_atrasos (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbconfig_atrasos where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
    Public Function retornarlista() As Boolean
        Try

            tblista = tab1.conectar("select * from tbconfig_atrasos where ativo = true order by hrinicial")

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbconfig_atrasos set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class



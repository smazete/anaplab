﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart

Public Class clsProcedimentos
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String

    Dim _table As Data.DataTable
    Dim tablename As String = "tbprocedimentos"
    Dim _contador As Integer = 0
    Dim _nrseq As Integer
    Dim _cod As String
    Dim _descricao As String
    Dim _valorfinal As Decimal
    Dim _plano As String = ""
    Dim _valorcusto As Decimal = 0
    Dim _convenio As Integer = 0
    Dim _dtcad As Date
    Dim _usercad As String = ""
    Dim _dtaltera As Date
    Dim _useraltera As String = ""
    Dim _ativo As Boolean = True
    Dim _dtexclui As Date
    Dim _userexclui As String = ""
    Dim _codigoamb As String = ""
    Dim _codigosus As String = ""
    Dim _naoincluircaixa As Boolean = False
    Dim _percobplano As Decimal = 0
    Dim _valorcobplano As Decimal = 0
    Dim _nrseqctrl As String = ""
    Dim _valorpaciente As Decimal = 0
    Dim _nrseqempresa As Integer = 0
    Dim _descricaoconvenio As String = ""
    Dim _perccobpaciente As Decimal = 0
    Dim _listaprocedimentos As New List(Of clsProcedimentos)
    Dim tbprocedimentos As New Data.DataTable
    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Cod As String
        Get
            Return _cod
        End Get
        Set(value As String)
            _cod = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Valorfinal As Decimal
        Get
            Return _valorfinal
        End Get
        Set(value As Decimal)
            _valorfinal = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Valorcusto As Decimal
        Get
            Return _valorcusto
        End Get
        Set(value As Decimal)
            _valorcusto = value
        End Set
    End Property

    Public Property Convenio As Integer
        Get
            Return _convenio
        End Get
        Set(value As Integer)
            _convenio = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtaltera As Date
        Get
            Return _dtaltera
        End Get
        Set(value As Date)
            _dtaltera = value
        End Set
    End Property

    Public Property Useraltera As String
        Get
            Return _useraltera
        End Get
        Set(value As String)
            _useraltera = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Codigoamb As String
        Get
            Return _codigoamb
        End Get
        Set(value As String)
            _codigoamb = value
        End Set
    End Property

    Public Property Codigosus As String
        Get
            Return _codigosus
        End Get
        Set(value As String)
            _codigosus = value
        End Set
    End Property

    Public Property Naoincluircaixa As Boolean
        Get
            Return _naoincluircaixa
        End Get
        Set(value As Boolean)
            _naoincluircaixa = value
        End Set
    End Property

    Public Property Percobplano As Decimal
        Get
            Return _percobplano
        End Get
        Set(value As Decimal)
            _percobplano = value
        End Set
    End Property

    Public Property Valorcobplano As Decimal
        Get
            Return _valorcobplano
        End Get
        Set(value As Decimal)
            _valorcobplano = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Valorpaciente As Decimal
        Get
            Return _valorpaciente
        End Get
        Set(value As Decimal)
            _valorpaciente = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Descricaoconvenio As String
        Get
            Return _descricaoconvenio
        End Get
        Set(value As String)
            _descricaoconvenio = value
            If value <> "" Then
                tb1 = tab1.IncluirAlterarDados("select * from tbconvenios where ativo = true and nome = '" & Descricaoconvenio & "'")
                If tb1.Rows.Count > 0 Then
                    _convenio = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Perccobpaciente As Decimal
        Get
            Return _perccobpaciente
        End Get
        Set(value As Decimal)
            _perccobpaciente = value
        End Set
    End Property

    Public Property Listaprocedimentos As List(Of clsProcedimentos)
        Get
            Return _listaprocedimentos
        End Get
        Set(value As List(Of clsProcedimentos))
            _listaprocedimentos = value
        End Set
    End Property

    Public Property Tbprocedimentos1 As DataTable
        Get
            Return tbprocedimentos
        End Get
        Set(value As DataTable)
            tbprocedimentos = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class

Partial Public Class clsProcedimentos

    Public Function procurar(Optional ativo As Boolean = True) As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbprocedimentos where nrseqempresa='" & _nrseqempresa & "' and  nrseq = '" & Nrseq & "'" & IIf(ativo = True, " and ativo=true", " and ativo=false"))
        If tb1.Rows.Count > 0 Then
            _nrseq = tb1.Rows(0)("nrseq").ToString
            _cod = tb1.Rows(0)("cod").ToString
            _descricao = tb1.Rows(0)("descricao").ToString
            _valorfinal = moeda(tb1.Rows(0)("valorfinal").ToString, True)
            _plano = tb1.Rows(0)("plano").ToString
            _valorcusto = moeda(tb1.Rows(0)("valorcusto").ToString, True)
            _convenio = numeros(tb1.Rows(0)("convenio").ToString)
            _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
            _usercad = tb1.Rows(0)("usercad").ToString
            _dtaltera = valordata(tb1.Rows(0)("dtaltera").ToString)
            _useraltera = tb1.Rows(0)("useraltera").ToString
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _dtexclui = valordata(tb1.Rows(0)("dtexclui").ToString)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            _codigoamb = tb1.Rows(0)("codigoamb").ToString
            _codigosus = tb1.Rows(0)("codigosus").ToString
            _naoincluircaixa = logico(tb1.Rows(0)("naoincluircaixa").ToString)
            _percobplano = moeda(tb1.Rows(0)("percobplano").ToString, True)
            _valorcobplano = moeda(tb1.Rows(0)("valorcobplano").ToString, True)
            _valorpaciente = moeda(tb1.Rows(0)("valorpaciente").ToString, True)
            _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
        Else
            _mensagemerro = "Não existe nenhum procedimento para essa consulta"
            Return False
        End If
        _table = tb1
        _contador = tb1.Rows.Count
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function medicosprocedimentos() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbprocedimentos where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _cod = tb1.Rows(0)("cod").ToString
        _descricao = tb1.Rows(0)("descricao").ToString
        _valorfinal = moeda(tb1.Rows(0)("valorfinal").ToString)
        _plano = tb1.Rows(0)("plano").ToString
        _valorcusto = moeda(tb1.Rows(0)("valorcusto").ToString)
        _convenio = numeros(tb1.Rows(0)("convenio").ToString)
        _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
        _usercad = tb1.Rows(0)("usercad").ToString
        _dtaltera = valordata(tb1.Rows(0)("dtaltera").ToString)
        _useraltera = tb1.Rows(0)("useraltera").ToString
        _ativo = logico(tb1.Rows(0)("ativo").ToString)
        _dtexclui = valordata(tb1.Rows(0)("dtexclui").ToString)
        _userexclui = tb1.Rows(0)("userexclui").ToString
        _codigoamb = tb1.Rows(0)("codigoamb").ToString
        _codigosus = tb1.Rows(0)("codigosus").ToString
        _naoincluircaixa = logico(tb1.Rows(0)("naoincluircaixa").ToString)
        _percobplano = moeda(tb1.Rows(0)("percobplano").ToString)
        _valorcobplano = moeda(tb1.Rows(0)("valorcobplano").ToString)
        _valorpaciente = moeda(tb1.Rows(0)("valorpaciente").ToString)
        _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbprocedimentos Set ativo = True, Naoincluircaixa = " & logico(Naoincluircaixa)

            If _cod <> "" Then
                xsql &= ",cod = '" & tratatexto(_cod) & "'"
            End If
            If _descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(_descricao) & "'"
            End If
            If _valorfinal <> 0 Then
                xsql &= ",valorfinal = " & moeda(_valorfinal)
            End If
            If _plano <> "" Then
                xsql &= ",plano = '" & tratatexto(_plano) & "'"
            End If
            If _valorcusto <> 0 Then
                xsql &= ",valorcusto = " & moeda(_valorcusto)
            End If
            If _perccobpaciente <> 0 Then
                xsql &= ",perccobpaciente = " & moeda(_perccobpaciente)
            End If
            If _convenio <> 0 Then
                xsql &= ",convenio = " & numeros(_convenio)
            End If

            If _codigoamb <> "" Then
                xsql &= ",codigoamb = '" & tratatexto(_codigoamb) & "'"
            End If

            If _codigosus <> "" Then
                xsql &= ",codigosus = '" & tratatexto(_codigosus) & "'"
            End If

            If _percobplano <> 0 Then
                xsql &= ",percobplano = " & moeda(_percobplano, False) & ""
            End If
            If _valorcobplano <> 0 Then
                xsql &= ",valorcobplano = " & moeda(_valorcobplano, False)
            End If
            If _valorpaciente <> 0 Then
                xsql &= ",valorpaciente = " & moeda(_valorpaciente, False)
            End If
            If _nrseqempresa <> 0 Then
                xsql &= ",nrseqempresa = " & numeros(_nrseqempresa, False)
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            _mensagemerro = "Procedimento salvo com sucesso !"
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function carregarprocedimentos(Optional sqltext As String = "") As Boolean

        Try
            Dim xsql As String = "select * from vwprocedimentos where nrseqempresa='" & _nrseqempresa & "'"
            If sqltext <> "" Then
                xsql = sqltext
            End If
            xsql &= " order by descricao"
            tbprocedimentos = tab1.conectar(xsql)
            _table = tbprocedimentos
            _contador = tbprocedimentos.Rows.Count
            Return True
        Catch ex As Exception
            Return False
        End Try



    End Function
    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbprocedimentos (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbprocedimentos where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbprocedimentos set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports clsSmart

Public Class clsbalancete
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim _table As Data.DataTable
    Dim _contador As Integer
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _nome As String
    Dim _arquivo As String
    Dim _ativo As Boolean
    Dim _dtexcluir As Date
    Dim _userexcluir As String
    Dim _obs As String
    Dim _tipo As String
    Dim _listabalanco As New List(Of clsbalancete)

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexcluir As Date
        Get
            Return _dtexcluir
        End Get
        Set(value As Date)
            _dtexcluir = value
        End Set
    End Property

    Public Property Userexcluir As String
        Get
            Return _userexcluir
        End Get
        Set(value As String)
            _userexcluir = value
        End Set
    End Property

    Public Property Obs As String
        Get
            Return _obs
        End Get
        Set(value As String)
            _obs = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property

    Public Property Listabalanco As List(Of clsbalancete)
        Get
            Return _listabalanco
        End Get
        Set(value As List(Of clsbalancete))
            _listabalanco = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class

Partial Public Class clsbalancete

    Public Function procurar() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbdocumentoanaplab where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        Usercad = tb1.Rows(0)("usercad").ToString
        Nome = tb1.Rows(0)("nome").ToString
        Arquivo = tb1.Rows(0)("arquivo").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Dtexcluir = FormatDateTime(tb1.Rows(0)("dtexcluir").ToString, DateFormat.ShortDate)
        Userexcluir = tb1.Rows(0)("userexcluir").ToString
        Obs = tb1.Rows(0)("obs").ToString
        Tipo = tb1.Rows(0)("tipo").ToString
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function carregabalanco() As Boolean


        Try
            _listabalanco.Clear()
            tb1 = tab1.conectar("select * from tbdocumentoanaplab where  ativo = true ")
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If

            For x As Integer = 0 To tbx.Rows.Count - 1
                _listabalanco.Add(New clsbalancete With {.Arquivo = tbx.Rows(x)("arquivo").ToString, .Nrseq = tbx.Rows(x)("nrseq").ToString, .Nome = tbx.Rows(x)("nome").ToString})
            Next


            _table = tbx
            Contador = tbx.Rows.Count

            Return True

        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function





    Public Function salvar() As Boolean
        Try

            Dim xsql As String
            xsql = " update tbdocumentoanaplab Set ativo = True"

            If Nome <> "" Then
                xsql &= ",nome = '" & tratatexto(Nome) & "'"
            End If
            If Arquivo <> "" Then
                xsql &= ",arquivo = '" & tratatexto(Arquivo) & "'"
            End If
            If Ativo <> 0 Then
                xsql &= ",ativo = " & moeda(Ativo)
            End If
            If Dtexcluir <> "" Then
                xsql &= ",dtexcluir = '" & formatadatamysql(Dtexcluir) & "'"
            End If
            If Userexcluir <> "" Then
                xsql &= ",userexcluir = '" & tratatexto(Userexcluir) & "'"
            End If
            If Obs <> "" Then
                xsql &= ",obs = '" & tratatexto(Obs) & "'"
            End If
            If Tipo <> "" Then
                xsql &= ",tipo = '" & tratatexto(Tipo) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function novosalvar() As Boolean
        Try

            novo()

            Dim xsql As String

            xsql = " update tbdocumentoanaplab Set ativo = True"

            If Nome <> "" Then
                xsql &= ",nome = '" & tratatexto(Nome) & "'"
            End If
            If Arquivo <> "" Then
                xsql &= ",arquivo = '" & tratatexto(Arquivo) & "'"
            End If
            If Obs <> "" Then
                xsql &= ",obs = '" & tratatexto(Obs) & "'"
            End If
            If Tipo <> "" Then
                xsql &= ",tipo = '" & tratatexto(Tipo) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbdocumentoanaplab (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbdocumentoanaplab where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbdocumentoanaplab set ativo = false, dtexcluir = '" & hoje() & "', userexcluir = '" & Usercad & "'  where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsjoia2


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    '  Dim _listaclasse As New List(Of clsjoia2)
    Dim _nrseq As Integer
    Dim _nrsequser As Integer
    Dim _mes As Integer
    Dim _ano As Integer
    Dim _statuspg As Integer
    Dim _frmpg As Integer
    Dim _dtpg As Date
    Dim _statusfiliacao As Integer
    Dim _ativo As Boolean
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _nrseqtipojoia As Integer
    Dim _parcelas As Integer
    Dim _descricaotipojoia As String

    Public Property Tb11 As DataTable
        Get
            Return tb1
        End Get
        Set(value As DataTable)
            tb1 = value
        End Set
    End Property

    Public Property Tab11 As clsBanco
        Get
            Return tab1
        End Get
        Set(value As clsBanco)
            tab1 = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    'Public Property Listaclasse As List(Of clsjoia2)
    '    Get
    '        Return _listaclasse
    '    End Get
    '    Set(value As List(Of clsjoia2))
    '        _listaclasse = value
    '    End Set
    'End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrsequser As Integer
        Get
            Return _nrsequser
        End Get
        Set(value As Integer)
            _nrsequser = value
        End Set
    End Property

    Public Property Mes As Integer
        Get
            Return _mes
        End Get
        Set(value As Integer)
            _mes = value
        End Set
    End Property

    Public Property Ano As Integer
        Get
            Return _ano
        End Get
        Set(value As Integer)
            _ano = value
        End Set
    End Property

    Public Property Statuspg As Integer
        Get
            Return _statuspg
        End Get
        Set(value As Integer)
            _statuspg = value
        End Set
    End Property

    Public Property Frmpg As Integer
        Get
            Return _frmpg
        End Get
        Set(value As Integer)
            _frmpg = value
        End Set
    End Property

    Public Property Dtpg As Date
        Get
            Return _dtpg
        End Get
        Set(value As Date)
            _dtpg = value
        End Set
    End Property

    Public Property Statusfiliacao As Integer
        Get
            Return _statusfiliacao
        End Get
        Set(value As Integer)
            _statusfiliacao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Nrseqtipojoia As Integer
        Get
            Return _nrseqtipojoia
        End Get
        Set(value As Integer)
            _nrseqtipojoia = value
        End Set
    End Property

    Public Property Descricaotipojoia As String
        Get
            Return _descricaotipojoia
        End Get
        Set(value As String)
            _descricaotipojoia = value
        End Set
    End Property

    Public Property Parcelas As Integer
        Get
            Return _parcelas
        End Get
        Set(value As Integer)
            _parcelas = value
        End Set
    End Property
End Class

Partial Public Class clsjoia2

    'Public Function Listarjoias2() As Boolean
    '    Try
    '        Listaclasse.Clear()
    '        tb1 = tab1.conectar("select * from tbjoias_2 where ativo = true")
    '        For x As Integer = 0 To Tb11.Rows.Count - 1
    '            Listaclasse.Add(New clsjoia2 With {.Nrseq = numeros(tb1.Rows(x)("nrseq").ToString), .Nrsequser = numeros(tb1.Rows(x)("nrsequser").ToString), .Mes = numeros(tb1.Rows(x)("mes").ToString), .Ano = numeros(tb1.Rows(x)("ano").ToString), .Statuspg = numeros(tb1.Rows(x)("statuspg").ToString), .Frmpg = numeros(tb1.Rows(x)("frmpg").ToString), .Dtpg = valordata(tb1.Rows(x)("dtpg").ToString), .Statusfiliacao = numeros(tb1.Rows(x)("statusfiliacao").ToString), .Ativo = tb1.Rows(x)("ativo").ToString, .Usercad = tb1.Rows(x)("usercad").ToString, .Dtexclui = valordata(tb1.Rows(x)("dtexclui").ToString), .Userexclui = tb1.Rows(x)("userexclui").ToString, .Nrseqtipojoia = numeros(tb1.Rows(x)("nrseqtipojoia").ToString), .Descricaotipojoia = tb1.Rows(x)("descricaotipojoia").ToString})
    '        Next
    '        Return True
    '    Catch excons As Exception
    '        Mensagemerro = excons.Message
    '        Return False
    '    End Try
    'End Function


    Public Function procurar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbjoias_2 where nrseq = " & Nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            Nrseq = numeros(Tb11.Rows(0)("nrseq").ToString)
            Nrsequser = numeros(Tb11.Rows(0)("nrsequser").ToString)
            Mes = numeros(Tb11.Rows(0)("mes").ToString)
            Ano = numeros(Tb11.Rows(0)("ano").ToString)
            Statuspg = numeros(Tb11.Rows(0)("statuspg").ToString)
            Frmpg = numeros(Tb11.Rows(0)("frmpg").ToString)
            Dtpg = FormatDateTime(valordata(Tb11.Rows(0)("dtpg").ToString), DateFormat.ShortDate)
            Statusfiliacao = numeros(Tb11.Rows(0)("statusfiliacao").ToString)
            Ativo = logico(Tb11.Rows(0)("ativo").ToString)
            Usercad = Tb11.Rows(0)("usercad").ToString
            Dtexclui = FormatDateTime(valordata(Tb11.Rows(0)("dtexclui").ToString), DateFormat.ShortDate)
            Userexclui = Tb11.Rows(0)("userexclui").ToString
            Nrseqtipojoia = numeros(Tb11.Rows(0)("nrseqtipojoia").ToString)
            Descricaotipojoia = Tb11.Rows(0)("descricaotipojoia").ToString
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbjoias_2 Set ativo = True"
            If Nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(Nrseq)
            End If
            If Nrsequser <> 0 Then
                xsql &= ",nrsequser = " & moeda(Nrsequser)
            End If
            If Mes <> 0 Then
                xsql &= ",mes = " & moeda(Mes)
            End If
            If Ano <> 0 Then
                xsql &= ",ano = " & moeda(Ano)
            End If
            If Statuspg <> 0 Then
                xsql &= ",statuspg = " & moeda(Statuspg)
            End If
            If Frmpg <> 0 Then
                xsql &= ",frmpg = " & moeda(Frmpg)
            End If
            If Dtpg <> "" Then
                xsql &= ",dtpg = '" & formatadatamysql(Dtpg) & "'"
            End If
            If Statusfiliacao <> 0 Then
                xsql &= ",statusfiliacao = " & moeda(Statusfiliacao)
            End If
            If Ativo <> True Then
                xsql &= ",ativo = '" & logico(Ativo) & "'"
            End If
            If Usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(Usercad) & "'"
            End If
            If Dtexclui <> "" Then
                xsql &= ",dtexclui = '" & formatadatamysql(Dtexclui) & "'"
            End If
            If Userexclui <> "" Then
                xsql &= ",userexclui = '" & tratatexto(Userexclui) & "'"
            End If
            If Nrseqtipojoia <> 0 Then
                xsql &= ",nrseqtipojoia = " & moeda(Nrseqtipojoia)
            End If
            If Descricaotipojoia <> "" Then
                xsql &= ",descricaotipojoia = '" & tratatexto(Descricaotipojoia) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbjoias_2 (nrseqctrl, dtcad, usercad, ativo, valor) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false , '200')")
            tb1 = tab1.conectar("Select * from  tbjoias_2 where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function gerarlote() As Boolean
        Try
            Dim ultimodia As Integer = 12

            For index = 1 To Parcelas

                Dim wcnrseqctrl As String = gerarnrseqcontrole()

                tb1 = tab1.IncluirAlterarDados("insert into tbjoias_2 (nrseqctrl, dtcad, usercad, ativo, statuspg, mes, ano,nrsequser) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', true, 0, '" & Mes & "','" & Ano & "','" & Nrsequser & "')")


                If Mes = 12 Then
                    Mes = 1
                    Ano = Ano + 1
                Else
                    Mes = Mes + 1
                End If
            Next


            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrsequser = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbjoias_2 set ativo = false where nrsequser = " & Nrsequser)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function



    Public Function estorno() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbjoias_2 set statuspg = '2' where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function baixar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbjoias_2 set statuspg = '1' where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function aberto() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbjoias_2 set statuspg = '0' where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluirtodas() As Boolean
        Try
            If Nrsequser = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbjoias_2 Set ativo = False , statuspg = 0 where nrsequser = " & Nrsequser)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


End Class

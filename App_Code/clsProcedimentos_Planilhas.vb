﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports clssessoes
Imports clsSmart
Public Class clsProcedimentos_Planilhas
    Dim tablename As String = "tbprocedimentos_planilhas"

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _contador As Integer
    Dim _table As Data.DataTable
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _arquivo As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _processado As Integer
    Dim _linhas As Integer
    Dim _linhaatual As Integer
    Dim _ativo As Integer
    Dim _dtprocessado As Date
    Dim _userprocessado As String

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Processado As Integer
        Get
            Return _processado
        End Get
        Set(value As Integer)
            _processado = value
        End Set
    End Property

    Public Property Linhas As Integer
        Get
            Return _linhas
        End Get
        Set(value As Integer)
            _linhas = value
        End Set
    End Property

    Public Property Linhaatual As Integer
        Get
            Return _linhaatual
        End Get
        Set(value As Integer)
            _linhaatual = value
        End Set
    End Property

    Public Property Ativo As Integer
        Get
            Return _ativo
        End Get
        Set(value As Integer)
            _ativo = value
        End Set
    End Property

    Public Property Dtprocessado As Date
        Get
            Return _dtprocessado
        End Get
        Set(value As Date)
            _dtprocessado = value
        End Set
    End Property

    Public Property Userprocessado As String
        Get
            Return _userprocessado
        End Get
        Set(value As String)
            _userprocessado = value
        End Set
    End Property
End Class

Partial Public Class clsProcedimentos_Planilhas

    Public Function consultarProcessado() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & tablename & " where processado = '" & _processado & "' order by nrseq desc")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _arquivo = tb1.Rows(0)("arquivo").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _usercad = tb1.Rows(0)("usercad").ToString
        _processado = tb1.Rows(0)("processado").ToString
        _linhas = tb1.Rows(0)("linhas").ToString
        _linhaatual = tb1.Rows(0)("linhaatual").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        _dtprocessado = FormatDateTime(tb1.Rows(0)("dtprocessado").ToString, DateFormat.ShortDate)
        _userprocessado = tb1.Rows(0)("userprocessado").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procurar() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & tablename & " where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _arquivo = tb1.Rows(0)("arquivo").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _usercad = tb1.Rows(0)("usercad").ToString
        _processado = tb1.Rows(0)("processado").ToString
        _linhas = tb1.Rows(0)("linhas").ToString
        _linhaatual = tb1.Rows(0)("linhaatual").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        _dtprocessado = FormatDateTime(tb1.Rows(0)("dtprocessado").ToString, DateFormat.ShortDate)
        _userprocessado = tb1.Rows(0)("userprocessado").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & tablename & " Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _arquivo <> "" Then
                xsql &= ",arquivo = '" & tratatexto(_arquivo) & "'"
            End If
            If _dtcad <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _processado <> 0 Then
                xsql &= ",processado = " & moeda(_processado)
            End If
            If _linhas <> 0 Then
                xsql &= ",linhas = " & moeda(_linhas)
            End If
            If _linhaatual <> 0 Then
                xsql &= ",linhaatual = " & moeda(_linhaatual)
            End If
            If _ativo <> 0 Then
                xsql &= ",ativo = " & moeda(_ativo)
            End If
            If _dtprocessado <> "" Then
                xsql &= ",dtprocessado = '" & formatadatamysql(_dtprocessado) & "'"
            End If
            If _userprocessado <> "" Then
                xsql &= ",userprocessado = '" & tratatexto(_userprocessado) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole("")
            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


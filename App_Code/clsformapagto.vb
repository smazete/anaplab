﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data
Public Class clsformapagto


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _tablename As String = "tbformapagto"
    Dim _table As New Data.DataTable

    Dim _nrseq As Integer = 0
    Dim _descricao As String
    Dim _valor As Decimal = 0
    Dim _maquina As String
    Dim _nrseqempresa As Integer = 0
    Dim _cod As Integer = 0
    Dim _copiadosistema As Integer = 0
    Dim _copiadosistemapor As String
    Dim _copiadosistemaem As DateTime
    Dim _agencia As String
    Dim _psk As String
    Dim _valortotal As Decimal = 0
    Dim _tpCobranca As String
    Dim _mesestolerancia As Decimal = 0
    Dim _multa As Decimal = 0
    Dim _juros As Integer = 0
    Dim _cobrarjuros As String
    Dim _baixarpor As String
    Dim _ativo As Boolean
    Dim _utilizar_debito As Boolean
    Dim _uc_cliente As Boolean
    Dim _nao_permite_uc_duplicada As Boolean
    Dim _avisar_uc_duplicada As Boolean
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _nrseqctrl As String
    Dim _padraocpfl As String
    Dim _salvo As Boolean
    Dim _dtexclui As DateTime
    Dim _userexclui As String


    Dim _mensagemerro As String
    Dim _contador As Integer = 0

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _valor
        End Get
        Set(value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property Maquina As String
        Get
            Return _maquina
        End Get
        Set(value As String)
            _maquina = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Cod As Integer
        Get
            Return _cod
        End Get
        Set(value As Integer)
            _cod = value
        End Set
    End Property

    Public Property Copiadosistema As Integer
        Get
            Return _copiadosistema
        End Get
        Set(value As Integer)
            _copiadosistema = value
        End Set
    End Property

    Public Property Copiadosistemapor As String
        Get
            Return _copiadosistemapor
        End Get
        Set(value As String)
            _copiadosistemapor = value
        End Set
    End Property

    Public Property Copiadosistemaem As Date
        Get
            Return _copiadosistemaem
        End Get
        Set(value As Date)
            _copiadosistemaem = value
        End Set
    End Property

    Public Property Agencia As String
        Get
            Return _agencia
        End Get
        Set(value As String)
            _agencia = value
        End Set
    End Property

    Public Property Psk As String
        Get
            Return _psk
        End Get
        Set(value As String)
            _psk = value
        End Set
    End Property

    Public Property Valortotal As Decimal
        Get
            Return _valortotal
        End Get
        Set(value As Decimal)
            _valortotal = value
        End Set
    End Property

    Public Property TpCobranca As String
        Get
            Return _tpCobranca
        End Get
        Set(value As String)
            _tpCobranca = value
        End Set
    End Property

    Public Property Mesestolerancia As Decimal
        Get
            Return _mesestolerancia
        End Get
        Set(value As Decimal)
            _mesestolerancia = value
        End Set
    End Property

    Public Property Multa As Decimal
        Get
            Return _multa
        End Get
        Set(value As Decimal)
            _multa = value
        End Set
    End Property

    Public Property Juros As Integer
        Get
            Return _juros
        End Get
        Set(value As Integer)
            _juros = value
        End Set
    End Property

    Public Property Cobrarjuros As String
        Get
            Return _cobrarjuros
        End Get
        Set(value As String)
            _cobrarjuros = value
        End Set
    End Property

    Public Property Baixarpor As String
        Get
            Return _baixarpor
        End Get
        Set(value As String)
            _baixarpor = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Utilizar_debito As Boolean
        Get
            Return _utilizar_debito
        End Get
        Set(value As Boolean)
            _utilizar_debito = value
        End Set
    End Property

    Public Property Uc_cliente As Boolean
        Get
            Return _uc_cliente
        End Get
        Set(value As Boolean)
            _uc_cliente = value
        End Set
    End Property

    Public Property Nao_permite_uc_duplicada As Boolean
        Get
            Return _nao_permite_uc_duplicada
        End Get
        Set(value As Boolean)
            _nao_permite_uc_duplicada = value
        End Set
    End Property

    Public Property Avisar_uc_duplicada As Boolean
        Get
            Return _avisar_uc_duplicada
        End Get
        Set(value As Boolean)
            _avisar_uc_duplicada = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Padraocpfl As String
        Get
            Return _padraocpfl
        End Get
        Set(value As String)
            _padraocpfl = value
        End Set
    End Property

    Public Property Salvo As Boolean
        Get
            Return _salvo
        End Get
        Set(value As Boolean)
            _salvo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class
Partial Public Class clsformapagto

    Public Function consultar() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where salvo = true and nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then

                _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _valor = moeda(tb1.Rows(0)("valor").ToString)
                _maquina = tb1.Rows(0)("maquina").ToString
                _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
                _cod = numeros(tb1.Rows(0)("cod").ToString)
                _copiadosistema = numeros(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _agencia = tb1.Rows(0)("agencia").ToString
                _psk = tb1.Rows(0)("psk").ToString
                _valortotal = moeda(tb1.Rows(0)("valortotal").ToString)
                _tpCobranca = tb1.Rows(0)("tpCobranca").ToString
                _mesestolerancia = moeda(tb1.Rows(0)("mesestolerancia").ToString)
                _multa = moeda(tb1.Rows(0)("multa").ToString)
                _juros = numeros(tb1.Rows(0)("juros").ToString)
                _cobrarjuros = tb1.Rows(0)("cobrarjuros").ToString
                _baixarpor = tb1.Rows(0)("baixarpor").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _utilizar_debito = logico(tb1.Rows(0)("utilizar_debito").ToString)
                _uc_cliente = logico(tb1.Rows(0)("uc_cliente").ToString)
                _nao_permite_uc_duplicada = logico(tb1.Rows(0)("nao_permite_uc_duplicada").ToString)
                _avisar_uc_duplicada = logico(tb1.Rows(0)("avisar_uc_duplicada").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _padraocpfl = tb1.Rows(0)("padraocpfl").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarpesq(type As String, typesearch As String, Optional ativo As Boolean = True) As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & _tablename & " Where " & type & " Like '" & typesearch & "%' and salvo=true and ativo= " & ativo & "")

            If tb1.Rows.Count > 0 Then

                _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _valor = moeda(tb1.Rows(0)("valor").ToString)
                _maquina = tb1.Rows(0)("maquina").ToString
                _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
                _cod = numeros(tb1.Rows(0)("cod").ToString)
                _copiadosistema = numeros(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _agencia = tb1.Rows(0)("agencia").ToString
                _psk = tb1.Rows(0)("psk").ToString
                _valortotal = moeda(tb1.Rows(0)("valortotal").ToString)
                _tpCobranca = tb1.Rows(0)("tpCobranca").ToString
                _mesestolerancia = moeda(tb1.Rows(0)("mesestolerancia").ToString)
                _multa = moeda(tb1.Rows(0)("multa").ToString)
                _juros = numeros(tb1.Rows(0)("juros").ToString)
                _cobrarjuros = tb1.Rows(0)("cobrarjuros").ToString
                _baixarpor = tb1.Rows(0)("baixarpor").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _utilizar_debito = logico(tb1.Rows(0)("utilizar_debito").ToString)
                _uc_cliente = logico(tb1.Rows(0)("uc_cliente").ToString)
                _nao_permite_uc_duplicada = logico(tb1.Rows(0)("nao_permite_uc_duplicada").ToString)
                _avisar_uc_duplicada = logico(tb1.Rows(0)("avisar_uc_duplicada").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _padraocpfl = tb1.Rows(0)("padraocpfl").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarcliente() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where salvo = true and nrseqempresa = '" & _nrseqempresa & "'")


            If tb1.Rows.Count > 0 Then

                _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _valor = moeda(tb1.Rows(0)("valor").ToString)
                _maquina = tb1.Rows(0)("maquina").ToString
                _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
                _cod = numeros(tb1.Rows(0)("cod").ToString)
                _copiadosistema = numeros(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _agencia = tb1.Rows(0)("agencia").ToString
                _psk = tb1.Rows(0)("psk").ToString
                _valortotal = moeda(tb1.Rows(0)("valortotal").ToString)
                _tpCobranca = tb1.Rows(0)("tpCobranca").ToString
                _mesestolerancia = moeda(tb1.Rows(0)("mesestolerancia").ToString)
                _multa = moeda(tb1.Rows(0)("multa").ToString)
                _juros = numeros(tb1.Rows(0)("juros").ToString)
                _cobrarjuros = tb1.Rows(0)("cobrarjuros").ToString
                _baixarpor = tb1.Rows(0)("baixarpor").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _utilizar_debito = logico(tb1.Rows(0)("utilizar_debito").ToString)
                _uc_cliente = logico(tb1.Rows(0)("uc_cliente").ToString)
                _nao_permite_uc_duplicada = logico(tb1.Rows(0)("nao_permite_uc_duplicada").ToString)
                _avisar_uc_duplicada = logico(tb1.Rows(0)("avisar_uc_duplicada").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _padraocpfl = tb1.Rows(0)("padraocpfl").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function consultartodos(Optional ativo As Boolean = True, Optional descricao As String = "") As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & _tablename & " Where salvo = true  " & IIf(descricao <> "", " and descricao like '%" & descricao & "%'", "") & " and ativo = " & ativo & "")

            If tb1.Rows.Count > 0 Then
                '_nrseq = tb1.Rows(0)("nrseq").ToString
                '_descricao = tb1.Rows(0)("descricao").ToString
                '_valor = tb1.Rows(0)("valor").ToString
                '       _maquina = tb1.Rows(0)("maquina").ToString
                '   _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                '   _cod = tb1.Rows(0)("cod").ToString
                '   _copiadosistema = tb1.Rows(0)("copiadosistema").ToString
                ''   _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                '_agencia = tb1.Rows(0)("agencia").ToString
                '_psk = tb1.Rows(0)("psk").ToString
                '_valortotal = tb1.Rows(0)("valortotal").ToString
                '_tpCobranca = tb1.Rows(0)("tpCobranca").ToString
                '_mesestolerancia = tb1.Rows(0)("mesestolerancia").ToString
                '_multa = tb1.Rows(0)("multa").ToString
                '_juros = tb1.Rows(0)("juros").ToString
                '_cobrarjuros = tb1.Rows(0)("cobrarjuros").ToString
                '_baixarpor = tb1.Rows(0)("baixarpor").ToString
                '_ativo = logico(tb1.Rows(0)("ativo").ToString)
                '_utilizar_debito = tb1.Rows(0)("utilizar_debito").ToString
                '_uc_cliente = tb1.Rows(0)("uc_cliente").ToString
                '_nao_permite_uc_duplicada = tb1.Rows(0)("nao_permite_uc_duplicada").ToString
                '_avisar_uc_duplicada = tb1.Rows(0)("avisar_uc_duplicada").ToString
                '_usercad = tb1.Rows(0)("usercad").ToString
                '_nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
                '_padraocpfl = tb1.Rows(0)("padraocpfl").ToString

                'Try
                '    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                '        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                '        _dtcad = dt.ToString("yyyy-MM-dd")
                '    End If
                'Catch exconsultasql As Exception
                '    _mensagemerro = exconsultasql.Message
                'End Try

                'Try
                '    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                '        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                '        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                '    End If
                'Catch exconsultasql As Exception
                '    _mensagemerro = exconsultasql.Message
                'End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function



    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, ativo, dtcad, usercad,nrseqempresa) values ('" & wcnrseqctrl & "',false,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "','" & _nrseqempresa & "')")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            Nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set salvo = true, Descricao = '" & _descricao & "', Valor ='" & _valor & "',Maquina ='" & _maquina & "',nrseqempresa ='" & _nrseqempresa & "',Cod ='" & _cod & "',Copiadosistema ='" & _copiadosistema & "',Copiadosistemapor ='" & _copiadosistemapor & "',Copiadosistemaem ='" & _copiadosistemaem & "',Agencia ='" & _agencia & "',Psk ='" & _psk & "',Valortotal ='" & _valortotal & "',TpCobranca ='" & _tpCobranca & "',Mesestolerancia ='" & _mesestolerancia & "',Multa ='" & _multa & "',Juros ='" & _juros & "',Cobrarjuros ='" & _cobrarjuros & "',Baixarpor ='" & _baixarpor & "',Ativo ='1',Utilizar_debito ='" & _utilizar_debito & "',Uc_cliente ='" & _uc_cliente & "',Nao_permite_uc_duplicada ='" & _nao_permite_uc_duplicada & "',Avisar_uc_duplicada ='" & _avisar_uc_duplicada & "',Padraocpfl ='" & _padraocpfl & "' where nrseq =" & _nrseq)

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If Nrseq = 0 Then
                _mensagemerro = "Please, enter a valid Funcionario!"
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & Nrseq)

            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.ToString
            Return False

        End Try

    End Function


End Class
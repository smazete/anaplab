﻿Imports Microsoft.VisualBasic

Public Class clssolicitacoes_contatos

    Dim _nrseq As Integer = 0
    Dim _nrseqsol As Integer = 0
    Dim _nrsequsuario As Integer = 0
    Dim _ativo As Boolean = True
    Dim _respondido As Boolean = False
    Dim _dtrespondido As Date
    Dim _hrrespondido As String

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqsol As Integer
        Get
            Return _nrseqsol
        End Get
        Set(value As Integer)
            _nrseqsol = value
        End Set
    End Property

    Public Property Nrsequsuario As Integer
        Get
            Return _nrsequsuario
        End Get
        Set(value As Integer)
            _nrsequsuario = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Respondido As Boolean
        Get
            Return _respondido
        End Get
        Set(value As Boolean)
            _respondido = value
        End Set
    End Property

    Public Property Dtrespondido As Date
        Get
            Return _dtrespondido
        End Get
        Set(value As Date)
            _dtrespondido = value
        End Set
    End Property

    Public Property Hrrespondido As String
        Get
            Return _hrrespondido
        End Get
        Set(value As String)
            _hrrespondido = value
        End Set
    End Property
End Class

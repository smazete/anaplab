﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports System.Data
Imports System.Runtime.CompilerServices

Public Class clsmensalidades

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _nrseqcliente As Integer
    Dim _mes As Integer
    Dim _ano As Integer
    Dim _parcelas As Integer
    Dim _statuspg As Integer
    Dim _frmpg As Integer
    Dim _dtpg As Date
    Dim _statusfiliacao As Integer
    Dim _dtemissao As Date
    Dim _dtvencimente As Date
    Dim _valor As Decimal
    Dim _valorpg As Decimal
    Dim _arquivo As String
    Dim _pago As String
    Dim _automatico As String
    Dim _ativo As String
    Dim _userexclui As String
    Dim _dtexclui As Date
    Dim _useraltera As String
    Dim _dtaltera As Date
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _segundavia As String
    Dim _matricula As String
    Dim _estornado As String
    Dim _mensagemp As String
    Dim _mensagemg As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqcliente As Integer
        Get
            Return _nrseqcliente
        End Get
        Set(value As Integer)
            _nrseqcliente = value
        End Set
    End Property

    Public Property Mes As Integer
        Get
            Return _mes
        End Get
        Set(value As Integer)
            _mes = value
        End Set
    End Property

    Public Property Ano As Integer
        Get
            Return _ano
        End Get
        Set(value As Integer)
            _ano = value
        End Set
    End Property

    Public Property Statuspg As Integer
        Get
            Return _statuspg
        End Get
        Set(value As Integer)
            _statuspg = value
        End Set
    End Property

    Public Property Frmpg As Integer
        Get
            Return _frmpg
        End Get
        Set(value As Integer)
            _frmpg = value
        End Set
    End Property

    Public Property Dtpg As Date
        Get
            Return _dtpg
        End Get
        Set(value As Date)
            _dtpg = value
        End Set
    End Property

    Public Property Statusfiliacao As Integer
        Get
            Return _statusfiliacao
        End Get
        Set(value As Integer)
            _statusfiliacao = value
        End Set
    End Property

    Public Property Dtemissao As Date
        Get
            Return _dtemissao
        End Get
        Set(value As Date)
            _dtemissao = value
        End Set
    End Property

    Public Property Dtvencimente As Date
        Get
            Return _dtvencimente
        End Get
        Set(value As Date)
            _dtvencimente = value
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Pago As String
        Get
            Return _pago
        End Get
        Set(value As String)
            _pago = value
        End Set
    End Property

    Public Property Automatico As String
        Get
            Return _automatico
        End Get
        Set(value As String)
            _automatico = value
        End Set
    End Property

    Public Property Ativo As String
        Get
            Return _ativo
        End Get
        Set(value As String)
            _ativo = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Useraltera As String
        Get
            Return _useraltera
        End Get
        Set(value As String)
            _useraltera = value
        End Set
    End Property

    Public Property Dtaltera As Date
        Get
            Return _dtaltera
        End Get
        Set(value As Date)
            _dtaltera = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Segundavia As Integer
        Get
            Return Segundavia1
        End Get
        Set(value As Integer)
            Segundavia1 = value
        End Set
    End Property

    Public Property Estornado As String
        Get
            Return _estornado
        End Get
        Set(value As String)
            _estornado = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _valor
        End Get
        Set(value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property Valorpg As Decimal
        Get
            Return _valorpg
        End Get
        Set(value As Decimal)
            _valorpg = value
        End Set
    End Property

    Public Property Segundavia1 As String
        Get
            Return _segundavia
        End Get
        Set(value As String)
            _segundavia = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Parcelas As Integer
        Get
            Return _parcelas
        End Get
        Set(value As Integer)
            _parcelas = value
        End Set
    End Property

    Public Property Mensagemp As String
        Get
            Return _mensagemp
        End Get
        Set(value As String)
            _mensagemp = value
        End Set
    End Property

    Public Property Mensagemg As String
        Get
            Return _mensagemg
        End Get
        Set(value As String)
            _mensagemg = value
        End Set
    End Property
End Class

Partial Public Class clsmensalidades

    Public Function procurar() As Boolean

        Try
            If _nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbmensalidades where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            _nrseq = tb1.Rows(0)("nrseq").ToString
            _nrseqcliente = tb1.Rows(0)("nrseqcliente").ToString
            _mes = tb1.Rows(0)("mes").ToString
            _ano = tb1.Rows(0)("ano").ToString
            '   _statuspg = tb1.Rows(0)("statuspg").ToString
            '  _frmpg = tb1.Rows(0)("frmpg").ToString
            '  _dtpg = FormatDateTime(tb1.Rows(0)("dtpg"))
            '   _statusfiliacao = tb1.Rows(0)("statusfiliacao").ToString
            ' _dtemissao = FormatDateTime(tb1.Rows(0)("dtemissao").ToString, DateFormat.ShortDate)
            ' _dtvencimente = FormatDateTime(tb1.Rows(0)("dtvencimente").ToString, DateFormat.ShortDate)
            ' _arquivo = tb1.Rows(0)("arquivo").ToString
            _pago = logico(tb1.Rows(0)("pago").ToString)
            _automatico = tb1.Rows(0)("automatico").ToString
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            '  _dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
            _useraltera = tb1.Rows(0)("useraltera").ToString
            ' _dtaltera = FormatDateTime(tb1.Rows(0)("dtaltera").ToString, DateFormat.ShortDate)
            '    _usercad = tb1.Rows(0)("usercad").ToString
            '_dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
            Segundavia1 = logico(tb1.Rows(0)("segundavia").ToString)
            _estornado = logico(tb1.Rows(0)("estornado").ToString)
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function gerarnovasmensalidades() As Boolean


        Try
            Dim dia As Integer = DateTime.Now.Day
            Dim mesatual As Integer = DateTime.Now.Month
            Dim mes As Integer
            Dim ano As Integer = DateTime.Now.Year
            Dim ultimodia As Integer = 12

            If dia >= 15 Then

                Dim wcnrseqctrl As String = gerarnrseqcontrole()

                tb1 = tab1.IncluirAlterarDados("insert into tbmensalidades (nrseqctrl, dtcad, usercad, ativo, statuspg, mes, ano, nrseqcliente) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', true, 3, '" & mesatual & "','" & ano & "','" & Nrseqcliente & "')")

                If mesatual = 12 Then
                    mes = 1
                    ano = ano + 1
                Else
                    mes = mesatual + 1
                End If
            Else
                mes = mesatual
            End If

            If mesatual > 5 Then
                Dim num As Integer = 0
                num = 12 - mes
                Parcelas = num + 12
            End If
            '      Parcelas

            For index = 1 To Parcelas

                Dim wcnrseqctrl As String = gerarnrseqcontrole()

                tb1 = tab1.IncluirAlterarDados("insert into tbmensalidades (nrseqctrl, dtcad, usercad, ativo, statuspg, mes, ano, nrseqcliente) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', true, 0, '" & mes & "','" & ano & "','" & Nrseqcliente & "')")


                If mes = 12 Then
                    mes = 1
                    ano = ano + 1
                Else
                    mes = mes + 1
                End If
            Next


            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try


    End Function
    Public Function procuramensalidadecls() As Boolean

        Try
            If _nrseqcliente = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            '     tb1 = tab1.conectar("select * from tbmensalidades where agencia = '" & Agencia & "' and ano = '" & _ano & "' and nrseqcliente = '" & _nrseqcliente & "' and ativo = '1'   ")
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If

            _nrseq = tb1.Rows(0)("nrseq").ToString
            _nrseqcliente = tb1.Rows(0)("nrseqcliente").ToString
            _mes = tb1.Rows(0)("mes").ToString
            _ano = tb1.Rows(0)("ano").ToString
            _statuspg = tb1.Rows(0)("statuspg").ToString
            _frmpg = tb1.Rows(0)("frmpg").ToString
            _dtpg = FormatDateTime(tb1.Rows(0)("dtpg").ToString, DateFormat.ShortDate)
            ' _statusfiliacao = tb1.Rows(0)("statusfiliacao").ToString
            ' _dtemissao = FormatDateTime(tb1.Rows(0)("dtemissao").ToString, DateFormat.ShortDate)
            ' _dtvencimente = FormatDateTime(tb1.Rows(0)("dtvencimente").ToString, DateFormat.ShortDate)
            ' _arquivo = tb1.Rows(0)("arquivo").ToString
            ' _pago = logico(tb1.Rows(0)("pago").ToString)
            ' _automatico = tb1.Rows(0)("automatico").ToString
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            ' _userexclui = tb1.Rows(0)("userexclui").ToString
            ' _dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
            ' _useraltera = tb1.Rows(0)("useraltera").ToString
            ' _dtaltera = FormatDateTime(tb1.Rows(0)("dtaltera").ToString, DateFormat.ShortDate)
            ' _usercad = tb1.Rows(0)("usercad").ToString
            '_dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
            '_segundavia = logico(tb1.Rows(0)("segundavia").ToString)
            '_estornado = logico(tb1.Rows(0)("estornado").ToString)

            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function validalogin() As Boolean
        If _nrseqcliente = 0 Then
            Mensagemerro = "Preencha o campo de Matricula"
        End If
        Dim xAssoc As New clsAssociado

        xAssoc.Matricula = _matricula

        If Not xAssoc.procurarmpormat() Then
            Mensagemerro = "Matricula nao encontrada"
            Return False
        End If
        _nrseqcliente = xAssoc.Nrseq

        tb1 = tab1.conectar("select * from tbmensalidades where nrseqcliente = '" & _nrseqcliente & "' and  pago = '0'   and ativo = '1' and  dtvencimento <= '2020-10-31'   ")
        If tb1.Rows.Count <> 0 And HttpContext.Current.Session("votar") = "1" Then
            Mensagemerro = "Votação não autorizada"
            Return False
        End If

        'mudar tambem para statuspg = 0 or statuspg  = o equivalente de isenta
        tb1 = tab1.conectar("select * from tbmensalidades where nrseqcliente = '" & _nrseqcliente & "' and  pago = '0'   and ativo = '1' and  dtvencimento <= '" & hoje() & "'   ")

        If tb1.Rows.Count <> 0 Then
            Mensagemerro = "Cliente esta devendo"
            Return False
        End If
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = _mensagemerro
            Return False
        End Try
    End Function

    Public Function salvarimporta() As Boolean
        Try
            tb1 = tab1.conectar("select * from tbmensalidades where nrseqcliente = '" & _nrseqcliente & "' and  pago = '0' and statuspg = '0'  and ativo = '1' order by  ano , mes asc limit 1 ")
            _nrseq = tb1.Rows(0)("nrseq").ToString

            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set pago = '1', dtpg = '" & formatadatamysql(Dtpg) & "', statuspg = '1', estornado = '0' where nrseq = '" & _nrseq & "' and nrseqcliente = '" & _nrseqcliente & "'")
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update Table Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _nrseqcliente <> 0 Then
                xsql &= ",nrseqcliente = " & moeda(_nrseqcliente)
            End If
            If _mes <> 0 Then
                xsql &= ",mes = " & moeda(_mes)
            End If
            If _ano <> 0 Then
                xsql &= ",ano = " & moeda(_ano)
            End If
            If _statuspg <> 0 Then
                xsql &= ",statuspg = " & moeda(_statuspg)
            End If
            If _frmpg <> 0 Then
                xsql &= ",frmpg = " & moeda(_frmpg)
            End If
            If _dtpg <> "" Then
                xsql &= ",dtpg = '" & formatadatamysql(_dtpg) & "'"
            End If
            If _statusfiliacao <> 0 Then
                xsql &= ",statusfiliacao = " & moeda(_statusfiliacao)
            End If
            If _dtemissao <> "" Then
                xsql &= ",dtemissao = '" & formatadatamysql(_dtemissao) & "'"
            End If
            If _dtvencimente <> "" Then
                xsql &= ",dtvencimente = '" & formatadatamysql(_dtvencimente) & "'"
            End If
            If _arquivo <> "" Then
                xsql &= ",arquivo = '" & tratatexto(_arquivo) & "'"
            End If
            If _pago <> 0 Then
                xsql &= ",pago = " & moeda(_pago)
            End If
            If _automatico <> 0 Then
                xsql &= ",automatico = " & moeda(_automatico)
            End If
            If _ativo <> 0 Then
                xsql &= ",ativo = " & moeda(_ativo)
            End If
            If _userexclui <> "" Then
                xsql &= ",userexclui = '" & tratatexto(_userexclui) & "'"
            End If
            If _dtexclui <> "" Then
                xsql &= ",dtexclui = '" & formatadatamysql(_dtexclui) & "'"
            End If
            If _useraltera <> "" Then
                xsql &= ",useraltera = '" & tratatexto(_useraltera) & "'"
            End If
            If _dtaltera <> "" Then
                xsql &= ",dtaltera = '" & formatadatamysql(_dtaltera) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _dtcad <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If Segundavia1 <> 0 Then
                xsql &= ",segundavia = " & moeda(Segundavia1)
            End If
            If _estornado <> 0 Then
                xsql &= ",estornado = " & moeda(_estornado)
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbmensalidades (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbmensalidades where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            tb1 = tab1.conectar("Select * from  tbmensalidades  where nrseq = '" & _nrseq & "'")
            Ativo = tb1.Rows(0)("ativo").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function corrigir() As Boolean
        Try

            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set pago = 0, statuspg = 0, estornado = 0,  valorpago = '20', dtpg ='0000-00-00', mensalidadeteste = 1 where  dtpg ='2020-09-21' ")

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function corrigedatas() As Boolean
        Try

            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set valorpago = '20', dtpg ='0000-00-00', frmpg = 0 ,statusfiliacao = 1 , mensalidadeteste = 1 where ano = '2021' and dtpg is null")

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function pagar() As Boolean
        Try

            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If

            If Not procurar() Then
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set pago = '1', dtpg = '" & formatadatamysql(Dtpg) & "', statuspg = '1' , estornado = '0' where nrseq = " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function



    Public Function aberto() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set pago = 0 , dtpg = '0000-00-00', statuspg = 0,  estornado = 0 where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function mensalidadeano() As Boolean
        Try
            '    If _nrseqassociado = "" Then
            '        _mensagemerro = "Por favor, selecione um Associado !"
            '        Return False
            '    End If
            '    If _dtinicio = "" Then
            '        _mensagemerro = "Por favor, coloque uma data inicial !"
            '        Return False
            '    End If

            tb1 = tab1.conectar("Select * from  tbmensalidades where ativo = '1' and  ano = '" & Ano & "' and mes '" & Mes & "' ")

            Dim xAssociado As New clsAssociado

            If Not xAssociado.procurar() Then
                _mensagemerro = "Por favor, selecione um Associado !"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set pago = '1', dtpg = '" & formatadatamysql(Dtpg) & "', statuspg = '1', valorpago = '" & Valorpg & "' , estornado = '0' where nrseq = " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function estornar() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set pago = '0', dtestorno = '" & hoje() & "',  statuspg = '0', valorpago = '0', estornado = '1' , pago = '0'  where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function

    Public Function gerarloteparaano() As Boolean
        Try

            Dim codigoassociado As Integer
            Dim numerosmegasena As Integer
            Dim associadonr As String = ""
            Dim associado As String = ""
            Dim pendentes As String = ""
            Dim nome As String = ""



            tb1 = tab1.conectar("select * from tbAssociados")

            ' codigoassociado = tb1.Rows.Count.ToString()

            codigoassociado = 3739

            While codigoassociado <> 0
                associado = ""
                nome = ""
                tb1 = tab1.conectar("select * from tbAssociados where ativo = 1 and nrseq = '" & codigoassociado & "'")

                If tb1.Rows.Count = 0 Then
                Else
                    associado = tb1.Rows(0)("nrseq").ToString
                    nome = tb1.Rows(0)("nome").ToString

                    Dim contames As Integer = 12
                    Dim mesuso As Integer = 1
                    Dim um As Integer = 1
                    Dim anouso As Integer = 2021
                    Usercad = "Lote2021"
                    For mesuso = 1 To contames

                        Dim wcnrseqctrl As String = gerarnrseqcontrole()
                        tb1 = tab1.IncluirAlterarDados("insert into tbmensalidades (nrseqctrl, dtcad, usercad, ativo, mensalidadeteste,valor,nrseqcliente, mes,ano,statuspg) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', true, false,20,'" & associado & "', '" & mesuso & "', '" & anouso & "','0')")


                        associadonr = "alguma coisa"

                    Next
                    numerosmegasena = numerosmegasena + 12
                End If


                codigoassociado = codigoassociado - 1
            End While
            tb1 = tab1.conectar("select * from tbmensalidades where ativo = 1 and usercad = '" & Usercad & "'")
            Mensagemg = tb1.Rows.Count.ToString()
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
        Return False
        End Try
        ' _mensagemg =
        ' _mensagemp = associadonr

    End Function

    'Public Function primeiramensalidades() As Boolean
    '    Try

    '        If Not procurar() Then
    '            Return False
    '        End If
    '        Dim moment As New System.DateTime(1999, 1, 13, 3, 57, 32, 11)
    '        Dim dia As Integer = moment.Day

    '        If dia <= 15 Then
    '            Dim wcnrseqctrl As String = gerarnrseqcontrole()
    '            tb1 = tab1.IncluirAlterarDados("insert into tbmensalidades (nrseqctrl, dtcad,  ativo) values ('" & wcnrseqctrl & "','" & hoje() & "', false)")
    '            tb1 = tab1.conectar("Select * from  tbmensalidades where nrseqctrl = '" & wcnrseqctrl & "'")
    '            _nrseq = tb1.Rows(0)("nrseq").ToString

    '        Else

    '        End If


    '        Dim contador = 12
    '        Parcelas = 1

    '        For _ = 1 To 10

    '        Next

    '        tb1 = tab1.IncluirAlterarDados("update tbmensalidades set pago='0', dtestorno = '" & hoje() & "', statuspg = '2', valorpago = '0', estornado = '1' , pago = '0'  where nrseq = " & _nrseq)
    '        Return True
    '    Catch exsalvar As Exception
    '        _mensagemerro = exsalvar.Message
    '        Return False
    '    End Try
    'End Function

    Public Function isento() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set pago = '0', statuspg = '3', valorpago = '0',dtpg = '" & formatadatamysql(Dtpg) & "', estornado = '0', pago = '0'   where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function desfiliar() As Boolean
        Try
            If _nrseqcliente = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set ativo = false,statusfiliacao = false where nrseqcliente = " & _nrseqcliente)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
    Public Function filiar() As Boolean
        Try
            If _nrseqcliente = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidades set ativo = true, statusfiliacao = true where nrseqcliente = " & _nrseqcliente)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    'Public Function geramensalidadenovoassociado() As Boolean
    '    Try




    '        For mesuso = 1 To contames

    '                    Dim wcnrseqctrl As String = gerarnrseqcontrole()
    '            tb1 = tab1.IncluirAlterarDados("insert into tbmensalidades (nrseqctrl, dtcad, usercad, ativo, mensalidadeteste,valor,nrseqcliente, mes,ano,statuspg, dtpg) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', true, false,20,'" & associado & "', '" & mesuso & "', '" & anouso & "','0',)")


    '            associadonr = "alguma coisa"

    '                Next

    '            tb1 = tab1.conectar("select * from tbmensalidades where ativo = 1 and usercad = '" & Usercad & "'")
    '        Mensagemg = tb1.Rows.Count.ToString()
    '        Return True
    '    Catch exsalvar As Exception
    '        _mensagemerro = exsalvar.Message
    '        Return False
    '    End Try
    '    ' _mensagemg =
    '    ' _mensagemp = associadonr

    'End Function

End Class




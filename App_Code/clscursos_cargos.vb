﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data

Public Class clscursos_cargos

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco

    Dim _listacursos As New Data.DataTable
    Dim _nrseq As Integer = 0
    Dim _nrseqcurso As Integer = 0
    Dim _nrseqcargo As Integer = 0
    Dim _cargahoraria As Decimal = 0
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _mensagemerro As String
    Dim _descricaocurso As String
    Dim _cargahorariacurso As String
    Dim _descricaocargo As String

    Public Sub New()
        _usercad = buscarsessoes("usuario")
    End Sub

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqcurso As Integer
        Get
            Return _nrseqcurso
        End Get
        Set(value As Integer)
            _nrseqcurso = value
            If value <> 0 Then
                tbx = tabx.conectar("select * from tbcursos where nrseq = " & value & " and ativo = true")
                If tbx.Rows.Count > 0 Then
                    _descricaocurso = tbx.Rows(0)("descricao").ToString
                End If
            End If
        End Set
    End Property

    Public Property Nrseqcargo As Integer
        Get
            Return _nrseqcargo
        End Get
        Set(value As Integer)
            _nrseqcargo = value
        End Set
    End Property

    Public Property Cargahoraria As Decimal
        Get
            Return _cargahoraria
        End Get
        Set(value As Decimal)
            _cargahoraria = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Descricaocurso As String
        Get
            Return _descricaocurso
        End Get
        Set(value As String)
            _descricaocurso = value
            If value <> "" Then
                tbx = tabx.conectar("select * from tbcursos where descricao = '" & value & "' and ativo = true")
                If tbx.Rows.Count > 0 Then
                    _nrseqcurso = tbx.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Cargahorariacurso As String
        Get
            Return _cargahorariacurso
        End Get
        Set(value As String)
            _cargahorariacurso = tratatexto(value)
        End Set
    End Property

    Public Property Descricaocargo As String
        Get
            Return _descricaocargo
        End Get
        Set(value As String)
            _descricaocargo = value
            If value <> "" AndAlso _nrseqcargo = 0 Then
                tbx = tabx.conectar("select * from tbcargos where descricao = '" & _descricaocargo & "' and ativo = true")
                If tbx.Rows.Count > 0 Then
                    _nrseqcargo = tbx.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Listacursos As DataTable
        Get
            Return _listacursos
        End Get
        Set(value As DataTable)
            _listacursos = value
        End Set
    End Property
End Class
Partial Public Class clscursos_cargos
    Public Function carregar() As Boolean

        Try
            If _nrseq = 0 Then
                _mensagemerro = "Selecione um curso válido !"
                Return False
            End If


            tb1 = tab1.conectar("select * from vwcargos_cursos where nrseqcurso = " & _nrseqcurso & " and nrseqcargo = " & _nrseqcargo & " and ativo = true")
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "O curso não existe !"
                Return False
            End If

            With tb1
                _dtcad = .Rows(0)("dtcad").ToString
                _usercad = .Rows(0)("usercad").ToString
                _nrseq = .Rows(0)("nrseq").ToString
                _nrseqcargo = .Rows(0)("nrseqcargo").ToString
                _nrseqcurso = .Rows(0)("nrseqcurso").ToString
                _cargahoraria = .Rows(0)("cargahoraria").ToString
                _ativo = .Rows(0)("ativo").ToString
                _descricaocargo = .Rows(0)("descricaocargo").ToString
                _descricaocurso = .Rows(0)("descricaocurso").ToString


            End With
            Return True
        Catch exsalavar As Exception
            Return False
        End Try
    End Function
    Public Function salvar() As Boolean

        Try
            If _nrseqcurso = 0 Then
                _mensagemerro = "Selecione um curso válido !"
                Return False
            End If
            If _nrseqcargo = 0 Then
                _mensagemerro = "Selecione um cargo válido !"
                Return False
            End If

            tb1 = tab1.conectar("select * from vwcargos_cursos where nrseqcurso = " & _nrseqcurso & " and nrseqcargo = " & _nrseqcargo & " and ativo = true")
            If tb1.Rows.Count > 0 Then
                _mensagemerro = "O curso já foi adicionado para o cargo !"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("insert into tbcargos_cursos (nrseqcurso, nrseqcargo, ativo, dtcad, usercad, cargahoraria) values (" & _nrseqcurso & "," & _nrseqcargo & ", true, '" & hoje() & "', '" & _usercad & "','" & _cargahoraria & "')")

            carregarlista()
            Return True
        Catch exsalavar As Exception
            Return False
        End Try
    End Function
    Public Function carregarlista() As Boolean
        Try

            _listacursos = tabx.conectar("select * from vwcargos_cursos where nrseqcargo = " & _nrseqcargo & " and ativo = true order by descricaocurso")
            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _mensagemerro = "Selecione um curso válido !"
                Return False
            End If




            tb1 = tab1.IncluirAlterarDados("update tbcargos_cursos set ativo = false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "' where nrseq = " & _nrseq)

            carregarlista()
            Return True
        Catch exsalavar As Exception
            Return False
        End Try
    End Function
End Class
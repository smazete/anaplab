﻿Imports System.Data
Imports System.Data.SqlClient

Public Class clsdbDataTable
    Private _strcon As String

    Private _banco As SqlConnection
    Private _comando As SqlCommand
    Private _adaptador As SqlDataAdapter
    Private _nometabela As String
    Private _tabela As New DataSet
    Private _sql As String
    Private _server As String
    Public Event CarregandoArquivo()
    Public Event SalvandoArquivo()

    'conexao com o servidor Gooday smartcode
    Public Sub New()
        'minha conexão servidor smartcode(teste)
        _tabela.Tables.Clear()



        Dim _banco As String = "dbtelesales"
        ' Dim _banco As String = "dbceva11112015"

        ' Dim _banco As String = "dbceva28092015_2"

        'Dim _banco As String = "dbceva15052015" _ultimo usado
        ' _server = "dbceva.db.11299532.hostedresource.com"
        'Dim _banco As String = "dbbkpceva20032015"

        '   Dim _banco As String = "dbcevabkp_02032015"
        ' Dim _banco As String = "dbceva15052015_original"
        ' Dim _banco As String = "dbceva101114"


        Dim _server As String = "dbtelesales.db.10852659.hostedresource.com"
        'Dim _banco As String = "testes"
        Dim _user As String = "dbtelesales"
        Dim _pass As String = "Smart734129@"

        _strcon = "server=" & _server.ToUpper & ";" & _
                      "Initial Catalog=" & _banco & ";" & _
                      "Persist Security Info=True;" & _
                      "User ID=" & _user & ";" & _
                      "Password=" & _pass & "; connect timeout=20000"
        limpartabela = Nothing
    End Sub


    'local e outros

    ''Public Sub New()
    ''    'minha conexão servidor smartcode(teste)
    ''    _tabela.Tables.Clear()



    ''    Dim _banco As String = "dbceva_11112015_2"
    ''    ' Dim _banco As String = "dbceva11112015"

    ''    ' Dim _banco As String = "dbceva28092015_2"

    ''    'Dim _banco As String = "dbceva15052015" _ultimo usado
    ''    ' _server = "dbceva.db.11299532.hostedresource.com"
    ''    'Dim _banco As String = "dbbkpceva20032015"

    ''    '   Dim _banco As String = "dbcevabkp_02032015"
    ''    ' Dim _banco As String = "dbceva15052015_original"
    ''    ' Dim _banco As String = "dbceva101114"


    ''    Dim _server As String = "localhost\SQLEXPRESS"
    ''    'Dim _banco As String = "testes"
    ''    Dim _user As String = "user_dbceva"
    ''    Dim _pass As String = "Smart734129@"

    ''    _strcon = "server=" & _server.ToUpper & ";" & _
    ''                  "Initial Catalog=" & _banco & ";" & _
    ''                  "Persist Security Info=True;" & _
    ''                  "User ID=" & _user & ";" & _
    ''                  "Password=" & _pass & "; connect timeout=20000"
    ''    limpartabela = Nothing
    ''End Sub

    'conexão com o servidor Web
    'primeiro servidor online, o banco esta no mundo clinica 
    'Public Sub New()
    '    'minha conexão servidor smartcode(teste)
    '    _tabela.Tables.Clear()
    '    _server = "dbceva.db.11299532.hostedresource.com"
    '    Dim _banco As String = "dbceva"

    '    'Dim _server As String = "localhost\SQLEXPRESS"
    '    'Dim _banco As String = "testes"
    '    Dim _user As String = "dbceva"
    '    Dim _pass As String = "Smart734129@"

    '    _strcon = "server=" & _server.ToUpper & ";" & _
    '                  "Initial Catalog=" & _banco & ";" & _
    '                  "Persist Security Info=True;" & _
    '                  "User ID=" & _user & ";" & _
    '                  "Password=" & _pass & "; connect timeout=20000"
    '    limpartabela = Nothing
    'End Sub
    ''conexão servidor ceva
    'Public Sub New()


    '    _server = "CGH2WSWEB001\SQLEXPRESS"

    '    Dim _banco As String = "dbceva"

    '    'Dim _server As String = "localhost\SQLEXPRESS"
    '    'Dim _banco As String = "testes"
    '    Dim _user As String = "user_dbceva"
    '    Dim _pass As String = "Smart734129@"

    '    _strcon = "server=" & _server.ToUpper & ";" & _
    '                  "Initial Catalog=" & _banco & ";" & _
    '                  "Persist Security Info=True;" & _
    '                  "User ID=" & _user & ";" & _
    '                  "Password=" & _pass & "; connect timeout=20000"
    '    limpartabela = Nothing
    'End Sub

    Public ReadOnly Property incluiralterardados() As DataTable
        Get
            _tabela.Tables.Clear()
            If _sql = "" Then
                Return Nothing
            Else
                If _tabela.Tables.Count > 0 Then
                    _tabela.Tables(0).Rows.Clear()
                    _tabela.Tables(0).Columns.Clear()
                End If

                _banco = New SqlConnection(_strcon)
                _comando = New SqlCommand(_sql, _banco)
                _adaptador = New SqlDataAdapter(_comando)
                _adaptador.Fill(_tabela)


            End If
        End Get
    End Property
    Public Property limpartabela() As String
        Get
            Return Nothing
        End Get
        Set(ByVal value As String)
            _tabela.Clear()

        End Set
    End Property
    Public Property ExecutaSql() As String
        Get
            Return _sql
        End Get
        Set(ByVal value As String)
            _sql = value

        End Set
    End Property

    Public ReadOnly Property conexao() As String
        Get
            Return _strcon
        End Get
    End Property
    Public ReadOnly Property conectar() As DataTable
        Get
            If _sql = "" Then
                _tabela.Tables.Clear()
                Return Nothing
            Else
                If _tabela.Tables.Count > 0 Then
                    _tabela.Tables(0).Rows.Clear()
                    _tabela.Tables(0).Columns.Clear()
                End If
                _banco = New SqlConnection(_strcon)
                _comando = New SqlCommand(_sql, _banco)
                _adaptador = New SqlDataAdapter(_comando)
                _adaptador.Fill(_tabela)
                NomedaTabela = _tabela.Tables(0).TableName

                Return _tabela.Tables(0)

            End If
        End Get
    End Property

    Public Property NomedaTabela() As String
        Get
            Return _nometabela
        End Get
        Set(ByVal value As String)
            _nometabela = value
        End Set
    End Property
 
    Public ReadOnly Property atualizar() As Boolean
        Get
            ' Try

            _adaptador.Update(_tabela, "tab_teste")
            Return True
            'Catch ex As Exception
            '    Return False
            'End Try

        End Get
    End Property


End Class

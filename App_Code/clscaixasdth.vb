﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clscaixasdth

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _table As Data.DataTable
    Dim tablename As String = "tbcaixasdth"

    Dim _contador As Integer
    Dim _mensagemerro As String

    Dim _userexclui As String
    Dim _dtexclui As DateTime

    Dim _nrseq As Integer = 0
    Dim _nrseqcaixa As Integer = 0
    Dim _nrseqplanoconta As Integer = 0
    Dim _nrseqempresa As Integer = 0

    Dim _hrcad As String
    Dim _planocontas As String
    Dim _descricao As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo As Boolean
    Dim _operacao As String
    Dim _valor As Decimal = 0
    Dim _nrseqdocumento As Integer = 0
    Dim _qtdparcelas As Integer = 0
    Dim _parcela As Integer = 0
    Dim _formapagto As String
    Dim _tipolancamento As String
    Dim _conteudo As String
    Dim _nrseqconsulta As Integer
    Dim _descricaodocumento As String
    Dim _descricaoplanocontas As String

    Public Sub New()
        _dtcad = hoje()
        _dtexclui = hoje()
        _userexclui = buscarsessoes("usuario")
        _usercad = buscarsessoes("usuario")
        _nrseqempresa = buscarsessoes("idempresaemuso")
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqcaixa As Integer
        Get
            Return _nrseqcaixa
        End Get
        Set(value As Integer)
            _nrseqcaixa = value
        End Set
    End Property

    Public Property Nrseqplanoconta As Integer
        Get
            Return _nrseqplanoconta
        End Get
        Set(value As Integer)
            _nrseqplanoconta = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Operacao As String
        Get
            Return _operacao
        End Get
        Set(value As String)
            _operacao = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _valor
        End Get
        Set(value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property Nrseqdocumento As Integer
        Get
            Return _nrseqdocumento
        End Get
        Set(value As Integer)
            _nrseqdocumento = value
        End Set
    End Property

    Public Property Qtdparcelas As Integer
        Get
            Return _qtdparcelas
        End Get
        Set(value As Integer)
            _qtdparcelas = value
        End Set
    End Property

    Public Property Formapagto As String
        Get
            Return _formapagto
        End Get
        Set(value As String)
            _formapagto = value
        End Set
    End Property

    Public Property Tipolancamento As String
        Get
            Return _tipolancamento
        End Get
        Set(value As String)
            _tipolancamento = value
        End Set
    End Property

    Public Property Conteudo As String
        Get
            Return _conteudo
        End Get
        Set(value As String)
            _conteudo = value
        End Set
    End Property

    Public Property Parcela As Integer
        Get
            Return _parcela
        End Get
        Set(value As Integer)
            _parcela = value
        End Set
    End Property

    Public Property Descricaodocumento As String
        Get
            Return _descricaodocumento
        End Get
        Set(value As String)
            _descricaodocumento = value
            If value <> "" Then
                tb1 = tab1.conectar("select * from tbdocumentos where descricao = '" & value & "' and ativo = true and nrseqempresa = " & Nrseqempresa)
                If tb1.Rows.Count > 0 Then
                    _nrseqdocumento = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Descricaoplanocontas As String
        Get
            Return _descricaoplanocontas
        End Get
        Set(value As String)
            _descricaoplanocontas = value
            If value <> "" Then
                tb1 = tab1.conectar("select * from tbplanocontas where descricao = '" & value & "' and ativo = true and nrseqempresa = " & Nrseqempresa)
                If tb1.Rows.Count > 0 Then
                    _nrseqplanoconta = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Hrcad As String
        Get
            Return _hrcad
        End Get
        Set(value As String)
            _hrcad = value
        End Set
    End Property

    Public Property Planocontas As String
        Get
            Return _planocontas
        End Get
        Set(value As String)
            _planocontas = value
            If value <> "" Then
                tb1 = tab1.conectar("select * from tbplanocontas where codigo = '" & value & "' and ativo = true and nrseqempresa = " & Nrseqempresa)
                If tb1.Rows.Count > 0 Then
                    _descricaoplanocontas = tb1.Rows(0)("descricao").ToString
                End If
            End If
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Nrseqconsulta As Integer
        Get
            Return _nrseqconsulta
        End Get
        Set(value As Integer)
            _nrseqconsulta = value
        End Set
    End Property
End Class

Partial Public Class clscaixasdth

    Public Function consultarlancamento(Optional ativo As Boolean = True) As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & tablename & " Where nrseq=" & _nrseq & " order by descricao")

            If tb1.Rows.Count > 0 Then

                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _nrseqcaixa = sonumeros(tb1.Rows(0)("nrseqcaixa").ToString)
                _nrseqplanoconta = sonumeros(tb1.Rows(0)("nrseqplanoconta").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _operacao = tb1.Rows(0)("operacao").ToString
                _valor = moeda(tb1.Rows(0)("valor").ToString)
                _nrseqdocumento = sonumeros(tb1.Rows(0)("nrseqdocumento").ToString)
                _qtdparcelas = sonumeros(tb1.Rows(0)("qtdparcelas").ToString)
                _parcela = sonumeros(tb1.Rows(0)("parcela").ToString)
                _formapagto = tb1.Rows(0)("formapagto").ToString
                _tipolancamento = tb1.Rows(0)("tipolancamento").ToString
                _conteudo = tb1.Rows(0)("conteudo").ToString
                _hrcad = tb1.Rows(0)("hrcad").ToString
                _nrseqconsulta = sonumeros(tb1.Rows(0)("nrseqconsulta").ToString)

                _contador = tb1.Rows.Count
                _table = tb1
            Else
                _mensagemerro = "Não possui nenhum lançamento"
            End If
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultar(Optional ativo As Boolean = True) As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & tablename & " Where ativo= " & ativo & " order by descricao")

            If tb1.Rows.Count > 0 Then

                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _nrseqcaixa = sonumeros(tb1.Rows(0)("nrseqcaixa").ToString)
                _nrseqplanoconta = sonumeros(tb1.Rows(0)("nrseqplanoconta").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _operacao = tb1.Rows(0)("operacao").ToString
                _valor = tb1.Rows(0)("valor").ToString
                _nrseqdocumento = sonumeros(tb1.Rows(0)("nrseqdocumento").ToString)
                _qtdparcelas = sonumeros(tb1.Rows(0)("qtdparcelas").ToString)
                _parcela = sonumeros(tb1.Rows(0)("parcela").ToString)
                _formapagto = tb1.Rows(0)("formapagto").ToString
                _tipolancamento = tb1.Rows(0)("tipolancamento").ToString
                _conteudo = tb1.Rows(0)("conteudo").ToString
                _hrcad = tb1.Rows(0)("hrcad").ToString
                _nrseqconsulta = sonumeros(tb1.Rows(0)("nrseqconsulta").ToString)

                _contador = tb1.Rows.Count
                _table = tb1
            Else
                _mensagemerro = "Não possui nenhum lançamento"
            End If
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function
    Public Function consultartodos(Optional ativo As Boolean = True, Optional descricao As String = "") As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & tablename & " Where ativo= " & ativo & IIf(descricao <> "", " and descricao like '" & descricao & "%'", "") & " order by descricao")

            If tb1.Rows.Count > 0 Then
                Contador = tb1.Rows.Count
                Table = tb1
            Else
                _mensagemerro = "Não possui nenhum lançamento"
            End If
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarcaixa(Optional ativo As Boolean = True) As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & tablename & " Where ativo= " & ativo & " and nrseqcaixa='" & _nrseqcaixa & "' order by descricao")

            If tb1.Rows.Count > 0 Then

                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _nrseqcaixa = sonumeros(tb1.Rows(0)("nrseqcaixa").ToString)
                _nrseqplanoconta = sonumeros(tb1.Rows(0)("nrseqplanoconta").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _operacao = tb1.Rows(0)("operacao").ToString
                _valor = tb1.Rows(0)("valor").ToString
                _nrseqdocumento = sonumeros(tb1.Rows(0)("nrseqdocumento").ToString)
                _qtdparcelas = sonumeros(tb1.Rows(0)("qtdparcelas").ToString)
                _parcela = sonumeros(tb1.Rows(0)("parcela").ToString)
                _formapagto = tb1.Rows(0)("formapagto").ToString
                _tipolancamento = tb1.Rows(0)("tipolancamento").ToString
                _conteudo = tb1.Rows(0)("conteudo").ToString
                _hrcad = tb1.Rows(0)("hrcad").ToString
                _nrseqconsulta = sonumeros(tb1.Rows(0)("nrseqconsulta").ToString)

                _contador = tb1.Rows.Count
                _table = tb1
            Else
                _mensagemerro = "Não possui nenhum lançamento"
            End If
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, ativo, dtcad, usercad) values ('" & wcnrseqctrl & "',false,'" & hoje() & "','" & _usercad & "')")

            tb1 = tab1.conectar("select * from " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try
            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo=true, nrseqcaixa='" & sonumeros(_nrseqcaixa) & "',nrseqplanoconta='" & sonumeros(_nrseqplanoconta) & "',descricao='" & _descricao & "',operacao='" & _operacao & "',valor='" & moeda(_valor) & "',nrseqdocumento='" & sonumeros(_nrseqdocumento) & "',qtdparcelas='" & sonumeros(_qtdparcelas) & "',parcela='" & sonumeros(_parcela) & "',formapagto='" & _formapagto & "',tipolancamento='" & _tipolancamento & "',conteudo='" & _conteudo & "',hrcad='" & _hrcad & "',nrseqconsulta='" & sonumeros(_nrseqconsulta) & "' where nrseq= " & _nrseq)
            consultar()
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _mensagemerro = "Nenhum plano de consulta selecionado."
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo = " & IIf(_ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Userexclui & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.ToString
            Return False

        End Try
    End Function

End Class
﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.IO

Public Class clsconfig_padroes

#Region "emailspadroes"
    Dim _remetente As String
    Dim _destinatarios As New List(Of String)
    Dim _anexos As New List(Of String)
    Dim _assuntoemail As String
    Dim _ocultos As New List(Of String)
    Dim _textoemail As String
    Dim _textotratado As String
#End Region

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _salvarem As String
    Dim _assunto As String
    Dim _padraoemail As String = ""
    Dim _nrseq As Integer = 0
    Dim _texto As String
    Dim _nome As String
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _arquivo As String
    Dim _novoagente As Boolean = False
    Dim _mensagemerro As String

    Dim _dadosdamensagemsite As New clscontatos
    Dim _dadosrecuperarsenha As New clsAssociado


    Public Sub New()
        _usercad = buscarsessoes("usuario")
        _dtcad = data()
    End Sub

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Texto As String
        Get
            Return _texto
        End Get
        Set(value As String)
            _texto = value
            tratatextoemail()
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = tratatexto(value)
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Novoagente As Boolean
        Get
            Return _novoagente
        End Get
        Set(value As Boolean)
            _novoagente = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Salvarem As String
        Get
            Return _salvarem
        End Get
        Set(value As String)
            _salvarem = value
        End Set
    End Property

    Public Property Assunto As String
        Get
            Return _assunto
        End Get
        Set(value As String)
            _assunto = tratatexto(value)
        End Set
    End Property

    Public Property Remetente As String
        Get
            Return _remetente
        End Get
        Set(value As String)
            _remetente = tratatexto(value)
        End Set
    End Property



    Public Property Assuntoemail As String
        Get
            Return _assuntoemail
        End Get
        Set(value As String)
            _assuntoemail = tratatexto(value)
        End Set
    End Property

    Public Property Ocultos As List(Of String)
        Get
            Return _ocultos
        End Get
        Set(value As List(Of String))
            _ocultos = (value)
        End Set
    End Property

    Public Property Textoemail As String
        Get
            Return _textoemail
        End Get
        Set(value As String)
            _textoemail = tratatexto(value)
        End Set
    End Property

    Public Property Padraoemail As String
        Get
            Return _padraoemail
        End Get
        Set(value As String)
            _padraoemail = tratatexto(value, 145)
        End Set
    End Property

    Public Property Textotratado As String
        Get
            Return _textotratado
        End Get
        Set(value As String)
            _textotratado = value
        End Set
    End Property

    Public Property Destinatarios As List(Of String)
        Get
            Return _destinatarios
        End Get
        Set(value As List(Of String))
            _destinatarios = value
        End Set
    End Property

    Public Property Anexos As List(Of String)
        Get
            Return _anexos
        End Get
        Set(value As List(Of String))
            _anexos = value
        End Set
    End Property

    Public Property Dadosdamensagemsite As clscontatos
        Get
            Return _dadosdamensagemsite
        End Get
        Set(value As clscontatos)
            _dadosdamensagemsite = value
        End Set
    End Property

    Public Property Dadosrecuperarsenha As clsAssociado
        Get
            Return _dadosrecuperarsenha
        End Get
        Set(value As clsAssociado)
            _dadosrecuperarsenha = value
        End Set
    End Property
End Class

Partial Public Class clsconfig_padroes
    Public Function salvarpadroes() As Boolean

        Try

            If _nome = "" Then
                _mensagemerro = "Enter a model name valid  !"
                Return False
            End If
            If _texto = "" Then
                _mensagemerro = "Enter a valid text !"
                Return False
            End If

            Dim FilePath As String = HttpContext.Current.Server.MapPath("~\modelosemails\")

            _arquivo = HttpContext.Current.Server.MapPath("~\modelosemails\modelo.txt")
            _arquivo = alteranome(_arquivo)

            Dim lermensagem As New StreamWriter(_arquivo)
            lermensagem.WriteLine(_texto)
            lermensagem.Close()

            tb1 = tab1.conectar("select * from tbconfig_padroes where ativo = true and nome = '" & _nome & "'")
            If tb1.Rows.Count = 0 Then
                tb1 = tab1.IncluirAlterarDados("insert into tbconfig_padroes (nome, arquivo, ativo, dtcad, usercad, assunto, padraoemail) values ('" & _nome & "','" & mARQUIVO(_arquivo) & "', true, '" & hoje() & "','" & _usercad & "','" & _assunto & "', '" & _padraoemail & "')")
            Else
                tb1 = tab1.IncluirAlterarDados("update tbconfig_padroes set assunto = '" & _assunto & "' , arquivo = '" & mARQUIVO(_arquivo) & "', padraonovoagente = " & logico(_novoagente) & ", padraoemail = '" & _padraoemail & "' where ativo = true and nome = '" & _nome & "'")
            End If

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function listatags() As List(Of clsconfig_padroes_tags)
        Dim lista As New List(Of clsconfig_padroes_tags)

        lista.Add(New clsconfig_padroes_tags With {.Tag = "{logo} - Logo"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{username} - Usuário"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{password} - Senha"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{horaagenda} - Hora Agenda"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{dataagenda} - Data Agenda"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{tabelanotas} - Notas"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{fornecedor} - Fornecedor"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{tpoperacao} - Tipo Operação"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{turno} - Turno"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{codigoagenda} - Código Agendamento"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{nomemotorista} - Motorista"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{codigobarras} - Código de Barras da Chave de Acesso"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{dadosagendamento} - Dados do agendamento"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{dadosmotorista} - Dados do motorista"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{nrpager} - Número do Pager"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{dadosdescarga} - Dados de descarregamento"})
        lista.Add(New clsconfig_padroes_tags With {.Tag = "{ocorrencias} - Ocorrências"})




        Return lista

    End Function
    Public Function carregar() As Boolean

        Try

            tb1 = tab1.conectar("select * from tbconfig_padroes where nome = '" & _nome & "' and ativo = true")

            If tb1.Rows.Count = 0 Then
                _texto = ""
                _arquivo = ""
                _nrseq = 0
                _assunto = ""
                _padraoemail = ""
            Else
                If File.Exists(HttpContext.Current.Server.MapPath("~\modelosemails\") & tb1.Rows(0)("arquivo").ToString) Then
                    Dim ler As New StreamReader(HttpContext.Current.Server.MapPath("~\modelosemails\") & tb1.Rows(0)("arquivo").ToString)
                    _texto = ler.ReadToEnd
                    ler.Close()
                Else
                    _texto = "Arquivo não localizado !"
                End If
                _assunto = tb1.Rows(0)("assuntoemail").ToString
                _padraoemail = tb1.Rows(0)("padraoemail").ToString
                _arquivo = HttpContext.Current.Server.MapPath("~\modelosemails\") & tb1.Rows(0)("arquivo").ToString
                _nrseq = tb1.Rows(0)("nrseq").ToString
            End If


            Return True

        Catch ex As Exception
            _mensagemerro = ex.Message
            Return False
        End Try

    End Function

    Public Function apagar() As Boolean

        Try

            tb1 = tab1.conectar("select * from tbconfig_padroes where nome = '" & _nome & "' and ativo = true")

            If tb1.Rows.Count <> 0 Then

                tb1 = tab1.conectar("update tbconfig_padroes set ativo = false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "' where nome = '" & _nome & "' and ativo = true")



            End If


            Return True

        Catch ex As Exception
            _mensagemerro = ex.Message
            Return False
        End Try

    End Function


    Public Function emailrecuperarsenha() As Boolean
        Try
            If _nome = "" Then
                _mensagemerro = "Selecione um texto padrão para enviar o email !"
                Return False
            End If
            carregar()
            Dim xconfig As New clsconfig
            xconfig.carregar()
novoteste:
            Dim email As New clsEnvioEmail("usuario")

            If _padraoemail = "" Then
                email.ConfigPorta = 587
                email.ConfigSMTP = "smtp.anaplab.com.br"
                email.Credenciais("atendimento@anaplab.com.br", "Bdp-5j78", "smtp.anaplab.com.br")
                email.AdicionaRemetente = "atendimento@anaplab.com.br"
            Else
                Dim xpadraoemail As New clscontasemails
                xpadraoemail.Usuario = _padraoemail
                If Not xpadraoemail.carregar Then
                    _padraoemail = ""
                    GoTo novoteste
                End If

                email.ConfigPorta = xpadraoemail.Porta
                email.ConfigSMTP = xpadraoemail.Servidor
                email.Credenciais(xpadraoemail.Usuario, xpadraoemail.Senha, xpadraoemail.Servidor)
                email.AdicionaRemetente = xpadraoemail.Usuario


            End If

            email.EhHTML = True

            For x As Integer = 0 To Anexos.Count - 1

                email.AdicionaAnexos = Anexos(x)
            Next

            For x As Integer = 0 To Destinatarios.Count - 1

                email.AdicionaDestinatarios = Destinatarios(x)
            Next

            For x As Integer = 0 To Ocultos.Count - 1

                email.AdicionaDestinatariosocultos = Ocultos(x)
            Next



            email.AdicionaAssunto = "ANAPLAB - AUTOATENDIMENTO - Senha de acesso"
            email.AdicionaMensagem = "<p>Prezado(a) colega  " & Dadosrecuperarsenha.Nome & " </p></br><p> Sua senha de acesso ao AUTOATENDIMENTO é " & Dadosrecuperarsenha.Senha & "</p><p> Atenciosamente.</p><p> EQUIPE ANAPLAB</p><p> Data: " & hoje() & " </p>"

            '   email.AdicionaAssunto = _assuntoemail
            '   email.AdicionaMensagem = _textoemail.Replace("{logo}", "<img src=""" & xconfig.Logoweb & """ Width=""60px"" Height=""60px""><br>").Replace("{logosistema}", "<img src=""" & xconfig.Logosistema & """ Width=""27px"" Height=""27px"">").Replace("{mensagem}", Dadosdamensagemsite.Mensagem).Replace("{remetente}", Dadosdamensagemsite.Nomecompleto) 

            ' email.AdicionaMensagem = _textotratado.Replace("{mensagem}", Dadosdamensagemsite.Mensagem).Replace("{remetente}", Dadosdamensagemsite.Nomecompleto).Replace("{datadamensagem}", hoje()).Replace("{email}", Dadosdamensagemsite.Email)



            If email.EnviarEmail() Then
                _mensagemerro = "E-mail Enviado !"
                Return True
            Else
                _mensagemerro = email.mensagemerro
                Return False
            End If
        Catch exenvio As Exception
            _mensagemerro = exenvio.Message
            Return False
        End Try

    End Function

    Public Function emailparanovoassociado() As Boolean
        Try
            If _nome = "" Then
                _mensagemerro = "Selecione um texto padrão para enviar o email !"
                Return False
            End If
            carregar()
            Dim xconfig As New clsconfig
            xconfig.carregar()
novoteste:
            Dim email As New clsEnvioEmail("usuario")

            If _padraoemail = "" Then
                email.ConfigPorta = 587
                email.ConfigSMTP = "smtp.anaplab.com.br"
                email.Credenciais("atendimento@anaplab.com.br", "Bdp-5j78", "smtp.anaplab.com.br")
                email.AdicionaRemetente = "atendimento@anaplab.com.br"
            Else
                Dim xpadraoemail As New clscontasemails
                xpadraoemail.Usuario = _padraoemail
                If Not xpadraoemail.carregar Then
                    _padraoemail = ""
                    GoTo novoteste
                End If

                email.ConfigPorta = xpadraoemail.Porta
                email.ConfigSMTP = xpadraoemail.Servidor
                email.Credenciais(xpadraoemail.Usuario, xpadraoemail.Senha, xpadraoemail.Servidor)
                email.AdicionaRemetente = xpadraoemail.Usuario


            End If

            email.EhHTML = True

            For x As Integer = 0 To Anexos.Count - 1

                email.AdicionaAnexos = Anexos(x)
            Next

            For x As Integer = 0 To Destinatarios.Count - 1

                email.AdicionaDestinatarios = Destinatarios(x)
            Next

            For x As Integer = 0 To Ocultos.Count - 1

                email.AdicionaDestinatariosocultos = Ocultos(x)
            Next



            email.AdicionaAssunto = "ANAPLAB - AUTOATENDIMENTO - Senha de acesso"
            email.AdicionaMensagem = "<p>Prezado(a) colega  " & Dadosrecuperarsenha.Nome & " </p></br><p> Sua cadastro foi recebido com sucesso.</p></br><p> Sentimo-nos honrados ao acolher o seu pedido de filiação e faremos tudo que estiver ao nosso alcance para atender as suas demandas judiciais, que também são as nossas.</p><p> Seu acesso ao autoatendimento está liberado e pode ser feito pelo menu AUTOATENDIMENTO, na página inicial do site da ANAPLAB (anaplab.com.br) utilizando sua matrícula (sem pontos, sem traço e com DV) e a senha cadastrada.</p><p> Em até 2 (dois) dias úteis, nosso Departamento de Tesouraria lhe enviará a CONFIRMAÇÃO DE SUA FILIAÇÃO, juntamente com as instruções para a adesão à sistemática de TRANSFERÊNCIAS AUTOMÁTICAS MENSAIS de suas mensalidades perante a sua instituição financeira.</p><p> Utilize nossos CANAIS DE ATENDIMENTO para emitir sugestões e/ou críticas. Sua participação é muito importante. Sinta-se em casa. Ajude-nos a construir uma Associação voltada para os interesses dos associados.</p></br><p> Atenciosamente.</p></br><p> Ari Zanella</p><p> Presidente Administrativo.</p>"

            '   email.AdicionaAssunto = _assuntoemail
            '   email.AdicionaMensagem = _textoemail.Replace("{logo}", "<img src=""" & xconfig.Logoweb & """ Width=""60px"" Height=""60px""><br>").Replace("{logosistema}", "<img src=""" & xconfig.Logosistema & """ Width=""27px"" Height=""27px"">").Replace("{mensagem}", Dadosdamensagemsite.Mensagem).Replace("{remetente}", Dadosdamensagemsite.Nomecompleto) 

            ' email.AdicionaMensagem = _textotratado.Replace("{mensagem}", Dadosdamensagemsite.Mensagem).Replace("{remetente}", Dadosdamensagemsite.Nomecompleto).Replace("{datadamensagem}", hoje()).Replace("{email}", Dadosdamensagemsite.Email)

            If email.EnviarEmail() Then
                _mensagemerro = "E-mail Enviado !"
                Return True
            Else
                _mensagemerro = email.mensagemerro
                Return False
            End If
        Catch exenvio As Exception
            _mensagemerro = exenvio.Message
            Return False
        End Try

    End Function


    Public Function envionovoassociadoautoatendimento() As Boolean
        Try
            If _nome = "" Then
                _mensagemerro = "Selecione um texto padrão para enviar o email !"
                Return False
            End If
            carregar()
            Dim xconfig As New clsconfig
            xconfig.carregar()
novoteste:
            Dim email As New clsEnvioEmail("usuario")

            If _padraoemail = "" Then
                email.ConfigPorta = 587
                email.ConfigSMTP = "smtp.anaplab.com.br"
                email.Credenciais("atendimento@anaplab.com.br", "Bdp-5j78", "smtp.anaplab.com.br")
                email.AdicionaRemetente = "atendimento@anaplab.com.br"
            Else
                Dim xpadraoemail As New clscontasemails
                xpadraoemail.Usuario = _padraoemail
                If Not xpadraoemail.carregar Then
                    _padraoemail = ""
                    GoTo novoteste
                End If

                email.ConfigPorta = xpadraoemail.Porta
                email.ConfigSMTP = xpadraoemail.Servidor
                email.Credenciais(xpadraoemail.Usuario, xpadraoemail.Senha, xpadraoemail.Servidor)
                email.AdicionaRemetente = xpadraoemail.Usuario


            End If

            email.EhHTML = True

            For x As Integer = 0 To Anexos.Count - 1

                email.AdicionaAnexos = Anexos(x)
            Next

            For x As Integer = 0 To Destinatarios.Count - 1

                email.AdicionaDestinatarios = Destinatarios(x)
            Next

            For x As Integer = 0 To Ocultos.Count - 1

                email.AdicionaDestinatariosocultos = Ocultos(x)
            Next



            email.AdicionaAssunto = "ANAPLAB - AUTOATENDIMENTO - Senha de acesso"
            email.AdicionaMensagem = "<p>Dados do cadastro</p><p>Matricula: " & Dadosrecuperarsenha.Matricula & "</p></br><p>Situação: " & Dadosrecuperarsenha.Situacaopb & "</p><p>Nome: " & Dadosrecuperarsenha.Nome & "</p><p>Mãe: " & Dadosrecuperarsenha.Nomemae & "</p><p>Sexo: " & Dadosrecuperarsenha.Sexo & "</p></br><p>Nascimento: " & Dadosrecuperarsenha.Dtnasc & "</p><p>CPF: " & Dadosrecuperarsenha.Cpf & "</p><p>RG: " & Dadosrecuperarsenha.Rg & "</p></br><p>CEP: " & Dadosrecuperarsenha.Cep & "</p></br><p>Endereço: " & Dadosrecuperarsenha.Endereco & "," & Dadosrecuperarsenha.Numero & "," & Dadosrecuperarsenha.Complemento & "</p><p>Cidade " & Dadosrecuperarsenha.Cidade & "," & Dadosrecuperarsenha.Bairro & " - " & Dadosrecuperarsenha.Estado & " </p></br><p>telefone: " & Dadosrecuperarsenha.Telefone & "</p><p>Celular: " & Dadosrecuperarsenha.Celular & "</p></br><p>email: " & Dadosrecuperarsenha.Email & "</p><p>Posse: " & Dadosrecuperarsenha.Dtposse & "</p><p>Aposentadoria: " & Dadosrecuperarsenha.Dtaposentadoria & "</p></br><p>Senha: " & Dadosrecuperarsenha.Senha & "</p><p>Agencia: " & Dadosrecuperarsenha.Agencia & "</p><p>Conta: " & Dadosrecuperarsenha.Contacorrente & "</p>"

            '   email.AdicionaAssunto = _assuntoemail
            '   email.AdicionaMensagem = _textoemail.Replace("{logo}", "<img src=""" & xconfig.Logoweb & """ Width=""60px"" Height=""60px""><br>").Replace("{logosistema}", "<img src=""" & xconfig.Logosistema & """ Width=""27px"" Height=""27px"">").Replace("{mensagem}", Dadosdamensagemsite.Mensagem).Replace("{remetente}", Dadosdamensagemsite.Nomecompleto) 

            ' email.AdicionaMensagem = _textotratado.Replace("{mensagem}", Dadosdamensagemsite.Mensagem).Replace("{remetente}", Dadosdamensagemsite.Nomecompleto).Replace("{datadamensagem}", hoje()).Replace("{email}", Dadosdamensagemsite.Email)

            If email.EnviarEmail() Then
                _mensagemerro = "E-mail Enviado !"
                Return True
            Else
                _mensagemerro = email.mensagemerro
                Return False
            End If
        Catch exenvio As Exception
            _mensagemerro = exenvio.Message
            Return False
        End Try

    End Function


    Public Function enviaremail() As Boolean
        Try
            If _nome = "" Then
                _mensagemerro = "Selecione um texto padrão para enviar o email !"
                Return False
            End If
            carregar()
            Dim xconfig As New clsconfig
            xconfig.carregar()
novoteste:
            Dim email As New clsEnvioEmail("usuario")

            If _padraoemail = "" Then
                email.ConfigPorta = 587
                email.ConfigSMTP = "smtp.anaplab.com.br"
                email.Credenciais("atendimento@anaplab.com.br", "Bdp-5j78", "smtp.anaplab.com.br")
                email.AdicionaRemetente = "atendimento@anaplab.com.br"
            Else
                Dim xpadraoemail As New clscontasemails
                xpadraoemail.Usuario = _padraoemail
                If Not xpadraoemail.carregar Then
                    _padraoemail = ""
                    GoTo novoteste
                End If

                email.ConfigPorta = xpadraoemail.Porta
                email.ConfigSMTP = xpadraoemail.Servidor
                email.Credenciais(xpadraoemail.Usuario, xpadraoemail.Senha, xpadraoemail.Servidor)
                email.AdicionaRemetente = xpadraoemail.Usuario


            End If

            email.EhHTML = True

            For x As Integer = 0 To Anexos.Count - 1

                email.AdicionaAnexos = Anexos(x)
            Next

            For x As Integer = 0 To Destinatarios.Count - 1

                email.AdicionaDestinatarios = Destinatarios(x)
            Next

            For x As Integer = 0 To Ocultos.Count - 1

                email.AdicionaDestinatariosocultos = Ocultos(x)
            Next



            email.AdicionaAssunto = Dadosdamensagemsite.Assunto
            email.AdicionaMensagem = "<p>Contato Anaplab informa o contato do remetente " & Dadosdamensagemsite.Nomecompleto & " </p></br><p> Nome: " & Dadosdamensagemsite.Nomecompleto & "</p><p> E-mail: " & Dadosdamensagemsite.Email & "</p><p> Mensagem: " & Dadosdamensagemsite.Mensagem & "</p>"

            '   email.AdicionaAssunto = _assuntoemail
            '   email.AdicionaMensagem = _textoemail.Replace("{logo}", "<img src=""" & xconfig.Logoweb & """ Width=""60px"" Height=""60px""><br>").Replace("{logosistema}", "<img src=""" & xconfig.Logosistema & """ Width=""27px"" Height=""27px"">").Replace("{mensagem}", Dadosdamensagemsite.Mensagem).Replace("{remetente}", Dadosdamensagemsite.Nomecompleto) 

            '   email.AdicionaMensagem = _mensagem.Replace("{mensagem}", Dadosdamensagemsite.Mensagem).Replace("{remetente}", Dadosdamensagemsite.Nomecompleto).Replace("{datadamensagem}", hoje()).Replace("{email}", Dadosdamensagemsite.Email)



            If email.EnviarEmail() Then
                _mensagemerro = "E-mail Enviado !"
                Return True
            Else
                _mensagemerro = email.mensagemerro
                Return False
            End If
        Catch exenvio As Exception
            _mensagemerro = exenvio.Message
            Return False
        End Try

    End Function

    Public Function tratatextoemail() As Boolean
        Try
            Dim xconfig As New clsconfig
            xconfig.carregar()



            _textotratado = _texto.Replace("{logo}", "<img src=""" & xconfig.Logoweb & """ >").Replace("{logosistema}", "<img src=""" & xconfig.Logoweb & """ >")

            Return True
        Catch extrataemail As Exception

            Return False
        End Try
    End Function
End Class
﻿Imports Microsoft.VisualBasic
Imports System.DateTime
Imports MySql.Data.MySqlClient '.MySqlConnection
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Data
Imports System.Windows.Forms
Imports System.IO
'Imports System.Security
'Imports System.Security.Permissions
Imports System.Reflection
Imports System.Runtime.CompilerServices


' This strong name key is used to create a code group that gives permissions to this assembly.




' The AllowPartiallyTrustedCallersAttribute requires the assembly to be signed with a strong name key.
' This attribute is necessary since the control is called by either an intranet or Internet
' Web page that should be running under restricted permissions.

'<Assembly: AssemblyVersion("6.3.6.0")> 
'' The AllowPartiallyTrustedCallersAttribute requires the assembly to be signed with a strong name key.
'' This attribute is necessary since the control is called by either an intranet or Internet
'' Web page that should be running under restricted permissions.

'<Assembly: AllowPartiallyTrustedCallers()> 



' The userControl1 displays an OpenFileDialog box, then displays a text box containing the name of 
' the file selected and a list box that displays the contents of the file.  The selected file must 
' contain text in order for the control to display the data properly.

'Demand the zone requirement for the calling application.

Public Class clsBancocep

    Private _strcon As String
    Private _banco As MySql.Data.MySqlClient.MySqlConnection
    Private _comando As MySql.Data.MySqlClient.MySqlCommand
    Private _adaptador As MySql.Data.MySqlClient.MySqlDataAdapter
    Private _nometabela As String
    Private _tabela As New Data.DataSet
    Private _sql As String
    Private nrseqCarregar As Integer

    Public Sub New()

        Dim _server As String = "https://p3nmssqladmin.secureserver.net/18"


        'Dim _server As String = "localhost\SQLEXPRESS"
        'Dim _banco As String = "testes"
        Dim _user As String = "dbceva"
        Dim _pass As String = "Smart734129@"
        'Dim _user As String = "usrsmart"
        'Dim _pass As String = "734129"
        '_strcon = "server=dbmy0042.whservidor.com;" & _
        '                                    "Database=minhaclini;" & _
        '                                    "PWD=" + _pass + ";" & _
        '                                    "UID=" + _user + ";" & "PORT = 3306;allow zero datetime=true"
        _strcon = "server=https://p3nmssqladmin.secureserver.net/18;" & _
                                            "Database=dbceva;" & _
                                            "PWD=" + _pass + ";" & _
                                            "UID=" + _user + ";" & "PORT = 3306;allow zero datetime=true"
        limpartabela = Nothing
    End Sub
    Public Property limpartabela() As String
        Get
            Return Nothing
        End Get
        Set(ByVal value As String)
            _tabela.Clear()

        End Set
    End Property
    Public Property ExecutaSql() As String
        Get
            Return _sql
        End Get
        Set(ByVal value As String)
            _sql = value

        End Set
    End Property

    Public ReadOnly Property conexao() As String
        Get
            Return _strcon
        End Get
    End Property



    'Public Function conectar() As Data.DataTable
    '     If _sql = "" Then
    '          Return Nothing
    '     Else

    '          _banco = New MySql.Data.MySqlClient.MySqlConnection(_strcon)
    '          _comando = New MySql.Data.MySqlClient.MySqlCommand(_sql, _banco)
    '          _adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter(_comando)

    '          _adaptador.Fill(_tabela)
    '          NomedaTabela = _tabela.Tables(0).TableName
    '          Return _tabela.Tables(0)

    '     End If

    'End Function

    Public ReadOnly Property conectar() As Data.DataTable
        Get
            If _sql = "" Then
                Return Nothing
            Else

                _banco = New MySql.Data.MySqlClient.MySqlConnection(_strcon)
                _comando = New MySql.Data.MySqlClient.MySqlCommand(_sql, _banco)
                _adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter(_comando)
                'rs.Open(_sql, _strcon)
                _adaptador.Fill(_tabela)
                If _sql.Substring(0, 1).ToUpper = "S" Then
                    NomedaTabela = _tabela.Tables(0).TableName
                    Return _tabela.Tables(0)
                End If
            End If
        End Get

    End Property



    Public ReadOnly Property incluiralterardados() As Data.DataTable
        Get
            If _sql = "" Then
                Return Nothing
            Else

                _banco = New MySql.Data.MySqlClient.MySqlConnection(_strcon)
                _comando = New MySql.Data.MySqlClient.MySqlCommand(_sql, _banco)
                _adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter(_comando)
                'rs.Open(_sql, _strcon)
                _adaptador.Fill(_tabela)
                If _sql.Substring(0, 1).ToUpper = "S" Then
                    NomedaTabela = _tabela.Tables(0).TableName

                End If
            End If
        End Get

    End Property

    'Public Function conectar() As Data.DataTable
    '    If _sql = "" Then
    '        Return Nothing
    '    Else

    '        _banco = New MySql.Data.MySqlClient.MySqlConnection(_strcon)
    '        _comando = New MySql.Data.MySqlClient.MySqlCommand(_sql, _banco)
    '        _adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter(_comando)

    '        _adaptador.Fill(_tabela)
    '        NomedaTabela = _tabela.Tables(0).TableName
    '        Return _tabela.Tables(0)

    '    End If

    'End Function
    Public Property NomedaTabela() As String
        Get
            Return _nometabela
        End Get
        Set(ByVal value As String)
            _nometabela = value
        End Set
    End Property
    'Public ReadOnly Property ImportaXml() As DataTable
    '    Get
    '        _abrir.FileName = ""
    '        _abrir.Filter = "*.xml|*.xml"
    '        _abrir.ShowDialog()
    '        If _abrir.FileName = "" Then
    '            Return Nothing
    '        Else
    '            Try
    '                RaiseEvent CarregandoArquivo()
    '                _tabela.ReadXml(_abrir.FileName)
    '                NomedaTabela = _abrir.FileName.Replace(".xml", "")
    '                Return _tabela.Tables(0)

    '            Catch ex As Exception
    '                Return Nothing
    '            End Try

    '        End If
    '    End Get
    'End Property
    'Public Function ExportaXml(ByVal campo As String) As Boolean

    '    If campo = "" Then
    '        _Salvar.FileName = ""
    '        _Salvar.Filter = "*.xml|*.xml"
    '        _Salvar.ShowDialog()
    '    Else
    '        _Salvar.FileName = campo
    '    End If
    '    If _Salvar.FileName = "" Then
    '        Return Nothing
    '    Else
    '        ' Try
    '        RaiseEvent SalvandoArquivo()
    '        conectar.WriteXml(_Salvar.FileName)
    '        NomedaTabela = _Salvar.FileName.Replace(".xml", "")
    '        Return True
    '        'Catch ex As Exception
    '        ' Return False
    '        ' End Try

    '    End If
    'End Function
    Public ReadOnly Property atualizar() As Boolean
        Get
            ' Try

            _adaptador.Update(_tabela, "tab_teste")
            Return True
            'Catch ex As Exception
            '    Return False
            'End Try

        End Get
    End Property
    'Public Function abrir(ByVal sqlquery As String)
    '    Dim teste As MySql.Data.MySqlClient.MySqlConnection = New MySql.Data.MySqlClient.MySqlConnection





    '    Dim acess_usr As String = "usrAureo"
    '    Dim acess_pwd As String = "aureo"
    '    'acess_usr = "usrsmartcode"
    '    'acess_pwd = "734129"
    '    Dim strPort As String = "3306"

    '    If sqlquery.ToUpper = "FECHAR" Then
    '        teste.Close()
    '        Exit Function
    '    End If



    '    If teste.State = 0 Then

    '        teste.ConnectionString = "server=www.softclinica.net;" & _
    '                                        "Database=softclinicanet;" & _
    '                                        "PWD=" + acess_pwd + ";" & _
    '                                        "UID=" + acess_usr + ";" & "PORT = " & strPort


    '        teste.Open()

    '    End If

    '    Dim resultado As New MySql.Data.MySqlClient.MySqlDataAdapter(sqlquery, teste)
    '    Dim daset As New Data.DataSet

    '    Dim da As New Data.DataTable
    '    resultado.Fill(da)
    '    Return da
    'End Function 
End Class

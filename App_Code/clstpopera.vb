﻿Imports Microsoft.VisualBasic
Imports clsSmart
Public Class clstpopera


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _descricao As String = ""
    Dim _ativo As Boolean = True
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String = ""
    Dim _tpnota As String = ""
    Dim _listaitens As New List(Of clstpopera)
    Dim _listatiposnotas As New List(Of String)
    Public Sub New()
        _usercad = HttpContext.Current.Session("usuario")
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Listaitens As List(Of clstpopera)
        Get
            Return _listaitens
        End Get
        Set(value As List(Of clstpopera))
            _listaitens = value
        End Set
    End Property

    Public Property Listatiposnotas As List(Of String)
        Get
            Return _listatiposnotas
        End Get
        Set(value As List(Of String))
            _listatiposnotas = value
        End Set
    End Property

    Public Property Tpnota As String
        Get
            Return _tpnota
        End Get
        Set(value As String)
            _tpnota = value
        End Set
    End Property
End Class
Partial Public Class clstpopera
    Public Function listar() As Boolean
        Try
            Listaitens.Clear()
            tb1 = tab1.conectar("select * from tbtpopera where ativo = true order by descricao")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaitens.Add(New clstpopera With {.Descricao = tb1.Rows(x)("descricao").ToString, .Nrseq = tb1.Rows(x)("nrseq").ToString, .Tpnota = tb1.Rows(x)("tpnota").ToString})
            Next
            Listatiposnotas.Clear()
            Listatiposnotas.Add("")
            tb1 = tab1.conectar("select distinct routingid1 from tbnotas_itens order by routingid1")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listatiposnotas.Add(tb1.Rows(x)("routingid1").ToString)
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If

            If Not procurar() Then
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbtpopera set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception

        End Try
    End Function
    Public Function procurar(Optional xpordescricao As Boolean = False) As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            If Not xpordescricao Then
                tb1 = tab1.conectar("select * from tbtpopera where nrseq = " & Nrseq)
            Else
                tb1 = tab1.conectar("select * from tbtpopera where descricao = '" & _descricao & "' and ativo = true ")
            End If
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Descricao = tb1.Rows(0)("descricao").ToString
            Tpnota = tb1.Rows(0)("tpnota").ToString
            Ativo = logico(tb1.Rows(0)("ativo").ToString)
            Usercad = tb1.Rows(0)("usercad").ToString
            Dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
            Return True

        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function salvar() As Boolean
        Try




            Dim xsql As String

            xsql = "select * from tbtpopera where descricao = '" & _descricao & "'"
            tb1 = tab1.conectar(xsql)
            If tb1.Rows.Count = 0 Then
                tb1 = tab1.IncluirAlterarDados("insert into tbtpopera (descricao, usercad, dtcad, ativo, tpnota) values ('" & _descricao & "', '" & _usercad & "', '" & hoje() & "', true, '" & _tpnota & "')")
                Return True
            End If
            _nrseq = tb1.Rows(0)("nrseq").ToString
            xsql = "update tbtpopera Set ativo = True"

            If _descricao <> "" Then
                xsql &= ",descricao = '" & _descricao & "'"
            End If
            If _tpnota <> "" Then
                xsql &= ",tpnota = '" & _tpnota & "'"
            End If

            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole("")
            tb1 = tab1.IncluirAlterarDados("insert into tbtpopera (ativo, dtcad, usercad, nrseqctrl) values (false, '" & hoje() & "', '" & _usercad & "', '" & wcnrseqctrl & "')")
            tb1 = tab1.conectar("Select * from  tbtpopera where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class
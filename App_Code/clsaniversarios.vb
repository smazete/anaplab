﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data
Public Class clsaniversarios

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _table As Data.DataTable
    Dim _contador As Integer = 0
    Dim _tablename As String = "vwaniversariosclientes"
    Dim _dtdependentes As DateTime
    Dim _dtmedicos As DateTime
    Dim _dtclientes As DateTime
    Dim _mensagemerro As String
    Dim _nrseqdependentes As String
    Dim _nrseqmedicos As String
    Dim _nrseqclientes As String
    Dim _mesesdiferenca As Integer = 0
    Dim _rangeinit As DateTime
    Dim _rangefinal As DateTime
    Dim _sqltext As String
    Dim _nomeempresa As String

    Public Property Dtdependentes As Date
        Get
            Return _dtdependentes
        End Get
        Set(value As Date)
            _dtdependentes = value
        End Set
    End Property

    Public Property Dtmedicos As Date
        Get
            Return _dtmedicos
        End Get
        Set(value As Date)
            _dtmedicos = value
        End Set
    End Property

    Public Property Dtclientes As Date
        Get
            Return _dtclientes
        End Get
        Set(value As Date)
            _dtclientes = value
        End Set
    End Property

    Public Property Nrseqdependentes As String
        Get
            Return _nrseqdependentes
        End Get
        Set(value As String)
            _nrseqdependentes = value
        End Set
    End Property

    Public Property Nrseqmedicos As String
        Get
            Return _nrseqmedicos
        End Get
        Set(value As String)
            _nrseqmedicos = value
        End Set
    End Property

    Public Property Nrseqclientes As String
        Get
            Return _nrseqclientes
        End Get
        Set(value As String)
            _nrseqclientes = value
        End Set
    End Property

    Public Property Nomeempresa As String
        Get
            Return _nomeempresa
        End Get
        Set(value As String)
            _nomeempresa = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Mesesdiferenca As Integer
        Get
            Return _mesesdiferenca
        End Get
        Set(value As Integer)
            _mesesdiferenca = value
        End Set
    End Property

    Public Property Rangeinit As Date
        Get
            Return _rangeinit
        End Get
        Set(value As Date)
            _rangeinit = value
        End Set
    End Property

    Public Property Rangefinal As Date
        Get
            Return _rangefinal
        End Get
        Set(value As Date)
            _rangefinal = value
        End Set
    End Property

    Public Property Sqltext As String
        Get
            Return _sqltext
        End Get
        Set(value As String)
            _sqltext = value
        End Set
    End Property
End Class
Partial Public Class clsaniversarios



    Public Function carregaraniversariosclientesdependentes(Optional xnrseqmedico As Integer = 0, Optional aniversariorange As Boolean = False, Optional aniversariodomes As Boolean = False) As Boolean
        Try


            If aniversariodomes = True Then
                _sqltext = "SELECT nrseq, nome, dtnasc ,period_diff(month(dtnasc), month(now())) as mesesrestantes, period_diff(day(dtnasc), day(now())) as diasrestantes FROM  " & _tablename & "  where " & IIf(xnrseqmedico <> 0, "nrseqmedico='" & xnrseqmedico & "' and ", "") & " ativo=true and dtnasc is not null and sign(period_diff(month(dtnasc), month(now()))) =0 and SIGN(period_diff(day(dtnasc), day(now()))) >=0 order by dtnasc"
            ElseIf aniversariorange = True Then
                _sqltext = "SELECT nrseq,nome, dtnasc ,period_diff(month(dtnasc), month(now())) as mesesrestantes, period_diff(day(dtnasc), day(now())) as diasrestantes FROM  " & _tablename & "  where " & IIf(xnrseqmedico <> 0, "nrseqmedico='" & xnrseqmedico & "' and ", "") & " ativo=true and dtnasc between '" & valordatamysql(_rangeinit) & "' and '" & valordatamysql(_rangefinal) & "' and dtnasc is not null "
            Else
                _sqltext = "SELECT nrseq, nome,dtnasc ,period_diff(month(dtnasc), month(now())) as mesesrestantes, period_diff(day(dtnasc), day(now())) as diasrestantes FROM  " & _tablename & "  where " & IIf(xnrseqmedico <> 0, "nrseqmedico='" & xnrseqmedico & "' and ", "") & " ativo=true and dtnasc is not null and sign(period_diff(month(dtnasc), month(now()))) >=0 and period_diff(month(dtnasc), month(now()))<=2 and SIGN(period_diff(day(dtnasc), day(now()))) >=0 order by dtnasc"
            End If

            tb1 = tab1.conectar(_sqltext)

            'tb1 = tab1.conectar("SELECT distinct(nrseqclientes) as nrseq, dtclientes as data FROM  " & tablename & "  where " & IIf(xnrseq <> 0, "nrseqclientes='" & xnrseq & "' and ", "") & " dtclientes is not null order by dtclientes")
            If tb1.Rows.Count > 0 Then
                _dtclientes = valordata(tb1.Rows(0)("dtnasc").ToString)
                _nrseqclientes = numeros(tb1.Rows(0)("nrseq").ToString)
            End If

            _table = tb1
            _contador = tb1.Rows.Count
            Return True
        Catch exlistar As Exception
            _mensagemerro = exlistar.Message
            Return False
        End Try

    End Function



End Class

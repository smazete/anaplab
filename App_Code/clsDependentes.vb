﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data
Public Class clsdependentes

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbdependentes"

    Dim _nrseq As Integer = 0
    Dim _nome As String
    Dim _endereco As String
    Dim _telresidencial As String
    Dim _telcelular As String
    Dim _cpf As String
    Dim _dtnasc As DateTime
    Dim _associado As Integer = 0
    Dim _matricula As String
    Dim _maquina As String
    Dim _numero As String
    Dim _complemento As String
    Dim _bairro As String
    Dim _cidade As String
    Dim _email As String
    Dim _sexo As String
    Dim _cliente As String
    Dim _id As String
    Dim _produto As String
    Dim _ativo As Boolean
    Dim _plano As String
    Dim _copiadosistema As Boolean
    Dim _copiadosistemapor As String
    Dim _copiadosistemaem As DateTime
    Dim _parentesco As String
    Dim _dtcad As DateTime
    Dim _UF As String
    Dim _nrseqctrl As String
    Dim _carencia As Boolean
    Dim _carteirinha As Boolean
    Dim _cep As String
    Dim _dataobito As DateTime
    Dim _diascarencia As Integer = 0
    Dim _dtAltera As DateTime
    Dim _dtcarencia As DateTime
    Dim _dtcarteirinha As DateTime
    Dim _dtdefinidacarencia As DateTime
    Dim _dtvalidadecart As DateTime
    Dim _estadocivil As String
    Dim _motivopendencia As String
    Dim _nrcarteirinha As String
    Dim _obito As Boolean
    Dim _pendencia As String
    Dim _pvpagante As Boolean
    Dim _rg As String
    Dim _segtitular As Boolean
    Dim _Tipo As String
    Dim _useraltera As String
    Dim _usercarencia As String
    Dim _usercarteirinha As String
    Dim _userexclui As String
    Dim _userexcluipendencia As String
    Dim _userobito As String
    Dim _userpendencia As String
    Dim _matriculaaux As String
    Dim _usercad As String
    Dim _nrseqempresa As Integer = 0
    Dim _Mensagemerro As String
    Dim _contador As Integer = 0

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Telresidencial As String
        Get
            Return _telresidencial
        End Get
        Set(value As String)
            _telresidencial = value
        End Set
    End Property

    Public Property Telcelular As String
        Get
            Return _telcelular
        End Get
        Set(value As String)
            _telcelular = value
        End Set
    End Property

    Public Property Cpf As String
        Get
            Return _cpf
        End Get
        Set(value As String)
            _cpf = value
        End Set
    End Property

    Public Property Dtnasc As Date
        Get
            Return _dtnasc
        End Get
        Set(value As Date)
            _dtnasc = value
        End Set
    End Property

    Public Property Associado As Integer
        Get
            Return _associado
        End Get
        Set(value As Integer)
            _associado = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Maquina As String
        Get
            Return _maquina
        End Get
        Set(value As String)
            _maquina = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Complemento As String
        Get
            Return _complemento
        End Get
        Set(value As String)
            _complemento = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Sexo As String
        Get
            Return _sexo
        End Get
        Set(value As String)
            _sexo = value
        End Set
    End Property

    Public Property Cliente As String
        Get
            Return _cliente
        End Get
        Set(value As String)
            _cliente = value
        End Set
    End Property

    Public Property Id As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property

    Public Property Produto As String
        Get
            Return _produto
        End Get
        Set(value As String)
            _produto = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Copiadosistema As Boolean
        Get
            Return _copiadosistema
        End Get
        Set(value As Boolean)
            _copiadosistema = value
        End Set
    End Property

    Public Property Copiadosistemapor As String
        Get
            Return _copiadosistemapor
        End Get
        Set(value As String)
            _copiadosistemapor = value
        End Set
    End Property

    Public Property Copiadosistemaem As Date
        Get
            Return _copiadosistemaem
        End Get
        Set(value As Date)
            _copiadosistemaem = value
        End Set
    End Property

    Public Property Parentesco As String
        Get
            Return _parentesco
        End Get
        Set(value As String)
            _parentesco = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property UF As String
        Get
            Return _UF
        End Get
        Set(value As String)
            _UF = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Carencia As Boolean
        Get
            Return _carencia
        End Get
        Set(value As Boolean)
            _carencia = value
        End Set
    End Property

    Public Property Carteirinha As Boolean
        Get
            Return _carteirinha
        End Get
        Set(value As Boolean)
            _carteirinha = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Dataobito As Date
        Get
            Return _dataobito
        End Get
        Set(value As Date)
            _dataobito = value
        End Set
    End Property

    Public Property Diascarencia As Integer
        Get
            Return _diascarencia
        End Get
        Set(value As Integer)
            _diascarencia = value
        End Set
    End Property

    Public Property DtAltera As Date
        Get
            Return _dtAltera
        End Get
        Set(value As Date)
            _dtAltera = value
        End Set
    End Property

    Public Property Dtcarencia As Date
        Get
            Return _dtcarencia
        End Get
        Set(value As Date)
            _dtcarencia = value
        End Set
    End Property

    Public Property Dtcarteirinha As Date
        Get
            Return _dtcarteirinha
        End Get
        Set(value As Date)
            _dtcarteirinha = value
        End Set
    End Property

    Public Property Dtdefinidacarencia As Date
        Get
            Return _dtdefinidacarencia
        End Get
        Set(value As Date)
            _dtdefinidacarencia = value
        End Set
    End Property

    Public Property Dtvalidadecart As Date
        Get
            Return _dtvalidadecart
        End Get
        Set(value As Date)
            _dtvalidadecart = value
        End Set
    End Property

    Public Property Estadocivil As String
        Get
            Return _estadocivil
        End Get
        Set(value As String)
            _estadocivil = value
        End Set
    End Property

    Public Property Motivopendencia As String
        Get
            Return _motivopendencia
        End Get
        Set(value As String)
            _motivopendencia = value
        End Set
    End Property

    Public Property Nrcarteirinha As String
        Get
            Return _nrcarteirinha
        End Get
        Set(value As String)
            _nrcarteirinha = value
        End Set
    End Property

    Public Property Obito As Boolean
        Get
            Return _obito
        End Get
        Set(value As Boolean)
            _obito = value
        End Set
    End Property

    Public Property Pendencia As String
        Get
            Return _pendencia
        End Get
        Set(value As String)
            _pendencia = value
        End Set
    End Property

    Public Property Pvpagante As Boolean
        Get
            Return _pvpagante
        End Get
        Set(value As Boolean)
            _pvpagante = value
        End Set
    End Property

    Public Property Rg As String
        Get
            Return _rg
        End Get
        Set(value As String)
            _rg = value
        End Set
    End Property

    Public Property Segtitular As Boolean
        Get
            Return _segtitular
        End Get
        Set(value As Boolean)
            _segtitular = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _Tipo
        End Get
        Set(value As String)
            _Tipo = value
        End Set
    End Property

    Public Property Useraltera As String
        Get
            Return _useraltera
        End Get
        Set(value As String)
            _useraltera = value
        End Set
    End Property

    Public Property Usercarencia As String
        Get
            Return _usercarencia
        End Get
        Set(value As String)
            _usercarencia = value
        End Set
    End Property

    Public Property Usercarteirinha As String
        Get
            Return _usercarteirinha
        End Get
        Set(value As String)
            _usercarteirinha = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Userexcluipendencia As String
        Get
            Return _userexcluipendencia
        End Get
        Set(value As String)
            _userexcluipendencia = value
        End Set
    End Property

    Public Property Userobito As String
        Get
            Return _userobito
        End Get
        Set(value As String)
            _userobito = value
        End Set
    End Property

    Public Property Userpendencia As String
        Get
            Return _userpendencia
        End Get
        Set(value As String)
            _userpendencia = value
        End Set
    End Property

    Public Property Matriculaaux As String
        Get
            Return _matriculaaux
        End Get
        Set(value As String)
            _matriculaaux = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _Mensagemerro
        End Get
        Set(value As String)
            _Mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property
End Class

Partial Public Class clsdependentes

    Public Function consultar() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where salvo = true and nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then

                _nome = tb1.Rows(0)("nome").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _telresidencial = tb1.Rows(0)("telresidencial").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _associado = tb1.Rows(0)("associado").ToString
                _matricula = tb1.Rows(0)("matricula").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _email = tb1.Rows(0)("email").ToString
                _sexo = tb1.Rows(0)("sexo").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _produto = tb1.Rows(0)("produto").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _parentesco = tb1.Rows(0)("parentesco").ToString
                _UF = tb1.Rows(0)("UF").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _carteirinha = logico(tb1.Rows(0)("carteirinha").ToString)
                _cep = tb1.Rows(0)("cep").ToString
                _diascarencia = sonumeros(tb1.Rows(0)("diascarencia").ToString)
                _estadocivil = tb1.Rows(0)("estadocivil").ToString
                _motivopendencia = tb1.Rows(0)("motivopendencia").ToString
                _nrcarteirinha = sonumeros(tb1.Rows(0)("nrcarteirinha").ToString)
                _obito = logico(tb1.Rows(0)("obito").ToString)
                _pendencia = tb1.Rows(0)("pendencia").ToString
                _pvpagante = logico(tb1.Rows(0)("pvpagante").ToString)
                _rg = tb1.Rows(0)("rg").ToString
                _segtitular = logico(tb1.Rows(0)("segtitular").ToString)
                _Tipo = tb1.Rows(0)("Tipo").ToString
                _useraltera = tb1.Rows(0)("useraltera").ToString
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _usercarteirinha = tb1.Rows(0)("usercarteirinha").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _userexcluipendencia = tb1.Rows(0)("userexcluipendencia").ToString
                _userobito = tb1.Rows(0)("userobito").ToString
                _userpendencia = tb1.Rows(0)("userpendencia").ToString
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _usercad = tb1.Rows(0)("usercad").ToString


                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dataobito").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dataobito").ToString
                        _dataobito = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtAltera").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtAltera").ToString
                        _dtAltera = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarteirinha").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarteirinha").ToString
                        _dtcarteirinha = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtdefinidacarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtdefinidacarencia").ToString
                        _dtdefinidacarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarmatricula() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE salvo=true and ativo = 1 AND matricula = '" & _matricula & "' GROUP BY nrseq, nome, matriculaaux ")

            If tb1.Rows.Count > 0 Then

                _nome = tb1.Rows(0)("nome").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _telresidencial = tb1.Rows(0)("telresidencial").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _associado = tb1.Rows(0)("associado").ToString
                _matricula = tb1.Rows(0)("matricula").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _email = tb1.Rows(0)("email").ToString
                _sexo = tb1.Rows(0)("sexo").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _produto = tb1.Rows(0)("produto").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _parentesco = tb1.Rows(0)("parentesco").ToString
                _UF = tb1.Rows(0)("UF").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _carteirinha = logico(tb1.Rows(0)("carteirinha").ToString)
                _cep = tb1.Rows(0)("cep").ToString
                _diascarencia = tb1.Rows(0)("diascarencia").ToString
                _estadocivil = tb1.Rows(0)("estadocivil").ToString
                _motivopendencia = tb1.Rows(0)("motivopendencia").ToString
                _nrcarteirinha = tb1.Rows(0)("nrcarteirinha").ToString
                _obito = logico(tb1.Rows(0)("obito").ToString)
                _pendencia = tb1.Rows(0)("pendencia").ToString
                _pvpagante = logico(tb1.Rows(0)("pvpagante").ToString)
                _rg = tb1.Rows(0)("rg").ToString
                _segtitular = logico(tb1.Rows(0)("segtitular").ToString)
                _Tipo = tb1.Rows(0)("Tipo").ToString
                _useraltera = tb1.Rows(0)("useraltera").ToString
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _usercarteirinha = tb1.Rows(0)("usercarteirinha").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _userexcluipendencia = tb1.Rows(0)("userexcluipendencia").ToString
                _userobito = tb1.Rows(0)("userobito").ToString
                _userpendencia = tb1.Rows(0)("userpendencia").ToString
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _usercad = tb1.Rows(0)("usercad").ToString


                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dataobito").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dataobito").ToString
                        _dataobito = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtAltera").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtAltera").ToString
                        _dtAltera = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarteirinha").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarteirinha").ToString
                        _dtcarteirinha = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtdefinidacarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtdefinidacarencia").ToString
                        _dtdefinidacarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function consultarmatriculaecliente(Optional tipoprocura As Integer = 0, Optional chkdesativados As CheckBox = Nothing) As Boolean

        Try

            If tipoprocura > 0 Then
                Dim sql = "SELECT * FROM " & _tablename & " WHERE cliente = '" & _cliente & "' AND matricula = '" & _matricula & "'"
                If tipoprocura = 1 Then
                    sql &= " AND nome Like  '%" & _nome & "%' "

                End If

                If tipoprocura = 2 Then
                    sql &= " AND matricula LIKE  '%" & _matricula & "%'"

                End If

                If tipoprocura = 3 Then
                    sql &= " AND cpf LIKE  '%" & _cpf.Replace(".", "") & "%'"

                End If


                If chkdesativados IsNot Nothing Then

                    sql &= " AND ativo = 1 "

                End If

                tb1 = tab1.conectar(sql)

            Else
                tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE salvo=true and cliente = '" & _cliente & "' AND matricula = '" & _matricula & "'")

            End If


            If tb1.Rows.Count > 0 Then

                _nome = tb1.Rows(0)("nome").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _telresidencial = tb1.Rows(0)("telresidencial").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _associado = tb1.Rows(0)("associado").ToString
                _matricula = tb1.Rows(0)("matricula").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _email = tb1.Rows(0)("email").ToString
                _sexo = tb1.Rows(0)("sexo").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _produto = tb1.Rows(0)("produto").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _parentesco = tb1.Rows(0)("parentesco").ToString
                _UF = tb1.Rows(0)("UF").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _carteirinha = logico(tb1.Rows(0)("carteirinha").ToString)
                _cep = tb1.Rows(0)("cep").ToString
                _diascarencia = tb1.Rows(0)("diascarencia").ToString
                _estadocivil = tb1.Rows(0)("estadocivil").ToString
                _motivopendencia = tb1.Rows(0)("motivopendencia").ToString
                _nrcarteirinha = tb1.Rows(0)("nrcarteirinha").ToString
                _obito = logico(tb1.Rows(0)("obito").ToString)
                _pendencia = tb1.Rows(0)("pendencia").ToString
                _pvpagante = logico(tb1.Rows(0)("pvpagante").ToString)
                _rg = tb1.Rows(0)("rg").ToString
                _segtitular = logico(tb1.Rows(0)("segtitular").ToString)
                _Tipo = tb1.Rows(0)("Tipo").ToString
                _useraltera = tb1.Rows(0)("useraltera").ToString
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _usercarteirinha = tb1.Rows(0)("usercarteirinha").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _userexcluipendencia = tb1.Rows(0)("userexcluipendencia").ToString
                _userobito = tb1.Rows(0)("userobito").ToString
                _userpendencia = tb1.Rows(0)("userpendencia").ToString
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _usercad = tb1.Rows(0)("usercad").ToString


                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dataobito").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dataobito").ToString
                        _dataobito = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtAltera").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtAltera").ToString
                        _dtAltera = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarteirinha").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarteirinha").ToString
                        _dtcarteirinha = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtdefinidacarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtdefinidacarencia").ToString
                        _dtdefinidacarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function consultarmatriculaenomeqtd() As Boolean

        Try

            tb1 = tab1.conectar("SELECT count(nrseq) As qnt FROM tbdependentes WHERE salvo=true and ativo = 1 AND cliente = '" & _cliente & "' AND matricula = '" & _matricula & "'")

            If tb1.Rows.Count > 0 Then

                _nome = tb1.Rows(0)("nome").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _telresidencial = tb1.Rows(0)("telresidencial").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _associado = tb1.Rows(0)("associado").ToString
                _matricula = tb1.Rows(0)("matricula").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _email = tb1.Rows(0)("email").ToString
                _sexo = tb1.Rows(0)("sexo").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _produto = tb1.Rows(0)("produto").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _parentesco = tb1.Rows(0)("parentesco").ToString
                _UF = tb1.Rows(0)("UF").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _carteirinha = logico(tb1.Rows(0)("carteirinha").ToString)
                _cep = tb1.Rows(0)("cep").ToString
                _diascarencia = tb1.Rows(0)("diascarencia").ToString
                _estadocivil = tb1.Rows(0)("estadocivil").ToString
                _motivopendencia = tb1.Rows(0)("motivopendencia").ToString
                _nrcarteirinha = tb1.Rows(0)("nrcarteirinha").ToString
                _obito = logico(tb1.Rows(0)("obito").ToString)
                _pendencia = tb1.Rows(0)("pendencia").ToString
                _pvpagante = logico(tb1.Rows(0)("pvpagante").ToString)
                _rg = tb1.Rows(0)("rg").ToString
                _segtitular = logico(tb1.Rows(0)("segtitular").ToString)
                _Tipo = tb1.Rows(0)("Tipo").ToString
                _useraltera = tb1.Rows(0)("useraltera").ToString
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _usercarteirinha = tb1.Rows(0)("usercarteirinha").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _userexcluipendencia = tb1.Rows(0)("userexcluipendencia").ToString
                _userobito = tb1.Rows(0)("userobito").ToString
                _userpendencia = tb1.Rows(0)("userpendencia").ToString
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _usercad = tb1.Rows(0)("usercad").ToString


                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dataobito").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dataobito").ToString
                        _dataobito = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtAltera").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtAltera").ToString
                        _dtAltera = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarteirinha").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarteirinha").ToString
                        _dtcarteirinha = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtdefinidacarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtdefinidacarencia").ToString
                        _dtdefinidacarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function procurar() As Boolean
        Try

            If _nrseq = 0 Then
                _Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from " & Tablename & " where excluido=false and nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then
                _nome = tb1.Rows(0)("nome").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _telresidencial = tb1.Rows(0)("telresidencial").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _associado = tb1.Rows(0)("associado").ToString
                _matricula = tb1.Rows(0)("matricula").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _email = tb1.Rows(0)("email").ToString
                _sexo = tb1.Rows(0)("sexo").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _produto = tb1.Rows(0)("produto").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _parentesco = tb1.Rows(0)("parentesco").ToString
                _UF = tb1.Rows(0)("UF").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _carteirinha = logico(tb1.Rows(0)("carteirinha").ToString)
                _cep = tb1.Rows(0)("cep").ToString
                _diascarencia = sonumeros(tb1.Rows(0)("diascarencia").ToString)
                _estadocivil = tb1.Rows(0)("estadocivil").ToString
                _motivopendencia = tb1.Rows(0)("motivopendencia").ToString
                _nrcarteirinha = tb1.Rows(0)("nrcarteirinha").ToString
                _obito = logico(tb1.Rows(0)("obito").ToString)
                _pendencia = tb1.Rows(0)("pendencia").ToString
                _pvpagante = logico(tb1.Rows(0)("pvpagante").ToString)
                _rg = tb1.Rows(0)("rg").ToString
                _segtitular = logico(tb1.Rows(0)("segtitular").ToString)
                _Tipo = tb1.Rows(0)("Tipo").ToString
                _useraltera = tb1.Rows(0)("useraltera").ToString
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _usercarteirinha = tb1.Rows(0)("usercarteirinha").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _userexcluipendencia = tb1.Rows(0)("userexcluipendencia").ToString
                _userobito = tb1.Rows(0)("userobito").ToString
                _userpendencia = tb1.Rows(0)("userpendencia").ToString
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _usercad = tb1.Rows(0)("usercad").ToString


                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dataobito").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dataobito").ToString
                        _dataobito = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtAltera").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtAltera").ToString
                        _dtAltera = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarteirinha").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarteirinha").ToString
                        _dtcarteirinha = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtdefinidacarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtdefinidacarencia").ToString
                        _dtdefinidacarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function


    Public Function consultartodos(Optional ativos As Boolean = False, Optional salvo As Boolean = False, Optional vinculo As Integer = 0) As Boolean

        Try
            tb1 = tab1.conectar("select * from " & _tablename & " where " & IIf(salvo = True, "salvo=true", "salvo=false") & IIf(vinculo = 0, "", " and associado= '" & vinculo & "'") & " and " & IIf(ativos = True, "ativo=true", "ativo=false") & " and excluido=false")
            _contador = tb1.Rows.Count
            _table = tb1
            Return True

        Catch excons As Exception
            _Mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function consultardependentes(Optional ativos As Boolean = False, Optional nrseqeditar As Integer = 0, Optional salvo As Boolean = False) As Boolean

        tb1 = tab1.conectar("select * from " & _tablename & " where " & IIf(salvo = False, "salvo=true", "salvo=false") & " and matricula = '" & _matricula & "' and " & IIf(ativos = True, "ativo=true", "ativo=false") & " " & IIf(nrseqeditar <> 0, " and nrseq= " & nrseqeditar, "") & " and excluido=false")
        _contador = tb1.Rows.Count
        _table = tb1
        Return True
        Try
        Catch excons As Exception
            _Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarmatriculaqtd() As Boolean

        Try

            tb1 = tab1.conectar("select count(*) as total from tbdependentes where salvo=true and matricula = '" & _matricula & "'")

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (salvo,nrseqctrl, dtcad, usercad, ativo,associado,excluido,matricula,matriculaaux,nrseqempresa) values (false,'" & wcnrseqctrl & "','" & valordatamysql(hoje()) & "','" & _usercad & "', true,'" & _associado & "',false,'" & _matricula & "','" & _matriculaaux & "','" & HttpContext.Current.Session("idempresaemuso") & "')")
            tb1 = tab1.conectar("Select * from  " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _Mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try
            tb1 = tab1.conectar("select * from " & _tablename & " where matricula = '" & _matricula & "' and nrseq='" & _nrseq & "'")

            'If tb1.Rows.Count > 0 Then
            '    _Mensagemerro = "Matricula já cadastrado no sistema!"
            '    Return False
            'End If

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set salvo=true, ativo=true, associado='" & _associado & "',nome='" & _nome & "',endereco='" & _endereco & "',telresidencial='" & _telresidencial & "',telcelular='" & _telcelular & "',cpf='" & _cpf & "',dtnasc='" & valordatamysql(_dtnasc) & "',maquina='" & _maquina & "',numero='" & _numero & "',complemento='" & _complemento & "',bairro='" & _bairro & "',cidade='" & _cidade & "',email='" & _email & "',sexo='" & _sexo & "',cliente='" & _cliente & "',id='" & _id & "',produto='" & _produto & "',plano='" & _plano & "',copiadosistema='" & _copiadosistema & "',copiadosistemapor='" & _copiadosistemapor & "',copiadosistemaem='" & _copiadosistemaem & "',parentesco='" & _parentesco & "',UF='" & _UF & "',carencia='" & _carencia & "',carteirinha='" & _carteirinha & "',cep='" & _cep & "',dataobito='" & _dataobito & "',diascarencia='" & _diascarencia & "',dtAltera='" & valordatamysql(_dtAltera) & "',dtcarencia='" & valordatamysql(_dtcarencia) & "',dtcarteirinha='" & valordatamysql(_dtcarteirinha) & "',dtdefinidacarencia='" & valordatamysql(_dtdefinidacarencia) & "',dtvalidadecart='" & valordatamysql(_dtvalidadecart) & "',estadocivil='" & _estadocivil & "',motivopendencia='" & _motivopendencia & "',nrcarteirinha='" & _nrcarteirinha & "',obito='" & _obito & "',pendencia='" & _pendencia & "',pvpagante='" & _pvpagante & "',rg='" & _rg & "',segtitular='" & _segtitular & "',Tipo='" & _Tipo & "',useraltera='" & _useraltera & "',usercarencia='" & _usercarencia & "',usercarteirinha='" & _usercarteirinha & "',userexcluipendencia='" & _userexcluipendencia & "',userobito='" & _userobito & "',userpendencia='" & _userpendencia & "' where nrseq= " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _Mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function consultarnaosalvos() As Boolean
        Try

            If _nrseq = 0 Then
                _Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from " & _tablename & " where excluido=false and nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then
                _nome = tb1.Rows(0)("nome").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _telresidencial = tb1.Rows(0)("telresidencial").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _associado = tb1.Rows(0)("associado").ToString
                _matricula = tb1.Rows(0)("matricula").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _email = tb1.Rows(0)("email").ToString
                _sexo = tb1.Rows(0)("sexo").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _produto = tb1.Rows(0)("produto").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _parentesco = tb1.Rows(0)("parentesco").ToString
                _UF = tb1.Rows(0)("UF").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _carteirinha = logico(tb1.Rows(0)("carteirinha").ToString)
                _cep = tb1.Rows(0)("cep").ToString
                _diascarencia = sonumeros(tb1.Rows(0)("diascarencia").ToString)
                _estadocivil = tb1.Rows(0)("estadocivil").ToString
                _motivopendencia = tb1.Rows(0)("motivopendencia").ToString
                _nrcarteirinha = tb1.Rows(0)("nrcarteirinha").ToString
                _obito = logico(tb1.Rows(0)("obito").ToString)
                _pendencia = tb1.Rows(0)("pendencia").ToString
                _pvpagante = logico(tb1.Rows(0)("pvpagante").ToString)
                _rg = tb1.Rows(0)("rg").ToString
                _segtitular = logico(tb1.Rows(0)("segtitular").ToString)
                _Tipo = tb1.Rows(0)("Tipo").ToString
                _useraltera = tb1.Rows(0)("useraltera").ToString
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _usercarteirinha = tb1.Rows(0)("usercarteirinha").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _userexcluipendencia = tb1.Rows(0)("userexcluipendencia").ToString
                _userobito = tb1.Rows(0)("userobito").ToString
                _userpendencia = tb1.Rows(0)("userpendencia").ToString
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _usercad = tb1.Rows(0)("usercad").ToString


                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dataobito").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dataobito").ToString
                        _dataobito = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtAltera").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtAltera").ToString
                        _dtAltera = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarteirinha").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarteirinha").ToString
                        _dtcarteirinha = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtdefinidacarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtdefinidacarencia").ToString
                        _dtdefinidacarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not consultarnaosalvos() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = false,excluido=true, dtexclui = '" & valordatamysql(hoje()) & "', userexclui = '" & _usercad & "' where nrseq = " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

End Class
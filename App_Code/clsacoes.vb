﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsacoes
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _nome As String
    Dim _numero As String
    Dim _justica As String
    Dim _conteudo As String
    Dim _data As String
    Dim _dtcad As Date
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _usercad As String
    Dim _ativo_old As Boolean
    Dim _ativo As Boolean
    Dim _nrseqctrl As String
    Dim _link As String
    Dim _participantes As String
    Dim _cancelados As String
    Dim _finalizada As Boolean
    Dim _finalizada_descrifim As String


    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Justica As String
        Get
            Return _justica
        End Get
        Set(value As String)
            _justica = value
        End Set
    End Property

    Public Property Conteudo As String
        Get
            Return _conteudo
        End Get
        Set(value As String)
            _conteudo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo_old As Boolean
        Get
            Return _ativo_old
        End Get
        Set(value As Boolean)
            _ativo_old = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Link As String
        Get
            Return _link
        End Get
        Set(value As String)
            _link = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Data As String
        Get
            Return _data
        End Get
        Set(value As String)
            _data = value
        End Set
    End Property

    Public Property Participantes As String
        Get
            Return _participantes
        End Get
        Set(value As String)
            _participantes = value
        End Set
    End Property

    Public Property Cancelados As String
        Get
            Return _cancelados
        End Get
        Set(value As String)
            _cancelados = value
        End Set
    End Property

    Public Property Finalizada As Boolean
        Get
            Return _finalizada
        End Get
        Set(value As Boolean)
            _finalizada = value
        End Set
    End Property

    Public Property Finalizada_descrifim As String
        Get
            Return _finalizada_descrifim
        End Get
        Set(value As String)
            _finalizada_descrifim = value
        End Set
    End Property
End Class

Partial Public Class clsacoes

    Public Function procurar() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbacoes where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Nome = tb1.Rows(0)("nome").ToString
        Numero = tb1.Rows(0)("numero").ToString
        Justica = tb1.Rows(0)("justica").ToString
        Conteudo = tb1.Rows(0)("conteudo").ToString
        Dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        Usercad = tb1.Rows(0)("usercad").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        Link = tb1.Rows(0)("link").ToString
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbacoes set ativo = 'true' "

            If Nome <> "" Then
                xsql &= ", nome = '" & tratatexto(Nome) & "'"
            End If
            If Numero <> "" Then
                xsql &= ", numero = '" & tratatexto(Numero) & "'"
            End If
            If Justica <> "" Then
                xsql &= ", justica = '" & tratatexto(Justica) & "'"
            End If
            If formatadatamysql(Data) <> "" Then
                xsql &= ", data = '" & formatadatamysql(Data) & "'"
            End If
            If Link <> "" Then
                xsql &= ", link = '" & tratatexto(Link) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbacoes (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbacoes where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbacoes set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
    Public Function nfim() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbacoes set finalizada = false where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function encerrar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbacoes set finalizada = true where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

End Class




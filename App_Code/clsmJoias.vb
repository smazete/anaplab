﻿Imports Microsoft.VisualBasic
Imports clsSmart

Public Class clsmJoias

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrsequser As Integer
    Dim _nrseq As Integer
    Dim _mes As Integer
    Dim _ano As Integer
    Dim _statuspg As Integer
    Dim _frmpg As Integer
    Dim _dtpg As Date
    Dim _userexclui As String
    Dim _usercad As String
    Dim _nrseqtipojoia As Boolean
    Dim _descricaotipojoia As String
    Dim _dtexclui As Date
    Dim _ativo As Boolean
    Dim _statusfiliacao As Integer
    Dim _ativas As Boolean

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrsequser As Integer
        Get
            Return _nrsequser
        End Get
        Set(value As Integer)
            _nrsequser = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Mes As Integer
        Get
            Return _mes
        End Get
        Set(value As Integer)
            _mes = value
        End Set
    End Property

    Public Property Ano As Integer
        Get
            Return _ano
        End Get
        Set(value As Integer)
            _ano = value
        End Set
    End Property

    Public Property Statuspg As Integer
        Get
            Return _statuspg
        End Get
        Set(value As Integer)
            _statuspg = value
        End Set
    End Property

    Public Property Frmpg As Integer
        Get
            Return _frmpg
        End Get
        Set(value As Integer)
            _frmpg = value
        End Set
    End Property

    Public Property Dtpg As Date
        Get
            Return _dtpg
        End Get
        Set(value As Date)
            _dtpg = value
        End Set
    End Property

    Public Property Statusfiliacao As Integer
        Get
            Return _statusfiliacao
        End Get
        Set(value As Integer)
            _statusfiliacao = value
        End Set
    End Property

    Public Property Ativas As Boolean
        Get
            Return _ativas
        End Get
        Set(value As Boolean)
            _ativas = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Nrseqtipojoia As Boolean
        Get
            Return _nrseqtipojoia
        End Get
        Set(value As Boolean)
            _nrseqtipojoia = value
        End Set
    End Property

    Public Property Descricaotipojoia As String
        Get
            Return _descricaotipojoia
        End Get
        Set(value As String)
            _descricaotipojoia = value
        End Set
    End Property
End Class

Partial Public Class clsmJoias

    Public Function procurar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbmensalidade_joias where nrseq = " & Nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If

            Nrseq = tb1.Rows(0)("nrseq").ToString
            Nrsequser = tb1.Rows(0)("nrsequser").ToString
            Mes = tb1.Rows(0)("mes").ToString
            Ano = tb1.Rows(0)("ano").ToString
            Statuspg = tb1.Rows(0)("statuspg").ToString
            Frmpg = tb1.Rows(0)("frmpg").ToString
            'Dtpg = FormatDateTime(tb1.Rows(0)("dtpg").ToString, DateFormat.ShortDate)
            Statusfiliacao = tb1.Rows(0)("statusfiliacao").ToString
            Ativo = logico(tb1.Rows(0)("ativo").ToString)
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbmensalidade_joias Set ativo = True "
            If Nrsequser <> 0 Then
                xsql &= ",nrsequser = " & moeda(Nrsequser)
            End If
            If Nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(Nrseq)
            End If
            If Mes <> 0 Then
                xsql &= ",mes = " & moeda(Mes)
            End If
            If Ano <> 0 Then
                xsql &= ",ano = " & moeda(Ano)
            End If
            If Statuspg <> 0 Then
                xsql &= ",statuspg = " & moeda(Statuspg)
            End If
            If Frmpg <> 0 Then
                xsql &= ",frmpg = " & moeda(Frmpg)
            End If
            If Dtpg <> "" Then
                xsql &= ",dtpg = '" & formatadatamysql(Dtpg) & "'"
            End If
            If Statusfiliacao <> 0 Then
                xsql &= ",statusfiliacao = " & moeda(Statusfiliacao)
            End If
            If Ativas <> True Then
                xsql &= ",ativas = '" & logico(Ativas) & "'"
            End If
            If nrseqtipojoia <> True Then
                xsql &= ",ativas = '" & logico(nrseqtipojoia) & "'"
            End If

            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidade_joias set ativo =  false ,statuspg = 0 , dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "' where nrseq = " & Nrseq & " And nrsequser = " & Nrsequser)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluirtodas() As Boolean
        Try
            If Nrsequser = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidade_joias Set ativo = False , statuspg = 0 where nrsequser = " & Nrsequser)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function estorno() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidade_joias set statuspg = '2' where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function baixar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidade_joias set statuspg = '1' where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function aberto() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmensalidade_joias set statuspg = '0' where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function



    Public Function valor() As Boolean
        Try

            tb1 = tab1.IncluirAlterarDados("update tbmensalidade_joias set valor = '100' ")
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function


End Class
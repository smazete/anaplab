﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart

Public Class clsAgenda_medica
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim _agendames As New List(Of clsAgenda_medica)
    Dim _matricula As String
    Dim _especialidade As String
    Dim _plano As String = ""
    Dim _agendadia As New List(Of clsAgenda_medica)
    Dim tablename As String = "tbagendamedica"
    Dim _table As Data.DataTable
    Dim _datastring As String
    Dim _ativo As Boolean = False
    Dim _contador As Integer = 0
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nrseqpaciente As Integer = 0
    Dim _nrseqdependente As Integer = 0
    Dim _nrseqmedico As Integer = 0
    Dim _valor As Decimal = 0
    Dim _nrseqespecialidade As Integer = 0
    Dim _nrseqservico As Integer = 0
    Dim _data As Date
    Dim _hora As String
    Dim _atendido As Boolean = False
    Dim _cancelado As Boolean = False
    Dim _retorno As Boolean = False
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _nrseqplano As Integer = 0
    Dim _nrseqprocedimento As Integer = 0
    Dim _nrseqempresa As Integer = 0
    Dim _ctrl As Integer = 0
    Dim _paciente As String
    Dim _valorrep As Decimal = 0
    Dim _confirmada As Boolean = False
    Dim _aguardando As Boolean = False
    Dim _obs As String
    Dim _indisponivel As Boolean = False
    Dim _nrseqsolicitadopor As Integer = 0
    Dim _horachegada As String
    Dim _horaatendido As String
    Dim _recibo As Integer = 0
    Dim _nrseqconfirmadapor As Integer = 0
    Dim _dtconfirmada As Date
    Dim _horaconfirmada As String
    Dim _nrseqcanceladopor As Integer = 0
    Dim _dtcancelado As Date
    Dim _nrseqatendidopor As Integer = 0
    Dim _encaixe As Boolean = False
    Dim _tela As String
    Dim _faturado As Boolean = False
    Dim _dtfaturado As Date
    Dim _userfaturado As String
    Dim _nrguiaconvenio As String
    Dim _dtalterado As Date
    Dim _useralterado As String
    Dim _formapagto As String
    Dim _qtdparcelas As Integer = 0
    Dim _senhaatendimento As String
    Dim _repassemanual As Decimal = 0
    Dim _nrseqctrl As String
    Dim _nrseqrecibo As Integer = 0
    Dim _repasseconfirmado As Boolean = False
    Dim _dtrepasseconfirmado As Date
    Dim _userrepasseconfirmado As String
    Dim _dtnfe As Date
    Dim _usernfe As String
    Dim _nrnfe As String
    Dim _nrlotenfe As String
    Dim _nomemedico As String
    Dim _nomecliente As String
    Dim _nomedependente As String
    Dim _nfe As Integer = 0
    Dim _nrseqconfirmacao As Integer = 0
    Dim _ehtitular As Boolean = False
    Dim _primeiraconsulta As Boolean = False
    Dim _usercancelado As String
    Dim _usermarcouconsulta As String
    Dim _useratendeu As String
    Dim _userconfirmoupresenca As String
    Dim _dtmarcado As DateTime
    Dim _dtatendido As DateTime
    Dim _nrseqcaixadth As Integer = 0
    Dim _userconfirmouconsulta As String
    Dim _dtconfirmouconsulta As DateTime
    Dim _chave As String = ""
    Dim _status As String = ""

    Public Sub New()
        _usercad = buscarsessoes("usuario")
        _useralterado = buscarsessoes("usuario")
        _usercancelado = buscarsessoes("usuario")
        _dtcancelado = hoje()
        _dtalterado = hoje()
        _dtcad = hoje()
        _userconfirmouconsulta = buscarsessoes("usuario")
    End Sub

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqpaciente As Integer
        Get
            Return _nrseqpaciente
        End Get
        Set(value As Integer)
            _nrseqpaciente = value
        End Set
    End Property

    Public Property Nrseqdependente As Integer
        Get
            Return _nrseqdependente
        End Get
        Set(value As Integer)
            _nrseqdependente = value
        End Set
    End Property

    Public Property Nrseqmedico As Integer
        Get
            Return _nrseqmedico
        End Get
        Set(value As Integer)
            _nrseqmedico = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _valor
        End Get
        Set(value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property Nrseqespecialidade As Integer
        Get
            Return _nrseqespecialidade
        End Get
        Set(value As Integer)
            _nrseqespecialidade = value
        End Set
    End Property

    Public Property Nrseqservico As Integer
        Get
            Return _nrseqservico
        End Get
        Set(value As Integer)
            _nrseqservico = value
        End Set
    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Property Hora As String
        Get
            Return _hora
        End Get
        Set(value As String)
            _hora = value
        End Set
    End Property

    Public Property Atendido As Boolean
        Get
            Return _atendido
        End Get
        Set(value As Boolean)
            _atendido = value
        End Set
    End Property

    Public Property Cancelado As Boolean
        Get
            Return _cancelado
        End Get
        Set(value As Boolean)
            _cancelado = value
        End Set
    End Property

    Public Property Retorno As Boolean
        Get
            Return _retorno
        End Get
        Set(value As Boolean)
            _retorno = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Nrseqplano As Integer
        Get
            Return _nrseqplano
        End Get
        Set(value As Integer)
            _nrseqplano = value
        End Set
    End Property

    Public Property Nrseqprocedimento As Integer
        Get
            Return _nrseqprocedimento
        End Get
        Set(value As Integer)
            _nrseqprocedimento = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Ctrl As Integer
        Get
            Return _ctrl
        End Get
        Set(value As Integer)
            _ctrl = value
        End Set
    End Property

    Public Property Paciente As String
        Get
            Return _paciente
        End Get
        Set(value As String)
            _paciente = value
        End Set
    End Property

    Public Property Valorrep As Decimal
        Get
            Return _valorrep
        End Get
        Set(value As Decimal)
            _valorrep = value
        End Set
    End Property

    Public Property Confirmada As Boolean
        Get
            Return _confirmada
        End Get
        Set(value As Boolean)
            _confirmada = value
        End Set
    End Property

    Public Property Aguardando As Boolean
        Get
            Return _aguardando
        End Get
        Set(value As Boolean)
            _aguardando = value
        End Set
    End Property

    Public Property Obs As String
        Get
            Return _obs
        End Get
        Set(value As String)
            _obs = value
        End Set
    End Property

    Public Property Indisponivel As Boolean
        Get
            Return _indisponivel
        End Get
        Set(value As Boolean)
            _indisponivel = value
        End Set
    End Property

    Public Property Nrseqsolicitadopor As Integer
        Get
            Return _nrseqsolicitadopor
        End Get
        Set(value As Integer)
            _nrseqsolicitadopor = value
        End Set
    End Property

    Public Property Horachegada As String
        Get
            Return _horachegada
        End Get
        Set(value As String)
            _horachegada = value
        End Set
    End Property

    Public Property Horaatendido As String
        Get
            Return _horaatendido
        End Get
        Set(value As String)
            _horaatendido = value
        End Set
    End Property

    Public Property Recibo As Integer
        Get
            Return _recibo
        End Get
        Set(value As Integer)
            _recibo = value
        End Set
    End Property

    Public Property Nrseqconfirmadapor As Integer
        Get
            Return _nrseqconfirmadapor
        End Get
        Set(value As Integer)
            _nrseqconfirmadapor = value
        End Set
    End Property

    Public Property Dtconfirmada As Date
        Get
            Return _dtconfirmada
        End Get
        Set(value As Date)
            _dtconfirmada = value
        End Set
    End Property

    Public Property Horaconfirmada As String
        Get
            Return _horaconfirmada
        End Get
        Set(value As String)
            _horaconfirmada = value
        End Set
    End Property

    Public Property Nrseqcanceladopor As Integer
        Get
            Return _nrseqcanceladopor
        End Get
        Set(value As Integer)
            _nrseqcanceladopor = value
        End Set
    End Property

    Public Property Dtcancelado As Date
        Get
            Return _dtcancelado
        End Get
        Set(value As Date)
            _dtcancelado = value
        End Set
    End Property

    Public Property Nrseqatendidopor As Integer
        Get
            Return _nrseqatendidopor
        End Get
        Set(value As Integer)
            _nrseqatendidopor = value
        End Set
    End Property

    Public Property Encaixe As Boolean
        Get
            Return _encaixe
        End Get
        Set(value As Boolean)
            _encaixe = value
        End Set
    End Property

    Public Property Tela As String
        Get
            Return _tela
        End Get
        Set(value As String)
            _tela = value
        End Set
    End Property

    Public Property Faturado As Boolean
        Get
            Return _faturado
        End Get
        Set(value As Boolean)
            _faturado = value
        End Set
    End Property

    Public Property Dtfaturado As Date
        Get
            Return _dtfaturado
        End Get
        Set(value As Date)
            _dtfaturado = value
        End Set
    End Property

    Public Property Userfaturado As String
        Get
            Return _userfaturado
        End Get
        Set(value As String)
            _userfaturado = value
        End Set
    End Property

    Public Property Nrguiaconvenio As String
        Get
            Return _nrguiaconvenio
        End Get
        Set(value As String)
            _nrguiaconvenio = value
        End Set
    End Property

    Public Property Dtalterado As Date
        Get
            Return _dtalterado
        End Get
        Set(value As Date)
            _dtalterado = value
        End Set
    End Property

    Public Property Useralterado As String
        Get
            Return _useralterado
        End Get
        Set(value As String)
            _useralterado = value
        End Set
    End Property

    Public Property Formapagto As String
        Get
            Return _formapagto
        End Get
        Set(value As String)
            _formapagto = value
        End Set
    End Property

    Public Property Qtdparcelas As Integer
        Get
            Return _qtdparcelas
        End Get
        Set(value As Integer)
            _qtdparcelas = value
        End Set
    End Property

    Public Property Senhaatendimento As String
        Get
            Return _senhaatendimento
        End Get
        Set(value As String)
            _senhaatendimento = value
        End Set
    End Property

    Public Property Repassemanual As Decimal
        Get
            Return _repassemanual
        End Get
        Set(value As Decimal)
            _repassemanual = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Nrseqrecibo As Integer
        Get
            Return _nrseqrecibo
        End Get
        Set(value As Integer)
            _nrseqrecibo = value
        End Set
    End Property

    Public Property Repasseconfirmado As Boolean
        Get
            Return _repasseconfirmado
        End Get
        Set(value As Boolean)
            _repasseconfirmado = value
        End Set
    End Property

    Public Property Dtrepasseconfirmado As Date
        Get
            Return _dtrepasseconfirmado
        End Get
        Set(value As Date)
            _dtrepasseconfirmado = value
        End Set
    End Property

    Public Property Userrepasseconfirmado As String
        Get
            Return _userrepasseconfirmado
        End Get
        Set(value As String)
            _userrepasseconfirmado = value
        End Set
    End Property

    Public Property Dtnfe As Date
        Get
            Return _dtnfe
        End Get
        Set(value As Date)
            _dtnfe = value
        End Set
    End Property

    Public Property Usernfe As String
        Get
            Return _usernfe
        End Get
        Set(value As String)
            _usernfe = value
        End Set
    End Property

    Public Property Nrnfe As String
        Get
            Return _nrnfe
        End Get
        Set(value As String)
            _nrnfe = value
        End Set
    End Property

    Public Property Nrlotenfe As String
        Get
            Return _nrlotenfe
        End Get
        Set(value As String)
            _nrlotenfe = value
        End Set
    End Property

    Public Property Nfe As Integer
        Get
            Return _nfe
        End Get
        Set(value As Integer)
            _nfe = value
        End Set
    End Property

    Public Property Nrseqconfirmacao As Integer
        Get
            Return _nrseqconfirmacao
        End Get
        Set(value As Integer)
            _nrseqconfirmacao = value
        End Set
    End Property

    Public Property Ehtitular As Boolean
        Get
            Return _ehtitular
        End Get
        Set(value As Boolean)
            _ehtitular = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Datastring As String
        Get
            Return _datastring
        End Get
        Set(value As String)
            _datastring = value
        End Set
    End Property

    Public Property Agendadia As List(Of clsAgenda_medica)
        Get
            Return _agendadia
        End Get
        Set(value As List(Of clsAgenda_medica))
            _agendadia = value
        End Set
    End Property

    Public Property Nomemedico As String
        Get
            Return _nomemedico
        End Get
        Set(value As String)
            _nomemedico = value
            If value <> "" Then
                tbx = tabx.conectar("select * from tbmedicos where nome = '" & _nomemedico & "' and ativo = true")
                If tbx.Rows.Count > 0 Then
                    _nrseqmedico = tbx.Rows(0)("cod").ToString
                End If
            End If
        End Set
    End Property

    Public Property Nomecliente As String
        Get
            Return _nomecliente
        End Get
        Set(value As String)
            _nomecliente = value
            If value <> "" Then
                tbx = tabx.conectar("select * from tbclientes where nome = '" & _nomemedico & "' and ativo = true")
                If tbx.Rows.Count > 0 Then
                    _nrseqpaciente = tbx.Rows(0)("nrseq").ToString
                    _matricula = tbx.Rows(0)("matricula").ToString
                End If
            End If
        End Set
    End Property

    Public Property Nomedependente As String
        Get
            Return _nomedependente
        End Get
        Set(value As String)
            _nomedependente = value
            If value <> "" Then
                tbx = tabx.conectar("select * from tbdependentes where nome = '" & _nomemedico & "' " & IIf(_matricula <> "", " and matricula = '" & _matricula & "'", " And 1=1 ") & " And Ativo = True")
                If tbx.Rows.Count > 0 Then
                    _nrseqdependente = tbx.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Especialidade As String
        Get
            Return _especialidade
        End Get
        Set(value As String)
            _especialidade = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Primeiraconsulta As Boolean
        Get
            Return _primeiraconsulta
        End Get
        Set(value As Boolean)
            _primeiraconsulta = value
        End Set
    End Property

    Public Property Chave As String
        Get
            Return _chave
        End Get
        Set(value As String)
            _chave = value
        End Set
    End Property

    Public Property Usercancelado As String
        Get
            Return _usercancelado
        End Get
        Set(value As String)
            _usercancelado = value
        End Set
    End Property

    Public Property Usermarcouconsulta As String
        Get
            Return _usermarcouconsulta
        End Get
        Set(value As String)
            _usermarcouconsulta = value
        End Set
    End Property

    Public Property Useratendeu As String
        Get
            Return _useratendeu
        End Get
        Set(value As String)
            _useratendeu = value
        End Set
    End Property

    Public Property Dtatendido As Date
        Get
            Return _dtatendido
        End Get
        Set(value As Date)
            _dtatendido = value
        End Set
    End Property

    Public Property Dtmarcado As Date
        Get
            Return _dtmarcado
        End Get
        Set(value As Date)
            _dtmarcado = value
        End Set
    End Property

    Public Property Status As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property

    Public Property Agendames As List(Of clsAgenda_medica)
        Get
            Return _agendames
        End Get
        Set(value As List(Of clsAgenda_medica))
            _agendames = value
        End Set
    End Property

    Public Property Nrseqcaixadth As Integer
        Get
            Return _nrseqcaixadth
        End Get
        Set(value As Integer)
            _nrseqcaixadth = value
        End Set
    End Property

    Public Property Userconfirmoupresenca As String
        Get
            Return _userconfirmoupresenca
        End Get
        Set(value As String)
            _userconfirmoupresenca = value
        End Set
    End Property

    Public Property Userconfirmouconsulta As String
        Get
            Return _userconfirmouconsulta
        End Get
        Set(value As String)
            _userconfirmouconsulta = value
        End Set
    End Property

    Public Property Dtconfirmouconsulta As Date
        Get
            Return _dtconfirmouconsulta
        End Get
        Set(value As Date)
            _dtconfirmouconsulta = value
        End Set
    End Property
End Class

Partial Public Class clsAgenda_medica

    Public Function Consultardata(Optional mounthyear As String = "", Optional range As String = "", Optional cancelado As Boolean = False) As Boolean
        Try
            If _datastring = "" AndAlso range = "" Then
                Exit Function
            End If
            If range <> "" Then
                tb1 = tab1.conectar("select DISTINCT data from " & tablename & " where nrseqmedico='" & _nrseqmedico & "' " & IIf(cancelado = True, "cancelado=" & _cancelado & "", "") & " and data BETWEEN '" & mounthyear & "01' AND '" & range & "'")
            Else
                tb1 = tab1.conectar("select *,count(*) as qtdconsultas from " & tablename & " where cancelado=false and nrseqmedico='" & _nrseqmedico & "' and data = '" & _datastring & "'")
            End If

            _table = tb1
            _contador = tb1.Rows.Count
            Return True

        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function consultarconsultas() As Boolean
        Try


            tb1 = tab1.conectar("select * from vwagenda where nrseqmedico='" & _nrseqmedico & "' and data = '" & _datastring & "' group by nrseq")

            If tb1.Rows.Count > 0 Then
                _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
                _nrseqpaciente = numeros(tb1.Rows(0)("nrseqpaciente").ToString)
                _nrseqdependente = numeros(tb1.Rows(0)("nrseqdependente").ToString)
                _nrseqmedico = numeros(tb1.Rows(0)("nrseqmedico").ToString)
                _valor = numeros(tb1.Rows(0)("valor").ToString)
                _nrseqespecialidade = numeros(tb1.Rows(0)("nrseqespecialidade").ToString)
                _nrseqservico = numeros(tb1.Rows(0)("nrseqservico").ToString)
                _data = valordata(tb1.Rows(0)("data").ToString)
                _hora = tratatexto(tb1.Rows(0)("hora").ToString)
                _atendido = logico(tb1.Rows(0)("atendido").ToString)
                _cancelado = logico(tb1.Rows(0)("cancelado").ToString)
                _retorno = logico(tb1.Rows(0)("retorno").ToString)
                _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
                _usercad = tratatexto(tb1.Rows(0)("usercad").ToString)
                _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
                _nrseqprocedimento = numeros(tb1.Rows(0)("nrseqprocedimento").ToString)
                _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
                _ctrl = numeros(tb1.Rows(0)("ctrl").ToString)
                _paciente = tratatexto(tb1.Rows(0)("paciente").ToString)
                _valorrep = numeros(tb1.Rows(0)("valorrep").ToString)
                _confirmada = logico(tb1.Rows(0)("confirmada").ToString)
                _aguardando = logico(tb1.Rows(0)("aguardando").ToString)
                _obs = tratatexto(tb1.Rows(0)("obs").ToString)
                _indisponivel = logico(tb1.Rows(0)("indisponivel").ToString)
                _nrseqsolicitadopor = numeros(tb1.Rows(0)("nrseqsolicitadopor").ToString)
                _horachegada = tratatexto(tb1.Rows(0)("horachegada").ToString)
                _horaatendido = tratatexto(tb1.Rows(0)("horaatendido").ToString)
                _recibo = numeros(tb1.Rows(0)("recibo").ToString)
                _nrseqconfirmadapor = numeros(tb1.Rows(0)("nrseqconfirmadapor").ToString)
                _dtconfirmada = valordata(tb1.Rows(0)("dtconfirmada").ToString)
                _horaconfirmada = tratatexto(tb1.Rows(0)("horaconfirmada").ToString)
                _nrseqcanceladopor = numeros(tb1.Rows(0)("nrseqcanceladopor").ToString)
                _nrseqatendidopor = numeros(tb1.Rows(0)("nrseqatendidopor").ToString)
                _encaixe = logico(tb1.Rows(0)("encaixe").ToString)
                _tela = tratatexto(tb1.Rows(0)("tela").ToString)
                _faturado = logico(tb1.Rows(0)("faturado").ToString)
                _dtfaturado = valordata(tb1.Rows(0)("dtfaturado").ToString)
                _userfaturado = tratatexto(tb1.Rows(0)("userfaturado").ToString)
                _nrguiaconvenio = tratatexto(tb1.Rows(0)("nrguiaconvenio").ToString)
                _dtalterado = valordata(tb1.Rows(0)("dtalterado").ToString)
                _useralterado = tratatexto(tb1.Rows(0)("useralterado").ToString)
                _formapagto = tratatexto(tb1.Rows(0)("formapagto").ToString)
                _qtdparcelas = numeros(tb1.Rows(0)("qtdparcelas").ToString)
                _senhaatendimento = tratatexto(tb1.Rows(0)("senhaatendimento").ToString)
                _repassemanual = numeros(tb1.Rows(0)("repassemanual").ToString)
                _nrseqrecibo = numeros(tb1.Rows(0)("nrseqrecibo").ToString)
                _repasseconfirmado = logico(tb1.Rows(0)("repasseconfirmado").ToString)
                _dtrepasseconfirmado = valordata(tb1.Rows(0)("dtrepasseconfirmado").ToString)
                _userrepasseconfirmado = tratatexto(tb1.Rows(0)("userrepasseconfirmado").ToString)
                _dtnfe = valordata(tb1.Rows(0)("dtnfe").ToString)
                _usernfe = tratatexto(tb1.Rows(0)("usernfe").ToString)
                _nrnfe = tratatexto(tb1.Rows(0)("nrnfe").ToString)
                _nrlotenfe = tratatexto(tb1.Rows(0)("nrlotenfe").ToString)
                _nfe = numeros(tb1.Rows(0)("nfe").ToString)
                _nrseqconfirmacao = numeros(tb1.Rows(0)("nrseqconfirmacao").ToString)
                _ehtitular = logico(tb1.Rows(0)("ehtitular").ToString)
                _primeiraconsulta = logico(tb1.Rows(0)("primeiraconsulta").ToString)
                _usercancelado = tratatexto(tb1.Rows(0)("usercancelado").ToString)
                _dtcancelado = valordata(tb1.Rows(0)("dtcancelado").ToString)
                _usermarcouconsulta = tratatexto(tb1.Rows(0)("usermarcouconsulta").ToString)
                _dtmarcado = valordata(tb1.Rows(0)("dtmarcado").ToString)
                _useratendeu = tratatexto(tb1.Rows(0)("useratendeu").ToString)
                _dtatendido = valordata(tb1.Rows(0)("dtatendido").ToString)
                _nrseqcaixadth = numeros(tb1.Rows(0)("nrseqcaixadth").ToString)
                _userconfirmouconsulta = tratatexto(tb1.Rows(0)("userconfirmouconsulta").ToString)
                _dtconfirmouconsulta = valordata(tb1.Rows(0)("dtconfirmouconsulta").ToString)

            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True

        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procurar() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from vwagenda where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        If tb1.Rows.Count > 0 Then
            _nrseqpaciente = numeros(tb1.Rows(0)("nrseqpaciente").ToString)
            _nrseqdependente = numeros(tb1.Rows(0)("nrseqdependente").ToString)
            _nrseqmedico = numeros(tb1.Rows(0)("nrseqmedico").ToString)
            _valor = numeros(tb1.Rows(0)("valor").ToString)
            _nrseqespecialidade = numeros(tb1.Rows(0)("nrseqespecialidade").ToString)
            _nrseqservico = numeros(tb1.Rows(0)("nrseqservico").ToString)
            _data = valordata(tb1.Rows(0)("data").ToString)
            _hora = tratatexto(tb1.Rows(0)("hora").ToString)
            _atendido = logico(tb1.Rows(0)("atendido").ToString)
            _cancelado = logico(tb1.Rows(0)("cancelado").ToString)
            _retorno = logico(tb1.Rows(0)("retorno").ToString)
            _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
            _usercad = tratatexto(tb1.Rows(0)("usercad").ToString)
            _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
            _nrseqprocedimento = numeros(tb1.Rows(0)("nrseqprocedimento").ToString)
            _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
            _ctrl = numeros(tb1.Rows(0)("ctrl").ToString)
            _paciente = tratatexto(tb1.Rows(0)("paciente").ToString)
            _valorrep = numeros(tb1.Rows(0)("valorrep").ToString)
            _confirmada = logico(tb1.Rows(0)("confirmada").ToString)
            _aguardando = logico(tb1.Rows(0)("aguardando").ToString)
            _obs = tratatexto(tb1.Rows(0)("obs").ToString)
            _indisponivel = logico(tb1.Rows(0)("indisponivel").ToString)
            _nrseqsolicitadopor = numeros(tb1.Rows(0)("nrseqsolicitadopor").ToString)
            _horachegada = tratatexto(tb1.Rows(0)("horachegada").ToString)
            _horaatendido = tratatexto(tb1.Rows(0)("horaatendido").ToString)
            _recibo = numeros(tb1.Rows(0)("recibo").ToString)
            _nrseqconfirmadapor = numeros(tb1.Rows(0)("nrseqconfirmadapor").ToString)
            _dtconfirmada = valordata(tb1.Rows(0)("dtconfirmada").ToString)
            _horaconfirmada = tratatexto(tb1.Rows(0)("horaconfirmada").ToString)
            _nrseqcanceladopor = numeros(tb1.Rows(0)("nrseqcanceladopor").ToString)
            _nrseqatendidopor = numeros(tb1.Rows(0)("nrseqatendidopor").ToString)
            _encaixe = logico(tb1.Rows(0)("encaixe").ToString)
            _tela = tratatexto(tb1.Rows(0)("tela").ToString)
            _faturado = logico(tb1.Rows(0)("faturado").ToString)
            _dtfaturado = valordata(tb1.Rows(0)("dtfaturado").ToString)
            _userfaturado = tratatexto(tb1.Rows(0)("userfaturado").ToString)
            _nrguiaconvenio = tratatexto(tb1.Rows(0)("nrguiaconvenio").ToString)
            _dtalterado = valordata(tb1.Rows(0)("dtalterado").ToString)
            _useralterado = tratatexto(tb1.Rows(0)("useralterado").ToString)
            _formapagto = tratatexto(tb1.Rows(0)("formapagto").ToString)
            _qtdparcelas = numeros(tb1.Rows(0)("qtdparcelas").ToString)
            _senhaatendimento = tratatexto(tb1.Rows(0)("senhaatendimento").ToString)
            _repassemanual = numeros(tb1.Rows(0)("repassemanual").ToString)
            _nrseqrecibo = numeros(tb1.Rows(0)("nrseqrecibo").ToString)
            _repasseconfirmado = logico(tb1.Rows(0)("repasseconfirmado").ToString)
            _dtrepasseconfirmado = valordata(tb1.Rows(0)("dtrepasseconfirmado").ToString)
            _userrepasseconfirmado = tratatexto(tb1.Rows(0)("userrepasseconfirmado").ToString)
            _dtnfe = valordata(tb1.Rows(0)("dtnfe").ToString)
            _usernfe = tratatexto(tb1.Rows(0)("usernfe").ToString)
            _nrnfe = tratatexto(tb1.Rows(0)("nrnfe").ToString)
            _nrlotenfe = tratatexto(tb1.Rows(0)("nrlotenfe").ToString)
            _nfe = numeros(tb1.Rows(0)("nfe").ToString)
            _nrseqconfirmacao = numeros(tb1.Rows(0)("nrseqconfirmacao").ToString)
            _ehtitular = logico(tb1.Rows(0)("ehtitular").ToString)
            _primeiraconsulta = logico(tb1.Rows(0)("primeiraconsulta").ToString)
            _usercancelado = tratatexto(tb1.Rows(0)("usercancelado").ToString)
            _userconfirmoupresenca = tratatexto(tb1.Rows(0)("userconfirmoupresenca").ToString)
            _dtcancelado = valordata(tb1.Rows(0)("dtcancelado").ToString)
            _usermarcouconsulta = tratatexto(tb1.Rows(0)("usermarcouconsulta").ToString)
            _dtmarcado = valordata(tb1.Rows(0)("dtmarcado").ToString)
            _useratendeu = tratatexto(tb1.Rows(0)("useratendeu").ToString)
            _dtatendido = valordata(tb1.Rows(0)("dtatendido").ToString)
            _nrseqcaixadth = numeros(tb1.Rows(0)("nrseqcaixadth").ToString)
            _userconfirmouconsulta = tratatexto(tb1.Rows(0)("userconfirmouconsulta").ToString)
            _dtconfirmouconsulta = valordata(tb1.Rows(0)("dtconfirmouconsulta").ToString)
        End If


        _table = tb1
        _contador = tb1.Rows.Count
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procurarhistorico() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        If _nrseqdependente <> 0 Then
            tb1 = tab1.conectar("select * from vwagenda where nrseqdependente='" & _nrseqdependente & "' and cancelado=False and atendido=True and confirmada=True")
        Else
            tb1 = tab1.conectar("select * from vwagenda where nrseqpaciente='" & _nrseqpaciente & "' and cancelado=False and atendido=True and confirmada=True")
        End If

        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Não possui nenhuma consulta agendada para esse cliente"
        End If

        _table = tb1
        _contador = tb1.Rows.Count
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & tablename & " Set nrseqcaixadth='" & _nrseqcaixadth & "', ativo = True,cancelado=False"

            If _nrseqpaciente <> 0 Then
                xsql &= ",nrseqpaciente = " & moeda(_nrseqpaciente)
            End If
            If _nrseqdependente <> 0 Then
                xsql &= ",nrseqdependente = " & moeda(_nrseqdependente)
            End If
            If _nrseqmedico <> 0 Then
                xsql &= ",nrseqmedico = " & moeda(_nrseqmedico)
            End If
            If _valor <> "" Then
                xsql &= ",valor = '" & tratatexto(_valor) & "'"
            End If
            If _nrseqespecialidade <> 0 Then
                xsql &= ",nrseqespecialidade = " & moeda(_nrseqespecialidade)
            End If
            If _nrseqservico <> 0 Then
                xsql &= ",nrseqservico = " & moeda(_nrseqservico)
            End If
            If _data <> "" Then
                xsql &= ",data = '" & formatadatamysql(_data) & "'"
            End If
            If _hora <> "" Then
                xsql &= ",hora = '" & tratatexto(_hora) & "'"
            End If
            If _atendido <> 0 Then
                xsql &= ",atendido = " & moeda(_atendido)
            End If
            If _retorno <> 0 Then
                xsql &= ",retorno = " & moeda(_retorno)
            End If
            If _dtcad <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _nrseqplano <> 0 Then
                xsql &= ",nrseqplano = " & moeda(_nrseqplano)
            End If
            If _nrseqprocedimento <> 0 Then
                xsql &= ",nrseqprocedimento = " & moeda(_nrseqprocedimento)
            End If
            If _nrseqempresa <> 0 Then
                xsql &= ",nrseqempresa = '" & moeda(_nrseqempresa) & "'"
            End If
            If _ctrl <> 0 Then
                xsql &= ",ctrl = " & moeda(_ctrl)
            End If
            If _paciente <> "" Then
                xsql &= ",paciente = '" & tratatexto(_paciente) & "'"
            End If
            If _valorrep <> "" Then
                xsql &= ",valorrep = '" & tratatexto(_valorrep) & "'"
            End If
            If _confirmada <> 0 Then
                xsql &= ",confirmada = " & moeda(_confirmada)
            End If
            If _aguardando <> 0 Then
                xsql &= ",aguardando = " & moeda(_aguardando)
            End If
            If _obs <> 0 Then
                xsql &= ",obs = " & moeda(_obs)
            End If
            If _indisponivel <> 0 Then
                xsql &= ",indisponivel = " & moeda(_indisponivel)
            End If
            If _nrseqsolicitadopor <> 0 Then
                xsql &= ",nrseqsolicitadopor = " & moeda(_nrseqsolicitadopor)
            End If
            If _horachegada <> "" Then
                xsql &= ",horachegada = '" & tratatexto(_horachegada) & "'"
            End If
            If _horaatendido <> "" Then
                xsql &= ",horaatendido = '" & tratatexto(_horaatendido) & "'"
            End If
            If _recibo <> 0 Then
                xsql &= ",recibo = " & moeda(_recibo)
            End If
            If _nrseqconfirmadapor <> 0 Then
                xsql &= ",nrseqconfirmadapor = " & moeda(_nrseqconfirmadapor)
            End If
            If _dtconfirmada <> "" Then
                xsql &= ",dtconfirmada = '" & formatadatamysql(_dtconfirmada) & "'"
            End If
            If _horaconfirmada <> "" Then
                xsql &= ",horaconfirmada = '" & tratatexto(_horaconfirmada) & "'"
            End If
            If _nrseqcanceladopor <> 0 Then
                xsql &= ",nrseqcanceladopor = " & moeda(_nrseqcanceladopor)
            End If

            If _nrseqatendidopor <> 0 Then
                xsql &= ",nrseqatendidopor = " & moeda(_nrseqatendidopor)
            End If
            If _encaixe <> 0 Then
                xsql &= ",encaixe = " & moeda(_encaixe)
            End If
            If _tela <> "" Then
                xsql &= ",tela = '" & tratatexto(_tela) & "'"
            End If
            If _faturado <> 0 Then
                xsql &= ",faturado = " & moeda(_faturado)
            End If
            If _dtfaturado <> "" Then
                xsql &= ",dtfaturado = '" & formatadatamysql(_dtfaturado) & "'"
            End If
            If _userfaturado <> "" Then
                xsql &= ",userfaturado = '" & tratatexto(_userfaturado) & "'"
            End If
            If _nrguiaconvenio <> "" Then
                xsql &= ",nrguiaconvenio = '" & tratatexto(_nrguiaconvenio) & "'"
            End If
            If _dtalterado <> "" Then
                xsql &= ",dtalterado = '" & formatadatamysql(_dtalterado) & "'"
            End If
            If _useralterado <> "" Then
                xsql &= ",useralterado = '" & tratatexto(_useralterado) & "'"
            End If
            If _formapagto <> "" Then
                xsql &= ",formapagto = '" & tratatexto(_formapagto) & "'"
            End If
            If _qtdparcelas <> 0 Then
                xsql &= ",qtdparcelas = " & moeda(_qtdparcelas)
            End If
            If _senhaatendimento <> "" Then
                xsql &= ",senhaatendimento = '" & tratatexto(_senhaatendimento) & "'"
            End If
            If _repassemanual <> "" Then
                xsql &= ",repassemanual = '" & tratatexto(_repassemanual) & "'"
            End If
            If _nrseqctrl <> "" Then
                xsql &= ",nrseqctrl = '" & tratatexto(_nrseqctrl) & "'"
            End If
            If _nrseqrecibo <> 0 Then
                xsql &= ",nrseqrecibo = " & moeda(_nrseqrecibo)
            End If
            If _repasseconfirmado <> 0 Then
                xsql &= ",repasseconfirmado = " & moeda(_repasseconfirmado)
            End If
            If _dtrepasseconfirmado <> "" Then
                xsql &= ",dtrepasseconfirmado = '" & formatadatamysql(_dtrepasseconfirmado) & "'"
            End If
            If _userrepasseconfirmado <> "" Then
                xsql &= ",userrepasseconfirmado = '" & tratatexto(_userrepasseconfirmado) & "'"
            End If
            If _dtnfe <> "" Then
                xsql &= ",dtnfe = '" & formatadatamysql(_dtnfe) & "'"
            End If
            If _usernfe <> "" Then
                xsql &= ",usernfe = '" & tratatexto(_usernfe) & "'"
            End If
            If _nrnfe <> "" Then
                xsql &= ",nrnfe = '" & tratatexto(_nrnfe) & "'"
            End If
            If _nrlotenfe <> "" Then
                xsql &= ",nrlotenfe = '" & tratatexto(_nrlotenfe) & "'"
            End If
            If _nfe <> 0 Then
                xsql &= ",nfe = " & moeda(_nfe)
            End If
            If _nrseqconfirmacao <> 0 Then
                xsql &= ",nrseqconfirmacao = " & moeda(_nrseqconfirmacao)
            End If
            If _ehtitular <> 0 Then
                xsql &= ",ehtitular = " & moeda(_ehtitular)
            End If
            If _userconfirmoupresenca <> "" Then
                xsql &= ", Userconfirmoupresenca = '" & tratatexto(_userconfirmoupresenca) & "'"
            End If
            If _nrseqcaixadth <> 0 Then
                xsql &= ", nrseqcaixadth = '" & numeros(_nrseqcaixadth) & "'"
            End If
            If _userconfirmouconsulta <> "" Then
                xsql &= ", userconfirmouconsulta = '" & tratatexto(_userconfirmouconsulta) & "'"
            End If
            If valordatamysql(_dtconfirmouconsulta) <> "" Then
                xsql &= ", dtconfirmouconsulta = '" & valordatamysql(_dtconfirmouconsulta) & "'"
            End If

            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function salvarmarcarconsulta() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & tablename & " Set ativo = True,nrseqcaixadth='" & _nrseqcaixadth & "',cancelado=False, valor =  " & moeda(_valor)
            '    If _nrseq <> 0 Then
            '    xsql &= ",nrseq = '" & _nrseq & "'"
            '    End If
            If _nrseqpaciente <> 0 Then
                xsql &= ",nrseqpaciente = '" & sonumeros(_nrseqpaciente) & "'"
            End If
            If _nrseqdependente <> 0 Then
                xsql &= ",nrseqdependente = '" & sonumeros(_nrseqdependente) & "'"
            End If
            If _nrseqmedico <> 0 Then
                xsql &= ",nrseqmedico = '" & sonumeros(_nrseqmedico) & "'"
            End If

            If _nrseqespecialidade <> 0 Then
                xsql &= ",nrseqespecialidade = '" & sonumeros(_nrseqespecialidade) & "'"
            End If
            If _nrseqservico <> 0 Then
                xsql &= ",nrseqservico = '" & sonumeros(_nrseqservico) & "'"
            End If
            If formatadatamysql(_data) <> "" Then
                xsql &= ",data = '" & formatadatamysql(_data) & "'"
            End If
            If _hora <> "" Then
                xsql &= ",hora = '" & _hora & "'"
            End If
            If logico(_atendido) IsNot Nothing Then
                xsql &= ",atendido =   " & logico(_atendido) & ""
            End If
            If logico(_cancelado) IsNot Nothing Then
                xsql &= ",cancelado =   " & logico(_cancelado) & ""
            End If
            If logico(_retorno) IsNot Nothing Then
                xsql &= ",retorno =     " & logico(_retorno) & ""
            End If
            If formatadatamysql(_dtcad) <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _nrseqplano <> 0 Then
                xsql &= ",nrseqplano =  '" & sonumeros(_nrseqplano) & "'"
            End If
            If _nrseqprocedimento <> 0 Then
                xsql &= ",nrseqprocedimento =   '" & sonumeros(_nrseqprocedimento) & "'"
            End If
            If _nrseqempresa <> 0 Then
                xsql &= ",nrseqempresa ='" & sonumeros(_nrseqempresa) & "'"
            End If
            If _ctrl <> 0 Then
                xsql &= ",ctrl =    '" & sonumeros(_ctrl) & "'"
            End If
            If _paciente <> "" Then
                xsql &= ",paciente = '" & tratatexto(_paciente) & "'"
            End If
            If _valorrep <> 0 Then
                xsql &= ",valorrep = '" & moeda(_valorrep) & "'"
            End If
            If logico(_confirmada) IsNot Nothing Then
                xsql &= ",confirmada = " & logico(_confirmada) & ""
            End If
            If logico(_aguardando) IsNot Nothing Then
                xsql &= ",aguardando = " & logico(_aguardando) & ""
            End If
            If _obs <> "" Then
                xsql &= ",obs = '" & tratatexto(_obs) & "'"
            End If
            If logico(_indisponivel) IsNot Nothing Then
                xsql &= ",indisponivel = " & logico(_indisponivel) & ""
            End If
            If _nrseqsolicitadopor <> 0 Then
                xsql &= ",nrseqsolicitadopor = '" & sonumeros(_nrseqsolicitadopor) & "'"
            End If
            If _horachegada <> "" Then
                xsql &= ",horachegada = '" & tratatexto(_horachegada) & "'"
            End If
            If _horaatendido <> "" Then
                xsql &= ",horaatendido = '" & tratatexto(_horaatendido) & "'"
            End If
            If _recibo <> 0 Then
                xsql &= ",recibo = '" & sonumeros(_recibo) & "'"
            End If
            If _nrseqconfirmadapor <> 0 Then
                xsql &= ",nrseqconfirmadapor = '" & sonumeros(_nrseqconfirmadapor) & "'"
            End If
            If formatadatamysql(_dtconfirmada) <> "" Then
                xsql &= ",dtconfirmada = '" & formatadatamysql(_dtconfirmada) & "'"
            End If
            If _horaconfirmada <> "" Then
                xsql &= ",horaconfirmada = '" & tratatexto(_horaconfirmada) & "'"
            End If
            If _nrseqcanceladopor <> 0 Then
                xsql &= ",nrseqcanceladopor = '" & sonumeros(_nrseqcanceladopor) & "'"
            End If
            If _nrseqatendidopor <> 0 Then
                xsql &= ",nrseqatendidopor = '" & sonumeros(_nrseqatendidopor) & "'"
            End If
            If logico(_encaixe) IsNot Nothing Then
                xsql &= ",encaixe = " & logico(_encaixe) & ""
            End If
            If _tela <> "" Then
                xsql &= ",tela = '" & tratatexto(_tela) & "'"
            End If
            If logico(_faturado) IsNot Nothing Then
                xsql &= ",faturado = " & logico(_faturado) & ""
            End If
            If formatadatamysql(_dtfaturado) <> "" Then
                xsql &= ",dtfaturado = '" & formatadatamysql(_dtfaturado) & "'"
            End If
            If _userfaturado <> "" Then
                xsql &= ",userfaturado = '" & tratatexto(_userfaturado) & "'"
            End If
            If _nrguiaconvenio <> "" Then
                xsql &= ",nrguiaconvenio = '" & tratatexto(_nrguiaconvenio) & "'"
            End If
            If formatadatamysql(_dtalterado) <> "" Then
                xsql &= ",dtalterado = '" & formatadatamysql(_dtalterado) & "'"
            End If
            If _useralterado <> "" Then
                xsql &= ",useralterado = '" & tratatexto(_useralterado) & "'"
            End If
            If _formapagto <> "" Then
                xsql &= ",formapagto = '" & tratatexto(_formapagto) & "'"
            End If
            If _qtdparcelas <> 0 Then
                xsql &= ",qtdparcelas = '" & sonumeros(_qtdparcelas) & "'"
            End If
            If _senhaatendimento <> "" Then
                xsql &= ",senhaatendimento = '" & tratatexto(_senhaatendimento) & "'"
            End If
            If _repassemanual <> 0 Then
                xsql &= ",repassemanual = '" & moeda(_repassemanual) & "'"
            End If
            If _nrseqrecibo <> 0 Then
                xsql &= ",nrseqrecibo = '" & sonumeros(_nrseqrecibo) & "'"
            End If
            If logico(_repasseconfirmado) IsNot Nothing Then
                xsql &= ",repasseconfirmado = '" & logico(_repasseconfirmado) & "'"
            End If
            If formatadatamysql(_dtrepasseconfirmado) <> "" Then
                xsql &= ",dtrepasseconfirmado = '" & formatadatamysql(_dtrepasseconfirmado) & "'"
            End If
            If _userrepasseconfirmado <> "" Then
                xsql &= ",userrepasseconfirmado = '" & tratatexto(_userrepasseconfirmado) & "'"
            End If
            If formatadatamysql(_dtnfe) <> "" Then
                xsql &= ",dtnfe = '" & formatadatamysql(_dtnfe) & "'"
            End If
            If _usernfe <> "" Then
                xsql &= ",usernfe = '" & tratatexto(_usernfe) & "'"
            End If
            If _nrnfe <> "" Then
                xsql &= ",nrnfe = '" & tratatexto(_nrnfe) & "'"
            End If
            If _nrlotenfe <> "" Then
                xsql &= ",nrlotenfe = '" & tratatexto(_nrlotenfe) & "'"
            End If
            If _nfe <> 0 Then
                xsql &= ",nfe = " & sonumeros(_nfe)
            End If
            If _nrseqconfirmacao <> 0 Then
                xsql &= ",nrseqconfirmacao = '" & sonumeros(_nrseqconfirmacao) & "'"
            End If
            If logico(_ehtitular) IsNot Nothing Then
                xsql &= ", ehtitular = " & logico(_ehtitular) & ""
            End If
            If logico(_primeiraconsulta) IsNot Nothing Then
                xsql &= ", primeiraconsulta = " & logico(_primeiraconsulta) & ""
            End If
            If _usermarcouconsulta <> "" Then
                xsql &= ", usermarcouconsulta = '" & tratatexto(_usermarcouconsulta) & "'"
            End If
            If formatadatamysql(_dtmarcado) <> "" Then
                xsql &= ", dtmarcado = '" & formatadatamysql(_dtmarcado) & "'"
            End If
            If _useratendeu <> "" Then
                xsql &= ", useratendeu = '" & tratatexto(_useratendeu) & "'"
            End If
            If formatadatamysql(_dtatendido) <> "" Then
                xsql &= ", dtatendido = '" & formatadatamysql(_dtatendido) & "'"
            End If
            If _userconfirmoupresenca <> "" Then
                xsql &= ", Userconfirmoupresenca = '" & tratatexto(_userconfirmoupresenca) & "'"
            End If
            If _nrseqcaixadth <> 0 Then
                xsql &= ", nrseqcaixadth = '" & numeros(_nrseqcaixadth) & "'"
            End If
            If _userconfirmouconsulta <> "" Then
                xsql &= ", userconfirmouconsulta = '" & tratatexto(_userconfirmouconsulta) & "'"
            End If
            If valordatamysql(_dtconfirmouconsulta) <> "" Then
                xsql &= ", dtconfirmouconsulta = '" & valordatamysql(_dtconfirmouconsulta) & "'"
            End If
            xsql &= " where nrseq = '" & _nrseq & "'"

            tb1 = tab1.IncluirAlterarDados(xsql)

            Return True

        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function



    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & "  (nrseqctrl, dtcad, usercad, ativo,cancelado) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', False,False)")
            tb1 = tab1.conectar("Select * from  " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function cancelar() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo = false,cancelado= True,usercancelado='" & buscarsessoes("usuario") & "',dtcancelado='" & hoje() & "' where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
    Public Function marcarconsulta() As Boolean
        Try
            If _paciente = "" Then
                _mensagemerro = "Informe um paciente válido !"
                Return False
            End If
            If _especialidade = "" Then
                _mensagemerro = "Informe uma especialidade !"
                Return False
            End If
            If _hora = "" Then
                _mensagemerro = "Selecione um horário livre !"
                Return False
            End If
            If Not IsDate(_data) Then
                _mensagemerro = "Selecione uma data válida !"
                Return False
            End If
            If _plano = "" Then
                _mensagemerro = "Selecione um plano válido !"
                Return False
            End If

            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString

            If _chave <> "" Then
                tb1 = tab1.IncluirAlterarDados("update tbagendamedica_procedimentos set nrseqagenda = " & _nrseq & " where chave = '" & _chave & "'")
            End If


            Dim xsql As String
            xsql = " update " & tablename & " Set nrseqcaixadth='" & _nrseqcaixadth & "', ativo = True,cancelado=False, ehtitular = " & logico(Ehtitular)

            If _nrseqpaciente <> 0 Then
                xsql &= ",nrseqpaciente = '" & sonumeros(_nrseqpaciente) & "'"
            End If
            If _nrseqdependente <> 0 Then
                xsql &= ",nrseqdependente = '" & sonumeros(_nrseqdependente) & "'"
            End If
            If _nrseqmedico <> 0 Then
                xsql &= ",nrseqmedico = '" & sonumeros(_nrseqmedico) & "'"
            End If

            If _nrseqespecialidade <> 0 Then
                xsql &= ",nrseqespecialidade = '" & sonumeros(_nrseqespecialidade) & "'"
            End If
            If _nrseqservico <> 0 Then
                xsql &= ",nrseqservico = '" & sonumeros(_nrseqservico) & "'"
            End If
            If formatadatamysql(_data) <> "" Then
                xsql &= ",data = '" & formatadatamysql(_data) & "'"
            End If
            If _hora <> "" Then
                xsql &= ",hora = '" & _hora & "'"
            End If
            If logico(_atendido) IsNot Nothing Then
                xsql &= ",atendido =   " & logico(_atendido) & ""
            End If
            If logico(_cancelado) IsNot Nothing Then
                xsql &= ",cancelado =   " & logico(_cancelado) & ""
            End If
            If logico(_retorno) IsNot Nothing Then
                xsql &= ",retorno = " & logico(_retorno) & ""
            End If
            If formatadatamysql(_dtcad) <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _nrseqplano <> 0 Then
                xsql &= ",nrseqplano =  '" & sonumeros(_nrseqplano) & "'"
            End If
            If _nrseqprocedimento <> 0 Then
                xsql &= ",nrseqprocedimento =   '" & sonumeros(_nrseqprocedimento) & "'"
            End If
            If _nrseqempresa <> 0 Then
                xsql &= ",nrseqempresa ='" & sonumeros(_nrseqempresa) & "'"
            End If
            If _ctrl <> 0 Then
                xsql &= ",ctrl ='" & sonumeros(_ctrl) & "'"
            End If
            If _paciente <> "" Then
                xsql &= ",paciente = '" & tratatexto(_paciente) & "'"
            End If
            If _valor <> 0 Then
                xsql &= ",valor = '" & moeda(_valor) & "'"
            End If
            If _valorrep <> 0 Then
                xsql &= ",valorrep = '" & moeda(_valorrep) & "'"
            End If
            If logico(_confirmada) IsNot Nothing Then
                xsql &= ",confirmada = " & logico(_confirmada) & ""
            End If
            If logico(_aguardando) IsNot Nothing Then
                xsql &= ",aguardando = " & logico(_aguardando) & ""
            End If
            If _obs <> "" Then
                xsql &= ",obs = '" & tratatexto(_obs) & "'"
            End If
            If logico(_indisponivel) IsNot Nothing Then
                xsql &= ",indisponivel = " & logico(_indisponivel) & ""
            End If
            If _nrseqsolicitadopor <> 0 Then
                xsql &= ",nrseqsolicitadopor = '" & sonumeros(_nrseqsolicitadopor) & "'"
            End If
            If _horachegada <> "" Then
                xsql &= ",horachegada = '" & tratatexto(_horachegada) & "'"
            End If
            If _horaatendido <> "" Then
                xsql &= ",horaatendido = '" & tratatexto(_horaatendido) & "'"
            End If
            If _recibo <> 0 Then
                xsql &= ",recibo = '" & sonumeros(_recibo) & "'"
            End If
            If _nrseqconfirmadapor <> 0 Then
                xsql &= ",nrseqconfirmadapor = '" & sonumeros(_nrseqconfirmadapor) & "'"
            End If
            If formatadatamysql(_dtconfirmada) <> "" Then
                xsql &= ",dtconfirmada = '" & formatadatamysql(_dtconfirmada) & "'"
            End If
            If _horaconfirmada <> "" Then
                xsql &= ",horaconfirmada = '" & tratatexto(_horaconfirmada) & "'"
            End If
            If _nrseqcanceladopor <> 0 Then
                xsql &= ",nrseqcanceladopor = '" & sonumeros(_nrseqcanceladopor) & "'"
            End If
            If _nrseqatendidopor <> 0 Then
                xsql &= ",nrseqatendidopor = '" & sonumeros(_nrseqatendidopor) & "'"
            End If
            If logico(_encaixe) IsNot Nothing Then
                xsql &= ",encaixe = " & logico(_encaixe) & ""
            End If
            If _tela <> "" Then
                xsql &= ",tela = '" & tratatexto(_tela) & "'"
            End If
            If logico(_faturado) IsNot Nothing Then
                xsql &= ",faturado = " & logico(_faturado) & ""
            End If
            If formatadatamysql(_dtfaturado) <> "" Then
                xsql &= ",dtfaturado = '" & formatadatamysql(_dtfaturado) & "'"
            End If
            If _userfaturado <> "" Then
                xsql &= ",userfaturado = '" & tratatexto(_userfaturado) & "'"
            End If
            If _nrguiaconvenio <> "" Then
                xsql &= ",nrguiaconvenio = '" & tratatexto(_nrguiaconvenio) & "'"
            End If
            If formatadatamysql(_dtalterado) <> "" Then
                xsql &= ",dtalterado = '" & formatadatamysql(_dtalterado) & "'"
            End If
            If _useralterado <> "" Then
                xsql &= ",useralterado = '" & tratatexto(_useralterado) & "'"
            End If
            If _formapagto <> "" Then
                xsql &= ",formapagto = '" & tratatexto(_formapagto) & "'"
            End If
            If _qtdparcelas <> 0 Then
                xsql &= ",qtdparcelas = '" & sonumeros(_qtdparcelas) & "'"
            End If
            If _senhaatendimento <> "" Then
                xsql &= ",senhaatendimento = '" & tratatexto(_senhaatendimento) & "'"
            End If
            If _repassemanual <> 0 Then
                xsql &= ",repassemanual = '" & moeda(_repassemanual) & "'"
            End If
            If _nrseqrecibo <> 0 Then
                xsql &= ",nrseqrecibo = '" & sonumeros(_nrseqrecibo) & "'"
            End If
            If logico(_repasseconfirmado) IsNot Nothing Then
                xsql &= ",repasseconfirmado = '" & logico(_repasseconfirmado) & "'"
            End If
            If formatadatamysql(_dtrepasseconfirmado) <> "" Then
                xsql &= ",dtrepasseconfirmado = '" & formatadatamysql(_dtrepasseconfirmado) & "'"
            End If
            If _userrepasseconfirmado <> "" Then
                xsql &= ",userrepasseconfirmado = '" & tratatexto(_userrepasseconfirmado) & "'"
            End If
            If formatadatamysql(_dtnfe) <> "" Then
                xsql &= ",dtnfe = '" & formatadatamysql(_dtnfe) & "'"
            End If
            If _usernfe <> "" Then
                xsql &= ",usernfe = '" & tratatexto(_usernfe) & "'"
            End If
            If _nrnfe <> "" Then
                xsql &= ",nrnfe = '" & tratatexto(_nrnfe) & "'"
            End If
            If _nrlotenfe <> "" Then
                xsql &= ",nrlotenfe = '" & tratatexto(_nrlotenfe) & "'"
            End If
            If _nfe <> 0 Then
                xsql &= ",nfe = " & sonumeros(_nfe)
            End If
            If _nrseqconfirmacao <> 0 Then
                xsql &= ",nrseqconfirmacao = '" & sonumeros(_nrseqconfirmacao) & "'"
            End If
            If logico(_ehtitular) IsNot Nothing Then
                xsql &= ", ehtitular = " & logico(_ehtitular) & ""
            End If
            If logico(_primeiraconsulta) IsNot Nothing Then
                xsql &= ", primeiraconsulta = " & logico(_primeiraconsulta) & ""
            End If
            If formatadatamysql(_dtcancelado) <> "" Then
                xsql &= ", dtcancelado = '" & formatadatamysql(_dtcancelado) & "'"
            End If
            If _usermarcouconsulta <> "" Then
                xsql &= ", usermarcouconsulta = '" & tratatexto(_usermarcouconsulta) & "'"
            End If
            If formatadatamysql(_dtmarcado) <> "" Then
                xsql &= ", dtmarcado = '" & formatadatamysql(_dtmarcado) & "'"
            End If
            If _useratendeu <> "" Then
                xsql &= ", useratendeu = '" & tratatexto(_useratendeu) & "'"
            End If
            If formatadatamysql(_dtatendido) <> "" Then
                xsql &= ", dtatendido = '" & formatadatamysql(_dtatendido) & "'"
            End If
            If _userconfirmoupresenca <> "" Then
                xsql &= ", Userconfirmoupresenca = '" & tratatexto(_userconfirmoupresenca) & "'"
            End If
            If _nrseqcaixadth <> 0 Then
                xsql &= ", nrseqcaixadth = '" & numeros(_nrseqcaixadth) & "'"
            End If
            If _userconfirmouconsulta <> "" Then
                xsql &= ", userconfirmouconsulta = '" & tratatexto(_userconfirmouconsulta) & "'"
            End If
            If valordatamysql(_dtconfirmouconsulta) <> "" Then
                xsql &= ", dtconfirmouconsulta = '" & valordatamysql(_dtconfirmouconsulta) & "'"
            End If
            xsql &= " where nrseq = '" & _nrseq & "'"

            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exmarcar As Exception
            Return False
        End Try

    End Function
    Public Function carregaragendames() As Boolean
        Try
            Agendames.Clear()
            tb1 = tab1.conectar("select nomemedico, count(*) as total,data   from vwagenda where month(data) = " & Month(_data) & " and year(data) = '" & Year(_data) & "' and ativo = true group by nomemedico,data order by nomemedico")
            For x As Integer = 0 To tb1.Rows.Count - 1

                Agendames.Add(New clsAgenda_medica With {.Data = tb1.Rows(x)("data").ToString, .Nomemedico = tb1.Rows(x)("nomemedico").ToString, .Contador = tb1.Rows(x)("total").ToString})
            Next

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function carregaragendaDiaMedico(dataselecionada As String) As Boolean
        Try
            Agendadia.Clear()


            tb1 = tab1.conectar("select *, substring(hora,2) as horaincompleta from vwagenda where data = '" & valordatamysql(_data) & "' and ativo = true and nomemedico = '" & _nomemedico & "'")

            tbx = tabx.conectar("select * from tbmedicos_horarios left join tbagendamedica on tbmedicos_horarios.nrseqmedico=tbagendamedica.nrseqmedico where tbmedicos_horarios.nrseqmedico = " & _nrseqmedico & " and tbmedicos_horarios.diasemana = " & _data.DayOfWeek + 1 & " and not exists(select hora from tbagendamedica where tbagendamedica.data='" & valordatamysql(dataselecionada) & "' and tbagendamedica.hora = tbmedicos_horarios.hora) group by tbmedicos_horarios.hora order by tbmedicos_horarios.hora")
            'tbx = tabx.conectar("Select * From tbmedicos_horarios left Join tbagendamedica On tbmedicos_horarios.hora=tbagendamedica.hora Where tbmedicos_horarios.diasemana = '" & _data.DayOfWeek + 1 & "' And tbmedicos_horarios.nrseqmedico ='" & _nrseqmedico & "' and tbagendamedica.hora is null  order by tbmedicos_horarios.hora")

            For y As Integer = 0 To tbx.Rows.Count - 1
                Dim tbproc As Data.DataRow()
                tbproc = tb1.Select("horaincompleta =  '" & tbx.Rows(y)("hora").ToString.Substring(0, 2) & "'")
                If tbproc.Count = 0 Then
                    Agendadia.Add(New clsAgenda_medica With {.Data = _data, .Nrseqpaciente = 0, .Paciente = "", .Hora = tbx.Rows(y)("hora").ToString, .Userfaturado = "", .Useralterado = "", .Usercad = "", .Userrepasseconfirmado = "", .Valor = 0, .Valorrep = 0, .Ativo = True})
                Else
                    Agendadia.Add(New clsAgenda_medica With {.Data = tbproc(0)("data").ToString, .Nrseqpaciente = tbproc(0)("nrseqpaciente").ToString, .Paciente = tbproc(0)("paciente").ToString, .Hora = tbproc(0)("hora").ToString, .Userfaturado = tbproc(0)("Userfaturado").ToString, .Useralterado = tbproc(0)("Useralterado").ToString, .Usercad = tbproc(0)("Usercad").ToString, .Usernfe = tbproc(0)("Usernfe").ToString, .Userrepasseconfirmado = tbproc(0)("Userrepasseconfirmado").ToString, .Valor = numeros(tbproc(0)("Valor").ToString), .Valorrep = numeros(tbproc(0)("Valorrep").ToString), .Ativo = logico(tbproc(0)("Ativo").ToString), .Aguardando = logico(tbproc(0)("Aguardando").ToString), .Atendido = logico(tbproc(0)("Atendido").ToString), .Cancelado = logico(tbproc(0)("Cancelado").ToString), .Confirmada = logico(tbproc(0)("Confirmada").ToString), .Dtalterado = valordata(tbproc(0)("Dtalterado").ToString), .Dtcad = valordata(tbproc(0)("Dtcad").ToString), .Dtcancelado = valordata(tbproc(0)("Dtcancelado").ToString), .Dtconfirmada = valordata(tbproc(0)("Dtconfirmada").ToString), .Dtfaturado = valordata(tbproc(0)("Dtfaturado").ToString), .Dtnfe = valordata(tbproc(0)("paciente").ToString), .Ehtitular = logico(tbproc(0)("Dtnfe").ToString), .Dtrepasseconfirmado = valordata(tbproc(0)("Dtrepasseconfirmado").ToString), .Faturado = logico(tbproc(0)("Faturado").ToString), .Formapagto = tbproc(0)("Formapagto").ToString, .Horaatendido = tbproc(0)("Horaatendido").ToString, .Horachegada = tbproc(0)("Horachegada").ToString, .Horaconfirmada = tbproc(0)("Horaconfirmada").ToString, .Indisponivel = logico(tbproc(0)("Indisponivel").ToString), .Nfe = tbproc(0)("Nfe").ToString, .Nrguiaconvenio = tbproc(0)("Nrguiaconvenio").ToString, .Nrlotenfe = tbproc(0)("Nrlotenfe").ToString, .Nrnfe = tbproc(0)("Nrnfe").ToString, .Nrseqmedico = numeros(tbproc(0)("Nrseqmedico").ToString), .Obs = tbproc(0)("Obs").ToString, .Retorno = logico(tbproc(0)("Retorno").ToString), .Senhaatendimento = tbproc(0)("Senhaatendimento").ToString, .Nomecliente = tbproc(0)("Nomecliente").ToString, .Nomedependente = tbproc(0)("Nomedependente").ToString, .Nomemedico = tbproc(0)("Nomemedico").ToString})
                End If
            Next


            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function carregaragendaDiaMedicoAgendado(dataselecionada As String) As Boolean
        Try
            Agendadia.Clear()


            tb1 = tab1.conectar("select *, substring(hora,2) as horaincompleta from vwagenda where data = '" & formatadatamysql(_data) & "' and ativo = true and nomemedico = '" & _nomemedico & "'")
            tbx = tabx.conectar("Select * From tbmedicos_horarios left Join tbagendamedica On tbmedicos_horarios.hora=tbagendamedica.hora Where tbmedicos_horarios.diasemana = '" & _data.DayOfWeek + 1 & "' And tbmedicos_horarios.nrseqmedico ='" & _nrseqmedico & "' and tbagendamedica.hora is null  order by tbmedicos_horarios.hora")
            For i As Integer = 0 To tbx.Rows.Count - 1

            Next
            For y As Integer = 0 To tbx.Rows.Count - 1
                Dim tbproc As Data.DataRow()
                tbproc = tb1.Select("horaincompleta =  '" & tbx.Rows(y)("hora").ToString.Substring(0, 2) & "'")
                If tbproc.Count = 0 Then
                    Agendadia.Add(New clsAgenda_medica With {.Data = _data, .Nrseqpaciente = 0, .Paciente = "", .Hora = tbx.Rows(y)("hora").ToString, .Userfaturado = "", .Useralterado = "", .Usercad = "", .Userrepasseconfirmado = "", .Valor = 0, .Valorrep = 0, .Ativo = True})
                Else
                    Agendadia.Add(New clsAgenda_medica With {.Data = tbproc(0)("data").ToString, .Nrseqpaciente = tbproc(0)("nrseqpaciente").ToString, .Paciente = tbproc(0)("paciente").ToString, .Hora = tbproc(0)("hora").ToString, .Userfaturado = tbproc(0)("Userfaturado").ToString, .Useralterado = tbproc(0)("Useralterado").ToString, .Usercad = tbproc(0)("Usercad").ToString, .Usernfe = tbproc(0)("Usernfe").ToString, .Userrepasseconfirmado = tbproc(0)("Userrepasseconfirmado").ToString, .Valor = numeros(tbproc(0)("Valor").ToString), .Valorrep = numeros(tbproc(0)("Valorrep").ToString), .Ativo = logico(tbproc(0)("Ativo").ToString), .Aguardando = logico(tbproc(0)("Aguardando").ToString), .Atendido = logico(tbproc(0)("Atendido").ToString), .Cancelado = logico(tbproc(0)("Cancelado").ToString), .Confirmada = logico(tbproc(0)("Confirmada").ToString), .Dtalterado = valordata(tbproc(0)("Dtalterado").ToString), .Dtcad = valordata(tbproc(0)("Dtcad").ToString), .Dtcancelado = valordata(tbproc(0)("Dtcancelado").ToString), .Dtconfirmada = valordata(tbproc(0)("Dtconfirmada").ToString), .Dtfaturado = valordata(tbproc(0)("Dtfaturado").ToString), .Dtnfe = valordata(tbproc(0)("paciente").ToString), .Ehtitular = logico(tbproc(0)("Dtnfe").ToString), .Dtrepasseconfirmado = valordata(tbproc(0)("Dtrepasseconfirmado").ToString), .Faturado = logico(tbproc(0)("Faturado").ToString), .Formapagto = tbproc(0)("Formapagto").ToString, .Horaatendido = tbproc(0)("Horaatendido").ToString, .Horachegada = tbproc(0)("Horachegada").ToString, .Horaconfirmada = tbproc(0)("Horaconfirmada").ToString, .Indisponivel = logico(tbproc(0)("Indisponivel").ToString), .Nfe = tbproc(0)("Nfe").ToString, .Nrguiaconvenio = tbproc(0)("Nrguiaconvenio").ToString, .Nrlotenfe = tbproc(0)("Nrlotenfe").ToString, .Nrnfe = tbproc(0)("Nrnfe").ToString, .Nrseqmedico = numeros(tbproc(0)("Nrseqmedico").ToString), .Obs = tbproc(0)("Obs").ToString, .Retorno = logico(tbproc(0)("Retorno").ToString), .Senhaatendimento = tbproc(0)("Senhaatendimento").ToString, .Nomecliente = tbproc(0)("Nomecliente").ToString, .Nomedependente = tbproc(0)("Nomedependente").ToString, .Nomemedico = tbproc(0)("Nomemedico").ToString})
                End If
            Next


            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function carregaragendaDiaMedicoA(soatendimentos As Boolean) As Boolean
        Try
            Agendadia.Clear()
            Dim xsql As String = "select *, substring(hora,2) as horaincompleta from vwagenda where data = '" & formatadatamysql(_data) & "' and ativo = true "
            If _nomemedico <> "" Then
                xsql &= " and nomemedico = '" & _nomemedico & "'"
            End If
            tb1 = tab1.conectar()

            For y As Integer = 0 To tb1.Rows.Count - 1

                Agendadia.Add(New clsAgenda_medica With {.Data = tb1.Rows(y)("data").ToString, .Nrseqpaciente = tb1.Rows(y)("nrseqpaciente").ToString, .Paciente = tb1.Rows(y)("paciente").ToString, .Hora = tb1.Rows(y)("hora").ToString, .Userfaturado = tb1.Rows(y)("Userfaturado").ToString, .Useralterado = tb1.Rows(y)("Useralterado").ToString, .Usercad = tb1.Rows(y)("Usercad").ToString, .Usernfe = tb1.Rows(y)("Usernfe").ToString, .Userrepasseconfirmado = tb1.Rows(y)("Userrepasseconfirmado").ToString, .Valor = numeros(tb1.Rows(y)("Valor").ToString), .Valorrep = numeros(tb1.Rows(y)("Valorrep").ToString), .Ativo = logico(tb1.Rows(y)("Ativo").ToString), .Aguardando = logico(tb1.Rows(y)("Aguardando").ToString), .Atendido = logico(tb1.Rows(y)("Atendido").ToString), .Cancelado = logico(tb1.Rows(y)("Cancelado").ToString), .Confirmada = logico(tb1.Rows(y)("Confirmada").ToString), .Dtalterado = valordata(tb1.Rows(y)("Dtalterado").ToString), .Dtcad = valordata(tb1.Rows(y)("Dtcad").ToString), .Dtcancelado = valordata(tb1.Rows(y)("Dtcancelado").ToString), .Dtconfirmada = valordata(tb1.Rows(y)("Dtconfirmada").ToString), .Dtfaturado = valordata(tb1.Rows(y)("Dtfaturado").ToString), .Dtnfe = valordata(tb1.Rows(y)("paciente").ToString), .Ehtitular = logico(tb1.Rows(y)("Dtnfe").ToString), .Dtrepasseconfirmado = valordata(tb1.Rows(y)("Dtrepasseconfirmado").ToString), .Faturado = logico(tb1.Rows(y)("Faturado").ToString), .Formapagto = tb1.Rows(y)("Formapagto").ToString, .Horaatendido = tb1.Rows(y)("Horaatendido").ToString, .Horachegada = tb1.Rows(y)("Horachegada").ToString, .Horaconfirmada = tb1.Rows(y)("Horaconfirmada").ToString, .Indisponivel = logico(tb1.Rows(y)("Indisponivel").ToString), .Nfe = tb1.Rows(y)("Nfe").ToString, .Nrguiaconvenio = tb1.Rows(y)("Nrguiaconvenio").ToString, .Nrlotenfe = tb1.Rows(y)("Nrlotenfe").ToString, .Nrnfe = tb1.Rows(y)("Nrnfe").ToString, .Nrseqmedico = numeros(tb1.Rows(y)("Nrseqmedico").ToString), .Obs = tb1.Rows(y)("Obs").ToString, .Retorno = logico(tb1.Rows(y)("Retorno").ToString), .Senhaatendimento = tb1.Rows(y)("Senhaatendimento").ToString, .Nomecliente = tb1.Rows(y)("Nomecliente").ToString, .Nomedependente = tb1.Rows(y)("Nomedependente").ToString, .Nomemedico = tb1.Rows(y)("Nomemedico").ToString})
            Next

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class


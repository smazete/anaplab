﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports clsSmart
Imports clssessoes

Public Class clslogin

    Dim _usuario As String

    Dim _senha As String

    Dim _mensagemerro As String

    Dim _tentativas As Integer

    Dim _urlretorno As String

    Dim _cliente As String = "Anaplab"

    Dim _produto As String = "Anaplab"



    Public Property Usuario As String
        Get
            Return _usuario
        End Get
        Set(value As String)
            _usuario = value
        End Set
    End Property

    Public Property Senha As String
        Get
            Return _senha
        End Get
        Set(value As String)
            _senha = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Tentativas As Integer
        Get
            Return _tentativas
        End Get
        Set(value As Integer)
            _tentativas = value
        End Set
    End Property

    Public Property Urlretorno As String
        Get
            Return _urlretorno
        End Get
        Set(value As String)
            _urlretorno = value
        End Set
    End Property

    Public Property Cliente As String
        Get
            Return _cliente
        End Get
        Set(value As String)
            _cliente = value
        End Set
    End Property

    Public Property Produto As String
        Get
            Return _produto
        End Get
        Set(value As String)
            _produto = value
        End Set
    End Property

    Public Function logar() As Boolean

        If Not loadcostumer() Then

        End If
        '   Dim xconfig As New clsconfig
        '  xconfig.carregar()

        Dim tblogin As New Data.DataTable
        Dim tablogin As New clsBanco
    '    Dim tbinforma As New Data.DataTable
        Dim tabinforma As New clsBanco
        Dim qtdtenta As Integer = 0
        If _usuario = "" Then
            _mensagemerro = "Entre com um usuário válido !"
            Return False
        End If

        tblogin = tablogin.conectar("Select * from vwusuarios where usuario = '" & _usuario & "'  ")
        If tblogin.Rows.Count = 0 Then
            _mensagemerro = "O usuário naõ existe  !"
            Return False
        Else
            qtdtenta = tblogin.Rows(0)("qtdtentativas").ToString
            If tblogin.Rows(0)("senha").ToString <> _senha Then
                If qtdtenta > 3 Then
                    _mensagemerro = "Desculpe, sua conta foi bloqueada! Entre em contato com seu administrador!"
                    ' tbinforma = tablogin.IncluirAlterarDados("update tbusuarios set suspenso = true, dtsuspenso = '" & hoje() & "', usersuspenso = '" & HttpContext.Current.Request.UserHostAddress & "' where usuario = '" & _usuario & "' and ativo = true ")
                    ' gravalog(0, _usuario, "Login", "Usuario " & _usuario & " Bloqueiou o acesso por excesso de tentativas com senha inválida !")
                    gravalog("Bloqueou o acesso ao sistema NewClinica às " & hoje() & " / " & hora(), "Acesso")
                    Dim xip As New clsbloqueioips
                    xip.Endereco = HttpContext.Current.Request.UserHostAddress
                    xip.gravarip()
                    Return False
                End If
                '    tbinforma = tablogin.IncluirAlterarDados("update tbusuarios set qtdtentativas = " & qtdtenta + 1 & " where usuario = '" & _usuario & "' and ativo = true ")
                ' gravalog(0, _usuario, "Login", "Usuario " & _usuario & " Errou a senha de acesso!")
                gravalog("Erro a senha no sistema Pad às " & hoje() & " / " & hora(), "Acesso")
                _mensagemerro = "Senha Inválida !"
                Return False
            End If
            If tblogin.Rows(0)("ativo").ToString = "0" Then
                _mensagemerro = "Acesso revogado: Sua senha foi excluída !"
                Return False
            End If
            If tblogin.Rows(0)("suspenso").ToString = "1" Then
                _mensagemerro = "Acesso revogado! Sua senha foi suspensa !"
                Return False
            End If

            HttpContext.Current.Session("id") = String.Format("Session ID: {0}", HttpContext.Current.Session.SessionID)
            HttpContext.Current.Session("idusuario") = tblogin.Rows(0)("nrseq").ToString
            HttpContext.Current.Session("idempresaemuso") = tblogin.Rows(0)("nrseqempresa").ToString
            HttpContext.Current.Session("master") = IIf(tblogin.Rows(0)("master").ToString = "1", "Sim", "Nao")
            HttpContext.Current.Session("usuario") = tblogin.Rows(0)("usuario").ToString
            '       HttpContext.Current.Session("usertransp") = tblogin.Rows(0)("nrseqtransp").ToString

            ' HttpContext.Current.Session("gestor") = IIf(tblogin.Rows(0)("gestor").ToString = "1", "Sim", "Nao")
            '    HttpContext.Current.Session("usuariomaster") = IIf(tblogin.Rows(0)("master").ToString = "1", "Sim", "Nao")
            '    HttpContext.Current.Session("nomeusuario") = tratanomes(IIf(tblogin.Rows(0)("nomecolaborador").ToString <> "", tblogin.Rows(0)("nomecolaborador").ToString, tblogin.Rows(0)("usuario").ToString))
            '      HttpContext.Current.Session("usuario") = tblogin.Rows(0)("usuario").ToString
            '    HttpContext.Current.Session("email") = tblogin.Rows(0)("Email").ToString
            '      HttpContext.Current.Session("imagemperfil") = tblogin.Rows(0)("imagemperfil").ToString
            '    HttpContext.Current.Session("permissao") = tblogin.Rows(0)("permissao").ToString
            '    HttpContext.Current.Session("nrseqcolaborador") = tblogin.Rows(0)("nrseqcolaborador").ToString
            '     HttpContext.Current.Session("novasenhaem") = tblogin.Rows(0)("novasenhaem").ToString

            Dim aCookie As New HttpCookie("Anaplab")
            aCookie.Values("id") = HttpContext.Current.Session("id")
            aCookie.Values("lastVisit") = DateTime.Now.ToString()
            aCookie.Values("usuario") = HttpContext.Current.Session("usuario")
            aCookie.Values("idusuario") = HttpContext.Current.Session("idusuario")
            aCookie.Values("master") = HttpContext.Current.Session("master")
            aCookie.Values("usuario") = HttpContext.Current.Session("usuario")
            aCookie.Values("idempresaemuso") = HttpContext.Current.Session("idempresaemuso")
            '       aCookie.Values("permissao") = HttpContext.Current.Session("permissao")
            '     aCookie.Values("nomeusuario") = HttpContext.Current.Session("nomeusuario")
            '     aCookie.Values("email") = HttpContext.Current.Session("email")
            '     aCookie.Values("gestor") = HttpContext.Current.Session("gestor")
            '    aCookie.Values("usuariomaster") = HttpContext.Current.Session("usuariomaster")
            '    aCookie.Values("nrseqcolaborador") = HttpContext.Current.Session("nrseqcolaborador")
            aCookie.Values("imagemperfil") = HttpContext.Current.Session("imagemperfil")
            '
            '      aCookie.Values("usertransp") = HttpContext.Current.Session("usertransp")
            '     aCookie.Values("nomecliente") = HttpContext.Current.Session("nomecliente")
            '     aCookie.Values("enderecocliente") = HttpContext.Current.Session("enderecocliente")
            '     aCookie.Values("bairrocliente") = HttpContext.Current.Session("bairrocliente")
            '       aCookie.Values("cidadecliente") = HttpContext.Current.Session("cidadecliente")
            '       aCookie.Values("ufcliente") = HttpContext.Current.Session("ufcliente")
            '       aCookie.Values("paiscliente") = HttpContext.Current.Session("paiscliente")
            '      aCookie.Values("telefonecliente") = HttpContext.Current.Session("telefonecliente")
            '      aCookie.Values("logocliente") = HttpContext.Current.Session("logocliente")
            '      aCookie.Values("corpadrao") = HttpContext.Current.Session("corpadrao")
            '      aCookie.Values("master") = HttpContext.Current.Session("master")
            '       aCookie.Values("novasenhaem") = HttpContext.Current.Session("novasenhaem")
            '      aCookie.Values("anobaseuso") = HttpContext.Current.Session("anobaseuso")
            '      aCookie.Expires = DateTime.Now.AddDays(4)
            HttpContext.Current.Response.Cookies.Add(aCookie)

            gravalog("Login no sistema Anaplab às " & hoje() & " / " & hora(), "Acesso")

            'If tblogin.Rows(0)("alterado").ToString <> "True" AndAlso tblogin.Rows(0)("alterado").ToString <> "1" Then
            '    HttpContext.Current.Session("usuario") = tblogin.Rows(0)("usuario").ToString
            '    _urlretorno = "seguranca.aspx"
            '    ' Response.Redirect("seguranca.aspx")
            '    Return True
            'End If


            If Not trataretornosmart(gravalogin(_usuario)) Then

                Return False
            End If
            '  gravalog "Login", "Usuario " & _usuario & " Entrou no sistema")
            '  tbinforma = tabinforma.IncluirAlterarDados("update tbusuarios set forcarlogin = false, qtdtentativas = 0 where usuario = '" & _usuario & "' and ativo = true ")
            _urlretorno = "paineladministrativo.aspx"
            Return True


        End If

    End Function
    Private Function trataretornosmart(resultado As String) As Boolean
        Select Case resultado.ToLower
            Case Is = "concluído"
                Return True
            Case Is = "Erro base de dados"
                Return True
            Case Is = "cliente bloqueado"
                _mensagemerro = "Seu acesso ao sistema está suspenso! Por favor, entre em contato com o suporte!"
                Return False
        End Select
    End Function
    Private Function gravalogin(usuario As String) As String
        Try
            Dim ip As String = HttpContext.Current.Request.UserHostAddress
            Dim nrseqctrl As String = gerarnrseqcontrole()
            Dim plataforma As String = HttpContext.Current.Request.Browser.Platform & " - " & HttpContext.Current.Request.Browser.Browser
            Dim tblog As New Data.DataTable
            Dim tablog As New clsdbSmart
            tblog = tablog.conectar("select * from tbclientes where nome = '" & _cliente & "'")
            If tblog.Rows.Count = 0 Then
                tblog = tablog.incluiralterardados("insert into tbclientes (nome, liberado, dtultimoacesso, conveniotipon, cobrebem, atualizacaotravada, ativo, ultimoacesso) values ('" & _cliente & "', true, '" & hoje() & "', false, false, false, true, '" & hoje() & "')")
            Else
                If tblog.Rows(0)("liberado").ToString = "0" OrElse tblog.Rows(0)("ativo").ToString = "0" Then
                    tblog = tablog.incluiralterardados("insert into tbacessosnegados (cliente, maquina, data, hora, usuario) values ('" & _cliente & "', '" & ip & "', '" & hoje() & "', '" & hora() & "', '" & _usuario & "')")
                    Return "cliente bloqueado"
                End If
                tblog = tablog.incluiralterardados("update tbclientes set ultimoacesso = '" & hoje() & "', dtultimoacesso = '" & hoje() & "' where nome = '" & _cliente & "'")

            End If
            tblog = tablog.incluiralterardados("insert into tbacessos (nomecliente, dataacesso, maquina, versao, usuario, horaacesso, usuariosistema, data, dtacesso, sistema, dominio, sistemaoperacional) values ('" & meucliente & "', '" & formatadatamysql(data) & "', '" & ip & "', '" & minhaversao & "', '" & usuario & "', '" & hora(True) & "', '" & usuario & "','" & formatadatamysql(data) & "', '" & formatadatamysql(data) & "','" & meuproduto & "','" & HttpContext.Current.Request.Url.OriginalString & "','" & plataforma & "')")
            Return "Concluído"
        Catch ex As Exception
            Return "Erro base de dados"
        End Try
        Return True
    End Function
    Public Function loadcostumer() As Boolean
        Try
            Dim tb1 As New Data.DataTable
            Dim tab1 As New clsBanco
            tb1 = tab1.conectar("select * from tbconfig")
            If tb1.Rows.Count = 0 Then
                With HttpContext.Current
                    .Session("nomecliente") = "SmartCode"
                    .Session("enderecocliente") = "Rua Alfredo Pinto, 1955 salas 14 e 15"
                    .Session("bairrocliente") = "Afonso Pena"
                    .Session("cidadecliente") = "São José dos Pinhais"
                    .Session("ufcliente") = "PR"
                    .Session("paiscliente") = "Brasil"
                    .Session("telefonecliente") = "(41) 3385 1270"
                    .Session("logocliente") = "logo.png"
                    .Session("corpadrao") = "#181B34"
                    .Session("anobaseuso") = "2019"
                End With

            Else
                With HttpContext.Current
                    Dim xarq As String = HttpContext.Current.Server.MapPath("~") & "social\" & tb1.Rows(0)("logo").ToString
                    .Session("nomecliente") = tb1.Rows(0)("nomecliente").ToString
                    .Session("enderecocliente") = tb1.Rows(0)("endereco").ToString
                    .Session("bairrocliente") = tb1.Rows(0)("bairro").ToString
                    .Session("cidadecliente") = tb1.Rows(0)("cidade").ToString
                    .Session("ufcliente") = tb1.Rows(0)("uf").ToString
                    .Session("paiscliente") = tb1.Rows(0)("pais").ToString
                    .Session("anobaseuso") = tb1.Rows(0)("anobaseaberto").ToString
                    .Session("telefonecliente") = tb1.Rows(0)("telefone").ToString
                    .Session("logocliente") = IIf(File.Exists(xarq), tb1.Rows(0)("logo").ToString, "logo.png")
                    .Session("corpadrao") = IIf(tb1.Rows(0)("corpadrao").ToString = "", "#181B34", tb1.Rows(0)("corpadrao").ToString)
                End With

            End If
            Return True
        Catch ex As Exception
            _mensagemerro = ex.Message
            Return False
        End Try
    End Function
End Class

﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.IO
Imports System.Data

Public Class clsespecialiades

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _tablename As String = "tbespecialidades"
    Dim _table As Data.DataTable
    Dim _contador As Integer
    Dim _mensagemerro As String
    Dim _cod As Integer
    Dim _nrseqplano As Integer
    Dim _descricao As String
    Dim _valor As Decimal
    Dim _plano As String
    Dim _ignoraretorno As Boolean
    Dim _ctrlfinan As Boolean
    Dim _retorno As Boolean
    Dim _imprimirguia As Boolean
    Dim _medtrab As Boolean
    Dim _enviadosite As Boolean
    Dim _novaconsultaem As Integer
    Dim _consultaapostermino As Integer
    Dim _padrao As String
    Dim _ativo As Boolean
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _ignorarvalor As Boolean
    Dim _descricaopadraorecebimentos As String
    Dim _cbo As String
    Dim _pontos As Decimal
    Dim _resgate As Decimal
    Dim _alterarespecialidade As Boolean
    Dim _usarpreparatorioconsulta As Boolean
    Dim _textopreparatorioconsulta As String
    Dim _nrseqctrl As String
    Dim _tuss As String
    Dim _usercad As String
    Dim _dtcad As DateTime = data()
    Dim _nrseqempresa As Integer

    Public Sub New()
        versessao()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Cod As Integer
        Get
            Return _cod
        End Get
        Set(value As Integer)
            _cod = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _valor
        End Get
        Set(value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Ignoraretorno As Boolean
        Get
            Return _ignoraretorno
        End Get
        Set(value As Boolean)
            _ignoraretorno = value
        End Set
    End Property

    Public Property Ctrlfinan As Boolean
        Get
            Return _ctrlfinan
        End Get
        Set(value As Boolean)
            _ctrlfinan = value
        End Set
    End Property

    Public Property Retorno As Boolean
        Get
            Return _retorno
        End Get
        Set(value As Boolean)
            _retorno = value
        End Set
    End Property

    Public Property Imprimirguia As Boolean
        Get
            Return _imprimirguia
        End Get
        Set(value As Boolean)
            _imprimirguia = value
        End Set
    End Property

    Public Property Medtrab As Boolean
        Get
            Return _medtrab
        End Get
        Set(value As Boolean)
            _medtrab = value
        End Set
    End Property

    Public Property Enviadosite As Boolean
        Get
            Return _enviadosite
        End Get
        Set(value As Boolean)
            _enviadosite = value
        End Set
    End Property

    Public Property Novaconsultaem As Integer
        Get
            Return _novaconsultaem
        End Get
        Set(value As Integer)
            _novaconsultaem = value
        End Set
    End Property

    Public Property Consultaapostermino As Integer
        Get
            Return _consultaapostermino
        End Get
        Set(value As Integer)
            _consultaapostermino = value
        End Set
    End Property

    Public Property Padrao As String
        Get
            Return _padrao
        End Get
        Set(value As String)
            _padrao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Ignorarvalor As Boolean
        Get
            Return _ignorarvalor
        End Get
        Set(value As Boolean)
            _ignorarvalor = value
        End Set
    End Property

    Public Property Descricaopadraorecebimentos As String
        Get
            Return _descricaopadraorecebimentos
        End Get
        Set(value As String)
            _descricaopadraorecebimentos = value
        End Set
    End Property

    Public Property Cbo As String
        Get
            Return _cbo
        End Get
        Set(value As String)
            _cbo = value
        End Set
    End Property

    Public Property Pontos As Decimal
        Get
            Return _pontos
        End Get
        Set(value As Decimal)
            _pontos = value
        End Set
    End Property

    Public Property Resgate As Decimal
        Get
            Return _resgate
        End Get
        Set(value As Decimal)
            _resgate = value
        End Set
    End Property

    Public Property Alterarespecialidade As Boolean
        Get
            Return _alterarespecialidade
        End Get
        Set(value As Boolean)
            _alterarespecialidade = value
        End Set
    End Property

    Public Property Usarpreparatorioconsulta As Boolean
        Get
            Return _usarpreparatorioconsulta
        End Get
        Set(value As Boolean)
            _usarpreparatorioconsulta = value
        End Set
    End Property

    Public Property Textopreparatorioconsulta As String
        Get
            Return _textopreparatorioconsulta
        End Get
        Set(value As String)
            _textopreparatorioconsulta = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Tuss As String
        Get
            Return _tuss
        End Get
        Set(value As String)
            _tuss = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Nrseqplano As Integer
        Get
            Return _nrseqplano
        End Get
        Set(value As Integer)
            _nrseqplano = value
        End Set
    End Property
End Class
Partial Public Class clsespecialiades
    Public Function procurar(Optional xprocurarnome As Boolean = False) As Boolean
        If xprocurarnome Then
            If _descricao = "" Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            tb1 = tab1.conectar("select * from " & _tablename & " where ativo = true and descricao = '" & _descricao & "'")
        Else
            If _cod = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            tb1 = tab1.conectar("select * from " & _tablename & " where cod = " & _cod)
        End If

        If tb1.Rows.Count > 0 Then
            _cod = tb1.Rows(0)("cod").ToString
            _descricao = tb1.Rows(0)("descricao").ToString
            _valor = tb1.Rows(0)("valor").ToString
            _plano = tb1.Rows(0)("plano").ToString
            _ignoraretorno = logico(tb1.Rows(0)("ignoraretorno").ToString)
            _ctrlfinan = logico(tb1.Rows(0)("ctrlfinan").ToString)
            _retorno = logico(tb1.Rows(0)("retorno").ToString)
            _imprimirguia = logico(tb1.Rows(0)("imprimirguia").ToString)
            _medtrab = logico(tb1.Rows(0)("medtrab").ToString)
            _enviadosite = logico(tb1.Rows(0)("enviadosite").ToString)
            _novaconsultaem = numeros(tb1.Rows(0)("novaconsultaem").ToString)
            _consultaapostermino = numeros(tb1.Rows(0)("consultaapostermino").ToString)
            _padrao = tb1.Rows(0)("padrao").ToString
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _dtexclui = valordatamysql(tb1.Rows(0)("dtexclui").ToString)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            _ignorarvalor = logico(tb1.Rows(0)("ignorarvalor").ToString)
            _descricaopadraorecebimentos = tb1.Rows(0)("descricaopadraorecebimentos").ToString
            _cbo = tb1.Rows(0)("cbo").ToString
            _pontos = moeda(tb1.Rows(0)("pontos").ToString)
            _resgate = moeda(tb1.Rows(0)("resgate").ToString)
            _alterarespecialidade = logico(tb1.Rows(0)("alterarespecialidade").ToString)
            _usarpreparatorioconsulta = logico(tb1.Rows(0)("usarpreparatorioconsulta").ToString)
            _textopreparatorioconsulta = tb1.Rows(0)("textopreparatorioconsulta").ToString
            _tuss = tb1.Rows(0)("tuss").ToString
            _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
            _dtcad = valordatamysql(tb1.Rows(0)("dtcad").ToString)
            _usercad = tratatexto(tb1.Rows(0)("usercad").ToString)
            _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
        Else
            Mensagemerro = "Não existe nenhuma especialidade para esse plano"
            Return False
        End If
        _contador = tb1.Rows.Count
        _table = tb1
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarTodos() As Boolean
        If _cod = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & _tablename)
        If tb1.Rows.Count > 0 Then
            _cod = tb1.Rows(0)("cod").ToString
            _descricao = tb1.Rows(0)("descricao").ToString
            _valor = tb1.Rows(0)("valor").ToString
            _plano = tb1.Rows(0)("plano").ToString
            _ignoraretorno = logico(tb1.Rows(0)("ignoraretorno").ToString)
            _ctrlfinan = logico(tb1.Rows(0)("ctrlfinan").ToString)
            _retorno = logico(tb1.Rows(0)("retorno").ToString)
            _imprimirguia = logico(tb1.Rows(0)("imprimirguia").ToString)
            _medtrab = logico(tb1.Rows(0)("medtrab").ToString)
            _enviadosite = logico(tb1.Rows(0)("enviadosite").ToString)
            _novaconsultaem = numeros(tb1.Rows(0)("novaconsultaem").ToString)
            _consultaapostermino = numeros(tb1.Rows(0)("consultaapostermino").ToString)
            _padrao = tb1.Rows(0)("padrao").ToString
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _dtexclui = valordatamysql(tb1.Rows(0)("dtexclui").ToString)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            _ignorarvalor = logico(tb1.Rows(0)("ignorarvalor").ToString)
            _descricaopadraorecebimentos = tb1.Rows(0)("descricaopadraorecebimentos").ToString
            _cbo = tb1.Rows(0)("cbo").ToString
            _pontos = moeda(tb1.Rows(0)("pontos").ToString)
            _resgate = moeda(tb1.Rows(0)("resgate").ToString)
            _alterarespecialidade = logico(tb1.Rows(0)("alterarespecialidade").ToString)
            _usarpreparatorioconsulta = logico(tb1.Rows(0)("usarpreparatorioconsulta").ToString)
            _textopreparatorioconsulta = tb1.Rows(0)("textopreparatorioconsulta").ToString
            _tuss = tb1.Rows(0)("tuss").ToString
            _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
            _dtcad = valordatamysql(tb1.Rows(0)("dtcad").ToString)
            _usercad = tratatexto(tb1.Rows(0)("usercad").ToString)
            _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
        Else
            Mensagemerro = "Não existe nenhuma especialidade para esse plano"
            Return False
        End If
        _contador = tb1.Rows.Count
        _table = tb1

        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarplanodesc(ativo As Boolean, Optional descricaoplano As Boolean = False) As Boolean
        If _cod = 0 Then
            Mensagemerro = "Informe um item válido para procurar"
        End If

        tb1 = tab1.conectar("select * from " & _tablename & " where ativo=" & ativo & IIf(descricaoplano = False, " and nrseqplano='" & _nrseqplano & "'", " and plano='" & _plano & "'") & " order by descricao")
        If tb1.Rows.Count > 0 Then
            _cod = tb1.Rows(0)("cod").ToString
            _descricao = tb1.Rows(0)("descricao").ToString
            _valor = tb1.Rows(0)("valor").ToString
            _plano = tb1.Rows(0)("plano").ToString
            _ignoraretorno = logico(tb1.Rows(0)("ignoraretorno").ToString)
            _ctrlfinan = logico(tb1.Rows(0)("ctrlfinan").ToString)
            _retorno = logico(tb1.Rows(0)("retorno").ToString)
            _imprimirguia = logico(tb1.Rows(0)("imprimirguia").ToString)
            _medtrab = logico(tb1.Rows(0)("medtrab").ToString)
            _enviadosite = logico(tb1.Rows(0)("enviadosite").ToString)
            _novaconsultaem = numeros(tb1.Rows(0)("novaconsultaem").ToString)
            _consultaapostermino = numeros(tb1.Rows(0)("consultaapostermino").ToString)
            _padrao = tb1.Rows(0)("padrao").ToString
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _dtexclui = valordatamysql(tb1.Rows(0)("dtexclui").ToString)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            _ignorarvalor = logico(tb1.Rows(0)("ignorarvalor").ToString)
            _descricaopadraorecebimentos = tb1.Rows(0)("descricaopadraorecebimentos").ToString
            _cbo = tb1.Rows(0)("cbo").ToString
            _pontos = moeda(tb1.Rows(0)("pontos").ToString)
            _resgate = moeda(tb1.Rows(0)("resgate").ToString)
            _alterarespecialidade = logico(tb1.Rows(0)("alterarespecialidade").ToString)
            _usarpreparatorioconsulta = logico(tb1.Rows(0)("usarpreparatorioconsulta").ToString)
            _textopreparatorioconsulta = tb1.Rows(0)("textopreparatorioconsulta").ToString
            _tuss = tb1.Rows(0)("tuss").ToString
            _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
            _dtcad = valordatamysql(tb1.Rows(0)("dtcad").ToString)
            _usercad = tratatexto(tb1.Rows(0)("usercad").ToString)
            _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
        End If
        _contador = tb1.Rows.Count
        _table = tb1
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarplano() As Boolean
        If _cod = 0 Then
            Mensagemerro = "Informe um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & _tablename & " where nrseqplano='" & _nrseqplano & "' and ativo = true order by descricao")
        If tb1.Rows.Count > 0 Then
            _cod = tb1.Rows(0)("cod").ToString
            _descricao = tb1.Rows(0)("descricao").ToString
            _valor = tb1.Rows(0)("valor").ToString
            _plano = tb1.Rows(0)("plano").ToString
            _ignoraretorno = logico(tb1.Rows(0)("ignoraretorno").ToString)
            _ctrlfinan = logico(tb1.Rows(0)("ctrlfinan").ToString)
            _retorno = logico(tb1.Rows(0)("retorno").ToString)
            _imprimirguia = logico(tb1.Rows(0)("imprimirguia").ToString)
            _medtrab = logico(tb1.Rows(0)("medtrab").ToString)
            _enviadosite = logico(tb1.Rows(0)("enviadosite").ToString)
            _novaconsultaem = numeros(tb1.Rows(0)("novaconsultaem").ToString)
            _consultaapostermino = numeros(tb1.Rows(0)("consultaapostermino").ToString)
            _padrao = tb1.Rows(0)("padrao").ToString
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _dtexclui = valordatamysql(tb1.Rows(0)("dtexclui").ToString)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            _ignorarvalor = logico(tb1.Rows(0)("ignorarvalor").ToString)
            _descricaopadraorecebimentos = tb1.Rows(0)("descricaopadraorecebimentos").ToString
            _cbo = tb1.Rows(0)("cbo").ToString
            _pontos = moeda(tb1.Rows(0)("pontos").ToString)
            _resgate = moeda(tb1.Rows(0)("resgate").ToString)
            _alterarespecialidade = logico(tb1.Rows(0)("alterarespecialidade").ToString)
            _usarpreparatorioconsulta = logico(tb1.Rows(0)("usarpreparatorioconsulta").ToString)
            _textopreparatorioconsulta = tb1.Rows(0)("textopreparatorioconsulta").ToString
            _tuss = tb1.Rows(0)("tuss").ToString
            _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
            _dtcad = valordatamysql(tb1.Rows(0)("dtcad").ToString)
            _usercad = tratatexto(tb1.Rows(0)("usercad").ToString)
            _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
        Else
            Mensagemerro = "Não existe nenhuma especialidade para esse plano"
            Return False
        End If
        _contador = tb1.Rows.Count
        _table = tb1
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try

            Dim xsql As String

            xsql = " update " & _tablename & " Set ativo = True "

            If _descricao <> "" Then
                xsql &= ", descricao = '" & _descricao & "'"
            End If
            If moeda(_valor) <> "" Then
                xsql &= ", valor = '" & moeda(_valor) & "'"
            End If
            If _plano <> "" Then
                xsql &= ", plano = '" & _plano & "'"
            End If
            If logico(_ignoraretorno) <> "" Then
                xsql &= ", ignoraretorno = " & logico(_ignoraretorno)
            End If
            If logico(_ctrlfinan) <> "" Then
                xsql &= ", ctrlfinan =" & logico(_ctrlfinan)
            End If
            If logico(_retorno) <> "" Then
                xsql &= ", retorno = " & logico(_retorno)
            End If
            If logico(_imprimirguia) <> "" Then
                xsql &= ", imprimirguia = " & logico(_imprimirguia)
            End If
            If logico(_medtrab) <> "" Then
                xsql &= ", medtrab = " & logico(_medtrab)
            End If
            'If _enviadosite <> True Then
            '    xsql &= ", enviadosite = '" & _enviadosite & "'"
            'End If
            If _novaconsultaem <> 0 Then
                xsql &= ", novaconsultaem = " & _novaconsultaem & "'"
            End If
            If _consultaapostermino <> 0 Then
                xsql &= ", consultaapostermino = '" & _consultaapostermino & "'"
            End If
            'If _padrao <> "" Then
            '    xsql &= ", padrao = '" & _padrao & "'"
            'End If
            If logico(_ativo) <> "" Then
                xsql &= ", ativo = " & logico(_ativo)
            End If
            If logico(_ignorarvalor) <> "" Then
                xsql &= ", ignorarvalor = " & logico(_ignorarvalor)
            End If
            If _descricaopadraorecebimentos <> "" Then
                xsql &= ", descricaopadraorecebimentos = '" & _descricaopadraorecebimentos & "'"
            End If
            If _cbo <> "" Then
                xsql &= ",cbo = '" & tratatexto(_cbo) & "'"
            End If
            'If _resgate <> "" Then
            '    xsql &= ",resgate = '" & tratatexto(_resgate) & "'"
            'End If
            If logico(_alterarespecialidade) <> "" Then
                xsql &= ",alterarespecialidade = " & logico(_alterarespecialidade)
            End If
            If logico(_usarpreparatorioconsulta) <> "" Then
                xsql &= ",usarpreparatorioconsulta = " & logico(_usarpreparatorioconsulta)
            End If
            If _textopreparatorioconsulta <> "" Then
                xsql &= ",textopreparatorioconsulta = '" & tratatexto(_textopreparatorioconsulta) & "'"
            End If
            If _tuss <> "" Then
                xsql &= ",tuss = '" & tratatexto(_tuss) & "'"
            End If
            If _nrseqempresa <> 0 Then
                xsql &= ",nrseqempresa = '" & moeda(_nrseqempresa) & "'"
            End If
            If _nrseqplano <> 0 Then
                xsql &= ",nrseqplano = '" & numeros(_nrseqplano) & "'"
            End If
            xsql &= " where cod = '" & _cod & "'"
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _cod = tb1.Rows(0)("cod").ToString

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _cod = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where Cod = " & _cod)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


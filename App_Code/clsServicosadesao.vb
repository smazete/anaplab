﻿
Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsServicosadesao


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _tablename As String = "tbservicos_adesao"
    Dim _table As New Data.DataTable

    Dim _nrseq As Integer
    Dim _nrseqservico As Integer
    Dim _nrseqempresa As Integer
    Dim _valor As Integer
    Dim _adesao As Boolean
    Dim _ativo As Boolean
    Dim _usercad As String
    Dim _dtcad As DateTime
    Dim _userexclui As String
    Dim _dtexclui As DateTime
    Dim _useradesao As String
    Dim _dtadesao As DateTime
    Dim _usernaoadesao As String
    Dim _dtnaoadesao As DateTime


    Dim _mensagemerro As String
    Dim _contador As Integer

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqservico As Integer
        Get
            Return _nrseqservico
        End Get
        Set(value As Integer)
            _nrseqservico = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Valor As Integer
        Get
            Return _valor
        End Get
        Set(value As Integer)
            _valor = value
        End Set
    End Property

    Public Property Adesao As Boolean
        Get
            Return _adesao
        End Get
        Set(value As Boolean)
            _adesao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Useradesao As String
        Get
            Return _useradesao
        End Get
        Set(value As String)
            _useradesao = value
        End Set
    End Property

    Public Property Dtadesao As Date
        Get
            Return _dtadesao
        End Get
        Set(value As Date)
            _dtadesao = value
        End Set
    End Property

    Public Property Usernaoadesao As String
        Get
            Return _usernaoadesao
        End Get
        Set(value As String)
            _usernaoadesao = value
        End Set
    End Property

    Public Property Dtnaoadesao As Date
        Get
            Return _dtnaoadesao
        End Get
        Set(value As Date)
            _dtnaoadesao = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class
Partial Public Class clsServicosadesao

    Public Function consultar() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then

                _nrseqservico = tb1.Rows(0)("nrseqservico").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _valor = tb1.Rows(0)("valor").Totb1.Rows(0)("valor").ToString
                _adesao = logico(tb1.Rows(0)("adesao").Totb1.Rows(0)("valor").ToString)
                _ativo = logico(tb1.Rows(0)("ativo").Totb1.Rows(0)("valor").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _useradesao = tb1.Rows(0)("useradesao").ToString
                _usernaoadesao = tb1.Rows(0)("usernaoadesao").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        Dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtadesao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtadesao").ToString
                        _dtadesao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnaoadesao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnaoadesao").ToString
                        _dtnaoadesao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarpesq(type As String, typesearch As String, Optional ativo As Boolean = True) As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & _tablename & " Where " & type & " Like '" & typesearch & "%' and ativo= " & ativo & "")

            If tb1.Rows.Count > 0 Then

                _nrseqservico = tb1.Rows(0)("nrseqservico").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _valor = tb1.Rows(0)("valor").Totb1.Rows(0)("valor").ToString
                _adesao = logico(tb1.Rows(0)("adesao").Totb1.Rows(0)("valor").ToString)
                _ativo = logico(tb1.Rows(0)("ativo").Totb1.Rows(0)("valor").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _useradesao = tb1.Rows(0)("useradesao").ToString
                _usernaoadesao = tb1.Rows(0)("usernaoadesao").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        Dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtadesao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtadesao").ToString
                        _dtadesao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnaoadesao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnaoadesao").ToString
                        _dtnaoadesao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarempresa() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqempresa = " & _nrseqempresa)

            If tb1.Rows.Count > 0 Then

                _nrseqservico = tb1.Rows(0)("nrseqservico").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _valor = tb1.Rows(0)("valor").Totb1.Rows(0)("valor").ToString
                _adesao = logico(tb1.Rows(0)("adesao").Totb1.Rows(0)("valor").ToString)
                _ativo = logico(tb1.Rows(0)("ativo").Totb1.Rows(0)("valor").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _useradesao = tb1.Rows(0)("useradesao").ToString
                _usernaoadesao = tb1.Rows(0)("usernaoadesao").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        Dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtadesao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtadesao").ToString
                        _dtadesao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnaoadesao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnaoadesao").ToString
                        _dtnaoadesao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, ativo, dtcad, usercad,nrseqempresa) values ('" & wcnrseqctrl & "',false,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "'," & HttpContext.Current.Session("idempresaemuso") & ")")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo=1, nrseqservico='" & _nrseqservico & "',nrseqempresa='" & _nrseqempresa & "',valor='" & _valor & "',adesao='" & _adesao & "',useradesao='" & _useradesao & "',dtadesao='" & _dtadesao & "',usernaoadesao='" & _usernaoadesao & "',dtnaoadesao='" & _dtnaoadesao & "' where nrseq= " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _mensagemerro = "Please, enter a valid Funcionario!"
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.ToString
            Return False

        End Try

    End Function

End Class
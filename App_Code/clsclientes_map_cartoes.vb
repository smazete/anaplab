﻿Imports Microsoft.VisualBasic

Public Class clsclientes_map_cartoes
    Dim _nomecartao As String = ""
    Dim _nrcartao As String = ""
    Dim _codseg As String = ""
    Dim _validade As String = ""
    Dim _ativo As Boolean = True
    Dim _dtcad As Date
    Dim _usercad As String = ""
    Dim _nrseq As Integer = 0
    Dim _principal As Boolean = False
    Public Property Nomecartao As String
        Get
            Return _nomecartao
        End Get
        Set(value As String)
            _nomecartao = value
        End Set
    End Property

    Public Property Nrcartao As String
        Get
            Return _nrcartao
        End Get
        Set(value As String)
            _nrcartao = value
        End Set
    End Property

    Public Property Codseg As String
        Get
            Return _codseg
        End Get
        Set(value As String)
            _codseg = value
        End Set
    End Property

    Public Property Validade As String
        Get
            Return _validade
        End Get
        Set(value As String)
            _validade = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Principal As Boolean
        Get
            Return _principal
        End Get
        Set(value As Boolean)
            _principal = value
        End Set
    End Property
End Class

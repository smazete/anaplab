﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes

Public Class clsaprovacao
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _table As String = "tbaprovacao"
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _matricula As String
    Dim _nome As String
    Dim _nomemae As String
    Dim _situacaopb As String
    Dim _sexo As String
    Dim _dtnasc As Date
    Dim _cpf As String
    Dim _rg As String
    Dim _cep As String
    Dim _endereco As String
    Dim _numero As String
    Dim _complemento As String
    Dim _bairro As String
    Dim _cidade As String
    Dim _estado As String
    Dim _telddd As String
    Dim _telefone As String
    Dim _email As String
    Dim _dtposse As Date
    Dim _dtaposentadoria As Date
    Dim _senha As String
    Dim _agencia As String
    Dim _contacorrente As String
    Dim _autorizacao As Integer
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo_old As Integer
    Dim _ativo As String
    Dim _matriculactrl As String
    Dim _codigo As Integer
    Dim _userexcluir As String
    Dim _dtexcluido As Date
    Dim _dddcelular As String
    Dim _celular As String
    Dim _bloqueado_old As Integer
    Dim _bloqueado As String
    Dim _dtbloqueio As Date
    Dim _userbloqueio As String
    Dim _dtdesbloqueio As Date
    Dim _userdesbloqueio As String
    Dim _ip As String
    Dim _dtsolexc As String
    Dim _motiexc As String
    Dim _dtinad As String
    Dim _filiacao As Integer
    Dim _vivomorto As String
    Dim _negociado As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Nomemae As String
        Get
            Return _nomemae
        End Get
        Set(value As String)
            _nomemae = value
        End Set
    End Property

    Public Property Situacaopb As String
        Get
            Return _situacaopb
        End Get
        Set(value As String)
            _situacaopb = value
        End Set
    End Property

    Public Property Sexo As String
        Get
            Return _sexo
        End Get
        Set(value As String)
            _sexo = value
        End Set
    End Property

    Public Property Dtnasc As Date
        Get
            Return _dtnasc
        End Get
        Set(value As Date)
            _dtnasc = value
        End Set
    End Property

    Public Property Cpf As String
        Get
            Return _cpf
        End Get
        Set(value As String)
            _cpf = value
        End Set
    End Property

    Public Property Rg As String
        Get
            Return _rg
        End Get
        Set(value As String)
            _rg = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Complemento As String
        Get
            Return _complemento
        End Get
        Set(value As String)
            _complemento = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Estado As String
        Get
            Return _estado
        End Get
        Set(value As String)
            _estado = value
        End Set
    End Property

    Public Property Telddd As String
        Get
            Return _telddd
        End Get
        Set(value As String)
            _telddd = value
        End Set
    End Property

    Public Property Telefone As String
        Get
            Return _telefone
        End Get
        Set(value As String)
            _telefone = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Dtposse As Date
        Get
            Return _dtposse
        End Get
        Set(value As Date)
            _dtposse = value
        End Set
    End Property

    Public Property Dtaposentadoria As Date
        Get
            Return _dtaposentadoria
        End Get
        Set(value As Date)
            _dtaposentadoria = value
        End Set
    End Property

    Public Property Senha As String
        Get
            Return _senha
        End Get
        Set(value As String)
            _senha = value
        End Set
    End Property

    Public Property Agencia As String
        Get
            Return _agencia
        End Get
        Set(value As String)
            _agencia = value
        End Set
    End Property

    Public Property Contacorrente As String
        Get
            Return _contacorrente
        End Get
        Set(value As String)
            _contacorrente = value
        End Set
    End Property

    Public Property Autorizacao As Integer
        Get
            Return _autorizacao
        End Get
        Set(value As Integer)
            _autorizacao = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo_old As Integer
        Get
            Return _ativo_old
        End Get
        Set(value As Integer)
            _ativo_old = value
        End Set
    End Property

    Public Property Ativo As String
        Get
            Return _ativo
        End Get
        Set(value As String)
            _ativo = value
        End Set
    End Property

    Public Property Matriculactrl As String
        Get
            Return _matriculactrl
        End Get
        Set(value As String)
            _matriculactrl = value
        End Set
    End Property

    Public Property Codigo As Integer
        Get
            Return _codigo
        End Get
        Set(value As Integer)
            _codigo = value
        End Set
    End Property

    Public Property Userexcluir As String
        Get
            Return _userexcluir
        End Get
        Set(value As String)
            _userexcluir = value
        End Set
    End Property

    Public Property Dtexcluido As Date
        Get
            Return _dtexcluido
        End Get
        Set(value As Date)
            _dtexcluido = value
        End Set
    End Property

    Public Property Dddcelular As String
        Get
            Return _dddcelular
        End Get
        Set(value As String)
            _dddcelular = value
        End Set
    End Property

    Public Property Celular As String
        Get
            Return _celular
        End Get
        Set(value As String)
            _celular = value
        End Set
    End Property

    Public Property Bloqueado_old As Integer
        Get
            Return _bloqueado_old
        End Get
        Set(value As Integer)
            _bloqueado_old = value
        End Set
    End Property

    Public Property Bloqueado As String
        Get
            Return _bloqueado
        End Get
        Set(value As String)
            _bloqueado = value
        End Set
    End Property

    Public Property Dtbloqueio As Date
        Get
            Return _dtbloqueio
        End Get
        Set(value As Date)
            _dtbloqueio = value
        End Set
    End Property

    Public Property Userbloqueio As String
        Get
            Return _userbloqueio
        End Get
        Set(value As String)
            _userbloqueio = value
        End Set
    End Property

    Public Property Dtdesbloqueio As Date
        Get
            Return _dtdesbloqueio
        End Get
        Set(value As Date)
            _dtdesbloqueio = value
        End Set
    End Property

    Public Property Userdesbloqueio As String
        Get
            Return _userdesbloqueio
        End Get
        Set(value As String)
            _userdesbloqueio = value
        End Set
    End Property

    Public Property Ip As String
        Get
            Return _ip
        End Get
        Set(value As String)
            _ip = value
        End Set
    End Property

    Public Property Dtsolexc As String
        Get
            Return _dtsolexc
        End Get
        Set(value As String)
            _dtsolexc = value
        End Set
    End Property

    Public Property Motiexc As String
        Get
            Return _motiexc
        End Get
        Set(value As String)
            _motiexc = value
        End Set
    End Property

    Public Property Dtinad As String
        Get
            Return _dtinad
        End Get
        Set(value As String)
            _dtinad = value
        End Set
    End Property

    Public Property Filiacao As Integer
        Get
            Return _filiacao
        End Get
        Set(value As Integer)
            _filiacao = value
        End Set
    End Property

    Public Property Vivomorto As String
        Get
            Return _vivomorto
        End Get
        Set(value As String)
            _vivomorto = value
        End Set
    End Property

    Public Property Negociado As String
        Get
            Return _negociado
        End Get
        Set(value As String)
            _negociado = value
        End Set
    End Property

    Public Property Table As String
        Get
            Return _table
        End Get
        Set(value As String)
            _table = value
        End Set
    End Property
End Class

Partial Public Class clsaprovacao

    Public Function procurar() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & Table & " where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _matricula = tb1.Rows(0)("matricula").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _nomemae = tb1.Rows(0)("nomemae").ToString
        _situacaopb = tb1.Rows(0)("situacaopb").ToString
        _sexo = tb1.Rows(0)("sexo").ToString
        _dtnasc = FormatDateTime(tb1.Rows(0)("dtnasc").ToString, DateFormat.ShortDate)
        _cpf = tb1.Rows(0)("cpf").ToString
        _rg = tb1.Rows(0)("rg").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _numero = tb1.Rows(0)("numero").ToString
        _complemento = tb1.Rows(0)("complemento").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        _estado = tb1.Rows(0)("estado").ToString
        _telddd = tb1.Rows(0)("telddd").ToString
        _telefone = tb1.Rows(0)("telefone").ToString
        _email = tb1.Rows(0)("email").ToString
        _dtposse = FormatDateTime(tb1.Rows(0)("dtposse").ToString, DateFormat.ShortDate)
        _dtaposentadoria = FormatDateTime(tb1.Rows(0)("dtaposentadoria").ToString, DateFormat.ShortDate)
        _senha = tb1.Rows(0)("senha").ToString
        _agencia = tb1.Rows(0)("agencia").ToString
        _contacorrente = tb1.Rows(0)("contacorrente").ToString
        '  _autorizacao = tb1.Rows(0)("autorizacao").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _usercad = tb1.Rows(0)("usercad").ToString
        ' _ativo_old = tb1.Rows(0)("ativo_old").ToString
        '      _ativo = tb1.Rows(0)("ativo").ToString
        '     _matriculactrl = tb1.Rows(0)("matriculactrl").ToString
        '    _codigo = tb1.Rows(0)("codigo").ToString
        '   _userexcluir = tb1.Rows(0)("userexcluir").ToString
        '   _dtexcluido = FormatDateTime(tb1.Rows(0)("dtexcluido").ToString, DateFormat.ShortDate)
        '  _dddcelular = tb1.Rows(0)("dddcelular").ToString
        '_celular = tb1.Rows(0)("celular").ToString
        ' _bloqueado_old = tb1.Rows(0)("bloqueado_old").ToString
        '_bloqueado = tb1.Rows(0)("bloqueado").ToString
        '_dtbloqueio = FormatDateTime(tb1.Rows(0)("dtbloqueio").ToString, DateFormat.ShortDate)
        '_userbloqueio = tb1.Rows(0)("userbloqueio").ToString
        '  _dtdesbloqueio = FormatDateTime(tb1.Rows(0)("dtdesbloqueio").ToString, DateFormat.ShortDate)
        '_userdesbloqueio = tb1.Rows(0)("userdesbloqueio").ToString
        '_ip = tb1.Rows(0)("ip").ToString
        '_dtsolexc = tb1.Rows(0)("dtsolexc").ToString
        '_motiexc = tb1.Rows(0)("motiexc").ToString
        '_dtinad = tb1.Rows(0)("dtinad").ToString
        '_filiacao = tb1.Rows(0)("filiacao").ToString
        '_vivomorto = tb1.Rows(0)("vivomorto").ToString
        '_negociado = tb1.Rows(0)("negociado").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function



    Public Function novoassociado() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into tbaprovacao (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbaprovacao where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString


            Dim xsql As String
            xsql = " update tbaprovacao Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & _nrseq
            End If
            If _matricula <> "" Then
                xsql &= ",matricula = '" & tratatexto(_matricula) & "'"
            End If
            If _nome <> "" Then
                xsql &= ",nome = '" & tratatexto(_nome) & "'"
            End If
            If _nomemae <> "" Then
                xsql &= ",nomemae = '" & tratatexto(_nomemae) & "'"
            End If
            If _situacaopb <> "" Then
                xsql &= ",situacaopb = '" & tratatexto(_situacaopb) & "'"
            End If
            If _sexo <> "" Then
                xsql &= ",sexo = '" & _sexo & "'"
            End If
            If formatadatamysql(_dtnasc) <> "" Then
                xsql &= ",dtnasc = '" & formatadatamysql(_dtnasc) & "'"
            End If
            If _cpf <> "" Then
                xsql &= ",cpf = '" & tratatexto(_cpf) & "'"
            End If
            If _rg <> "" Then
                xsql &= ",rg = '" & tratatexto(_rg) & "'"
            End If
            If _cep <> "" Then
                xsql &= ",cep = '" & tratatexto(_cep) & "'"
            End If
            If _endereco <> "" Then
                xsql &= ",endereco = '" & tratatexto(_endereco) & "'"
            End If
            If _numero <> "" Then
                xsql &= ",numero = '" & tratatexto(_numero) & "'"
            End If
            If _complemento <> "" Then
                xsql &= ",complemento = '" & tratatexto(_complemento) & "'"
            End If
            If _bairro <> "" Then
                xsql &= ",bairro = '" & tratatexto(_bairro) & "'"
            End If
            If _cidade <> "" Then
                xsql &= ",cidade = '" & tratatexto(_cidade) & "'"
            End If
            If _estado <> "" Then
                xsql &= ",estado = '" & tratatexto(_estado) & "'"
            End If
            If _telddd <> "" Then
                xsql &= ",telddd = '" & tratatexto(_telddd) & "'"
            End If
            If _telefone <> "" Then
                xsql &= ",telefone = '" & tratatexto(_telefone) & "'"
            End If
            If _email <> "" Then
                xsql &= ",email = '" & tratatexto(_email) & "'"
            End If
            If formatadatamysql(_dtposse) <> "" Then
                xsql &= ",dtposse = '" & formatadatamysql(_dtposse) & "'"
            End If
            If formatadatamysql(_dtaposentadoria) <> "" Then
                xsql &= ",dtaposentadoria = '" & formatadatamysql(_dtaposentadoria) & "'"
            End If
            If _senha <> "" Then
                xsql &= ",senha = '" & tratatexto(_senha) & "'"
            End If
            If _agencia <> "" Then
                xsql &= ",agencia = '" & tratatexto(_agencia) & "'"
            End If
            If _contacorrente <> "" Then
                xsql &= ",contacorrente = '" & tratatexto(_contacorrente) & "'"
            End If
            If _autorizacao <> 0 Then
                xsql &= ",autorizacao = " & moeda(_autorizacao)
            End If
            If formatadatamysql(_dtcad) <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _ativo_old <> 0 Then
                xsql &= ",ativo_old = " & moeda(_ativo_old)
            End If
            If _ativo <> "" Then
                xsql &= ",ativo = '" & tratatexto(_ativo) & "'"
            End If
            If _matriculactrl <> "" Then
                xsql &= ",matriculactrl = '" & tratatexto(_matriculactrl) & "'"
            End If
            If _codigo <> 0 Then
                xsql &= ",codigo = " & moeda(_codigo)
            End If
            If _userexcluir <> "" Then
                xsql &= ",userexcluir = '" & tratatexto(_userexcluir) & "'"
            End If
            If formatadatamysql(_dtexcluido) <> "" Then
                xsql &= ",dtexcluido = '" & formatadatamysql(_dtexcluido) & "'"
            End If
            If _dddcelular <> "" Then
                xsql &= ",dddcelular = '" & tratatexto(_dddcelular) & "'"
            End If
            If _celular <> "" Then
                xsql &= ",celular = '" & tratatexto(_celular) & "'"
            End If
            If _bloqueado_old <> 0 Then
                xsql &= ",bloqueado_old = " & moeda(_bloqueado_old)
            End If
            If _bloqueado <> "" Then
                xsql &= ",bloqueado = '" & tratatexto(_bloqueado) & "'"
            End If
            If formatadatamysql(_dtbloqueio) <> "" Then
                xsql &= ",dtbloqueio = '" & formatadatamysql(_dtbloqueio) & "'"
            End If
            If _userbloqueio <> "" Then
                xsql &= ",userbloqueio = '" & tratatexto(_userbloqueio) & "'"
            End If
            If formatadatamysql(_dtdesbloqueio) <> "" Then
                xsql &= ",dtdesbloqueio = '" & formatadatamysql(_dtdesbloqueio) & "'"
            End If
            If _userdesbloqueio <> "" Then
                xsql &= ",userdesbloqueio = '" & tratatexto(_userdesbloqueio) & "'"
            End If
            If _ip <> "" Then
                xsql &= ",ip = '" & tratatexto(_ip) & "'"
            End If
            If formatadatamysql(_dtsolexc) <> "" Then
                xsql &= ",dtsolexc = '" & formatadatamysql(_dtsolexc) & "'"
            End If
            If _motiexc <> "" Then
                xsql &= ",motiexc = '" & tratatexto(_motiexc) & "'"
            End If
            If formatadatamysql(_dtinad) <> "" Then
                xsql &= ",dtinad = '" & formatadatamysql(_dtinad) & "'"
            End If
            If _filiacao <> 0 Then
                xsql &= ",filiacao = " & moeda(_filiacao)
            End If
            If _vivomorto <> "" Then
                xsql &= ",vivomorto = '" & tratatexto(_vivomorto) & "'"
            End If
            If _negociado <> "" Then
                xsql &= ",negociado = '" & tratatexto(_negociado) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function




    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update Table Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _matricula <> "" Then
                xsql &= ",matricula = '" & tratatexto(_matricula) & "'"
            End If
            If _nome <> "" Then
                xsql &= ",nome = '" & tratatexto(_nome) & "'"
            End If
            If _nomemae <> "" Then
                xsql &= ",nomemae = '" & tratatexto(_nomemae) & "'"
            End If
            If _situacaopb <> "" Then
                xsql &= ",situacaopb = '" & tratatexto(_situacaopb) & "'"
            End If
            If _sexo <> "" Then
                xsql &= ",sexo = '" & tratatexto(_sexo) & "'"
            End If
            If formatadatamysql(_dtnasc) <> "" Then
                xsql &= ",dtnasc = '" & formatadatamysql(_dtnasc) & "'"
            End If
            If _cpf <> "" Then
                xsql &= ",cpf = '" & tratatexto(_cpf) & "'"
            End If
            If _rg <> "" Then
                xsql &= ",rg = '" & tratatexto(_rg) & "'"
            End If
            If _cep <> "" Then
                xsql &= ",cep = '" & tratatexto(_cep) & "'"
            End If
            If _endereco <> "" Then
                xsql &= ",endereco = '" & tratatexto(_endereco) & "'"
            End If
            If _numero <> "" Then
                xsql &= ",numero = '" & tratatexto(_numero) & "'"
            End If
            If _complemento <> "" Then
                xsql &= ",complemento = '" & tratatexto(_complemento) & "'"
            End If
            If _bairro <> "" Then
                xsql &= ",bairro = '" & tratatexto(_bairro) & "'"
            End If
            If _cidade <> "" Then
                xsql &= ",cidade = '" & tratatexto(_cidade) & "'"
            End If
            If _estado <> "" Then
                xsql &= ",estado = '" & tratatexto(_estado) & "'"
            End If
            If _telddd <> "" Then
                xsql &= ",telddd = '" & tratatexto(_telddd) & "'"
            End If
            If _telefone <> "" Then
                xsql &= ",telefone = '" & tratatexto(_telefone) & "'"
            End If
            If _email <> "" Then
                xsql &= ",email = '" & tratatexto(_email) & "'"
            End If
            If formatadatamysql(_dtposse) <> "" Then
                xsql &= ",dtposse = '" & formatadatamysql(_dtposse) & "'"
            End If
            If formatadatamysql(_dtaposentadoria) <> "" Then
                xsql &= ",dtaposentadoria = '" & formatadatamysql(_dtaposentadoria) & "'"
            End If
            If _senha <> "" Then
                xsql &= ",senha = '" & tratatexto(_senha) & "'"
            End If
            If _agencia <> "" Then
                xsql &= ",agencia = '" & tratatexto(_agencia) & "'"
            End If
            If _contacorrente <> "" Then
                xsql &= ",contacorrente = '" & tratatexto(_contacorrente) & "'"
            End If
            If _autorizacao <> 0 Then
                xsql &= ",autorizacao = " & moeda(_autorizacao)
            End If
            If formatadatamysql(_dtcad) <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _ativo_old <> 0 Then
                xsql &= ",ativo_old = " & moeda(_ativo_old)
            End If
            If _ativo <> "" Then
                xsql &= ",ativo = '" & tratatexto(_ativo) & "'"
            End If
            If _matriculactrl <> "" Then
                xsql &= ",matriculactrl = '" & tratatexto(_matriculactrl) & "'"
            End If
            If _codigo <> 0 Then
                xsql &= ",codigo = " & moeda(_codigo)
            End If
            If _userexcluir <> "" Then
                xsql &= ",userexcluir = '" & tratatexto(_userexcluir) & "'"
            End If
            If formatadatamysql(_dtexcluido) <> "" Then
                xsql &= ",dtexcluido = '" & formatadatamysql(_dtexcluido) & "'"
            End If
            If _dddcelular <> "" Then
                xsql &= ",dddcelular = '" & tratatexto(_dddcelular) & "'"
            End If
            If _celular <> "" Then
                xsql &= ",celular = '" & tratatexto(_celular) & "'"
            End If
            If _bloqueado_old <> 0 Then
                xsql &= ",bloqueado_old = " & moeda(_bloqueado_old)
            End If
            If _bloqueado <> "" Then
                xsql &= ",bloqueado = '" & tratatexto(_bloqueado) & "'"
            End If
            If formatadatamysql(_dtbloqueio) <> "" Then
                xsql &= ",dtbloqueio = '" & formatadatamysql(_dtbloqueio) & "'"
            End If
            If _userbloqueio <> "" Then
                xsql &= ",userbloqueio = '" & tratatexto(_userbloqueio) & "'"
            End If
            If formatadatamysql(_dtdesbloqueio) <> "" Then
                xsql &= ",dtdesbloqueio = '" & formatadatamysql(_dtdesbloqueio) & "'"
            End If
            If _userdesbloqueio <> "" Then
                xsql &= ",userdesbloqueio = '" & tratatexto(_userdesbloqueio) & "'"
            End If
            If _ip <> "" Then
                xsql &= ",ip = '" & tratatexto(_ip) & "'"
            End If
            If _dtsolexc <> "" Then
                xsql &= ",dtsolexc = '" & tratatexto(_dtsolexc) & "'"
            End If
            If _motiexc <> "" Then
                xsql &= ",motiexc = '" & tratatexto(_motiexc) & "'"
            End If
            If _dtinad <> "" Then
                xsql &= ",dtinad = '" & tratatexto(_dtinad) & "'"
            End If
            If _filiacao <> 0 Then
                xsql &= ",filiacao = " & moeda(_filiacao)
            End If
            If _vivomorto <> "" Then
                xsql &= ",vivomorto = '" & tratatexto(_vivomorto) & "'"
            End If
            If _negociado <> "" Then
                xsql &= ",negociado = '" & tratatexto(_negociado) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function



    Public Function carregatodos() As Boolean

        tb1 = tab1.conectar("select * from tbAssociados")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _matricula = tb1.Rows(0)("matricula").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _nomemae = tb1.Rows(0)("nomemae").ToString
        _situacaopb = tb1.Rows(0)("situacaopb").ToString
        _sexo = tb1.Rows(0)("sexo").ToString
        _dtnasc = FormatDateTime(tb1.Rows(0)("dtnasc").ToString, DateFormat.ShortDate)
        _cpf = tb1.Rows(0)("cpf").ToString
        _rg = tb1.Rows(0)("rg").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _numero = tb1.Rows(0)("numero").ToString
        _complemento = tb1.Rows(0)("complemento").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        _estado = tb1.Rows(0)("estado").ToString
        _telddd = tb1.Rows(0)("telddd").ToString
        _telefone = tb1.Rows(0)("telefone").ToString
        _email = tb1.Rows(0)("email").ToString
        _dtposse = FormatDateTime(tb1.Rows(0)("dtposse").ToString, DateFormat.ShortDate)
        _dtaposentadoria = FormatDateTime(tb1.Rows(0)("dtaposentadoria").ToString, DateFormat.ShortDate)
        _senha = tb1.Rows(0)("senha").ToString
        _agencia = tb1.Rows(0)("agencia").ToString
        _contacorrente = tb1.Rows(0)("contacorrente").ToString
        _autorizacao = tb1.Rows(0)("autorizacao").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _usercad = tb1.Rows(0)("usercad").ToString
        _ativo_old = tb1.Rows(0)("ativo_old").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        _matriculactrl = tb1.Rows(0)("matriculactrl").ToString
        _codigo = tb1.Rows(0)("codigo").ToString
        _userexcluir = tb1.Rows(0)("userexcluir").ToString
        _dtexcluido = FormatDateTime(tb1.Rows(0)("dtexcluido").ToString, DateFormat.ShortDate)
        _dddcelular = tb1.Rows(0)("dddcelular").ToString
        _celular = tb1.Rows(0)("celular").ToString
        _bloqueado_old = tb1.Rows(0)("bloqueado_old").ToString
        _bloqueado = tb1.Rows(0)("bloqueado").ToString
        _dtbloqueio = FormatDateTime(tb1.Rows(0)("dtbloqueio").ToString, DateFormat.ShortDate)
        _userbloqueio = tb1.Rows(0)("userbloqueio").ToString
        _dtdesbloqueio = FormatDateTime(tb1.Rows(0)("dtdesbloqueio").ToString, DateFormat.ShortDate)
        _userdesbloqueio = tb1.Rows(0)("userdesbloqueio").ToString
        _ip = tb1.Rows(0)("ip").ToString
        _dtsolexc = tb1.Rows(0)("dtsolexc").ToString
        _motiexc = tb1.Rows(0)("motiexc").ToString
        _dtinad = tb1.Rows(0)("dtinad").ToString
        _filiacao = tb1.Rows(0)("filiacao").ToString
        _vivomorto = tb1.Rows(0)("vivomorto").ToString
        _negociado = tb1.Rows(0)("negociado").ToString

        '   _table = tb1
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function









    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & Table & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  " & Table & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
    Public Function CalcularDigitoModulo11(ByVal numero As String, Optional AdmiteDigZero As Boolean = False, Optional pesoinicial As Integer = 2, Optional condicaobradesco As Boolean = False) As Integer

        Dim soma As Integer = 0
        Dim peso As Integer = pesoinicial
        Dim digito As Integer
        Dim base As Integer = 9
        If condicaobradesco Then base = 7

        For I = numero.Length - 1 To 0 Step -1

            soma = soma + numero.Substring(I, 1) * peso

            If pesoinicial <> 2 Then
                If (peso > 2) Then
                    peso = peso - 1
                Else
                    peso = pesoinicial
                End If
            Else
                If (peso < base) Then
                    peso = peso + 1
                Else
                    peso = 2
                End If
            End If


        Next I


        If condicaobradesco AndAlso (soma Mod 11) = 1 Then
            ' se o resto for 1, retorna "P", represnetado como -1 no digito
            Return -1
        End If

        digito = 11 - (soma Mod 11)

        If digito > 9 Then
            digito = 0
        End If

        If Not AdmiteDigZero AndAlso digito = 0 Then
            digito = 1
        End If

        Return digito

    End Function


    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update" & Table & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class



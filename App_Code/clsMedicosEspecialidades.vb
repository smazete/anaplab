﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Public Class clsMedicosEspecialidades
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim tablename As String = "tbmedicosespecialidades"
    Dim _nomemedico As String = ""
    Dim _descricao As String = ""
    Dim _table As Data.DataTable
    Dim _mensagemerro As String
    Dim _contador As Integer
    Dim _nrseq As Integer
    Dim _nrseqmedico As Integer
    Dim _nrseqespecialidade As Integer
    Dim _vlrepasse As Decimal = 0
    Dim _valor As Decimal = 0
    Dim _plano As String = ""
    Dim _percrepasse As Decimal = 0
    Dim _enviadosite As Integer
    Dim _ativo As Boolean = True
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _nrseqempresa As Integer = 0
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _nrseqctrl As String
    Dim _listaespecialidades As New List(Of clsMedicosEspecialidades)
    Private _valorespecialidades1 As Decimal = 0
    Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqmedico As Integer
        Get
            Return _nrseqmedico
        End Get
        Set(value As Integer)
            _nrseqmedico = value
        End Set
    End Property

    Public Property Nrseqespecialidade As Integer
        Get
            Return _nrseqespecialidade
        End Get
        Set(value As Integer)
            _nrseqespecialidade = value
        End Set
    End Property

    Public Property Vlrepasse As Decimal
        Get
            Return _vlrepasse
        End Get
        Set(value As Decimal)
            _vlrepasse = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Percrepasse As Decimal
        Get
            Return _percrepasse
        End Get
        Set(value As Decimal)
            _percrepasse = value
        End Set
    End Property

    Public Property Enviadosite As Integer
        Get
            Return _enviadosite
        End Get
        Set(value As Integer)
            _enviadosite = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _valor
        End Get
        Set(value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
            If value <> "" Then
                tbx = tabx.conectar("select * from tbespecialidades where descricao = '" & value & "'")
                If tbx.Rows.Count <> 0 Then
                    _nrseqespecialidade = tbx.Rows(0)("cod").ToString
                End If
            End If
        End Set
    End Property

    Public Property Nomemedico As String
        Get
            Return _nomemedico
        End Get
        Set(value As String)
            _nomemedico = value
            If value <> "" Then
                tbx = tabx.conectar("select * from tbmedicos where nome = '" & _nomemedico & "'")
                If tbx.Rows.Count <> 0 Then
                    _nrseqmedico = tbx.Rows(0)("cod").ToString
                End If
            End If
        End Set
    End Property

    Public Property Listaespecialidades As List(Of clsMedicosEspecialidades)
        Get
            Return _listaespecialidades
        End Get
        Set(value As List(Of clsMedicosEspecialidades))
            _listaespecialidades = value
        End Set
    End Property

    Public Property Valorespecialidades As Decimal
        Get
            Return _valorespecialidades1
        End Get
        Set(value As Decimal)
            _valorespecialidades1 = value
        End Set
    End Property
End Class

Partial Public Class clsMedicosEspecialidades

    Public Function procurar() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbmedicosespecialidades where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _nrseqmedico = numeros(tb1.Rows(0)("nrseqmedico").ToString)
        _nrseqespecialidade = numeros(tb1.Rows(0)("nrseqespecialidade").ToString)
        _vlrepasse = numeros(tb1.Rows(0)("vlrepasse").ToString)
        _plano = tb1.Rows(0)("plano").ToString
        _percrepasse = numeros(tb1.Rows(0)("percrepasse").ToString)

        _ativo = logico(tb1.Rows(0)("ativo").ToString)
        '_dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        ' _userexclui = tb1.Rows(0)("userexclui").ToString
        _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procurarvalor() As Boolean

        tb1 = tab1.conectar("select * from vwmedicosespecialidades where nrseqmedico = " & _nrseqmedico & " and descricaoespecialidades = '" & _descricao & "' and plano = '" & _plano & "' and ativo = true")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _nrseqmedico = numeros(tb1.Rows(0)("nrseqmedico").ToString)
        _nrseqespecialidade = numeros(tb1.Rows(0)("nrseqespecialidade").ToString)
        _vlrepasse = numeros(tb1.Rows(0)("vlrepasse").ToString)
        _plano = tb1.Rows(0)("plano").ToString
        _percrepasse = numeros(tb1.Rows(0)("percrepasse").ToString)
        Valorespecialidades = numeros(tb1.Rows(0)("valorespecialidades").ToString)
        _ativo = logico(tb1.Rows(0)("ativo").ToString)
        '_dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        ' _userexclui = tb1.Rows(0)("userexclui").ToString
        _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function editar() As Boolean

        tb1 = tab1.conectar("select * from vwmedicosespecialidades where nrseq = '" & Nrseq & "'")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If

        _nrseq = tb1.Rows(0)("nrseq").ToString

        _nrseqmedico = numeros(tb1.Rows(0)("nrseqmedico").ToString)
        _nrseqespecialidade = numeros(tb1.Rows(0)("nrseqespecialidade").ToString)
        _vlrepasse = numeros(tb1.Rows(0)("vlrepasse").ToString)
        _plano = tb1.Rows(0)("plano").ToString
        _percrepasse = numeros(tb1.Rows(0)("percrepasse").ToString)
        Valorespecialidades = numeros(tb1.Rows(0)("valorespecialidades").ToString)
        _ativo = logico(tb1.Rows(0)("ativo").ToString)
        _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String

            tb1 = tab1.conectar("select * from tbmedicosespecialidades where nrseqespecialidade = " & _nrseqespecialidade & " and nrseqmedico = " & moeda(_nrseqmedico) & " and plano = '" & _plano & "'")
        If tb1.Rows.Count = 0 Then
                Dim wcnrseqctrl As String = gerarnrseqcontrole()
                tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
                tb1 = tab1.conectar("Select * from  " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
                _nrseq = tb1.Rows(0)("nrseq").ToString

            Else
                _nrseq = tb1.Rows(0)("nrseq").ToString
            End If

            xsql = " update tbmedicosespecialidades Set ativo = True, vlrepasse = " & moeda(_vlrepasse) & ", valor = " & moeda(Valor) & ", percrepasse = " & moeda(_percrepasse)

            If _nrseqmedico <> 0 Then
                xsql &= ",nrseqmedico = " & moeda(_nrseqmedico)
            End If
            If _nrseqespecialidade <> 0 Then
                xsql &= ",nrseqespecialidade = " & moeda(_nrseqespecialidade)
            End If


            If _plano <> "" Then
                xsql &= ",plano = '" & tratatexto(_plano) & "'"
            End If


            If _nrseqempresa <> 0 Then
                xsql &= ",nrseqempresa = " & moeda(_nrseqempresa)
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function cadastrorapido() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, dtcad, usercad, ativo, nrseqmedico, plano, nrseqespecialidade, valor, vlrepasse) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', true, '" & Nrseqmedico & "', '" & Plano & "','" & Nrseqespecialidade & "','" & moeda(Valor) & "','" & moeda(Vlrepasse) & "' )")

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbmedicosespecialidades set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function carregartodaspormedico() As Boolean

        Try
            _listaespecialidades.Clear()
            tbx = tabx.conectar("select * from vwmedicosespecialidades where ativo = true and nrseqmedico = '" & _nrseqmedico & "'")
            If tbx.Rows.Count = 0 Then
                _mensagemerro = "Nenhuma especialidade direcionada ao profissional "
                Return False
            End If
            For x As Integer = 0 To tbx.Rows.Count - 1
                _listaespecialidades.Add(New clsMedicosEspecialidades With {.Descricao = tbx.Rows(x)("descricaoespecialidades").ToString, .Nrseq = tbx.Rows(x)("nrseq").ToString, .Vlrepasse = tbx.Rows(x)("Vlrepasse").ToString, .Valorespecialidades = tbx.Rows(x)("Valorespecialidades").ToString, .Plano = tbx.Rows(x)("plano").ToString})
            Next

            _table = tbx
            _contador = tbx.Rows.Count

            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
End Class
﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports clsSmart


Public Class clsimportcsv

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _listaclasse As New List(Of clsimportcsv)
    Dim _nrseq As Integer
    Dim _nrseqcliente As Integer
    Dim _nrseqmensalidade As Integer
    Dim _nomecliente As String
    Dim _outrosdados As Integer
    Dim _data As Date
    Dim _ativo As Boolean
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _operacao As String
    Dim _userexclui As String
    Dim _agencia As String
    Dim _contacorrente As String
    Dim _nrseqctrl As String
    Dim _nomearquivo As String
    Dim _obs As String
    Dim _valor As Decimal
    Dim _vinculada As Boolean

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Listaclasse As List(Of clsimportcsv)
        Get
            Return _listaclasse
        End Get
        Set(value As List(Of clsimportcsv))
            _listaclasse = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqcliente As Integer
        Get
            Return _nrseqcliente
        End Get
        Set(value As Integer)
            _nrseqcliente = value
        End Set
    End Property

    Public Property Nrseqmensalidade As Integer
        Get
            Return _nrseqmensalidade
        End Get
        Set(value As Integer)
            _nrseqmensalidade = value
        End Set
    End Property

    Public Property Nomecliente As String
        Get
            Return _nomecliente
        End Get
        Set(value As String)
            _nomecliente = value
        End Set
    End Property

    Public Property Outrosdados As Integer
        Get
            Return _outrosdados
        End Get
        Set(value As Integer)
            _outrosdados = value
        End Set
    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Operacao As String
        Get
            Return _operacao
        End Get
        Set(value As String)
            _operacao = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Agencia As String
        Get
            Return _agencia
        End Get
        Set(value As String)
            _agencia = value
        End Set
    End Property

    Public Property Contacorrente As String
        Get
            Return _contacorrente
        End Get
        Set(value As String)
            _contacorrente = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _valor
        End Get
        Set(value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property Vinculada As Boolean
        Get
            Return _vinculada
        End Get
        Set(value As Boolean)
            _vinculada = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Obs As String
        Get
            Return _obs
        End Get
        Set(value As String)
            _obs = value
        End Set
    End Property

    Public Property Nomearquivo As String
        Get
            Return _nomearquivo
        End Get
        Set(value As String)
            _nomearquivo = value
        End Set
    End Property
End Class

Partial Public Class clsimportcsv

    Public Function Listarimportcsv() As Boolean
        Try
            Listaclasse.Clear()
            tb1 = tab1.conectar("select * from tbimportcsv where ativo = true")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaclasse.Add(New clsimportcsv With {.Nrseq = numeros(tb1.Rows(x)("nrseq").ToString), .Nrseqcliente = numeros(tb1.Rows(x)("nrseqcliente").ToString), .Nrseqmensalidade = numeros(tb1.Rows(x)("nrseqmensalidade").ToString), .Nomecliente = tb1.Rows(x)("nomecliente").ToString, .Outrosdados = numeros(tb1.Rows(x)("outrosdados").ToString), .Data = valordata(tb1.Rows(x)("data").ToString), .Ativo = tb1.Rows(x)("ativo").ToString, .Usercad = tb1.Rows(x)("usercad").ToString, .Dtexclui = valordata(tb1.Rows(x)("dtexclui").ToString), .Operacao = tb1.Rows(x)("operacao").ToString, .Userexclui = tb1.Rows(x)("userexclui").ToString, .Agencia = tb1.Rows(x)("agencia").ToString, .Contacorrente = tb1.Rows(x)("contacorrente").ToString, .Valor = tb1.Rows(x)("valor").ToString, .Vinculada = tb1.Rows(x)("vinculada").ToString})
            Next
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function procurar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbimportcsv where nrseq = '" & Nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If

            Nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
            Nrseqcliente = numeros(tb1.Rows(0)("nrseqcliente").ToString)
            Nrseqmensalidade = numeros(tb1.Rows(0)("nrseqmensalidade").ToString)
            Nomecliente = tb1.Rows(0)("nomecliente").ToString
            Outrosdados = numeros(tb1.Rows(0)("outrosdados").ToString)
            Data = FormatDateTime(valordata(tb1.Rows(0)("data").ToString), DateFormat.ShortDate)
            Ativo = logico(tb1.Rows(0)("ativo").ToString)
            Usercad = tb1.Rows(0)("usercad").ToString
            Dtexclui = FormatDateTime(valordata(tb1.Rows(0)("dtexclui").ToString), DateFormat.ShortDate)
            Operacao = tb1.Rows(0)("operacao").ToString
            Userexclui = tb1.Rows(0)("userexclui").ToString
            Agencia = tb1.Rows(0)("agencia").ToString
            Contacorrente = tb1.Rows(0)("contacorrente").ToString
            Valor = tb1.Rows(0)("valor").ToString
            Vinculada = logico(tb1.Rows(0)("vinculada").ToString)

            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procuraexiste() As Boolean
        Try
            'ver com o claudio melhor maneira para controlar
            'tb1 = tab1.conectar("select * from tbimportcsv where nomearquivo = '" & Nomearquivo & "' and data ='" & Data & "' and agencia = '" & Agencia & "' and conta = '" & Contacorrente & "' and valor = '" & Valor & "' and outros = '" & Outrosdados & "'")
            tb1 = tab1.conectar("select * from tbimportcsv where  agencia = '" & Agencia & "' and contacorrente = '" & Contacorrente & "' and valor = '" & Valor & "' and outrosdados = '" & Outrosdados & "'")
            If tb1.Rows.Count = 0 Then
                Return True
            End If
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvarmanual() As Boolean
        Try
            If procuraexiste() = True Then
                novo()

                Dim xsql As String
                xsql = " update tbimportcsv Set ativo = True "

                If Nrseqcliente <> 0 Then
                    xsql &= ",nrseqcliente = '" & moeda(Nrseqcliente) & "'"
                End If
                If Nrseqmensalidade <> 0 Then
                    xsql &= ",nrseqmensalidade = '" & moeda(Nrseqmensalidade) & "'"
                End If
                If Nomecliente <> "" Then
                    xsql &= ",nomecliente = '" & tratatexto(Nomecliente) & "'"
                End If
                If Outrosdados <> 0 Then
                    xsql &= ",outrosdados = '" & moeda(Outrosdados) & "'"
                End If
                If formatadatamysql(Data) <> "" Then
                    xsql &= ",data = '" & formatadatamysql(Data) & "'"
                End If
                If Usercad <> "" Then
                    xsql &= ",usercad = '" & tratatexto(Usercad) & "'"
                End If
                If Agencia <> "" Then
                    xsql &= ",agencia = '" & tratatexto(Agencia) & "'"
                End If
                If Contacorrente <> "" Then
                    xsql &= ",contacorrente = '" & Contacorrente & "'"
                End If
                If moeda(Valor) <> "" Then
                    xsql &= ",valor = '" & moeda(Valor) & "'"
                End If
                If Vinculada <> True Then
                    xsql &= ",vinculada = '" & logico(Vinculada) & "'"
                End If
                xsql &= " where nrseq = '" & Nrseq & "'"
                tb1 = tab1.IncluirAlterarDados(xsql)
                Return True

            Else

                Return False
            End If

        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function salvarauto() As Boolean
        Try
            If procuraexiste() = True Then
                novo()



                Dim xsql As String
                xsql = " update tbimportcsv Set ativo = '" & logico(Ativo) & "'"

                If Nrseqcliente <> 0 Then
                    xsql &= ",nrseqcliente = " & Nrseqcliente
                End If
                If Nrseqmensalidade <> 0 Then
                    xsql &= ",nrseqmensalidade = " & Nrseqmensalidade
                End If
                If Nomecliente <> "" Then
                    xsql &= ",nomecliente = '" & tratatexto(Nomecliente) & "'"
                End If
                If Outrosdados <> 0 Then
                    xsql &= ",outrosdados = " & moeda(Outrosdados) & "'"
                End If
                If formatadatamysql(Data) <> "" Then
                    xsql &= ",data = '" & formatadatamysql(Data) & "'"
                End If
                If Usercad <> "" Then
                    xsql &= ",usercad = '" & tratatexto(Usercad) & "'"
                End If
                If Agencia <> "" Then
                    xsql &= ",agencia = '" & tratatexto(Agencia) & "'"
                End If
                If Contacorrente <> "" Then
                    xsql &= ",contacorrente = '" & sonumeros(Contacorrente) & "'"
                End If
                If moeda(Valor) <> "" Then
                    xsql &= ",valor = '" & moeda(Valor) & "'"
                End If
                If Vinculada <> True Then
                    xsql &= ",vinculada = '" & logico(Vinculada) & "'"
                End If
                xsql &= " where nrseq = " & Nrseq
                tb1 = tab1.IncluirAlterarDados(xsql)
                Return True
            Else
                Return False
            End If

        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbimportcsv (nrseqctrl, data, usercad, ativo, operacao) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false, 'C')")
            tb1 = tab1.conectar("Select * from  tbimportcsv where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbimportcsv set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class
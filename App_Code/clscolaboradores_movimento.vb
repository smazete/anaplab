﻿Imports Microsoft.VisualBasic
Imports clsSmart

Public Class clscolaboradores_movimento

    'Desenvolvedor: Claudio Smart
    'Data inicial :  29/08/2019
    'Data Atualização :  29/08/2019 - por Claudio

    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nrseqcolaborador As Integer = 0
    Dim _nrseqcolempresa As Integer = 0
    Dim _dtcad As Date
    Dim _dtadmissao As Date
    Dim _dtdemissao As Date
    Dim _ativo As Boolean
    Dim _usercad As String

    Public Sub New()

    End Sub
    Public Sub New(usuariocarga As String)
        _usercad = usuariocarga
        _dtcad = Data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqcolaborador As Integer
        Get
            Return _nrseqcolaborador
        End Get
        Set(value As Integer)
            _nrseqcolaborador = value
        End Set
    End Property

    Public Property Nrseqcolempresa As Integer
        Get
            Return _nrseqcolempresa
        End Get
        Set(value As Integer)
            _nrseqcolempresa = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Dtadmissao As Date
        Get
            Return _dtadmissao
        End Get
        Set(value As Date)
            _dtadmissao = value
        End Set
    End Property

    Public Property Dtdemissao As Date
        Get
            Return _dtdemissao
        End Get
        Set(value As Date)
            _dtdemissao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property
End Class

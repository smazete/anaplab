﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Public Class clsConvenios

    Dim tablename As String = "tbconvenios"
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _contador As Integer
    Dim _table As Data.DataTable
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _nome As String
    Dim _cnpj As String
    Dim _ativo As Boolean
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _endereco As String
    Dim _numero As String
    Dim _compl As String
    Dim _bairro As String
    Dim _cidade As String
    Dim _uf As String
    Dim _telefonefixo As String
    Dim _telefonecel As String
    Dim _url As String
    Dim _facebook As String
    Dim _insta As String
    Dim _twitter As String
    Dim _dtexclui As DateTime
    Dim _userexclui As String
    Dim _email As String
    Dim _salvo As Boolean
    Dim _nrseqctrl As String
    Dim _cep As String
    Dim _nomefantasia As String
    Dim _whats As String
    Dim _nrseqempresa As Integer

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Cnpj As String
        Get
            Return _cnpj
        End Get
        Set(value As String)
            _cnpj = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Compl As String
        Get
            Return _compl
        End Get
        Set(value As String)
            _compl = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Uf As String
        Get
            Return _uf
        End Get
        Set(value As String)
            _uf = value
        End Set
    End Property

    Public Property Telefonefixo As String
        Get
            Return _telefonefixo
        End Get
        Set(value As String)
            _telefonefixo = value
        End Set
    End Property

    Public Property Telefonecel As String
        Get
            Return _telefonecel
        End Get
        Set(value As String)
            _telefonecel = value
        End Set
    End Property

    Public Property Url As String
        Get
            Return _url
        End Get
        Set(value As String)
            _url = value
        End Set
    End Property

    Public Property Facebook As String
        Get
            Return _facebook
        End Get
        Set(value As String)
            _facebook = value
        End Set
    End Property

    Public Property Insta As String
        Get
            Return _insta
        End Get
        Set(value As String)
            _insta = value
        End Set
    End Property

    Public Property Twitter As String
        Get
            Return _twitter
        End Get
        Set(value As String)
            _twitter = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Salvo As Boolean
        Get
            Return _salvo
        End Get
        Set(value As Boolean)
            _salvo = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Nomefantasia As String
        Get
            Return _nomefantasia
        End Get
        Set(value As String)
            _nomefantasia = value
        End Set
    End Property

    Public Property Whats As String
        Get
            Return _whats
        End Get
        Set(value As String)
            _whats = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property
End Class

Partial Public Class clsConvenios

    Public Function consultarEmpresaNomefantasia() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & tablename & "  where nrseqempresa = '" & _nrseqempresa & "' and nomefantasia = '" & _nomefantasia & "' order by nomefantasia")
        If tb1.Rows.Count = 0 Then
            _mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _cnpj = tb1.Rows(0)("cnpj").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
        _usercad = tb1.Rows(0)("usercad").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _numero = tb1.Rows(0)("numero").ToString
        _compl = tb1.Rows(0)("compl").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        _uf = tb1.Rows(0)("uf").ToString
        _telefonefixo = tb1.Rows(0)("telefonefixo").ToString
        _telefonecel = tb1.Rows(0)("telefonecel").ToString
        _url = tb1.Rows(0)("url").ToString
        _facebook = tb1.Rows(0)("facebook").ToString
        _insta = tb1.Rows(0)("insta").ToString
        _twitter = tb1.Rows(0)("twitter").ToString
        _dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        _userexclui = tb1.Rows(0)("userexclui").ToString
        _email = tb1.Rows(0)("email").ToString
        _salvo = tb1.Rows(0)("salvo").ToString
        _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _nomefantasia = tb1.Rows(0)("nomefantasia").ToString
        _whats = tb1.Rows(0)("whats").ToString
        _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString

        _contador = tb1.Rows.Count
        _table = tb1

        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarEmpresa() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbconvenios where nrseq = '" & Nrseq & "' order by nomefantasia")
        If tb1.Rows.Count = 0 Then
            _mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _cnpj = tb1.Rows(0)("cnpj").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
        _usercad = tb1.Rows(0)("usercad").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _numero = tb1.Rows(0)("numero").ToString
        _compl = tb1.Rows(0)("compl").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        _uf = tb1.Rows(0)("uf").ToString
        _telefonefixo = tb1.Rows(0)("telefonefixo").ToString
        _telefonecel = tb1.Rows(0)("telefonecel").ToString
        _url = tb1.Rows(0)("url").ToString
        _facebook = tb1.Rows(0)("facebook").ToString
        _insta = tb1.Rows(0)("insta").ToString
        _twitter = tb1.Rows(0)("twitter").ToString
        _dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        _userexclui = tb1.Rows(0)("userexclui").ToString
        _email = tb1.Rows(0)("email").ToString
        '     _salvo = tb1.Rows(0)("salvo").ToString
        _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _nomefantasia = tb1.Rows(0)("nomefantasia").ToString
        _whats = tb1.Rows(0)("whats").ToString
        _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString

        _contador = tb1.Rows.Count
        _table = tb1

        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarnome(Optional exibirativos As Boolean = True, Optional buscarnomes As Boolean = True) As Boolean

        tb1 = tab1.conectar("select * from " & tablename & " where salvo=True and ativo=" & _ativo & IIf(buscarnomes = True, " and nome like '" & _nome & "%'", ""))

        If tb1.Rows.Count > 0 Then
            _nrseq = tb1.Rows(0)("nrseq").ToString
            _nome = tb1.Rows(0)("nome").ToString
            _cnpj = tb1.Rows(0)("cnpj").ToString
            _ativo = tb1.Rows(0)("ativo").ToString
            _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
            _usercad = tb1.Rows(0)("usercad").ToString
            _endereco = tb1.Rows(0)("endereco").ToString
            _numero = tb1.Rows(0)("numero").ToString
            _compl = tb1.Rows(0)("compl").ToString
            _bairro = tb1.Rows(0)("bairro").ToString
            _cidade = tb1.Rows(0)("cidade").ToString
            _uf = tb1.Rows(0)("uf").ToString
            _telefonefixo = tb1.Rows(0)("telefonefixo").ToString
            _telefonecel = tb1.Rows(0)("telefonecel").ToString
            _url = tb1.Rows(0)("url").ToString
            _facebook = tb1.Rows(0)("facebook").ToString
            _insta = tb1.Rows(0)("insta").ToString
            _twitter = tb1.Rows(0)("twitter").ToString
            _dtexclui = valordata(tb1.Rows(0)("dtexclui").ToString)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            _email = tb1.Rows(0)("email").ToString
            '     _salvo = tb1.Rows(0)("salvo").ToString
            _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
            _cep = tb1.Rows(0)("cep").ToString
            _nomefantasia = tb1.Rows(0)("nomefantasia").ToString
            _whats = tb1.Rows(0)("whats").ToString
            ' _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
        Else
            _mensagemerro = "Nenhum convenio encontrado"

        End If

        _contador = tb1.Rows.Count
        _table = tb1

        If _mensagemerro <> "" Then
            Return False
        End If

        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultartodos() As Boolean

        tb1 = tab1.conectar("select * from " & tablename & " where ativo=True and salvo=True")

        If tb1.Rows.Count > 0 Then
            _nrseq = tb1.Rows(0)("nrseq").ToString
            _nome = tb1.Rows(0)("nome").ToString
            _cnpj = tb1.Rows(0)("cnpj").ToString
            _ativo = tb1.Rows(0)("ativo").ToString
            _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
            _usercad = tb1.Rows(0)("usercad").ToString
            _endereco = tb1.Rows(0)("endereco").ToString
            _numero = tb1.Rows(0)("numero").ToString
            _compl = tb1.Rows(0)("compl").ToString
            _bairro = tb1.Rows(0)("bairro").ToString
            _cidade = tb1.Rows(0)("cidade").ToString
            _uf = tb1.Rows(0)("uf").ToString
            _telefonefixo = tb1.Rows(0)("telefonefixo").ToString
            _telefonecel = tb1.Rows(0)("telefonecel").ToString
            _url = tb1.Rows(0)("url").ToString
            _facebook = tb1.Rows(0)("facebook").ToString
            _insta = tb1.Rows(0)("insta").ToString
            _twitter = tb1.Rows(0)("twitter").ToString
            _dtexclui = valordata(tb1.Rows(0)("dtexclui").ToString)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            _email = tb1.Rows(0)("email").ToString
            '     _salvo = tb1.Rows(0)("salvo").ToString
            _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
            _cep = tb1.Rows(0)("cep").ToString
            _nomefantasia = tb1.Rows(0)("nomefantasia").ToString
            _whats = tb1.Rows(0)("whats").ToString
            ' _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
        Else
            _mensagemerro = "Nenhum convenio encontrado"

        End If

        _contador = tb1.Rows.Count
        _table = tb1

        If _mensagemerro <> "" Then
            Return False
        End If

        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procurar(Optional exibirativos As Boolean = False) As Boolean
        If _nrseq = 0 Then
            _mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & tablename & " where nrseq = " & _nrseq & IIf(exibirativos = True, "ativo=True and Salvo=True", ""))

        If tb1.Rows.Count = 0 Then
            _mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _cnpj = tb1.Rows(0)("cnpj").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
        _usercad = tb1.Rows(0)("usercad").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _numero = tb1.Rows(0)("numero").ToString
        _compl = tb1.Rows(0)("compl").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        _uf = tb1.Rows(0)("uf").ToString
        _telefonefixo = tb1.Rows(0)("telefonefixo").ToString
        _telefonecel = tb1.Rows(0)("telefonecel").ToString
        _url = tb1.Rows(0)("url").ToString
        _facebook = tb1.Rows(0)("facebook").ToString
        _insta = tb1.Rows(0)("insta").ToString
        _twitter = tb1.Rows(0)("twitter").ToString
        _dtexclui = valordata(tb1.Rows(0)("dtexclui").ToString)
        _userexclui = tb1.Rows(0)("userexclui").ToString
        _email = tb1.Rows(0)("email").ToString
        '     _salvo = tb1.Rows(0)("salvo").ToString
        _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _nomefantasia = tb1.Rows(0)("nomefantasia").ToString
        _whats = tb1.Rows(0)("whats").ToString
        ' _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString


        _contador = tb1.Rows.Count
        _table = tb1

        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String

            xsql = " update " & tablename & " Set ativo = True,salvo=True"
            If _nome <> "" Then
                xsql &= ", nome = '" & tratatexto(_nome) & "'"
        End If
            If _cnpj <> "" Then
                xsql &= ", cnpj = '" & tratatexto(_cnpj) & "'"
            End If
            If _endereco <> "" Then
                xsql &= ", endereco = '" & tratatexto(_endereco) & "'"
            End If
            If _numero <> "" Then
                xsql &= ", numero = '" & tratatexto(_numero) & "'"
            End If
            If _compl <> "" Then
                xsql &= ", compl = '" & tratatexto(_compl) & "'"
            End If
            If _bairro <> "" Then
                xsql &= ", bairro = '" & tratatexto(_bairro) & "'"
            End If
            If _cidade <> "" Then
                xsql &= ", cidade = '" & tratatexto(_cidade) & "'"
            End If
            If _uf <> "" Then
                xsql &= ", uf = '" & tratatexto(_uf) & "'"
            End If
            If _telefonefixo <> "" Then
                xsql &= ", telefonefixo = '" & tratatexto(_telefonefixo) & "'"
            End If
            If _telefonecel <> "" Then
                xsql &= ", telefonecel = '" & tratatexto(_telefonecel) & "'"
            End If
            If _url <> "" Then
                xsql &= ", url = '" & tratatexto(_url) & "'"
            End If
            If _facebook <> "" Then
                xsql &= ", facebook = '" & tratatexto(_facebook) & "'"
            End If
            If _insta <> "" Then
                xsql &= ", insta = '" & tratatexto(_insta) & "'"
            End If
            If _twitter <> "" Then
                xsql &= ", twitter = '" & tratatexto(_twitter) & "'"
            End If
            If _email <> "" Then
                xsql &= ", email = '" & tratatexto(_email) & "'"
            End If
            If _cep <> "" Then
                xsql &= ", cep = '" & tratatexto(_cep) & "'"
            End If
            If _nomefantasia <> "" Then
                xsql &= ", nomefantasia = '" & tratatexto(_nomefantasia) & "'"
            End If
            If _whats <> "" Then
                xsql &= ", whats = '" & tratatexto(_whats) & "'"
            End If
            '    If _nrseqempresa <> 0 Then
            '         xsql &= ",nrseqempresa = " & moeda(_nrseqempresa)
            '   End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, ativo, dtcad, usercad) values ('" & wcnrseqctrl & "',false,'" & valordatamysql(hoje()) & "','" & HttpContext.Current.Session("usuario") & "')")

            tb1 = tab1.conectar("select * from " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


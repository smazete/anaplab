﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data

Public Class clscolaboradores

    'Desenvolvedor: Claudio Smart
    'Data inicial :  23/08/2019
    'Data Atualização :  23/08/2019 - por Claudio
    'Data Atualização :  23/08/2019 - por Claudio
    'Data Atualização :  02/09/2019 - por Claudio



#Region "listas"

    Dim _listaempresas As New List(Of clscolaboradores_empresas)
    Dim _listamovimentos As New List(Of clscolaboradores_movimento)

#End Region


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim _emailalterado As String
    Dim _nrseqempresaatual As Integer = 0
    Dim _validargestor As Boolean = False
    Dim _codigocargoatual As String
    Dim _codigobuatual As String
    Dim _desligado As String
    Dim _buatual As String = ""
    Dim _contrato As String
    Dim _nrseqcargoatual As Integer = 0
    Dim _nrseqbuatual As Integer = 0
    Dim _listacadgoatual As New List(Of ClsCargos)
    Dim _listabuatual As New List(Of ClsBus)

    Dim _pads As New Data.DataTable
    Dim _permissaopadrao As String
    Dim _gravarusuario As Boolean = False
    Dim _count As Integer = 0
    Dim _procurarpor As String = "nrseq"
    Dim _empresaatual As String
    Dim _cargoatual As String = ""
    Dim _dtadmissaoatual As Date
    Dim _dtdemissaoatual As Date
    Dim _nomegestor As String
    Dim _nrseqgestor As Integer = 0
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nome As String = ""
    Dim _endereco As String = ""
    Dim _numero As String = ""
    Dim _bairro As String = ""
    Dim _cidade As String = ""
    Dim _uf As String = ""
    Dim _telefone As String = ""
    Dim _celular As String = ""
    Dim _dtcad As DateTime = data()
    Dim _usercad As String
    Dim _ativo As Boolean
    Dim _dtexclui As Date
    Dim _userexclui As String

    Dim _status As String
    Dim _Horista As Boolean
    Dim _matricula As String

    Dim _nrseqpermisssao As Integer = 0
    Dim _nrsequsuario As Integer = 0
    Dim _email As String

    Dim _usuario As String

    Dim _nrseqctrl As String
    Dim _cep As String
    Dim _complemento As String


    Dim _gestor As Boolean

    Public Sub New()
        versessao()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub

    Public Sub New(usuariocarga As String)
        _usercad = usuariocarga
        _dtcad = data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Uf As String
        Get
            Return _uf
        End Get
        Set(value As String)
            _uf = value
        End Set
    End Property

    Public Property Telefone As String
        Get
            Return _telefone
        End Get
        Set(value As String)
            _telefone = value
        End Set
    End Property

    Public Property Celular As String
        Get
            Return _celular
        End Get
        Set(value As String)
            _celular = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property



    Public Property Status As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property

    Public Property Horista As Boolean
        Get
            Return _Horista
        End Get
        Set(value As Boolean)
            _Horista = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property





    Public Property Nrseqpermisssao As Integer
        Get
            Return _nrseqpermisssao
        End Get
        Set(value As Integer)
            _nrseqpermisssao = value
        End Set
    End Property

    Public Property Nrsequsuario As Integer
        Get
            Return _nrsequsuario
        End Get
        Set(value As Integer)
            _nrsequsuario = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property





    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return _usuario
        End Get
        Set(value As String)

            _usuario = value
            If _usuario = "" Then
                _nrsequsuario = 0
            Else
                tbx = tabx.conectar("select * from vwusuarios where usuario = '" & _usuario & "'")
                If tbx.Rows.Count > 0 Then
                    _nrsequsuario = tbx.Rows(0)("nrseq").ToString
                Else
                    _nrsequsuario = 0
                End If
            End If
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Complemento As String
        Get
            Return _complemento
        End Get
        Set(value As String)
            _complemento = value
        End Set
    End Property





    Public Property Nomegestor As String
        Get
            Return _nomegestor
        End Get
        Set(value As String)
            _nomegestor = value
            If value = "" Then
                _nrseqgestor = 0
            Else
                tbx = tabx.conectar("select * from tbcolaboradores where nome = '" & value & "'")
                If tbx.Rows.Count > 0 Then
                    _nrseqgestor = tbx.Rows(0)("nrseq").ToString
                Else
                    _nrseqgestor = 0
                End If
            End If
        End Set
    End Property

    Public Property Nrseqgestor As Integer
        Get
            Return _nrseqgestor
        End Get
        Set(value As Integer)
            _nrseqgestor = value
        End Set
    End Property

    Public Property Gestor As Boolean
        Get
            Return _gestor
        End Get
        Set(value As Boolean)
            _gestor = value
        End Set
    End Property

    Public Property Listaempresas As List(Of clscolaboradores_empresas)
        Get
            Return _listaempresas
        End Get
        Set(value As List(Of clscolaboradores_empresas))
            _listaempresas = value
        End Set
    End Property

    Public Property Listamovimentos As List(Of clscolaboradores_movimento)
        Get
            Return _listamovimentos
        End Get
        Set(value As List(Of clscolaboradores_movimento))
            _listamovimentos = value
        End Set
    End Property

    Public Property Empresaatual As String
        Get
            Return _empresaatual
        End Get
        Set(value As String)
            _empresaatual = value
        End Set
    End Property

    Public Property Cargoatual As String
        Get
            Return _cargoatual
        End Get
        Set(value As String)
            _cargoatual = tratatexto(value,, 45)



        End Set
    End Property

    Public Property Buatual As String
        Get
            Return _buatual
        End Get
        Set(value As String)
            _buatual = tratatexto(value,, 45)
            'tbx = tabx.conectar("select nrseq from tbbus where descricao = '" & _buatual & "'")
            'If tbx.Rows.Count = 0 Then
            '    Dim wcnrseqctrl As String = gerarnrseqcontrole()
            '    tbx = tabx.IncluirAlterarDados("insert into tbbus (ativo, usercad, dtcad, descricao, codigobu, nrseqctrl) values (true, '" & _usercad & "', '" & hoje() & "', '" & _buatual & "','" & _codigobuatual & "', '" & wcnrseqctrl & "')")
            '    tbx = tabx.conectar("select nrseq from tbbus where nrseqctrl = '" & wcnrseqctrl & "'")
            '    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
            'Else
            '    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
            '  End If
        End Set
    End Property

    Public Property Dtadmissaoatual As Date
        Get
            Return _dtadmissaoatual
        End Get
        Set(value As Date)
            _dtadmissaoatual = value
        End Set
    End Property

    Public Property Dtdemissaoatual As Date
        Get
            Return _dtdemissaoatual
        End Get
        Set(value As Date)
            _dtdemissaoatual = value
        End Set
    End Property

    Public Property Count As Integer
        Get
            Return _count
        End Get
        Set(value As Integer)
            _count = value
        End Set
    End Property

    Public Property Gravarusuario As Boolean
        Get
            Return _gravarusuario
        End Get
        Set(value As Boolean)
            _gravarusuario = value
        End Set
    End Property

    Public Property Permissaopadrao As String
        Get
            Return _permissaopadrao
        End Get
        Set(value As String)
            _permissaopadrao = value
        End Set
    End Property

    Public Property Procurarpor As String
        Get
            Return _procurarpor
        End Get
        Set(value As String)
            _procurarpor = value
        End Set
    End Property

    Public Property Pads As DataTable
        Get
            Return _pads
        End Get
        Set(value As DataTable)
            _pads = value
        End Set
    End Property

    Public Property Codigocargoatual As String
        Get
            Return _codigocargoatual
        End Get
        Set(value As String)
            _codigocargoatual = tratatexto(value,, 45)
            'tbx = tabx.conectar("select * from tbcargos where codigocargo = '" & _codigocargoatual & "' and ativo = true ")
            'If tbx.Rows.Count = 0 Then
            '    Dim wcnrseqctrl As String = gerarnrseqcontrole()
            '    tbx = tabx.IncluirAlterarDados("insert into tbcargos (ativo, usercad, dtcad, descricao, codigocargo, nrseqctrl) values (true, '" & _usercad & "', '" & hoje() & "', '" & _cargoatual & "','" & _codigocargoatual & "', '" & wcnrseqctrl & "')")
            '    tbx = tabx.conectar("select nrseq from tbcargos where nrseqctrl = '" & wcnrseqctrl & "'")
            '    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
            'Else
            '    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
            'End If
        End Set
    End Property

    Public Property Codigobuatual As String
        Get
            Return _codigobuatual
        End Get
        Set(value As String)
            _codigobuatual = tratatexto(value,, 45)
            'tbx = tabx.conectar("select * from tbbus where codigobu = '" & _codigobuatual & "' and ativo = true ")
            'If tbx.Rows.Count = 0 Then
            '    Dim wcnrseqctrl As String = gerarnrseqcontrole()
            '    tbx = tabx.IncluirAlterarDados("insert into tbbus (ativo, usercad, dtcad, descricao, codigobu, nrseqctrl) values (true, '" & _usercad & "', '" & hoje() & "', '" & _buatual & "','" & _codigobuatual & "', '" & wcnrseqctrl & "')")
            '    tbx = tabx.conectar("select nrseq from tbbus where nrseqctrl = '" & wcnrseqctrl & "'")
            '    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
            'Else
            '    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
            'End If
        End Set
    End Property

    Public Property Desligado As String
        Get
            Return _desligado
        End Get
        Set(value As String)
            _desligado = value
        End Set
    End Property

    Public Property Contrato As String
        Get
            Return _contrato
        End Get
        Set(value As String)
            _contrato = value
        End Set
    End Property

    Public Property Nrseqcargoatual As Integer
        Get
            Return _nrseqcargoatual
        End Get
        Set(value As Integer)
            _nrseqcargoatual = value
        End Set
    End Property

    Public Property Nrseqbuatual As Integer
        Get
            Return _nrseqbuatual
        End Get
        Set(value As Integer)
            _nrseqbuatual = value
        End Set
    End Property

    Public Property Validargestor As Boolean
        Get
            Return _validargestor
        End Get
        Set(value As Boolean)
            _validargestor = value
        End Set
    End Property

    Public Property Listacadgoatual As List(Of ClsCargos)
        Get
            Return _listacadgoatual
        End Get
        Set(value As List(Of ClsCargos))
            _listacadgoatual = value
            For x As Integer = 0 To value.Count - 1
                tbx = tabx.conectar("select * from tbcargos where descricao = '" & _listacadgoatual(x).Descricao & "' and codigocargo = '" & _listacadgoatual(x).Codigocargo & "' and ativo = true ")
                If tbx.Rows.Count = 0 Then
                    Dim wcnrseqctrl As String = gerarnrseqcontrole()
                    tbx = tabx.IncluirAlterarDados("insert into tbcargos (ativo, usercad, dtcad, descricao, codigocargo, nrseqctrl) values (true, '" & _usercad & "', '" & hoje() & "', '" & _listacadgoatual(x).Descricao & "','" & _listacadgoatual(x).Codigocargo & "', '" & wcnrseqctrl & "')")
                    tbx = tabx.conectar("select nrseq from tbcargos where nrseqctrl = '" & wcnrseqctrl & "'")
                    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
                Else
                    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
                End If
            Next
        End Set
    End Property

    Public Property Listabuatual As List(Of ClsBus)
        Get
            Return _listabuatual
        End Get
        Set(value As List(Of ClsBus))
            _listabuatual = value
            For x As Integer = 0 To value.Count - 1
                tbx = tabx.conectar("select * from tbbus where descricao = '" & _listabuatual(x).Descricao & "' and codigocargo = '" & _listabuatual(x).Codigobu & "' and ativo = true ")
                If tbx.Rows.Count = 0 Then
                    Dim wcnrseqctrl As String = gerarnrseqcontrole()
                    tbx = tabx.IncluirAlterarDados("insert into tbbus (ativo, usercad, dtcad, descricao, codigocargo, nrseqctrl) values (true, '" & _usercad & "', '" & hoje() & "', '" & _listabuatual(x).Descricao & "','" & _listabuatual(x).Codigobu & "', '" & wcnrseqctrl & "')")
                    tbx = tabx.conectar("select nrseq from tbbus where nrseqctrl = '" & wcnrseqctrl & "'")
                    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
                Else
                    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
                End If
            Next
        End Set
    End Property

    Public Property Nrseqempresaatual As Integer
        Get
            Return _nrseqempresaatual
        End Get
        Set(value As Integer)
            _nrseqempresaatual = value
        End Set
    End Property

    Public Property Emailalterado As String
        Get
            Return _emailalterado
        End Get
        Set(value As String)
            _emailalterado = value
        End Set
    End Property
End Class
Partial Public Class clscolaboradores
    Public Function novo() As Boolean
        Try

            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into tbcolaboradores (ativo, Dtcad, Usercad, Nrseqctrl) values (False, '" & hoje() & "','" & Usercad & "', '" & wcnrseqctrl & "')")

            tb1 = tab1.conectar("select * from tbcolaboradores where nrseqctrl = '" & wcnrseqctrl & "'")

            Nrseq = tb1.Rows(0)("nrseq").ToString
            Nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString

            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function novousuario() As Boolean
        Try

            If Not carregar() Then
                _mensagemerro = "Selecione um colaborador válido !"
                Return False
            End If

            If _email <> _emailalterado Then
                tb1 = tab1.IncluirAlterarDados("update tbcolaboradores set email = '" & _emailalterado & "' where nrseq = " & _nrseq)
            End If
            Dim xusuario As New clsusuarios(_usercad)
            xusuario.Nomecolaborador = _nome
            xusuario.Usuario = _matricula.Trim
            xusuario.Senha = _matricula.Trim
            xusuario.Email = IIf(_emailalterado = "", _matricula.Trim & "@sistemasceva.com.br", _emailalterado)
            xusuario.Permissao = _permissaopadrao
            If Not xusuario.salvarusuario() Then
                _mensagemerro = xusuario.Mensagemerro
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbcolaboradores set nrsequsuario = " & xusuario.Nrseq & " where nrseq =  " & _nrseq)
            _mensagemerro = "Usuário criado com sucesso !"
            Return True
        Catch exnovouser As Exception
            _mensagemerro = exnovouser.Message
            Return False
        End Try

    End Function

    Public Function Salvar() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            ' _ativo = Ativo

            If _email <> "" AndAlso Not _email.Contains("@") Then
                _mensagemerro = "Informe um e-mail válido !"
                Return False
            End If


            If _gravarusuario AndAlso _nrsequsuario = 0 Then
                Dim xusuario As New clsusuarios(_usercad)
                xusuario.Nomecolaborador = _nome
                xusuario.Usuario = _matricula.Trim ' encontrachr(_nome.Trim, " ", 1) & " " & encontrachr(_nome.Trim, " ", 0)
                xusuario.Senha = _matricula.Trim
                xusuario.Email = _matricula.Trim & "@sistemasceva.com.br"
                xusuario.Permissao = _permissaopadrao
                xusuario.salvarusuario()
                If xusuario.Nrseq <> 0 Then
                    _nrsequsuario = xusuario.Nrseq
                End If
            End If

            If _nrsequsuario <> 0 Then
                Dim wcdatacadreal As Date = data()
                Dim xuser As New clsusuarios(_usercad)
                xuser.Nrseq = _nrsequsuario
                If xuser.procurausuario() Then
                    If IsDate(xuser.Dtcad) Then
                        wcdatacadreal = xuser.Dtcad
                    End If
                End If

                tb1 = tab1.conectar("select * from tbcolaboradores where nrsequsuario = " & _nrsequsuario)

                If tb1.Rows.Count = 0 Then
                    tb1 = tab1.IncluirAlterarDados("insert into tbcolaboradores (nrseqctrl, ativo, dtcad, usercad) values ('" & wcnrseqctrl & "', true, '" & formatadatamysql(wcdatacadreal) & "', '" & _usercad & "')")
                    tb1 = tab1.conectar("select * from tbcolaboradores where nrseqctrl = '" & wcnrseqctrl & "'")
                    _nrseq = tb1.Rows(0)("nrseq").ToString
                End If
                If Validargestor Then
                    Dim tbtemp As New Data.DataTable
                    Dim tabtemp As New clsBanco
                    tbtemp = tabtemp.conectar("select * from tbcolaboradores where matricula = '" & _matricula & "' and ativo = true")
                    If tbtemp.Rows.Count > 0 Then
                        _nome = tbtemp.Rows(0)("nome").ToString
                    End If
                    _gestor = True
                Else
                    _gestor = logico(tb1.Rows(0)("gestor").ToString)
                End If

            End If

            If _nrseqcargoatual = 0 AndAlso _cargoatual <> "" Then

                tbx = tabx.conectar("select * from tbcargos where descricao = '" & _cargoatual & "' and codigocargo = '" & _codigocargoatual & "' and ativo = true ")
                If tbx.Rows.Count = 0 Then
                    Dim xwnrseqctrl As String = gerarnrseqcontrole()
                    tbx = tabx.IncluirAlterarDados("insert into tbcargos (ativo, usercad, dtcad, descricao, codigocargo, nrseqctrl) values (true, '" & _usercad & "', '" & hoje() & "', '" & _cargoatual & "','" & _codigocargoatual & "', '" & xwnrseqctrl & "')")
                    tbx = tabx.conectar("select nrseq from tbcargos where nrseqctrl = '" & xwnrseqctrl & "'")
                    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
                Else
                    _nrseqcargoatual = tbx.Rows(0)("nrseq").ToString
                End If
            End If
            If _nrseqbuatual = 0 AndAlso _buatual <> "" Then

                tbx = tabx.conectar("select * from tbbus where descricao = '" & _buatual & "' and codigobu = '" & _codigobuatual & "' and ativo = true ")
                If tbx.Rows.Count = 0 Then
                    Dim xwnrseqctrl As String = gerarnrseqcontrole()
                    tbx = tabx.IncluirAlterarDados("insert into tbbus (ativo, usercad, dtcad, descricao, codigobu, nrseqctrl) values (true, '" & _usercad & "', '" & hoje() & "', '" & _buatual & "','" & _codigobuatual & "', '" & xwnrseqctrl & "')")
                    tbx = tabx.conectar("select nrseq from tbbus where nrseqctrl = '" & xwnrseqctrl & "'")
                    _nrseqbuatual = tbx.Rows(0)("nrseq").ToString
                Else
                    _nrseqbuatual = tbx.Rows(0)("nrseq").ToString
                End If

            End If
            If _nrseqempresaatual = 0 AndAlso _empresaatual <> "" Then

                tbx = tabx.conectar("select * from tbempresas where empresa = '" & _empresaatual & "'  and ativo = true ")
                If tbx.Rows.Count = 0 Then
                    Dim xwnrseqctrl As String = gerarnrseqcontrole()
                    tbx = tabx.IncluirAlterarDados("insert into tbempresas (ativo, usercad, dtcad, empresa, nrseqctrl) values (true, '" & _usercad & "', '" & hoje() & "', '" & _empresaatual & "', '" & xwnrseqctrl & "')")
                    tbx = tabx.conectar("select nrseq from tbempresas where nrseqctrl = '" & xwnrseqctrl & "'")
                    _nrseqempresaatual = tbx.Rows(0)("nrseq").ToString
                Else
                    _nrseqempresaatual = tbx.Rows(0)("nrseq").ToString
                End If

            End If

            Dim xsql As String = "Update tbcolaboradores Set ativo = true, gestor = " & _gestor & ", horista = " & _Horista

            If _nome <> "" Then
                xsql &= " , nome = '" & _nome & "'"

            End If
            If _nome <> "" Then
                xsql &= " , endereco = '" & _endereco & "'"

            End If
            If _numero <> "" Then
                xsql &= " , numero = '" & _numero & "'"

            End If
            If _bairro <> "" Then
                xsql &= " , bairro = '" & _bairro & "'"

            End If
            If _cidade <> "" Then
                xsql &= " , cidade = '" & _cidade & "'"

            End If
            If _email <> "" Then
                xsql &= " , email = '" & _email & "'"

            End If
            If _cep <> "" Then
                xsql &= " , cep = '" & _cep & "'"

            End If
            If _uf <> "" Then
                xsql &= " , uf = '" & _uf & "'"

            End If
            If _telefone <> "" Then
                xsql &= " , telefone = '" & _telefone & "'"

            End If

            If _celular <> "" Then
                xsql &= " , celular = '" & _celular & "'"

            End If
            If _status <> "" Then
                xsql &= " , status = '" & _status & "'"

            End If
            If _matricula <> "" Then
                xsql &= " , matricula = '" & _matricula & "'"
            End If
            If _nrseqpermisssao <> 0 Then
                xsql &= " , nrseqpermissao = " & _nrseqpermisssao
            End If
            If _nrsequsuario <> 0 Then
                xsql &= " , nrsequsuario = " & _nrsequsuario
            End If
            If IsDate(_dtadmissaoatual) Then
                xsql &= " , dtadmissao = '" & formatadatamysql(_dtadmissaoatual) & "'"
            End If
            If _complemento <> "" Then
                xsql &= " , complemento = '" & _complemento & "'"
            End If
            If _nrseqgestor <> 0 Then
                xsql &= " , nrseqgestor = " & _nrseqgestor
            End If
            If _nrseqcargoatual <> 0 Then
                xsql &= " , nrseqcargoatual =  " & _nrseqcargoatual
            End If
            If _nrseqbuatual <> 0 Then
                xsql &= " , nrseqbuatual =  " & _nrseqbuatual
            End If
            If _desligado <> "" Then
                xsql &= " , desligado =  '" & _desligado & "'"
            End If
            If _contrato <> "" Then
                xsql &= " , contrato =  '" & _contrato & "'"
            End If
            If _nrseqempresaatual <> 0 Then
                xsql &= " , nrseqempresaatual =  " & _nrseqempresaatual
            End If
            xsql &= "  where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)

            gravalog("Alterou os dados do colaborador " & _nome, "Ação")
            _mensagemerro = "Salvo com suscesso !"
            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function
    Public Function excluir() As Boolean
        Try
            If Not carregar() Then
                Return False
            End If
            If _ativo Then
                tb1 = tab1.IncluirAlterarDados("Update tbcolaboradores ativo = false, dtexclui =  '" & hoje() & "', userexclui = '" & _usercad & "' where nrseqctrl = '" & _nrseqctrl & "' ")
                gravalog("Excluiu o colaborador " & _nome, "Ação")
            Else
                tb1 = tab1.IncluirAlterarDados("Update tbcolaboradores ativo = true where nrseqctrl = '" & _nrseqctrl & "' ")
                gravalog("Reativou o colaborador " & _nome, "Ação")
            End If
            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function
    Public Function carregar() As Boolean
        Try

            Dim xsql As String = "select * from vwcolaboradores where 1=1 "
            Select Case _procurarpor.ToLower
                Case Is = "email"
                    xsql &= " and email = '" & _email & "'"
                Case Is = "matricula"
                    If _matricula = "" Then
                        Return False
                    End If
                    xsql &= " and matricula = '" & _matricula & "'"
                Case Is = "nrsequsuario"
                    xsql &= " and nrsequsuario = " & _nrsequsuario
                Case Is = "nrseq"
                    If _nrseq = 0 Then
                        Return False
                    End If
                    xsql &= " and nrseq = " & _nrseq
            End Select
            tb1 = tab1.conectar(xsql)
            _count = tb1.Rows.Count
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Colaborador não existe !"
                Return False
            End If

            With tb1
                _nrseq = .Rows(0)("nrseq").ToString
                _cargoatual = .Rows(0)("descricaocargo").ToString
                _buatual = .Rows(0)("descricaobu").ToString
                _empresaatual = .Rows(0)("empresa").ToString
                _dtadmissaoatual = tratadata(.Rows(0)("dtadmissao").ToString, True)
                _dtdemissaoatual = tratadata(.Rows(0)("dtdemissao").ToString, True)

                Dim xemp As New clscolaboradores_empresas(_usercad)
                xemp.Bu = .Rows(0)("descricaobu").ToString
                xemp.Cargo = .Rows(0)("descricaocargo").ToString
                xemp.Empresa = .Rows(0)("empresa").ToString
                Listaempresas.Add(xemp)

                Dim xdatas As New clscolaboradores_movimento(_usercad)
                xdatas.Dtadmissao = tratadata(.Rows(0)("dtadmissao").ToString, True)
                xdatas.Dtdemissao = tratadata(.Rows(0)("dtdemissao").ToString, True)
                Listamovimentos.Add(xdatas)

                _matricula = .Rows(0)("matricula").ToString
                _nome = .Rows(0)("nome").ToString
                _ativo = .Rows(0)("ativo").ToString

                _celular = .Rows(0)("celular").ToString
                _telefone = .Rows(0)("telefone").ToString
                _uf = .Rows(0)("uf").ToString
                _gestor = logico(.Rows(0)("gestor").ToString)
                _usercad = .Rows(0)("usercad").ToString
                Usuario = .Rows(0)("usuario").ToString
                _bairro = .Rows(0)("bairro").ToString
                _cep = .Rows(0)("cep").ToString
                _cidade = .Rows(0)("cidade").ToString
                _complemento = .Rows(0)("complemento").ToString

                _email = .Rows(0)("email").ToString
                Endereco = .Rows(0)("endereco").ToString
                _nome = .Rows(0)("nome").ToString
                _Horista = logico(.Rows(0)("horista").ToString)

                Nomegestor = .Rows(0)("nomegestor").ToString

                _nrseqbuatual = tratanumeros(.Rows(0)("nrseqbuatual").ToString)
                _nrseqcargoatual = tratanumeros(.Rows(0)("nrseqcargoatual").ToString)
                _contrato = .Rows(0)("contrato").ToString
                _desligado = .Rows(0)("desligado").ToString
                _codigobuatual = .Rows(0)("codigobuatual").ToString
                _codigocargoatual = .Rows(0)("codigocargoatual").ToString


                If _nrseqcargoatual <> 0 Then
                    xsql &= " , nrseqcargoatual =  " & _nrseqcargoatual
                End If
                If _nrseqbuatual <> 0 Then
                    xsql &= " , nrseqbuatual =  " & _nrseqbuatual
                End If
                If _desligado <> "" Then
                    xsql &= " , desligado =  '" & _desligado & "'"
                End If
                If _contrato <> "" Then
                    xsql &= " , contrato =  '" & _contrato & "'"
                End If

            End With

            gravalog("Procurou o colaborador " & _nome, "Ação")

            _pads = tabx.conectar("select * from vwpad where nrseqcolaborador = " & _nrseq)

            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function
End Class
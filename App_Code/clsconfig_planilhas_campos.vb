﻿Imports Microsoft.VisualBasic

Public Class clsconfig_planilhas_campos

    'Desenvolvedor: Claudio Smart
    'Data inicial :  03/09/2019

    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nrseqconfig As Integer = 0
    Dim _campo As String = ""
    Dim _planilha As String = ""
    Dim _ativo As Boolean

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqconfig As Integer
        Get
            Return _nrseqconfig
        End Get
        Set(value As Integer)
            _nrseqconfig = value
        End Set
    End Property

    Public Property Campo As String
        Get
            Return _campo
        End Get
        Set(value As String)
            _campo = value
        End Set
    End Property

    Public Property Planilha As String
        Get
            Return _planilha
        End Get
        Set(value As String)
            _planilha = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property
End Class

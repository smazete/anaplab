﻿Imports Microsoft.VisualBasic
Imports clsSmart

Public Class clsacoesdth


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _nrseqacoes As String
    Dim _matricula As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo As Boolean

    Dim _participantes As String
    Dim _cancelados As String
    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqacoes As String
        Get
            Return _nrseqacoes
        End Get
        Set(value As String)
            _nrseqacoes = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Participantes As String
        Get
            Return _participantes
        End Get
        Set(value As String)
            _participantes = value
        End Set
    End Property

    Public Property Cancelados As String
        Get
            Return _cancelados
        End Get
        Set(value As String)
            _cancelados = value
        End Set
    End Property
End Class

Partial Public Class clsacoesdth

    Public Function procurar() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbacoes_detalhes where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Nrseqacoes = tb1.Rows(0)("nrseqacoes").ToString
        Matricula = tb1.Rows(0)("matricula").ToString
        Dtcad = FormatDateTime(valordata(tb1.Rows(0)("dtcad").ToString), DateFormat.ShortDate)
        Usercad = tb1.Rows(0)("usercad").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function valida() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbacoes_detalhes where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function adicionar() As Boolean
        If Matricula = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbacoes_detalhes where matricula = '" & Matricula & "' and nrseqacoes = '" & Nrseqacoes & "'")
        If tb1.Rows.Count = 0 Then
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbacoes_detalhes (nrseqctrl, Nrseqacoes, Matricula, Dtcad, Usercad, Ativo) values ('" & wcnrseqctrl & "','" & Nrseqacoes & "', '" & _matricula & "','" & hoje() & "','" & _usercad & "', true)")
            Return True
        End If
        _mensagemerro = "O Cliente já esta vinculado a esta Ação"

        Return False
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function canceladoscount() As Boolean
        If Nrseqacoes = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbacoes_detalhes where ativo = '0' and Nrseqacoes = " & Nrseqacoes)
        If tb1.Rows.Count = 0 Then
            Cancelados = "0"
            Return False
        End If
        Cancelados = tb1.Rows.Count
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function participantescount() As Boolean
        If Nrseqacoes = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbacoes_detalhes where  ativo = '1'  and nrseqacoes = " & Nrseqacoes)
        If tb1.Rows.Count = 0 Then
            _participantes = ""
            Return False
        End If
        _participantes = tb1.Rows.Count
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function



    Public Function procurarporacoes() As Boolean
        If _nrseqacoes = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbacoes_detalhes where nrseqacoes = " & _nrseqacoes)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _nrseqacoes = tb1.Rows(0)("nrseqacoes").ToString
        _matricula = tb1.Rows(0)("matricula").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _usercad = tb1.Rows(0)("usercad").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbacoes_detalhes Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _nrseqacoes <> "" Then
                xsql &= ",nrseqacoes = '" & tratatexto(_nrseqacoes) & "'"
            End If
            If _matricula <> "" Then
                xsql &= ",matricula = '" & tratatexto(_matricula) & "'"
            End If
            If _dtcad <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _ativo <> 0 Then
                xsql &= ",ativo = " & moeda(_ativo)
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try

            If Not procurar() Then
                Return False
            End If
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbacoes_detalhes (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbacoes_detalhes where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbacoes_detalhes set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function remover() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If _matricula = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Nrseqacoes = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbacoes_detalhes set ativo = false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "' where nrseq = '" & _nrseq & "' and nrseqacoes = '" & _nrseqacoes & "' and matricula = '" & _matricula & "'")
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function



    Public Function ativar() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If _matricula = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Nrseqacoes = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbacoes_detalhes set ativo = true, dtreativado = '" & hoje() & "', userreativa = '" & _usercad & "' where nrseq = '" & _nrseq & "' and nrseqacoes = '" & _nrseqacoes & "' and matricula = '" & _matricula & "'")
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class



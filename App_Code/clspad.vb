﻿Imports SelectPdf
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System.Web
Imports System
Imports System.Collections.Generic
Imports System.IO.Compression

Enum procurarpor
    nrseq
    ano_colaborador
End Enum
Public Class clspad
    ' *******************************************
    ' Desenvolvido por Claudio Smart
    ' Data: 23/08/2019
    ' Atualizado em: 26/08/2019
    ' Atualizado em: 06/09/2019
    ' Atualizado em: 18/09/2019
    ' Atualizado em: 09/11/2019
    ' Atualizado em: 20/11/2019
    ' Atualizado em: 13/01/2020
    ' Atualizado em: 24/01/2020
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim tbobj As New Data.DataTable
    Dim tabobj As New clsBanco

#Region "Listas"
    Private _ListaBaixarPADs As New List(Of clspad)
    Private _relatoriopad As New List(Of clspad)
    Private _situacoes As New List(Of String)

    Private _cursos As New List(Of clspad_cursos)
    Private _avaliacoes As New List(Of clspad_avaliacoes)
    Private _competencias As New List(Of clspad_competencias)

#End Region
#Region "Importacoes"
    Dim _descricao As String
    Dim _PlanoDesenvolvimento As String
    Dim _objetivos As String
    Dim _data As Date
#End Region
#Region "solicitações edições"
    Dim _etapa As New List(Of String)

#End Region

    Dim _filtroespecial As String = ""
    Dim _naoprocurar As Boolean = False
    Dim _arquivopdf As String = ""
    Dim _gradecursos As New Data.DataTable
    Dim _gradecursosanteriores As New Data.DataTable
    Dim _nrseqresultado As Integer = 0
    Dim _descricaoresultado As String = ""
    Dim _pontosresultado As Decimal = 0
    Dim _obs As String = ""
    Dim _nrseq As Integer = 0
    Dim _ativo As Boolean
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _anobase As Integer = 0
    Dim _nrseqgestor As Integer = 0
    Dim _nrseqcolaborador As Integer = 0
    Dim _mensagemerro As String
    Dim _nomegestor As String
    Dim _nomecolaborador As String = ""
    Dim _matriculacolaborador As String = ""

    Dim _matriculagestor As String = ""
    Dim _procurarpor As String
    Dim _descricaobu As String = ""
    Dim _descricaocargo As String = ""
    Dim _empresa As String = ""
    Dim _contrato As String = ""
    Dim _status As String = ""
    Dim _desligado As String = ""
    Dim _totalavaliacoes1 As Integer = 0
    Dim _totalavaliacoes2 As Integer = 0
    Dim _totalavaliacoes3 As Integer = 0
    Dim _totalcompetencias As Integer = 0
    Dim _totalcursos As Integer = 0
    Dim _dtadmissao As Date
    Dim _cidade As String
    Dim _arquivolog As System.IO.StreamWriter


    Public Sub New()
        versessao()
        Usercad = HttpContext.Current.Session("usuario")
        _dtcad = clsSmart.data()
        _situacoes.Clear()
    End Sub
    Public Sub New(xusuario As String)
        Usercad = xusuario
        _dtcad = clsSmart.data()
        _situacoes.Clear()
    End Sub
    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Anobase As Integer
        Get
            Return _anobase
        End Get
        Set(value As Integer)
            _anobase = value
        End Set
    End Property

    Public Property Nrseqgestor As Integer
        Get
            Return _nrseqgestor
        End Get
        Set(value As Integer)
            _nrseqgestor = value
        End Set
    End Property

    Public Property Nrseqcolaborador As Integer
        Get
            Return _nrseqcolaborador
        End Get
        Set(value As Integer)
            _nrseqcolaborador = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Cursos As List(Of clspad_cursos)
        Get
            Return _cursos
        End Get
        Set(value As List(Of clspad_cursos))
            _cursos = value
        End Set
    End Property

    Public Property Avaliacoes As List(Of clspad_avaliacoes)
        Get
            Return _avaliacoes
        End Get
        Set(value As List(Of clspad_avaliacoes))
            _avaliacoes = value
        End Set
    End Property

    Public Property Competencias As List(Of clspad_competencias)
        Get
            Return _competencias
        End Get
        Set(value As List(Of clspad_competencias))
            _competencias = value
        End Set
    End Property

    Public Property Nomegestor As String
        Get
            Return _nomegestor
        End Get
        Set(value As String)
            _nomegestor = value
        End Set
    End Property

    Public Property Nomecolaborador As String
        Get
            Return _nomecolaborador
        End Get
        Set(value As String)
            _nomecolaborador = value
            If Not _naoprocurar AndAlso value <> "" Then
                tbx = tabx.conectar("select * from tbcolaboradores where nome = '" & value & "' and ativo = true ")
                If tbx.Rows.Count > 0 Then
                    _nrseqcolaborador = tbx.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Matriculacolaborador As String
        Get
            Return _matriculacolaborador
        End Get
        Set(value As String)
            _matriculacolaborador = value

            If Not _naoprocurar AndAlso _matriculacolaborador <> "" Then
                tbx = tabx.conectar("select * from tbcolaboradores where matricula = '" & _matriculacolaborador & "'")
                If tbx.Rows.Count <> 0 Then
                    _nomecolaborador = tbx.Rows(0)("nome").ToString
                    _nrseqcolaborador = tbx.Rows(0)("nrseq").ToString
                End If
            End If

        End Set
    End Property

    Public Property ProcurarPADpor As String
        Get
            Return _procurarpor
        End Get
        Set(value As String)
            _procurarpor = value
        End Set
    End Property

    Public Property Obs As String
        Get
            Return _obs
        End Get
        Set(value As String)
            _obs = value
        End Set
    End Property

    Public Property Pontosresultado As Decimal
        Get
            Return _pontosresultado
        End Get
        Set(value As Decimal)
            _pontosresultado = value
        End Set
    End Property

    Public Property Descricaoresultado As String
        Get
            Return _descricaoresultado
        End Get
        Set(value As String)
            _descricaoresultado = value
            If Not _naoprocurar AndAlso _descricaoresultado <> "" Then
                Nrseqresultado = 0
                tbx = tabx.conectar("select * from tbresultados_finais where ativo = true and descricao = '" & _descricaoresultado & "'")
                If tbx.Rows.Count > 0 Then
                    Nrseqresultado = tbx.Rows(0)("nrseq").ToString

                End If
            End If
        End Set
    End Property

    Public Property Nrseqresultado As Integer
        Get
            Return _nrseqresultado
        End Get
        Set(value As Integer)
            _nrseqresultado = value
        End Set
    End Property

    Public Property Gradecursos As DataTable
        Get
            Return _gradecursos
        End Get
        Set(value As DataTable)
            _gradecursos = value
        End Set
    End Property
    Public Property Gradecursosanteriores As DataTable
        Get
            Return _gradecursosanteriores
        End Get
        Set(value As DataTable)
            _gradecursosanteriores = value
        End Set
    End Property

    Public Property Arquivopdf As String
        Get
            Return _arquivopdf
        End Get
        Set(value As String)
            _arquivopdf = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property PlanoDesenvolvimento As String
        Get
            Return _PlanoDesenvolvimento
        End Get
        Set(value As String)
            _PlanoDesenvolvimento = value
        End Set
    End Property

    Public Property Objetivos As String
        Get
            Return _objetivos
        End Get
        Set(value As String)
            _objetivos = value
        End Set
    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Property Descricaobu As String
        Get
            Return _descricaobu
        End Get
        Set(value As String)
            _descricaobu = value
        End Set
    End Property

    Public Property Descricaocargo As String
        Get
            Return _descricaocargo
        End Get
        Set(value As String)
            _descricaocargo = value
        End Set
    End Property

    Public Property Empresa As String
        Get
            Return _empresa
        End Get
        Set(value As String)
            _empresa = value
        End Set
    End Property

    Public Property Contrato As String
        Get
            Return _contrato
        End Get
        Set(value As String)
            _contrato = value
        End Set
    End Property

    Public Property Status As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property

    Public Property Desligado As String
        Get
            Return _desligado
        End Get
        Set(value As String)
            _desligado = value
        End Set
    End Property

    Public Property ListaBaixarPADs As List(Of clspad)
        Get
            Return _ListaBaixarPADs
        End Get
        Set(value As List(Of clspad))
            _ListaBaixarPADs = value
        End Set
    End Property

    Public Property Etapa As List(Of String)
        Get
            Return _etapa
        End Get
        Set(value As List(Of String))
            _etapa = value
        End Set
    End Property

    Public Property Relatoriopad As List(Of clspad)
        Get
            Return _relatoriopad
        End Get
        Set(value As List(Of clspad))
            _relatoriopad = value
        End Set
    End Property

    Public Property Dtadmissao As Date
        Get
            Return _dtadmissao
        End Get
        Set(value As Date)
            _dtadmissao = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Matriculagestor As String
        Get
            Return _matriculagestor
        End Get
        Set(value As String)
            _matriculagestor = value
        End Set
    End Property

    Public Property Totalavaliacoes1 As Integer
        Get
            Return _totalavaliacoes1
        End Get
        Set(value As Integer)
            _totalavaliacoes1 = value
        End Set
    End Property

    Public Property Totalavaliacoes2 As Integer
        Get
            Return _totalavaliacoes2
        End Get
        Set(value As Integer)
            _totalavaliacoes2 = value
        End Set
    End Property

    Public Property Totalavaliacoes3 As Integer
        Get
            Return _totalavaliacoes3
        End Get
        Set(value As Integer)
            _totalavaliacoes3 = value
        End Set
    End Property

    Public Property Totalcompetencias As Integer
        Get
            Return _totalcompetencias
        End Get
        Set(value As Integer)
            _totalcompetencias = value
        End Set
    End Property

    Public Property Totalcursos As Integer
        Get
            Return _totalcursos
        End Get
        Set(value As Integer)
            _totalcursos = value
        End Set
    End Property

    Public Property Naoprocurar As Boolean
        Get
            Return _naoprocurar
        End Get
        Set(value As Boolean)
            _naoprocurar = value
        End Set
    End Property

    Public Property Situacoes As List(Of String)
        Get
            Return _situacoes
        End Get
        Set(value As List(Of String))
            _situacoes = value
        End Set
    End Property

    Public Property Filtroespecial As String
        Get
            Return _filtroespecial
        End Get
        Set(value As String)
            _filtroespecial = value
        End Set
    End Property

    Public Property Arquivolog As System.IO.StreamWriter
        Get
            Return _arquivolog
        End Get
        Set(value As System.IO.StreamWriter)
            _arquivolog = value
        End Set
    End Property
End Class
Partial Public Class clspad

    Public Function novo() As Boolean
        Try

            If _nrseqcolaborador = 0 Then
                _mensagemerro = "Selecione um colaborador válido !"
                Return False
            End If
            Dim xcolaborador As New clscolaboradores
            xcolaborador.Nrseq = _nrseqcolaborador
            xcolaborador.Procurarpor = "nrseq"
            If Not xcolaborador.carregar Then
                _mensagemerro = "O colaborador não existe !"
                Return False
            End If
            If Not xcolaborador.Horista Then
                _mensagemerro = "O colaborador não é horista! Impossível criar o PAD dele."
                Return False
            End If
            _nrseqgestor = buscarsessoes("nrseqcolaborador")
            Dim _wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbpad (nrseqctrl, dtcad, usercad, ativo, nrseqcolaborador, nrseqgestor, anobase) values ('" & _wcnrseqctrl & "', '" & hoje() & "', '" & _usercad & "', true, " & _nrseqcolaborador & ", " & _nrseqgestor & ", " & _anobase & ")")
            tb1 = tab1.conectar("Select * from tbpad where nrseqctrl = '" & _wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function procurar() As Boolean
        Try
            Dim qtdtestes As Integer = 1
retestar:
            If _procurarpor.ToLower <> "nrseq" AndAlso _anobase = 0 Then
                _mensagemerro = "Selecione um ano base válido !"
                Return False
            End If

            Dim xsql As String = "select * from vwpad where 1=1 and ativo = true "
            Select Case _procurarpor.ToLower
                Case Is = "nrseq"
                    xsql &= " and nrseq = " & _nrseq
                Case Is = "colaborador"
                    xsql &= " and nomecolaborador = '" & _nomecolaborador & "' and anobase = " & _anobase
                Case Is = "matricula"
                    xsql &= " and matriculacolaborador = '" & _matriculacolaborador & "' and anobase = " & _anobase
            End Select



            tb1 = tab1.conectar(xsql)
            If tb1.Rows.Count = 0 Then
                If Not novo() Then

                    Return False
                End If
                If qtdtestes > 3 Then
                    _mensagemerro = "Impossível criar PAD"
                    Return False
                End If
                qtdtestes += 1
                GoTo retestar
            End If

            With tb1

                If .Rows(0)("pontosresultado").ToString <> "" Then
                    _pontosresultado = .Rows(0)("pontosresultado").ToString
                    Descricaoresultado = .Rows(0)("descricaoresultado").ToString

                End If
                _matriculacolaborador = .Rows(0)("matriculacolaborador").ToString
                _nrseq = .Rows(0)("nrseq").ToString
                _nrseqcolaborador = .Rows(0)("nrseqcolaborador").ToString
                _nrseqgestor = .Rows(0)("nrseqgestor").ToString
                Nomecolaborador = .Rows(0)("nomecolaborador").ToString
                Nomegestor = .Rows(0)("nomegestor").ToString
                _usercad = .Rows(0)("usercad").ToString
                _ativo = logico(.Rows(0)("ativo").ToString)
                _dtcad = .Rows(0)("dtcad").ToString
                _anobase = .Rows(0)("anobase").ToString
                _contrato = .Rows(0)("contrato").ToString
                _empresa = .Rows(0)("empresa").ToString
                _descricaobu = .Rows(0)("descricaobu").ToString
                _descricaocargo = .Rows(0)("descricaocargo").ToString
                _status = .Rows(0)("status").ToString
                _desligado = .Rows(0)("desligado").ToString
                _dtadmissao = tratadata(.Rows(0)("dtadmissao").ToString, gravardata.recuperar)
                _cidade = .Rows(0)("cidade").ToString
                _matriculagestor = .Rows(0)("Matriculagestor").ToString
            End With
            tb1 = tab1.conectar("select * from tbsolicitacoes where nrseqpad =  " & _nrseq & " and liberado = true and (feito = false or feito is null)")
            If tb1.Rows.Count > 0 Then
                For x As Integer = 0 To tb1.Rows.Count - 1
                    _etapa.Add(tb1.Rows(x)("etapa").ToString)
                Next
            End If
            tb1 = tab1.conectar("select * from vwpad_cursos where nrseqpad = " & _nrseq)
            _cursos.Clear()
            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim xcurso As New clspad_cursos
                With tb1
                    xcurso.Anobase = .Rows(x)("anobase").ToString
                    xcurso.Nrseq = .Rows(x)("nrseq").ToString
                    'xcurso.Nrseqcurso = .Rows(0)("nrseqcurso").ToString
                    xcurso.Ativo = logico(.Rows(x)("ativo").ToString)
                    xcurso.Cargahoraria = .Rows(x)("cargahoraria").ToString
                    xcurso.Cursoativo = logico(.Rows(x)("cursoativo").ToString)
                    xcurso.Descricao = .Rows(x)("descricao").ToString
                    xcurso.Dtcad = .Rows(x)("dtcad").ToString
                    xcurso.Concluido = IIf(.Rows(x)("concluido").ToString = "Sim", True, False)
                    If logico(.Rows(x)("concluido").ToString) Then
                        xcurso.Dtconcluido = .Rows(x)("dtconcluido").ToString
                    End If
                End With
                _cursos.Add(xcurso)
            Next


novoteste:


            For x As Integer = 1 To 3


                tbobj = tabobj.conectar("insert into tbpad_avaliacoes (nrseqobjetivo, nrseqpad, revisao, ativo, dtcad, usercad) SELECT nrseq, " & _nrseq & ", " & x & ", true, '" & hoje() & "', '" & _usercad & "' FROM tbobjetivos where ativo = true and tipo = 'Objetivo' and not nrseq in (select nrseqobjetivo from vwpad_avaliacoes where nrseqpad = " & _nrseq & " and revisao = " & x & ");")

            Next


            tb1 = tab1.conectar("select * from vwpad_avaliacoes where nrseqpad = " & _nrseq)
            _avaliacoes.Clear()
            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim xava As New clspad_avaliacoes
                With tb1
                    xava.Ativo = logico(.Rows(x)("avaliacaoativa").ToString)
                    xava.Nrseq = .Rows(x)("nrseq").ToString
                    xava.Nrseqpad = _nrseq
                    xava.Usercad = .Rows(x)("usercad").ToString
                    xava.Revisao = .Rows(x)("revisao").ToString
                    xava.Descricaoobjetivo = .Rows(x)("descricaoobjetivo").ToString
                    xava.Descricaoresultado = .Rows(x)("descricaoresultado").ToString
                    xava.Descricao = .Rows(x)("descricao").ToString
                    xava.Justificativa = .Rows(x)("justificativa").ToString
                End With
                _avaliacoes.Add(xava)
            Next


            tbobj = tabobj.conectar("insert into tbpad_competencias (nrseqobjetivo, nrseqpad,  ativo, dtcad, usercad) SELECT nrseq, " & _nrseq & ", true, '" & hoje() & "', '" & _usercad & "' FROM  tbobjetivos where ativo = true and tipo = 'Competência' and not nrseq in (select nrseqobjetivo from vwpad_competencias where nrseqpad = " & _nrseq & ")")

            _competencias.Clear()

            tb1 = tab1.conectar("select * from vwpad_competencias where nrseqpad = " & _nrseq)
            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim xcomp As New clspad_competencias
                With tb1
                    xcomp.Ativo = logico(.Rows(x)("competenciaativa").ToString)
                    xcomp.Nrseq = .Rows(x)("nrseq").ToString
                    xcomp.Nrseqpad = _nrseq
                    xcomp.Usercad = .Rows(x)("usercad").ToString

                    xcomp.Descricaoobjetivo = .Rows(x)("descricaoobjetivo").ToString
                    xcomp.Descricaoresultado = .Rows(x)("descricaoresultado").ToString
                    xcomp.Justificativa = .Rows(x)("justificativa").ToString

                End With
                _competencias.Add(xcomp)
            Next
            contaravaliacoes()
            carregarcursos()


            Return True
        Catch exproc As Exception
            _mensagemerro = exproc.Message
            Return False
        End Try
    End Function
    Public Function contaravaliacoes() As Boolean

        Try
            If _nrseq = 0 Then
                Return False
            End If

            tb1 = tab1.conectar("select * from vwpad_cursos where nrseqpad = " & _nrseq)
            _cursos.Clear()
            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim xcurso As New clspad_cursos
                With tb1
                    xcurso.Anobase = .Rows(x)("anobase").ToString
                    xcurso.Nrseq = .Rows(x)("nrseq").ToString
                    'xcurso.Nrseqcurso = .Rows(0)("nrseqcurso").ToString
                    xcurso.Ativo = logico(.Rows(x)("ativo").ToString)
                    xcurso.Cargahoraria = .Rows(x)("cargahoraria").ToString
                    xcurso.Cursoativo = logico(.Rows(x)("cursoativo").ToString)
                    xcurso.Descricao = .Rows(x)("descricao").ToString
                    xcurso.Dtcad = .Rows(x)("dtcad").ToString
                    xcurso.Concluido = IIf(.Rows(x)("concluido").ToString = "Sim", True, False)
                    If logico(.Rows(x)("concluido").ToString) Then
                        xcurso.Dtconcluido = .Rows(x)("dtconcluido").ToString
                    End If
                End With
                _cursos.Add(xcurso)
            Next

            tb1 = tab1.conectar("select * from vwpad_avaliacoes where nrseqpad = " & _nrseq)
            _avaliacoes.Clear()
            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim xava As New clspad_avaliacoes
                With tb1
                    xava.Ativo = logico(.Rows(x)("avaliacaoativa").ToString)
                    xava.Nrseq = .Rows(x)("nrseq").ToString
                    xava.Nrseqpad = _nrseq
                    xava.Usercad = .Rows(x)("usercad").ToString
                    xava.Revisao = .Rows(x)("revisao").ToString
                    xava.Descricaoobjetivo = .Rows(x)("descricaoobjetivo").ToString
                    xava.Descricaoresultado = .Rows(x)("descricaoresultado").ToString
                    xava.Descricao = .Rows(x)("descricao").ToString
                    xava.Justificativa = .Rows(x)("justificativa").ToString
                End With
                _avaliacoes.Add(xava)
            Next

            _competencias.Clear()

            tb1 = tab1.conectar("select * from vwpad_competencias where nrseqpad = " & _nrseq)
            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim xcomp As New clspad_competencias
                With tb1
                    xcomp.Ativo = logico(.Rows(x)("competenciaativa").ToString)
                    xcomp.Nrseq = .Rows(x)("nrseq").ToString
                    xcomp.Nrseqpad = _nrseq
                    xcomp.Usercad = .Rows(x)("usercad").ToString

                    xcomp.Descricaoobjetivo = .Rows(x)("descricaoobjetivo").ToString
                    xcomp.Descricaoresultado = .Rows(x)("descricaoresultado").ToString
                    xcomp.Justificativa = .Rows(x)("justificativa").ToString

                End With
                _competencias.Add(xcomp)
            Next


            Dim conta As Integer = 0

            For y As Integer = 1 To 3
                conta = 0
                For x As Integer = 0 To _avaliacoes.Count - 1
                    If _avaliacoes(x).Revisao = y AndAlso _avaliacoes(x).Descricao <> "" Then
                        If y < 3 Then
                            conta += 1
                        Else
                            If _avaliacoes(x).Descricaoresultado <> "" Then
                                conta += 1
                            End If
                        End If

                    End If
                Next
                tb1 = tab1.IncluirAlterarDados("update tbpad set totalavaliacoes" & y & " = " & conta & " where nrseq = " & _nrseq)
            Next
            conta = 0
            For x As Integer = 0 To _cursos.Count - 1
                If _cursos(x).Ativo = True Then
                    conta += 1
                End If
            Next
            tb1 = tab1.IncluirAlterarDados("update tbpad set totalcursos = " & conta & " where nrseq = " & _nrseq)
            conta = 0
            For x As Integer = 0 To _competencias.Count - 1
                If _competencias(x).Ativo = True AndAlso _competencias(x).Descricaoresultado <> "" Then
                    conta += 1
                End If
            Next
            tb1 = tab1.IncluirAlterarDados("update tbpad set totalcompetencias = " & conta & " where nrseq = " & _nrseq)
            Return True
        Catch excontar As Exception
            Return False
        End Try

    End Function
    Public Function salvar() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Selecione uma PAD valida para salvar !"
                Return False
            End If
            If _nrseqcolaborador = 0 Then
                _mensagemerro = "Selecione um colaborador válido !"
                Return False
            End If

            If _anobase = 0 Then
                _mensagemerro = "Selecione um ano base válido !"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbpad set nrseqcolaborador = " & _nrseqcolaborador & ", nrseqgestor = " & _nrseqgestor & ", ativo = true where nrseq = " & _nrseq)

            gravalog("Salvou o PAD do colaborador " & Nomecolaborador & " do ano de " & _anobase, "Ação")
            Return True
        Catch exproc As Exception
            _mensagemerro = exproc.Message
            Return False
        End Try
    End Function
    Public Function adicionaavaliacao() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Selecione um PAD válido !"
                Return False
            End If
            For x As Integer = 0 To _avaliacoes.Count - 1
                tb1 = tab1.IncluirAlterarDados("update tbpad_avaliacoes set descricao = '" & _avaliacoes(x).Descricao & "', nrseqresultado = " & _avaliacoes(x).Nrseqresultado & ", justificativa = '" & _avaliacoes(x).Justificativa & "' where nrseq = " & _avaliacoes(x).Nrseq)
            Next
            contaravaliacoes()
        Catch ex As Exception

        End Try

    End Function
    Public Function adicionacurso(curso As String, adicionais As String) As Boolean
        Try
            Dim wcnrseqcurso As Integer = 0
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, selecione um PAD válido !"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbcursos where descricao = '" & curso & "' and ativo = true ")
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Por favor, selecione um curso válido !"
                Return False
            End If
            wcnrseqcurso = tb1.Rows(0)("nrseq").ToString

            tb1 = tab1.conectar("select * from tbpad_cursos where nrseqcurso = " & wcnrseqcurso & " and ativo = true and nrseqpad = " & _nrseq)
            If tb1.Rows.Count > 0 Then
                _mensagemerro = "Esse curso já está definido para esse PAD ! Selecione outro !"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("insert into tbpad_cursos (nrseqpad, nrseqcurso, ativo, dtcad, usercad, adicionais) values (" & _nrseq & ", " & wcnrseqcurso & ", true, '" & hoje() & "','" & _usercad & "','" & tratatexto(adicionais) & "')")

            carregarcursos()
            Return True
        Catch excurso As Exception
            Return False
        End Try


    End Function

    Public Function excluircurso(curso As String) As Boolean
        Try
            Dim wcnrseqcurso As Integer = 0
            Dim wcnrseqpadcurso As Integer = 0
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, selecione um PAD válido !"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbcursos where descricao = '" & curso & "' and ativo = true ")
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Por favor, selecione um curso válido !"
                Return False
            End If
            wcnrseqcurso = tb1.Rows(0)("nrseq").ToString

            tb1 = tab1.conectar("select * from tbpad_cursos where nrseqcurso = " & wcnrseqcurso & " and ativo = true and nrseqpad = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Esse curso não existe para o PAD ! Selecione outro !"
                Return False
            End If
            wcnrseqpadcurso = tb1.Rows(0)("nrseq").ToString


            tb1 = tab1.IncluirAlterarDados("update tbpad_cursos set ativo = " & IIf(logico(tb1.Rows(0)("ativo").ToString) = "True", "false", "True") & " where nrseq = " & wcnrseqpadcurso)

            carregarcursos()
            Return True
        Catch excurso As Exception
            Return False
        End Try


    End Function

    Public Function alterarstatuscurso(curso As String, concluido As Boolean, xanobase As Integer) As Boolean
        Try
            Dim wcnrseqcurso As Integer = 0
            Dim wcnrseqpadcurso As Integer = 0
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, selecione um PAD válido !"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbcursos where descricao = '" & curso & "' and ativo = true ")
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Por favor, selecione um curso válido !"
                Return False
            End If
            wcnrseqcurso = tb1.Rows(0)("nrseq").ToString

            tb1 = tab1.conectar("select * from vwpad_cursos where nrseqcurso = " & wcnrseqcurso & " and ativo = true and nrseqpad = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Esse curso não existe para o PAD ! Selecione outro !"
                Return False
            End If
            wcnrseqpadcurso = tb1.Rows(0)("nrseq").ToString
            _nrseqcolaborador = tb1.Rows(0)("nrseqcolaborador").ToString
            _anobase = xanobase

            tb1 = tab1.IncluirAlterarDados("update tbpad_cursos set concluido = " & IIf(logico(concluido) = "True", "True", "False") & ", dtconcluido = '" & hoje() & "', userconcluido = '" & _usercad & "' where nrseq = " & wcnrseqpadcurso)

            carregarcursos()
            _mensagemerro = "Situação atualizada comm sucesso !"
            Return True
        Catch excurso As Exception
            _mensagemerro = excurso.Message
            Return False
        End Try


    End Function
    Public Function alterarstatuscurso(nrseqcurso As Integer, xanobase As Integer, concluido As Boolean, hrfeito As String) As Boolean
        Try
            Dim wcnrseqcurso As Integer = 0
            Dim wcnrseqpadcurso As Integer = 0
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, selecione um PAD válido !"
                Return False
            End If

            'tb1 = tab1.conectar("select * from tbcursos where nrseq = " & nrseqcurso & " and ativo = true ")
            'If tb1.Rows.Count = 0 Then
            '    _mensagemerro = "Por favor, selecione um curso válido !"
            '    Return False
            'End If
            'wcnrseqcurso = tb1.Rows(0)("nrseq").ToString

            tb1 = tab1.conectar("select * from vwpad_cursos where nrseq = " & nrseqcurso & " and ativo = true and nrseqpad = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Esse curso não existe para o PAD ! Selecione outro !"
                Return False
            End If
            wcnrseqpadcurso = tb1.Rows(0)("nrseq").ToString
            _nrseqcolaborador = tb1.Rows(0)("nrseqcolaborador").ToString
            _anobase = xanobase

            tb1 = tab1.IncluirAlterarDados("update tbpad_cursos set concluido = " & IIf(logico(concluido) = "True", "True", "False") & ", dtconcluido = '" & hoje() & "', userconcluido = '" & _usercad & "' where nrseq = " & wcnrseqpadcurso)

            carregarcursos()
            _mensagemerro = "Situação atualizada comm sucesso !"
            Return True
        Catch excurso As Exception
            _mensagemerro = excurso.Message
            Return False
        End Try


    End Function
    Public Function salvarresultados() As Boolean

        Try

            For x As Integer = 0 To _competencias.Count - 1
                tb1 = tab1.IncluirAlterarDados("update tbpad_competencias set  nrseqresultado = " & _competencias(x).Nrseqresultado & ", justificativa = '" & _competencias(x).Justificativa & "' where nrseq = " & _competencias(x).Nrseq)
            Next


            tb1 = tab1.IncluirAlterarDados("update tbpad set obs = '" & _obs & "', Pontosresultado = " & moeda(_pontosresultado) & ", nrseqresultado = '" & _nrseqresultado & "' where nrseq = " & _nrseq)

            gravalog("Salvou os resultados do PAD do colaborador " & Nomecolaborador & " do ano de " & _anobase, "Ação")

            Return True
        Catch exsalv As Exception
            _mensagemerro = exsalv.Message
            Return False
        End Try

    End Function

    Public Function carregarcursos() As Boolean
        Try

            _gradecursos = tab1.conectar("select * from vwpad_cursos where nrseqpad = " & _nrseq & " and ativo = true order by descricao")

            _gradecursosanteriores = tab1.conectar("select * from vwpad_cursos where anobase = " & (_anobase - 1) & " and nrseqcolaborador = " & _nrseqcolaborador & " and ativo = true order by descricao")


            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function imprimirPAD(old As Boolean) As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Selecione uma PAD válida para imprimir !"
                Return False
            End If
            ProcurarPADpor = "nrseq"
            _anobase = Year(clsSmart.data)
            If Not procurar() Then
                _mensagemerro = "Selecione uma PAD válida para imprimir !"
                Return False
            End If

            Dim raiz As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath
            Dim r As String = ""
            Dim salvarEm As String = HttpContext.Current.Server.MapPath("~/")

            Dim arquivo As String = "pad" & _nrseq & "_" & DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString & ".pdf"
            salvarEm &= "arquivoslotes\" & arquivo
            Dim FullPath = raiz & "gerarpad.aspx?pad=" & _nrseq

            Dim converter As New HtmlToPdf()
            Dim doc As PdfDocument = converter.ConvertUrl(FullPath)
            Dim pdf As Byte() = doc.Save()
            doc.Close()

            File.WriteAllBytes(salvarEm, pdf)
            Arquivopdf = salvarEm
            gravalog("Imprimiu o formulário PAD do colaborador " & Nomecolaborador & " do ano de " & _anobase, "Ação")
            Return True
        Catch eximprimir As Exception
            _mensagemerro = eximprimir.Message
            Return False
        End Try


    End Function

    Public Function importarpad() As Boolean

        Try
            _nrseq = 0
            If _objetivos.Contains("trabalho") Then _objetivos = "Segurança do trabalho"
            tb1 = tab1.conectar("select * from tbobjetivos where ativo = true and descricao = '" & _objetivos & "'")
            If tb1.Rows.Count = 0 Then
                Return False
            End If
            Dim nrseqobjetivo As Integer = tb1.Rows(0)("nrseq").ToString
            tb1 = tab1.conectar("select * from tbcolaboradores where matricula = '" & _matriculacolaborador & "'")
            If tb1.Rows.Count = 0 Then
                tb1 = tab1.IncluirAlterarDados("insert into tbfaltas (matricula) values ('" & _matriculacolaborador & "')")
                Return False
            End If
            _nrseqgestor = tratanumeros(tb1.Rows(0)("nrseqgestor").ToString)
            _nrseqcolaborador = tb1.Rows(0)("nrseq").ToString
            tb1 = tab1.conectar("select * from tbpad where nrseqcolaborador = " & tb1.Rows(0)("nrseq") & " and anobase = " & _data.Year & " and ativo = true ")
            If tb1.Rows.Count = 0 Then
                tb1 = tab1.IncluirAlterarDados("insert into tbpad (nrseqcolaborador, anobase, ativo, dtcad, nrseqgestor ) values (" & _nrseqcolaborador & ", " & _data.Year & ", true, '" & formatadatamysql(_data) & "'," & _nrseqgestor & ")")
                tb1 = tab1.conectar("select * from tbpad where nrseqcolaborador = " & _nrseqcolaborador & " and anobase = " & _data.Year & " and ativo = true ")
                _nrseq = tb1.Rows(0)("nrseq").ToString
            Else
                _nrseq = tb1.Rows(0)("nrseq").ToString
            End If
            Dim revisao As Integer = 1
            If _data.Month() > 6 Then
                revisao = 2
            End If
            Dim xdescricao As String = ""
            xdescricao = tratatexto(retiraCaracteresEspeciais(_descricao) & IIf(_PlanoDesenvolvimento = "", "", " Plano de desenvolvimento:" & retiraCaracteresEspeciais(_PlanoDesenvolvimento)))
            tb1 = tab1.conectar("select * from tbpad_avaliacoes where nrseqobjetivo = " & nrseqobjetivo & " and nrseqpad = " & _nrseq & " and ativo = true ")
            If tb1.Rows.Count = 0 Then
                revisao = 1
            Else
                revisao = 2
            End If

            tb1 = tab1.IncluirAlterarDados("insert into tbpad_avaliacoes (nrseqpad, nrseqobjetivo, descricao, revisao, nrseqresultado, ativo, dtcad, usercad) values (" & _nrseq & ", " & nrseqobjetivo & ",'" & _descricao & "'," & revisao & ",0, true, '" & formatadatamysql(_data) & "', '" & _usercad & "')")


            Return True
        Catch eximport As Exception
            _mensagemerro = eximport.Message
            Return False
        End Try

    End Function
    Public Function imprimirpad() As Boolean
        If _nrseq = 0 Then
            Return False
        End If
        If Not procurar() Then
            Return False
        End If
        Try
            Dim caminhologo As String = HttpContext.Current.Server.MapPath("img")
            ' Dim caminhologo As String = HttpContext.Current.Server.MapPath("imgpropostas")
            Dim caminhoimg As String = HttpContext.Current.Server.MapPath("imgpropostas")
            Dim xlogo As String = caminhologo & "\CevaLogopdfnovo2.jpg"
            Dim xfita As String = caminhologo & "\fitapremio.png"
            Dim xlogosistema As String = caminhologo & "\padlogo.png"
            ' Dim xlogo As String = caminhologo & "\logo02.jpg"

            Dim ximagemvermelho As String = caminhologo & "\image002.png"

            Dim _imagemvermelho As Image = Image.FromFile(ximagemvermelho)
            Dim _novalogo As Image = Image.FromFile(xlogo)

            '--------------------configuração do objeto--------------------------
            Dim Ialtura As Integer = 10 'altura do dos itens
            Dim Ilargura As Integer = 150 'largura dos itens

            Dim Imargintop As Integer = 0 'definir a distancia do top
            Dim Imarginesquerda As Integer = 5 'definir a distancia lateral 

            Dim Icorborda_compra As New Color 'corda borda
            Dim Icorborda As New Color 'corda borda
            Dim Itamanhoborda As Integer = 1 ' definições da Bordas 


            Dim corHex As String = "#990033"
            Dim Icorfundo As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml(corHex)

            Dim corHex2 As String = "#F8F8F8"
            Dim Icorfundocinza As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml(corHex2)

            Dim azulceva As String = "#051039"
            Dim icorazulceva As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml(azulceva)
            Dim vermelhoceva As String = "#FF0000"
            Dim icorvermelhoceva As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml(vermelhoceva)
            'Dim Icorfundo As New Color 'cor de fundo
            Dim Icorfundoconteudo As New Color ' padrao do conteudo é branco
            Dim Icorletra As New Color
            Dim Icorletra_compra As New Color
            Dim Icorletrapreto As New Color
            Dim centrallegenda As Integer = 5
            Dim tamanhofonte As Integer = 8
            Dim tamanhofonterota As Integer = 8
            ' Dim tamanhofonteItens As Integer = 8
            Dim tamanhoborda As Integer = 1
            Dim recuotextocabecalho As Integer = 4
            Dim recuotextoinformacao As Integer = recuotextocabecalho + 12
            Dim recuoborda As Integer = 30
            Dim janelainicial As Integer = 0
            Dim reculojanela As Integer = 34
            Dim pagina As Integer = 0
            Dim qtdlinha As Integer = 0
            Dim addmargintop As Integer = 10 'definir a distancia do top
            Dim addmarginesquerda As Integer = 150 'definir a distancia lateral
            Dim inicioret As Integer = 0
            'mPd.InserirIMG(pagina, xaviao, 0, 100, _aviaopdf.Size.Width, _aviaopdf.Size.Height, 100)
            ''   mPd.InserirIMG(pagina, xlogo, 480, 140, _novalogo.Size.Width, _novalogo.Size.Height, 100)
            'mPd.InserirIMG(pagina, xlogo, 480, 140, _novalogo.Size.Width, _novalogo.Size.Height, 100)
            Using Doc = New PdfSharp.Pdf.PdfDocument()

                Dim Page = Doc.AddPage()
                Dim Graphics = PdfSharp.Drawing.XGraphics.FromPdfPage(Page)
                Dim TextFormatter = New PdfSharp.Drawing.Layout.XTextFormatter(Graphics)
                Dim Font8 = New PdfSharp.Drawing.XFont("Arial", 9, PdfSharp.Drawing.XFontStyle.Bold)
                Dim Font6bold = New PdfSharp.Drawing.XFont("Arial", 6, PdfSharp.Drawing.XFontStyle.Bold)
                Dim Font7 = New PdfSharp.Drawing.XFont("Arial", 8, PdfSharp.Drawing.XFontStyle.Regular)
                Dim Font6 = New PdfSharp.Drawing.XFont("Arial", 6, PdfSharp.Drawing.XFontStyle.Regular)
                Dim coluna As Integer = 0
                Graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(xlogo), 10, 10)

                TextFormatter.DrawString("Ficha PAD", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(100, 10, Page.Width, Page.Height))
                TextFormatter.DrawString("Programa de Avaliação de Desempenho", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(100, 20, Page.Width, Page.Height))
                TextFormatter.DrawString("Gestor:", Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(380, 10, Page.Width, Page.Height))
                TextFormatter.DrawString(_nomegestor, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(380, 20, Page.Width, Page.Height))
                janelainicial = 40
                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.Black, 0, janelainicial, 600, 5, 0, 0)
                janelainicial += 10
                TextFormatter.DrawString("Informações Gerais", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                '   mPd.DesenharTexto(0, Imarginesquerda, 125, "Air Freight (LSE) from " & Detalhes(0).Origin & " to " & Detalhes(0).Destination & " (" & _incoterms & ")", 10, estilofonte, fontenegjanelainicialrito, Icorletrapreto)

                janelainicial += 10
                TextFormatter.DrawString("Colaborador", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                ' TextFormatter.DrawString("Nome", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(161, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString("Desligado/Status", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString("Ano PAD", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(461, janelainicial, Page.Width, Page.Height))
                janelainicial += 10
                TextFormatter.DrawString(_matriculacolaborador, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString(" - " & _nomecolaborador, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(60, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString(_desligado & "/" & _status, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString(_anobase, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(461, janelainicial, Page.Width, Page.Height))

                janelainicial += 10



                TextFormatter.DrawString("Cargo", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                'TextFormatter.DrawString("Status", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(161, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString("Empresa / BU", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))
                janelainicial += 10
                TextFormatter.DrawString(_descricaocargo, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                ' TextFormatter.DrawString(_status, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString(_empresa & "/" & _descricaobu, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))

                janelainicial += 20


                Dim lista01 As New List(Of clspad_avaliacoes)
                Dim lista02 As New List(Of clspad_avaliacoes)
                Dim lista03 As New List(Of clspad_avaliacoes)
                For x As Integer = 0 To Avaliacoes.Count - 1
                    If Avaliacoes(x).Ativo = False Then
                        Continue For
                    End If

                    If Avaliacoes(x).Revisao = 1 Then
                        lista01.Add(New clspad_avaliacoes With {.Descricao = Avaliacoes(x).Descricao, .Descricaoobjetivo = Avaliacoes(x).Descricaoobjetivo, .Descricaoresultado = Avaliacoes(x).Descricaoresultado})

                    End If
                    If Avaliacoes(x).Revisao = 2 Then
                        lista02.Add(New clspad_avaliacoes With {.Descricao = Avaliacoes(x).Descricao, .Descricaoobjetivo = Avaliacoes(x).Descricaoobjetivo, .Descricaoresultado = Avaliacoes(x).Descricaoresultado})

                    End If
                    If Avaliacoes(x).Revisao = 3 Then
                        lista03.Add(New clspad_avaliacoes With {.Justificativa = Avaliacoes(x).Justificativa, .Descricao = Avaliacoes(x).Descricao, .Descricaoobjetivo = Avaliacoes(x).Descricaoobjetivo, .Descricaoresultado = Avaliacoes(x).Descricaoresultado})

                    End If
                Next
                Dim caracterfinalx As Integer = wctermoqtdcaracterlinha
                Dim numposicaox As Integer = 0
                Dim completaespacox As Integer = 0
                janelainicial += 5
                ' calcula o tamanho das linhas de grades das avaliações 1, 2 e 3, de acordo com as listas quebradas de descrições
                Dim tamanhograde01 As Integer = 0
                Dim tamanhograde02 As Integer = 0
                Dim tamanhograde03 As Integer = 0
                Dim tamanhograde04 As Integer = 0
                Dim tamanhoobs As Integer = 0


                tamanhoobs += retornaqtdlinhassalto(_obs, 75)


                tamanhoobs += 3

                tamanhoobs = tamanhoobs * 10


                For x As Integer = 0 To lista01.Count - 1
                    tamanhograde01 += retornaqtdlinhassalto(lista01(x).Descricao, 75)
                Next

                tamanhograde01 += ((lista01.Count * 2) + 1)

                tamanhograde01 = tamanhograde01 * 10


                For x As Integer = 0 To lista02.Count - 1
                    tamanhograde02 += retornaqtdlinhassalto(lista02(x).Descricao, 75)
                Next

                tamanhograde02 += ((lista02.Count * 2) + 1)

                tamanhograde02 = tamanhograde02 * 10

                Dim grademaior01ou02 As Integer = tamanhograde01
                If tamanhograde01 < tamanhograde02 Then
                    grademaior01ou02 = tamanhograde02
                End If

                For x As Integer = 0 To lista03.Count - 1
                    tamanhograde03 += retornaqtdlinhassalto(lista03(x).Descricao, 103)
                    tamanhograde03 += retornaqtdlinhassalto(lista03(x).Justificativa, 124)
                Next

                tamanhograde03 += ((lista03.Count * 2) + 1)

                tamanhograde03 = tamanhograde03 * 10


                For x As Integer = 0 To Competencias.Count - 1
                    ' tamanhograde04 += retornaqtdlinhassalto(lista03(x).Descricao, 200)
                    tamanhograde04 += retornaqtdlinhassalto(Competencias(x).Justificativa.ToString, 200)
                Next

                tamanhograde04 += ((Competencias.Count * 2) + 1)

                tamanhograde04 = tamanhograde04 * 10


                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.White, 4, janelainicial, 290, tamanhograde01, 5, 5)
                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.White, 301, janelainicial, 290, tamanhograde02, 5, 5)
                janelainicial += 2
                Dim colunatemp As Integer = janelainicial

                TextFormatter.DrawString("Objetivos", Font8, PdfSharp.Drawing.XBrushes.Red, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                janelainicial += 10
                For x As Integer = 0 To lista01.Count - 1
                    TextFormatter.DrawString(lista01(x).Descricaoobjetivo, Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    janelainicial += 10
                    Dim descimp As List(Of String)
                    descimp = convertetextoemlista(lista01(x).Descricao)
                    For z As Integer = 0 To descimp.Count - 1
                        caracterfinalx = 65
                        Dim wcqtdlinhasmensagemx As Integer = Len(descimp(z)) / caracterfinalx
                        numposicaox = 0

                        completaespacox = 0
                        For y As Integer = 0 To wcqtdlinhasmensagemx

                            '  caracterfinal = wctermoqtdcaracterlinha
                            If descimp(z).Length > caracterfinalx Then
                                caracterfinalx = 65
                            Else
                                ' caracterfinalx = encontraposicao(descava(z), numposicaox, 75)
                                caracterfinalx = encontraposicao(descimp(z), numposicaox, 75)

                            End If

                            completaespacox = wctermoqtdcaracterlinha - caracterfinalx


                            'inicia a contagem das linhas
                            Dim txttextolinha As String = msubstring(descimp(z), numposicaox, caracterfinalx)
                            'recebe  o texto completo da linha

                            Dim txtnovotexto As String = ""

                            ' text que receberar o novo texto

                            For h As Integer = 0 To txttextolinha.Length - 1
                                'inicia tratamento dos caracteres da linha processada
                                Dim caracter = txttextolinha.Substring(h, 1)

                                If caracter = " " And completaespacox <> 0 Then
                                    caracter = "  "
                                    completaespacox -= 1
                                End If
                                'verifica se é um enter ou um espaço muito grande
                                txtnovotexto = txtnovotexto & caracter

                            Next
                            If txtnovotexto = "" Then Continue For
                            ' TextFormatter.DrawString(txtnovotexto, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(8, janelainicial, Page.Width, Page.Height))
                            TextFormatter.DrawString(txtnovotexto, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))

                            qtdlinha += 1
                            janelainicial += 10



                            numposicaox = numposicaox + caracterfinalx + 1  'continua a pegando as linhas da ultima possiçao, a informação +1 é devido ao espaço  da linha anterior
                        Next
                    Next

                    janelainicial += 10
                Next
                janelainicial = colunatemp
                TextFormatter.DrawString("Revisão Meio de Ano", Font8, PdfSharp.Drawing.XBrushes.Red, New PdfSharp.Drawing.XRect(308, janelainicial, Page.Width, Page.Height))
                janelainicial += 10
                For x As Integer = 0 To lista02.Count - 1
                    TextFormatter.DrawString(lista02(x).Descricaoobjetivo, Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(308, janelainicial, Page.Width, Page.Height))
                    janelainicial += 10
                    Dim descimp As List(Of String)
                    descimp = convertetextoemlista(lista02(x).Descricao)
                    For z As Integer = 0 To descimp.Count - 1
                        caracterfinalx = 65
                        Dim wcqtdlinhasmensagemx As Integer = Len(descimp(z)) / caracterfinalx
                        numposicaox = 0

                        completaespacox = 0
                        For y As Integer = 0 To wcqtdlinhasmensagemx

                            '  caracterfinal = wctermoqtdcaracterlinha
                            If descimp(z).Length > caracterfinalx Then
                                caracterfinalx = 65
                            Else
                                ' caracterfinalx = encontraposicao(descava(z), numposicaox, 75)
                                caracterfinalx = encontraposicao(descimp(z), numposicaox, 75)
                            End If

                            completaespacox = wctermoqtdcaracterlinha - caracterfinalx


                            'inicia a contagem das linhas
                            Dim txttextolinha As String = msubstring(descimp(z), numposicaox, caracterfinalx)
                            'recebe  o texto completo da linha

                            Dim txtnovotexto As String = ""

                            ' text que receberar o novo texto

                            For h As Integer = 0 To txttextolinha.Length - 1
                                'inicia tratamento dos caracteres da linha processada
                                Dim caracter = txttextolinha.Substring(h, 1)

                                If caracter = " " And completaespacox <> 0 Then
                                    caracter = "  "
                                    completaespacox -= 1
                                End If
                                'verifica se é um enter ou um espaço muito grande
                                txtnovotexto = txtnovotexto & caracter

                            Next
                            If txtnovotexto = "" Then Continue For
                            ' TextFormatter.DrawString(txtnovotexto, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(8, janelainicial, Page.Width, Page.Height))
                            TextFormatter.DrawString(txtnovotexto, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(308, janelainicial, Page.Width, Page.Height))

                            qtdlinha += 1
                            janelainicial += 10



                            numposicaox = numposicaox + caracterfinalx + 1  'continua a pegando as linhas da ultima possiçao, a informação +1 é devido ao espaço  da linha anterior
                        Next
                    Next


                    ' janelainicial += 10
                    janelainicial += 10



                Next
                janelainicial = colunatemp + grademaior01ou02 + 2
                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.White, 4, janelainicial, 587, tamanhograde03, 5, 5)

                janelainicial += 2
                TextFormatter.DrawString("Revisão de Final de Ano", Font8, PdfSharp.Drawing.XBrushes.Red, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                janelainicial += 10
                '       Dim saltodado As Integer = janelainicial
                For x As Integer = 0 To lista03.Count - 1

                    TextFormatter.DrawString(lista03(x).Descricaoobjetivo, Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString("Resultado", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(450, janelainicial, Page.Width, Page.Height))
                    janelainicial += 10
                    TextFormatter.DrawString(lista03(x).Descricaoresultado, Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(450, janelainicial, Page.Width, Page.Height))
                    Dim descimp As List(Of String)
                    descimp = convertetextoemlista(lista03(x).Descricao)
                    For z As Integer = 0 To descimp.Count - 1
                        caracterfinalx = 103
                        Dim wcqtdlinhasmensagemx As Integer = Len(descimp(z)) / caracterfinalx
                        numposicaox = 0

                        completaespacox = 0
                        For y As Integer = 0 To wcqtdlinhasmensagemx

                            '  caracterfinal = wctermoqtdcaracterlinha
                            If descimp(z).Length > caracterfinalx Then
                                caracterfinalx = 103
                            Else
                                ' caracterfinalx = encontraposicao(descava(z), numposicaox, 75)
                                caracterfinalx = encontraposicao(descimp(z), numposicaox, 103)
                            End If

                            completaespacox = wctermoqtdcaracterlinha - caracterfinalx


                            'inicia a contagem das linhas
                            Dim txttextolinha As String = msubstring(descimp(z), numposicaox, caracterfinalx)
                            'recebe  o texto completo da linha

                            Dim txtnovotexto As String = ""

                            ' text que receberar o novo texto

                            For h As Integer = 0 To txttextolinha.Length - 1
                                'inicia tratamento dos caracteres da linha processada
                                Dim caracter = txttextolinha.Substring(h, 1)

                                If caracter = " " And completaespacox <> 0 Then
                                    caracter = "  "
                                    completaespacox -= 1
                                End If
                                'verifica se é um enter ou um espaço muito grande
                                txtnovotexto = txtnovotexto & caracter

                            Next
                            If txtnovotexto = "" Then Continue For
                            ' TextFormatter.DrawString(txtnovotexto, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(8, janelainicial, Page.Width, Page.Height))
                            TextFormatter.DrawString(txtnovotexto, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))

                            qtdlinha += 1
                            janelainicial += 10



                            numposicaox = numposicaox + caracterfinalx + 1  'continua a pegando as linhas da ultima possiçao, a informação +1 é devido ao espaço  da linha anterior
                        Next
                    Next

                    If lista03(x).Justificativa <> "" Then
                        ' TextFormatter.DrawString("Justificativa:", Font6bold, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                        '  janelainicial += 8
                        descimp = convertetextoemlista("Justificativa:" & lista03(x).Justificativa)
                        For z As Integer = 0 To descimp.Count - 1
                            caracterfinalx = 124
                            Dim wcqtdlinhasmensagemx As Integer = Len(descimp(z)) / caracterfinalx
                            numposicaox = 0

                            completaespacox = 0
                            For y As Integer = 0 To wcqtdlinhasmensagemx

                                '  caracterfinal = wctermoqtdcaracterlinha
                                If descimp(z).Length > caracterfinalx Then
                                    caracterfinalx = 124
                                Else
                                    ' caracterfinalx = encontraposicao(descava(z), numposicaox, 75)
                                    caracterfinalx = encontraposicao(descimp(z), numposicaox, 124)
                                End If

                                completaespacox = wctermoqtdcaracterlinha - caracterfinalx


                                'inicia a contagem das linhas
                                Dim txttextolinha As String = msubstring(descimp(z), numposicaox, caracterfinalx)
                                'recebe  o texto completo da linha

                                Dim txtnovotexto As String = ""

                                ' text que receberar o novo texto

                                For h As Integer = 0 To txttextolinha.Length - 1
                                    'inicia tratamento dos caracteres da linha processada
                                    Dim caracter = txttextolinha.Substring(h, 1)

                                    If caracter = " " And completaespacox <> 0 Then
                                        caracter = "  "
                                        completaespacox -= 1
                                    End If
                                    'verifica se é um enter ou um espaço muito grande
                                    txtnovotexto = txtnovotexto & caracter

                                Next
                                If txtnovotexto = "" Then Continue For
                                ' TextFormatter.DrawString(txtnovotexto, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(8, janelainicial, Page.Width, Page.Height))
                                TextFormatter.DrawString(txtnovotexto, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))

                                qtdlinha += 1
                                janelainicial += 8



                                numposicaox = numposicaox + caracterfinalx + 1  'continua a pegando as linhas da ultima possiçao, a informação +1 é devido ao espaço  da linha anterior
                            Next
                        Next
                        janelainicial -= 8
                    End If
                    janelainicial += 10
                Next

                If janelainicial < 460 Then
                    janelainicial = 469
                Else
                    janelainicial += 10
                End If

                Dim xpuloupagina As Boolean = False


                If (janelainicial + (_cursos.Count * 10) + tamanhograde04 >= 700) Then
                    xpuloupagina = True
                End If

                pagina += 1
                Graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(ximagemvermelho), 0, 780, _imagemvermelho.Size.Width, _imagemvermelho.Size.Height)
                Graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(xlogosistema), 550, 810, 16, 16)
                TextFormatter.DrawString("Página:" & pagina, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(20, 820, Page.Width, Page.Height))
                TextFormatter.DrawString(FormatDateTime(Date.Now.Date, DateFormat.ShortDate), Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(20, 830, Page.Width, Page.Height))
                TextFormatter.DrawString("Sistema PAD ", Font6, PdfSharp.Drawing.XBrushes.Gold, New PdfSharp.Drawing.XRect(550, 830, Page.Width, Page.Height))

                If xpuloupagina Then
                    janelainicial = 10
                    Doc.Pages.Add()
                    Graphics = PdfSharp.Drawing.XGraphics.FromPdfPage(Doc.Pages.Item(Doc.Pages.Count - 1))
                    TextFormatter = New PdfSharp.Drawing.Layout.XTextFormatter(Graphics)


                    Graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(xlogo), 10, 10)

                    TextFormatter.DrawString("Ficha PAD", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(100, 10, Page.Width, Page.Height))
                    TextFormatter.DrawString("Programa de Avaliação de Desempenho", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(100, 20, Page.Width, Page.Height))
                    TextFormatter.DrawString("Gestor:", Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(380, 10, Page.Width, Page.Height))
                    TextFormatter.DrawString(_nomegestor, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(380, 20, Page.Width, Page.Height))
                    janelainicial = 40
                    Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.Black, 0, janelainicial, 600, 5, 0, 0)
                    janelainicial += 10
                    TextFormatter.DrawString("Informações Gerais", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    '   mPd.DesenharTexto(0, Imarginesquerda, 125, "Air Freight (LSE) from " & Detalhes(0).Origin & " to " & Detalhes(0).Destination & " (" & _incoterms & ")", 10, estilofonte, fontenegjanelainicialrito, Icorletrapreto)

                    janelainicial += 10
                    TextFormatter.DrawString("Colaborador", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    ' TextFormatter.DrawString("Nome", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(161, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString("Desligado/Status", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString("Ano PAD", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(461, janelainicial, Page.Width, Page.Height))
                    janelainicial += 10
                    TextFormatter.DrawString(_matriculacolaborador, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString(" - " & _nomecolaborador, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(60, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString(_desligado & "/" & _status, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString(_anobase, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(461, janelainicial, Page.Width, Page.Height))

                    janelainicial += 10



                    TextFormatter.DrawString("Cargo", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    'TextFormatter.DrawString("Status", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(161, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString("Empresa / BU", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))
                    janelainicial += 10
                    TextFormatter.DrawString(_descricaocargo, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    ' TextFormatter.DrawString(_status, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString(_empresa & "/" & _descricaobu, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(311, janelainicial, Page.Width, Page.Height))

                    janelainicial += 20

                End If

                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.White, 4, janelainicial, 587, (_cursos.Count * 10) + 20, 5, 5)
                janelainicial += 2
                TextFormatter.DrawString("Treinamentos indicados", Font8, PdfSharp.Drawing.XBrushes.Red, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString("Concluído ?", Font8, PdfSharp.Drawing.XBrushes.Red, New PdfSharp.Drawing.XRect(450, janelainicial, Page.Width, Page.Height))
                janelainicial += 10
                For x As Integer = 0 To _cursos.Count - 1
                    TextFormatter.DrawString(_cursos(x).Descricao, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    TextFormatter.DrawString(If(_cursos(x).Concluido, "Sim - " & FormatDateTime(_cursos(x).Dtconcluido, DateFormat.ShortDate), "Não"), Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(450, janelainicial, Page.Width, Page.Height))
                    janelainicial += 10


                Next

                janelainicial += 12


                '  Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.White, 4, janelainicial, 290, (_competencias.Count * 10) + 20, 5, 5)
                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.White, 4, janelainicial, 290, tamanhograde04, 5, 5)
                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.White, 301, janelainicial, 290, tamanhoobs, 5, 5)
                janelainicial += 2
                colunatemp = janelainicial

                TextFormatter.DrawString("Competências", Font8, PdfSharp.Drawing.XBrushes.Red, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                janelainicial += 10

                For x As Integer = 0 To _competencias.Count - 1
                    TextFormatter.DrawString(_competencias(x).Descricaoobjetivo, Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                    '  janelainicial += 10
                    TextFormatter.DrawString(_competencias(x).Descricaoresultado, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(180, janelainicial, Page.Width, Page.Height))
                    If _competencias(x).Justificativa <> "" Then
                        janelainicial += 10
                        'TextFormatter.DrawString("Justificativa:", Font6bold, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                        'janelainicial += 8
                        Dim descava As New List(Of String)
                        descava = convertetextoemlista("Justificativa:" & _competencias(x).Justificativa)
                        For z As Integer = 0 To descava.Count - 1
                            caracterfinalx = 90
                            Dim wcqtdlinhasmensagemx As Integer = Len(descava(z)) / caracterfinalx
                            numposicaox = 0

                            completaespacox = 0
                            For y As Integer = 0 To wcqtdlinhasmensagemx

                                '  caracterfinal = wctermoqtdcaracterlinha
                                If descava(z).Length > caracterfinalx Then
                                    caracterfinalx = 90
                                Else
                                    caracterfinalx = encontraposicao(descava(z), numposicaox, 90)
                                End If

                                completaespacox = wctermoqtdcaracterlinha - caracterfinalx


                                'inicia a contagem das linhas
                                Dim txttextolinha As String = msubstring(descava(z), numposicaox, caracterfinalx)
                                'recebe  o texto completo da linha

                                Dim txtnovotexto As String = ""

                                ' text que receberar o novo texto

                                For h As Integer = 0 To txttextolinha.Length - 1
                                    'inicia tratamento dos caracteres da linha processada
                                    Dim caracter = txttextolinha.Substring(h, 1)

                                    If caracter = " " And completaespacox <> 0 Then
                                        caracter = "  "
                                        completaespacox -= 1
                                    End If
                                    'verifica se é um enter ou um espaço muito grande
                                    txtnovotexto = txtnovotexto & caracter

                                Next
                                If txtnovotexto = "" Then Continue For
                                ' TextFormatter.DrawString(txtnovotexto, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(8, janelainicial, Page.Width, Page.Height))
                                TextFormatter.DrawString(txtnovotexto, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))

                                qtdlinha += 1
                                janelainicial += 8



                                numposicaox = numposicaox + caracterfinalx + 1  'continua a pegando as linhas da ultima possiçao, a informação +1 é devido ao espaço  da linha anterior
                            Next
                        Next
                        janelainicial -= 8
                    End If
                    janelainicial += 10
                Next
                janelainicial = colunatemp
                TextFormatter.DrawString("Observações", Font8, PdfSharp.Drawing.XBrushes.Red, New PdfSharp.Drawing.XRect(308, janelainicial, Page.Width, Page.Height))
                janelainicial += 10

                Dim descimpobs As List(Of String)
                descimpobs = convertetextoemlista(_obs)
                For z As Integer = 0 To descimpobs.Count - 1
                    caracterfinalx = 75
                    Dim wcqtdlinhasmensagemx As Integer = Len(descimpobs(z)) / caracterfinalx
                    numposicaox = 0

                    completaespacox = 0
                    For y As Integer = 0 To wcqtdlinhasmensagemx

                        '  caracterfinal = wctermoqtdcaracterlinha
                        If descimpobs(z).Length > caracterfinalx Then
                            caracterfinalx = 75
                        Else
                            caracterfinalx = encontraposicao(descimpobs(z), numposicaox, 75)
                        End If

                        completaespacox = wctermoqtdcaracterlinha - caracterfinalx


                        'inicia a contagem das linhas
                        Dim txttextolinha As String = msubstring(descimpobs(z), numposicaox, caracterfinalx)
                        'recebe  o texto completo da linha

                        Dim txtnovotexto As String = ""

                        ' text que receberar o novo texto

                        For h As Integer = 0 To txttextolinha.Length - 1
                            'inicia tratamento dos caracteres da linha processada
                            Dim caracter = txttextolinha.Substring(h, 1)

                            If caracter = " " And completaespacox <> 0 Then
                                caracter = "  "
                                completaespacox -= 1
                            End If
                            'verifica se é um enter ou um espaço muito grande
                            txtnovotexto = txtnovotexto & caracter

                        Next
                        If txtnovotexto = "" Then Continue For
                        ' TextFormatter.DrawString(txtnovotexto, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(8, janelainicial, Page.Width, Page.Height))
                        TextFormatter.DrawString(txtnovotexto, Font7, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(308, janelainicial, Page.Width, Page.Height))

                        qtdlinha += 1
                        janelainicial += 10



                        numposicaox = numposicaox + caracterfinalx + 1  'continua a pegando as linhas da ultima possiçao, a informação +1 é devido ao espaço  da linha anterior
                    Next
                Next

                janelainicial = colunatemp + IIf(tamanhoobs > (tamanhograde04) + 20, tamanhoobs, (tamanhograde04) + 10)

                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.Black, PdfSharp.Drawing.XBrushes.White, 4, janelainicial, 587, 90, 5, 5)
                janelainicial += 4
                TextFormatter.DrawString("Resultado Final:", Font8, PdfSharp.Drawing.XBrushes.Red, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                Graphics.DrawRoundedRectangle(PdfSharp.Drawing.XPens.LightGray, PdfSharp.Drawing.XBrushes.DarkBlue, 195, janelainicial, _descricaoresultado.Length * 8, recuotextocabecalho * 3, 0, 0)


                TextFormatter.DrawString(_descricaoresultado, Font8, PdfSharp.Drawing.XBrushes.White, New PdfSharp.Drawing.XRect(200, janelainicial, Page.Width, Page.Height))


                If _pontosresultado > 4 Then

                    Graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(xfita), 550, janelainicial, 16, 32)

                End If

                janelainicial += 10
                TextFormatter.DrawString("Aceite:", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(11, janelainicial, Page.Width, Page.Height))
                janelainicial += 40

                Graphics.DrawLine(PdfSharp.Drawing.XPens.Black, 50, janelainicial, 250, janelainicial)
                Graphics.DrawLine(PdfSharp.Drawing.XPens.Black, New PdfSharp.Drawing.XPoint(350, janelainicial), New PdfSharp.Drawing.XPoint(550, janelainicial))
                janelainicial += 10
                TextFormatter.DrawString("Gestor", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(130, janelainicial, Page.Width, Page.Height))
                TextFormatter.DrawString("Colaborador", Font8, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(430, janelainicial, Page.Width, Page.Height))

                ' **********************Obsevações
                Dim xpulou As Boolean = False


                If (janelainicial >= 750) Then
                    xpulou = True
                End If

                pagina += 1
                Graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(ximagemvermelho), 0, 780, _imagemvermelho.Size.Width, _imagemvermelho.Size.Height)
                Graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(xlogosistema), 550, 810, 16, 16)
                TextFormatter.DrawString("Página:" & pagina, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(20, 820, Page.Width, Page.Height))
                TextFormatter.DrawString(FormatDateTime(Date.Now.Date, DateFormat.ShortDate), Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(20, 830, Page.Width, Page.Height))
                TextFormatter.DrawString("Sistema PAD ", Font6, PdfSharp.Drawing.XBrushes.Gold, New PdfSharp.Drawing.XRect(550, 830, Page.Width, Page.Height))

                If xpulou Then
                    janelainicial = 10
                    Doc.Pages.Add()
                    Graphics = PdfSharp.Drawing.XGraphics.FromPdfPage(Doc.Pages.Item(Doc.Pages.Count - 1))
                    TextFormatter = New PdfSharp.Drawing.Layout.XTextFormatter(Graphics)
                End If





                'Imargintop -= addmargintop
                'pagina += 1
                'Graphics.DrawImage(PdfSharp.Drawing.XImage.FromFile(ximagemvermelho), 0, 780, _imagemvermelho.Size.Width, _imagemvermelho.Size.Height)
                'TextFormatter.DrawString("Página:" & pagina, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(20, 820, Page.Width, Page.Height))
                '   TextFormatter.DrawString("Página:" & pagina, Font6, PdfSharp.Drawing.XBrushes.Black, New PdfSharp.Drawing.XRect(8, 820, Page.Width, Page.Height))
                ' Gravar o arquivo
                Dim tempo As String = Replace(Date.Now, ":", "").Replace("/", "").Replace(" ", "")
                _arquivopdf = "PAD_" & _nrseq & "_" & tempo & ".pdf"

                'Automatico
                Dim FolderPath As String = "~\arquivoslotes\"
                Dim FilePath As String = HttpContext.Current.Server.MapPath(FolderPath + _arquivopdf)
                '  mPd.GerarArquivo(FilePath) 'local
                Doc.Info.Author = "SmartCode Soluções em softwares - +55 (41) 3385-1270"
                Doc.Info.CreationDate = Data()
                Doc.Info.Creator = "PAD - SmartCode"
                Doc.Save(FilePath)
                Return True
                '  System.Diagnostics.Process.Start("arquivo.pdf")
            End Using
        Catch eximprimir As Exception
            _mensagemerro = eximprimir.Message
            Return False
        End Try

    End Function
    Public Function baixartodos() As Boolean
        Try

            If _ListaBaixarPADs.Count = 0 Then
                _mensagemerro = "Selecione ao menos um PAD para baixar !"
                Return False
            End If
            Dim wcpastatemporaria As String = "zipado" & sonumeros(Date.Now.Date) & sonumeros(hora(True))
            Dim wcpastacriad As String = HttpContext.Current.Server.MapPath("~") & "arquivoslotes\" & wcpastatemporaria
            criarpastas(wcpastacriad)
            Dim salvarem As String = (HttpContext.Current.Server.MapPath("~") & "arquivoslotes\diversos\notas.zip")
            criarpastas(mPASTA(salvarem))
            salvarem = alteranome(HttpContext.Current.Server.MapPath("~") & "arquivoslotes\diversos\notas.zip")
            Dim baixoutudo As Boolean = True

            For x As Integer = 0 To _ListaBaixarPADs.Count - 1
                _procurarpor = "nrseq"
                _nrseq = _ListaBaixarPADs(x).Nrseq
                If Not imprimirpad() Then
                    _ListaBaixarPADs(x).Arquivopdf = _arquivopdf
                    baixoutudo = False
                Else
                    File.Copy(HttpContext.Current.Server.MapPath("~") & "arquivoslotes\" & _arquivopdf, HttpContext.Current.Server.MapPath("~") & "arquivoslotes\" & wcpastatemporaria & "\" & _arquivopdf)
                End If
            Next

            ZipFile.CreateFromDirectory(wcpastacriad, salvarem)
            _arquivopdf = mARQUIVO(salvarem)
            Return baixoutudo
        Catch extodos As Exception
            _mensagemerro = extodos.Message
            Return False
        End Try

    End Function
    Public Function relatorio() As Boolean
        Try
            _relatoriopad.Clear()
            Totalavaliacoes1 = 0
            Totalavaliacoes2 = 0
            Totalavaliacoes3 = 0
            Totalcompetencias = 0
            Totalcursos = 0
            Dim xsql As String = "select * from vwpad where ativo = true"
            If _nrseq <> 0 Then
                xsql &= " and nrseq = " & _nrseq
            End If

            If _nomegestor <> "" Then
                xsql &= " and nomegestor like '%" & _nomegestor & "'"
            End If

            If _nomecolaborador <> "" Then
                xsql &= " and nomecolaborador like '%" & _nomecolaborador & "'"
            End If

            If _anobase <> 0 Then
                xsql &= " and anobase = " & _anobase
            End If
            If _empresa <> "" Then
                xsql &= " and empresa = '" & _empresa & "'"
            End If
            If _cidade <> "" Then
                xsql &= " and cidade = '" & _cidade & "'"
            End If

            If _descricaoresultado <> "" Then
                xsql &= " and descricaoresultado = '" & _descricaoresultado & "'"

            End If
            Dim xsqlsituacao As String = ""
            If Situacoes.Count > 0 Then
                For x As Integer = 0 To Situacoes.Count - 1
                    xsqlsituacao &= " status = '" & Situacoes(x) & "' or "
                Next
                xsql &= " and (" & xsqlsituacao.Substring(0, xsqlsituacao.Length - 3) & ")"
            End If


            xsql &= " order by anobase, nomecolaborador"
            Dim tbtemp As New Data.DataTable
            Dim tabtemp As New clsBanco

            Dim tbcursos As New Data.DataTable
            Dim tabcursos As New clsBanco
            Dim tbavalia As New Data.DataTable
            Dim tabavalia As New clsBanco
            Dim tbcompete As New Data.DataTable
            Dim tabcompete As New clsBanco

            tbcursos = tabcursos.conectar("select * from vwpad_cursos where ativo = true " & IIf(_anobase <> 0, " and anobase = " & _anobase, ""))
            Arquivolog.WriteLine("Filtrou cursos:" & hora())
            tbcompete = tabcursos.conectar("select * from vwpad_competencias where competenciaativa = true " & IIf(_anobase <> 0, " and anobase = " & _anobase, ""))
            Arquivolog.WriteLine("Filtrou competências:" & hora())
            tbavalia = tabcursos.conectar("select * from vwpad_avaliacoes where 1=1 " & IIf(_anobase <> 0, " and anobase = " & _anobase, ""))
            Arquivolog.WriteLine("Filtrou avaliações:" & hora())


            tbtemp = tabtemp.conectar(xsql)
            Arquivolog.WriteLine("Filtrou pads:" & hora())
            For x As Integer = 0 To tbtemp.Rows.Count - 1
                Dim xpad1 As New clspad
                xpad1.Naoprocurar = True
                Dim tbproc As Data.DataRow()
                With tbtemp

                    If .Rows(x)("pontosresultado").ToString <> "" Then
                        xpad1.Pontosresultado = .Rows(x)("pontosresultado").ToString
                        xpad1.Descricaoresultado = .Rows(x)("descricaoresultado").ToString

                    End If

                    xpad1.Matriculacolaborador = .Rows(x)("matriculacolaborador").ToString
                    xpad1.Nrseq = .Rows(x)("nrseq").ToString
                    xpad1.Nrseqcolaborador = .Rows(x)("nrseqcolaborador").ToString
                    xpad1.Nrseqgestor = .Rows(x)("nrseqgestor").ToString
                    xpad1.Nomecolaborador = .Rows(x)("nomecolaborador").ToString
                    xpad1.Nomegestor = .Rows(x)("nomegestor").ToString
                    xpad1.Usercad = .Rows(x)("usercad").ToString
                    xpad1.Ativo = logico(.Rows(x)("ativo").ToString)
                    xpad1.Dtcad = .Rows(x)("dtcad").ToString
                    xpad1.Anobase = .Rows(x)("anobase").ToString
                    xpad1.Contrato = .Rows(x)("contrato").ToString
                    xpad1.Empresa = .Rows(x)("empresa").ToString
                    xpad1.Descricaobu = .Rows(x)("descricaobu").ToString
                    xpad1.Descricaocargo = .Rows(x)("descricaocargo").ToString
                    xpad1.Status = .Rows(x)("status").ToString
                    xpad1.Desligado = .Rows(x)("desligado").ToString
                    xpad1.Dtadmissao = tratadata(.Rows(x)("dtadmissao").ToString, gravardata.recuperar)
                    xpad1.Cidade = .Rows(x)("cidade").ToString
                    xpad1.Matriculagestor = .Rows(x)("Matriculagestor").ToString
                End With

                '   tbx = tab1.conectar("select * from vwpad_cursos where nrseqpad = " & xpad1.Nrseq)
                tbproc = tbcursos.Select(" nrseqpad = " & xpad1.Nrseq)
                xpad1.Cursos.Clear()
                For x1 As Integer = 0 To tbproc.Count - 1
                    Dim xcurso As New clspad_cursos
                    With tbproc
                        xcurso.Anobase = tbproc(x1)("anobase").ToString
                        xcurso.Nrseq = tbproc(x1)("nrseq").ToString
                        'xcurso.Nrseqcurso = tbproc(0)("nrseqcurso").ToString
                        xcurso.Ativo = logico(tbproc(x1)("ativo").ToString)
                        xcurso.Cargahoraria = tbproc(x1)("cargahoraria").ToString
                        xcurso.Cursoativo = logico(tbproc(x1)("cursoativo").ToString)
                        xcurso.Descricao = tbproc(x1)("descricao").ToString
                        xcurso.Dtcad = tbproc(x1)("dtcad").ToString
                        xcurso.Concluido = IIf(tbproc(x1)("concluido").ToString = "Sim", True, False)
                        If logico(tbproc(x1)("concluido").ToString) Then
                            xcurso.Dtconcluido = tbproc(x1)("dtconcluido").ToString
                        End If
                        Totalcursos += 1
                    End With
                    xpad1.Cursos.Add(xcurso)
                Next
                Dim pularcolaborador As Boolean = False
                tbproc = tbavalia.Select(" nrseqpad = " & xpad1.Nrseq)
                xpad1.Avaliacoes.Clear()
                For x1 As Integer = 0 To tbproc.Count - 1
                    Dim xava As New clspad_avaliacoes


                    xava.Ativo = logico(tbproc(x1)("avaliacaoativa").ToString)
                    xava.Nrseq = tbproc(x1)("nrseq").ToString
                    xava.Nrseqpad = _nrseq
                    xava.Usercad = tbproc(x1)("usercad").ToString
                    xava.Revisao = tbproc(x1)("revisao").ToString
                    xava.Descricaoobjetivo = tbproc(x1)("descricaoobjetivo").ToString
                    xava.Descricaoresultado = tbproc(x1)("descricaoresultado").ToString
                    xava.Descricao = tbproc(x1)("descricao").ToString
                    xava.Justificativa = tbproc(x1)("justificativa").ToString
                    Select Case tbproc(x1)("revisao").ToString
                        Case Is = "1"
                            Totalavaliacoes1 += 1
                        Case Is = "2"
                            Totalavaliacoes2 += 1
                        Case Is = "3"
                            Totalavaliacoes3 += 1
                    End Select
                    xpad1.Avaliacoes.Add(xava)
                Next

                Select Case _filtroespecial.ToLower
                    Case Is = "Com Objetivos".ToLower
                        If _totalavaliacoes1 <= 0 Then
                            Continue For
                        End If
                    Case Is = "Sem Objetivos".ToLower
                        If _totalavaliacoes1 > 0 Then
                            Continue For
                        End If
                    Case Is = "Com revisão de meio de ano".ToLower
                        If _totalavaliacoes2 = 0 Then
                            Continue For
                        End If
                    Case Is = "Sem revisão de meio de ano".ToLower
                        If _totalavaliacoes2 > 0 Then
                            Continue For
                        End If
                    Case Is = "Com revisão de final de ano".ToLower
                        If _totalavaliacoes3 = 0 Then
                            Continue For
                        End If
                    Case Is = "Sem revisão de meio de ano".ToLower
                        If _totalavaliacoes3 > 0 Then
                            Continue For
                        End If
                End Select



                tbproc = tbcompete.Select(" nrseqpad = " & xpad1.Nrseq)
                For x1 As Integer = 0 To tbproc.Count - 1
                    Dim xcomp As New clspad_competencias
                    With tbx
                        xcomp.Naoprocurar = True
                        xcomp.Ativo = logico(tbproc(x1)("competenciaativa").ToString)
                        xcomp.Nrseq = tbproc(x1)("nrseq").ToString
                        xcomp.Nrseqpad = _nrseq
                        xcomp.Usercad = tbproc(x1)("usercad").ToString

                        xcomp.Descricaoobjetivo = tbproc(x1)("descricaoobjetivo").ToString
                        xcomp.Descricaoresultado = tbproc(x1)("descricaoresultado").ToString
                        xcomp.Justificativa = tbproc(x1)("justificativa").ToString
                        Totalcompetencias += 1
                    End With
                    xpad1.Competencias.Add(xcomp)
                Next
                Arquivolog.WriteLine("Adicionou pad " & x & " de " & tbtemp.Rows.Count & ":" & hora())
                _relatoriopad.Add(xpad1)
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
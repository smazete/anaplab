﻿Imports Microsoft.VisualBasic

Imports clsSmart
Imports clssessoes
Imports System.Data



Public Class clsConvenios_Categorias
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Public Sub New()
        '   Usercad = buscarsessao("usuario")
    End Sub

    Public convenios As New List(Of clsConvenios)
    Public categorias As New List(Of clsCategoria)

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbconvenios_categorias"

    Dim _nrseq As Integer
    Dim _nrseqconvenio As Integer
    Dim _nrseqcat As Integer
    Dim _ativo As Boolean
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _dtexclui As DateTime
    Dim _userexclui As String

    Dim _Mensagemerro As String
    Dim _contador As Integer

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqconvenio As Integer
        Get
            Return _nrseqconvenio
        End Get
        Set(value As Integer)
            _nrseqconvenio = value
        End Set
    End Property

    Public Property Nrseqcat As Integer
        Get
            Return _nrseqcat
        End Get
        Set(value As Integer)
            _nrseqcat = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _Mensagemerro
        End Get
        Set(value As String)
            _Mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class
Partial Public Class clsConvenios_Categorias

    Public Function Consultar() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then

                _nrseqconvenio = tb1.Rows(0)("Nrseqconvenio").ToString
                _nrseqcat = tb1.Rows(0)("Nrseqcat").ToString
                _ativo = logico(tb1.Rows(0)("Ativo").ToString)
                _usercad = tb1.Rows(0)("Usercad").ToString
                _userexclui = tb1.Rows(0)("Userexclui").ToString
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Dim xcategoria As New clsCategoria

                If Not xcategoria.Consultardescricao Then
                    _Mensagemerro = "Erro ao consultar!"
                    Return False
                End If

                For x As Integer = 0 To tb1.Rows.Count - 1

                    With xcategoria
                        .Nrseq = tb1.Rows(x)("Nrseq").ToString
                        .Descricao = tb1.Rows(x)("Descricao").ToString
                        .Ativo = logico(tb1.Rows(x)("Ativo").ToString)
                        .Usercad = tb1.Rows(x)("Usercad").ToString
                        .Userexclui = tb1.Rows(x)("Userexclui").ToString
                        Try
                            If tb1.Rows(x)("dtcad").GetType() IsNot GetType(DBNull) Then
                                Dim dt As Date = tb1.Rows(x)("dtcad").ToString
                                .Dtcad = dt.ToString("yyyy-MM-dd")
                            End If
                        Catch exconsultasql As Exception
                            _Mensagemerro = exconsultasql.Message
                        End Try

                        Try
                            If tb1.Rows(x)("dtexclui").GetType() IsNot GetType(DBNull) Then
                                Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                                .Dtexclui = dt.ToString("yyyy-MM-dd")
                            End If
                        Catch exconsultasql As Exception
                            _Mensagemerro = exconsultasql.Message
                        End Try

                    End With
                    categorias.Add(xcategoria)
                Next

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            Dim xcategoria As New clsCategoria

            tb1 = tab1.IncluirAlterarDados("INSERT INTO tbconvenios_categorias (nrseqcat, ativo, nrseqconvenio, dtcad, usercad) SELECT tbcategorias.nrseq, true,  " & _nrseqconvenio & ", '" & hoje() & "','" & HttpContext.Current.Session("usuario") & "' from tbcategorias where tbcategorias.ativo = true and tbcategorias.descricao like '%" & xcategoria.Descricao & "%'")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqconvenio = '" & _nrseqconvenio & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _Mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _Mensagemerro = "Erro ao excluir!"
                Return False
            End If

            Consultar()

            tb1 = tab1.IncluirAlterarDados("update " & Tablename & " set ativo = " & IIf(_ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _Mensagemerro = exexcluir.ToString
            Return False

        End Try

    End Function

    '    Public Function salvar() As Boolean

    '        Try

    '            tb1 = tab1.IncluirAlterarDados("update tbconvenios set ativo=true, salvo = true, nome = '" & _nome & "', endereco = '" & _endereco & "', cidade = '" & _cidade & "', uf = '" & _uf & "', numero = '" & _numero & "', compl = '" & _compl & "', cnpj = '" & _cnpj & "', bairro = '" & _bairro & "', facebook = '" & _facebook & "', twitter = '" & _twitter & "', insta = '" & _insta & "', email = '" & _email & "', url = '" & _url & "', telefonefixo = '" & _telefonefixo & "', telefonecel = '" & _telefonecel & "', cep = '" & _cep & "', nomefantasia = '" & _nomefantasia & "', whats = '" & _whats & "' where nrseq = " & Nrseq)
    '=
    '            Consultar()
    '            Return True
    '        Catch exsalvar As Exception
    '            Mensagemerro = exsalvar.ToString
    '            Return False
    '        End Try
    '    End Function

End Class
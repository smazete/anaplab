﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsclientes

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim _tabeladependentes As New Data.DataTable

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbclientes"
    Dim _listacartao As New List(Of clsclientes_map_cartoes)
    Dim _listaservicos As New List(Of clsClientes_servicosdiversos)
    Dim _tabelaservicosclientes As New Data.DataTable
    Private _detalhes As New List(Of clsServicosdiversos)()

#Region "opções de filtros"

    Dim _incluirsuspensos As Boolean = False
    Dim _incluirnegativados As Boolean = False
    Dim _incluircancelados As Boolean = False
    Dim _listaclientes As New Data.DataTable

#End Region
    Dim _funcioempresa As Integer = 0
    Dim _novamatricula As String = ""
    Dim _usardigitomatricula As Boolean = False
    Dim _nrseq As Integer = 0
    Dim _matricula As String
    Dim _nome As String
    Dim _plano As String
    Dim _cpf As String
    Dim _rg As String
    Dim _emissor As String
    Dim _dtemitido As DateTime
    Dim _dtnasc As DateTime
    Dim _codformapgto As Integer = 0
    Dim _FormaPagtoMens As String
    Dim _logradouro As String
    Dim _numero As String
    Dim _complemento As String
    Dim _bairro As String
    Dim _cidade As String
    Dim _cep As String
    Dim _uf As String
    Dim _telresidencia As String
    Dim _telcelular As String
    Dim _nomereferencia As String
    Dim _numreferencia As String
    Dim _email As String
    Dim _profissao As String
    Dim _indicacao As String
    Dim _liberado As Boolean
    Dim _dtbaixa As DateTime
    Dim _dtcad As DateTime
    Dim _produto As String
    Dim _maquina As String
    Dim _cliente As String
    Dim _id As String
    Dim _diascarencia As String
    Dim _dtcarencia As DateTime
    Dim _usercarencia As String
    Dim _carencia As Boolean
    Dim _dtvalidadecart As DateTime
    Dim _suspenso As Boolean
    Dim _negativado As Boolean
    Dim _nrseqctrl As String
    Dim _nomeseguro As String
    Dim _copiadosistema As Boolean
    Dim _copiadosistemapor As String
    Dim _copiadosistemaem As DateTime
    Dim _senha As String
    Dim _periodicidadepagtos As String
    Dim _planoorigem As String
    Dim _DtPParcela As DateTime
    Dim _ucempresarial As Boolean
    Dim _numidentificacao As String
    Dim _usercad As String
    Dim _codigo As Integer = 0
    Dim _num As String
    Dim _tbvendedor As Integer = 0
    Dim _sexo As String
    Dim _dtvenda As DateTime
    Dim _nomeconta As String
    Dim _cpfconta As String
    Dim _uc As String
    Dim _pn As String
    Dim _cobempresa As Boolean
    Dim _nrseqFuncEmpreConv As Boolean
    Dim _carteiraemit As Boolean
    Dim _emitcarteira As Boolean
    Dim _cobsuspenso As Boolean
    Dim _gravacoes As Boolean
    Dim _dtinclusao As DateTime
    Dim _criadoem As DateTime
    Dim _cadastradopor As String
    Dim _nossonumero As String
    Dim _codaux As String
    Dim _dtalterado As DateTime
    Dim _alteradopor As String
    Dim _baixadopor As String
    Dim _dtsuspenso As DateTime
    Dim _suspensopor As String
    Dim _dtreativado As DateTime
    Dim _reativadopor As String
    Dim _cliente_dependente As Boolean
    Dim _matriculaaux As String
    Dim _alterado As Boolean
    Dim _cobraempresa As Boolean
    Dim _razaosocial As String
    Dim _pessoajuridica As Boolean
    Dim _dtnegativado As DateTime
    Dim _usernegativado As String
    Dim _userbaixa As String
    Dim _nomecartao As String
    Dim _numerocartao As Integer = 0
    Dim _codigocartao As Integer = 0
    Dim _validadecartao As String
    Dim _nrseqempresa As Integer = 0
    Dim _Mensagemerro As String
    Dim _contador As Integer = 0

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Cpf As String
        Get
            Return _cpf
        End Get
        Set(value As String)
            _cpf = value
        End Set
    End Property

    Public Property Rg As String
        Get
            Return _rg
        End Get
        Set(value As String)
            _rg = value
        End Set
    End Property

    Public Property Emissor As String
        Get
            Return _emissor
        End Get
        Set(value As String)
            _emissor = value
        End Set
    End Property

    Public Property Dtemitido As Date
        Get
            Return _dtemitido
        End Get
        Set(value As Date)
            _dtemitido = value
        End Set
    End Property

    Public Property Dtnasc As Date
        Get
            Return _dtnasc
        End Get
        Set(value As Date)
            _dtnasc = value
        End Set
    End Property

    Public Property Codformapgto As Integer
        Get
            Return _codformapgto
        End Get
        Set(value As Integer)
            _codformapgto = value
        End Set
    End Property

    Public Property FormaPagtoMens As String
        Get
            Return _FormaPagtoMens
        End Get
        Set(value As String)
            _FormaPagtoMens = value
        End Set
    End Property

    Public Property Logradouro As String
        Get
            Return _logradouro
        End Get
        Set(value As String)
            _logradouro = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Complemento As String
        Get
            Return _complemento
        End Get
        Set(value As String)
            _complemento = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Uf As String
        Get
            Return _uf
        End Get
        Set(value As String)
            _uf = value
        End Set
    End Property

    Public Property Telresidencia As String
        Get
            Return _telresidencia
        End Get
        Set(value As String)
            _telresidencia = value
        End Set
    End Property

    Public Property Telcelular As String
        Get
            Return _telcelular
        End Get
        Set(value As String)
            _telcelular = value
        End Set
    End Property

    Public Property Nomereferencia As String
        Get
            Return _nomereferencia
        End Get
        Set(value As String)
            _nomereferencia = value
        End Set
    End Property

    Public Property Numreferencia As String
        Get
            Return _numreferencia
        End Get
        Set(value As String)
            _numreferencia = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Profissao As String
        Get
            Return _profissao
        End Get
        Set(value As String)
            _profissao = value
        End Set
    End Property

    Public Property Indicacao As String
        Get
            Return _indicacao
        End Get
        Set(value As String)
            _indicacao = value
        End Set
    End Property

    Public Property Liberado As Boolean
        Get
            Return _liberado
        End Get
        Set(value As Boolean)
            _liberado = value
        End Set
    End Property

    Public Property Dtbaixa As Date
        Get
            Return _dtbaixa
        End Get
        Set(value As Date)
            _dtbaixa = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Produto As String
        Get
            Return _produto
        End Get
        Set(value As String)
            _produto = value
        End Set
    End Property

    Public Property Maquina As String
        Get
            Return _maquina
        End Get
        Set(value As String)
            _maquina = value
        End Set
    End Property

    Public Property Cliente As String
        Get
            Return _cliente
        End Get
        Set(value As String)
            _cliente = value
        End Set
    End Property

    Public Property Id As String
        Get
            Return _id
        End Get
        Set(value As String)
            _id = value
        End Set
    End Property

    Public Property Diascarencia As String
        Get
            Return _diascarencia
        End Get
        Set(value As String)
            _diascarencia = value
        End Set
    End Property

    Public Property Dtcarencia As Date
        Get
            Return _dtcarencia
        End Get
        Set(value As Date)
            _dtcarencia = value
        End Set
    End Property

    Public Property Usercarencia As String
        Get
            Return _usercarencia
        End Get
        Set(value As String)
            _usercarencia = value
        End Set
    End Property

    Public Property Carencia As Boolean
        Get
            Return _carencia
        End Get
        Set(value As Boolean)
            _carencia = value
        End Set
    End Property

    Public Property Dtvalidadecart As Date
        Get
            Return _dtvalidadecart
        End Get
        Set(value As Date)
            _dtvalidadecart = value
        End Set
    End Property

    Public Property Suspenso As Boolean
        Get
            Return _suspenso
        End Get
        Set(value As Boolean)
            _suspenso = value
        End Set
    End Property

    Public Property Negativado As Boolean
        Get
            Return _negativado
        End Get
        Set(value As Boolean)
            _negativado = value
        End Set
    End Property

    Public Property nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Nomeseguro As String
        Get
            Return _nomeseguro
        End Get
        Set(value As String)
            _nomeseguro = value
        End Set
    End Property

    Public Property Copiadosistema As Boolean
        Get
            Return _copiadosistema
        End Get
        Set(value As Boolean)
            _copiadosistema = value
        End Set
    End Property

    Public Property Copiadosistemapor As String
        Get
            Return _copiadosistemapor
        End Get
        Set(value As String)
            _copiadosistemapor = value
        End Set
    End Property

    Public Property Copiadosistemaem As Date
        Get
            Return _copiadosistemaem
        End Get
        Set(value As Date)
            _copiadosistemaem = value
        End Set
    End Property

    Public Property Senha As String
        Get
            Return _senha
        End Get
        Set(value As String)
            _senha = value
        End Set
    End Property

    Public Property Periodicidadepagtos As String
        Get
            Return _periodicidadepagtos
        End Get
        Set(value As String)
            _periodicidadepagtos = value
        End Set
    End Property

    Public Property Planoorigem As String
        Get
            Return _planoorigem
        End Get
        Set(value As String)
            _planoorigem = value
        End Set
    End Property

    Public Property DtPParcela As Date
        Get
            Return _DtPParcela
        End Get
        Set(value As Date)
            _DtPParcela = value
        End Set
    End Property

    Public Property Ucempresarial As Boolean
        Get
            Return _ucempresarial
        End Get
        Set(value As Boolean)
            _ucempresarial = value
        End Set
    End Property

    Public Property Numidentificacao As String
        Get
            Return _numidentificacao
        End Get
        Set(value As String)
            _numidentificacao = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Codigo As Integer
        Get
            Return _codigo
        End Get
        Set(value As Integer)
            _codigo = value
        End Set
    End Property

    Public Property Num As String
        Get
            Return _num
        End Get
        Set(value As String)
            _num = value
        End Set
    End Property

    Public Property Tbvendedor As Integer
        Get
            Return _tbvendedor
        End Get
        Set(value As Integer)
            _tbvendedor = value
        End Set
    End Property

    Public Property Sexo As String
        Get
            Return _sexo
        End Get
        Set(value As String)
            _sexo = value
        End Set
    End Property

    Public Property Dtvenda As Date
        Get
            Return _dtvenda
        End Get
        Set(value As Date)
            _dtvenda = value
        End Set
    End Property

    Public Property Nomeconta As String
        Get
            Return _nomeconta
        End Get
        Set(value As String)
            _nomeconta = value
        End Set
    End Property

    Public Property Cpfconta As String
        Get
            Return _cpfconta
        End Get
        Set(value As String)
            _cpfconta = value
        End Set
    End Property

    Public Property Uc As String
        Get
            Return _uc
        End Get
        Set(value As String)
            _uc = value
        End Set
    End Property

    Public Property Pn As String
        Get
            Return _pn
        End Get
        Set(value As String)
            _pn = value
        End Set
    End Property

    Public Property Cobempresa As Boolean
        Get
            Return _cobempresa
        End Get
        Set(value As Boolean)
            _cobempresa = value
        End Set
    End Property

    Public Property nrseqFuncEmpreConv As Boolean
        Get
            Return _nrseqFuncEmpreConv
        End Get
        Set(value As Boolean)
            _nrseqFuncEmpreConv = value
        End Set
    End Property

    Public Property Carteiraemit As Boolean
        Get
            Return _carteiraemit
        End Get
        Set(value As Boolean)
            _carteiraemit = value
        End Set
    End Property

    Public Property Emitcarteira As Boolean
        Get
            Return _emitcarteira
        End Get
        Set(value As Boolean)
            _emitcarteira = value
        End Set
    End Property

    Public Property Cobsuspenso As Boolean
        Get
            Return _cobsuspenso
        End Get
        Set(value As Boolean)
            _cobsuspenso = value
        End Set
    End Property

    Public Property Gravacoes As Boolean
        Get
            Return _gravacoes
        End Get
        Set(value As Boolean)
            _gravacoes = value
        End Set
    End Property

    Public Property Dtinclusao As Date
        Get
            Return _dtinclusao
        End Get
        Set(value As Date)
            _dtinclusao = value
        End Set
    End Property

    Public Property Criadoem As Date
        Get
            Return _criadoem
        End Get
        Set(value As Date)
            _criadoem = value
        End Set
    End Property

    Public Property Cadastradopor As String
        Get
            Return _cadastradopor
        End Get
        Set(value As String)
            _cadastradopor = value
        End Set
    End Property

    Public Property Nossonumero As String
        Get
            Return _nossonumero
        End Get
        Set(value As String)
            _nossonumero = value
        End Set
    End Property

    Public Property Codaux As String
        Get
            Return _codaux
        End Get
        Set(value As String)
            _codaux = value
        End Set
    End Property

    Public Property Dtalterado As Date
        Get
            Return _dtalterado
        End Get
        Set(value As Date)
            _dtalterado = value
        End Set
    End Property

    Public Property Alteradopor As String
        Get
            Return _alteradopor
        End Get
        Set(value As String)
            _alteradopor = value
        End Set
    End Property

    Public Property Baixadopor As String
        Get
            Return _baixadopor
        End Get
        Set(value As String)
            _baixadopor = value
        End Set
    End Property

    Public Property Dtsuspenso As Date
        Get
            Return _dtsuspenso
        End Get
        Set(value As Date)
            _dtsuspenso = value
        End Set
    End Property

    Public Property Suspensopor As String
        Get
            Return _suspensopor
        End Get
        Set(value As String)
            _suspensopor = value
        End Set
    End Property

    Public Property Dtreativado As Date
        Get
            Return _dtreativado
        End Get
        Set(value As Date)
            _dtreativado = value
        End Set
    End Property

    Public Property Reativadopor As String
        Get
            Return _reativadopor
        End Get
        Set(value As String)
            _reativadopor = value
        End Set
    End Property

    Public Property Cliente_dependente As Boolean
        Get
            Return _cliente_dependente
        End Get
        Set(value As Boolean)
            _cliente_dependente = value
        End Set
    End Property

    Public Property Matriculaaux As String
        Get
            Return _matriculaaux
        End Get
        Set(value As String)
            _matriculaaux = value
        End Set
    End Property

    Public Property Alterado As Boolean
        Get
            Return _alterado
        End Get
        Set(value As Boolean)
            _alterado = value
        End Set
    End Property

    Public Property Cobraempresa As Boolean
        Get
            Return _cobraempresa
        End Get
        Set(value As Boolean)
            _cobraempresa = value
        End Set
    End Property

    Public Property Razaosocial As String
        Get
            Return _razaosocial
        End Get
        Set(value As String)
            _razaosocial = value
        End Set
    End Property

    Public Property Pessoajuridica As Boolean
        Get
            Return _pessoajuridica
        End Get
        Set(value As Boolean)
            _pessoajuridica = value
        End Set
    End Property

    Public Property Dtnegativado As Date
        Get
            Return _dtnegativado
        End Get
        Set(value As Date)
            _dtnegativado = value
        End Set
    End Property

    Public Property Usernegativado As String
        Get
            Return _usernegativado
        End Get
        Set(value As String)
            _usernegativado = value
        End Set
    End Property

    Public Property Userbaixa As String
        Get
            Return _userbaixa
        End Get
        Set(value As String)
            _userbaixa = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _Mensagemerro
        End Get
        Set(value As String)
            _Mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Novamatricula As String
        Get
            Return _novamatricula
        End Get
        Set(value As String)
            _novamatricula = value
        End Set
    End Property

    Public Property Usardigitomatricula As Boolean
        Get
            Return _usardigitomatricula
        End Get
        Set(value As Boolean)
            _usardigitomatricula = value
        End Set
    End Property

    Public Property Nomecartao As String
        Get
            Return _nomecartao
        End Get
        Set(value As String)
            _nomecartao = value
        End Set
    End Property

    Public Property Numerocartao As Integer
        Get
            Return _numerocartao
        End Get
        Set(value As Integer)
            _numerocartao = value
        End Set
    End Property

    Public Property Funcioempresa As Integer
        Get
            Return _funcioempresa
        End Get
        Set(value As Integer)
            _funcioempresa = value
        End Set
    End Property

    Public Property Codigocartao As Integer
        Get
            Return _codigocartao
        End Get
        Set(value As Integer)
            _codigocartao = value
        End Set
    End Property

    Public Property Validadecartao As String
        Get
            Return _validadecartao
        End Get
        Set(value As String)
            _validadecartao = value
        End Set
    End Property

    Public Property Incluirsuspensos As Boolean
        Get
            Return _incluirsuspensos
        End Get
        Set(value As Boolean)
            _incluirsuspensos = value
        End Set
    End Property

    Public Property Incluirnegativados As Boolean
        Get
            Return _incluirnegativados
        End Get
        Set(value As Boolean)
            _incluirnegativados = value
        End Set
    End Property

    Public Property Incluircancelados As Boolean
        Get
            Return _incluircancelados
        End Get
        Set(value As Boolean)
            _incluircancelados = value
        End Set
    End Property

    Public Property Listaclientes As DataTable
        Get
            Return _listaclientes
        End Get
        Set(value As DataTable)
            _listaclientes = value
        End Set
    End Property

    Public Property Listacartao As List(Of clsclientes_map_cartoes)
        Get
            Return _listacartao
        End Get
        Set(value As List(Of clsclientes_map_cartoes))
            _listacartao = value
        End Set
    End Property

    Public Property Listaservicos As List(Of clsClientes_servicosdiversos)
        Get
            Return _listaservicos
        End Get
        Set(value As List(Of clsClientes_servicosdiversos))
            _listaservicos = value
        End Set
    End Property

    Public Property Tabelaservicosclientes As DataTable
        Get
            Return _tabelaservicosclientes
        End Get
        Set(value As DataTable)
            _tabelaservicosclientes = value
        End Set
    End Property

    Public Property Tabeladependentes As DataTable
        Get
            Return _tabeladependentes
        End Get
        Set(value As DataTable)
            _tabeladependentes = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property
End Class

Partial Public Class clsclientes

    Public Function consultartodos() As Boolean

        Try

            tb1 = tab1.conectar("select * from tbclientes where salvo = true and liberado = true")

            If tb1.Rows.Count > 0 Then
                _contador = tb1.Rows.Count
                _table = tb1
            End If

            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function virificamatricula() As Boolean

        Try

            tb1 = tab1.conectar("select * from tbclientes where matricula = '" & Matricula & "' nrseq <> '" & nrseq & "' ")

            If tb1.Rows.Count > 0 Then
                Exit Function
            End If
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultar(Optional salvo As Boolean = False) As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseq = " & _nrseq & IIf(salvo = True, " and salvo=true", ""))

            If tb1.Rows.Count > 0 Then

                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                '_nomecartao = tb1.Rows(0)("nomecartao").ToString
                '_numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                '_validadecartao = tb1.Rows(0)("validadecartao").ToString
                '_codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)

                _dtemitido = tratadata(tb1.Rows(0)("dtemitido").ToString, gravardata.recuperar)
                _dtnasc = tratadata(tb1.Rows(0)("dtnasc").ToString, gravardata.recuperar)

                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = tratadata(tb1.Rows(0)("dtbaixa").ToString, gravardata.recuperar)
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                carregarcartoes()
            Else
                _Mensagemerro = "Nenhum cliente localizado !"
            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function
    Public Function carregarcartoes() As Boolean
        Try

            _listacartao.Clear()
            If nrseq = 0 Then
                Return False
            End If
            tbx = tabx.conectar("select * from tbclientes_map_cartoes where nrseqcliente = " & _nrseq & " order by dtcad desc")
            For x As Integer = 0 To tbx.Rows.Count - 1
                _listacartao.Add(New clsclientes_map_cartoes With {.Codseg = tbx.Rows(x)("codseg").ToString, .Nomecartao = tbx.Rows(x)("Nomecartao").ToString, .Nrcartao = tbx.Rows(x)("Nrcartao").ToString, .Validade = tbx.Rows(x)("Validade").ToString, .Ativo = tbx.Rows(x)("Ativo").ToString, .Nrseq = tbx.Rows(x)("Nrseq").ToString, .Usercad = tbx.Rows(x)("Usercad").ToString, .Dtcad = tbx.Rows(x)("Dtcad").ToString, .Principal = tbx.Rows(x)("principal").ToString})
            Next
        Catch excard As Exception
            Return False
        End Try

    End Function
    Public Function verificaSeCobrarEmpresa() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE nrseq = '" & _nrseq & "' AND ((cobempresa <> 1 AND (razaosocial = '' OR razaosocial IS NULL)) OR pessoajuridica = 1)")

            If tb1.Rows.Count > 0 Then

                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = tb1.Rows(0)("dtnasc").ToString
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarrazaosocial() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE razaosocial = '" & _razaosocial & "' AND cobraempresa = 1 ")

            If tb1.Rows.Count > 0 Then
                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarnumidentificacao(Optional consultanome As Boolean = False) As Boolean

        Try

            tb1 = tab1.conectar("select * from tbclientes where numidentificacao = '" & _numidentificacao & "' " & IIf(consultanome = True, "and nome like '" & _nome & "%'", ""))

            If tb1.Rows.Count > 0 Then
                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarmatricula() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE nrseq = '" & _nrseq & "' OR matricula = '" & _matricula & "' group by matricula")

            If tb1.Rows.Count > 0 Then
                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarnome() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE nome = '" & _nome & "'")

            If tb1.Rows.Count > 0 Then

                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function listarclientes() As Boolean

        Try
            Dim xsql As String = "select * from tbclientes where salvo = true"

            If _nome <> "" Then
                xsql &= " and nome like '" & _nome & "%'"
            End If
            If _cpf <> "" Then
                xsql &= " and cpf like '" & _cpf & "%'"
            End If
            If _matricula <> "" Then
                xsql &= " and matricula like '" & _matricula & "%'"
            End If
            If Not _incluircancelados Then
                xsql &= " and liberado = true"
            End If
            If Not _incluirnegativados Then
                xsql &= " and (negativado = false or negativado is null)"
            End If
            If Not _incluirsuspensos Then
                xsql &= " and (suspenso = false or suspenso is null)"
            End If

            xsql &= " order by nome"
            Listaclientes = tab1.conectar(xsql)
            Return True

        Catch exlista As Exception
            Return False
        End Try

    End Function

    Public Function consultarcliente(Optional tipoprocura As Integer = 0, Optional chkstatus As CheckBox = Nothing) As Boolean


        Try

            If tipoprocura > 0 Then
                Dim sql = "SELECT * FROM " & _tablename & " WHERE ( nome <> '' AND nome <> ' ' AND nome IS NOT NULL ) AND cliente = '" & Cliente & "'"
                If tipoprocura = 1 Then
                    sql &= " AND nome Like  '%" & _nome & "%' "

                End If

                If tipoprocura = 2 Then
                    sql &= " AND matricula LIKE  '%" & _matricula & "%'"

                End If

                If tipoprocura = 3 Then
                    sql &= " AND cpf LIKE  '%" & _cpf.Replace(".", "") & "%'"

                End If
                If tipoprocura = 4 Then
                    sql &= " AND razaosocial LIKE  '%" & _razaosocial.Replace(".", "") & "%'"

                End If

                If chkstatus IsNot Nothing Then
                    If chkstatus.ID = "chkSuspensos" Then
                        sql &= " AND suspenso <> 1 "
                    End If

                    If chkstatus.ID = "chkNegativados" Then
                        sql &= " AND negativado <> 1 "
                    End If

                    If chkstatus.ID = "chkCancelados" Then
                        sql &= " AND liberado = 1 "
                    End If

                End If

                tb1 = tab1.conectar(sql)

            Else
                tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE ( nome <> '' AND nome <> ' ' AND nome IS NOT NULL ) AND cliente = '" & _cliente & "'")
            End If

            If tb1.Rows.Count > 0 Then

                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarmatriculaecliente(Optional apenasmatricula As Boolean = False, Optional type As Integer = 0) As Boolean


        Try


            If apenasmatricula = True Then
                tb1 = tab1.conectar("SELECT * , matricula FROM " & _tablename & " WHERE cliente = '" & _cliente & "' " & IIf(type = 1, "AND nome LIKE '%" & _nome & "%' ", " ") & IIf(type = 2, "AND matricula LIKE '%" & _matricula & "%' ", "") & IIf(type = 3, "AND nrseq LIKE '%" & _nrseq & "%' ", "") & "ORDER BY nome, matricula, nrseq")
            End If

            If Not apenasmatricula = True Then

                tb1 = tab1.conectar("SELECT * FROM " & _tablename & " where matricula = '" & _matricula & "' AND cliente = '" & _cliente & "'")
            End If

            If tb1.Rows.Count > 0 Then

                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarEmpresaConveniada() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE cliente = '" & _cliente & "' AND pessoajuridica = 1")

            If tb1.Rows.Count > 0 Then
                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try


                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function consultarcpf() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where cpf = '" & _cpf & "' and nrseq <> '" & _nrseq & "'")

            If tb1.Rows.Count > 0 Then
                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function consultarplano() As Boolean

        Try

            tb1 = tab1.conectar("select distinct plano from " & _tablename & " where cliente = '" & _nome & "'")

            If tb1.Rows.Count > 0 Then
                _matricula = tb1.Rows(0)("matricula").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _plano = tb1.Rows(0)("plano").ToString
                _cpf = tb1.Rows(0)("cpf").ToString
                _rg = tb1.Rows(0)("rg").ToString
                _emissor = tb1.Rows(0)("emissor").ToString
                _codformapgto = tratanumero(tb1.Rows(0)("codformapgto").ToString)
                _FormaPagtoMens = tb1.Rows(0)("FormaPagtoMens").ToString
                _logradouro = tb1.Rows(0)("logradouro").ToString
                _numero = tb1.Rows(0)("numero").ToString
                _complemento = tb1.Rows(0)("complemento").ToString
                _bairro = tb1.Rows(0)("bairro").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _cep = tb1.Rows(0)("cep").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _telresidencia = tb1.Rows(0)("telresidencia").ToString
                _telcelular = tb1.Rows(0)("telcelular").ToString
                _nomereferencia = tb1.Rows(0)("nomereferencia").ToString
                _numreferencia = tb1.Rows(0)("numreferencia").ToString
                _email = tb1.Rows(0)("email").ToString
                _profissao = tb1.Rows(0)("profissao").ToString
                _indicacao = tb1.Rows(0)("indicacao").ToString
                _liberado = logico(tb1.Rows(0)("liberado").ToString)
                _produto = tb1.Rows(0)("produto").ToString
                _maquina = tb1.Rows(0)("maquina").ToString
                _cliente = tb1.Rows(0)("cliente").ToString
                _id = tb1.Rows(0)("id").ToString
                _diascarencia = tratanumero(tb1.Rows(0)("diascarencia").ToString)
                _usercarencia = tb1.Rows(0)("usercarencia").ToString
                _carencia = logico(tb1.Rows(0)("carencia").ToString)
                _suspenso = logico(tb1.Rows(0)("suspenso").ToString)
                _negativado = logico(tb1.Rows(0)("negativado").ToString)
                _nomeseguro = tb1.Rows(0)("nomeseguro").ToString
                _copiadosistema = logico(tb1.Rows(0)("copiadosistema").ToString)
                _copiadosistemapor = tb1.Rows(0)("copiadosistemapor").ToString
                _senha = tb1.Rows(0)("nrseq").ToString
                _periodicidadepagtos = tb1.Rows(0)("periodicidadepagtos").ToString
                _planoorigem = tb1.Rows(0)("planoorigem").ToString
                _ucempresarial = logico(tb1.Rows(0)("ucempresarial").ToString)
                _numidentificacao = tb1.Rows(0)("numidentificacao").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _codigo = tratanumero(tb1.Rows(0)("codigo").ToString)
                _num = tb1.Rows(0)("num").ToString
                _tbvendedor = tratanumero(tb1.Rows(0)("tbvendedor").ToString)
                _sexo = tb1.Rows(0)("sexo").ToString
                _nomeconta = tb1.Rows(0)("nomeconta").ToString
                _cpfconta = tb1.Rows(0)("cpfconta").ToString
                _uc = tb1.Rows(0)("uc").ToString
                _pn = tb1.Rows(0)("pn").ToString
                _cobempresa = logico(tb1.Rows(0)("cobempresa").ToString)
                ' _nrseqFuncEmpreConv = logico(tb1.Rows(0)("nrseqFuncEmpreConv").ToString)
                _carteiraemit = logico(tb1.Rows(0)("carteiraemit").ToString)
                _emitcarteira = logico(tb1.Rows(0)("emitcarteira").ToString)
                _cobsuspenso = logico(tb1.Rows(0)("cobsuspenso").ToString)
                _gravacoes = logico(tb1.Rows(0)("gravacoes").ToString)
                _cadastradopor = tb1.Rows(0)("cadastradopor").ToString
                _nossonumero = tb1.Rows(0)("nossonumero").ToString
                _codaux = tb1.Rows(0)("codaux").ToString
                _alteradopor = tb1.Rows(0)("alteradopor").ToString
                _baixadopor = tb1.Rows(0)("baixadopor").ToString
                _suspensopor = tb1.Rows(0)("suspensopor").ToString
                _reativadopor = tb1.Rows(0)("reativadopor").ToString
                _cliente_dependente = logico(tb1.Rows(0)("cliente_dependente").ToString)
                _matriculaaux = tb1.Rows(0)("matriculaaux").ToString
                _alterado = logico(tb1.Rows(0)("alterado").ToString)
                _cobraempresa = logico(tb1.Rows(0)("cobraempresa").ToString)
                _razaosocial = tb1.Rows(0)("razaosocial").ToString
                _pessoajuridica = logico(tb1.Rows(0)("pessoajuridica").ToString)
                _usernegativado = tb1.Rows(0)("usernegativado").ToString
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _nomecartao = tb1.Rows(0)("nomecartao").ToString
                _numerocartao = sonumeros(tb1.Rows(0)("numerocartao").ToString)
                _validadecartao = tb1.Rows(0)("validadecartao").ToString
                _codigocartao = sonumeros(tb1.Rows(0)("codigocartao").ToString)
                Try
                    If tb1.Rows(0)("dtemitido").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtemitido").ToString
                        _dtemitido = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtnasc").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnasc").ToString
                        _dtnasc = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtbaixa").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtbaixa").ToString
                        _dtbaixa = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtcarencia").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcarencia").ToString
                        _dtcarencia = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvalidadecart").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvalidadecart").ToString
                        _dtvalidadecart = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("copiadosistemaem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("copiadosistemaem").ToString
                        _copiadosistemaem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("DtPParcela").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("DtPParcela").ToString
                        _DtPParcela = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtvenda").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtvenda").ToString
                        _dtvenda = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtinclusao").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtinclusao").ToString
                        _dtinclusao = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("criadoem").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("criadoem").ToString
                        _criadoem = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtalterado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtalterado").ToString
                        _dtalterado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtsuspenso").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtsuspenso").ToString
                        _dtsuspenso = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtreativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtreativado").ToString
                        _dtreativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtnegativado").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtnegativado").ToString
                        _dtnegativado = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (cliente ,nrseqctrl , liberado, dtcad, usercad,dtinclusao,criadoem,cadastradopor,nrseqempresa) values ('" & _cliente & "','" & wcnrseqctrl & "', false, '" & valordatamysql(data()) & "','" & HttpContext.Current.Session("usuario") & "','" & valordatamysql(data()) & "','" & valordatamysql(data()) & "','" & HttpContext.Current.Session("usuario") & "','" & HttpContext.Current.Session("idempresaemuso") & "')")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _Mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & _tablename & " Set salvo =  true ,  liberado = True"
            If _matricula <> "" Then
                xsql &= ",matricula = '" & tratatexto(_matricula) & "'"
            End If
            If _nome <> "" Then
                xsql &= ",nome = '" & tratatexto(_nome) & "'"
            End If
            If _plano <> "" Then
                xsql &= ",plano = '" & tratatexto(_plano) & "'"
            End If
            If _cpf <> "" Then
                xsql &= ",cpf = '" & tratatexto(_cpf) & "'"
            End If
            If _rg <> "" Then
                xsql &= ",rg = '" & tratatexto(_rg) & "'"
            End If
            If _emissor <> "" Then
                xsql &= ",emissor = '" & tratatexto(_emissor) & "'"
            End If
            If valordatamysql(_dtemitido) <> "" Then
                xsql &= ",dtemitido = '" & valordatamysql(_dtemitido) & "'"
            End If
            If valordatamysql(_dtnasc) <> "" Then
                xsql &= ",dtnasc = '" & valordatamysql(_dtnasc) & "'"
            End If
            If sonumeros(_codformapgto) <> 0 Then
                xsql &= ",codformapgto = '" & sonumeros(_codformapgto) & "'"
            End If
            If _FormaPagtoMens <> "" Then
                xsql &= ",formapagtomens = '" & tratatexto(_FormaPagtoMens) & "'"
            End If
            If _logradouro <> "" Then
                xsql &= ",logradouro = '" & tratatexto(_logradouro) & "'"
            End If
            If _numero <> "" Then
                xsql &= ",numero = '" & tratatexto(_numero) & "'"
            End If
            If _complemento <> "" Then
                xsql &= ",complemento = '" & tratatexto(_complemento) & "'"
            End If
            If _bairro <> "" Then
                xsql &= ",bairro = '" & tratatexto(_bairro) & "'"
            End If
            If _cidade <> "" Then
                xsql &= ",cidade = '" & tratatexto(_cidade) & "'"
            End If
            If _cep <> "" Then
                xsql &= ",cep = '" & tratatexto(_cep) & "'"
            End If
            If _uf <> "" Then
                xsql &= ",uf = '" & tratatexto(_uf) & "'"
            End If
            If _telresidencia <> "" Then
                xsql &= ",telresidencia = '" & tratatexto(_telresidencia) & "'"
            End If
            If _telcelular <> "" Then
                xsql &= ",telcelular = '" & tratatexto(_telcelular) & "'"
            End If
            If _nomereferencia <> "" Then
                xsql &= ",nomereferencia = '" & tratatexto(_nomereferencia) & "'"
            End If
            If _numreferencia <> "" Then
                xsql &= ",numreferencia = '" & tratatexto(_numreferencia) & "'"
            End If
            If _email <> "" Then
                xsql &= ",email = '" & tratatexto(_email) & "'"
            End If
            If _profissao <> "" Then
                xsql &= ",profissao = '" & tratatexto(_profissao) & "'"
            End If
            If _indicacao <> "" Then
                xsql &= ",indicacao = '" & tratatexto(_indicacao) & "'"
            End If
            If valordatamysql(_dtbaixa) <> "" Then
                xsql &= ",dtbaixa = '" & valordatamysql(_dtbaixa) & "'"
            End If
            If _produto <> "" Then
                xsql &= ",produto = '" & tratatexto(_produto) & "'"
            End If
            If _maquina <> "" Then
                xsql &= ",maquina = '" & tratatexto(_maquina) & "'"
            End If
            'If _cliente <> "" Then
            '    xsql &= ",cliente = '" & tratatexto(_cliente) & "'"
            ''End If
            'If _id <> "" Then
            '    xsql &= ",id = '" & tratatexto(_id) & "'"
            'End If
            '    If sonumeros(_diascarencia) <> 0 Then
            '   xsql &= ",diascarencia = " & sonumeros(_diascarencia)
            '  End If
            'If formatadatamysql(_dtcarencia) <> "" Then
            '    xsql &= ",dtcarencia = '" & formatadatamysql(_dtcarencia) & "'"
            'End If
            'If _usercarencia <> "" Then
            '    xsql &= ",usercarencia = '" & tratatexto(_usercarencia) & "'"
            'End If
            'If logico(_carencia) IsNot Nothing Then
            '    xsql &= ",carencia = " & logico(_carencia)
            'End If
            If valordatamysql(_dtvalidadecart) <> "" Then
                xsql &= ",dtvalidadecart = '" & valordatamysql(_dtvalidadecart) & "'"
            End If
            If logico(_suspenso) IsNot Nothing Then
                xsql &= ",suspenso = " & logico(_suspenso) & ""
            End If
            If logico(_negativado) IsNot Nothing Then
                xsql &= ",negativado = " & logico(_negativado) & ""
            End If
            If _nomeseguro <> "" Then
                xsql &= ",nomeseguro = '" & tratatexto(_nomeseguro) & "'"
            End If
            If logico(_copiadosistema) IsNot Nothing Then
                xsql &= ",copiadosistema = " & logico(_copiadosistema) & ""
            End If
            If _copiadosistemapor <> "" Then
                xsql &= ",copiadosistemapor = '" & tratatexto(_copiadosistemapor) & "'"
            End If
            If valordatamysql(_copiadosistemaem) <> "" Then
                xsql &= ",copiadosistemaem = '" & valordatamysql(_copiadosistemaem) & "'"
            End If
            If _senha <> "" Then
                xsql &= ",senha = '" & tratatexto(_senha) & "'"
            End If
            If _periodicidadepagtos <> "" Then
                xsql &= ",periodicidadepagtos = '" & tratatexto(_periodicidadepagtos) & "'"
            End If
            If _planoorigem <> "" Then
                xsql &= ",planoorigem = '" & tratatexto(_planoorigem) & "'"
            End If
            If valordatamysql(_DtPParcela) <> "" Then
                xsql &= ",dtpparcela = '" & valordatamysql(_DtPParcela) & "'"
            End If
            If logico(_ucempresarial) IsNot Nothing Then
                xsql &= ",ucempresarial = " & logico(_ucempresarial) & ""
            End If
            'If _numidentificacao <> "" Then
            '    xsql &= ",numidentificacao = '" & tratatexto(_numidentificacao) & "'"
            'End If

            If sonumeros(_codigo) <> 0 Then
                xsql &= ",codigo = '" & sonumeros(_codigo) & "'"
            End If
            If _num <> "" Then
                xsql &= ",num = '" & tratatexto(_num) & "'"
            End If
            If sonumeros(_tbvendedor) <> 0 Then
                xsql &= ",tbvendedor = '" & sonumeros(_tbvendedor) & "'"
            End If
            If _sexo <> "" Then
                xsql &= ",sexo = '" & tratatexto(_sexo) & "'"
            End If
            If valordatamysql(_dtvenda) <> "" Then
                xsql &= ",dtvenda = '" & valordatamysql(_dtvenda) & "'"
            End If
            If _nomeconta <> "" Then
                xsql &= ",nomeconta = '" & tratatexto(_nomeconta) & "'"
            End If
            If _cpfconta <> "" Then
                xsql &= ",cpfconta = '" & tratatexto(_cpfconta) & "'"
            End If
            If _uc <> "" Then
                xsql &= ",uc = '" & tratatexto(_uc) & "'"
            End If
            If _pn <> "" Then
                xsql &= ",pn = '" & tratatexto(_pn) & "'"
            End If
            If logico(_cobempresa) IsNot Nothing Then
                xsql &= ",cobempresa = " & logico(_cobempresa) & ""
            End If
            If logico(_funcioempresa) IsNot Nothing Then
                xsql &= ",funcioempresa = " & logico(_funcioempresa) & ""
            End If
            If logico(_carteiraemit) IsNot Nothing Then
                xsql &= ",carteiraemit = " & logico(_carteiraemit) & ""
            End If
            If logico(_emitcarteira) IsNot Nothing Then
                xsql &= ",emitcarteira = " & logico(_emitcarteira) & ""
            End If
            If logico(_cobsuspenso) IsNot Nothing Then
                xsql &= ", cobsuspenso = " & logico(_cobsuspenso) & ""
            End If
            If logico(_gravacoes) IsNot Nothing Then
                xsql &= ",gravacoes = " & logico(_gravacoes) & ""
            End If
            If _nossonumero <> "" Then
                xsql &= ",nossonumero = '" & tratatexto(_nossonumero) & "'"
            End If
            If _codaux <> "" Then
                xsql &= ",codaux = '" & tratatexto(_codaux) & "'"
            End If
            If valordatamysql(_dtalterado) <> "" Then
                xsql &= ",dtalterado = '" & valordatamysql(_dtalterado) & "'"
            End If
            If _alteradopor <> "" Then
                xsql &= ",alteradopor = '" & tratatexto(_alteradopor) & "'"
            End If
            If _baixadopor <> "" Then
                xsql &= ",baixadopor = '" & tratatexto(_baixadopor) & "'"
            End If
            If valordatamysql(_dtsuspenso) <> "" Then
                xsql &= ",dtsuspenso = '" & valordatamysql(_dtsuspenso) & "'"
            End If
            If _suspensopor <> "" Then
                xsql &= ",suspensopor = '" & tratatexto(_suspensopor) & "'"
            End If
            If valordatamysql(_dtreativado) <> "" Then
                xsql &= ",dtreativado = '" & valordatamysql(_dtreativado) & "'"
            End If
            If _reativadopor <> "" Then
                xsql &= ",reativadopor = '" & tratatexto(_reativadopor) & "'"
            End If
            If logico(_cliente_dependente) IsNot Nothing Then
                xsql &= ",cliente_dependente = " & logico(_cliente_dependente) & ""
            End If
            If _matriculaaux <> "" Then
                xsql &= ",matriculaaux = '" & tratatexto(_matriculaaux) & "'"
            End If
            If logico(_alterado) IsNot Nothing Then
                xsql &= ",alterado = " & logico(_alterado) & ""
            End If
            If logico(_cobraempresa) IsNot Nothing Then
                xsql &= ",cobraempresa = " & logico(_cobraempresa) & ""
            End If
            If _razaosocial <> "" Then
                xsql &= ",razaosocial = '" & tratatexto(_razaosocial) & "'"
            End If
            If logico(_pessoajuridica) IsNot Nothing Then
                xsql &= ",pessoajuridica = " & logico(_pessoajuridica) & ""
            End If
            If valordatamysql(_dtnegativado) <> "" Then
                xsql &= ",dtnegativado = '" & valordatamysql(_dtnegativado) & "'"
            End If
            If _usernegativado <> "" Then
                xsql &= ",usernegativado = '" & tratatexto(_usernegativado) & "'"
            End If
            If _userbaixa <> "" Then
                xsql &= ",userbaixa = '" & tratatexto(_userbaixa) & "'"
            End If
            '       If _nomecartao <> "" Then
            '     xsql &= ",nomecartao = '" & tratatexto(_nomecartao) & "'"
            '     End If
            '  If sonumeros(_numerocartao) <> "" Then
            '    xsql &= ",numerocartao = " & sonumeros(_numerocartao) & ""
            '    End If
            '     If sonumeros(_codigocartao) <> "" Then
            '     xsql &= ",codigocartao = '" & sonumeros(_codigocartao) & "'"
            '     End If
            '         If _validadecartao <> "" Then
            '       xsql &= ",validadecartao = '" & _validadecartao & "'"
            '       End If
            xsql &= " where nrseq = " & _nrseq & ""
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _Mensagemerro = "Selecione um cliente válido !"
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update tbclientes set liberado = " & IIf(_liberado, "false, dtexclui = '" & valordatamysql(hoje()) & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _Mensagemerro = exexcluir.ToString
            Return False

        End Try


    End Function

    Public Function geramatricula() As Boolean

        Dim matriculatemp As String = ""
        Dim codforma As Integer = 0, valor As Decimal = 0
        Dim xidentifica As String = zeros("0", 2)
        tb1 = tab1.conectar("Select * from tbplanos where nrseq = '" & _plano & "'")
        If tb1.Rows.Count > 0 AndAlso sonumeros(xidentifica).Length > 0 Then
            xidentifica = zeros(tb1.Rows(0)("identificador").ToString, 2)
        End If


        tb1 = tab1.conectar("Select * from tbformapagto where nrseq = '" & _FormaPagtoMens & "' order by descricao")
        If tb1.Rows.Count = 0 Then
            Codformapgto = 0
            valor = 0

        Else
            codforma = tb1.Rows(0)("nrseq").ToString
            If tb1.Rows(0)("valor").ToString = "" Then
                valor = 0
            Else
                valor = tb1.Rows(0)("valor").ToString
            End If
        End If




        If Usardigitomatricula Then
            tb1 = tab1.conectar("Select matricula from tbclientes where plano = '" & _plano & "' order by CAST(matricula AS UNSIGNED) desc")
            If tb1.Rows.Count = 0 Then
                matriculatemp = zeros(xidentifica, 2) & zeros(CType(mRight(0, 6), Integer) + 1, 6)
            Else
                matriculatemp = zeros(xidentifica, 2) & zeros(CType(mRight(tb1.Rows(0)("matricula").ToString, 6), Integer) + 1, 6)
            End If

        Else
            tb1 = tab1.conectar("Select matricula from tbclientes where plano = '" & _plano & "' ORDER BY CAST(matricula AS UNSIGNED) DESC")
            If tb1.Rows.Count = 0 Then
                matriculatemp = zeros(1, 8)
            Else
                matriculatemp = zeros(CType(tb1.Rows(0)("matricula").ToString, Integer) + 1, 8)
            End If
        End If
        tb1 = tab1.IncluirAlterarDados("update tbclientes set matricula = '" & matriculatemp & "' where codigo = " & nrseq)

        _matricula = matriculatemp

        Return True

    End Function
    Public Function adicionarcartao() As Boolean
        Try



            tb1 = tab1.conectar("select * from tbclientes_map_cartoes where nrseqcliente = " & _nrseq)


            For y As Integer = 0 To _listacartao.Count - 1
                Dim tbproc As Data.DataRow()
                tbproc = tb1.Select(" nrcartao = '" & _listacartao(y).Nrcartao & "'")
                If tbproc.Count > 0 Then
                    tbx = tabx.IncluirAlterarDados("update tbclientes_map_cartoes set nomecartao = '" & _listacartao(y).Nomecartao & "', validade = '" & _listacartao(y).Validade & "', codseg = " & _listacartao(y).Codseg & ", ativo = true, principal = " & _listacartao(y).Principal & " where nrseq = " & tbproc(0)("nrseq").ToString)
                Else
                    tbx = tabx.IncluirAlterarDados("insert into tbclientes_map_cartoes (nrcartao, nomecartao, validade, codseg, ativo, dtcad, usercad, principal, nrseqcliente) values ('" & _listacartao(y).Nrcartao & "', '" & _listacartao(y).Nomecartao & "', '" & _listacartao(y).Validade & "', '" & _listacartao(y).Codseg & "', true, '" & hoje() & "', '" & _usercad & "'," & _listacartao(y).Principal & ", " & _nrseq & ")")
                End If
            Next



            Return True
        Catch excard As Exception
            _Mensagemerro = excard.Message
            Return False
        End Try

    End Function
    Public Function adicionarservicos() As Boolean
        Try
            For x As Integer = 0 To _listaservicos.Count - 1
                tb1 = tab1.conectar("select * from tbclientes_servicosdiversos where nrseq = " & _listaservicos(x).Nrseq)
                If tb1.Rows.Count = 0 Then
                    tb1 = tab1.IncluirAlterarDados("insert into tbclientes_servicosdiversos (matricula, ativo, salvo, cobrado,     dtcad,    dtcobrarate, nrseqdependente, validade, valor, nrseqservico, usercad, databaixa, nrseqcliente) values ('" & _listaservicos(x).Matricula & "',true, true, false, '" & hoje() & "',  '" & formatadatamysql(_listaservicos(x).Dtcobrarate) & "', " & _listaservicos(x).Nrseqdependente & ", '" & formatadatamysql(_listaservicos(x).Validade) & "', " & moeda(_listaservicos(x).Valor) & ", " & _listaservicos(x).Nrseqservico & ",'" & Usercad & "', '" & formatadatamysql(_listaservicos(x).Databaixa) & "', " & _listaservicos(x).Nrseqcliente & ")")
                Else
                    tb1 = tab1.IncluirAlterarDados("update tbclientes_servicosdiversos set dtcobrarate = '" & formatadatamysql(_listaservicos(x).Dtcobrarate) & "', databaixa = '" & formatadatamysql(_listaservicos(x).Databaixa) & "', nrseqdependente =" & _listaservicos(x).Nrseqdependente & " , valor =" & moeda(_listaservicos(x).Valor) & " , validade = '" & formatadatamysql(_listaservicos(x).Validade) & "' where nrseq = " & _listaservicos(x).Nrseq)
                End If
            Next

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function carregarservicos() As Boolean
        Try
            If _nrseq = 0 Then
                _Mensagemerro = "Selecione um cliente válido !"
                Return False
            End If
            Tabelaservicosclientes = tab1.conectar("select * from vwclientes_servicosdiversos where nrseqcliente = " & _nrseq & " order by nrseq")

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function carregardependentes() As Boolean
        Try
            If _matricula = "" Then
                _Mensagemerro = "Abra um cliente válido para localizar os dependentes !"
                Return False
            End If
            Tabeladependentes = tab1.conectar("select * from tbdependentes where ativo = true and matricula = '" & _matricula & "' ")
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function consultarnomecpf(Optional nome As Boolean = False, Optional cpf As Boolean = False) As Boolean

        tb1 = tab1.conectar("select * from " & tablename & " where salvo=true" & IIf(nome = True, " and nome like '" & _nome & "%'", "") & IIf(cpf = True, " and cpf ='" & _cpf & "'", ""))
        If tb1.Rows.Count = 0 Then
            _Mensagemerro = "Não possui nenhum dado gravado para essa consulta."
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _matricula = tb1.Rows(0)("matricula").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _plano = tb1.Rows(0)("plano").ToString
        _cpf = tb1.Rows(0)("cpf").ToString
        _rg = tb1.Rows(0)("rg").ToString
        _telresidencia = tb1.Rows(0)("telresidencia").ToString
        _telcelular = tb1.Rows(0)("telcelular").ToString
        _email = tb1.Rows(0)("email").ToString
        _liberado = tb1.Rows(0)("liberado").ToString
        _table = tb1
        _contador = tb1.Rows.Count
        Return True
        Try
        Catch excons As Exception
            _Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function agendacarregacliente() As Boolean
        If _nrseq = 0 Then
            _Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & tablename & " where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            _Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _matricula = tb1.Rows(0)("matricula").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _plano = tb1.Rows(0)("plano").ToString
        _cpf = tb1.Rows(0)("cpf").ToString
        _rg = tb1.Rows(0)("rg").ToString
        _telresidencia = tb1.Rows(0)("telresidencia").ToString
        _telcelular = tb1.Rows(0)("telcelular").ToString
        _email = tb1.Rows(0)("email").ToString
        _liberado = tb1.Rows(0)("liberado").ToString
        _table = tb1
        Return True
        Try
        Catch excons As Exception
            _Mensagemerro = excons.Message
            Return False
        End Try

    End Function
End Class
﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data

Public Class clsconfig_cadastros


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco

    Dim _table As Data.DataTable
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _dddpadrao As String = ""
    Dim _estadopadrao As String = ""

    Dim _qtdMaximaDependentes As Integer = 0
    Dim _acaoPadraoTaxaAdesao As String = ""
    Dim _qtdMaximaAlteracoesDependentes As Integer = 0
    Dim _qtdMesesZerarAlteracoesDependentes As Integer = 0
    Dim _testarCpf As Boolean = False
    Dim _naoPermitirCpfClienteDuplicado As Boolean = False
    Dim _travarCpf As Boolean = False
    Dim _naoPermitirCpfDependenteDuplicado As Boolean = False
    Dim _repetirDadosClienteDependentes As Boolean = False
    Dim _naoPermitirAlteracoesMatriculas As Boolean = False
    Dim _attMatriculaIgnorandoPlano As Boolean = False
    Dim _permitirApenasAdmReativarControl As Boolean = False
    Dim _opcaoCarteirinhaEmitidaCadCliSempreMarcada As Boolean = False
    Dim _ativarControleEmissaoCarteirinhas As Boolean = False
    Dim _exigirNumCarteirinhaCliente As Boolean = False
    Dim _usarDigitoVerificadorNaMatricula As Boolean = False
    Dim _permitirAlterarMensalidade As Boolean = False
    Dim _permitirApenasAdmExcluiCad As Boolean = False
    Dim _tratarCpfDependenteNaoObrigatorio As Boolean = False
    Dim _tratarParentescoNaoObrigatorio As Boolean = False
    Dim _imprimirProtocoloEntregaCarteirinhas As Boolean = False
    Dim _tratarDtNascNaoObrigatorio As Boolean = False
    Dim _apenasAdmAlterarCadFinCli As Boolean = False
    Dim _usarModuloFuneral As Boolean = False
    Dim _tratarSaldoDeMatriculas As Boolean = False
    Dim _nrseqempresa As Integer = 0
    Dim _ativo As Boolean = False
    Dim _dtUltimaEdicao As DateTime
    Dim _userUltimaEdicao As String = ""
    Dim _exigirTaxaDeAdesaoNoCadastroCliente As Boolean = False
    Dim _ativarControleDeCarteirinhasCadastroClientes As Boolean = False
    Dim _valordecarteirinhasdeclientes As Decimal = 0
    Dim _valorcarteirinhadedependentes As Decimal = 0
    Dim _deixarMarcadoOpcaoCarteirinhaCadastroClientes As Boolean = False
    Dim _deixarMarcadoOpcaoCarteirinhaCadastroDependentes As Boolean = False
    Dim _acaoParaPadraoTaxasCarteirinhas As String = ""
    Dim _apenasAdminEditarClientes As Boolean = False

    Public Sub New()
        _nrseqempresa = buscarsessoes("idempresaemuso")
        _userUltimaEdicao = buscarsessoes("usuario")

    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Dddpadrao As String
        Get
            Return _dddpadrao
        End Get
        Set(value As String)
            _dddpadrao = value
        End Set
    End Property

    Public Property Estadopadrao As String
        Get
            Return _estadopadrao
        End Get
        Set(value As String)
            _estadopadrao = value
        End Set
    End Property

    Public Property QtdMaximaDependentes As Integer
        Get
            Return _qtdMaximaDependentes
        End Get
        Set(value As Integer)
            _qtdMaximaDependentes = value
        End Set
    End Property

    Public Property AcaoPadraoTaxaAdesao As String
        Get
            Return _acaoPadraoTaxaAdesao
        End Get
        Set(value As String)
            _acaoPadraoTaxaAdesao = value
        End Set
    End Property

    Public Property QtdMaximaAlteracoesDependentes As Integer
        Get
            Return _qtdMaximaAlteracoesDependentes
        End Get
        Set(value As Integer)
            _qtdMaximaAlteracoesDependentes = value
        End Set
    End Property

    Public Property QtdMesesZerarAlteracoesDependentes As Integer
        Get
            Return _qtdMesesZerarAlteracoesDependentes
        End Get
        Set(value As Integer)
            _qtdMesesZerarAlteracoesDependentes = value
        End Set
    End Property

    Public Property TestarCpf As Boolean
        Get
            Return _testarCpf
        End Get
        Set(value As Boolean)
            _testarCpf = value
        End Set
    End Property

    Public Property NaoPermitirCpfClienteDuplicado As Boolean
        Get
            Return _naoPermitirCpfClienteDuplicado
        End Get
        Set(value As Boolean)
            _naoPermitirCpfClienteDuplicado = value
        End Set
    End Property

    Public Property TravarCpf As Boolean
        Get
            Return _travarCpf
        End Get
        Set(value As Boolean)
            _travarCpf = value
        End Set
    End Property

    Public Property NaoPermitirCpfDependenteDuplicado As Boolean
        Get
            Return _naoPermitirCpfDependenteDuplicado
        End Get
        Set(value As Boolean)
            _naoPermitirCpfDependenteDuplicado = value
        End Set
    End Property

    Public Property RepetirDadosClienteDependentes As Boolean
        Get
            Return _repetirDadosClienteDependentes
        End Get
        Set(value As Boolean)
            _repetirDadosClienteDependentes = value
        End Set
    End Property

    Public Property NaoPermitirAlteracoesMatriculas As Boolean
        Get
            Return _naoPermitirAlteracoesMatriculas
        End Get
        Set(value As Boolean)
            _naoPermitirAlteracoesMatriculas = value
        End Set
    End Property

    Public Property AttMatriculaIgnorandoPlano As Boolean
        Get
            Return _attMatriculaIgnorandoPlano
        End Get
        Set(value As Boolean)
            _attMatriculaIgnorandoPlano = value
        End Set
    End Property

    Public Property PermitirApenasAdmReativarControl As Boolean
        Get
            Return _permitirApenasAdmReativarControl
        End Get
        Set(value As Boolean)
            _permitirApenasAdmReativarControl = value
        End Set
    End Property

    Public Property OpcaoCarteirinhaEmitidaCadCliSempreMarcada As Boolean
        Get
            Return _opcaoCarteirinhaEmitidaCadCliSempreMarcada
        End Get
        Set(value As Boolean)
            _opcaoCarteirinhaEmitidaCadCliSempreMarcada = value
        End Set
    End Property

    Public Property AtivarControleEmissaoCarteirinhas As Boolean
        Get
            Return _ativarControleEmissaoCarteirinhas
        End Get
        Set(value As Boolean)
            _ativarControleEmissaoCarteirinhas = value
        End Set
    End Property

    Public Property ExigirNumCarteirinhaCliente As Boolean
        Get
            Return _exigirNumCarteirinhaCliente
        End Get
        Set(value As Boolean)
            _exigirNumCarteirinhaCliente = value
        End Set
    End Property

    Public Property UsarDigitoVerificadorNaMatricula As Boolean
        Get
            Return _usarDigitoVerificadorNaMatricula
        End Get
        Set(value As Boolean)
            _usarDigitoVerificadorNaMatricula = value
        End Set
    End Property

    Public Property PermitirAlterarMensalidade As Boolean
        Get
            Return _permitirAlterarMensalidade
        End Get
        Set(value As Boolean)
            _permitirAlterarMensalidade = value
        End Set
    End Property

    Public Property PermitirApenasAdmExcluiCad As Boolean
        Get
            Return _permitirApenasAdmExcluiCad
        End Get
        Set(value As Boolean)
            _permitirApenasAdmExcluiCad = value
        End Set
    End Property

    Public Property TratarCpfDependenteNaoObrigatorio As Boolean
        Get
            Return _tratarCpfDependenteNaoObrigatorio
        End Get
        Set(value As Boolean)
            _tratarCpfDependenteNaoObrigatorio = value
        End Set
    End Property

    Public Property TratarParentescoNaoObrigatorio As Boolean
        Get
            Return _tratarParentescoNaoObrigatorio
        End Get
        Set(value As Boolean)
            _tratarParentescoNaoObrigatorio = value
        End Set
    End Property

    Public Property ImprimirProtocoloEntregaCarteirinhas As Boolean
        Get
            Return _imprimirProtocoloEntregaCarteirinhas
        End Get
        Set(value As Boolean)
            _imprimirProtocoloEntregaCarteirinhas = value
        End Set
    End Property

    Public Property TratarDtNascNaoObrigatorio As Boolean
        Get
            Return _tratarDtNascNaoObrigatorio
        End Get
        Set(value As Boolean)
            _tratarDtNascNaoObrigatorio = value
        End Set
    End Property

    Public Property ApenasAdmAlterarCadFinCli As Boolean
        Get
            Return _apenasAdmAlterarCadFinCli
        End Get
        Set(value As Boolean)
            _apenasAdmAlterarCadFinCli = value
        End Set
    End Property

    Public Property UsarModuloFuneral As Boolean
        Get
            Return _usarModuloFuneral
        End Get
        Set(value As Boolean)
            _usarModuloFuneral = value
        End Set
    End Property

    Public Property TratarSaldoDeMatriculas As Boolean
        Get
            Return _tratarSaldoDeMatriculas
        End Get
        Set(value As Boolean)
            _tratarSaldoDeMatriculas = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property DtUltimaEdicao As DateTime
        Get
            Return _dtUltimaEdicao
        End Get
        Set(value As DateTime)
            _dtUltimaEdicao = value
        End Set
    End Property

    Public Property UserUltimaEdicao As Integer
        Get
            Return _userUltimaEdicao
        End Get
        Set(value As Integer)
            _userUltimaEdicao = value
        End Set
    End Property

    Public Property ExigirTaxaDeAdesaoNoCadastroCliente As Boolean
        Get
            Return _exigirTaxaDeAdesaoNoCadastroCliente
        End Get
        Set(value As Boolean)
            _exigirTaxaDeAdesaoNoCadastroCliente = value
        End Set
    End Property

    Public Property AtivarControleDeCarteirinhasCadastroClientes As Boolean
        Get
            Return _ativarControleDeCarteirinhasCadastroClientes
        End Get
        Set(value As Boolean)
            _ativarControleDeCarteirinhasCadastroClientes = value
        End Set
    End Property

    Public Property Valordecarteirinhasdeclientes As Decimal
        Get
            Return _valordecarteirinhasdeclientes
        End Get
        Set(value As Decimal)
            _valordecarteirinhasdeclientes = value
        End Set
    End Property

    Public Property Valorcarteirinhadedependentes As Decimal
        Get
            Return _valorcarteirinhadedependentes
        End Get
        Set(value As Decimal)
            _valorcarteirinhadedependentes = value
        End Set
    End Property

    Public Property DeixarMarcadoOpcaoCarteirinhaCadastroClientes As Boolean
        Get
            Return _deixarMarcadoOpcaoCarteirinhaCadastroClientes
        End Get
        Set(value As Boolean)
            _deixarMarcadoOpcaoCarteirinhaCadastroClientes = value
        End Set
    End Property

    Public Property DeixarMarcadoOpcaoCarteirinhaCadastroDependentes As Boolean
        Get
            Return _deixarMarcadoOpcaoCarteirinhaCadastroDependentes
        End Get
        Set(value As Boolean)
            _deixarMarcadoOpcaoCarteirinhaCadastroDependentes = value
        End Set
    End Property

    Public Property AcaoParaPadraoTaxasCarteirinhas As String
        Get
            Return _acaoParaPadraoTaxasCarteirinhas
        End Get
        Set(value As String)
            _acaoParaPadraoTaxasCarteirinhas = value
        End Set
    End Property

    Public Property ApenasAdminEditarClientes As Boolean
        Get
            Return _apenasAdminEditarClientes
        End Get
        Set(value As Boolean)
            _apenasAdminEditarClientes = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property
End Class
Partial Public Class clsconfig_cadastros

    Public Function carregar(Optional empresa As Boolean = False) As Boolean

        Try
retestar:
            If empresa = True Then
                tb1 = tab1.conectar("SELECT * FROM tbconfiguracao_cadastro WHERE nrseqempresa = '" & _nrseqempresa & "'")
            Else
                tb1 = tab1.conectar("select * from tbconfiguracao_cadastro")
            End If

            If tb1.Rows.Count = 0 Then

                tb1 = tab1.IncluirAlterarDados("insert into tbconfiguracao_cadastro (dddpadrao, estadopadrao, qtdmaximadependentes, qtdmaximaalteracoesdependentes, qtdmeseszeraralteracoesdependentes, testarcpf, naopermitircpfclienteduplicado, travarcpf, naopermitircpfdependenteduplicado, repetirDadosClienteDependentes, naoPermitirAlteracoesMatriculas, attMatriculaIgnorandoPlano, permitirApenasAdmReativarControl, opcaoCarteirinhaEmitidaCadCliSempreMarcada, ativarControleEmissaoCarteirinhas, exigirNumCarteirinhaCliente, usarDigitoVerificadorNaMatricula, permitirAlterarMensalidade, permitirApenasAdmExcluiCad, tratarCpfDependenteNaoObrigatorio, tratarParentescoNaoObrigatorio, imprimirProtocoloEntregaCarteirinhas, tratarDtNascNaoObrigatorio, apenasAdmAlterarCadFinCli, usarModuloFuneral, tratarSaldoDeMatriculas, nrseqempresa, ativo,   exigirTaxaDeAdesaoNoCadastroCliente, ativarControleDeCarteirinhasCadastroClientes,  valordecarteirinhasdeclientes, valorcarteirinhadedependentes, deixarMarcadoOpcaoCarteirinhaCadastroClientes, deixarMarcadoOpcaoCarteirinhaCadastroDependentes, acaoParaPadraoTaxasCarteirinhas, apenasAdminEditarClientes ) values ('" & _dddpadrao & "','" & _estadopadrao & "'," & moeda(QtdMaximaDependentes) & ", " & moeda(QtdMaximaAlteracoesDependentes) & ", " & moeda(QtdMesesZerarAlteracoesDependentes) & ", " & logico(TestarCpf) & ", " & logico(NaoPermitirCpfClienteDuplicado) & ", " & logico(TravarCpf) & ", " & logico(NaoPermitirCpfDependenteDuplicado) & ", " & logico(RepetirDadosClienteDependentes) & ",  " & logico(NaoPermitirAlteracoesMatriculas) & ",  " & logico(AttMatriculaIgnorandoPlano) & ",  " & logico(PermitirApenasAdmReativarControl) & ",  " & logico(OpcaoCarteirinhaEmitidaCadCliSempreMarcada) & ",  " & logico(AtivarControleEmissaoCarteirinhas) & ",  " & logico(ExigirNumCarteirinhaCliente) & ",  " & logico(UsarDigitoVerificadorNaMatricula) & ",  " & logico(PermitirAlterarMensalidade) & ",  " & logico(PermitirApenasAdmExcluiCad) & ",  " & logico(TratarCpfDependenteNaoObrigatorio) & ",  " & logico(TratarParentescoNaoObrigatorio) & ",  " & logico(ImprimirProtocoloEntregaCarteirinhas) & ",  " & logico(TratarDtNascNaoObrigatorio) & ",  " & logico(ApenasAdmAlterarCadFinCli) & ",  " & logico(UsarModuloFuneral) & ",  " & logico(TratarSaldoDeMatriculas) & ", " & moeda(Nrseqempresa) & ", true,   " & logico(ExigirTaxaDeAdesaoNoCadastroCliente) & ",  " & logico(AtivarControleDeCarteirinhasCadastroClientes) & ",  " & moeda(Valordecarteirinhasdeclientes) & ", " & moeda(Valorcarteirinhadedependentes) & ",  " & logico(DeixarMarcadoOpcaoCarteirinhaCadastroClientes) & ",  " & logico(DeixarMarcadoOpcaoCarteirinhaCadastroDependentes) & ", '" & AcaoParaPadraoTaxasCarteirinhas & "',  " & logico(ApenasAdminEditarClientes) & ")")





                GoTo retestar
            Else

                With tb1
                    _acaoPadraoTaxaAdesao = tb1.Rows(0)("acaoPadraoTaxaAdesao").ToString
                    _acaoParaPadraoTaxasCarteirinhas = tb1.Rows(0)("acaoPadraoTaxaAdesao").ToString
                    _apenasAdmAlterarCadFinCli = logico(tb1.Rows(0)("apenasAdmAlterarCadFinCli").ToString)
                    _apenasAdminEditarClientes = logico(tb1.Rows(0)("apenasAdminEditarClientes").ToString)
                    _ativarControleDeCarteirinhasCadastroClientes = logico(tb1.Rows(0)("ativarControleDeCarteirinhasCadastroClientes").ToString)
                    _ativarControleEmissaoCarteirinhas = logico(tb1.Rows(0)("ativarControleEmissaoCarteirinhas").ToString)
                    _ativo = True
                    _attMatriculaIgnorandoPlano = logico(tb1.Rows(0)("attMatriculaIgnorandoPlano").ToString)
                    _dddpadrao = tb1.Rows(0)("dddpadrao").ToString
                    _deixarMarcadoOpcaoCarteirinhaCadastroClientes = logico(tb1.Rows(0)("deixarMarcadoOpcaoCarteirinhaCadastroClientes").ToString)
                    _deixarMarcadoOpcaoCarteirinhaCadastroDependentes = logico(tb1.Rows(0)("deixarMarcadoOpcaoCarteirinhaCadastroDependentes").ToString)
                    _estadopadrao = tb1.Rows(0)("estadopadrao").ToString
                    ' _dtUltimaEdicao = tb1.rows(0)("apenasAdmAlterarCadFinCli").ToString
                    _exigirNumCarteirinhaCliente = logico(tb1.Rows(0)("exigirNumCarteirinhaCliente").ToString)
                    _exigirTaxaDeAdesaoNoCadastroCliente = logico(tb1.Rows(0)("exigirTaxaDeAdesaoNoCadastroCliente").ToString)
                    _imprimirProtocoloEntregaCarteirinhas = logico(tb1.Rows(0)("imprimirProtocoloEntregaCarteirinhas").ToString)
                    _naoPermitirAlteracoesMatriculas = logico(tb1.Rows(0)("naoPermitirAlteracoesMatriculas").ToString)
                    _naoPermitirCpfClienteDuplicado = logico(tb1.Rows(0)("naoPermitirCpfClienteDuplicado").ToString)
                    _naoPermitirCpfDependenteDuplicado = logico(tb1.Rows(0)("naoPermitirCpfDependenteDuplicado").ToString)
                    _nrseq = tb1.Rows(0)("nrseq").ToString
                    _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                    _opcaoCarteirinhaEmitidaCadCliSempreMarcada = logico(tb1.Rows(0)("opcaoCarteirinhaEmitidaCadCliSempreMarcada").ToString)
                    _permitirAlterarMensalidade = logico(tb1.Rows(0)("permitirAlterarMensalidade").ToString)
                    _permitirApenasAdmExcluiCad = logico(tb1.Rows(0)("permitirApenasAdmExcluiCad").ToString)
                    _permitirApenasAdmReativarControl = logico(tb1.Rows(0)("permitirApenasAdmReativarControl").ToString)
                    _qtdMaximaAlteracoesDependentes = tb1.Rows(0)("qtdMaximaAlteracoesDependentes").ToString
                    _qtdMaximaDependentes = tb1.Rows(0)("qtdMaximaDependentes").ToString
                    _qtdMesesZerarAlteracoesDependentes = tb1.Rows(0)("qtdMesesZerarAlteracoesDependentes").ToString
                    _repetirDadosClienteDependentes = logico(tb1.Rows(0)("repetirDadosClienteDependentes").ToString)
                    _testarCpf = logico(tb1.Rows(0)("testarCpf").ToString)
                    _tratarCpfDependenteNaoObrigatorio = logico(tb1.Rows(0)("tratarCpfDependenteNaoObrigatorio").ToString)
                    _tratarDtNascNaoObrigatorio = logico(tb1.Rows(0)("tratarDtNascNaoObrigatorio").ToString)
                    _tratarParentescoNaoObrigatorio = logico(tb1.Rows(0)("tratarParentescoNaoObrigatorio").ToString)
                    _tratarSaldoDeMatriculas = logico(tb1.Rows(0)("tratarSaldoDeMatriculas").ToString)
                    _travarCpf = logico(tb1.Rows(0)("travarCpf").ToString)
                    _usarDigitoVerificadorNaMatricula = logico(tb1.Rows(0)("usarDigitoVerificadorNaMatricula").ToString)
                    _usarModuloFuneral = logico(tb1.Rows(0)("usarModuloFuneral").ToString)
                    _userUltimaEdicao = tb1.Rows(0)("userUltimaEdicao").ToString
                    _valorcarteirinhadedependentes = tb1.Rows(0)("valorcarteirinhadedependentes").ToString
                    _valordecarteirinhasdeclientes = tb1.Rows(0)("valordecarteirinhasdeclientes").ToString


                End With

            End If
            If empresa = True Then
                tb1 = tab1.conectar("SELECT * FROM tbconfiguracao_cadastro WHERE nrseqempresa = '" & _nrseqempresa & "'")
            Else
                tb1 = tab1.conectar("select * from tbconfiguracao_cadastro")
            End If
            _table = tb1

            Return True
        Catch excarregar As Exception
            _mensagemerro = excarregar.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try

            tb1 = tab1.IncluirAlterarDados("update tbconfiguracao_cadastro set dddpadrao = '" & _dddpadrao & "', estadopadrao = '" & _estadopadrao & "', qtdmaximadependentes = " & moeda(QtdMaximaDependentes) & ", qtdmaximaalteracoesdependentes " & moeda(QtdMaximaAlteracoesDependentes) & ", qtdmeseszeraralteracoesdependentes = " & moeda(QtdMesesZerarAlteracoesDependentes) & ", testarcpf = " & logico(TestarCpf) & ", naopermitircpfclienteduplicado = " & logico(NaoPermitirCpfClienteDuplicado) & ", travarcpf = " & logico(TravarCpf) & ", naopermitircpfdependenteduplicado = " & logico(NaoPermitirCpfDependenteDuplicado) & ", repetirDadosClienteDependentes = " & logico(RepetirDadosClienteDependentes) & ", naoPermitirAlteracoesMatriculas, attMatriculaIgnorandoPlano = " & logico(AttMatriculaIgnorandoPlano) & ", permitirApenasAdmReativarControl = " & logico(PermitirApenasAdmReativarControl) & ", opcaoCarteirinhaEmitidaCadCliSempreMarcada = " & logico(OpcaoCarteirinhaEmitidaCadCliSempreMarcada) & ", ativarControleEmissaoCarteirinhas = " & logico(AtivarControleEmissaoCarteirinhas) & ", exigirNumCarteirinhaCliente = " & logico(ExigirNumCarteirinhaCliente) & ", usarDigitoVerificadorNaMatricula = " & logico(UsarDigitoVerificadorNaMatricula) & ", permitirAlterarMensalidade = " & logico(PermitirAlterarMensalidade) & ", permitirApenasAdmExcluiCad = " & logico(PermitirApenasAdmExcluiCad) & ", tratarCpfDependenteNaoObrigatorio = " & logico(TratarCpfDependenteNaoObrigatorio) & ", tratarParentescoNaoObrigatorio = " & logico(TratarParentescoNaoObrigatorio) & ", imprimirProtocoloEntregaCarteirinhas =  " & logico(ImprimirProtocoloEntregaCarteirinhas) & ", tratarDtNascNaoObrigatorio = " & logico(TratarDtNascNaoObrigatorio) & ", apenasAdmAlterarCadFinCli = " & logico(ApenasAdmAlterarCadFinCli) & ", usarModuloFuneral = " & logico(UsarModuloFuneral) & ", tratarSaldoDeMatriculas = " & logico(TratarSaldoDeMatriculas) & ", tratarSaldoDeMatriculas = " & moeda(Nrseqempresa) & ", nrseqempresa = " & moeda(Nrseqempresa) & ", ativo = true , dtUltimaEdicao = '" & hoje() & "', userUltimaEdicao = '" & _userUltimaEdicao & "', exigirTaxaDeAdesaoNoCadastroCliente =  " & logico(ExigirTaxaDeAdesaoNoCadastroCliente) & ", ativarControleDeCarteirinhasCadastroClientes = " & logico(AtivarControleDeCarteirinhasCadastroClientes) & ",  valordecarteirinhasdeclientes = " & moeda(Valordecarteirinhasdeclientes) & ", valorcarteirinhadedependentes = " & moeda(Valorcarteirinhadedependentes) & ", deixarMarcadoOpcaoCarteirinhaCadastroClientes = " & logico(DeixarMarcadoOpcaoCarteirinhaCadastroClientes) & ", deixarMarcadoOpcaoCarteirinhaCadastroDependentes = " & logico(DeixarMarcadoOpcaoCarteirinhaCadastroDependentes) & ", acaoParaPadraoTaxasCarteirinhas = '" & AcaoParaPadraoTaxasCarteirinhas & "', apenasAdminEditarClientes = " & logico(ApenasAdminEditarClientes))
            Return True
        Catch exalvar As Exception
            _mensagemerro = exalvar.Message.ToLower
            Return False
        End Try

    End Function
End Class
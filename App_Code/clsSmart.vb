﻿Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Xml
Imports System.IO
Imports System.IO.Directory
Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports ExceptionHelper
Public Class clsSmart

    Enum gravardata
        gravar
        recuperar
    End Enum
    Enum tipoestado
        sigla
        nome
    End Enum


    Public Shared Function VALIDACPF(ByVal CPF As String) As Boolean
        On Error Resume Next
        CPF = sonumeros(CPF)
        If CPF = "00000000000" OrElse CPF = "11111111111" OrElse CPF = "22222222222" Then Return False
        Dim soma As Integer
        Dim Resto As Integer
        Dim i As Integer
        'Valida argumento
        CPF = Replace(CPF, ".", "").Replace(",", "")
        CPF = Replace(CPF, "-", "")
        If Len(CPF) <> 11 Then
            Return False

        End If
        If CPF.Replace("0", "").Length = 0 Then
            Return False
        End If
        soma = 0
        For i = 1 To 9
            soma = soma + Val(Mid$(CPF, i, 1)) * (11 - i)
        Next i
        Resto = 11 - (soma - (Int(soma / 11) * 11))
        If Resto = 10 Or Resto = 11 Then Resto = 0
        If Resto <> Val(Mid$(CPF, 10, 1)) Then
            Return False
        End If
        soma = 0
        For i = 1 To 10
            soma = soma + Val(Mid$(CPF, i, 1)) * (12 - i)
        Next i
        Resto = 11 - (soma - (Int(soma / 11) * 11))
        If Resto = 10 Or Resto = 11 Then Resto = 0
        If Resto <> Val(Mid$(CPF, 11, 1)) Then
            Return False
        End If
        Return True
    End Function
    Public Shared wctermoqtdcaracterlinha As Integer = 160
    Public Shared wcusuario As String = ""
    Public Shared wcgrupo As String = ""
    Public Shared wcempresa As String = ""
    Public Shared wcemailsolicitante As String = ""
    Public Shared wcplano As String = ""
    Private Shared meubanco As New clsBanco
    Public Shared minhaversao As String = "3.00 (release " & sonumeros(meubanco.servidor) & ")"
    Public Shared meucliente As String = "Ceva"
    Public Shared meuproduto As String = "CT-e"
    Public Shared Function tratadata(data As String, Optional tipo As gravardata = gravardata.gravar) As String
        If IsDate(data) = False Then data = Date.Now.Date
        If tipo = gravardata.gravar Then
            Return Format(CType(data, Date), "MM/dd/yyyy")
        Else
            Return Format(CType(data, Date), "dd/MM/yyyy")
        End If
    End Function
    Public Shared Function valordata(data As String) As String
        If IsDate(data) = False Then data = "01/01/1900"
        Return FormatDateTime(data, DateFormat.ShortDate)

    End Function
    Public Shared Function valordatamysql(data As String) As String
        If IsDate(data) = False Then data = "01/01/1900"
        Return Format(CType(data, Date), "yyyy-MM-dd")

    End Function
    Public Shared Function tratanumero(valor As String, Optional valorpadrao As Decimal = 0) As Decimal
        If valor = "" Then

            valor = valorpadrao
        End If
        Try
            Return CType(valor, Decimal)
        Catch ex As Exception
            Return valorpadrao
        End Try

    End Function
    Public Shared Function convertetextoemlista(texto As String) As List(Of String)
        Dim novotexto As New List(Of String)
        Dim textotemp As String = ""
        texto = texto.Replace(vbTab, "")
        For x As Integer = 0 To texto.Length - 1

            textotemp &= texto.Substring(x, 1)
            If texto.Substring(x, 1) = vbLf Then
                novotexto.Add(textotemp.Replace(vbLf, ""))
                textotemp = ""
            End If
        Next
        novotexto.Add(textotemp.Replace(vbLf, ""))
        Return novotexto
    End Function
    Public Shared Function criarpastas(caminho As String) As String
        Try


            Dim pastatemp As String = ""
            caminho &= "\"


            For x As Integer = 0 To caminho.Length - 1
                If caminho.Substring(x, 1) = "\" And pastatemp <> "" Then
                    If Not Directory.Exists(pastatemp) Then
                        Try
                            Directory.CreateDirectory(pastatemp)
                        Catch ex2 As Exception

                        End Try

                    End If
                End If
                pastatemp &= caminho.Substring(x, 1)

            Next
            Return "Concluído"

        Catch ex As Exception
            Return "erro " & ex.Message
        End Try


    End Function
    Public Shared Function MesExtenso(ByVal mes As Integer) As String
        Select Case mes
            Case Is = 1
                Return "Janeiro"
            Case Is = 2
                Return "Fevereiro"
            Case Is = 3
                Return "Março"
            Case Is = 4
                Return "Abril"
            Case Is = 5
                Return "Maio"
            Case Is = 6
                Return "Junho"
            Case Is = 7
                Return "Julho"
            Case Is = 8
                Return "Agosto"
            Case Is = 9
                Return "Setembro"
            Case Is = 10
                Return "Outubro"
            Case Is = 11
                Return "Novembro"
            Case Is = 12
                Return "Dezembro"
        End Select
    End Function
    Public Shared Function DiaSemana(ByVal dia As Integer) As String
        Select Case dia
            Case Is = 0
                DiaSemana = "Domingo"
            Case Is = 1
                DiaSemana = "Segunda"
            Case Is = 2
                DiaSemana = "Terça"
            Case Is = 3
                DiaSemana = "Quarta"
            Case Is = 4
                DiaSemana = "Quinta"
            Case Is = 5
                DiaSemana = "Sexta"
            Case Is = 6
                DiaSemana = "Sábado"
        End Select

        Return DiaSemana

    End Function
    Public Shared Function tratatexto(ByVal texto As String, Optional comaspas As Boolean = True) As String
        Try
            If comaspas Then
                Return texto.Replace("'", "''")
            Else
                Return texto.Replace("'", "")
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Shared Function hoje() As String
        Return formatadatamysql(data())
    End Function

    Public Shared Function hojecomhora() As String
        Return DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
    End Function

    Public Shared Function hojemysql() As String
        Return DateTime.Now.ToString("yyyy-MM-dd")
    End Function

    Public Shared Function hojemysqlcomhoras() As String
        Return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
    End Function
    Public Shared Function logico(valor As String) As String

        If valor = "1" OrElse valor.ToLower = "true" Then
            Return "True"
        Else
            Return "False"
        End If
    End Function
    Public Shared Function logico(valor As String, tela As Boolean) As String

        If valor = "1" OrElse valor.ToLower = "true" Then
            If tela Then
                Return "Sim"
            End If
            Return "True"
        Else
            If tela Then
                Return "Não"
            End If
            Return "False"
        End If
    End Function
    Public Shared Function ehnatal() As Boolean
        If data.Month = 12 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function separaremails(emails As String) As String()
        Dim listavazia(1) As String
        If emails.Length = 0 Then
            listavazia(0) = ""
            Return listavazia
        End If
        If emails.Substring(emails.Length - 1, 1) <> ";" Then
            emails &= ";"
        End If
        Dim conta As Integer = 0
        Dim temp As String = ""
        Dim tamanho As Integer = 0
        For x As Integer = 0 To emails.Length - 1

            If emails.Substring(x, 1) = ";" Then
                conta += 1
            End If
        Next
        Dim lista1(conta + 1) As String
        conta = 0
        For x As Integer = 0 To emails.Length - 1

            If emails.Substring(x, 1) = ";" Then

                lista1(conta) = temp
                conta += 1
                temp = ""
                Continue For
            End If

            temp &= emails.Substring(x, 1)
            tamanho += 1



        Next
        Return lista1
    End Function
    Public Shared Function validaemail(email As String) As Boolean
        If email.Contains("@") AndAlso email.Contains(".") Then
            Return True
        End If
        Return False
    End Function
    Public Shared Function encontraposicao(expressao As String, numposicao As Integer) As String
        Dim caracterfinal As Integer

        If Len(msubstring(expressao, numposicao, wctermoqtdcaracterlinha)) < wctermoqtdcaracterlinha Then
            caracterfinal = wctermoqtdcaracterlinha
            Return caracterfinal
            Exit Function
        End If
        For h As Integer = 0 To Len(expressao) '50 é um valor apenas para que ele volte poderia ser o tamanha da frase
            If msubstring(expressao, (numposicao + (wctermoqtdcaracterlinha - h)), 1) = " " Then


                caracterfinal = wctermoqtdcaracterlinha - h
                Exit For
            End If
        Next

        Return caracterfinal
        ' original
        'For h As Integer = 0 To 50 '50 é um valor apenas para que ele volte poderia ser o tamanha da frase
        '    If msubstring(txttexto, (numposicao + (caracterfinal - h)), 1) = " " Then
        '        caracterfinal -= h
        '        completaespaco = h
        '        Exit For
        '    End If
        'Next


    End Function
    Public Shared Function encontraposicao(expressao As String, numposicao As Integer, novaqtd As Integer) As String
        Dim caracterfinal As Integer
        wctermoqtdcaracterlinha = novaqtd
        If Len(msubstring(expressao, numposicao, wctermoqtdcaracterlinha)) < wctermoqtdcaracterlinha Then
            caracterfinal = wctermoqtdcaracterlinha
            Return caracterfinal
            Exit Function
        End If
        For h As Integer = 0 To Len(expressao) '50 é um valor apenas para que ele volte poderia ser o tamanha da frase
            If msubstring(expressao, (numposicao + (wctermoqtdcaracterlinha - h)), 1) = " " Then


                caracterfinal = wctermoqtdcaracterlinha - h
                Exit For
            End If
        Next
        Return caracterfinal
        ' original
        'For h As Integer = 0 To 50 '50 é um valor apenas para que ele volte poderia ser o tamanha da frase
        '    If msubstring(txttexto, (numposicao + (caracterfinal - h)), 1) = " " Then
        '        caracterfinal -= h
        '        completaespaco = h
        '        Exit For
        '    End If
        'Next

    End Function

    Public Shared Function validadata(ByVal Texto As String) As Boolean
        Dim Dia As String
        Dim Mes As String
        Dim Ano As String
        Texto = Replace(Texto, "/", "")
        Dia = Left(Texto, 2)
        Mes = Mid(Texto, 3, 2)
        Ano = Right(Texto, 4)
        Try
            Dim data As String = CDate(Mes & "/" & Dia & "/" & Ano)
            If IsDate(data) = True Then
                validadata = True
            Else
                validadata = False
            End If
        Catch ex As Exception
            validadata = False
        End Try
    End Function

    Public Shared Matricula As String
    Public Shared usuariologado As String

    Public Shared Function tratatexto(ByVal expressao As String) As String
        If expressao = "" Then Return ""
        Return Replace(Replace(expressao, "'", "''"), ",", ",")
    End Function
    Public Shared Function lerxml2(doc1 As String) As Data.DataTable
        Try

            Dim documento As New XmlDocument
            documento.Load(doc1)

            Dim tbdados As New Data.DataTable
            tbdados.Columns.Clear()
            tbdados.Rows.Clear()
            If documento.ChildNodes.Count = 0 Then
                tbdados.Columns.Add("Erro")
                tbdados.Rows.Add()
                tbdados.Rows(0)("Erro") = ("Nenhum registro localizado !")
                Return tbdados
            End If
            For Y As Integer = 0 To documento.ChildNodes(0).ChildNodes(0).ChildNodes.Count - 1
                If tbdados.Columns.Contains(documento.ChildNodes(0).ChildNodes(0).ChildNodes(Y).Name) Then
                    Exit For
                End If

                tbdados.Columns.Add(documento.ChildNodes(0).ChildNodes(0).ChildNodes(Y).Name)
            Next
            'MsgBox(documento.ChildNodes(0).ChildNodes(0).ChildNodes.Count)
            '   MsgBox(tbdados.Columns.Count)
            For x As Integer = 0 To documento.ChildNodes(0).ChildNodes.Count - 1
                tbdados.Rows.Add()

                For Y As Integer = 0 To documento.ChildNodes(0).ChildNodes(0).ChildNodes.Count - 1
                    'Debug.Print(documento.ChildNodes(0).ChildNodes(0).ChildNodes(Y).Name)
                    'Debug.Print(documento.ChildNodes(0).ChildNodes(0).ChildNodes.Count)
                    'Debug.Print(documento.ChildNodes(0).ChildNodes(0).ChildNodes(Y).InnerText)
                    tbdados.Rows(x)(documento.ChildNodes(0).ChildNodes(0).ChildNodes(Y).Name) = documento.ChildNodes(0).ChildNodes(x).ChildNodes(Y).InnerText
                Next
            Next
            'Debug.Print(tbdados.Rows.Count)
            Return tbdados

        Catch ex2 As Exception
            Threading.Thread.Sleep(10)
        End Try
    End Function
    Public Shared Function tratatextohtml(ByVal expressao As String) As String
        Return Replace(Replace(Replace(Replace(Replace(Replace(Replace(expressao, "&#231;", "ç"), "&#227;", "a"), "&#233;", "e"), "&#237;", "í"), "&#244;", "ô"), "&#245;", "õ"), " em (Clientes existentes)", "")
    End Function

    Public Shared Function tratatermo(ByVal expressao As String) As String

        expressao = Replace(Replace(Replace(Replace(Replace(expressao, "		", ""), ControlChars.Cr, ""), ControlChars.Lf, ""), " 	", ""), "	", "")

        Return expressao
    End Function

    Public Shared Function tratanumeros(ByVal expressao As String) As String
        If expressao = "" Then
            expressao = 0
        End If
        expressao = Replace(expressao, ",", ".")
        Return expressao
    End Function


    Public Shared Function trataNAN(ByVal expressao As String) As String

        If expressao = "" Then
            expressao = 0
        End If

        If expressao = "NaN (Não é um número)" Or expressao = "+Infinito" Then
            expressao = 0
        End If

        expressao = Replace(expressao, ",", ".")

        Return expressao
    End Function

    Public Shared Function trataporcentagem(ByVal expressao As String) As String
        If expressao = "" Then
            expressao = 0
        End If

        expressao = expressao.Replace(",", ".")
        Dim numerointeiro As Integer = encontrachr(expressao, ".", 1)
        Dim casadecimal As Decimal = encontrachr(expressao, ".", 2)

        If casadecimal > 50 Then
            numerointeiro += 1
        End If
        expressao = Replace(expressao, ",", ".")
        Return numerointeiro


    End Function

    Public Shared Function encontrachr(ByVal texto As String, ByVal caract As String, ByVal posicao As Integer) As String
        Dim y As Integer
        Dim posicoes As Integer
        Dim ultima As Integer
        Dim achou As Boolean
        Dim retorno As String
        posicoes = 1
        ultima = 0
        achou = False
        retorno = ""
        For y = 1 To Len(texto)

            If Mid(texto, y, 1) = caract Then
                If posicoes = posicao Then
                    retorno = Mid(texto, IIf(ultima = 0, 1, ultima), IIf(ultima = 0, y - ultima - 1, y - ultima))
                    achou = True
                    Exit For
                Else
                    achou = False
                End If
                posicoes = posicoes + 1
                ultima = y + 1
            End If
        Next
        If achou = False Then
            retorno = Mid(texto, IIf(ultima = 0, 1, ultima), IIf(ultima = 0, y - ultima - 1, y - ultima))
        End If
        Return retorno
    End Function
    Public Shared Function primeiraletra(ByVal palavra As String)
        If palavra.Length = 0 Then
            Return ""
            Exit Function
        End If
        Dim x As Integer
        Dim nova As String
        nova = ""
        nova = palavra.Substring(0, 1).ToUpper

        For x = 1 To palavra.Length - 1
            If palavra.Substring(x - 1, 1).ToUpper = " " Then
                nova = nova & palavra.Substring(x, 1).ToUpper
            Else
                nova = nova & palavra.Substring(x, 1).ToLower
            End If
        Next
        Return nova
    End Function
    Public Shared Function data(nome As Boolean) As String

        Return Year(Date.Now.Date) & zeros(Date.Now.Date.Month, 2) & zeros(Date.Now.Date.Day, 2) & "_" & zeros(Date.Now.TimeOfDay.Hours, 2) & zeros(Date.Now.TimeOfDay.Minutes, 2) & zeros(Date.Now.TimeOfDay.Seconds, 2)


    End Function
    Public Shared Function tratanomes(nome As String) As String
        Try
            Return primeiraletra(encontrachr(nome, " ", 1)) & " " & primeiraletra(encontrachr(nome, " ", 0))
        Catch ex As Exception
            Return nome
        End Try
    End Function

    Public Shared Function formatadatamysql2(ByVal data As String, Optional ByVal inverter As Boolean = False, Optional ByVal comtracos As Boolean = False, Optional ByVal comhoras As Boolean = False)
        Dim novadata As Date
        If IsDate(data) = False Then
            novadata = CDate("01/01/1900")
        Else
            novadata = CDate(data)
        End If

        If comtracos Then
            If Not inverter Then
                If Not comhoras Then
                    Return Format(novadata, "dd-MM-yyyy")
                Else
                    Return Format(novadata, "dd-MM-yyyy HH:MM:SS")
                End If

            Else
                If Not comhoras Then
                    Return Format(novadata, "yyyy-MM-dd")
                Else
                    Return Format(novadata, "yyyy-MM-dd HH:MM:SS")
                End If

            End If
        Else
            If Not inverter Then
                If Not comhoras Then
                    Return Format(novadata, "yyyy/MM/dd")
                Else
                    Return Format(novadata, "yyyy/MM/dd HH:MM:SS")
                End If
            Else
                If Not comhoras Then
                    Return Format(novadata, "dd/MM/yyyy")
                Else
                    Return Format(novadata, "dd/MM/yyyy HH:MM:SS")
                End If
            End If
        End If

    End Function


    Public Shared Function formatadatamysql(ByVal data As String, Optional ByVal inverter As Boolean = False, Optional comhoras As Boolean = False, Optional MySql As Boolean = False) As String
        Dim novadata As Date

        If IsDate(data) = False Then
            novadata = CDate("01/01/1900")
        Else
            novadata = CDate(data)
        End If

        If MySql Then
            If comhoras Then
                If Not inverter Then
                    Return Format(novadata, "yyyy-MM-dd hh:mm:ss")
                Else
                    Return Format(novadata, "dd-MM-yyyy")
                End If
            Else
                If Not inverter Then
                    Return Format(novadata, "yyyy-MM-dd")
                Else
                    Return Format(novadata, "dd-MM-yyyy")
                End If
            End If
        End If

        If comhoras Then
            If Not inverter Then
                Return Format(novadata, "yyyy/MM/dd hh:mm:ss")
            Else
                Return Format(novadata, "dd/MM/yyyy")
            End If
        Else
            If Not inverter Then
                Return Format(novadata, "yyyy/MM/dd")
            Else
                Return Format(novadata, "dd/MM/yyyy")
            End If
        End If


    End Function

    Public Shared Sub encheestados(ByVal combo As DropDownList, ByVal tipo As tipoestado)
        Dim tbestados As New Data.DataTable
        tbestados.Columns.Add("UF")
        tbestados.Columns.Add("Estado")
        tbestados.Rows.Clear()

        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "AC"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Acre"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "AL"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Alagoas"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "AP"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Amapá"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "AM"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Amazonas"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "BA"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Bahia"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "CE"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Ceará"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "DF"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Distrito Federal"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "ES"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Espírito Santo"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "GO"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Goiás"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "MA"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Maranhão"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "MG"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Minas Gerais"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "MT"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Mato Grosso"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "MS"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Mato Grosso do Sul"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "PA"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Pará"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "PB"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Paraíba"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "PE"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Pernambuco"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "PI"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Piauí"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "PR"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Paraná"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "RJ"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Rio de Janeiro"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "RN"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Rio Grande do Norte"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "RS"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Rio Grande do Sul"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "RO"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Rondônia"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "RR"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Roraima"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "SE"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Sergipe"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "SC"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Santa Catarina"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "SP"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "São Paulo"
        tbestados.Rows.Add()
        tbestados.Rows(tbestados.Rows.Count - 1)("UF") = "TO"
        tbestados.Rows(tbestados.Rows.Count - 1)("Estado") = "Tocantins"
        tbestados.Rows.Add()
        If tipo = tipoestado.sigla Then
            combo.DataSource = tbestados
            combo.DataTextField = "UF"
            combo.DataBind()
        Else
            combo.DataSource = tbestados
            combo.DataTextField = "Estado"
            combo.DataBind()
        End If


    End Sub
    Public Shared Function Conv_TXT_Data(ByVal Texto As String) As String
        Dim Dia As String
        Dim Mes As String
        Dim Ano As String
        Texto = Replace(Texto, "/", "")
        Dia = Left(Texto, 2)
        Mes = Mid(Texto, 3, 2)
        Ano = Right(Texto, 4)
        Return Ano & "/" & Mes & "/" & Dia

    End Function
    Public Shared Function conectado() As Boolean
        Try
            Return My.Computer.Network.Ping("www.globo.com")
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Shared Function moeda(ByVal valor As String, Optional ByVal tela As Boolean = False) As String
        Dim negativo As Boolean = False
        If valor = "" Then
            moeda = 0
            Exit Function
        End If
        If valor.Contains(".") Then
            Return valor.Replace("R$", "")
        End If
        'If valor.Contains("-") = True Then
        '    negativo = True
        'End If
        Dim novovalor As String = valor.Replace("R$", "").Replace(".", "")
        If tela = False Then
            Return novovalor.Replace(",", ".").Trim
        Else
            Return novovalor
        End If
    End Function

    Public Shared Function porcentagem(ByVal valor As String, Optional ByVal tela As Boolean = False) As String
        Dim negativo As Boolean = False
        If valor = "" Then
            porcentagem = 0
            Exit Function
        End If
        If valor.Contains("%") Then
            valor.Replace("%", "")
        End If
        Dim novovalor As String = valor.Replace("%", "")
        If tela = False Then
            Return novovalor.Replace(",", ".").Trim
        Else
            Return novovalor
        End If
    End Function

    Public Shared Function gerarsenhaautomatica(ByVal nome As String) As String
        Dim valor As String

        valor = Replace(Date.Now, ":", "")
        valor = Replace(valor, "/", "")
        valor = Replace(valor, " ", "")
        valor = sonumeros(valor) & Rnd(100).ToString()
        Dim novovalor As Integer

        ' novovalor = valor
        'For x As Integer = 0 To Len(valor) - 1
        '     novovalor = novovalor + Mid(valor, x + 1, 1)
        'Next

        gerarsenhaautomatica = Mid(nome, 1, 5) & novovalor

    End Function
    Public Shared Function Dataextensonovo() As String
        Dim cultura As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("pt-BR")
        Dim formato As System.Globalization.DateTimeFormatInfo = cultura.DateTimeFormat
        Dim dia As Integer = DateTime.Now.Day
        Dim ano As Integer = DateTime.Now.Year
        Dim Mes As String = cultura.TextInfo.ToTitleCase(formato.GetMonthName(DateTime.Now.Month))
        Dim DiadaSemana As String = cultura.TextInfo.ToTitleCase(formato.GetDayName(DateTime.Now.DayOfWeek))
        Dim dataporExtenso As String = DiadaSemana & ", " & dia & " de " & Mes & " de " & ano
        Return dataporExtenso
    End Function

    Public Shared Function Acessoarearestrita(ByVal tipo As String, ByVal acesso As String) As Boolean
        If tipo = "ADMINISTRADOR" Then
            tipo = 1
        End If
        If tipo = "USUARIO" Then
            tipo = 2
        End If
        If tipo = "" Then
            Return False
        End If
        If tipo = 1 Then
            Return True
        End If
        If tipo = 2 Then

            If tipo <> acesso Then
                Return False
            End If
        End If

        Return True
    End Function


    Public Shared Function Acessorestrito(ByVal tipo As String, ByVal acesso As String) As Boolean
        If tipo = "ADMINISTRADOR" Then
            Return True
        End If
        If tipo = "USUARIO" Then
            Return acesso = "USUARIO"
        End If

        Return True
    End Function

    Public Shared Function BuscaCep(ByVal cep As String) As Hashtable
        Dim ds As Data.DataSet
        Dim _resultado As String
        Try
            ds = New Data.DataSet
            ds.ReadXml("http://cep.republicavirtual.com.br/web_cep.php?cep=" + cep.Replace("-", "").Trim() + "&formato=xml")
            Dim ht As New System.Collections.Hashtable

            If Not IsNothing(ds) Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    _resultado = ds.Tables(0).Rows(0).Item("resultado").ToString()
                    Select Case _resultado
                        Case ("1")
                            ht.Add("UF", ds.Tables(0).Rows(0).Item("uf").ToString().Trim())
                            ht.Add("cidade", ds.Tables(0).Rows(0).Item("cidade").ToString().Trim())
                            ht.Add("bairro", ds.Tables(0).Rows(0).Item("bairro").ToString().Trim())
                            ht.Add("tipologradouro", ds.Tables(0).Rows(0).Item("tipo_logradouro").ToString().Trim())
                            ht.Add("logradouro", ds.Tables(0).Rows(0).Item("logradouro").ToString().Trim())
                            ht.Add("tipo", 1)

                        Case ("2")
                            ht.Add("UF", ds.Tables(0).Rows(0).Item("uf").ToString().Trim())
                            ht.Add("cidade", ds.Tables(0).Rows(0).Item("cidade").ToString().Trim())
                            ht.Add("tipo", 2)
                        Case Else
                            ht.Add("tipo", 0)
                    End Select
                End If
            End If
            Return (ht)
        Catch ex As Exception
            MsgBox("Falha ao Buscar o Cep" & vbCrLf & ex.ToString, MsgBoxStyle.Information)
            '   Throw (New Exception("Falha ao Buscar o Cep" & vbCrLf & ex.ToString))
            Return (Nothing)
        End Try
    End Function

    Public Shared Function zeros(ByVal expr As String, ByVal qtd As Integer) As String
        Dim dd As String
        Dim x As Integer
        dd = ""
        If qtd <= Len(expr) Then
            Return expr
            Exit Function
        End If
        For x = 1 To (qtd - Len(expr))
            dd = dd & "0"
        Next
        Return dd & expr

    End Function
    Public Shared Function sonumeros(ByVal valor As String, Optional pontos As Boolean = False) As String
        Dim wcnumero As String, x As Integer
        wcnumero = ""

        For x = 1 To Len(valor)
            If IsNumeric(Mid(valor, x, 1)) OrElse (pontos AndAlso (Mid(valor, x, 1) = "." OrElse Mid(valor, x, 1) = ",")) Then
                wcnumero = wcnumero & Mid(valor, x, 1)
            End If
        Next

        If wcnumero = "" Then
            wcnumero = 0
        End If
        sonumeros = (wcnumero)
    End Function
    Public Shared Function gerarnrseqcontrole() As String
        Dim var As String = sonumeros(Date.Now.ToString) & sonumeros(hora)
        Try
            var &= sonumeros(HttpContext.Current.Request.UserHostAddress)
        Catch ex As Exception

        End Try
        Return mLeft(var, 35)
    End Function
    Public Shared Function clonarlinhas(linhas As Data.DataRow()) As Data.DataTable

        If linhas.Count = 0 Then
            Return Nothing
        End If
        Dim tabela As New Data.DataTable
        For y As Integer = 0 To linhas(0).Table.Columns.Count - 1
            tabela.Columns.Add(linhas(0).Table.Columns(y).ColumnName)
        Next
        For u As Integer = 0 To linhas.Length - 1
            tabela.Rows.Add()

            For y As Integer = 0 To linhas(u).Table.Columns.Count - 1

                Try
                    tabela.Rows(u)(linhas(u).Table.Columns(y).ColumnName) = (linhas(u)(y).ToString)
                Catch ex As Exception

                End Try
            Next

        Next
        Return tabela
    End Function
    Public Shared Function mLeft(ByVal expressao As String, ByVal qtd As Integer) As String
        Return Microsoft.VisualBasic.Left(expressao, qtd)
    End Function
    Public Shared Function mRight(ByVal expressao As String, ByVal qtd As Integer) As String
        Return Microsoft.VisualBasic.Right(expressao, qtd)
    End Function
    Public Shared Function mmeio(ByVal expressao As String, ByVal inicio As Integer, ByVal qtd As Integer) As String
        Return Microsoft.VisualBasic.Mid(expressao, inicio, qtd)
    End Function
    Public Shared Function mARQUIVO(ByVal wcCAMINHO As String, Optional ByVal web As Boolean = False) As String
        Dim xx As Integer
        Dim simbolo As String = "\"
        If web Then
            simbolo = "/"
        End If
        For xx = 1 To Microsoft.VisualBasic.Len(wcCAMINHO)
            'Debug.Print(Microsoft.VisualBasic.Left(Microsoft.VisualBasic.Right(wcCAMINHO, xx + 1), 1))
            If Microsoft.VisualBasic.Left(Microsoft.VisualBasic.Right(wcCAMINHO, xx + 1), 1) = simbolo Then
                mARQUIVO = Microsoft.VisualBasic.Right(wcCAMINHO, xx)
                xx = Microsoft.VisualBasic.Len(wcCAMINHO)
                Exit For
            End If
        Next

    End Function





    Public Shared Function estadocep(ByVal cep As String, ByVal uf As String) As String


        cep = Mid(cep, 1, 5)
        If cep > 1000 And cep < 19999 Then
            uf = "SP"
        End If


        If cep > 20000 And cep < 28999 Then
            uf = "RJ"
        End If


        If cep > 30000 And cep < 39999 Then
            uf = "MG"
        End If

        If cep > 40000 And cep < 48999 Then
            uf = "BA"
        End If

        If cep > 49000 And cep < 49999 Then
            uf = "SE"
        End If

        If cep > 50000 And cep < 56999 Then
            uf = "PE"
        End If

        If cep > 57000 And cep < 57999 Then
            uf = "SE"
        End If

        If cep > 58000 And cep < 58999 Then
            uf = "PB"
        End If

        If cep > 59000 And cep < 59999 Then
            uf = "RN"
        End If
        If cep > 60000 And cep < 63999 Then
            uf = "CE"
        End If

        If cep > 64000 And cep < 64999 Then
            uf = "PI"
        End If
        If cep > 65000 And cep < 65999 Then
            uf = "MA"
        End If

        If cep > 66000 And cep < 68899 Then
            uf = "PA"
        End If

        If cep > 68900 And cep < 68999 Then
            uf = "AP"
        End If

        If cep > 69000 And cep < 69299 Then
            uf = "AM"
        End If

        If cep > 69300 And cep < 69399 Then
            uf = "RR"
        End If

        If cep > 69900 And cep < 69999 Then
            uf = "AC"
        End If

        If cep > 70000 And cep < 72799 Then
            uf = "DF"
        End If

        If cep > 72800 And cep < 72999 Then
            uf = "GO"
        End If

        If cep > 77000 And cep < 77999 Then
            uf = "TO"
        End If

        If cep > 78000 And cep < 78899 Then
            uf = "MT"
        End If

        If cep > 78900 And cep < 78999 Then
            uf = "RO"
        End If

        If cep > 79000 And cep < 79999 Then
            uf = "MS"
        End If
        If cep > 80000 And cep < 87999 Then
            uf = "PR"
        End If

        If cep > 88000 And cep < 89999 Then
            uf = "SC"
        End If


        If cep > 90000 And cep < 99999 Then
            uf = "RS"
        End If

        Return uf

    End Function

    Public Shared Function dataExtenso() As String
        Dim cultura As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("pt-BR")
        Dim formato As System.Globalization.DateTimeFormatInfo = cultura.DateTimeFormat
        Dim dia As Integer = DateTime.Now.Day
        Dim ano As Integer = DateTime.Now.Year
        Dim mes As String = cultura.TextInfo.ToTitleCase(formato.GetMonthName(DateTime.Now.Month))
        Dim DiadaSemana As String = cultura.TextInfo.ToTitleCase(formato.GetDayName(DateTime.Now.DayOfWeek))

        Dim dataporExtenso As String = DiadaSemana & ", " & dia & " de " & mes & " de " & ano
        Return dataporExtenso
    End Function
    Public Shared Function mPASTA(ByVal wcCAMINHO As String) As String
        If wcCAMINHO = "" Then Return ""
        Dim wctemp As String = ""
        Dim xx As Integer
        For xx = 1 To wcCAMINHO.Length
            If Microsoft.VisualBasic.Left(Microsoft.VisualBasic.Right(wcCAMINHO, xx + 1), 1) = "\" Then
                wctemp = (Microsoft.VisualBasic.Right(wcCAMINHO, xx).Length)
                mPASTA = Microsoft.VisualBasic.Left(wcCAMINHO, (wcCAMINHO.Length) - wctemp)
                xx = (wcCAMINHO.Length)
            End If
        Next

    End Function
    Public Shared Function validarusuario(ByVal sessao As String, grupo As String, pagina As String, funcao As String) As String
        'sessao = verificar se esta logado 
        'pagina informa a pagina que o usuario esta tentando acessar
        'função o que ele esta tendnado fazer na pagina (criar,editar,deletar)
        If sessao = Nothing Then


            Return "login"
        Else
            '   Return "Autenticacao"
        End If


        If HttpContext.Current.Session("master") = "Sim" Then
            Return "Ok"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        tb1 = tab1.conectar("select * from vwusuarios where usuario = '" & HttpContext.Current.Session("usuario") & "'")
        If tb1.Rows.Count > 0 Then
            If CDate(tb1.Rows(0)("novasenhaem").ToString) <= data() Then
                tb1.Rows(0)("alterado") = "0"
            End If



            'If tb1.Rows(0)("gravado").ToString = "0" Then
            '    Return "completardadosagente"
            'End If
        Else
            Return "login"
        End If



        tb1 = tab1.conectar("select * from vwpermissoes where descricao='" & grupo & "' and pagina='" & pagina & "'  and usuario = '" & HttpContext.Current.Session("usuario") & "'")

        If tb1.Rows.Count = 0 Then
            Return "login"
        Else
            If CDate(tb1.Rows(0)("novasenhaem").ToString) <= data() Then
                tb1.Rows(0)("alterado") = "0"
            End If
            If tb1.Rows(0)("forcarlogin").ToString = "1" OrElse tb1.Rows(0)("usuarioativo").ToString = "0" Then
                Return "login"
            End If
            If tb1.Rows(0)("alterado").ToString = "0" OrElse tb1.Rows(0)("alterado").ToString = "" Then
                Return "seguranca"
            End If
            If tb1.Rows(0)("suspenso").ToString = "1" Then
                Return "suspenso"
            End If
            If tb1.Rows(0)("ativo").ToString = True Then
                Return "Ok"
            Else
                Return "semacesso"
            End If

        End If

    End Function

    Public Shared Function caminhoservidor() As String

        Return HttpContext.Current.Server.MapPath("~")

    End Function
    Public Shared Function msubstring(expressao As String, inicio As Integer, tamanho As Integer)
        If expressao.Length < inicio Then Return ""
        Dim subexpre As String = expressao.Substring(inicio)
        Return mLeft(subexpre, tamanho)
    End Function
    Public Shared Function mLeft(ByVal expressao, ByVal qtd) As String
        Return Microsoft.VisualBasic.Left(expressao, qtd)
    End Function


    Public Shared Function tempo(horainicial As Date, horafinal As Date)

        Dim segundos As Integer = DateDiff(DateInterval.Second, horainicial, horafinal)

        Dim minutos As Decimal = segundos / 60
        Dim horas As Decimal = minutos / 60
        Dim InSeconds As Long, InMinutes As Long, InHours As Decimal
        Dim seconds As Integer, minutes As Decimal, hours As Decimal

        Dim HOURS_IN_DAY As Integer = 24
        Dim MINUTES_IN_HOUR As Integer = 60
        Dim SECONDS_IN_MINUTE As Integer = 60

        Dim r As String

        InSeconds = segundos
        seconds = InSeconds Mod (SECONDS_IN_MINUTE)
        InMinutes = InSeconds / SECONDS_IN_MINUTE
        minutes = InMinutes Mod (MINUTES_IN_HOUR)
        InHours = InMinutes / MINUTES_IN_HOUR
        hours = InHours Mod (HOURS_IN_DAY)
        If hours > 0 Then
            r = zeros(Int(hours), 2) & ":"
        Else
            r = "00:"
        End If
        If minutes > 0 And minutes < 10 Then
            r = r & "0" & minutes & ":"
        ElseIf minutes >= 10 Then
            r = r & minutes & ":"
        ElseIf minutes = 0 Then
            r = r & "00:"
        End If
        If seconds > 0 And seconds < 10 Then
            r = r & "0" & seconds
        ElseIf seconds >= 10 Then
            r = r & seconds
        ElseIf seconds = 0 Then
            r = "00"
        End If
        Return r
    End Function

    Public Shared Function gravalogin(usuario As String, Optional plataforma As String = "") As Boolean
        Try
            Dim ip As String = ""
            Dim nrseqctrl As String = gerarnrseqcontrole()
            If ip = String.Empty Or ip = Nothing Then
                ip = HttpContext.Current.Request.UserHostAddress
            End If
            Dim tblog As New Data.DataTable
            Dim tablog As New clsdbSmart
            If plataforma = "" Then
                plataforma = HttpContext.Current.Request.Browser.Platform & " - " & HttpContext.Current.Request.Browser.Browser
            End If
            tblog = tablog.incluiralterardados("insert into tbacessos (nomecliente, dataacesso, maquina, versao, usuario, horaacesso, usuariosistema, data, dtacesso, sistema, dominio, sistemaoperacional) values ('" & meucliente & "', '" & formatadatamysql(data) & "', '" & ip & "', '" & minhaversao & "', '" & usuario & "', '" & hora(True) & "', '" & usuario & "','" & formatadatamysql(data) & "', '" & formatadatamysql(data) & "','" & meuproduto & "','" & HttpContext.Current.Request.Url.OriginalString & "','" & plataforma & "')")
            Return True
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Shared Function data() As Date
        Return Date.Now.Date
    End Function

    Public Shared Function hora(Optional segundos As Boolean = False) As String

        Return IIf(segundos, mLeft(Date.Now.TimeOfDay.ToString, 5), mLeft(Date.Now.TimeOfDay.ToString, 8))

    End Function

    Public Shared Function montararvore(nrseqlote As Integer, treeArvore As TreeView) As String
        Dim tbbusca As New Data.DataTable, tbdth As New Data.DataTable
        Dim tabbusca As New clsBanco, tabdth As New clsBanco
        Dim tbaux As New Data.DataTable
        Dim tabaux As New clsBanco
        Dim nrseqcte As Integer = 0
        tbbusca = tabbusca.conectar("Select tblotes.*,tblotes_arquivos.arquivo, tblotes_arquivos.nrseq as nrseqarquivo from tblotes inner join tblotes_arquivos on tblotes.nrseq = tblotes_arquivos.nrseqlote where tblotes.nrseq = " & nrseqlote & " and tblotes_arquivos.tparquivo = 'XML CT-e' and tblotes_arquivos.ativo = true order by tblotes_arquivos.nrseq")

        With treeArvore
            .Nodes.Clear()

            For x As Integer = 0 To tbbusca.Rows.Count - 1
                Dim subguias As Integer = 0
                .Nodes.Add(New TreeNode(tbbusca.Rows(x)("arquivo").ToString, tbbusca.Rows(x)("arquivo").ToString))

                'Carrega Cte cabeçalho
                tbdth = tabdth.conectar("Select * from tbcte where nrseqarquivo = " & tbbusca.Rows(x)("nrseqarquivo").ToString)
                If tbdth.Rows.Count <> 0 Then
                    .Nodes(x).ChildNodes.Add(New TreeNode("Cte", "cte" & x))
                    nrseqcte = tbdth.Rows(0)("nrseq").ToString
                    For y As Integer = 0 To tbdth.Rows.Count - 1
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Emissão:" & tbdth.Rows(y)("dtemissao").ToString, "emissao" & y))

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Nat Operação:" & tbdth.Rows(y)("natope").ToString, "natope" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Município Inicial:" & tbdth.Rows(y)("municipioinicial").ToString, "munini" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Município Final:" & tbdth.Rows(y)("municipiofinal").ToString, "munini" & y))

                    Next
                End If

                'Carrega Cte emissor

                tbdth = tabdth.conectar("Select * from tbcte_emissor where nrseqcte = " & nrseqcte)
                If tbdth.Rows.Count <> 0 Then
                    subguias += 1
                    .Nodes(x).ChildNodes.Add(New TreeNode("Emissor", "emissor" & x))
                    For y As Integer = 0 To tbdth.Rows.Count - 1

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("CNPJ:" & tbdth.Rows(y)("cnpj").ToString, "cnpfemissor" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Insc Estadual:" & tbdth.Rows(y)("ie").ToString, "ieemissor" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Nome:" & tbdth.Rows(y)("nome").ToString, "nomeemissor" & y))



                    Next
                End If

                'Carrega Cte complemento

                tbdth = tabdth.conectar("Select * from tbcte_compl where nrseqcte = " & nrseqcte)
                If tbdth.Rows.Count <> 0 Then
                    subguias += 1
                    .Nodes(x).ChildNodes.Add(New TreeNode("Cte Complemento", "ctecomplemento" & x))
                    For y As Integer = 0 To tbdth.Rows.Count - 1

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Descrição:" & tbdth.Rows(y)("descricao").ToString, "descricao" & y))

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Observação:" & tbdth.Rows(y)("obs").ToString, "obs" & y))


                    Next
                End If

                'Carrega tomador

                tbdth = tabdth.conectar("Select * from tbcte_tomador where nrseqcte = " & nrseqcte)
                If tbdth.Rows.Count <> 0 Then
                    subguias += 1
                    .Nodes(x).ChildNodes.Add(New TreeNode("Tomador", "tomador" & x))
                    For y As Integer = 0 To tbdth.Rows.Count - 1

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("CNPJ:" & tbdth.Rows(y)("cnpj").ToString, "cnpjtomador" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Insc Estadual:" & tbdth.Rows(y)("ie").ToString, "ietomador" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Nome:" & tbdth.Rows(y)("nome").ToString, "nometomador" & y))



                    Next
                End If

                'Carrega remetente

                tbdth = tabdth.conectar("Select * from tbcte_remetente where nrseqcte = " & nrseqcte)
                If tbdth.Rows.Count <> 0 Then
                    subguias += 1
                    .Nodes(x).ChildNodes.Add(New TreeNode("Remetente", "remetente" & x))
                    For y As Integer = 0 To tbdth.Rows.Count - 1

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("CNPJ:" & tbdth.Rows(y)("cnpj").ToString, "cnpjremetente" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Insc Estadual:" & tbdth.Rows(y)("ie").ToString, "ieremetente" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Nome:" & tbdth.Rows(y)("nome").ToString, "nomeremetente" & y))


                    Next
                End If

                'Carrega destinatario

                tbdth = tabdth.conectar("Select * from tbcte_destinatario where nrseqcte = " & nrseqcte)
                If tbdth.Rows.Count <> 0 Then
                    subguias += 1
                    .Nodes(x).ChildNodes.Add(New TreeNode("Destinatários", "destinatario" & x))
                    For y As Integer = 0 To tbdth.Rows.Count - 1

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("CNPJ:" & tbdth.Rows(y)("cnpj").ToString, "cnpjdestinatario" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Insc Estadual:" & tbdth.Rows(y)("ie").ToString, "iedestinatario" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Nome:" & tbdth.Rows(y)("nome").ToString, "nomedestinatario" & y))
                        tbaux = tabaux.conectar("Select * from tbcte_destinatario_endereco where nrseqdestinatario = " & tbdth.Rows(y)("nrseq").ToString)

                        For z As Integer = 0 To tbaux.Rows.Count - 1
                            .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Endereço:" & tbaux.Rows(z)("endereco").ToString, "enderecodestinatario" & y))
                            .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Número:" & tbaux.Rows(z)("numero").ToString, "numeroenderecodestinatario" & y))
                            .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Bairro:" & tbaux.Rows(z)("bairro").ToString, "bairroenderecodestinatario" & y))
                            .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Cep:" & tbaux.Rows(z)("cep").ToString, "cependerecodestinatario" & y))
                            .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Cod Município:" & tbaux.Rows(z)("codmunicipio").ToString, "codmunenderecodestinatario" & y))
                            .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Município:" & tbaux.Rows(z)("municipio").ToString, "munienderecodestinatario" & y))
                            .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Cod País:" & tbaux.Rows(z)("codpais").ToString, "codpaisenderecodestinatario" & y))
                            .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("País:" & tbaux.Rows(z)("pais").ToString, "paisenderecodestinatario" & y))
                        Next

                    Next
                End If

                'Carrega veículos

                tbdth = tabdth.conectar("Select * from tbcte_veiculos where nrseqcte = " & nrseqcte)
                If tbdth.Rows.Count <> 0 Then
                    subguias += 1
                    .Nodes(x).ChildNodes.Add(New TreeNode("Veículos", "veiculos" & x))
                    For y As Integer = 0 To tbdth.Rows.Count - 1

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Placa:" & tbdth.Rows(y)("placa").ToString, "placaveiculo" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Renavam:" & tbdth.Rows(y)("renavam").ToString, "renavamveiculo" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("UF:" & tbdth.Rows(y)("uf").ToString, "ufveiculos" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Tara:" & tbdth.Rows(y)("tara").ToString, "taraveiculos" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Cap Kg:" & tbdth.Rows(y)("capkg").ToString, "capkgveiculos" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Cap M3:" & tbdth.Rows(y)("capm3").ToString, "capm3veiculos" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Tp Propriedade:" & tbdth.Rows(y)("tpprop").ToString, "topropveiculos" & y))

                    Next
                End If

                'Carrega motoristas

                tbdth = tabdth.conectar("Select * from tbcte_motoristas where nrseqcte = " & nrseqcte)
                If tbdth.Rows.Count <> 0 Then
                    subguias += 1
                    .Nodes(x).ChildNodes.Add(New TreeNode("Motoristas", "motoristas" & x))
                    For y As Integer = 0 To tbdth.Rows.Count - 1

                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("CPF:" & tbdth.Rows(y)("cpf").ToString, "cpfmotoristas" & y))
                        .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Nome:" & tbdth.Rows(y)("nome").ToString, "cpfmotoristas" & y))


                    Next
                End If

                'Carrega valores

                tbdth = tabdth.conectar("Select * from tbcte_cobranca where nrseqcte = " & nrseqcte)
                If tbdth.Rows.Count <> 0 Then
                    subguias += 1
                    .Nodes(x).ChildNodes.Add(New TreeNode("Cobrança", "cobranca" & x))
                    For y As Integer = 0 To tbdth.Rows.Count - 1
                        If tbdth.Rows(y)("tipodecobranca").ToString.ToLower = "fatura" Then
                            .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Faturas", "Faturas" & y))
                            tbaux = tabaux.conectar("Select * from tbcte_cobranca_faturas where nrseqfatura = " & tbdth.Rows(y)("nrseq").ToString)
                            For w As Integer = 0 To tbaux.Rows.Count - 1
                                .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Número Fatura" & tbdth.Rows(y)("numerofatura").ToString, "numFaturas" & y))
                                .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Valor de Origem" & FormatCurrency(tbdth.Rows(y)("valororigem").ToString), "valororfatura" & y))
                                .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Valor Liquidado" & FormatCurrency(tbdth.Rows(y)("valorliquidado").ToString), "valorliqfatura" & y))
                            Next
                        Else
                            .Nodes(x).ChildNodes(subguias).ChildNodes.Add(New TreeNode("Duplicatas", "Duplicata" & y))
                            tbaux = tabaux.conectar("Select * from tbcte_cobranca_duplicatas where nrseqfatura = " & tbdth.Rows(y)("nrseq").ToString)
                            For w As Integer = 0 To tbaux.Rows.Count - 1
                                .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Número Duplicata" & tbdth.Rows(y)("numeroduplicata").ToString, "numDuplicatas" & y))
                                .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Data de Vencimento" & formatadatamysql(tbdth.Rows(y)("dtvencimento").ToString, True), "dtvencduplicata" & y))
                                .Nodes(x).ChildNodes(subguias).ChildNodes(y).ChildNodes.Add(New TreeNode("Valor Duplicata" & FormatCurrency(tbdth.Rows(y)("valorduplicata").ToString), "valorduplicata" & y))
                            Next
                        End If



                    Next
                End If

            Next






        End With
    End Function
    Public Shared Function procupapedagio(cidade As String, referencia As String) As Decimal
        Dim xref As String
        If cidade Is Nothing Then Return 0
        If cidade.ToString.ToLower.Contains("guarulhos") OrElse cidade.ToString.ToLower.Contains("campinas") Then
            xref = referencia
            referencia = cidade
            cidade = xref
        End If
        Dim tbproc As New Data.DataTable
        Dim tabproc As New clsBanco
        tbproc = tabproc.conectar("select * from tbpedagios where (cidade) like ('%" & cidade & "%') and referencia = '" & referencia & "' and ativo = true")
        If tbproc.Rows.Count = 0 Then
            tbproc = tabproc.conectar("select * from tbpedagios where soundex(cidade) like soundex('%" & cidade & "%') and referencia = '" & referencia & "' and ativo = true")
            If tbproc.Rows.Count = 0 Then
                Return 0
            End If
        End If

        Return tbproc.Rows(0)("valor").ToString
    End Function
    Public Shared Function retiraCaracteresEspeciais(ByVal strAFiltrar As String, Optional removerespacosfinal As Boolean = True)

        Dim posASubstituir As Integer = 0
        Dim curPos As Integer = 0
        Dim curChar As String = ""
        Dim substituirDe As String = ""
        Dim substituirPara As String = ""
        Dim strFiltrada As String = ""

        substituirDe = "äáàãâÄÁÀÃéèêëËÉÈÊíìïÍÌÎÏóòôõöÓÒÔÕÖúùûüÚÙÛÜçÇÂ'�ºª~`´'"
        substituirPara = "aaaaaAAAAeeeeEEEEiiiIIIIoooooOOOOOuuuuUUUUcCA       "

        For curPos = 1 To Len(strAFiltrar) 'ciclo na string a filtrar...
            curChar = Mid(strAFiltrar, curPos, 1) 'pega em cada caracter da string
            posASubstituir = InStr(substituirDe, curChar) 'verifica se está na string de caracteres a substituir
            If posASubstituir Then  ' se estiver,
                strFiltrada = strFiltrada & Mid(substituirPara, posASubstituir, 1) 'entra na string filtrada o equivalente não-acentuado
            Else 'se não estiver
                strFiltrada = strFiltrada & curChar 'entra na string filtrada tal como está
            End If
        Next curPos
        For x As Integer = 1 To 31
            strFiltrada = strFiltrada.Replace(Chr(x), "")
        Next
        For x As Integer = 126 To 254
            strFiltrada = strFiltrada.Replace(Chr(x), "")
        Next
        strFiltrada = strFiltrada.Replace("@", "").Replace(".", "")

        If removerespacosfinal Then
            Return strFiltrada.Trim
        Else
            Return strFiltrada
        End If

    End Function
    Public Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New System.IO.StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function
    Public Shared Function leremailhtml(mensagem As String) As String

        Dim xprox As Integer = 0
        Dim xfinal As Integer = 0
        Dim nuncaachou As Boolean = True
        For x As Integer = 0 To mensagem.Length - 1
            If mensagem.Substring(x, 1) = "<" Then
                If mensagem.ToLower.Substring(x + 1, 4).Contains("html") Then
                    xprox = x
                    nuncaachou = False
                End If

            End If
            If mensagem.Substring(x, 1) = "/" Then
                If mensagem.ToLower.Substring(x + 1, 5).Contains("html>") Then
                    xfinal = x + 6
                    nuncaachou = False
                End If
            End If
        Next
        If nuncaachou Then
            Return mensagem
        Else
            Return mensagem.Substring(xprox, xfinal - xprox)
        End If



    End Function
    Public Shared Sub baixaremails()
        Try


            Dim tbteste As New Data.DataTable
            Dim tabteste As New clsBanco


            Dim email As New clsRecebeEMail
            Dim i As Integer
            Dim msg As clsRecebeEMail.Message
            Dim msgstring As String
            Dim messages1 As Integer


            messages1 = email.connect

            If messages1 = -1 Then

                Exit Sub
            End If
            Dim originalCaption As String = ""
            Dim linhas As Integer = 0

            ' lblteste.Text = "Qtd mensagens: " & messages1
            For i = messages1 To 1 Step -1

                messages1.ToString()

                msgstring = email.GetMessage(i)
                Dim newString As String = leremailhtml(msgstring.Replace(vbCr, "").Replace(vbLf, ""))
                msg = email.CreateFromText(msgstring)

                If Not msg._From.Contains("VPS.mailer@accenture.com") Then
                    Continue For
                End If

                tbteste = tabteste.conectar("Select * from tbemails where remetente = '" & msg._From.Trim & "' and receive = '" & msg._Received.Trim & msg._Date.ToString & "'")
                If tbteste.Rows.Count = 0 Then
                    tbteste = tabteste.IncluirAlterarDados("insert into tbemails (data, assunto, mensagem, remetente, nrmensagem, receive) values ('" & formatadatamysql(msg._Date) & "','" & msg._Subject & "','" & tratatexto(newString) & "','" & msg._From.Trim & "'," & i & ",'" & msg._Received.Trim & msg._Date.ToString & "')")

                End If



                linhas += 1

                '    msgitem.Text = msg._From
                '    msgitem.SubItems.Add(msg._Subject)
                '  msgitem.SubItems.Add(msg._Date)
                ' ListView1.Items.Add(msgitem)
                '  TextBox1.AppendText(msg._Body & vbCrLf)
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Shared Function valida(usuario As String, pagina As String, ehmaster As String) As String
        If usuario Is Nothing Then
            Return "login"
        End If
        Dim tbpagina As New Data.DataTable
        Dim tabpagina As New clsBanco
        If ehmaster.ToLower = "sim" Then
            Return "ok"
        End If
        tbpagina = tabpagina.conectar("SELECT tbpermissoes.*,tbpermissoesdth.pagina  FROM tbpermissoes inner join tbpermissoesdth on tbpermissoes.nrseq = tbpermissoesdth.nrseqpermissao inner join tbusuarios on tbusuarios.permissao = tbpermissoes.descricao where tbpermissoesdth.pagina = '" & pagina & "' and tbpermissoes.ativo = true and tbpermissoesdth.ativo = true and tbusuarios.usuario = '" & usuario & "'")
        If tbpagina.Rows.Count = 0 Then
            HttpContext.Current.Response.Redirect("erro.aspx?mensagem=Descupe! Você não tem acesso à essa página!{novalinha}Entre em contato com o administrador do sistema para solicitar o acesso!")
            Return "erro"
        End If
        Return "ok"
    End Function
    Public Shared Function validasenha(senha As String) As Boolean
        If senha.Length < 4 Then
            Return False
        End If
        If sonumeros(senha).Length = senha.Length Then
            Return False
        End If
        Return True
    End Function


    Public Shared Function atualizarcte(nrlote As Integer) As Boolean

        Dim resultado As String = ""


        Dim tbarq As New Data.DataTable
        Dim tabarq As New clsBanco
        Dim tbgravar As New Data.DataTable
        Dim tabgravar As New clsBanco


        tbarq = tabarq.conectar("select * from tblotes_arquivos where nrseqlote = " & nrlote & " and tparquivo = 'XML CT-e' and ativo = true")

        For x As Integer = 0 To tbarq.Rows.Count - 1
            tbgravar = tabgravar.conectar("Select * from tbcte where nrseqarquivo = " & tbarq.Rows(x)("nrseq").ToString)
            If tbgravar.Rows.Count = 0 Then
                Continue For
            End If


            HttpContext.Current.Session("cte") = tbgravar.Rows(0)("nrseq").ToString
            Dim abrir As New XmlDocument

            abrir.Load(HttpContext.Current.Server.MapPath("~\arquivoslotes\xml\" & tbarq.Rows(x)("arquivo").ToString))

            With abrir.ChildNodes(1).ChildNodes(0).ChildNodes(0)
                For z As Integer = 0 To .ChildNodes.Count - 1



                    Select Case .ChildNodes(z).Name.ToLower
                        Case Is = "ide"
                            Dim wcnrseqctrlcte As String = gerarnrseqcontrole() & nrlote & "_ " & tbarq.Rows(x)("nrseq").ToString & x


                            For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                                If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("toma") Then


                                    HttpContext.Current.Session("tomador") = "nao"
                                    Dim wcnrseqctrl As String = gerarnrseqcontrole() & "tomador" & HttpContext.Current.Session("cte")


                                    For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1

                                        Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                            Case Is = "cnpj"
                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            Case Is = "ie"
                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            Case Is = "xnome"
                                                HttpContext.Current.Session("tomador") = "sim"
                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                        End Select

                                    Next
                                End If
                                Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                    Case Is = "toma4"

                                    Case Is = "nct"
                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte set nrcte = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                    Case Is = "natop"
                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte set natope = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)

                                    Case Is = "dhemi"
                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte set dtemissao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                    Case Is = "xmunini"
                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte set municipioinicial = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                    Case Is = "xmunfim"
                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte set municipiofinal = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                End Select

                            Next
                            'Case Is = "compl"


                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1
                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "xemi"

                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_compl set descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "xobs"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_compl set obs = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)

                            '        End Select

                            '    Next
                            'Case Is = "emit"

                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "cnpj"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "ie"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "xnome"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '        End Select

                            '    Next

                            'Case Is = "rem"

                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "cnpj"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "ei"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "xnome"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '        End Select

                            '    Next

                            'Case Is = "dest"
                            '    Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & HttpContext.Current.Session("cte").ToString

                            '    tbgravar = tabgravar.conectar("select * from tbcte_destinatario where nrseqctrl = '" & wcnrseqctrl_desc & "'")

                            '    Session("ctedest") = tbgravar.Rows(0)("nrseq").ToString
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1


                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "cnpj"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                If Session("tomador") = "nao" Then
                            '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set cnpj = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & Session("cte").ToString)
                            '                End If
                            '            Case Is = "ie"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                If Session("tomador") = "nao" Then
                            '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set ie = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & Session("cte").ToString)
                            '                End If
                            '            Case Is = "xnome"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                If Session("tomador") = "nao" Then
                            '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set nome = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & Session("cte").ToString)
                            '                End If
                            '            Case Is = "enderdest"
                            '                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_destinatario_endereco (nrseqcte, nrseqdestinatario) values ('" & Session("cte").ToString & "'," & Session("ctedest") & ")")
                            '                For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                            '                    Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                            '                        Case Is = "xlgr"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set endereco = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                        Case Is = "nro"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set numero = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                        Case Is = "xbairro"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set bairro = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                        Case Is = "cep"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set cep = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                        Case Is = "cmun"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set codmunicipio = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                        Case Is = "xmun"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set municipio = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                        Case Is = "uf"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set uf = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                        Case Is = "xpais"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set pais = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                        Case Is = "cpais"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set codpais = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & Session("cte").ToString)
                            '                    End Select

                            '                Next
                            '        End Select

                            '    Next
                            'Case Is = "imp"
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1
                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "infAdFisco".ToLower

                            '                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_fisco (nrseqcte, descricao) values ('" & Session("cte").ToString & "','" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText.ToString) & "')")

                            '        End Select
                            '    Next
                            'Case Is = "vprest"
                            '    tbgravar = tabgravar.IncluirAlterarDados("update tbcte set valor = " & moeda(.ChildNodes(z).ChildNodes(0).InnerText) & " where nrseq = " & Session("cte").ToString)
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "comp"
                            '                Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & Session("cte").ToString
                            '                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_descricao (nrseqcte, nrseqctrl) values ('" & Session("cte").ToString & "','" & wcnrseqctrl_desc & "')")
                            '                For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                            '                    Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                            '                        Case Is = "xnome"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_descricao set descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                            '                        Case Is = "vcomp"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_descricao set valor = " & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & " where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                            '                    End Select
                            '                Next


                            '        End Select

                            '    Next

                            'Case Is = "infctenorm"
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1


                            '        If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("cobr") Then
                            '            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1

                            '                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                            '                    Case Is = "fat"
                            '                        Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & Session("cte").ToString & "fat"
                            '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca (nrseqcte, tipodecobranca, nrseqctrl) values (" & Session("cte") & ",'Fatura','" & wcnrseqctrl_desc & "')")
                            '                        tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                            '                        Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                            '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca_faturas (nrseqcte, nrseqcobranca, nrseqctrl) values (" & Session("cte") & "," & Session("cobranca") & ",'" & wcnrseqctrl_desc & "')")
                            '                        tbgravar = tabgravar.conectar("Select * from tbcte_cobranca_faturas where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                            '                        Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                            '                        For i As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1

                            '                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).Name.ToLower
                            '                                Case Is = "nfat"
                            '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set numerofatura = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString & "' where nrseq = " & Session("cobranca"))
                            '                                Case Is = "vorig"
                            '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set valororigem = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & Session("cobranca"))
                            '                                Case Is = "vliq"
                            '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set valorliquido = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & Session("cobranca"))
                            '                            End Select
                            '                        Next

                            '                    Case Is = "dup"
                            '                        Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & Session("cte").ToString & "fat"
                            '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca (nrseqcte, tipodecobranca, nrseqctrl) values (" & Session("cte") & ",'Fatura','" & wcnrseqctrl_desc & "')")
                            '                        tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                            '                        Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                            '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca_duplicatas (nrseqcte, nrseqcobranca, nrseqctrl) values (" & Session("cte") & "," & Session("cobranca") & ",'" & wcnrseqctrl_desc & "')")
                            '                        tbgravar = tabgravar.conectar("Select * from tbcte_cobranca_duplicatas where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                            '                        Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                            '                        For i As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1

                            '                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).Name.ToLower
                            '                                Case Is = "ndup"
                            '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set numeroduplicata = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString & "' where nrseq = " & Session("cobranca"))
                            '                                Case Is = "dvenc"
                            '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set dtvencimento = '" & formatadatamysql(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & Session("cobranca"))
                            '                                Case Is = "vdup"
                            '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set valorduplicata = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & Session("cobranca"))
                            '                            End Select
                            '                        Next
                            '                End Select

                            '            Next
                            '        End If



                            '        If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("infmodal") Then
                            '            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                            '                If .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower.Contains("rodo") Then
                            '                    For k As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1
                            '                        If .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower = "moto" Then
                            '                            Dim wcnrseqctrlveic As String = gerarnrseqcontrole() & k & Session("cte").ToString

                            '                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_motoristas (nrseqcte,nrseqctrl) values ('" & Session("cte").ToString & "','" & wcnrseqctrlveic & "')")

                            '                            For k1 As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes.Count - 1
                            '                                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).Name.ToLower
                            '                                    Case Is = "xnome"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_motoristas set nome = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "cpf"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_motoristas set cpf = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                End Select
                            '                            Next
                            '                        End If
                            '                        If .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower = "veic" Then
                            '                            Dim wcnrseqctrlveic As String = gerarnrseqcontrole() & k & Session("cte").ToString
                            '                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_veiculos (nrseqcte,nrseqctrl) values ('" & Session("cte").ToString & "','" & wcnrseqctrlveic & "')")
                            '                            For k1 As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes.Count - 1
                            '                                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).Name.ToLower
                            '                                    Case Is = "renavam"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set renavam = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "placa"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set placa = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "tara"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tara = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "capkg"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set capkg = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "capm3"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set capm3 = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "tpprop"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpprop = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "tpveic"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpveic = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "tpprop"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpprop = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "tpcar"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpcar = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                    Case Is = "uf"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set uf = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                            '                                End Select
                            '                            Next
                            '                        End If
                            '                    Next
                            '                End If
                            '            Next
                            '        End If

                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower

                            '            Case Is = "seg"
                            '                Dim wcnrseqseg As String = gerarnrseqcontrole() & y & Session("cte")
                            '                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_seguradora (nrseqcte, nrseqctrl) values ('" & Session("cte").ToString & "','" & wcnrseqseg & "')")
                            '                tbgravar = tabgravar.conectar("Select * from tbcte_seguradora where nrseqctrl = '" & wcnrseqseg & "'")
                            '                Session("nrseqseguro") = tbgravar.Rows(0)("nrseq").ToString
                            '                For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                            '                    Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                            '                        Case Is = "xseg"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_seguradora set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseq =  " & Session("nrseqseguro").ToString)
                            '                        Case Is = "napol"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_seguradora set apolices = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseq =  " & Session("nrseqseguro").ToString)
                            '                    End Select
                            '                Next

                            '            Case Is = "infcarga"
                            '                Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & Session("cte").ToString
                            '                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_itens (nrseqcte, nrseqctrl) values ('" & Session("cte").ToString & "','" & wcnrseqctrl_desc & "')")
                            '                tbgravar = tabgravar.conectar("select * from tbcte_itens where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                            '                Session("ctecargadth") = tbgravar.Rows(0)("nrseq").ToString
                            '                For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                            '                    Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                            '                        Case Is = "vcarga"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set codcarga = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                            '                        Case Is = "propred"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set denominacao = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                            '                        Case Is = "xoutcat"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set categoria = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                            '                        Case Is = "infq"
                            '                            Dim wcnrseqctrl_dthitem As String = gerarnrseqcontrole() & y.ToString & w.ToString & Session("ctecargadth").ToString
                            '                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_itens_dth (nrseqcteitem, nrseqctrl) values ('" & Session("ctecargadth").ToString & "','" & wcnrseqctrl_dthitem & "')")
                            '                            tbgravar = tabgravar.conectar("select * from tbcte_itens_dth where nrseqctrl = '" & wcnrseqctrl_dthitem & "'")
                            '                            Session("registrodth") = tbgravar.Rows(0)("nrseq").ToString
                            '                            For k As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1
                            '                                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower
                            '                                    Case Is = "cunid"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set unidade = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & Session("registrodth"))
                            '                                    Case Is = "tpmed"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set tipomedida = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & Session("registrodth"))
                            '                                    Case Is = "qcarga"
                            '                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set carga = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & Session("registrodth"))
                            '                                End Select

                            '                            Next
                            '                    End Select


                            '                Next


                            '        End Select

                            '    Next
                    End Select

                Next
            End With

            tbgravar = tabgravar.conectar("update tbcte set ativo = true where nrseq = '" & HttpContext.Current.Session("cte") & "'")
        Next







        Return True

    End Function
    Public Shared Function processarlotegravarcte(nrlote As Integer, Optional existe As Boolean = False) As Boolean
        Try


            Dim resultado As String = ""


            Dim tbarq As New Data.DataTable
            Dim tabarq As New clsBanco
            Dim tbgravar As New Data.DataTable
            Dim tabgravar As New clsBanco


            tbarq = tabarq.conectar("select * from tblotes_arquivos where nrseqlote = " & nrlote & " and tparquivo = 'XML CT-e' and ativo = true")

            For x As Integer = 0 To tbarq.Rows.Count - 1
                'tbgravar = tabgravar.conectar("Select * from tbcte where nrseqarquivo = " & tbarq.Rows(x)("nrseq").ToString)
                'If tbgravar.Rows.Count <> 0 Then
                '    Continue For
                'End If
                Dim abrir As New XmlDocument

                abrir.Load(HttpContext.Current.Server.MapPath("~\arquivoslotes\xml\" & tbarq.Rows(x)("arquivo").ToString))
                Dim varinicial As Integer = 1
                Try
tentadenovo:


                    If varinicial < 0 Then
                        varinicial = 1
                        GoTo terminou
                    End If
                    Dim contatudo As Integer = abrir.ChildNodes(varinicial).ChildNodes(0).ChildNodes(0).ChildNodes.Count - 1
                Catch exvalida As Exception
                    varinicial -= 1
                    GoTo tentadenovo
                End Try
terminou:



                With abrir.ChildNodes(varinicial).ChildNodes(0).ChildNodes(0)
                    For z As Integer = 0 To .ChildNodes.Count - 1



                        Select Case .ChildNodes(z).Name.ToLower
                            Case Is = "ide"
                                Dim wcnrseqctrlcte As String = gerarnrseqcontrole() & nrlote & "_ " & tbarq.Rows(x)("nrseq").ToString & x

                                tbgravar = tabgravar.IncluirAlterarDados("select * from  tbcte  where nrseqarquivo = " & tbarq.Rows(x)("nrseq").ToString & "")
                                If tbgravar.Rows.Count = 0 Then
                                    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte (nrseqarquivo, dtcad, usercad, ativo, nrseqctrl) values (" & tbarq.Rows(x)("nrseq").ToString & ",'" & formatadatamysql(data) & "','" & HttpContext.Current.Session("usuario").ToString & "',false, '" & wcnrseqctrlcte & "')")
                                    tbgravar = tabgravar.conectar("select * from tbcte where nrseqctrl = '" & wcnrseqctrlcte & "'")

                                End If

                                HttpContext.Current.Session("cte") = tbgravar.Rows(0)("nrseq").ToString

                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                                    If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("toma") Then


                                        HttpContext.Current.Session("tomador") = "nao"
                                        Dim wcnrseqctrl As String = gerarnrseqcontrole() & "tomador" & HttpContext.Current.Session("cte")
                                        tbgravar = tabgravar.conectar("select * from tbcte_tomador where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                        If tbgravar.Rows.Count = 0 Then
                                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_tomador (nrseqcte, wcnrseqctrl ) values (" & HttpContext.Current.Session("cte").ToString & ",'" & wcnrseqctrl & "')")
                                        End If

                                        For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1

                                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                                Case Is = "cnpj"
                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                Case Is = "ie"
                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                Case Is = "xnome"
                                                    HttpContext.Current.Session("tomador") = "sim"
                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            End Select

                                        Next
                                    End If
                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                        'Case Is = "toma4"

                                        Case Is = "nct"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set nrcte = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                        Case Is = "natop"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set natope = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)

                                        Case Is = "dhemi"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set dtemissao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                        Case Is = "xmunini"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set municipioinicial = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                        Case Is = "xmunfim"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set municipiofinal = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                    End Select

                                Next
                            Case Is = "compl"
                                tbgravar = tabgravar.conectar("select * from tbcte_compl where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                If tbgravar.Rows.Count = 0 Then
                                    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_compl (nrseqcte) values ('" & HttpContext.Current.Session("cte").ToString & "')")
                                End If
                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1
                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                        Case Is = "xemi"

                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_compl set descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                        Case Is = "xobs"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_compl set obs = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)

                                    End Select

                                Next
                            Case Is = "emit"
                                tbgravar = tabgravar.conectar("select * from tbcte_emissor where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                If tbgravar.Rows.Count = 0 Then
                                    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_emissor (nrseqcte) values ('" & HttpContext.Current.Session("cte").ToString & "')")
                                End If
                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                        Case Is = "cnpj"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                        Case Is = "ie"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                        Case Is = "xnome"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                    End Select

                                Next

                            Case Is = "rem"
                                tbgravar = tabgravar.conectar("select * from tbcte_remetente where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                If tbgravar.Rows.Count = 0 Then
                                    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_remetente (nrseqcte) values ('" & HttpContext.Current.Session("cte").ToString & "')")
                                End If
                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                        Case Is = "cnpj"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                        Case Is = "ei"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                        Case Is = "xnome"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                    End Select

                                Next

                            Case Is = "dest"
                                tbgravar = tabgravar.conectar("select * from tbcte_destinatario where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                If tbgravar.Rows.Count = 0 Then
                                    Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & HttpContext.Current.Session("cte").ToString
                                    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_destinatario (nrseqcte, nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "')")
                                    tbgravar = tabgravar.conectar("select * from tbcte_destinatario where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                End If


                                HttpContext.Current.Session("ctedest") = tbgravar.Rows(0)("nrseq").ToString
                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1


                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                        Case Is = "cnpj"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            If HttpContext.Current.Session("tomador") = "nao" Then
                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set cnpj = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            End If
                                        Case Is = "ie"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            If HttpContext.Current.Session("tomador") = "nao" Then
                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set ie = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            End If
                                        Case Is = "xnome"
                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            If HttpContext.Current.Session("tomador") = "nao" Then
                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set nome = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            End If
                                        Case Is = "enderdest"
                                            tbgravar = tabgravar.conectar("select * from tbcte_destinatario_endereco where nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and nrseqdestinatario = " & HttpContext.Current.Session("ctedest"))
                                            If tbgravar.Rows.Count = 0 Then
                                                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_destinatario_endereco (nrseqcte, nrseqdestinatario) values ('" & HttpContext.Current.Session("cte").ToString & "'," & HttpContext.Current.Session("ctedest") & ")")
                                            End If

                                            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                                    Case Is = "xlgr"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set endereco = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                    Case Is = "nro"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set numero = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                    Case Is = "xbairro"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set bairro = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                    Case Is = "cep"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set cep = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                    Case Is = "cmun"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set codmunicipio = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                    Case Is = "xmun"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set municipio = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                    Case Is = "uf"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set uf = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                    Case Is = "xpais"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set pais = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                    Case Is = "cpais"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set codpais = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                End Select

                                            Next
                                    End Select

                                Next
                            Case Is = "imp"
                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1
                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                        Case Is = "infAdFisco".ToLower
                                            tbgravar = tabgravar.conectar("select * from tbcte_fisco where nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText.ToString) & "'")
                                            If tbgravar.Rows.Count = 0 Then
                                                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_fisco (nrseqcte, descricao) values ('" & HttpContext.Current.Session("cte").ToString & "','" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText.ToString) & "')")
                                            End If


                                    End Select
                                Next
                            Case Is = "vprest"
                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte set valor = " & moeda(.ChildNodes(z).ChildNodes(0).InnerText) & " where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                        Case Is = "comp"



                                            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                                    Case Is = "xnome"
                                                        Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & HttpContext.Current.Session("cte").ToString
                                                        tbgravar = tabgravar.conectar("select * from tbcte_descricao where nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "'")
                                                        If tbgravar.Rows.Count = 0 Then
                                                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_descricao (nrseqcte, nrseqctrl, descricao ) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "', '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "')")
                                                        Else
                                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_descricao set descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where  nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "'")
                                                        End If

                                                    Case Is = "vcomp"
                                                        ' procura a descricao anterior (indice anterior x- 1) e grava o valor
                                                        Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & HttpContext.Current.Session("cte").ToString
                                                        tbgravar = tabgravar.conectar("select * from tbcte_descricao where nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w - 1).InnerText) & "'")
                                                        If tbgravar.Rows.Count = 0 Then
                                                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_descricao (nrseqcte, nrseqctrl, descricao) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "','" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w - 1).InnerText) & "')")
                                                        Else
                                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_descricao set valor = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where  nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w - 1).InnerText) & "'")
                                                        End If


                                                        'Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & HttpContext.Current.Session("cte").ToString
                                                        'tbgravar = tabgravar.conectar("select * from tbcte_descricao where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                        'If tbgravar.Rows.Count = 0 Then
                                                        '    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_descricao (nrseqcte, nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "')")
                                                        'Else
                                                        '    wcnrseqctrl_desc = tbgravar.Rows(0)("nrseqctrl").ToString
                                                        'End If
                                                        'tbgravar = tabgravar.IncluirAlterarDados("update tbcte_descricao set valor = " & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & " where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                                                End Select
                                            Next


                                    End Select

                                Next

                            Case Is = "infctenorm"
                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1


                                    If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("cobr") Then
                                        For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1

                                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                                Case Is = "fat"
                                                    Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & HttpContext.Current.Session("cte").ToString & "fat"
                                                    tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqcte = " & HttpContext.Current.Session("cte") & " and tipodecobranca = 'fatura'")
                                                    If tbgravar.Rows.Count = 0 Then
                                                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca (nrseqcte, tipodecobranca, nrseqctrl) values (" & HttpContext.Current.Session("cte") & ",'Fatura','" & wcnrseqctrl_desc & "')")
                                                        tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                                        HttpContext.Current.Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                                                    Else
                                                        HttpContext.Current.Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                                                    End If
                                                    tbgravar = tabgravar.conectar("select * from tbcte_cobranca_faturas where nrseqcte = " & HttpContext.Current.Session("cte") & " and tipodecobranca = 'fatura' and nrseqcobranca = " & HttpContext.Current.Session("cobranca"))
                                                    If tbgravar.Rows.Count = 0 Then
                                                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca_faturas (nrseqcte, nrseqcobranca, nrseqctrl) values (" & HttpContext.Current.Session("cte") & "," & HttpContext.Current.Session("cobranca") & ",'" & wcnrseqctrl_desc & "')")
                                                        tbgravar = tabgravar.conectar("Select * from tbcte_cobranca_faturas where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                                        HttpContext.Current.Session("nrseqduplicata") = tbgravar.Rows(0)("nrseq").ToString
                                                    Else
                                                        HttpContext.Current.Session("nrseqduplicata") = tbgravar.Rows(0)("nrseq").ToString
                                                    End If

                                                    For i As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1

                                                        Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).Name.ToLower
                                                            Case Is = "nfat"
                                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set numerofatura = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                                            Case Is = "vorig"
                                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set valororigem = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                                            Case Is = "vliq"
                                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set valorliquido = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                                        End Select
                                                    Next

                                                Case Is = "dup"
                                                    Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & HttpContext.Current.Session("cte").ToString & "fat"
                                                    tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqcte = " & HttpContext.Current.Session("cte") & " and tipodecobranca = 'Duplicata'")
                                                    If tbgravar.Rows.Count = 0 Then
                                                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca (nrseqcte, tipodecobranca, nrseqctrl) values (" & HttpContext.Current.Session("cte") & ",'Duplicata','" & wcnrseqctrl_desc & "')")
                                                        tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                                        HttpContext.Current.Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString

                                                    Else
                                                        HttpContext.Current.Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                                                    End If
                                                    tbgravar = tabgravar.conectar("select * from tbcte_cobranca_duplicatas where nrseqcte = " & HttpContext.Current.Session("cobranca") & " and nrseqcobranca = " & HttpContext.Current.Session("cobranca"))
                                                    If tbgravar.Rows.Count = 0 Then
                                                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca_duplicatas (nrseqcte, nrseqcobranca, nrseqctrl) values (" & HttpContext.Current.Session("cte") & "," & HttpContext.Current.Session("cobranca") & ",'" & wcnrseqctrl_desc & "')")
                                                        tbgravar = tabgravar.conectar("Select * from tbcte_cobranca_duplicatas where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                                        HttpContext.Current.Session("nrseqduplicata") = tbgravar.Rows(0)("nrseq").ToString
                                                    Else
                                                        HttpContext.Current.Session("nrseqduplicata") = tbgravar.Rows(0)("nrseq").ToString
                                                    End If
                                                    For i As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1

                                                        Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).Name.ToLower
                                                            Case Is = "ndup"
                                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set numeroduplicata = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                                            Case Is = "dvenc"
                                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set dtvencimento = '" & formatadatamysql(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                                            Case Is = "vdup"
                                                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set valorduplicata = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                                        End Select
                                                    Next
                                            End Select

                                        Next
                                    End If



                                    If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("infmodal") Then
                                        For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                            If .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower.Contains("rodo") Then
                                                For k As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1
                                                    If .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower = "moto" Then
                                                        Dim wcnrseqctrlveic As String = gerarnrseqcontrole() & k & HttpContext.Current.Session("cte").ToString
                                                        tbgravar = tabgravar.IncluirAlterarDados("select * from tbcte_motoristas where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                        If tbgravar.Rows.Count = 0 Then
                                                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_motoristas (nrseqcte,nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrlveic & "')")
                                                        Else
                                                            wcnrseqctrlveic = tbgravar.Rows(0)("nrseqctrl").ToString
                                                        End If

                                                        For k1 As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes.Count - 1
                                                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).Name.ToLower
                                                                Case Is = "xnome"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_motoristas set nome = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "cpf"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_motoristas set cpf = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                            End Select
                                                        Next
                                                    End If
                                                    If .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower = "veic" Then
                                                        Dim wcnrseqctrlveic As String = gerarnrseqcontrole() & k & HttpContext.Current.Session("cte").ToString
                                                        tbgravar = tabgravar.conectar("select * from tbcte_veiculos where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                        If tbgravar.Rows.Count = 0 Then
                                                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_veiculos (nrseqcte,nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrlveic & "')")
                                                        Else
                                                            wcnrseqctrlveic = tbgravar.Rows("nrseqctrl").ToString
                                                        End If


                                                        For k1 As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes.Count - 1
                                                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).Name.ToLower
                                                                Case Is = "renavam"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set renavam = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "placa"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set placa = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "tara"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tara = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "capkg"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set capkg = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "capm3"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set capm3 = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "tpprop"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpprop = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "tpveic"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpveic = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "tpprop"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpprop = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "tpcar"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpcar = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                                Case Is = "uf"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set uf = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                                            End Select
                                                        Next
                                                    End If
                                                Next
                                            End If
                                        Next
                                    End If

                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower

                                        Case Is = "seg"
                                            Dim wcnrseqseg As String = gerarnrseqcontrole() & y & HttpContext.Current.Session("cte")
                                            tbgravar = tabgravar.conectar("select * from tbcte_seguradora where nrseqcte = " & HttpContext.Current.Session("cte"))
                                            If tbgravar.Rows.Count = 0 Then
                                                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_seguradora (nrseqcte, nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqseg & "')")
                                                tbgravar = tabgravar.conectar("Select * from tbcte_seguradora where nrseqctrl = '" & wcnrseqseg & "'")
                                                HttpContext.Current.Session("nrseqseguro") = tbgravar.Rows(0)("nrseq").ToString
                                            Else
                                                HttpContext.Current.Session("nrseqseguro") = tbgravar.Rows(0)("nrseq").ToString
                                            End If



                                            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                                    Case Is = "xseg"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_seguradora set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseq =  " & HttpContext.Current.Session("nrseqseguro").ToString)
                                                    Case Is = "napol"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_seguradora set apolices = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseq =  " & HttpContext.Current.Session("nrseqseguro").ToString)
                                                End Select
                                            Next

                                        Case Is = "infcarga"
                                            Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & HttpContext.Current.Session("cte").ToString
                                            tbgravar = tabgravar.conectar("select * from tbcte_itens where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                            If tbgravar.Rows.Count = 0 Then
                                                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_itens (nrseqcte, nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "')")
                                                tbgravar = tabgravar.conectar("select * from tbcte_itens where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                                HttpContext.Current.Session("ctecargadth") = tbgravar.Rows(0)("nrseq").ToString
                                            Else
                                                HttpContext.Current.Session("ctecargadth") = tbgravar.Rows(0)("nrseq").ToString
                                                wcnrseqctrl_desc = tbgravar.Rows(0)("nrseqctrl").ToString
                                            End If

                                            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                                    Case Is = "vcarga"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set codcarga = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                                                    Case Is = "propred"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set denominacao = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                                                    Case Is = "xoutcat"
                                                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set categoria = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                                                    Case Is = "infq"
                                                        Dim wcnrseqctrl_dthitem As String = gerarnrseqcontrole() & y.ToString & w.ToString & HttpContext.Current.Session("ctecargadth").ToString
                                                        tbgravar = tabgravar.IncluirAlterarDados("delete from tbcte_itens_dth where nrseqcteitem = " & HttpContext.Current.Session("ctecargadth").ToString)
                                                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_itens_dth (nrseqcteitem, nrseqctrl) values ('" & HttpContext.Current.Session("ctecargadth").ToString & "','" & wcnrseqctrl_dthitem & "')")
                                                        tbgravar = tabgravar.conectar("select * from tbcte_itens_dth where nrseqctrl = '" & wcnrseqctrl_dthitem & "'")
                                                        HttpContext.Current.Session("registrodth") = tbgravar.Rows(0)("nrseq").ToString
                                                        For k As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1
                                                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower
                                                                Case Is = "cunid"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set unidade = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & HttpContext.Current.Session("registrodth"))
                                                                Case Is = "tpmed"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set tipomedida = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & HttpContext.Current.Session("registrodth"))
                                                                Case Is = "qcarga"
                                                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set carga = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & HttpContext.Current.Session("registrodth"))
                                                            End Select

                                                        Next
                                                End Select


                                            Next


                                    End Select

                                Next
                        End Select

                    Next
                End With

                tbgravar = tabgravar.conectar("update tbcte set ativo = true where nrseq = '" & HttpContext.Current.Session("cte") & "'")
            Next

            Return validacte(nrlote)

        Catch exlote As Exception
            Dim resultadoenvioemail As String
            Try
                Dim envio As New clsEnvioEmail
                envio.configpadrao()
                ' envio.AdicionaDestinatarios = "suporte@smartcodesolucoes.com.br"
                envio.AdicionaDestinatarios = "alertas@smartcodesoluces.com.br"
                envio.AdicionaAssunto = "Erro sistema Ceva Cte - " & data() & " / " & hora()
                envio.EhHTML = True
                envio.AdicionaMensagem = "<html><br> <Center> <img src=""http://www.smartcodesolucoes.com.br/img/simbolo.jpg"" width=""37px"" height=""32px""> <br> <span color=red> Relatório de erros </span> </center> <br> " & "Erro processamento no lote: " & nrlote & " <br> " & exlote.Message & " (" & exlote.HelpLink & ") <br> Página: " & HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Length - 1) & " <br> Usuário: " & HttpContext.Current.Session("usuario") & " </html>"
                If envio.EnviarEmail Then
                    resultadoenvioemail = "E-mail enviado com sucesso !"
                Else
                    resultadoenvioemail = "E-mail não enviado !"
                End If
            Catch exemail As Exception
                resultadoenvioemail = "E-mail não enviado !" & exemail.Message
            End Try
            Dim logerro As New System.IO.StreamWriter(HttpContext.Current.Server.MapPath("~") & "\logerros\errolotes" & sonumeros(HttpContext.Current.Request.UserHostAddress) & sonumeros(data) & sonumeros(hora(True)) & ".txt")
            'logerro.WriteLine(_sql)
            logerro.WriteLine("Log de erro - Data: " & data() & " / Hora: " & hora(True) & " Mensagem: " & exlote.Message & " (" & exlote.LineNumber & ")")
            logerro.WriteLine("Resultado notificação por e-mail " & resultadoenvioemail)
            logerro.Close()



            Return False
        End Try



        Return True

    End Function
    Public Shared Function diferencatempo(data1 As Date, hora1 As String, data2 As Date, hora2 As String, Optional somentedias As Boolean = False) As String

        If somentedias Then
            Return DateDiff(DateInterval.Day, data1, data2) & " days"
        End If


        If DateDiff(DateInterval.Day, data1, data2) > 0 Then
            Dim dias As Integer = DateDiff(DateInterval.Day, data1, data2)
            Dim minutos As Integer = DateDiff(DateInterval.Minute, CDate(data2 & " 00:00:01"), CDate(data2 & " " & hora2))
            If minutos < 60 Then
                Return "0 dias, 0 hrs e " & minutos & " min"
            Else
                Return dias & " dias e " & Microsoft.VisualBasic.Int(minutos / 60) & " hrs"
            End If
        Else
            Dim minutos As Integer = DateDiff(DateInterval.Minute, CDate(data1 & " " & hora1), CDate(data2 & " " & hora2))
            If minutos < 60 Then
                Return "0 dias, 0 hrs e " & minutos & " min"
            Else
                Return "0 dias e " & Microsoft.VisualBasic.Int(minutos / 60) & " hrs"
            End If

        End If
    End Function
    Public Shared Function validanumero(valor As String) As Decimal
        If valor = "" Then Return 0
        Return valor
    End Function
    Public Shared Function validacte(nrlote As Integer) As Boolean
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        tb1 = tab1.conectar("select tbcte.* from tbcte inner join vwlotesarq on tbcte.nrseqarquivo = vwlotesarq.nrseq where vwlotesarq.ativo = true and  vwlotesarq.nrseqlote = " & nrlote)
        If tb1.Rows.Count = 0 Then
            Return False
        End If
        Dim valido As Boolean = True
        For x As Integer = 0 To tb1.Rows.Count - 1
            If tb1.Rows(x)("municipioinicial").ToString = "" OrElse tb1.Rows(x)("municipiofinal").ToString = "" Then
                valido = False
                Exit For
            End If
        Next

        Return valido
    End Function


    Public Shared Function corrigirdescricaofrete(nrlote As Integer, Optional existe As Boolean = False) As Boolean
        Try


            Dim resultado As String = ""


            Dim tbarq As New Data.DataTable
            Dim tabarq As New clsBanco
            Dim tbgravar As New Data.DataTable
            Dim tabgravar As New clsBanco


            tbarq = tabarq.conectar("select * from tblotes_arquivos where nrseqlote = " & nrlote & " and tparquivo = 'XML CT-e' and ativo = true")

            For x As Integer = 0 To tbarq.Rows.Count - 1
                'tbgravar = tabgravar.conectar("Select * from tbcte where nrseqarquivo = " & tbarq.Rows(x)("nrseq").ToString)
                'If tbgravar.Rows.Count <> 0 Then
                '    Continue For
                'End If
                Dim abrir As New XmlDocument

                abrir.Load(HttpContext.Current.Server.MapPath("~\arquivoslotes\xml\" & tbarq.Rows(x)("arquivo").ToString))
                Dim varinicial As Integer = 1
                Try
tentadenovo:


                    If varinicial < 0 Then
                        varinicial = 1
                        GoTo terminou
                    End If
                    Dim contatudo As Integer = abrir.ChildNodes(varinicial).ChildNodes(0).ChildNodes(0).ChildNodes.Count - 1
                Catch exvalida As Exception
                    varinicial -= 1
                    GoTo tentadenovo
                End Try
terminou:



                With abrir.ChildNodes(varinicial).ChildNodes(0).ChildNodes(0)
                    For z As Integer = 0 To .ChildNodes.Count - 1



                        Select Case .ChildNodes(z).Name.ToLower
                            Case Is = "ide"
                                Dim wcnrseqctrlcte As String = gerarnrseqcontrole() & nrlote & "_ " & tbarq.Rows(x)("nrseq").ToString & x

                                tbgravar = tabgravar.IncluirAlterarDados("select * from  tbcte  where nrseqarquivo = " & tbarq.Rows(x)("nrseq").ToString & "")
                                If tbgravar.Rows.Count = 0 Then
                                    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte (nrseqarquivo, dtcad, usercad, ativo, nrseqctrl) values (" & tbarq.Rows(x)("nrseq").ToString & ",'" & formatadatamysql(data) & "','" & HttpContext.Current.Session("usuario").ToString & "',false, '" & wcnrseqctrlcte & "')")
                                    tbgravar = tabgravar.conectar("select * from tbcte where nrseqctrl = '" & wcnrseqctrlcte & "'")

                                End If

                                HttpContext.Current.Session("cte") = tbgravar.Rows(0)("nrseq").ToString

                                'For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                                '    If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("toma") Then


                                '        HttpContext.Current.Session("tomador") = "nao"
                                '        Dim wcnrseqctrl As String = gerarnrseqcontrole() & "tomador" & HttpContext.Current.Session("cte")
                                '        tbgravar = tabgravar.conectar("select * from tbcte_tomador where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                '        If tbgravar.Rows.Count = 0 Then
                                '            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_tomador (nrseqcte, wcnrseqctrl ) values (" & HttpContext.Current.Session("cte").ToString & ",'" & wcnrseqctrl & "')")
                                '        End If

                                '        For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1

                                '            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                '                Case Is = "cnpj"
                                '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                '                Case Is = "ie"
                                '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                '                Case Is = "xnome"
                                '                    HttpContext.Current.Session("tomador") = "sim"
                                '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                '            End Select

                                '        Next
                                '    End If
                                '    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                '        'Case Is = "toma4"

                                '        Case Is = "nct"
                                '            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set nrcte = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                '        Case Is = "natop"
                                '            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set natope = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)

                                '        Case Is = "dhemi"
                                '            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set dtemissao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                '        Case Is = "xmunini"
                                '            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set municipioinicial = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                '        Case Is = "xmunfim"
                                '            tbgravar = tabgravar.IncluirAlterarDados("update tbcte set municipiofinal = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                '    End Select

                                'Next

                            'Case Is = "compl"
                            '    tbgravar = tabgravar.conectar("select * from tbcte_compl where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '    If tbgravar.Rows.Count = 0 Then
                            '        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_compl (nrseqcte) values ('" & HttpContext.Current.Session("cte").ToString & "')")
                            '    End If
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1
                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "xemi"

                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_compl set descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "xobs"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_compl set obs = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText & vbNewLine) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)

                            '        End Select

                            '    Next
                            'Case Is = "emit"
                            '    tbgravar = tabgravar.conectar("select * from tbcte_emissor where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '    If tbgravar.Rows.Count = 0 Then
                            '        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_emissor (nrseqcte) values ('" & HttpContext.Current.Session("cte").ToString & "')")
                            '    End If
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "cnpj"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "ie"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "xnome"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_emissor set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '        End Select

                            '    Next

                            'Case Is = "rem"
                            '    tbgravar = tabgravar.conectar("select * from tbcte_remetente where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '    If tbgravar.Rows.Count = 0 Then
                            '        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_remetente (nrseqcte) values ('" & HttpContext.Current.Session("cte").ToString & "')")
                            '    End If
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "cnpj"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "ei"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '            Case Is = "xnome"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_remetente set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '        End Select

                            '    Next

                            'Case Is = "dest"
                            '    tbgravar = tabgravar.conectar("select * from tbcte_destinatario where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '    If tbgravar.Rows.Count = 0 Then
                            '        Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & HttpContext.Current.Session("cte").ToString
                            '        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_destinatario (nrseqcte, nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "')")
                            '        tbgravar = tabgravar.conectar("select * from tbcte_destinatario where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                            '    End If


                            '    HttpContext.Current.Session("ctedest") = tbgravar.Rows(0)("nrseq").ToString
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1


                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "cnpj"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set cnpj = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                If HttpContext.Current.Session("tomador") = "nao" Then
                            '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set cnpj = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                End If
                            '            Case Is = "ie"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set ie = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                If HttpContext.Current.Session("tomador") = "nao" Then
                            '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set ie = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                End If
                            '            Case Is = "xnome"
                            '                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                If HttpContext.Current.Session("tomador") = "nao" Then
                            '                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_tomador set nome = '" & tratatexto(tratatexto(.ChildNodes(z).ChildNodes(y).InnerText)) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                End If
                            '            Case Is = "enderdest"
                            '                tbgravar = tabgravar.conectar("select * from tbcte_destinatario_endereco where nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and nrseqdestinatario = " & HttpContext.Current.Session("ctedest"))
                            '                If tbgravar.Rows.Count = 0 Then
                            '                    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_destinatario_endereco (nrseqcte, nrseqdestinatario) values ('" & HttpContext.Current.Session("cte").ToString & "'," & HttpContext.Current.Session("ctedest") & ")")
                            '                End If

                            '                For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                            '                    Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                            '                        Case Is = "xlgr"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set endereco = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                        Case Is = "nro"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set numero = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                        Case Is = "xbairro"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set bairro = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                        Case Is = "cep"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set cep = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                        Case Is = "cmun"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set codmunicipio = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                        Case Is = "xmun"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set municipio = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                        Case Is = "uf"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set uf = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                        Case Is = "xpais"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set pais = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                        Case Is = "cpais"
                            '                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_destinatario_endereco set codpais = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                            '                    End Select

                            '                Next
                            '        End Select

                            '    Next
                            'Case Is = "imp"
                            '    For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1
                            '        Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                            '            Case Is = "infAdFisco".ToLower
                            '                tbgravar = tabgravar.conectar("select * from tbcte_fisco where nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText.ToString) & "'")
                            '                If tbgravar.Rows.Count = 0 Then
                            '                    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_fisco (nrseqcte, descricao) values ('" & HttpContext.Current.Session("cte").ToString & "','" & tratatexto(.ChildNodes(z).ChildNodes(y).InnerText.ToString) & "')")
                            '                End If


                            '        End Select
                            '    Next
                            Case Is = "vprest"
                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte set valor = " & moeda(.ChildNodes(z).ChildNodes(0).InnerText) & " where nrseq = " & HttpContext.Current.Session("cte").ToString)
                                For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1

                                    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower
                                        Case Is = "comp"



                                            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                                    Case Is = "xnome"
                                                        Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & HttpContext.Current.Session("cte").ToString
                                                        tbgravar = tabgravar.conectar("select * from tbcte_descricao where nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "'")
                                                        If tbgravar.Rows.Count = 0 Then
                                                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_descricao (nrseqcte, nrseqctrl, descricao ) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "', '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "')")
                                                        Else
                                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_descricao set descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where  nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "'")
                                                        End If

                                                    Case Is = "vcomp"
                                                        ' procura a descricao anterior (indice anterior x- 1) e grava o valor
                                                        Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & HttpContext.Current.Session("cte").ToString
                                                        tbgravar = tabgravar.conectar("select * from tbcte_descricao where nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w - 1).InnerText) & "'")
                                                        If tbgravar.Rows.Count = 0 Then
                                                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_descricao (nrseqcte, nrseqctrl, descricao) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "','" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w - 1).InnerText) & "')")
                                                        Else
                                                            tbgravar = tabgravar.IncluirAlterarDados("update tbcte_descricao set valor = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where  nrseqcte = " & HttpContext.Current.Session("cte").ToString & " and descricao = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w - 1).InnerText) & "'")
                                                        End If


                                                        'Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & HttpContext.Current.Session("cte").ToString
                                                        'tbgravar = tabgravar.conectar("select * from tbcte_descricao where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                                        'If tbgravar.Rows.Count = 0 Then
                                                        '    tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_descricao (nrseqcte, nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "')")
                                                        'Else
                                                        '    wcnrseqctrl_desc = tbgravar.Rows(0)("nrseqctrl").ToString
                                                        'End If
                                                        'tbgravar = tabgravar.IncluirAlterarDados("update tbcte_descricao set valor = " & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & " where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                                                End Select
                                            Next


                                    End Select

                                Next

                            Case Is = "infctenorm"
                                'For y As Integer = 0 To .ChildNodes(z).ChildNodes.Count - 1


                                '    If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("cobr") Then
                                '        For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1

                                '            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                '                Case Is = "fat"
                                '                    Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & HttpContext.Current.Session("cte").ToString & "fat"
                                '                    tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqcte = " & HttpContext.Current.Session("cte") & " and tipodecobranca = 'fatura'")
                                '                    If tbgravar.Rows.Count = 0 Then
                                '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca (nrseqcte, tipodecobranca, nrseqctrl) values (" & HttpContext.Current.Session("cte") & ",'Fatura','" & wcnrseqctrl_desc & "')")
                                '                        tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                '                        HttpContext.Current.Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                                '                    Else
                                '                        HttpContext.Current.Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                                '                    End If
                                '                    tbgravar = tabgravar.conectar("select * from tbcte_cobranca_faturas where nrseqcte = " & HttpContext.Current.Session("cte") & " and tipodecobranca = 'fatura' and nrseqcobranca = " & HttpContext.Current.Session("cobranca"))
                                '                    If tbgravar.Rows.Count = 0 Then
                                '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca_faturas (nrseqcte, nrseqcobranca, nrseqctrl) values (" & HttpContext.Current.Session("cte") & "," & HttpContext.Current.Session("cobranca") & ",'" & wcnrseqctrl_desc & "')")
                                '                        tbgravar = tabgravar.conectar("Select * from tbcte_cobranca_faturas where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                '                        HttpContext.Current.Session("nrseqduplicata") = tbgravar.Rows(0)("nrseq").ToString
                                '                    Else
                                '                        HttpContext.Current.Session("nrseqduplicata") = tbgravar.Rows(0)("nrseq").ToString
                                '                    End If

                                '                    For i As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1

                                '                        Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).Name.ToLower
                                '                            Case Is = "nfat"
                                '                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set numerofatura = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                '                            Case Is = "vorig"
                                '                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set valororigem = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                '                            Case Is = "vliq"
                                '                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_faturas set valorliquido = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                '                        End Select
                                '                    Next

                                '                Case Is = "dup"
                                '                    Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & x.ToString & HttpContext.Current.Session("cte").ToString & "fat"
                                '                    tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqcte = " & HttpContext.Current.Session("cte") & " and tipodecobranca = 'Duplicata'")
                                '                    If tbgravar.Rows.Count = 0 Then
                                '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca (nrseqcte, tipodecobranca, nrseqctrl) values (" & HttpContext.Current.Session("cte") & ",'Duplicata','" & wcnrseqctrl_desc & "')")
                                '                        tbgravar = tabgravar.conectar("select * from tbcte_cobranca where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                '                        HttpContext.Current.Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString

                                '                    Else
                                '                        HttpContext.Current.Session("cobranca") = tbgravar.Rows(0)("nrseq").ToString
                                '                    End If
                                '                    tbgravar = tabgravar.conectar("select * from tbcte_cobranca_duplicatas where nrseqcte = " & HttpContext.Current.Session("cobranca") & " and nrseqcobranca = " & HttpContext.Current.Session("cobranca"))
                                '                    If tbgravar.Rows.Count = 0 Then
                                '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_cobranca_duplicatas (nrseqcte, nrseqcobranca, nrseqctrl) values (" & HttpContext.Current.Session("cte") & "," & HttpContext.Current.Session("cobranca") & ",'" & wcnrseqctrl_desc & "')")
                                '                        tbgravar = tabgravar.conectar("Select * from tbcte_cobranca_duplicatas where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                '                        HttpContext.Current.Session("nrseqduplicata") = tbgravar.Rows(0)("nrseq").ToString
                                '                    Else
                                '                        HttpContext.Current.Session("nrseqduplicata") = tbgravar.Rows(0)("nrseq").ToString
                                '                    End If
                                '                    For i As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1

                                '                        Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).Name.ToLower
                                '                            Case Is = "ndup"
                                '                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set numeroduplicata = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                '                            Case Is = "dvenc"
                                '                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set dtvencimento = '" & formatadatamysql(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                '                            Case Is = "vdup"
                                '                                tbgravar = tabgravar.IncluirAlterarDados("update tbcte_cobranca_duplicatas set valorduplicata = '" & moeda(.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(i).InnerText.ToString) & "' where nrseq = " & HttpContext.Current.Session("nrseqduplicata"))
                                '                        End Select
                                '                    Next
                                '            End Select

                                '        Next
                                '    End If



                                '    If .ChildNodes(z).ChildNodes(y).Name.ToLower.Contains("infmodal") Then
                                '        For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                '            If .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower.Contains("rodo") Then
                                '                For k As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1
                                '                    If .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower = "moto" Then
                                '                        Dim wcnrseqctrlveic As String = gerarnrseqcontrole() & k & HttpContext.Current.Session("cte").ToString
                                '                        tbgravar = tabgravar.IncluirAlterarDados("select * from tbcte_motoristas where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                '                        If tbgravar.Rows.Count = 0 Then
                                '                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_motoristas (nrseqcte,nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrlveic & "')")
                                '                        Else
                                '                            wcnrseqctrlveic = tbgravar.Rows(0)("nrseqctrl").ToString
                                '                        End If

                                '                        For k1 As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes.Count - 1
                                '                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).Name.ToLower
                                '                                Case Is = "xnome"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_motoristas set nome = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "cpf"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_motoristas set cpf = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                            End Select
                                '                        Next
                                '                    End If
                                '                    If .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower = "veic" Then
                                '                        Dim wcnrseqctrlveic As String = gerarnrseqcontrole() & k & HttpContext.Current.Session("cte").ToString
                                '                        tbgravar = tabgravar.conectar("select * from tbcte_veiculos where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                '                        If tbgravar.Rows.Count = 0 Then
                                '                            tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_veiculos (nrseqcte,nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrlveic & "')")
                                '                        Else
                                '                            wcnrseqctrlveic = tbgravar.Rows("nrseqctrl").ToString
                                '                        End If


                                '                        For k1 As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes.Count - 1
                                '                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).Name.ToLower
                                '                                Case Is = "renavam"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set renavam = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "placa"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set placa = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "tara"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tara = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "capkg"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set capkg = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "capm3"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set capm3 = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "tpprop"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpprop = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "tpveic"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpveic = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "tpprop"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpprop = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "tpcar"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set tpcar = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                                Case Is = "uf"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_veiculos set uf = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).ChildNodes(k1).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrlveic & "'")
                                '                            End Select
                                '                        Next
                                '                    End If
                                '                Next
                                '            End If
                                '        Next
                                '    End If

                                '    Select Case .ChildNodes(z).ChildNodes(y).Name.ToLower

                                '        Case Is = "seg"
                                '            Dim wcnrseqseg As String = gerarnrseqcontrole() & y & HttpContext.Current.Session("cte")
                                '            tbgravar = tabgravar.conectar("select * from tbcte_seguradora where nrseqcte = " & HttpContext.Current.Session("cte"))
                                '            If tbgravar.Rows.Count = 0 Then
                                '                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_seguradora (nrseqcte, nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqseg & "')")
                                '                tbgravar = tabgravar.conectar("Select * from tbcte_seguradora where nrseqctrl = '" & wcnrseqseg & "'")
                                '                HttpContext.Current.Session("nrseqseguro") = tbgravar.Rows(0)("nrseq").ToString
                                '            Else
                                '                HttpContext.Current.Session("nrseqseguro") = tbgravar.Rows(0)("nrseq").ToString
                                '            End If



                                '            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                '                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                '                    Case Is = "xseg"
                                '                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_seguradora set nome = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseq =  " & HttpContext.Current.Session("nrseqseguro").ToString)
                                '                    Case Is = "napol"
                                '                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_seguradora set apolices = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseq =  " & HttpContext.Current.Session("nrseqseguro").ToString)
                                '                End Select
                                '            Next

                                '        Case Is = "infcarga"
                                '            Dim wcnrseqctrl_desc As String = gerarnrseqcontrole() & y.ToString & HttpContext.Current.Session("cte").ToString
                                '            tbgravar = tabgravar.conectar("select * from tbcte_itens where nrseqcte = " & HttpContext.Current.Session("cte").ToString)
                                '            If tbgravar.Rows.Count = 0 Then
                                '                tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_itens (nrseqcte, nrseqctrl) values ('" & HttpContext.Current.Session("cte").ToString & "','" & wcnrseqctrl_desc & "')")
                                '                tbgravar = tabgravar.conectar("select * from tbcte_itens where nrseqctrl = '" & wcnrseqctrl_desc & "'")
                                '                HttpContext.Current.Session("ctecargadth") = tbgravar.Rows(0)("nrseq").ToString
                                '            Else
                                '                HttpContext.Current.Session("ctecargadth") = tbgravar.Rows(0)("nrseq").ToString
                                '                wcnrseqctrl_desc = tbgravar.Rows(0)("nrseqctrl").ToString
                                '            End If

                                '            For w As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes.Count - 1
                                '                Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).Name.ToLower
                                '                    Case Is = "vcarga"
                                '                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set codcarga = '" & tratatexto(.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                                '                    Case Is = "propred"
                                '                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set denominacao = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                                '                    Case Is = "xoutcat"
                                '                        tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens set categoria = '" & (.ChildNodes(z).ChildNodes(y).ChildNodes(w).InnerText) & "' where nrseqctrl =  '" & wcnrseqctrl_desc & "'")
                                '                    Case Is = "infq"
                                '                        Dim wcnrseqctrl_dthitem As String = gerarnrseqcontrole() & y.ToString & w.ToString & HttpContext.Current.Session("ctecargadth").ToString
                                '                        tbgravar = tabgravar.IncluirAlterarDados("delete from tbcte_itens_dth where nrseqcteitem = " & HttpContext.Current.Session("ctecargadth").ToString)
                                '                        tbgravar = tabgravar.IncluirAlterarDados("insert into tbcte_itens_dth (nrseqcteitem, nrseqctrl) values ('" & HttpContext.Current.Session("ctecargadth").ToString & "','" & wcnrseqctrl_dthitem & "')")
                                '                        tbgravar = tabgravar.conectar("select * from tbcte_itens_dth where nrseqctrl = '" & wcnrseqctrl_dthitem & "'")
                                '                        HttpContext.Current.Session("registrodth") = tbgravar.Rows(0)("nrseq").ToString
                                '                        For k As Integer = 0 To .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes.Count - 1
                                '                            Select Case .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).Name.ToLower
                                '                                Case Is = "cunid"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set unidade = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & HttpContext.Current.Session("registrodth"))
                                '                                Case Is = "tpmed"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set tipomedida = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & HttpContext.Current.Session("registrodth"))
                                '                                Case Is = "qcarga"
                                '                                    tbgravar = tabgravar.IncluirAlterarDados("update tbcte_itens_dth set carga = '" & .ChildNodes(z).ChildNodes(y).ChildNodes(w).ChildNodes(k).InnerText & "' where nrseq = " & HttpContext.Current.Session("registrodth"))
                                '                            End Select

                                '                        Next
                                '                End Select


                                '            Next


                                '    End Select

                                'Next


                        End Select

                    Next
                End With

                ' tbgravar = tabgravar.conectar("update tbcte set ativo = true where nrseq = '" & HttpContext.Current.Session("cte") & "'")
            Next

            '  Return validacte(nrlote)

        Catch exlote As Exception
            Dim resultadoenvioemail As String
            Try
                Dim envio As New clsEnvioEmail
                envio.configpadrao()
                ' envio.AdicionaDestinatarios = "suporte@smartcodesolucoes.com.br"
                envio.AdicionaDestinatarios = "alertas@smartcodesoluces.com.br"
                envio.AdicionaAssunto = "Erro sistema Ceva Cte - " & data() & " / " & hora()
                envio.EhHTML = True
                envio.AdicionaMensagem = "<html><br> <Center> <img src=""http://www.smartcodesolucoes.com.br/img/simbolo.jpg"" width=""37px"" height=""32px""> <br> <span color=red> Relatório de erros </span> </center> <br> " & "Erro processamento no lote: " & nrlote & " <br> " & exlote.Message & " (" & exlote.InnerException.ToString & ") <br> Página: " & HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Length - 1) & " <br> Usuário: " & HttpContext.Current.Session("usuario") & " </html>"
                If envio.EnviarEmail Then
                    resultadoenvioemail = "E-mail enviado com sucesso !"
                Else
                    resultadoenvioemail = "E-mail não enviado !"
                End If
            Catch exemail As Exception
                resultadoenvioemail = "E-mail não enviado !" & exemail.Message
            End Try
            Dim logerro As New System.IO.StreamWriter(HttpContext.Current.Server.MapPath("~") & "\logerros\errolotes" & sonumeros(HttpContext.Current.Request.UserHostAddress) & sonumeros(data) & sonumeros(hora(True)) & ".txt")
            'logerro.WriteLine(_sql)
            logerro.WriteLine("Log de erro - Data: " & data() & " / Hora: " & hora(True) & " Mensagem: " & exlote.Message & " (" & exlote.LineNumber & ")")
            logerro.WriteLine("Resultado notificação por e-mail " & resultadoenvioemail)
            logerro.Close()



            Return False
        End Try



        Return True

    End Function
    Public Shared Function tratatexto(ByVal texto As String, Optional comaspas As Boolean = True, Optional tamanho As Integer = 0) As String
        Try
            If comaspas Then
                Return IIf(tamanho = 0, texto.Replace("'", "''"), mLeft(texto.Replace("'", "''"), tamanho))
            Else
                Return IIf(tamanho = 0, texto.Replace("'", ""), mLeft(texto.Replace("'", ""), tamanho))
            End If
        Catch ex As Exception
            Return ""
        End Try



    End Function
    Public Shared Function gravalog(acao As String, tipo As String) As Boolean
        Try

            Dim ip As String = HttpContext.Current.Request.UserHostAddress
            Dim nrseqctrl As String = gerarnrseqcontrole()
            Dim navegador As String = HttpContext.Current.Request.Browser.Browser & " Versão:" & HttpContext.Current.Request.Browser.Version
            Dim url As String = HttpContext.Current.Request.Url.ToString
            Dim idusuario As Integer = HttpContext.Current.Session("idusuario")

            Dim tblog As New Data.DataTable
            Dim tablog As New clsBanco


            tblog = tablog.IncluirAlterarDados("insert into tblog (nrsequsuario,dtcad,acao,nrseqctrl,ip,url,navegador,hora, tipodelog) values (" & idusuario & ",'" & formatadatamysql(Date.Now.Date) & "','" & acao & "','" & nrseqctrl & "','" & ip & "','" & url & "','" & navegador & "','" & TimeOfDay & "','" & tipo.ToString & "') ")
            Return True
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Shared Function soletras(ByVal valor As String) As String
        Dim wcnumero As String, x As Integer
        wcnumero = ""
        For x = 1 To Len(valor)
            If Not IsNumeric(Mid(valor, x, 1)) Then
                wcnumero = wcnumero & Mid(valor, x, 1)
            End If
        Next
        Return (wcnumero)
    End Function

    Public Shared Function csvToDatatable(arquivo As String, ByVal separator As String, Optional arquivolog As String = "") As Data.DataTable
        'If arquivolog = "" Then
        '    arquivolog = HttpContext.Current.Server.MapPath("~") & "\logerros\logimporta.txt"

        'End If
        arquivolog = alteranome(arquivo)
        Dim logerro As New StreamWriter(arquivolog)
        Dim wclinha As Integer = 0
        Dim dt As New System.Data.DataTable
        Dim firstLine As Boolean = True
        Dim file = arquivo
        Dim qtderro As Integer = 0
        Dim qtdcols As Integer = 0
        ' Dim uploadFile As HttpPostedFile = file
        Using sr As New StreamReader(file)
            While Not sr.EndOfStream
                If firstLine Then
                    firstLine = False
                    Dim cols = sr.ReadLine.Split(separator)
                    For Each col In cols
                        dt.Columns.Add(col.Trim)
                        qtdcols += 1
                    Next
                Else
                    wclinha += 1
                    Dim data() As String = sr.ReadLine.Split(separator)
                    For jj As Integer = 0 To data.Count - 1
                        data(jj) = data(jj).Trim
                    Next
                    If qtdcols = data.Count Then
                        dt.Rows.Add(data.ToArray)
                    Else
                        qtderro += 1
                        logerro.WriteLine("Numero de colunas inválidas! Linha: " & wclinha)
                        For y As Integer = 0 To data.Count - 1
                            If Not data(y) Is Nothing Then
                                logerro.WriteLine(data(y))
                            End If

                        Next
                    End If
                End If

            End While
        End Using
        logerro.Close()
        Return dt
    End Function
    Public Shared Function alteranome(nome As String) As String
        Dim novonome As String = mPASTA(nome) & encontrachr(mARQUIVO(nome), ".", 1) & "(" & Directory.GetFiles(mPASTA(nome), (encontrachr(mARQUIVO(nome), ".", 1) & "*." & encontrachr(mARQUIVO(nome), ".", 0))).Length & ")" & "." & encontrachr(mARQUIVO(nome), ".", 0)
        Return novonome
    End Function
    Public Shared Function retornaqtdlinhassalto(descricao As String, qtdchr As Integer) As Integer
        Dim descimp As List(Of String)
        Dim qtdlinhas As Integer = 0
        descimp = convertetextoemlista(descricao)
        Dim numposicaox As Integer = 0
        Dim completaespacox As Integer = 0
        Dim caracterfinalx As Integer = qtdchr
        For z As Integer = 0 To descimp.Count - 1
            caracterfinalx = qtdchr
            Dim wcqtdlinhasmensagemx As Integer = Len(descimp(z)) / qtdchr
            numposicaox = 0

            completaespacox = 0
            For y As Integer = 0 To wcqtdlinhasmensagemx

                '  caracterfinal = wctermoqtdcaracterlinha

                caracterfinalx = encontraposicao(descimp(z), numposicaox, qtdchr)
                completaespacox = wctermoqtdcaracterlinha - caracterfinalx


                'inicia a contagem das linhas
                Dim txttextolinha As String = msubstring(descimp(z), numposicaox, caracterfinalx)
                'recebe  o texto completo da linha

                Dim txtnovotexto As String = ""

                ' text que receberar o novo texto

                For h As Integer = 0 To txttextolinha.Length - 1
                    'inicia tratamento dos caracteres da linha processada
                    Dim caracter = txttextolinha.Substring(h, 1)

                    If caracter = " " And completaespacox <> 0 Then
                        caracter = "  "
                        completaespacox -= 1
                    End If
                    'verifica se é um enter ou um espaço muito grande
                    txtnovotexto = txtnovotexto & caracter

                Next
                If txtnovotexto = "" Then Continue For
                qtdlinhas += 1

                numposicaox = numposicaox + caracterfinalx + 1  'continua a pegando as linhas da ultima possiçao, a informação +1 é devido ao espaço  da linha anterior
            Next
        Next
        Return qtdlinhas
    End Function
    Public Shared Function SearchForFiles(ByVal RootFolder As String, ByVal FileFilter() As String) As List(Of String)
        Dim ReturnedData As New List(Of String)                             'List to hold the search results
        Dim FolderStack As New Stack(Of String)                             'Stack for searching the folders
        FolderStack.Push(RootFolder)                                        'Start at the specified root folder
        Do While FolderStack.Count > 0                                      'While there are things in the stack
            Dim ThisFolder As String = FolderStack.Pop                      'Grab the next folder to process
            Try                                                             'Use a try to catch any errors
                For Each SubFolder In GetDirectories(ThisFolder)            'Loop through each sub folder in this folder
                    FolderStack.Push(SubFolder)                             'Add to the stack for further processing
                Next                                                        'Process next sub folder
                For Each FileExt In FileFilter                              'For each File filter specified


                    ReturnedData.AddRange(GetFiles(ThisFolder, FileExt))    'Search for and return the matched file names


                Next                                                        'Process next FileFilter
            Catch ex As Exception                                           'For simplicity sake
            End Try                                                         'We'll ignore the errors
        Loop                                                                'Process next folder in the stack
        Return ReturnedData                                                 'Return the list of files that match
    End Function
    Public Shared Sub criarPADinicial()

        Dim tba As New Data.DataTable
        Dim taba As New clsBanco
        Dim tbb As New Data.DataTable
        Dim tabb As New clsBanco
        Dim xsql As String = "select nrseq, nrseqgestor, 2019, true, now() , 'Sistema', 'Inclusão automatica' from tbcolaboradores where not matricula in (SELECT matricula FROM vwcolaboradores_sem_pad where anobase = " & Year(Date.Now.Date) & ")"
        tba = taba.conectar(xsql)

        If tba.Rows.Count > 0 Then



            xsql = "insert into tbpad (nrseqcolaborador, nrseqgestor, anobase, ativo,dtcad, usercad, nrseqctrl) select nrseq, nrseqgestor, " & Year(Date.Now.Date) & ", true, now() , 'Sistema','Inclusão automatica' from tbcolaboradores where not matricula in (SELECT matricula FROM vwcolaboradores_sem_pad where anobase = " & Year(Date.Now.Date) & ")"
            tba = taba.IncluirAlterarDados(xsql)
        End If

        '  tbb = tabb.conectar("select * from tbpad where ")
    End Sub
    Public Shared Function ultimodiames(data As Date) As Date
        Dim wcdata As Date = CDate("01/" & zeros(data.AddMonths(1).Month, 2) & "/" & data.AddMonths(1).Year)
        Return wcdata.AddDays(-1)

    End Function
    Public Shared Function retornacodsemana(dia As String) As Integer
        Dim xcoddia As Integer = 0
        Select Case dia.ToLower
            Case Is = "domingo"
                xcoddia = 1
            Case Is = "segunda"
                xcoddia = 2
            Case Is = "terça"
                xcoddia = 3
            Case Is = "quarta"
                xcoddia = 4
            Case Is = "quinta"
                xcoddia = 5
            Case Is = "sexta"
                xcoddia = 6
            Case Is = "sábado"
                xcoddia = 7
        End Select
        Return xcoddia
    End Function
    Public Shared Function numeros(valor As String, Optional v As Boolean = False) As Decimal
        If valor = "" OrElse Not IsNumeric(valor) Then
            Return 0
        End If
        Return CType(valor, Decimal)
    End Function


    Public Shared Function getDecimal(ByVal number As Byte) As String
        Try
            Select Case number
                Case 0
                    Return ""
                Case 1 To 19
                    Dim strArray() As String =
                       {"Um", "Dois", "Três", "Quatro", "Cinco", "Seis",
                        "Sete", "Oito", "Nove", "Dez", "Onze",
                        "Doze", "Treze", "Quatorze", "Quinze",
                        "Dezesseis", "Dezessete", "Dezoito", "Dezenove"}
                    Return strArray(number - 1) + " "
                Case 20 To 99
                    Dim strArray() As String =
                        {"Vinte", "Trinta", "Quarenta", "Cinquenta",
                        "Sessenta", "Setenta", "Oitenta", "Noventa"}
                    If (number Mod 10) = 0 Then
                        Return strArray(number \ 10 - 2) + " "
                    Else
                        Return strArray(number \ 10 - 2) + " e " + getDecimal(number Mod 10) + " "
                    End If
                Case Else
                    Return ""
            End Select
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function getInteger(ByVal number As Decimal) As String
        Try
            number = Int(number)
            Select Case number
                Case Is < 0
                    Return "-" & getInteger(-number)
                Case 0
                    Return ""
                Case 1 To 19
                    Dim strArray() As String =
                        {"Um", "Dois", "Três", "Quatro", "Cinco", "Seis",
                        "Sete", "Oito", "Nove", "Dez", "Onze", "Doze",
                        "Treze", "Quatorze", "Quinze", "Dezesseis",
                        "Dezessete", "Dezoito", "Dezenove"}
                    Return strArray(number - 1) + " "
                Case 20 To 99
                    Dim strArray() As String =
                        {"Vinte", "Trinta", "Quarenta", "Cinquenta",
                        "Sessenta", "Setenta", "Oitenta", "Noventa"}
                    If (number Mod 10) = 0 Then
                        Return strArray(number \ 10 - 2)
                    Else
                        Return strArray(number \ 10 - 2) + " e " + getInteger(number Mod 10)
                    End If
                Case 100
                    Return "Cem"
                Case 101 To 999
                    Dim strArray() As String =
                           {"Cento", "Duzentos", "Trezentos", "Quatrocentos", "Quinhentos",
                           "Seiscentos", "Setecentos", "Oitocentos", "Novecentos"}
                    If (number Mod 100) = 0 Then
                        Return strArray(number \ 100 - 1) + " "
                    Else
                        Return strArray(number \ 100 - 1) + " e " + getInteger(number Mod 100)
                    End If
                Case 1000 To 1999
                    Select Case (number Mod 1000)
                        Case 0
                            Return "Mil"
                        Case Is <= 100
                            Return "Mil e " + getInteger(number Mod 1000)
                        Case Else
                            Return "Mil, " + getInteger(number Mod 1000)
                    End Select
                Case 2000 To 999999
                    Select Case (number Mod 1000)
                        Case 0
                            Return getInteger(number \ 1000) & "Mil"
                        Case Is <= 100
                            Return getInteger(number \ 1000) & "Mil e " & getInteger(number Mod 1000)
                        Case Else
                            Return getInteger(number \ 1000) & "Mil, " & getInteger(number Mod 1000)
                    End Select
                Case 1000000 To 1999999
                    Select Case (number Mod 1000000)
                        Case 0
                            Return "Um Milhão"
                        Case Is <= 100
                            Return getInteger(number \ 1000000) + "Milhão e " & getInteger(number Mod 1000000)
                        Case Else
                            Return getInteger(number \ 1000000) + "Milhão, " & getInteger(number Mod 1000000)
                    End Select
                Case 2000000 To 999999999
                    Select Case (number Mod 1000000)
                        Case 0
                            Return getInteger(number \ 1000000) + " Milhões"
                        Case Is <= 100
                            Return getInteger(number \ 1000000) + "Milhões e " & getInteger(number Mod 1000000)
                        Case Else
                            Return getInteger(number \ 1000000) + "Milhões, " & getInteger(number Mod 1000000)
                    End Select
                Case 1000000000 To 1999999999
                    Select Case (number Mod 1000000000)
                        Case 0
                            Return "Um Bilhão"
                        Case Is <= 100
                            Return getInteger(number \ 1000000000) + "Bilhão e " + getInteger(number Mod 1000000000)
                        Case Else
                            Return getInteger(number \ 1000000000) + "Bilhão, " + getInteger(number Mod 1000000000)
                    End Select
                Case Else
                    Select Case (number Mod 1000000000)
                        Case 0
                            Return getInteger(number \ 1000000000) + " Bilhões"
                        Case Is <= 100
                            Return getInteger(number \ 1000000000) + "Bilhões e " + getInteger(number Mod 1000000000)
                        Case Else
                            Return getInteger(number \ 1000000000) + "Bilhões, " + getInteger(number Mod 1000000000)
                    End Select
            End Select
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function NumeroToExtenso(ByVal number As Decimal, Optional ehmoeda As Boolean = True) As String
        Dim cent As Integer
        Try
            ' se for =0 retorna 0 reais
            If number = 0 Then
                Return "Zero" & IIf(ehmoeda, " Reais", "")
            End If
            ' Verifica a parte decimal, ou seja, os centavos
            cent = Decimal.Round((number - Int(number)) * 100, MidpointRounding.ToEven)
            ' Verifica apenas a parte inteira
            number = Int(number)
            ' Caso existam centavos
            If cent > 0 Then
                ' Caso seja 1 não coloca "Reais" mas sim "Real"
                If number = 1 Then
                    Return "Um" & IIf(ehmoeda, " Real e ", "") & getDecimal(cent) & IIf(ehmoeda, "centavos", "")
                    ' Caso o valor seja inferior a 1 Real
                ElseIf number = 0 Then
                    Return getDecimal(cent) & IIf(ehmoeda, "centavos", "")
                Else
                    Return getInteger(number) & IIf(ehmoeda, " Reais", "") & " e " & getDecimal(cent) & IIf(ehmoeda, "centavos", "")
                End If
            Else
                ' Caso seja 1 não coloca "Reais" mas sim "Real"
                If number = 1 Then
                    Return "Um " & IIf(ehmoeda, "Real", "")
                Else
                    Return getInteger(number) & IIf(ehmoeda, " Reais", "")
                End If
            End If
        Catch ex As Exception
            Return ""
        End Try

    End Function

End Class

































﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsCategoria
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Public Sub New()
        '   Usercad = buscarsessao("usuario")
    End Sub

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbcategorias"

    Dim _nrseq As Integer
    Dim _descricao As String
    Dim _ativo As Boolean
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _dtexclui As DateTime
    Dim _userexclui As String

    Dim _Mensagemerro As String
    Dim _contador As Integer

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _Mensagemerro
        End Get
        Set(value As String)
            _Mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class
Partial Public Class clsCategoria

    Public Function Consultar() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseq = tb1.Rows(0)("nrseq").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try



    End Function

    Public Function Consultardescricao() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where ativo=true and descricao = '" & _descricao & "'")

            If tb1.Rows.Count > 0 Then
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseq = tb1.Rows(0)("nrseq").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarcategoriajoinconveniocategoria() As Boolean

        Try

            tb1 = tab1.conectar("select a.*, (select count(*) from tbconvenios_categorias where ativo = true and nrseqcat = a.nrseq) as qtd from tbcategorias a where ativo = true order by descricao")


            If tb1.Rows.Count > 0 Then
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseq = tb1.Rows(0)("nrseq").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function carregarcat() As Boolean

        Try

            Dim xconvenios As New clsConvenios

            tb1 = tab1.conectar("select tbcategorias.* from tbcategorias where not nrseq in (select nrseqcat from tbconvenios_categorias inner join tbconvenios on tbconvenios.nrseq = tbconvenios_categorias.nrseqconvenio where tbconvenios.nrseq = " & xconvenios.Nrseq & " and tbconvenios_categorias.ativo = true) order by tbcategorias.descricao")


            If tb1.Rows.Count > 0 Then
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseq = tb1.Rows(0)("nrseq").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function carregarcatexcl() As Boolean

        Try

            Dim xconvenios As New clsConvenios

            tb1 = tab1.conectar("select tbcategorias.*, tbconvenios_categorias.nrseq as nrseqfim from tbcategorias inner join tbconvenios_categorias on tbcategorias.nrseq = tbconvenios_categorias.nrseqcat inner join tbconvenios on tbconvenios.nrseq = tbconvenios_categorias.nrseqconvenio where tbconvenios.nrseq = " & xconvenios.Nrseq & " and tbconvenios_categorias.ativo = true order by tbcategorias.descricao")


            If tb1.Rows.Count > 0 Then
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseq = tb1.Rows(0)("nrseq").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function NovoeSalvar() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, ativo, dtcad, usercad,descricao) values ('" & wcnrseqctrl & "',true,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "','" & _descricao & "')")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _Mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _Mensagemerro = "Erro ao excluir!"
                Return False
            End If

            Consultar()

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _Mensagemerro = exexcluir.ToString
            Return False

        End Try

    End Function


End Class
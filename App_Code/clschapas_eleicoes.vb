﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes

Public Class clschapas_eleicoes

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _mensagemtitulo As String = "Ops !"
    Dim _mensagemicone As String = "error"
    Dim _listaclasse As New List(Of clschapas_eleicoes)
    Dim _nrseq As Integer
    Dim _numero As String
    Dim _descricao As String
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Mensagemtitulo As String
        Get
            Return _mensagemtitulo
        End Get
        Set(value As String)
            _mensagemtitulo = value
        End Set
    End Property

    Public Property Mensagemicone As String
        Get
            Return _mensagemicone
        End Get
        Set(value As String)
            _mensagemicone = value
        End Set
    End Property

    Public Property Listaclasse As List(Of clschapas_eleicoes)
        Get
            Return _listaclasse
        End Get
        Set(value As List(Of clschapas_eleicoes))
            _listaclasse = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property
End Class

Partial Public Class clschapas_eleicoes

    Public Function Listarchapas_eleicoes() As Boolean
        Try
            Listaclasse.Clear()
            tb1 = tab1.conectar("select * from tbchapas_eleicoes where ativo = true")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaclasse.Add(New clschapas_eleicoes With {.nrseq = numeros(tb1.rows(x)("nrseq").tostring), .numero = tb1.rows(x)("numero").tostring, .descricao = tb1.rows(x)("descricao").tostring, .ativo = logico(tb1.rows(x)("ativo").tostring), .dtcad = valordata(tb1.rows(x)("dtcad").tostring), .usercad = tb1.rows(x)("usercad").tostring, .dtexclui = valordata(tb1.rows(x)("dtexclui").tostring), .userexclui = tb1.rows(x)("userexclui").tostring})
            Next
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function procurar() As Boolean
        Try
            If _nrseq = 0 Then
                mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbchapas_eleicoes where nrseq = " & _nrseq)
            If tb1.rows.count = 0 Then
                mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            _nrseq = numeros(tb1.rows(0)("nrseq").tostring)
            _numero = tb1.rows(0)("numero").tostring
            _descricao = tb1.rows(0)("descricao").tostring
            _ativo = logico(tb1.rows(0)("ativo").tostring)
            _dtcad = FormatDateTime(valordata(tb1.rows(0)("dtcad").tostring), DateFormat.ShortDate)
            _usercad = tb1.rows(0)("usercad").tostring
            _dtexclui = FormatDateTime(valordata(tb1.rows(0)("dtexclui").tostring), DateFormat.ShortDate)
            _userexclui = tb1.rows(0)("userexclui").tostring
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbchapas_eleicoes Set ativo = True"
            If _numero <> "" Then
                xsql &= ",numero = '" & tratatexto(_numero) & "'"
            End If
            If _descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(_descricao) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.incluiralterardados("insert into tbchapas_eleicoes (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbchapas_eleicoes where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.rows(0)("nrseq").tostring
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbchapas_eleicoes set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class



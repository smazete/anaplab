﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes

Public Class clscolaboradores_empresas
    'Desenvolvedor: Claudio Smart
    'Data inicial :  23/08/2019
    'Data Atualização :  29/08/2019 - por Claudio
    'Data Atualização :  29/08/2019 - por Claudio
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco


    Dim _nrseq As Integer = 0
    Dim _nrseqcolaborador As Integer = 0
    Dim _nrseqempresa As Integer = 0
    Dim _nrseqcargo As Integer = 0
    Dim _nrseqbu As Integer = 0
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _cargo As String
    Dim _empresa As String
    Dim _bu As String

    Public Sub New()
        _usercad = buscarsessoes("usuario")
        _dtcad = data()
    End Sub
    Public Sub New(usuariocarga As String)
        _usercad = usuariocarga
        _dtcad = data()
    End Sub

    Public Property Bu As String
        Get
            Return _bu
        End Get
        Set(value As String)
            _bu = tratatexto(value,, 45)
            If _bu = "" Then
                _nrseqbu = 0
            Else
retestarbu:
                tbx = tabx.conectar("select * from tbbus where descricao = '" & _bu & "'")
                If tbx.Rows.Count > 0 Then
                    _nrseqbu = tbx.Rows(0)("nrseq").ToString
                Else
                    tbx = tabx.IncluirAlterarDados("insert into tbbus (descricao, dtcad, usercad, ativo) values ('" & _bu & "', '" & hoje() & "', '" & _usercad & "', true)")

                    GoTo retestarbu
                    _nrseqbu = 0
                End If
            End If
        End Set
    End Property

    Public Property Cargo As String
        Get
            Return _cargo
        End Get
        Set(value As String)
            _cargo = tratatexto(value,, 45)
            If _cargo = "" Then
                Nrseqcargo = 0
            Else
retestarcargo:
                tbx = tabx.conectar("select * from tbcargos where descricao = '" & _cargo & "'")
                If tbx.Rows.Count > 0 Then
                    Nrseqcargo = tbx.Rows(0)("nrseq").ToString
                Else
                    tbx = tabx.IncluirAlterarDados("insert into tbcargos (descricao, dtcad, usercad, ativo) values ('" & _cargo & "', '" & hoje() & "','" & _usercad & "', true)")

                    GoTo retestarcargo
                    Nrseqcargo = 0
                End If
            End If
        End Set
    End Property

    Public Property Nrseqbu As Integer
        Get
            Return _nrseqbu
        End Get
        Set(value As Integer)
            _nrseqbu = value
        End Set
    End Property

    Public Property Empresa As String
        Get
            Return _empresa
        End Get
        Set(value As String)
            _empresa = tratatexto(value,, 45)
            If _empresa = "" Then
                _nrseqempresa = 0
            Else
retestarempresa:
                tbx = tabx.conectar("select * from tbempresas where empresa = '" & _empresa & "'")
                If tbx.Rows.Count > 0 Then
                    _nrseqempresa = tbx.Rows(0)("nrseq").ToString
                Else
                    tbx = tabx.IncluirAlterarDados("insert into tbempresas (descricao, ativo, dtcad, usercad) values ('" & _empresa & "', true, '" & hoje() & "', '" & _usercad & "')")
                    GoTo retestarempresa
                    _nrseqempresa = 0
                End If
            End If
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Nrseqcargo As Integer
        Get
            Return _nrseqcargo
        End Get
        Set(value As Integer)
            _nrseqcargo = value
        End Set
    End Property
End Class

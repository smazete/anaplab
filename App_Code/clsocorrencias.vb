﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports System.Data

Public Class clsocorrencias
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _descricao As String
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _tblistas As New Data.DataTable
    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Tblistas As DataTable
        Get
            Return _tblistas
        End Get
        Set(value As DataTable)
            _tblistas = value
        End Set
    End Property
End Class

Partial Public Class clsocorrencias

    Public Function procurar() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbocorrencias where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Descricao = tb1.Rows(0)("descricao").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        Usercad = tb1.Rows(0)("usercad").ToString

        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String

            tb1 = tab1.conectar("select * from tbocorrencias where descricao = '" & _descricao & "'")
            If tb1.Rows.Count = 0 Then
                tb1 = tab1.IncluirAlterarDados("insert into tbocorrencias (descricao, dtcad, usercad, ativo) values ('" & Descricao & "','" & hoje() & "','" & Usercad & "', true)")
            Else
                xsql = " update tbocorrencias Set ativo = True"

                If Descricao <> "" Then
                    xsql &= ",descricao = '" & tratatexto(Descricao) & "'"
                End If
                xsql &= " where descricao = '" & _descricao & "'"
                tb1 = tab1.IncluirAlterarDados(xsql)
            End If


            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function



    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbocorrencias set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
    Public Function retornalista() As Boolean

        Try
            Tblistas = tab1.conectar("select * from tbocorrencias where ativo = true order by descricao")

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class



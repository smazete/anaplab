﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsPlanos_map


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _listaplanos As New List(Of clsPlanos_map)


    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbplanos"

    Dim _nrseq As Integer
    Dim _descricao As String
    Dim _identificador As String
    Dim _ativo As Boolean
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _endereco As String
    Dim _cnpj As String
    Dim _uf As String
    Dim _cidade As String
    Dim _nomerecibo As String
    Dim _telcontato As String
    Dim _bancopadrao As String
    Dim _carteirapadrao As String
    Dim _planopadrao As Boolean
    Dim _textocarteirinhaverso As String

    Dim _nrseqctrl As String
    Dim _userexclui As String
    Dim _dtexclui As DateTime
    Dim _ignorarsaltodematricula As Boolean
    Dim _planocontrato As String
    Dim _padraocontrato As String


    Dim _mensagemerro As String
    Dim _contador As Integer

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property


    Public Property Identificador As String
        Get
            Return _identificador
        End Get
        Set(value As String)
            _identificador = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Cnpj As String
        Get
            Return _cnpj
        End Get
        Set(value As String)
            _cnpj = value
        End Set
    End Property

    Public Property Uf As String
        Get
            Return _uf
        End Get
        Set(value As String)
            _uf = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Nomerecibo As String
        Get
            Return _nomerecibo
        End Get
        Set(value As String)
            _nomerecibo = value
        End Set
    End Property

    Public Property Telcontato As String
        Get
            Return _telcontato
        End Get
        Set(value As String)
            _telcontato = value
        End Set
    End Property

    Public Property Bancopadrao As String
        Get
            Return _bancopadrao
        End Get
        Set(value As String)
            _bancopadrao = value
        End Set
    End Property

    Public Property Carteirapadrao As String
        Get
            Return _carteirapadrao
        End Get
        Set(value As String)
            _carteirapadrao = value
        End Set
    End Property

    Public Property Planopadrao As Boolean
        Get
            Return _planopadrao
        End Get
        Set(value As Boolean)
            _planopadrao = value
        End Set
    End Property

    Public Property Textocarteirinhaverso As String
        Get
            Return _textocarteirinhaverso
        End Get
        Set(value As String)
            _textocarteirinhaverso = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Ignorarsaltodematricula As Boolean
        Get
            Return _ignorarsaltodematricula
        End Get
        Set(value As Boolean)
            _ignorarsaltodematricula = value
        End Set
    End Property

    Public Property Planocontrato As String
        Get
            Return _planocontrato
        End Get
        Set(value As String)
            _planocontrato = value
        End Set
    End Property

    Public Property Padraocontrato As String
        Get
            Return _padraocontrato
        End Get
        Set(value As String)
            _padraocontrato = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Listaplanos As List(Of clsPlanos_map)
        Get
            Return _listaplanos
        End Get
        Set(value As List(Of clsPlanos_map)) '
            _listaplanos = value
        End Set
    End Property
End Class
Partial Public Class clsPlanos_map

    Public Function retornaplanos() As Boolean

        Try
            _listaplanos.Clear()
            tb1 = tab1.conectar("select * from tbplanos_map where salvo=true and ativo = true order by descricao")

            For x As Integer = 0 To tb1.Rows.Count - 1
                _listaplanos.Add(New clsPlanos_map With {.Nrseq = tb1.Rows(x)("nrseq").ToString, .Descricao = tb1.Rows(x)("descricao").ToString, .Identificador = tb1.Rows(x)("identificador").ToString, .Telcontato = tb1.Rows(x)("telcontato").ToString, .Uf = tb1.Rows(x)("uf").ToString, .Usercad = tb1.Rows(x)("Usercad").ToString, .Ativo = tb1.Rows(x)("Ativo").ToString, .Cidade = tb1.Rows(x)("Cidade").ToString, .Bancopadrao = tb1.Rows(x)("Bancopadrao").ToString, .Carteirapadrao = tb1.Rows(x)("Carteirapadrao").ToString, .Cnpj = tb1.Rows(x)("Cnpj").ToString, .Endereco = tb1.Rows(x)("Endereco").ToString, .Ignorarsaltodematricula = tb1.Rows(x)("Ignorarsaltodematricula").ToString, .Nomerecibo = tb1.Rows(x)("Nomerecibo").ToString, .Padraocontrato = tb1.Rows(x)("Padraocontrato").ToString, .Textocarteirinhaverso = tb1.Rows(x)("Textocarteirinhaverso").ToString})
            Next
            _table = tb1
            _contador = tb1.Rows.Count

        Catch ex As Exception

        End Try

    End Function


    Public Function consultar() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE salvo=true and ativo=true AND nrseq = '" & _nrseq & "' ")

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _descricao = tb1.Rows(0)("descricao").ToString

                _identificador = tb1.Rows(0)("identificador").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cnpj = tb1.Rows(0)("cnpj").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _nomerecibo = tb1.Rows(0)("nomerecibo").ToString
                _telcontato = tb1.Rows(0)("telcontato").ToString
                _bancopadrao = tb1.Rows(0)("bancopadrao").ToString
                _carteirapadrao = tb1.Rows(0)("carteirapadrao").ToString
                _planopadrao = logico(tb1.Rows(0)("planopadrao").ToString)
                _textocarteirinhaverso = tb1.Rows(0)("textocarteirinhaverso").ToString

                _userexclui = tb1.Rows(0)("userexclui").ToString
                _ignorarsaltodematricula = logico(tb1.Rows(0)("ignorarsaltodematricula").ToString)
                _planocontrato = tb1.Rows(0)("planocontrato").ToString
                _padraocontrato = tb1.Rows(0)("padraocontrato").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try


                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultartodos(Optional chkinativo As Boolean = False) As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE salvo=true" & IIf(chkinativo = True, " and ativo=" & _ativo, " and ativo=true"))

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _descricao = tb1.Rows(0)("descricao").ToString

                _identificador = tb1.Rows(0)("identificador").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cnpj = tb1.Rows(0)("cnpj").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _nomerecibo = tb1.Rows(0)("nomerecibo").ToString
                _telcontato = tb1.Rows(0)("telcontato").ToString
                _bancopadrao = tb1.Rows(0)("bancopadrao").ToString
                _carteirapadrao = tb1.Rows(0)("carteirapadrao").ToString
                _planopadrao = logico(tb1.Rows(0)("planopadrao").ToString)
                _textocarteirinhaverso = tb1.Rows(0)("textocarteirinhaverso").ToString






                _userexclui = tb1.Rows(0)("userexclui").ToString
                _ignorarsaltodematricula = logico(tb1.Rows(0)("ignorarsaltodematricula").ToString)
                _planocontrato = tb1.Rows(0)("planocontrato").ToString
                _padraocontrato = tb1.Rows(0)("padraocontrato").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function procurar(Optional descricao As Boolean = False, Optional inativo As Boolean = False, Optional CNPJ As Boolean = False) As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE salvo = true And Ativo = " & _ativo & IIf(descricao = True, " And descricao Like '" & _descricao & "%'", ""))

            If tb1.Rows.Count > 0 Then

                _descricao = tb1.Rows(0)("descricao").ToString
                _identificador = tb1.Rows(0)("identificador").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cnpj = tb1.Rows(0)("cnpj").ToString
                _uf = tb1.Rows(0)("uf").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _nomerecibo = tb1.Rows(0)("nomerecibo").ToString
                _telcontato = tb1.Rows(0)("telcontato").ToString
                _bancopadrao = tb1.Rows(0)("bancopadrao").ToString
                _carteirapadrao = tb1.Rows(0)("carteirapadrao").ToString
                _planopadrao = logico(tb1.Rows(0)("planopadrao").ToString)
                _textocarteirinhaverso = tb1.Rows(0)("textocarteirinhaverso").ToString


                _userexclui = tb1.Rows(0)("userexclui").ToString
                _ignorarsaltodematricula = logico(tb1.Rows(0)("ignorarsaltodematricula").ToString)
                _planocontrato = tb1.Rows(0)("planocontrato").ToString
                _padraocontrato = tb1.Rows(0)("padraocontrato").ToString




                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function




    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, ativo, dtcad, usercad) values ('" & wcnrseqctrl & "',false,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "')")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try
            tb1 = tab1.conectar("select * from " & _tablename & " where cnpj = '" & _cnpj & "'")

            If tb1.Rows.Count > 1 Then
                _mensagemerro = "CNPJ já em uso no sistema!"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set salvo=1 , ativo=1, descricao='" & _descricao & "',identificador='" & _identificador & "',endereco='" & _endereco & "',cnpj='" & _cnpj & "',uf='" & _uf & "',cidade='" & _cidade & "',nomerecibo='" & _nomerecibo & "',telcontato='" & _telcontato & "',bancopadrao='" & _bancopadrao & "',carteirapadrao='" & _carteirapadrao & "',planopadrao='" & _planopadrao & "',textocarteirinhaverso='" & _textocarteirinhaverso & "',ignorarsaltodematricula='" & _ignorarsaltodematricula & "',planocontrato='" & _planocontrato & "',padraocontrato='" & _padraocontrato & "' where nrseq= " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _mensagemerro = "Please, enter a valid Funcionario!"
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.ToString
            Return False

        End Try


    End Function

End Class
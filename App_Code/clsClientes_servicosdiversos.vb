﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data
Public Class clsClientes_servicosdiversos


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbclientes_servicosdiversos"

    Dim _nrseq As Integer
    Dim _matricula As String
    Dim _plano As String
    Dim _nrseqservico As Integer
    Dim _valor As Integer
    Dim _dtcad As DateTime
    Dim _ativo As Boolean
    Dim _dtexclusao As DateTime
    Dim _userexclusao As String
    Dim _processado As Boolean
    Dim _processadoem As DateTime
    Dim _cobradoem As DateTime
    Dim _usercad As String

    Dim _cobrado As Boolean
    Dim _nrseqoriginal As Integer

    Dim _cpfdependente As String
    Dim _usercobrado As String
    Dim _dtcobrado As DateTime
    Dim _databaixa As DateTime
    Dim _validade As DateTime
    Dim _nrseqctrl As String

    Dim _dtcobrarate As DateTime
    Dim _nrseqdependente As Integer
    Dim _nrseqcliente As Integer
    Dim _salvo As Boolean

    Dim _dtalteracao As DateTime
    Dim _useralteracao As String
    Dim _dtcobranca As DateTime

    Dim _Mensagemerro As String
    Dim _contador As Integer


    Private _detalhes As New List(Of clsServicosdiversos)()

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Nrseqservico As Integer
        Get
            Return _nrseqservico
        End Get
        Set(value As Integer)
            _nrseqservico = value
        End Set
    End Property

    Public Property Valor As Integer
        Get
            Return _valor
        End Get
        Set(value As Integer)
            _valor = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclusao As Date
        Get
            Return _dtexclusao
        End Get
        Set(value As Date)
            _dtexclusao = value
        End Set
    End Property

    Public Property Userexclusao As String
        Get
            Return _userexclusao
        End Get
        Set(value As String)
            _userexclusao = value
        End Set
    End Property

    Public Property Processado As Boolean
        Get
            Return _processado
        End Get
        Set(value As Boolean)
            _processado = value
        End Set
    End Property

    Public Property Processadoem As Date
        Get
            Return _processadoem
        End Get
        Set(value As Date)
            _processadoem = value
        End Set
    End Property

    Public Property Cobradoem As Date
        Get
            Return _cobradoem
        End Get
        Set(value As Date)
            _cobradoem = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property



    Public Property Cobrado As Boolean
        Get
            Return _cobrado
        End Get
        Set(value As Boolean)
            _cobrado = value
        End Set
    End Property

    Public Property Nrseqoriginal As Integer
        Get
            Return _nrseqoriginal
        End Get
        Set(value As Integer)
            _nrseqoriginal = value
        End Set
    End Property



    Public Property Cpfdependente As String
        Get
            Return _cpfdependente
        End Get
        Set(value As String)
            _cpfdependente = value
        End Set
    End Property

    Public Property Usercobrado As String
        Get
            Return _usercobrado
        End Get
        Set(value As String)
            _usercobrado = value
        End Set
    End Property

    Public Property Dtcobrado As Date
        Get
            Return _dtcobrado
        End Get
        Set(value As Date)
            _dtcobrado = value
        End Set
    End Property

    Public Property Databaixa As Date
        Get
            Return _databaixa
        End Get
        Set(value As Date)
            _databaixa = value
        End Set
    End Property

    Public Property Validade As Date
        Get
            Return _validade
        End Get
        Set(value As Date)
            _validade = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property



    Public Property Dtcobrarate As Date
        Get
            Return _dtcobrarate
        End Get
        Set(value As Date)
            _dtcobrarate = value
        End Set
    End Property

    Public Property Nrseqdependente As Integer
        Get
            Return _nrseqdependente
        End Get
        Set(value As Integer)
            _nrseqdependente = value
        End Set
    End Property

    Public Property Nrseqcliente As Integer
        Get
            Return _nrseqcliente
        End Get
        Set(value As Integer)
            _nrseqcliente = value
        End Set
    End Property

    Public Property Salvo As Boolean
        Get
            Return _salvo
        End Get
        Set(value As Boolean)
            _salvo = value
        End Set
    End Property



    Public Property Dtalteracao As Date
        Get
            Return _dtalteracao
        End Get
        Set(value As Date)
            _dtalteracao = value
        End Set
    End Property

    Public Property Useralteracao As String
        Get
            Return _useralteracao
        End Get
        Set(value As String)
            _useralteracao = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _Mensagemerro
        End Get
        Set(value As String)
            _Mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Dtcobranca As Date
        Get
            Return _dtcobranca
        End Get
        Set(value As Date)
            _dtcobranca = value
        End Set
    End Property
End Class
Partial Public Class clsClientes_servicosdiversos


    'Public Function consultarservicosvinculados(Optional dttext As String = "", Optional formastext As String = "", Optional desabilitadostext As String = "") As Boolean

    '    Try

    '        tb1 = tab1.conectar("SELECT cs.nrseq AS csnrseq, s.Servico As sservico, cs.valor As csvalor, d.nome As dependente, cs.cobrancaunica As cscobrancaunica, cs.cobrado As cscobrado, o.Servico AS oservico, cs.dtcad As csdtcad, cs.usercad As csusercad,  cs.dtcobrado AS csdtcobranca, c.matricula As cmatricula, c.nome As ccliente, p.descricao As formpagto FROM tbClientes_ServicosDiversos AS cs LEFT JOIN  tbdependentes_map AS d ON cs.nrseqdependente = d.nrseq LEFT JOIN  tbservicosdiversos AS s ON cs.nrseqservico = s.nrseq LEFT JOIN  tbservicosdiversos AS o ON cs.nrseqoriginal = o.nrseq LEFT JOIN tbclientes_map AS c ON cs.nrseqcliente = c.nrseq LEFT JOIN tbformapagto_map AS p ON c.codformapgto = p.nrseq WHERE cs.cobrancaunica = true AND cs.ativo = TRUE" & dttext & " " & formastext & " " & desabilitadostext)


    '        If tb1.Rows.Count - 1 > 0 Then


    '            _nrseq = tb1.Rows(0)("nrseq").ToString
    '            _matricula = tb1.Rows(0)("matricula").ToString
    '            _plano = tb1.Rows(0)("plano").ToString
    '            _nrseqservico = tb1.Rows(0)("nrseqservico").ToString
    '            _valor = tb1.Rows(0)("valor").ToString
    '            _ativo = logico(tb1.Rows(0)("ativo").ToString)
    '            _userexclusao = tb1.Rows(0)("userexclusao").ToString
    '            _processado = logico(tb1.Rows(0)("processado").ToString)
    '            _usercad = tb1.Rows(0)("usercad").ToString

    '            _cobrado = tb1.Rows(0)("cobrado").ToString
    '            _nrseqoriginal = tb1.Rows(0)("nrseqoriginal").ToString
    '            _duplicarvalor = logico(tb1.Rows(0)("duplicarvalor").ToString)
    '            _cpfdependente = tb1.Rows(0)("cpfdependente").ToString
    '            _usercobrado = tb1.Rows(0)("usercobrado").ToString
    '            _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
    '            _cobrarate = logico(tb1.Rows(0)("cobrarate").ToString)
    '            _nrseqdependente = tb1.Rows(0)("nrseqdependente").ToString
    '            _nrseqcliente = tb1.Rows(0)("nrseqcliente").ToString
    '            _salvo = logico(tb1.Rows(0)("salvo").ToString)
    '            _exigirvalidade = logico(tb1.Rows(0)("exigirvalidade").ToString)
    '            _exigirdependente = logico(tb1.Rows(0)("exigirdependente").ToString)
    '            _exigirdtbaixa = logico(tb1.Rows(0)("exigirdtbaixa").ToString)
    '            _useralteracao = tb1.Rows(0)("useralteracao").ToString


    '            Try
    '                If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcad").ToString
    '                    _dtcad = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtexclusao").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtexclusao").ToString
    '                    _dtexclusao = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("processadoem").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("processadoem").ToString
    '                    _processadoem = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("cobradoem").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("cobradoem").ToString
    '                    _cobradoem = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtcobrado").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobrado").ToString
    '                    _dtcobrado = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("databaixa").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("databaixa").ToString
    '                    _databaixa = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("validade").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("validade").ToString
    '                    _validade = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtcobrarate").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobrarate").ToString
    '                    _dtcobrarate = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtalteracao").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtalteracao").ToString
    '                    _dtalteracao = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try


    '        End If

    '        _contador = tb1.Rows.Count
    '        _table = tb1
    '        Return True
    '    Catch exconsultar As Exception
    '        _Mensagemerro = exconsultar.ToString
    '        Return False

    '    End Try

    'End Function

    'Public Function consultar() As Boolean

    '    Try

    '        tb1 = tab1.conectar("select * from " & _tablename & " where nrseq = " & _nrseq)

    '        If tb1.Rows.Count - 1 > 0 Then

    '            _nrseq = tb1.Rows(0)("nrseq").ToString
    '            _matricula = tb1.Rows(0)("matricula").ToString
    '            _plano = tb1.Rows(0)("plano").ToString
    '            _nrseqservico = tb1.Rows(0)("nrseqservico").ToString
    '            _valor = tb1.Rows(0)("valor").ToString
    '            _ativo = logico(tb1.Rows(0)("ativo").ToString)
    '            _userexclusao = tb1.Rows(0)("userexclusao").ToString
    '            _processado = logico(tb1.Rows(0)("processado").ToString)
    '            _usercad = tb1.Rows(0)("usercad").ToString
    '            _cobrancaunica = logico(tb1.Rows(0)("cobrancaunica").ToString)
    '            _cobrado = tb1.Rows(0)("cobrado").ToString
    '            _nrseqoriginal = tb1.Rows(0)("nrseqoriginal").ToString
    '            _duplicarvalor = logico(tb1.Rows(0)("duplicarvalor").ToString)
    '            _cpfdependente = tb1.Rows(0)("cpfdependente").ToString
    '            _usercobrado = tb1.Rows(0)("usercobrado").ToString
    '            _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
    '            _cobrarate = logico(tb1.Rows(0)("cobrarate").ToString)
    '            _nrseqdependente = tb1.Rows(0)("nrseqdependente").ToString
    '            _nrseqcliente = tb1.Rows(0)("nrseqcliente").ToString
    '            _salvo = logico(tb1.Rows(0)("salvo").ToString)
    '            _exigirvalidade = logico(tb1.Rows(0)("exigirvalidade").ToString)
    '            _exigirdependente = logico(tb1.Rows(0)("exigirdependente").ToString)
    '            _exigirdtbaixa = logico(tb1.Rows(0)("exigirdtbaixa").ToString)
    '            _useralteracao = tb1.Rows(0)("useralteracao").ToString

    '            Try
    '                If tb1.Rows(0)("dtcobranca").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobranca").ToString
    '                    _dtcobranca = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try

    '            Try
    '                If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcad").ToString
    '                    _dtcad = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtexclusao").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtexclusao").ToString
    '                    _dtexclusao = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("processadoem").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("processadoem").ToString
    '                    _processadoem = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("cobradoem").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("cobradoem").ToString
    '                    _cobradoem = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtcobrado").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobrado").ToString
    '                    _dtcobrado = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("databaixa").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("databaixa").ToString
    '                    _databaixa = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("validade").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("validade").ToString
    '                    _validade = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtcobrarate").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobrarate").ToString
    '                    _dtcobrarate = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtalteracao").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtalteracao").ToString
    '                    _dtalteracao = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try


    '        End If

    '        _contador = tb1.Rows.Count
    '        _table = tb1
    '        Return True
    '    Catch exconsultar As Exception
    '        _Mensagemerro = exconsultar.ToString
    '        Return False

    '    End Try

    'End Function

    'Public Function consultarcliente() As Boolean

    '    Try

    '        tb1 = tab1.conectar("select * from " & _tablename & " where nrseqcliente = " & _nrseqcliente)

    '        If tb1.Rows.Count - 1 > 0 Then


    '            _nrseq = tb1.Rows(0)("nrseq").ToString
    '            _matricula = tb1.Rows(0)("matricula").ToString
    '            _plano = tb1.Rows(0)("plano").ToString
    '            _nrseqservico = tb1.Rows(0)("nrseqservico").ToString
    '            _valor = tb1.Rows(0)("valor").ToString
    '            _ativo = logico(tb1.Rows(0)("ativo").ToString)
    '            _userexclusao = tb1.Rows(0)("userexclusao").ToString
    '            _processado = logico(tb1.Rows(0)("processado").ToString)
    '            _usercad = tb1.Rows(0)("usercad").ToString
    '            _cobrancaunica = logico(tb1.Rows(0)("cobrancaunica").ToString)
    '            _cobrado = tb1.Rows(0)("cobrado").ToString
    '            _nrseqoriginal = tb1.Rows(0)("nrseqoriginal").ToString
    '            _duplicarvalor = logico(tb1.Rows(0)("duplicarvalor").ToString)
    '            _cpfdependente = tb1.Rows(0)("cpfdependente").ToString
    '            _usercobrado = tb1.Rows(0)("usercobrado").ToString
    '            _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
    '            _cobrarate = logico(tb1.Rows(0)("cobrarate").ToString)
    '            _nrseqdependente = tb1.Rows(0)("nrseqdependente").ToString
    '            _nrseqcliente = tb1.Rows(0)("nrseqcliente").ToString
    '            _salvo = logico(tb1.Rows(0)("salvo").ToString)
    '            _exigirvalidade = logico(tb1.Rows(0)("exigirvalidade").ToString)
    '            _exigirdependente = logico(tb1.Rows(0)("exigirdependente").ToString)
    '            _exigirdtbaixa = logico(tb1.Rows(0)("exigirdtbaixa").ToString)
    '            _useralteracao = tb1.Rows(0)("useralteracao").ToString

    '            Try
    '                If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcad").ToString
    '                    _dtcad = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try

    '            Try
    '                If tb1.Rows(0)("dtcobranca").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobranca").ToString
    '                    _dtcobranca = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtexclusao").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtexclusao").ToString
    '                    _dtexclusao = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("processadoem").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("processadoem").ToString
    '                    _processadoem = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("cobradoem").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("cobradoem").ToString
    '                    _cobradoem = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtcobrado").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobrado").ToString
    '                    _dtcobrado = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("databaixa").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("databaixa").ToString
    '                    _databaixa = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("validade").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("validade").ToString
    '                    _validade = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtcobrarate").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobrarate").ToString
    '                    _dtcobrarate = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtalteracao").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtalteracao").ToString
    '                    _dtalteracao = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try


    '        End If

    '        _contador = tb1.Rows.Count
    '        _table = tb1
    '        Return True
    '    Catch exconsultar As Exception
    '        _Mensagemerro = exconsultar.ToString
    '        Return False

    '    End Try

    'End Function

    'Public Function consultarnome(Optional ativo As Boolean = True) As Boolean

    '    Try

    '        tb1 = tab1.conectar("Select * from tbcadfunerarias Where matricula Like '" & _matricula & "%' and ativo= " & ativo & "")

    '        If tb1.Rows.Count - 1 > 0 Then

    '            _nrseq = tb1.Rows(0)("nrseq").ToString
    '            _matricula = tb1.Rows(0)("matricula").ToString
    '            _plano = tb1.Rows(0)("plano").ToString
    '            _nrseqservico = tb1.Rows(0)("nrseqservico").ToString
    '            _valor = tb1.Rows(0)("valor").ToString
    '            _ativo = logico(tb1.Rows(0)("ativo").ToString)
    '            _userexclusao = tb1.Rows(0)("userexclusao").ToString
    '            _processado = logico(tb1.Rows(0)("processado").ToString)
    '            _usercad = tb1.Rows(0)("usercad").ToString
    '            _cobrancaunica = logico(tb1.Rows(0)("cobrancaunica").ToString)
    '            _cobrado = tb1.Rows(0)("cobrado").ToString
    '            _nrseqoriginal = tb1.Rows(0)("nrseqoriginal").ToString
    '            _duplicarvalor = logico(tb1.Rows(0)("duplicarvalor").ToString)
    '            _cpfdependente = tb1.Rows(0)("cpfdependente").ToString
    '            _usercobrado = tb1.Rows(0)("usercobrado").ToString
    '            _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
    '            _cobrarate = logico(tb1.Rows(0)("cobrarate").ToString)
    '            _nrseqdependente = tb1.Rows(0)("nrseqdependente").ToString
    '            _nrseqcliente = tb1.Rows(0)("nrseqcliente").ToString
    '            _salvo = logico(tb1.Rows(0)("salvo").ToString)
    '            _exigirvalidade = logico(tb1.Rows(0)("exigirvalidade").ToString)
    '            _exigirdependente = logico(tb1.Rows(0)("exigirdependente").ToString)
    '            _exigirdtbaixa = logico(tb1.Rows(0)("exigirdtbaixa").ToString)
    '            _useralteracao = tb1.Rows(0)("useralteracao").ToString

    '            Try
    '                If tb1.Rows(0)("dtcobranca").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobranca").ToString
    '                    _dtcobranca = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try

    '            Try
    '                If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcad").ToString
    '                    _dtcad = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtexclusao").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtexclusao").ToString
    '                    _dtexclusao = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("processadoem").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("processadoem").ToString
    '                    _processadoem = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("cobradoem").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("cobradoem").ToString
    '                    _cobradoem = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtcobrado").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobrado").ToString
    '                    _dtcobrado = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("databaixa").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("databaixa").ToString
    '                    _databaixa = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("validade").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("validade").ToString
    '                    _validade = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtcobrarate").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtcobrarate").ToString
    '                    _dtcobrarate = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try
    '            Try
    '                If tb1.Rows(0)("dtalteracao").GetType() IsNot GetType(DBNull) Then
    '                    Dim dt As Date = tb1.Rows(0)("dtalteracao").ToString
    '                    _dtalteracao = dt.ToString("yyyy-MM-dd")
    '                End If
    '            Catch exconsultasql As Exception
    '                _Mensagemerro = exconsultasql.Message
    '            End Try


    '        End If

    '        _contador = tb1.Rows.Count
    '        _table = tb1
    '        Return True
    '    Catch exconsultar As Exception
    '        _Mensagemerro = exconsultar.ToString
    '        Return False

    '    End Try
    'End Function

    'Public Function Novo() As Boolean

    '    Try

    '        'tb1 = tab1.conectar("INSERT INTO " & _tablename & " (plano, nrseqservico, valor, dtcad, usercad, cobrancaunica, nrseqoriginal, duplicarvalor, nrseqcliente, cobradoem, exigirvalidade, exigirdependente, exigirdtbaixa, usercobrado, dtcobrado, cobrado, ativo) VALUES ('" & _plano & "','" & _nrseqservico & "','" & _valor & "','" & hoje() & "','" & HttpContext.Current.Session("usuario") & "','" & _cobrancaunica & "','" & _nrseqoriginal & "','" & _duplicarvalor & "','" & _nrseqcliente & "','" & _cobradoem & "','" & _exigirvalidade & "','" & _exigirdependente & "','" & _exigirdtbaixa & "','" & _usercobrado & "','" & _dtcobrado & "','" & _cobrado & "',1")


    '        Dim wcnrseqctrl As String = gerarnrseqcontrole()

    '        tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, ativo, dtcad, usercad) values ('" & wcnrseqctrl & "',false,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "')")

    '        tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

    '        _nrseq = tb1.Rows(0)("nrseq").ToString

    '        Return True

    '    Catch exnovo As Exception
    '        _Mensagemerro = exnovo.Message
    '        Return False
    '    End Try

    'End Function

    'Public Function salvar(Optional concluir As Boolean = True) As Boolean
    '    'tb1 = tab1.conectar("select * from " & _tablename & " where matricula = '" & _matricula & "'")

    '    'If tb1.Rows.Count - 1 > 0 Then
    '    '    _Mensagemerro = "Matricula já cadastrado no sistema!"
    '    '    Return False
    '    'End If

    '    consultar()
    '    Try
    '        Dim xsql As String
    '        xsql = " update " & _tablename & " Set salvo=true, ativo = True"

    '        If _matricula <> "" Then
    '            xsql &= ",matricula = '" & tratatexto(_matricula) & "'"
    '        End If
    '        If _plano <> "" Then
    '            xsql &= ",plano = '" & tratatexto(_plano) & "'"
    '        End If
    '        If sonumeros(_nrseqservico) <> 0 Then
    '            xsql &= ",nrseqservico = " & sonumeros(_nrseqservico)
    '        End If
    '        If sonumeros(_valor) <> "" Then
    '            xsql &= ",valor = '" & sonumeros(_valor) & "'"
    '        End If
    '        If formatadatamysql(_dtcad) <> "" Then
    '            xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
    '        End If
    '        If formatadatamysql(_dtexclusao) <> "" Then
    '            xsql &= ",dtexclusao = '" & formatadatamysql(_dtexclusao) & "'"
    '        End If
    '        If _userexclusao <> "" Then
    '            xsql &= ",userexclusao = '" & tratatexto(_userexclusao) & "'"
    '        End If
    '        If logico(_processado) IsNot Nothing Then
    '            xsql &= ",processado = " & logico(_processado)
    '        End If
    '        If formatadatamysql(_processadoem) <> "" Then
    '            xsql &= ",processadoem = '" & formatadatamysql(_processadoem) & "'"
    '        End If
    '        If formatadatamysql(_cobradoem) <> "" Then
    '            xsql &= ",cobradoem = '" & formatadatamysql(_cobradoem) & "'"
    '        End If
    '        If _usercad <> "" Then
    '            xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
    '        End If
    '        If logico(_cobrancaunica) IsNot Nothing Then
    '            xsql &= ",cobrancaunica = " & logico(_cobrancaunica)
    '        End If
    '        If sonumeros(_cobrado) <> 0 Then
    '            xsql &= ",cobrado = " & sonumeros(_cobrado)
    '        End If
    '        If sonumeros(_nrseqoriginal) <> 0 Then
    '            xsql &= ",nrseqoriginal = " & sonumeros(_nrseqoriginal)
    '        End If
    '        If logico(_duplicarvalor) IsNot Nothing Then
    '            xsql &= ",duplicarvalor = " & logico(_duplicarvalor)
    '        End If
    '        If _cpfdependente <> "" Then
    '            xsql &= ",cpfdependente = '" & tratatexto(_cpfdependente) & "'"
    '        End If
    '        If _usercobrado <> "" Then
    '            xsql &= ",usercobrado = '" & tratatexto(_usercobrado) & "'"
    '        End If
    '        If formatadatamysql(_dtcobrado) <> "" Then
    '            xsql &= ",dtcobrado = '" & formatadatamysql(_dtcobrado) & "'"
    '        End If
    '        If formatadatamysql(_databaixa) <> "" Then
    '            xsql &= ",databaixa = '" & formatadatamysql(_databaixa) & "'"
    '        End If
    '        If formatadatamysql(_validade) <> "" Then
    '            xsql &= ",validade = '" & formatadatamysql(_validade) & "'"
    '        End If

    '        If logico(_cobrarate) IsNot Nothing Then
    '            xsql &= ",cobrarate = " & logico(_cobrarate)
    '        End If
    '        If formatadatamysql(_dtcobrarate) <> "" Then
    '            xsql &= ",dtcobrarate = '" & formatadatamysql(_dtcobrarate) & "'"
    '        End If
    '        If sonumeros(_nrseqdependente) <> 0 Then
    '            xsql &= ",nrseqdependente = " & sonumeros(_nrseqdependente)
    '        End If
    '        If sonumeros(_nrseqcliente) <> 0 Then
    '            xsql &= ",nrseqcliente = " & sonumeros(_nrseqcliente)
    '        End If
    '        If logico(_exigirvalidade) IsNot Nothing Then
    '            xsql &= ",exigirvalidade = " & logico(_exigirvalidade)
    '        End If
    '        If logico(_exigirdependente) IsNot Nothing Then
    '            xsql &= ",exigirdependente = " & logico(_exigirdependente)
    '        End If
    '        If logico(_exigirdtbaixa) IsNot Nothing Then
    '            xsql &= ",exigirdtbaixa = " & logico(_exigirdtbaixa)
    '        End If
    '        If formatadatamysql(_dtalteracao) <> "" Then
    '            xsql &= ",dtalteracao = '" & formatadatamysql(_dtalteracao) & "'"
    '        End If
    '        If _useralteracao <> "" Then
    '            xsql &= ",useralteracao = '" & tratatexto(_useralteracao) & "'"
    '        End If
    '        xsql &= " where nrseq = " & _nrseq
    '        tb1 = tab1.IncluirAlterarDados(xsql)
    '        consultar()

    '        Return True
    '    Catch exsalvar As Exception
    '        _Mensagemerro = exsalvar.Message
    '        Return False
    '    End Try

    'End Function

    'Public Function excluir() As Boolean

    '    Try
    '        If _nrseq = 0 Then
    '            _Mensagemerro = "Please, enter a valid Funcionario!"
    '            Return False
    '        End If

    '        consultar()

    '        tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

    '        Return True
    '    Catch exexcluir As Exception
    '        _Mensagemerro = exexcluir.ToString
    '        Return False

    '    End Try

    'End Function

End Class
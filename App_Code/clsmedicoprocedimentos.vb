﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart

Public Class clsmedicosprocedimentos
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _cod As String
    Dim _descricao As String
    Dim _valorfinal As Decimal
    Dim _plano As String = ""
    Dim _valorcusto As Decimal = 0
    Dim _convenio As String
    Dim _procedimento As String
    Dim _dtcad As Date
    Dim _usercad As String = ""
    Dim _dtaltera As Date
    Dim _useraltera As String = ""
    Dim _ativo As Boolean = True
    Dim _dtexclui As Date
    Dim _userexclui As String = ""
    Dim _codigoamb As String = ""
    Dim _codigosus As String = ""
    Dim _naoincluircaixa As Boolean = False
    Dim _percobplano As Decimal = 0
    Dim _valorcobplano As Decimal = 0
    Dim _nrseqctrl As String = ""
    Dim _valorpaciente As Decimal = 0
    Dim _nrseqempresa As Integer = 0
    Dim _descricaoconvenio As String = ""
    Dim _perccobpaciente As Decimal = 0
    Dim _listaprocedimentos As New List(Of clsProcedimentos)
    Dim tbprocedimentos As New Data.DataTable
    Dim _chave As String = ""
    Dim _obs As String = ""
    Dim _nrseqprocedimento As Integer = 0
    Dim _nrseqconvenio As Integer = 0
    Dim _nrseqagenda As Integer = 0
    Dim _valormp As Decimal = 0


    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Cod As String
        Get
            Return _cod
        End Get
        Set(value As String)
            _cod = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Valorfinal As Decimal
        Get
            Return _valorfinal
        End Get
        Set(value As Decimal)
            _valorfinal = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Valorcusto As Decimal
        Get
            Return _valorcusto
        End Get
        Set(value As Decimal)
            _valorcusto = value
        End Set
    End Property

    Public Property Convenio As String
        Get
            Return _convenio
        End Get
        Set(value As String)
            _convenio = value
        End Set
    End Property

    Public Property Procedimento As String
        Get
            Return _procedimento
        End Get
        Set(value As String)
            _procedimento = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtaltera As Date
        Get
            Return _dtaltera
        End Get
        Set(value As Date)
            _dtaltera = value
        End Set
    End Property

    Public Property Useraltera As String
        Get
            Return _useraltera
        End Get
        Set(value As String)
            _useraltera = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Codigoamb As String
        Get
            Return _codigoamb
        End Get
        Set(value As String)
            _codigoamb = value
        End Set
    End Property

    Public Property Codigosus As String
        Get
            Return _codigosus
        End Get
        Set(value As String)
            _codigosus = value
        End Set
    End Property

    Public Property Naoincluircaixa As Boolean
        Get
            Return _naoincluircaixa
        End Get
        Set(value As Boolean)
            _naoincluircaixa = value
        End Set
    End Property

    Public Property Percobplano As Decimal
        Get
            Return _percobplano
        End Get
        Set(value As Decimal)
            _percobplano = value
        End Set
    End Property

    Public Property Valorcobplano As Decimal
        Get
            Return _valorcobplano
        End Get
        Set(value As Decimal)
            _valorcobplano = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Valorpaciente As Decimal
        Get
            Return _valorpaciente
        End Get
        Set(value As Decimal)
            _valorpaciente = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Descricaoconvenio As String
        Get
            Return _descricaoconvenio
        End Get
        Set(value As String)
            _descricaoconvenio = value
            If value <> "" Then
                tb1 = tab1.IncluirAlterarDados("select * from tbconvenios where ativo = true and nome = '" & Descricaoconvenio & "'")
                If tb1.Rows.Count > 0 Then
                    _convenio = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Perccobpaciente As Decimal
        Get
            Return _perccobpaciente
        End Get
        Set(value As Decimal)
            _perccobpaciente = value
        End Set
    End Property

    Public Property Listaprocedimentos As List(Of clsProcedimentos)
        Get
            Return _listaprocedimentos
        End Get
        Set(value As List(Of clsProcedimentos))
            _listaprocedimentos = value
        End Set
    End Property

    Public Property Tbprocedimentos1 As DataTable
        Get
            Return tbprocedimentos
        End Get
        Set(value As DataTable)
            tbprocedimentos = value
        End Set
    End Property

    Public Property Chave As String
        Get
            Return _chave
        End Get
        Set(value As String)
            _chave = value
        End Set
    End Property

    Public Property Nrseqprocedimento As Integer
        Get
            Return _nrseqprocedimento
        End Get
        Set(value As Integer)
            _nrseqprocedimento = value
        End Set
    End Property

    Public Property Nrseqconvenio As Integer
        Get
            Return _nrseqconvenio
        End Get
        Set(value As Integer)
            _nrseqconvenio = value
        End Set
    End Property

    Public Property Nrseqagenda As Integer
        Get
            Return _nrseqagenda
        End Get
        Set(value As Integer)
            _nrseqagenda = value
        End Set
    End Property

    Public Property Valormp As Decimal
        Get
            Return _valormp
        End Get
        Set(value As Decimal)
            _valormp = value
        End Set
    End Property

    Public Property Obs As String
        Get
            Return _obs
        End Get
        Set(value As String)
            _obs = value
        End Set
    End Property
End Class

Partial Public Class clsmedicosprocedimentos

    Public Function procurar() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbagendamedica_procedimentos where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Cod = tb1.Rows(0)("cod").ToString
        Descricao = tb1.Rows(0)("descricao").ToString
        Valorfinal = tb1.Rows(0)("valorfinal").ToString
        Plano = tb1.Rows(0)("plano").ToString
        Valorcusto = tb1.Rows(0)("valorcusto").ToString
        Convenio = tb1.Rows(0)("convenio").ToString
        Dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
        Usercad = tb1.Rows(0)("usercad").ToString
        Dtaltera = valordata(tb1.Rows(0)("dtaltera").ToString)
        Useraltera = tb1.Rows(0)("useraltera").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Dtexclui = valordata(tb1.Rows(0)("dtexclui").ToString)
        Userexclui = tb1.Rows(0)("userexclui").ToString
        Codigoamb = tb1.Rows(0)("codigoamb").ToString
        Codigosus = tb1.Rows(0)("codigosus").ToString
        Naoincluircaixa = tb1.Rows(0)("naoincluircaixa").ToString
        Percobplano = tb1.Rows(0)("percobplano").ToString
        Valorcobplano = tb1.Rows(0)("valorcobplano").ToString
        Nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        Valorpaciente = tb1.Rows(0)("valorpaciente").ToString
        Nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbagendamedica_procedimentos Set ativo = True, Naoincluircaixa = " & logico(Naoincluircaixa)

            If Cod <> "" Then
                xsql &= ",cod = '" & tratatexto(Cod) & "'"
            End If
            If Descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(Descricao) & "'"
            End If
            If Valorfinal <> 0 Then
                xsql &= ",valorfinal = " & moeda(Valorfinal)
            End If
            If Plano <> "" Then
                xsql &= ",plano = '" & tratatexto(Plano) & "'"
            End If
            If Valorcusto <> 0 Then
                xsql &= ",valorcusto = " & moeda(Valorcusto)
            End If
            If _perccobpaciente <> 0 Then
                xsql &= ",perccobpaciente = " & moeda(_perccobpaciente)
            End If


            If Convenio <> 0 Then
                xsql &= ",convenio = " & moeda(Convenio)
            End If

            If Codigoamb <> "" Then
                xsql &= ",codigoamb = '" & tratatexto(Codigoamb) & "'"
            End If
            If Codigosus <> "" Then
                xsql &= ",codigosus = '" & tratatexto(Codigosus) & "'"
            End If

            If Percobplano <> 0 Then
                xsql &= ",percobplano = " & moeda(Percobplano) & ""
            End If
            If Valorcobplano <> 0 Then
                xsql &= ",valorcobplano = " & moeda(Valorcobplano)
            End If
            If Nrseqctrl <> "" Then
                xsql &= ",nrseqctrl = '" & tratatexto(Nrseqctrl) & "'"
            End If
            If Valorpaciente <> 0 Then
                xsql &= ",valorpaciente = " & moeda(Valorpaciente)
            End If
            If Nrseqempresa <> 0 Then
                xsql &= ",nrseqempresa = " & moeda(Nrseqempresa)
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Mensagemerro = "Procedimento salvo com sucesso !"
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function carregarprocedimentosmodal() As Boolean


        Try

            tb1 = tab1.conectar("select P.descricao AS Procedimento ,  C.nome AS Convenio , AP.chave AS chave, AP.nrseq AS nrseq, AP.valor As Valor from tbagendamedica_procedimentos AS AP join  tbprocedimentos  AS P on AP.nrseqprocedimento = P.nrseq join  tbconvenios  AS C on AP.nrseqconvenio = C.nrseq where AP.chave = '" & _chave & "'")

            Nrseq = tb1.Rows(0)("AP.nrseq").ToString
            Convenio = tb1.Rows(0)("C.nome").ToString
            Procedimento = tb1.Rows(0)("AP.procedimento").ToString
            Valorfinal = tb1.Rows(0)("AP.Valor").ToString
            Chave = tb1.Rows(0)("AP.chave").ToString
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function novamp() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbagendamedica_procedimentos (nrseqctrl, dtcad, usercad, ativo, nrseqprocedimento, nrseqconvenio , valor , chave, observacao) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false ,'" & Nrseqprocedimento & "','" & Nrseqconvenio & "','" & Valormp & "','" & Chave & "','" & Obs & "')")
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbagendamedica_procedimentos set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function deletar1() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("delete from tbagendamedica_procedimentos  where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function deletartodos() As Boolean
        Try
            If Chave = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("delete from tbagendamedica_procedimentos  where chave = " & Chave)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

End Class


﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart

Public Class clsProntuarios_padroes
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _tablename As String = "tbprontuarios_padroes"
    Dim _table As Data.DataTable
    Dim _contador As Integer = 0
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _descricao As String
    Dim _texto As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo As Boolean = False
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _nrseqctrl As String

    Public Property table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Texto As String
        Get
            Return _texto
        End Get
        Set(value As String)
            _texto = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property
End Class

Partial Public Class clsProntuarios_padroes

    Public Function procurar() As Boolean
        If _nrseq = 0 Then
            _mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tabela where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            _mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _descricao = tb1.Rows(0)("descricao").ToString
        _texto = tb1.Rows(0)("texto").ToString
        _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
        _usercad = tb1.Rows(0)("usercad").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        _dtexclui = valordata(tb1.Rows(0)("dtexclui").ToString)
        _userexclui = tb1.Rows(0)("userexclui").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & _tablename & " Set ativo = True"
            If _descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(_descricao) & "'"
            End If
            If _texto <> "" Then
                xsql &= ",texto = '" & tratatexto(_texto) & "'"
            End If
            If _dtcad <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If logico(_ativo) IsNot Nothing Then
                xsql &= ",ativo = " & logico(_ativo)
            End If
            If _dtexclui <> "" Then
                xsql &= ",dtexclui = '" & formatadatamysql(_dtexclui) & "'"
            End If
            If _userexclui <> "" Then
                xsql &= ",userexclui = '" & tratatexto(_userexclui) & "'"
            End If
            If _nrseqctrl <> "" Then
                xsql &= ",nrseqctrl = '" & tratatexto(_nrseqctrl) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole("")
            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbtabela where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


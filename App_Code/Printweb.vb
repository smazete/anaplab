﻿Imports System.Data
Imports System.Configuration
Imports System.Linq
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Xml.Linq
Imports System.IO
Imports System.Text

''' <summary>
''' Summary description for Printweb
''' </summary>
Public Class Printweb
    Public Sub New()
    End Sub
    Public Shared Sub PrintWebControl(ctrl As Control)
        PrintWebControl(ctrl, String.Empty)
    End Sub
    Public Shared Sub PrintWebControl(ctrl As Control, Script As String)
        Dim stringWrite As New StringWriter()
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        If TypeOf ctrl Is WebControl Then
            Dim w As New Unit(100, UnitType.Percentage)
            DirectCast(ctrl, WebControl).Width = w
        End If
        Dim pg As New Page()
        pg.EnableEventValidation = False
        If Script <> String.Empty Then
            pg.ClientScript.RegisterStartupScript(pg.[GetType](), "PrintJavaScript", Script)
        End If
        Dim frm As New HtmlForm()
        pg.Controls.Add(frm)
        frm.Attributes.Add("runat", "server")
        frm.Controls.Add(ctrl)
        pg.DesignerInitialize()
        pg.RenderControl(htmlWrite)
        Dim strHTML As String = stringWrite.ToString()
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Write(strHTML)
        HttpContext.Current.Response.Write("<script>window.print();</script>")
        HttpContext.Current.Response.[End]()
    End Sub
End Class
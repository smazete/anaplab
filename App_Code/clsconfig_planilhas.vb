﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsconfig_planilhas

    'Desenvolvedor: Claudio Smart
    'Data inicial :  03/09/2019

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim _grade As New Data.DataTable
    Dim _listacampos As New List(Of clsconfig_planilhas_campos)
    Dim _listacampostabela As New List(Of String)

    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _padrao As String = ""
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String

    Public Sub New()
        _usercad = buscarsessoes("usuario")
        _dtcad = data()

        tb1 = tab1.conectar("select * from vwcolaboradores limit 1")

        For x As Integer = 0 To tb1.Columns.Count - 1
            _listacampostabela.Add(tb1.Columns(x).ColumnName.ToLower)
        Next

    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Padrao As String
        Get
            Return _padrao
        End Get
        Set(value As String)
            _padrao = tratatexto(value,, 45)
            If _padrao <> "" Then
                tbx = tabx.conectar("select * from tbconfig_planilhas where padrao = '" & _padrao & "'")
                If tbx.Rows.Count > 0 Then
                    _nrseq = tbx.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Listacampos As List(Of clsconfig_planilhas_campos)
        Get
            Return _listacampos
        End Get
        Set(value As List(Of clsconfig_planilhas_campos))
            _listacampos = value
        End Set
    End Property

    Public Property Grade As DataTable
        Get
            Return _grade
        End Get
        Set(value As DataTable)
            _grade = value
        End Set
    End Property
End Class
Partial Public Class clsconfig_planilhas

    Public Function salvar() As Boolean
        Try
            tb1 = tab1.conectar("select * from tbconfig_planilhas where padrao = '" & _padrao & "'")
            If tb1.Rows.Count = 0 Then
                Dim wcnrseqctrl As String = gerarnrseqcontrole()
                tb1 = tab1.IncluirAlterarDados("insert into tbconfig_planilhas (padrao, ativo, usercad, dtcad, nrseqctrl) values ('" & _padrao & "', true, '" & _usercad & "', '" & hoje() & "','" & wcnrseqctrl & "')")
                tb1 = tab1.conectar("select * from tbconfig_planilhas where nrseqctrl = '" & wcnrseqctrl & "'")
                _nrseq = tb1.Rows(0)("nrseq").ToString
            Else
                _nrseq = tb1.Rows(0)("nrseq").ToString
                tb1 = tab1.IncluirAlterarDados("update tbconfig_planilhas set ativo = true where padrao = '" & _padrao & "'")
            End If

            tb1 = tab1.conectar("select * from tbconfig_planilhas_campos where nrseqconfig = " & _nrseq)
            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim xcamp As New clsconfig_planilhas_campos
                xcamp.Ativo = logico(tb1.Rows(x)("ativo").ToString)
                xcamp.Campo = tb1.Rows(x)("campo").ToString
                xcamp.Planilha = tb1.Rows(x)("planilha").ToString

                _listacampos.Add(xcamp)
            Next
            If _listacampos.Count = 0 Then

                For y As Integer = 0 To _listacampostabela.Count - 1



                    tb1 = tab1.IncluirAlterarDados("insert into tbconfig_planilhas_campos (nrseqconfig, campo, planilha, ativo) values (" & _nrseq & ",'" & _listacampostabela(y) & "', '', true)")
                Next

            Else
                For y As Integer = 0 To _listacampostabela.Count - 1

                    Dim campoexiste As Boolean = False
                    For x As Integer = 0 To _listacampos.Count - 1
                        If _listacampos(x).Ativo AndAlso (_listacampos(x).Campo = _listacampostabela(y)) Then
                            campoexiste = True
                            Exit For
                        End If
                    Next
                    If Not campoexiste Then
                        tb1 = tab1.IncluirAlterarDados("insert into tbconfig_planilhas_campos (nrseqconfig, campo, planilha, ativo) values (" & _nrseq & ",'" & _listacampostabela(y) & "', '', true)")
                    End If
                Next
            End If

            Return True
        Catch exsalvar As Exception
            Return False
        End Try
    End Function
    Public Function AdicionaCampoPlanilha() As Boolean
        Try

            For z As Integer = 0 To _listacampos.Count - 1

                tb1 = tab1.IncluirAlterarDados("update tbconfig_planilhas_campos set planilha = '" & _listacampos(z).Planilha.Trim & "' where nrseqconfig = " & _nrseq & " and campo = '" & _listacampos(z).Campo.Trim & "'")
            Next
            _mensagemerro = "Campos salvos com sucesso !"
            Return True
        Catch exaddcampo As Exception
            _mensagemerro = exaddcampo.Message
            Return False
        End Try

    End Function
    Public Function carregargrade() As Boolean

        Try
            _grade = tab1.conectar("select * from vwconfig_planilhas where padrao = '" & _padrao & "' and ativo = true order by campo")

            Return True
        Catch exgrade As Exception
            Return False
        End Try

    End Function
End Class
﻿Imports Microsoft.VisualBasic

Public Class clsmedicos_horarios
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nrseqmedico As Integer = 0
    Dim _hora As String
    Dim _ativo As Boolean
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _diasemana As Integer = 0

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqmedico As Integer
        Get
            Return _nrseqmedico
        End Get
        Set(value As Integer)
            _nrseqmedico = value
        End Set
    End Property

    Public Property Hora As String
        Get
            Return _hora
        End Get
        Set(value As String)
            _hora = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Diasemana As Integer
        Get
            Return _diasemana
        End Get
        Set(value As Integer)
            _diasemana = value
        End Set
    End Property
End Class

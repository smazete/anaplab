﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart

Public Class clsdocassociados

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _nome As String
    Dim _arquivo As String
    Dim _matricula As String
    Dim _ativo As Boolean
    Dim _dtexcluir As Date
    Dim _userexcluir As String
    Dim _nrseqctrl As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexcluir As Date
        Get
            Return _dtexcluir
        End Get
        Set(value As Date)
            _dtexcluir = value
        End Set
    End Property

    Public Property Userexcluir As String
        Get
            Return _userexcluir
        End Get
        Set(value As String)
            _userexcluir = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property
End Class

Partial Public Class clsdocassociados

    Public Function procurar() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbdocumentosassociados where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _usercad = tb1.Rows(0)("usercad").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _arquivo = tb1.Rows(0)("arquivo").ToString
        _matricula = tb1.Rows(0)("matricula").ToString
        _ativo = tb1.Rows(0)("ativo").ToString
        _dtexcluir = FormatDateTime(tb1.Rows(0)("dtexcluir").ToString, DateFormat.ShortDate)
        _userexcluir = tb1.Rows(0)("userexcluir").ToString
        _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function novosalvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbdocumentosassociados Set ativo = True "

            If _nome <> "" Then
                xsql &= ", nome = '" & tratatexto(_nome) & "'"
            End If
            If _arquivo <> "" Then
                xsql &= ", arquivo = '" & tratatexto(_arquivo) & "'"
            End If
            If _matricula <> "" Then
                xsql &= ", matricula = '" & tratatexto(_matricula) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean

        Try
            Dim xsql As String
            xsql = " update tbdocumentosassociados Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _dtcad <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _nome <> "" Then
                xsql &= ",nome = '" & tratatexto(_nome) & "'"
            End If
            If _arquivo <> "" Then
                xsql &= ",arquivo = '" & tratatexto(_arquivo) & "'"
            End If
            If _matricula <> "" Then
                xsql &= ",matricula = '" & tratatexto(_matricula) & "'"
            End If
            If _ativo <> 0 Then
                xsql &= ",ativo = " & moeda(_ativo)
            End If
            If _dtexcluir <> "" Then
                xsql &= ",dtexcluir = '" & formatadatamysql(_dtexcluir) & "'"
            End If
            If _userexcluir <> "" Then
                xsql &= ",userexcluir = '" & tratatexto(_userexcluir) & "'"
            End If
            If _nrseqctrl <> "" Then
                xsql &= ",nrseqctrl = '" & tratatexto(_nrseqctrl) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbdocumentosassociados (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbdocumentosassociados where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.conectar("select * from tbdocumentosassociados where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbdocumentosassociados set ativo = false, dtexcluir = '" & hoje() & "', userexcluir = '" & _usercad & "' where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class

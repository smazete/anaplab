﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsEmpresas


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbempresas"
    Dim _nrseq As Integer
    Dim _cnpj As String
    Dim _nome As String
    Dim _ativo As Boolean
    Dim _usercad As String
    Dim _dtcad As DateTime
    Dim _userexclui As String
    Dim _dtexclui As DateTime
    Dim _salvo As Boolean
    Dim _nrseqctrl As String
    Dim _telefone As String
    Dim _endereco As String
    Dim _cidade As String
    Dim _uf As String

    Dim _mensagemerro As String
    Dim _contador As Integer




    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Cnpj As String
        Get
            Return _cnpj
        End Get
        Set(value As String)
            _cnpj = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Salvo As Boolean
        Get
            Return _salvo
        End Get
        Set(value As Boolean)
            _salvo = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Telefone As String
        Get
            Return _telefone
        End Get
        Set(value As String)
            _telefone = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Uf As String
        Get
            Return _uf
        End Get
        Set(value As String)
            _uf = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property
End Class
Partial Public Class clsEmpresas

    Public Function consultar() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE salvo=true and ativo = true AND nrseq = '" & _nrseq & "' ")

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _cnpj = tb1.Rows(0)("cnpj").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _salvo = tb1.Rows(0)("salvo").ToString
                _telefone = tb1.Rows(0)("telefone").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _uf = tb1.Rows(0)("uf").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function procurarempresas(Optional nome As String = "", Optional nrseq As Integer = 0, Optional inativos As Boolean = False) As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where salvo = true " & IIf(nome <> "", " and nome Like '%" & nome & "%'", "") & IIf(nrseq <> 0, " and nrseq Like '" & nrseq & "%'", "") & IIf(inativos = True, " and ativo = false", " and ativo = true"))


            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _cnpj = tb1.Rows(0)("cnpj").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _salvo = tb1.Rows(0)("salvo").ToString
                _telefone = tb1.Rows(0)("telefone").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _uf = tb1.Rows(0)("uf").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try


    End Function


    Public Function consultarsalvos() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE salvo=true and ativo = true order by nome")

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _cnpj = tb1.Rows(0)("cnpj").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _salvo = tb1.Rows(0)("salvo").ToString
                _telefone = tb1.Rows(0)("telefone").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _uf = tb1.Rows(0)("uf").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarcnpj() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " salvo=true and where ativo = '1' and cnpj = '" & _cnpj & "'")

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _cnpj = tb1.Rows(0)("cnpj").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _salvo = logico(tb1.Rows(0)("salvo").ToString)
                _telefone = tb1.Rows(0)("telefone").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _uf = tb1.Rows(0)("uf").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function carregarempresa() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE salvo=true and ativo = true AND nrseq = '" & HttpContext.Current.Session("idempresaemuso") & "' ")

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _cnpj = tb1.Rows(0)("cnpj").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _salvo = logico(tb1.Rows(0)("salvo").ToString)
                _telefone = tb1.Rows(0)("telefone").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _uf = tb1.Rows(0)("uf").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarnome(Optional distintos As Boolean = False) As Boolean

        Try

            If distintos = True Then
                tb1 = tab1.conectar("Select distinct " & _tablename & ".nome from tbempresas where salvo=true and nrseq = '" & _nrseq & "'")
            Else
                tb1 = tab1.conectar("Select * from " & _tablename & " Where nome Like '" & _nome & "%' and ativo= " & Ativo & "")
            End If

            If tb1.Rows.Count > 0 AndAlso distintos = False Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _cnpj = tb1.Rows(0)("cpnj").ToString
                _nome = tb1.Rows(0)("nome").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _salvo = logico(tb1.Rows(0)("salvo").ToString)
                _telefone = tb1.Rows(0)("telefone").ToString
                _endereco = tb1.Rows(0)("endereco").ToString
                _cidade = tb1.Rows(0)("cidade").ToString
                _uf = tb1.Rows(0)("uf").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultartodos(Optional ativo As Boolean = True) As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & _tablename & " where ativo=" & ativo & " and salvo=true and nrseq = '" & _nrseq & "'")

            If tb1.Rows.Count > 0 Then
                _contador = tb1.Rows.Count
                _table = tb1
            Else
                _mensagemerro = "Nenhuma empresa encontrada"
            End If

            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarempresa() As Boolean

        Try

            tb1 = tab1.conectar("select distinct " & _tablename & ".nome from " & _tablename & " where salvo=true and nrseq = '" & _nrseq & "'")


            If tb1.Rows.Count > 0 Then

                _nome = tb1.Rows(0)("nome").ToString

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, ativo,salvo, dtcad, usercad) values ('" & wcnrseqctrl & "',false,false,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "')")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try
            tb1 = tab1.conectar("select * from " & _tablename & " where cnpj = '" & _cnpj & "'")

            If tb1.Rows.Count > 1 Then
                _mensagemerro = "CNPJ já em uso no sistema!"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo=1, cnpj='" & _cnpj & "',nome='" & _nome & "',ativo=1,salvo=true,telefone='" & _telefone & "',endereco='" & _endereco & "',uf='" & _uf & "',cidade='" & _cidade & "' where nrseq= " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _mensagemerro = "Please, enter a valid Funcionario!"
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(_ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)
            consultarsalvos()
            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.ToString
            Return False

        End Try


    End Function

End Class
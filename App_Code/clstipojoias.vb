﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clstipojoias

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _listaclasse As New List(Of clstipojoias)
    Dim _nrseq As Integer
    Dim _ativo As Boolean
    Dim _descricao As String
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _valor As Integer
    Dim _dtexclui As Date
    Dim _userexclui As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Listaclasse As List(Of clstipojoias)
        Get
            Return _listaclasse
        End Get
        Set(value As List(Of clstipojoias))
            _listaclasse = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Valor As Integer
        Get
            Return _valor
        End Get
        Set(value As Integer)
            _valor = value
        End Set
    End Property
End Class

Partial Public Class clstipojoias

    Public Function Listartipojoias() As Boolean
        Try
            Listaclasse.Clear()
            tb1 = tab1.conectar("select * from tbtipojoias where ativo = true")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaclasse.Add(New clstipojoias With {.Nrseq = numeros(tb1.Rows(x)("nrseq").ToString), .Ativo = tb1.Rows(x)("ativo").ToString, .Descricao = tb1.Rows(x)("descricao").ToString, .Dtcad = valordata(tb1.Rows(x)("dtcad").ToString), .Dtexclui = valordata(tb1.Rows(x)("dtexclui").ToString), .Userexclui = tb1.Rows(x)("userexclui").ToString})
            Next

            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function procurar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbtipojoias where nrseq = " & Nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            Nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
            Ativo = logico(tb1.Rows(0)("ativo").ToString)
            Descricao = tb1.Rows(0)("descricao").ToString
            Dtcad = FormatDateTime(valordata(tb1.Rows(0)("dtcad").ToString), DateFormat.ShortDate)
            Dtexclui = FormatDateTime(valordata(tb1.Rows(0)("dtexclui").ToString), DateFormat.ShortDate)
            Userexclui = tb1.Rows(0)("userexclui").ToString
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbtipojoias Set ativo = True"
            If Ativo <> True Then
                xsql &= ",ativo = '" & logico(Ativo) & "'"
            End If
            If Valor <> "" Then
                xsql &= ", valor = '" & moeda(Valor) & "'"
            End If
            If Descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(Descricao) & "'"
            End If
            If Dtcad <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(Dtcad) & "'"
            End If
            If Dtexclui <> "" Then
                xsql &= ",dtexclui = '" & formatadatamysql(Dtexclui) & "'"
            End If
            If Userexclui <> "" Then
                xsql &= ",userexclui = '" & tratatexto(Userexclui) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbtipojoias (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbtipojoias where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbtipojoias set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class

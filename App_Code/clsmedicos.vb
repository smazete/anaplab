﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data

Public Class clsmedicos


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco

    Dim _table As Data.DataTable

    Dim _tablename As String = "tbmedicos"
    Dim _mensagemerro As String
    Dim _contador As Integer
    Dim _cod As Integer
    Dim _nome As String
    Dim _endereco As String
    Dim _bairro As String
    Dim _estado As String
    Dim _cep As String
    Dim _tel1 As String
    Dim _tel2 As String
    Dim _celular As String
    Dim _salario As Decimal
    Dim _cidade As String
    Dim _dtinclusao As Date
    Dim _userinc As String
    Dim _horasem As Integer
    Dim _cpf As String
    Dim _rg As String
    Dim _crm As String
    Dim _permitir As Integer
    Dim _ativo As Boolean
    Dim _login As String
    Dim _email As String
    Dim _cepcomercial As String
    Dim _cidadecomercial As String
    Dim _estadocomercial As String
    Dim _bairrocomercial As String
    Dim _enderecocomercial As String
    Dim _vacinas As Integer
    Dim _dtnasc As Date
    Dim _porcclinica As Decimal
    Dim _porcprofissional As Decimal
    Dim _nometipon As String
    Dim _enviadosite As Integer
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _tipon_gerenciapropria As Integer
    Dim _tipon_hora As String
    Dim _naoenviartipon As Integer
    Dim _arqassinatura As Integer
    Dim _impassinaturaaso As String
    Dim _usercad As String = ""
    Dim _dtcad As DateTime = data()
    Dim _ufcrm As String
    Dim _cbo As String
    Dim _ignorarqtdconsultasplano As Integer
    Dim _limitesconsultas As Integer
    Dim _nrseqctrl As String
    Dim _nrseq As String
    Dim _nrseqempresa As Integer
    Dim _numero As String
    Dim _numerocomercial As String
    Dim _listahorarios As New List(Of clsmedicos_horarios)
    Dim _listaespecialiadades As New List(Of clsMedicosEspecialidades)
    Dim _listamedicos As New List(Of clsmedicos)



    Public Sub New()
        '       versessao()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub
    Public Sub New(xusuario As String)
        '       versessao()
        _usercad = xusuario
        _dtcad = data()
    End Sub
    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property


    Public Property Cod As Integer
        Get
            Return _cod
        End Get
        Set(value As Integer)
            _cod = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Estado As String
        Get
            Return _estado
        End Get
        Set(value As String)
            _estado = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Tel1 As String
        Get
            Return _tel1
        End Get
        Set(value As String)
            _tel1 = value
        End Set
    End Property

    Public Property Tel2 As String
        Get
            Return _tel2
        End Get
        Set(value As String)
            _tel2 = value
        End Set
    End Property

    Public Property Celular As String
        Get
            Return _celular
        End Get
        Set(value As String)
            _celular = value
        End Set
    End Property

    Public Property Salario As Decimal
        Get
            Return _salario
        End Get
        Set(value As Decimal)
            _salario = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Dtinclusao As Date
        Get
            Return _dtinclusao
        End Get
        Set(value As Date)
            _dtinclusao = value
        End Set
    End Property

    Public Property Userinc As String
        Get
            Return _userinc
        End Get
        Set(value As String)
            _userinc = value
        End Set
    End Property

    Public Property Horasem As Integer
        Get
            Return _horasem
        End Get
        Set(value As Integer)
            _horasem = value
        End Set
    End Property

    Public Property Cpf As String
        Get
            Return _cpf
        End Get
        Set(value As String)
            _cpf = value
        End Set
    End Property

    Public Property Rg As String
        Get
            Return _rg
        End Get
        Set(value As String)
            _rg = value
        End Set
    End Property

    Public Property Crm As String
        Get
            Return _crm
        End Get
        Set(value As String)
            _crm = value
        End Set
    End Property

    Public Property Permitir As Integer
        Get
            Return _permitir
        End Get
        Set(value As Integer)
            _permitir = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Login As String
        Get
            Return _login
        End Get
        Set(value As String)
            _login = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Cepcomercial As String
        Get
            Return _cepcomercial
        End Get
        Set(value As String)
            _cepcomercial = value
        End Set
    End Property

    Public Property Cidadecomercial As String
        Get
            Return _cidadecomercial
        End Get
        Set(value As String)
            _cidadecomercial = value
        End Set
    End Property

    Public Property Estadocomercial As String
        Get
            Return _estadocomercial
        End Get
        Set(value As String)
            _estadocomercial = value
        End Set
    End Property

    Public Property Bairrocomercial As String
        Get
            Return _bairrocomercial
        End Get
        Set(value As String)
            _bairrocomercial = value
        End Set
    End Property

    Public Property Enderecocomercial As String
        Get
            Return _enderecocomercial
        End Get
        Set(value As String)
            _enderecocomercial = value
        End Set
    End Property

    Public Property Vacinas As Integer
        Get
            Return _vacinas
        End Get
        Set(value As Integer)
            _vacinas = value
        End Set
    End Property

    Public Property Dtnasc As Date
        Get
            Return _dtnasc
        End Get
        Set(value As Date)
            _dtnasc = value
        End Set
    End Property

    Public Property Porcclinica As Decimal
        Get
            Return _porcclinica
        End Get
        Set(value As Decimal)
            _porcclinica = value
        End Set
    End Property

    Public Property Porcprofissional As Decimal
        Get
            Return _porcprofissional
        End Get
        Set(value As Decimal)
            _porcprofissional = value
        End Set
    End Property

    Public Property Nometipon As String
        Get
            Return _nometipon
        End Get
        Set(value As String)
            _nometipon = value
        End Set
    End Property

    Public Property Enviadosite As Integer
        Get
            Return _enviadosite
        End Get
        Set(value As Integer)
            _enviadosite = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Tipon_gerenciapropria As Integer
        Get
            Return _tipon_gerenciapropria
        End Get
        Set(value As Integer)
            _tipon_gerenciapropria = value
        End Set
    End Property

    Public Property Tipon_hora As String
        Get
            Return _tipon_hora
        End Get
        Set(value As String)
            _tipon_hora = value
        End Set
    End Property

    Public Property Naoenviartipon As Integer
        Get
            Return _naoenviartipon
        End Get
        Set(value As Integer)
            _naoenviartipon = value
        End Set
    End Property

    Public Property Arqassinatura As Integer
        Get
            Return _arqassinatura
        End Get
        Set(value As Integer)
            _arqassinatura = value
        End Set
    End Property

    Public Property Impassinaturaaso As String
        Get
            Return _impassinaturaaso
        End Get
        Set(value As String)
            _impassinaturaaso = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Ufcrm As String
        Get
            Return _ufcrm
        End Get
        Set(value As String)
            _ufcrm = value
        End Set
    End Property

    Public Property Cbo As String
        Get
            Return _cbo
        End Get
        Set(value As String)
            _cbo = value
        End Set
    End Property

    Public Property Ignorarqtdconsultasplano As Integer
        Get
            Return _ignorarqtdconsultasplano
        End Get
        Set(value As Integer)
            _ignorarqtdconsultasplano = value
        End Set
    End Property

    Public Property Limitesconsultas As Integer
        Get
            Return _limitesconsultas
        End Get
        Set(value As Integer)
            _limitesconsultas = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Nrseq As String
        Get
            Return _nrseq
        End Get
        Set(value As String)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Numerocomercial As String
        Get
            Return _numerocomercial
        End Get
        Set(value As String)
            _numerocomercial = value
        End Set
    End Property

    Public Property tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Listahorarios As List(Of clsmedicos_horarios)
        Get
            Return _listahorarios
        End Get
        Set(value As List(Of clsmedicos_horarios))
            _listahorarios = value
        End Set
    End Property

    Public Property Listaespecialiadades As List(Of clsMedicosEspecialidades)
        Get
            Return _listaespecialiadades
        End Get
        Set(value As List(Of clsMedicosEspecialidades))
            _listaespecialiadades = value
        End Set
    End Property

    Public Property Listamedicos As List(Of clsmedicos)
        Get
            Return _listamedicos
        End Get
        Set(value As List(Of clsmedicos))
            _listamedicos = value
        End Set
    End Property


End Class

Partial Public Class clsmedicos
    Public Function listarmedicos() As Boolean
        Try
            _listamedicos.Clear()
            tb1 = tab1.conectar("select * from tbmedicos where ativo = true order by nome")
            For x As Integer = 0 To tb1.Rows.Count - 1
                _listamedicos.Add(New clsmedicos With {.Nome = tb1.Rows(x)("nome").ToString})
            Next


            Return True
        Catch exlistar As Exception
            _mensagemerro = exlistar.Message
            Return False
        End Try

    End Function
    Public Function procurar() As Boolean
        If _cod = 0 Then
            Mensagemerro = " Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & tablename & " where cod = '" & _cod & "'")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = " Selecione um item válido para procurar"
            Return False
        End If
        _cod = tb1.Rows(0)("cod").ToString
        _nrseq = tb1.Rows(0)("cod").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _estado = tb1.Rows(0)("estado").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _tel1 = tb1.Rows(0)("tel1").ToString
        _tel2 = tb1.Rows(0)("tel2").ToString
        _celular = tb1.Rows(0)("celular").ToString
        '     _salario = tb1.Rows(0)("salario").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        '     _dtinclusao = FormatDateTime(tb1.Rows(0)("dtinclusao").ToString, DateFormat.ShortDate)
        '    _userinc = tb1.Rows(0)("userinc").ToString
        '   _horasem = tb1.Rows(0)("horasem").ToString
        _cpf = tb1.Rows(0)("cpf").ToString
        '  _rg = tb1.Rows(0)("rg").ToString
        _crm = tb1.Rows(0)("crm").ToString
        ''     _permitir = tb1.Rows(0)("permitir").ToString
        _ativo = logico(tb1.Rows(0)("ativo").ToString)
        ''   _login = tb1.Rows(0)("login").ToString
        _email = tb1.Rows(0)("email").ToString
        _cepcomercial = tb1.Rows(0)("cepcomercial").ToString
        _cidadecomercial = tb1.Rows(0)("cidadecomercial").ToString
        _estadocomercial = tb1.Rows(0)("estadocomercial").ToString
        _bairrocomercial = tb1.Rows(0)("bairrocomercial").ToString
        _enderecocomercial = tb1.Rows(0)("enderecocomercial").ToString
        '       _vacinas = tb1.Rows(0)("vacinas").ToString
        _dtnasc = FormatDateTime(valordata(tb1.Rows(0)("dtnasc").ToString), DateFormat.ShortDate)
        '  _porcclinica = tb1.Rows(0)("porcclinica").ToString
        ' _porcprofissional = tb1.Rows(0)("porcprofissional").ToString
        '_nometipon = tb1.Rows(0)("nometipon").ToString
        '      _enviadosite = tb1.Rows(0)("enviadosite").ToString
        '  _dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        '   _userexclui = tb1.Rows(0)("userexclui").ToString
        '     _tipon_gerenciapropria = tb1.Rows(0)("tipon_gerenciapropria").ToString
        '    _tipon_hora = tb1.Rows(0)("tipon_hora").ToString
        '   _naoenviartipon = tb1.Rows(0)("naoenviartipon").ToString
        '   _arqassinatura = tb1.Rows(0)("arqassinatura").ToString
        '   _impassinaturaaso = tb1.Rows(0)("impassinaturaaso").ToString
        '   _usercad = tb1.Rows(0)("usercad").ToString
        '  _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _ufcrm = tb1.Rows(0)("ufcrm").ToString
        _cbo = tb1.Rows(0)("cbo").ToString

        _listahorarios.Clear()
        tb1 = tab1.conectar("select * from tbmedicos_horarios where nrseqmedico = '" & _nrseq & "' and ativo = true ")

        For x As Integer = 0 To tb1.Rows.Count - 1
            _listahorarios.Add(New clsmedicos_horarios With {.Ativo = True, .Diasemana = tb1.Rows(x)("diasemana").ToString, .Hora = tb1.Rows(x)("hora").ToString, .Nrseq = tb1.Rows(x)("nrseq").ToString, .Nrseqmedico = _nrseq, .Dtcad = valordata(tb1.Rows(x)("dtcad").ToString), .Usercad = tb1.Rows(x)("usercad").ToString})
        Next

        tb1 = tab1.conectar("select * from tbmedicosespecialidades where nrseqmedico = '" & _nrseq & "' and ativo = true ")

        For x As Integer = 0 To tb1.Rows.Count - 1
            _listaespecialiadades.Add(New clsMedicosEspecialidades With {.Ativo = tb1.Rows(x)("ativo").ToString, .Dtcad = tb1.Rows(x)("dtcad").ToString, .Nrseq = tb1.Rows(x)("nrseq").ToString, .Nrseqempresa = tb1.Rows(x)("Nrseqempresa").ToString})
        Next
        '  _ignorarqtdconsultasplano = tb1.Rows(0)("ignorarqtdconsultasplano").ToString
        '  _limitesconsultas = tb1.Rows(0)("limitesconsultas").ToString
        ' _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        '  _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
        '_numero = tb1.Rows(0)("numero").ToString
        ' _numerocomercial = tb1.Rows(0)("numerocomercial").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarMedicos(Optional xprocurarnome As Boolean = False) As Boolean

        If xprocurarnome Then
            If _nome = "" Then
                Mensagemerro = "Selecione um profissional válido para procurar"
                Return False
            End If
            tb1 = tab1.conectar("select * from " & tablename & " where nome = '" & _nome & "'")
        Else
            tb1 = tab1.conectar("select * from " & tablename & " where cod =" & Nrseq)
        End If

        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _cod = tb1.Rows(0)("cod").ToString
        _nrseq = tb1.Rows(0)("cod").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _estado = tb1.Rows(0)("estado").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _tel1 = tb1.Rows(0)("tel1").ToString
        _tel2 = tb1.Rows(0)("tel2").ToString
        _celular = tb1.Rows(0)("celular").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        _userinc = tb1.Rows(0)("userinc").ToString '
        _cpf = tb1.Rows(0)("cpf").ToString
        _rg = tb1.Rows(0)("rg").ToString
        _crm = tb1.Rows(0)("crm").ToString
        '   _permitir = tb1.Rows(0)("permitir").ToString
        _ativo = logico(tb1.Rows(0)("ativo").ToString)
        _login = tb1.Rows(0)("login").ToString
        _email = tb1.Rows(0)("email").ToString
        _cepcomercial = tb1.Rows(0)("cepcomercial").ToString
        _cidadecomercial = tb1.Rows(0)("cidadecomercial").ToString
        _estadocomercial = tb1.Rows(0)("estadocomercial").ToString
        _bairrocomercial = tb1.Rows(0)("bairrocomercial").ToString
        _enderecocomercial = tb1.Rows(0)("enderecocomercial").ToString
        '_vacinas = tb1.Rows(0)("vacinas").ToString
        _dtnasc = FormatDateTime(tb1.Rows(0)("dtnasc").ToString, DateFormat.ShortDate)
        '_salario = tb1.Rows(0)("salario").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        '_dtinclusao = FormatDateTime(tb1.Rows(0)("dtinclusao").ToString, DateFormat.ShortDate)
        '_userinc = tb1.Rows(0)("userinc").ToString
        '_horasem = tb1.Rows(0)("horasem").ToString
        '_cpf = tb1.Rows(0)("cpf").ToString
        '_rg = tb1.Rows(0)("rg").ToString
        '_crm = tb1.Rows(0)("crm").ToString
        '_permitir = tb1.Rows(0)("permitir").ToString
        '_ativo = tb1.Rows(0)("ativo").ToString
        '_login = tb1.Rows(0)("login").ToString
        '_email = tb1.Rows(0)("email").ToString
        '_cepcomercial = tb1.Rows(0)("cepcomercial").ToString
        '_cidadecomercial = tb1.Rows(0)("cidadecomercial").ToString
        '_estadocomercial = tb1.Rows(0)("estadocomercial").ToString
        '_bairrocomercial = tb1.Rows(0)("bairrocomercial").ToString
        '_enderecocomercial = tb1.Rows(0)("enderecocomercial").ToString
        '_vacinas = tb1.Rows(0)("vacinas").ToString
        '_dtnasc = FormatDateTime(tb1.Rows(0)("dtnasc").ToString, DateFormat.ShortDate)
        '_porcclinica = tb1.Rows(0)("porcclinica").ToString
        '_porcprofissional = tb1.Rows(0)("porcprofissional").ToString
        '_nometipon = tb1.Rows(0)("nometipon").ToString
        '_enviadosite = tb1.Rows(0)("enviadosite").ToString
        _dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        _userexclui = tb1.Rows(0)("userexclui").ToString
        '_dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        '_userexclui = tb1.Rows(0)("userexclui").ToString
        '_tipon_gerenciapropria = tb1.Rows(0)("tipon_gerenciapropria").ToString
        '_tipon_hora = tb1.Rows(0)("tipon_hora").ToString
        '_naoenviartipon = tb1.Rows(0)("naoenviartipon").ToString
        '_arqassinatura = tb1.Rows(0)("arqassinatura").ToString
        ' _impassinaturaaso = tb1.Rows(0)("impassinaturaaso").ToString
        _usercad = tb1.Rows(0)("usercad").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _ufcrm = tb1.Rows(0)("ufcrm").ToString
        _cbo = tb1.Rows(0)("cbo").ToString
        ' _ignorarqtdconsultasplano = tb1.Rows(0)("ignorarqtdconsultasplano").ToString
        '_limitesconsultas = tb1.Rows(0)("limitesconsultas").ToString
        '_nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        '_nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
        ''    _numero = tb1.Rows(0)("numero").ToString
        '  _numerocomercial = tb1.Rows(0)("numerocomercial").ToString
        '_impassinaturaaso = tb1.Rows(0)("impassinaturaaso").ToString
        '_usercad = tb1.Rows(0)("usercad").ToString
        '_dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        '_ufcrm = tb1.Rows(0)("ufcrm").ToString
        '_cbo = tb1.Rows(0)("cbo").ToString
        '_ignorarqtdconsultasplano = tb1.Rows(0)("ignorarqtdconsultasplano").ToString
        '_limitesconsultas = tb1.Rows(0)("limitesconsultas").ToString
        '_nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        '_nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
        '_numero = tb1.Rows(0)("numero").ToString
        '_numerocomercial = tb1.Rows(0)("numerocomercial").ToString 
        _contador = tb1.Rows.Count
        _table = tb1

        tb1 = tab1.conectar("select * from vwmedicosespecialidades where ativo = true and nrseqmedico = " & _nrseq & " order by descricaoespecialidades")
        For x As Integer = 0 To tb1.Rows.Count - 1
            _listaespecialiadades.Add(New clsMedicosEspecialidades With {.Nrseqespecialidade = tb1.Rows(x)("nrseqespecialidade").ToString, .Nrseqmedico = _nrseq, .Descricao = tb1.Rows(x)("descricaoespecialidades").ToString, .Plano = tb1.Rows(x)("plano").ToString, .Valor = numeros(tb1.Rows(x)("valorespecialidades").ToString), .Vlrepasse = numeros(tb1.Rows(x)("vlrepasse").ToString)})
        Next
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarTodos() As Boolean

        tb1 = tab1.conectar("select * from " & tablename & " where ativo=true order by nome")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _cod = tb1.Rows(0)("cod").ToString
        _nome = tb1.Rows(0)("nome").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _estado = tb1.Rows(0)("estado").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _tel1 = tb1.Rows(0)("tel1").ToString
        _tel2 = tb1.Rows(0)("tel2").ToString
        _celular = tb1.Rows(0)("celular").ToString
        '    _salario = tb1.Rows(0)("salario").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        '     _dtinclusao = FormatDateTime(tb1.Rows(0)("dtinclusao").ToString, DateFormat.ShortDate)
        _userinc = tb1.Rows(0)("userinc").ToString '
        ' _horasem = tb1.Rows(0)("horasem").ToString
        _cpf = tb1.Rows(0)("cpf").ToString
        _rg = tb1.Rows(0)("rg").ToString
        _crm = tb1.Rows(0)("crm").ToString
        '   _permitir = tb1.Rows(0)("permitir").ToString
        _ativo =  logico(tb1.Rows(0)("ativo").ToString)
        _login = tb1.Rows(0)("login").ToString
        _email = tb1.Rows(0)("email").ToString
        _cepcomercial = tb1.Rows(0)("cepcomercial").ToString
        _cidadecomercial = tb1.Rows(0)("cidadecomercial").ToString
        _estadocomercial = tb1.Rows(0)("estadocomercial").ToString
        _bairrocomercial = tb1.Rows(0)("bairrocomercial").ToString
        _enderecocomercial = tb1.Rows(0)("enderecocomercial").ToString
        '_vacinas = tb1.Rows(0)("vacinas").ToString
        ' _dtnasc = FormatDateTime(tb1.Rows(0)("dtnasc").ToString, DateFormat.ShortDate)
        '_salario = tb1.Rows(0)("salario").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        '_dtinclusao = FormatDateTime(tb1.Rows(0)("dtinclusao").ToString, DateFormat.ShortDate)
        '_userinc = tb1.Rows(0)("userinc").ToString
        '_horasem = tb1.Rows(0)("horasem").ToString
        '_cpf = tb1.Rows(0)("cpf").ToString
        '_rg = tb1.Rows(0)("rg").ToString
        '_crm = tb1.Rows(0)("crm").ToString
        '_permitir = tb1.Rows(0)("permitir").ToString
        '_ativo = tb1.Rows(0)("ativo").ToString
        '_login = tb1.Rows(0)("login").ToString
        '_email = tb1.Rows(0)("email").ToString
        '_cepcomercial = tb1.Rows(0)("cepcomercial").ToString
        '_cidadecomercial = tb1.Rows(0)("cidadecomercial").ToString
        '_estadocomercial = tb1.Rows(0)("estadocomercial").ToString
        '_bairrocomercial = tb1.Rows(0)("bairrocomercial").ToString
        '_enderecocomercial = tb1.Rows(0)("enderecocomercial").ToString
        '_vacinas = tb1.Rows(0)("vacinas").ToString
        '_dtnasc = FormatDateTime(tb1.Rows(0)("dtnasc").ToString, DateFormat.ShortDate)
        '_porcclinica = tb1.Rows(0)("porcclinica").ToString
        '_porcprofissional = tb1.Rows(0)("porcprofissional").ToString
        '_nometipon = tb1.Rows(0)("nometipon").ToString
        '_enviadosite = tb1.Rows(0)("enviadosite").ToString
        '    _dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        _userexclui = tb1.Rows(0)("userexclui").ToString
        '_dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        '_userexclui = tb1.Rows(0)("userexclui").ToString
        '_tipon_gerenciapropria = tb1.Rows(0)("tipon_gerenciapropria").ToString
        '_tipon_hora = tb1.Rows(0)("tipon_hora").ToString
        '_naoenviartipon = tb1.Rows(0)("naoenviartipon").ToString
        '_arqassinatura = tb1.Rows(0)("arqassinatura").ToString
        ' _impassinaturaaso = tb1.Rows(0)("impassinaturaaso").ToString
        _usercad = tb1.Rows(0)("usercad").ToString
        _dtcad = FormatDateTime(valordata(tb1.Rows(0)("dtcad").ToString), DateFormat.ShortDate)
        _ufcrm = tb1.Rows(0)("ufcrm").ToString
        _cbo = tb1.Rows(0)("cbo").ToString
        ' _ignorarqtdconsultasplano = tb1.Rows(0)("ignorarqtdconsultasplano").ToString
        '_limitesconsultas = tb1.Rows(0)("limitesconsultas").ToString
        '_nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        '_nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
        ''    _numero = tb1.Rows(0)("numero").ToString
        '  _numerocomercial = tb1.Rows(0)("numerocomercial").ToString
        '_impassinaturaaso = tb1.Rows(0)("impassinaturaaso").ToString
        '_usercad = tb1.Rows(0)("usercad").ToString
        '_dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        '_ufcrm = tb1.Rows(0)("ufcrm").ToString
        '_cbo = tb1.Rows(0)("cbo").ToString
        '_ignorarqtdconsultasplano = tb1.Rows(0)("ignorarqtdconsultasplano").ToString
        '_limitesconsultas = tb1.Rows(0)("limitesconsultas").ToString
        '_nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        '_nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
        '_numero = tb1.Rows(0)("numero").ToString
        '_numerocomercial = tb1.Rows(0)("numerocomercial").ToString 
        _contador = tb1.Rows.Count
        _table = tb1
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & tablename & " Set ativo = 'True' "
            If _nome <> "" Then
                xsql &= ", nome = '" & tratatexto(_nome) & "'"
            End If
            If _endereco <> "" Then
                xsql &= ", endereco = '" & tratatexto(_endereco) & "'"
            End If
            If _bairro <> "" Then
                xsql &= ", bairro = '" & tratatexto(_bairro) & "'"
            End If
            If _estado <> "" Then
                xsql &= ", estado = '" & tratatexto(_estado) & "'"
            End If
            If _cep <> "" Then
                xsql &= ", cep = '" & tratatexto(_cep) & "'"
            End If
            If _tel1 <> "" Then
                xsql &= ", tel1 = '" & tratatexto(_tel1) & "'"
            End If
            If _tel2 <> "" Then
                xsql &= ", tel2 = '" & tratatexto(_tel2) & "'"
            End If
            If _celular <> "" Then
                xsql &= ", celular = '" & tratatexto(_celular) & "'"
            End If
            'If _salario <> "" Then
            'xsql &= ",salario = '" & tratatexto(_salario) & "'"
            'End If
            If _cidade <> "" Then
                xsql &= ", cidade = '" & tratatexto(_cidade) & "'"
            End If
            'If _dtinclusao <> "" Then
            'xsql &= ",dtinclusao = '" & valordatamysql(_dtinclusao) & "'"
            'End If
            ' If _userinc <> "" Then
            'xsql &= ",userinc = '" & tratatexto(_userinc) & "'"
            'End If
            'If _horasem <> 0 Then
            'xsql &= ",horasem = " & moeda(_horasem)
            'End If
            If _cpf <> "" Then
                xsql &= ", cpf = '" & tratatexto(_cpf) & "'"
            End If
            ' If _rg <> "" Then
            'xsql &= ",rg = '" & tratatexto(_rg) & "'"
            'End If
            If _crm <> "" Then
                xsql &= ", crm = '" & tratatexto(_crm) & "'"
            End If
            If _permitir <> 0 Then
                xsql &= ", permitir = " & moeda(_permitir) & "'"
            End If
            If _ativo <> 0 Then
                xsql &= ", ativo = " & logico(_ativo) & "'"
            End If
            If _login <> "" Then
                xsql &= ", login = '" & tratatexto(_login) & "'"
            End If
            If _email <> "" Then
                xsql &= ", email = '" & tratatexto(_email) & "'"
            End If
            If _cepcomercial <> "" Then
                xsql &= ", cepcomercial = '" & tratatexto(_cepcomercial) & "'"
            End If
            If _cidadecomercial <> "" Then
                xsql &= ", cidadecomercial = '" & tratatexto(_cidadecomercial) & "'"
            End If
            If _estadocomercial <> "" Then
                xsql &= ", estadocomercial = '" & tratatexto(_estadocomercial) & "'"
            End If
            If _bairrocomercial <> "" Then
                xsql &= ", bairrocomercial = '" & tratatexto(_bairrocomercial) & "'"
            End If
            If _enderecocomercial <> "" Then
                xsql &= ", enderecocomercial = '" & tratatexto(_enderecocomercial) & "'"
            End If
            '  If _vacinas <> 0 Then
            ' xsql &= ",vacinas = " & moeda(_vacinas)
            'End If
            If valordatamysql(_dtnasc) <> "" Then
                xsql &= ",dtnasc = '" & valordatamysql(_dtnasc) & "'"
            End If
            'If _porcclinica <> "" Then
            'xsql &= ",porcclinica = '" & tratatexto(_porcclinica) & "'"
            'End If
            '     If _porcprofissional <> "" Then
            '   xsql &= ",porcprofissional = '" & tratatexto(_porcprofissional) & "'"
            '      End If
            '      If _nometipon <> "" Then
            ' xsql &= ",nometipon = '" & tratatexto(_nometipon) & "'"
            'End If
            ' If _enviadosite <> 0 Then
            ' xsql &= ",enviadosite = " & moeda(_enviadosite)
            ' End If
            If valordatamysql(_dtexclui) <> "" Then
                xsql &= ",dtexclui = '" & valordatamysql(_dtexclui) & "'"
            End If
            If _userexclui <> "" Then
                xsql &= ",userexclui = '" & tratatexto(_userexclui) & "'"
            End If
            'If _tipon_gerenciapropria <> 0 Then
            'xsql &= ",tipon_gerenciapropria = " & moeda(_tipon_gerenciapropria)
            'End If
            ' If _tipon_hora <> "" Then
            'xsql &= ",tipon_hora = '" & tratatexto(_tipon_hora) & "'"
            'End If
            'If _naoenviartipon <> 0 Then
            'xsql &= ",naoenviartipon = " & moeda(_naoenviartipon)
            'End If
            'If _arqassinatura <> 0 Then
            ' xsql &= ",arqassinatura = " & moeda(_arqassinatura)
            'End If
            'If _impassinaturaaso <> "" Then
            'xsql &= ",impassinaturaaso = '" & tratatexto(_impassinaturaaso) & "'"
            'End If
            'If _usercad <> "" Then
            'xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            'End If
            'If _dtcad <> "" Then
            'xsql &= ",dtcad = '" & valordatamysql(_dtcad) & "'"
            'End If
            If _ufcrm <> "" Then
                xsql &= ",ufcrm = '" & tratatexto(_ufcrm) & "'"
            End If
            'If _cbo <> "" Then
            'xsql &= ",cbo = '" & tratatexto(_cbo) & "'"
            'End If
            ' If _ignorarqtdconsultasplano <> 0 Then
            'xsql &= ",ignorarqtdconsultasplano = " & moeda(_ignorarqtdconsultasplano)
            'End If
            'If _limitesconsultas <> 0 Then
            ' xsql &= ",limitesconsultas = " & moeda(_limitesconsultas)
            ' End If
            ' If _nrseqctrl <> "" Then
            'xsql &= ",nrseqctrl = '" & tratatexto(_nrseqctrl) & "'"
            'End If
            ' If _nrseqempresa <> 0 Then
            ' xsql &= ",nrseqempresa = " & moeda(_nrseqempresa)
            ' End If
            'If _numero <> "" Then
            ' xsql &= ",numero = '" & tratatexto(_numero) & "'"
            ' End If
            ' If _numerocomercial <> "" Then
            'xsql &= ",numerocomercial = '" & tratatexto(_numerocomercial) & "'"
            'End If
            xsql &= " where cod = " & _cod
            tb1 = tab1.IncluirAlterarDados(xsql)

            If _listahorarios.Count > 0 Then
                tb1 = tab1.conectar("select * from tbmedicos_horarios where nrseqmedico = " & _nrseq & " and ativo = true ")
                For x As Integer = 0 To tb1.Rows.Count - 1
                    Dim existehora As Boolean = False
                    For y As Integer = 0 To _listahorarios.Count - 1
                        If _listahorarios(y).Hora = tb1.Rows(x)("hora").ToString AndAlso _listahorarios(y).Diasemana = tb1.Rows(x)("diasemana").ToString Then
                            existehora = True
                        End If
                    Next
                    If Not existehora Then
                        tbx = tabx.IncluirAlterarDados("update tbmedicos_horarios set ativo = false, dtexclui = '" & hora() & "', userexclui = '" & _usercad & "' where nrseq = " & tb1.Rows(x)("nrseq").ToString)
                    End If

                Next


                For x As Integer = 0 To _listahorarios.Count - 1

                Dim tbproc As Data.DataRow()
                tbproc = tb1.Select(" diasemana = " & _listahorarios(x).Diasemana & " and hora = '" & _listahorarios(x).Hora & "'")
                    If tbproc.Count = 0 Then
                        tbx = tabx.IncluirAlterarDados("insert into tbmedicos_horarios (diasemana, hora, ativo, dtcad, usercad, nrseqmedico) values (" & moeda(_listahorarios(x).Diasemana) & ", '" & _listahorarios(x).Hora & "', true, '" & hoje() & "', '" & _usercad & "'," & _nrseq & ")")
                    Else
                        tbx = tabx.IncluirAlterarDados("update tbmedicos_horarios set ativo = true where nrseq = " & tbproc(0)("nrseq").ToString)
                    End If


            Next
            End If

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & valordatamysql(hoje()) & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _cod = tb1.Rows(0)("cod").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _cod = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If

            If _ativo = True Then
                tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo = false , dtexclui = '" & valordatamysql(hoje()) & "', userexclui = '" & _usercad & "' where cod = " & _cod)
                _mensagemerro = "Medico " & _nome & " Bloqueado!"
            Else
                tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo = true  where cod = " & _cod)

                _mensagemerro = "Medico " & _nome & " liberado !"



            End If
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class

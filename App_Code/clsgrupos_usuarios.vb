﻿Imports Microsoft.VisualBasic
Imports clsSmart

Public Class clsgrupos_usuarios
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _nrsequsuario As Integer
    Dim _nrseqgrupo As Integer
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _usuario As String = ""
    Dim _email As String = ""

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrsequsuario As Integer
        Get
            Return _nrsequsuario
        End Get
        Set(value As Integer)
            _nrsequsuario = value
        End Set
    End Property

    Public Property Nrseqgrupo As Integer
        Get
            Return _nrseqgrupo
        End Get
        Set(value As Integer)
            _nrseqgrupo = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return _usuario
        End Get
        Set(value As String)
            _usuario = value
            If _usuario <> "" Then
                _nrsequsuario = 0
                tb1 = tab1.conectar("select * from tbusuarios where ativo = true and usuario = '" & _usuario & "'")
                If tb1.Rows.Count > 0 Then
                    _nrsequsuario = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property
End Class

Partial Public Class clsgrupos_usuarios

    Public Function procurar() As Boolean
        If Nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tabela where nrseq = " & Nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Nrsequsuario = tb1.Rows(0)("nrsequsuario").ToString
        Nrseqgrupo = tb1.Rows(0)("nrseqgrupo").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        Usercad = tb1.Rows(0)("usercad").ToString
        Dtexclui = FormatDateTime(tb1.Rows(0)("dtexclui").ToString, DateFormat.ShortDate)
        Userexclui = tb1.Rows(0)("userexclui").ToString
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update Table Set ativo = True"
            If Nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(Nrseq)
            End If
            If Nrsequsuario <> 0 Then
                xsql &= ",nrsequsuario = " & moeda(Nrsequsuario)
            End If
            If Nrseqgrupo <> 0 Then
                xsql &= ",nrseqgrupo = " & moeda(Nrseqgrupo)
            End If
            If Ativo <> 0 Then
                xsql &= ",ativo = " & moeda(Ativo)
            End If
            If Dtcad <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(Dtcad) & "'"
            End If
            If Usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(Usercad) & "'"
            End If
            If Dtexclui <> "" Then
                xsql &= ",dtexclui = '" & formatadatamysql(Dtexclui) & "'"
            End If
            If Userexclui <> "" Then
                xsql &= ",userexclui = '" & tratatexto(Userexclui) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole("")
            tb1 = tab1.IncluirAlterarDados("insert into Table (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbtabela where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbdocas set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class



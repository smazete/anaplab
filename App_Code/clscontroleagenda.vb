﻿Imports Microsoft.VisualBasic

Public Class clscontroleagenda
    Dim _hora As String
    Dim _doca As String
    Dim _valor As Boolean
    Dim _status As String
    Dim _lista As New List(Of clscontroleagenda)
    Dim _data As Date

    Public Property Hora As String
        Get
            Return _hora
        End Get
        Set(value As String)
            _hora = value
        End Set
    End Property

    Public Property Doca As String
        Get
            Return _doca
        End Get
        Set(value As String)
            _doca = value
        End Set
    End Property

    Public Property Valor As Boolean
        Get
            Return _valor
        End Get
        Set(value As Boolean)
            _valor = value
        End Set
    End Property

    Public Property Status As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property

    Public Property Lista As List(Of clscontroleagenda)
        Get
            Return _lista
        End Get
        Set(value As List(Of clscontroleagenda))
            _lista = value
        End Set
    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Function verstatus() As Boolean

        If _lista.Count = 0 Then
            _lista.Add(New clscontroleagenda With {.Hora = _hora, .Doca = _doca, .Data = _data})
        End If
        For x As Integer = 0 To _lista.Count - 1
            If _doca = _lista(x).Doca AndAlso _hora = _lista(x).Hora AndAlso _data = _lista(x).Data Then
                _status = _lista(x).Status
                _valor = _lista(x).Valor
                Exit For
            End If
        Next
        Return True
    End Function
    Public Function adicionastatus(Optional statusvazio As Boolean = False) As Boolean
        Dim existe As Boolean = False
        For x As Integer = 0 To _lista.Count - 1
            If _doca = _lista(x).Doca AndAlso _hora = _lista(x).Hora AndAlso _data = _lista(x).Data Then
                _lista(x).Status = _status
                _lista(x).Valor = _valor
                existe = True
                Exit For
            End If
        Next
        If Not existe Then
            _lista.Add(New clscontroleagenda With {.Status = IIf(statusvazio, "", _status), .Hora = _hora, .Doca = _doca, .Valor = _valor, .Data = _data})
        End If
        Return True
    End Function
End Class

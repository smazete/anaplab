﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsServicosdiversos


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _tablename As String = "tbservicosdiversos"
    Dim _table As New Data.DataTable

    Dim _listaservicos As New List(Of clsServicosdiversos)


    Dim _nrseq As Integer
    Dim _Servico As String
    Dim _Valor As Decimal
    Dim _cobrancaunica As Boolean
    Dim _dependentes As Boolean
    Dim _original As String
    Dim _duplicarvalor As Boolean
    Dim _exigirdata As Boolean
    Dim _ativo As Boolean
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _dtexclui As DateTime
    Dim _userexclui As String
    Dim _dtvalidadeservico As Boolean
    Dim _custo As Decimal
    Dim _cobrarate As Boolean
    Dim _nrseqempresa As Integer
    Dim _nrseqctrl As String
    Dim _salvo As Boolean

    Dim _mensagemerro As String
    Dim _contador As Integer

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Servico As String
        Get
            Return _Servico
        End Get
        Set(value As String)
            _Servico = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _Valor
        End Get
        Set(value As Decimal)
            _Valor = value
        End Set
    End Property

    Public Property Cobrancaunica As Boolean
        Get
            Return _cobrancaunica
        End Get
        Set(value As Boolean)
            _cobrancaunica = value
        End Set
    End Property

    Public Property Dependentes As Boolean
        Get
            Return _dependentes
        End Get
        Set(value As Boolean)
            _dependentes = value
        End Set
    End Property

    Public Property Original As String
        Get
            Return _original
        End Get
        Set(value As String)
            _original = value
        End Set
    End Property

    Public Property Duplicarvalor As Boolean
        Get
            Return _duplicarvalor
        End Get
        Set(value As Boolean)
            _duplicarvalor = value
        End Set
    End Property

    Public Property Exigirdata As Boolean
        Get
            Return _exigirdata
        End Get
        Set(value As Boolean)
            _exigirdata = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtvalidadeservico As Boolean
        Get
            Return _dtvalidadeservico
        End Get
        Set(value As Boolean)
            _dtvalidadeservico = value
        End Set
    End Property

    Public Property Custo As Decimal
        Get
            Return _custo
        End Get
        Set(value As Decimal)
            _custo = value
        End Set
    End Property

    Public Property Cobrarate As Boolean
        Get
            Return _cobrarate
        End Get
        Set(value As Boolean)
            _cobrarate = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Listaservicos As List(Of clsServicosdiversos)
        Get
            Return _listaservicos
        End Get
        Set(value As List(Of clsServicosdiversos))
            _listaservicos = value
        End Set
    End Property
End Class
Partial Public Class clsServicosdiversos

    Public Function carregarservicos() As Boolean

        Try
            Listaservicos.Clear()
            tb1 = tab1.conectar("select * from tbservicosdiversos where ativo = true order by servico")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaservicos.Add(New clsServicosdiversos With {.Servico = tb1.Rows(x)("servico").ToString, .Nrseq = tb1.Rows(x)("nrseq").ToString})
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function consultar(Optional xprocurardescricao As Boolean = False) As Boolean

        Try
            If xprocurardescricao Then
                tb1 = tab1.conectar("select * from " & _tablename & " where ativo=true and salvo=true and servico = '" & _Servico & "'")
            Else
                tb1 = tab1.conectar("select * from " & _tablename & " where ativo=true and salvo=true and nrseq = " & _nrseq)
            End If


            If tb1.Rows.Count > 0 Then
                _nrseq = tb1.Rows(0)("nrseq").ToString
                _Servico = tb1.Rows(0)("servico").ToString
                _Valor = tb1.Rows(0)("valor").ToString
                _cobrancaunica = logico(tb1.Rows(0)("cobrancaunica").ToString)
                _dependentes = logico(tb1.Rows(0)("dependentes").ToString)
                _original = tb1.Rows(0)("original").ToString
                _duplicarvalor = logico(tb1.Rows(0)("duplicarvalor").ToString)
                _exigirdata = logico(tb1.Rows(0)("exigirdata").ToString)
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _dtvalidadeservico = logico(tb1.Rows(0)("dtvalidadeservico").ToString)
                _custo = tb1.Rows(0)("custo").ToString
                _cobrarate = logico(tb1.Rows(0)("cobrarate").ToString)
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarativo() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where salvo=true and ativo=true ")

            If tb1.Rows.Count > 0 Then

                _Servico = tb1.Rows(0)("servico").ToString
                _Valor = tb1.Rows(0)("valor").ToString
                _cobrancaunica = logico(tb1.Rows(0)("cobrancaunica").ToString)
                _dependentes = logico(tb1.Rows(0)("dependentes").ToString)
                _original = tb1.Rows(0)("original").ToString
                _duplicarvalor = logico(tb1.Rows(0)("duplicarvalor").ToString)
                _exigirdata = logico(tb1.Rows(0)("exigirdata").ToString)
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _dtvalidadeservico = logico(tb1.Rows(0)("dtvalidadeservico").ToString)
                _custo = tb1.Rows(0)("custo").ToString
                _cobrarate = logico(tb1.Rows(0)("cobrarate").ToString)
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultartodos(Optional ativo As Boolean = True, Optional servico As String = "") As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & _tablename & " Where salvo=true " & IIf(servico <> "", " and Servico like '" & servico & "%'", "") & " and ativo= " & ativo & "")

            If tb1.Rows.Count > 0 Then

                _Servico = tb1.Rows(0)("servico").ToString
                _Valor = tb1.Rows(0)("valor").ToString
                _cobrancaunica = logico(tb1.Rows(0)("cobrancaunica").ToString)
                _dependentes = logico(tb1.Rows(0)("dependentes").ToString)
                _original = tb1.Rows(0)("original").ToString
                _duplicarvalor = logico(tb1.Rows(0)("duplicarvalor").ToString)
                _exigirdata = logico(tb1.Rows(0)("exigirdata").ToString)
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _dtvalidadeservico = logico(tb1.Rows(0)("dtvalidadeservico").ToString)
                _custo = tb1.Rows(0)("custo").ToString
                _cobrarate = logico(tb1.Rows(0)("cobrarate").ToString)
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function

    Public Function consultarempresa() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where salvo=true and nrseqempresa = " & _nrseqempresa)
            If tb1.Rows.Count > 0 Then

                _Servico = tb1.Rows(0)("servico").ToString
                _Valor = tb1.Rows(0)("valor").ToString
                _cobrancaunica = logico(tb1.Rows(0)("cobrancaunica").ToString)
                _dependentes = logico(tb1.Rows(0)("dependentes").ToString)
                _original = tb1.Rows(0)("original").ToString
                _duplicarvalor = logico(tb1.Rows(0)("duplicarvalor").ToString)
                _exigirdata = logico(tb1.Rows(0)("exigirdata").ToString)
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _dtvalidadeservico = logico(tb1.Rows(0)("dtvalidadeservico").ToString)
                _custo = tb1.Rows(0)("custo").ToString
                _cobrarate = logico(tb1.Rows(0)("cobrarate").ToString)
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try
    End Function


    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, ativo, dtcad, usercad,nrseqempresa) values ('" & wcnrseqctrl & "',false,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "'," & HttpContext.Current.Session("idempresaemuso") & ")")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            Nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set salvo=true, ativo=true, servico ='" & _Servico & "',valor ='" & _Valor & "',cobrancaunica =" & _cobrancaunica & ",dependentes =" & _dependentes & ",original ='" & _original & "',duplicarvalor =" & _duplicarvalor & ",exigirdata =" & _exigirdata & ",dtvalidadeservico =" & _dtvalidadeservico & ",custo ='" & _custo & "',cobrarate =" & _cobrarate & " where nrseq= " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If Nrseq = 0 Then
                _mensagemerro = "Please, enter a valid Funcionario!"
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(_ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

            consultarativo()

            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.ToString
            Return False

        End Try

    End Function

End Class

﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clsSmart
Public Class clsProdutos
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim tablename As String = "tbprodutos"
    Dim _table As Data.DataTable
    Dim _contador As Integer
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _produto As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo As Boolean
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _descricao As String
    Dim _nrseqctrl As String
    Dim _carenciapadrao As Integer
    Dim _qtdacessospadrao As Integer
    Dim _plano As String
    Dim _nrseqempresa As Integer
    Dim _descritivo As String

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Produto As String
        Get
            Return _produto
        End Get
        Set(value As String)
            _produto = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Carenciapadrao As Integer
        Get
            Return _carenciapadrao
        End Get
        Set(value As Integer)
            _carenciapadrao = value
        End Set
    End Property

    Public Property Qtdacessospadrao As Integer
        Get
            Return _qtdacessospadrao
        End Get
        Set(value As Integer)
            _qtdacessospadrao = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Descritivo As String
        Get
            Return _descritivo
        End Get
        Set(value As String)
            _descritivo = value
        End Set
    End Property
End Class

Partial Public Class clsProdutos

    Public Function procurar(Optional nrseq As Integer = 0, Optional produto As String = "", Optional ativo As Boolean = True) As Boolean

        Try

            tb1 = tab1.conectar("select * from " & tablename & " where salvo=true and ativo=" & IIf(ativo = False, "false", "true") & " and nrseqempresa= '" & _nrseqempresa & "'" & IIf(nrseq <> 0, " and nrseq Like  '%" & nrseq & "%'", "") & IIf(produto <> "", " and produto LIKE '%" & produto & "'", ""))

            If tb1.Rows.Count > 0 Then
                _produto = tb1.Rows(0)("produto").ToString
                _dtcad = formatadatamysql(tb1.Rows(0)("dtcad").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _dtexclui = formatadatamysql(tb1.Rows(0)("dtexclui").ToString)
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _carenciapadrao = sonumeros(tb1.Rows(0)("carenciapadrao").ToString)
                _qtdacessospadrao = sonumeros(tb1.Rows(0)("qtdacessospadrao").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _descritivo = tb1.Rows(0)("descritivo").ToString
            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function consultar(Optional nrseq As Integer = 0, Optional nrseqempresa As Integer = 0) As Boolean
        Try

            tb1 = tab1.conectar("select * from " & tablename & " where salvo=true and ativo = true" & IIf(nrseq <> 0, " and nrseq='" & nrseq & "'", "") & IIf(nrseqempresa <> 0, " and nrseqempresa='" & _nrseqempresa & "'", ""))
            If tb1.Rows.Count > 0 Then
                _produto = tb1.Rows(0)("produto").ToString
                _dtcad = formatadatamysql(tb1.Rows(0)("dtcad").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _dtexclui = formatadatamysql(tb1.Rows(0)("dtexclui").ToString)
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _carenciapadrao = sonumeros(tb1.Rows(0)("carenciapadrao").ToString)
                _qtdacessospadrao = sonumeros(tb1.Rows(0)("qtdacessospadrao").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _descritivo = tb1.Rows(0)("descritivo").ToString
            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True

        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function consultartodos(Optional ativo As Boolean = True, Optional produto As String = "", Optional nrseqempresa As Integer = 0) As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & tablename & " Where salvo=true" & IIf(produto <> "", " and produto like '" & produto & "%'", "") & " and ativo= " & ativo & IIf(nrseqempresa <> 0, " and nrseqempresa ='" & nrseqempresa & "'", ""))
            If tb1.Rows.Count > 0 Then
                _produto = tb1.Rows(0)("produto").ToString
                _dtcad = formatadatamysql(tb1.Rows(0)("dtcad").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _dtexclui = formatadatamysql(tb1.Rows(0)("dtexclui").ToString)
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _carenciapadrao = sonumeros(tb1.Rows(0)("carenciapadrao").ToString)
                _qtdacessospadrao = sonumeros(tb1.Rows(0)("qtdacessospadrao").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _descritivo = tb1.Rows(0)("descritivo").ToString
            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True

        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultarempresa() As Boolean
        Try
            tb1 = tab1.conectar("select * from " & tablename & " where salvo=true and ativo = true AND nrseqempresa=" & _nrseqempresa)
            If tb1.Rows.Count > 0 Then
                _produto = tb1.Rows(0)("produto").ToString
                _dtcad = formatadatamysql(tb1.Rows(0)("dtcad").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _dtexclui = formatadatamysql(tb1.Rows(0)("dtexclui").ToString)
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _descricao = tb1.Rows(0)("descricao").ToString
                _carenciapadrao = sonumeros(tb1.Rows(0)("carenciapadrao").ToString)
                _qtdacessospadrao = sonumeros(tb1.Rows(0)("qtdacessospadrao").ToString)
                _plano = tb1.Rows(0)("plano").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _descritivo = tb1.Rows(0)("descritivo").ToString
            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True

        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & tablename & " Set salvo=true, ativo = True"
            If _produto <> "" Then
                xsql &= ",produto = '" & tratatexto(_produto) & "'"
            End If
            If _ativo <> 0 Then
                xsql &= ",ativo = " & logico(_ativo)
            End If
            If _descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(_descricao) & "'"
            End If
            If _nrseqctrl <> "" Then
                xsql &= ",nrseqctrl = '" & tratatexto(_nrseqctrl) & "'"
            End If
            If _carenciapadrao <> 0 Then
                xsql &= ",carenciapadrao = " & sonumeros(_carenciapadrao)
            End If
            If _qtdacessospadrao <> 0 Then
                xsql &= ",qtdacessospadrao = " & sonumeros(_qtdacessospadrao)
            End If
            If _plano <> "" Then
                xsql &= ",plano = '" & tratatexto(_plano) & "'"
            End If
            If _nrseqempresa <> 0 Then
                xsql &= ",nrseqempresa = " & sonumeros(_nrseqempresa)
            End If
            If _descritivo <> "" Then
                xsql &= ",descritivo = '" & tratatexto(_descritivo) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl,nrseqempresa, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & _nrseqempresa & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  " & tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not consultar(_nrseq) Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


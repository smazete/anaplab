﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports clsSmart

Public Class clscaixapostal
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _cxpstatus As Integer
    Dim _msgp As Integer
    Dim _msga As Integer
    Dim _msgi As Integer
    Dim _nrseqassociado As Integer
    Dim _assunto As String
    Dim _texto As String
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _dtver As Date
    Dim _visto As Boolean
    Dim _ativo As Boolean

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Cxpstatus As Integer
        Get
            Return _cxpstatus
        End Get
        Set(value As Integer)
            _cxpstatus = value
        End Set
    End Property

    Public Property Msgp As Integer
        Get
            Return _msgp
        End Get
        Set(value As Integer)
            _msgp = value
        End Set
    End Property

    Public Property Msga As Integer
        Get
            Return _msga
        End Get
        Set(value As Integer)
            _msga = value
        End Set
    End Property

    Public Property Msgi As Integer
        Get
            Return _msgi
        End Get
        Set(value As Integer)
            _msgi = value
        End Set
    End Property

    Public Property Nrseqassociado As Integer
        Get
            Return _nrseqassociado
        End Get
        Set(value As Integer)
            _nrseqassociado = value
        End Set
    End Property

    Public Property Assunto As String
        Get
            Return _assunto
        End Get
        Set(value As String)
            _assunto = value
        End Set
    End Property

    Public Property Texto As String
        Get
            Return _texto
        End Get
        Set(value As String)
            _texto = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Dtver As Date
        Get
            Return _dtver
        End Get
        Set(value As Date)
            _dtver = value
        End Set
    End Property

    Public Property Visto As Boolean
        Get
            Return _visto
        End Get
        Set(value As Boolean)
            _visto = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property
End Class

Partial Public Class clscaixapostal

    Public Function procurar() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbcaixapostal where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _cxpstatus = tb1.Rows(0)("cxpstatus").ToString
        _msgp = tb1.Rows(0)("msgp").ToString
        _msga = tb1.Rows(0)("msga").ToString
        _msgi = tb1.Rows(0)("msgi").ToString
        _nrseqassociado = tb1.Rows(0)("nrseqassociado").ToString
        _assunto = tb1.Rows(0)("assunto").ToString
        _texto = tb1.Rows(0)("texto").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        _visto = tb1.Rows(0)("visto").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function



    Public Function procurarassociados() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from tbcaixapostal where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _cxpstatus = tb1.Rows(0)("cxpstatus").ToString
        _msgp = tb1.Rows(0)("msgp").ToString
        _msga = tb1.Rows(0)("msga").ToString
        _msgi = tb1.Rows(0)("msgi").ToString
        _nrseqassociado = tb1.Rows(0)("nrseqassociado").ToString
        _assunto = tb1.Rows(0)("assunto").ToString
        _texto = tb1.Rows(0)("texto").ToString
        _dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        '     _visto = tb1.Rows(0)("visto").ToString
        Return True
        Try

        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function



    Public Function salvar() As Boolean
        Try
            novo()

            Dim xsql As String
            xsql = " update tbcaixapostal Set ativo = True"

            If _cxpstatus <> 0 Then
                xsql &= ",cxpstatus = " & moeda(_cxpstatus)
            End If
            If _msgp <> 0 Then
                xsql &= ",msgp = " & moeda(_msgp)
            End If
            If _msga <> 0 Then
                xsql &= ",msga = " & moeda(_msga)
            End If
            If _msgi <> 0 Then
                xsql &= ",msgi = " & moeda(_msgi)
            End If
            If _nrseqassociado <> 0 Then
                xsql &= ",nrseqassociado = " & moeda(_nrseqassociado)
            End If
            If _assunto <> "" Then
                xsql &= ",assunto = '" & tratatexto(_assunto) & "'"
            End If
            If _texto <> "" Then
                xsql &= ",texto = '" & tratatexto(_texto) & "'"
            End If
            If formatadatamysql(_dtcad) <> "" Then
                xsql &= ",dtcad = '" & formatadatamysql(_dtcad) & "'"
            End If
            If _visto <> 0 Then
                xsql &= ",visto = " & moeda(_visto)
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbcaixapostal (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', true)")
            tb1 = tab1.conectar("Select * from  tbcaixapostal where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbcaixapostal set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

End Class



﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsCaixas

    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _contador As Integer

    Dim tablename As String = "tbcaixas"
    Dim _table As Data.DataTable

    Dim _mensagemerro As String

    Dim _detalhes As New List(Of clscaixasdth)
    Dim _tabeladth As New Data.DataTable
    Dim _nrseqempresa As Integer = 0
    Dim _nrseq As Integer = 0
    Dim _usuario As String
    Dim _data As Date
    Dim _dtfechamento As Date
    Dim _ativo As Boolean
    Dim _fechado As Boolean
    Dim _userfechado As String
    Dim _vlinicial As Decimal = 0
    Dim _vlfinal As Decimal = 0
    Dim _funcionario As String
    Dim _valorinicial As Decimal = 0
    Dim _Valorfinal As Decimal = 0
    Dim _nrsequsuario As Integer = 0
    Dim _status As String
    Dim _totalcreditos As Decimal = 0
    Dim _totaldebitos As Decimal = 0
    Dim _totalexcluidos As Decimal = 0
    Dim _qtdlancamentosativos As Integer = 0
    Dim _qtdlancamentosexcluidos As Integer = 0
    Dim _valoratual As Decimal = 0


    Public Sub New()

        _nrsequsuario = buscarsessoes("idusuario")
        If buscarsessoes("idempresaemuso") = "" Then
            _nrseq = 0
        Else
            _nrseq = buscarsessoes("idempresaemuso")
        End If

    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return _usuario
        End Get
        Set(value As String)
            _usuario = value
            If _usuario <> "" Then
                tbx = tab1.conectar("select * from tbusuarios where usuario = '" & _usuario & "' and nrseqempresa = " & _nrseqempresa)
                If tbx.Rows.Count > 0 Then
                    _nrsequsuario = tbx.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Property Dtfechamento As Date
        Get
            Return _dtfechamento
        End Get
        Set(value As Date)
            _dtfechamento = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Fechado As Boolean
        Get
            Return _fechado
        End Get
        Set(value As Boolean)
            _fechado = value
        End Set
    End Property

    Public Property Userfechado As String
        Get
            Return _userfechado
        End Get
        Set(value As String)
            _userfechado = value
        End Set
    End Property

    Public Property Vlinicial As Decimal
        Get
            Return _vlinicial
        End Get
        Set(value As Decimal)
            _vlinicial = value
        End Set
    End Property

    Public Property Vlfinal As Decimal
        Get
            Return _vlfinal
        End Get
        Set(value As Decimal)
            _vlfinal = value
        End Set
    End Property

    Public Property Funcionario As String
        Get
            Return _funcionario
        End Get
        Set(value As String)
            _funcionario = value
        End Set
    End Property

    Public Property Valorinicial As Decimal
        Get
            Return _valorinicial
        End Get
        Set(value As Decimal)
            _valorinicial = value
        End Set
    End Property

    Public Property Valorfinal As Decimal
        Get
            Return _Valorfinal
        End Get
        Set(value As Decimal)
            _Valorfinal = value
        End Set
    End Property

    Public Property Nrsequsuario As Integer
        Get
            Return _nrsequsuario
        End Get
        Set(value As Integer)
            _nrsequsuario = value
            If _nrsequsuario <> 0 Then
                tbx = tabx.conectar("select * from tbusuarios where nrseq = '" & _nrsequsuario & "'")
                If tbx.Rows.Count > 0 Then
                    _usuario = tbx.Rows(0)("usuario").ToString
                End If
            End If
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Detalhes As List(Of clscaixasdth)
        Get
            Return _detalhes
        End Get
        Set(value As List(Of clscaixasdth))
            _detalhes = value
        End Set
    End Property

    Public Property Totalcreditos As Decimal
        Get
            Return _totalcreditos
        End Get
        Set(value As Decimal)
            _totalcreditos = value
        End Set
    End Property

    Public Property Totaldebitos As Decimal
        Get
            Return _totaldebitos
        End Get
        Set(value As Decimal)
            _totaldebitos = value
        End Set
    End Property

    Public Property Totalexcluidos As Decimal
        Get
            Return _totalexcluidos
        End Get
        Set(value As Decimal)
            _totalexcluidos = value
        End Set
    End Property

    Public Property Qtdlancamentosativos As Integer
        Get
            Return _qtdlancamentosativos
        End Get
        Set(value As Integer)
            _qtdlancamentosativos = value
        End Set
    End Property

    Public Property Qtdlancamentosexcluidos As Integer
        Get
            Return _qtdlancamentosexcluidos
        End Get
        Set(value As Integer)
            _qtdlancamentosexcluidos = value
        End Set
    End Property

    Public Property Valoratual As Decimal
        Get
            Return _valoratual
        End Get
        Set(value As Decimal)
            _valoratual = value
        End Set
    End Property

    Public Property Status As String
        Get
            Return _status
        End Get
        Set(value As String)
            _status = value
        End Set
    End Property

    Public Property Tabeladth As DataTable
        Get
            Return _tabeladth
        End Get
        Set(value As DataTable)
            _tabeladth = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class


Partial Public Class clsCaixas

    Public Function fecharcaixa() As Boolean
        Try

            localizacaixa(ApenasAbertos:=True, pornrseq:=True)

            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set fechado=true,dtfechamento='" & hoje() & "' where nrseq= " & _nrseq)

            localizacaixa()

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function atualizarvalores() As Boolean
        Try

            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set data='" & formatadatamysql2(_data, True, True) & "',vlinicial='" & _vlinicial & "',vlfinal='" & _vlfinal & "' where nrseq= " & _nrseq)

            localizacaixa(ApenasAbertos:=True, pornrseq:=True)

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function novocaixa() As Boolean

        Try

            If localizacaixa(_usuario) Then
                _mensagemerro = "Já existe um caixa aberto para esse usuário !"
                Return False
            End If
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbcaixas (ativo, fechado, vlinicial, vlfinal, valorinicial, valorfinal, nrsequsuario, funcionario, usuario, data, nrseqctrl ) values (true, false, " & moeda(_vlinicial) & "," & moeda(_vlfinal) & "," & moeda(_valorinicial) & ", " & moeda(_Valorfinal) & ", " & _nrsequsuario & ", '" & Funcionario & "', '" & _usuario & "','" & hoje() & "','" & wcnrseqctrl & "')")
            tb1 = tab1.conectar("select * from tbcaixas where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            _mensagemerro = "Caixa aberto com sucesso !"
            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message.ToString
            Return False
        End Try



    End Function

    Public Function consultausuario(Optional dtabertura As DateTime = Nothing, Optional cod As Integer = 0) As Boolean

        Try

            tb1 = tab1.conectar("select * from " & tablename & " where nrsequsuario = " & _nrsequsuario & IIf(dtabertura = Nothing, "", " and data='" & formatadatamysql(dtabertura) & "'") & IIf(cod = 0, "", " and nrseq='" & cod & "'"))
            If tb1.Rows.Count > 0 Then
                _nrseq = tb1.Rows(0)("nrseq").ToString
                _usuario = tb1.Rows(0)("usuario").ToString
                _data = formatadatamysql(tb1.Rows(0)("data").ToString)
                _dtfechamento = formatadatamysql(tb1.Rows(0)("dtfechamento").ToString)
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _fechado = logico(tb1.Rows(0)("fechado").ToString)
                _userfechado = tb1.Rows(0)("userfechado").ToString
                _vlinicial = numeros(tb1.Rows(0)("vlinicial").ToString)
                _vlfinal = numeros(tb1.Rows(0)("vlfinal").ToString)
                _funcionario = tb1.Rows(0)("funcionario").ToString
                _valorinicial = numeros(tb1.Rows(0)("valorinicial").ToString)
                _Valorfinal = numeros(tb1.Rows(0)("Valorfinal").ToString)
                _nrsequsuario = numeros(tb1.Rows(0)("nrsequsuario").ToString)
                _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
            Else
                _mensagemerro = "Erro"
            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True
        Catch ex As Exception
            _mensagemerro = ex.Message
        End Try



    End Function

    Public Function localizacaixa(Optional xusuario As String = "", Optional pornrseq As Boolean = False, Optional ApenasAbertos As Boolean = True, Optional exibirexcluido As Boolean = False, Optional status As Boolean = False) As Boolean

        Try

            If xusuario <> "" Then
                _nrsequsuario = xusuario
            End If


            '   If _nrsequsuario = "" Then
            '  _mensagemerro = "Informe um usuário válido !"
            ''  Return False
            '  End If
            Dim xsql As String = "select * from tbcaixas where 1=1 "
            If pornrseq Then
                xsql &= " and nrseq = " & _nrseq
            Else
                xsql &= " and nrsequsuario = " & _nrsequsuario
            End If
            If ApenasAbertos Then
                xsql &= " and fechado = false "
            End If
            If Not exibirexcluido Then
                xsql &= " and ativo = true "
            End If
            If status = True Then
                xsql &= "and fechado = " & _fechado
            End If

            xsql &= " order by nrseq desc"
            tb1 = tab1.conectar(xsql)

            If tb1.Rows.Count = 0 Then
                _nrseq = 0
                novocaixa()

                _mensagemerro = "Nenhum caixa aberto encontrado !"
                Return False
            End If

            _table = tb1
            _contador = tb1.Rows.Count

            _nrseq = tb1.Rows(0)("nrseq").ToString
            _data = formatadatamysql(tb1.Rows(0)("data").ToString)
            _fechado = logico(tb1.Rows(0)("fechado").ToString)

            _dtfechamento = formatadatamysql(tb1.Rows(0)("dtfechamento").ToString,, True)

            _funcionario = tb1.Rows(0)("funcionario").ToString
            _usuario = tb1.Rows(0)("usuario").ToString
            _vlfinal = tratanumero(tb1.Rows(0)("vlfinal").ToString)
            _Valorfinal = tratanumero(tb1.Rows(0)("valorfinal").ToString)
            _vlinicial = tratanumero(tb1.Rows(0)("vlinicial").ToString)
            _valorinicial = tratanumero(tb1.Rows(0)("valorinicial").ToString)
            _userfechado = tratanumero(tb1.Rows(0)("userfechado").ToString)
            _ativo = logico(tb1.Rows(0)("ativo").ToString)

            _status = IIf(_fechado, "Fechado", "Aberto")


            _valoratual = 0
            _totalcreditos = 0
            _totaldebitos = 0


            tb1 = tab1.conectar("select * from vwcaixas where nrseqcaixa = " & _nrseq & " and ativo=true order by nrseq")

            For x As Integer = 0 To tb1.Rows.Count - 1

                Dim xdth As New clscaixasdth

                With tb1
                    xdth.Ativo = logico(.Rows(x)("ativo").ToString)

                    xdth.Tipolancamento = .Rows(x)("tipolancamento").ToString
                    xdth.Qtdparcelas = .Rows(x)("qtdparcelas").ToString
                    xdth.Operacao = .Rows(x)("operacao").ToString
                    xdth.Conteudo = .Rows(x)("conteudo").ToString
                    xdth.Usercad = .Rows(x)("usercad").ToString
                    xdth.Valor = tratanumero(.Rows(x)("valor").ToString)
                    xdth.Parcela = tratanumero(.Rows(x)("parcela").ToString)
                    xdth.Nrseqplanoconta = tratanumero(.Rows(x)("nrseqplanoconta").ToString)
                    xdth.Nrseqdocumento = tratanumero(.Rows(x)("nrseqdocumento").ToString)
                    xdth.Nrseq = tratanumero(.Rows(x)("nrseq").ToString)
                    xdth.Descricao = .Rows(x)("descricao").ToString
                    xdth.Descricaodocumento = .Rows(x)("Descricaodocumento").ToString
                    xdth.Descricaoplanocontas = .Rows(x)("Descricaoplanocontas").ToString
                    ' xdth.Hrcad = .Rows(x)("hrcad").ToString
                    xdth.Planocontas = .Rows(x)("planocontas").ToString

                    If xdth.Ativo Then
                        If xdth.Operacao.ToUpper = "C" Then
                            _totalcreditos += xdth.Valor
                        Else
                            _totaldebitos += xdth.Valor
                        End If
                        _qtdlancamentosativos += 1

                    Else
                        _totalexcluidos += xdth.Valor
                        _qtdlancamentosexcluidos += 1
                    End If
                End With
                _detalhes.Add(xdth)

            Next

            _valoratual = _totalcreditos - _totaldebitos
            Return True

        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try



    End Function
    Public Function entrada() As Boolean

        Try
            localizacaixa()
            If _nrseq = 0 Then
                _mensagemerro = "Selecione um caixa válido !"
                Return False

            End If

            For x As Integer = 0 To _detalhes.Count - 1

                With _detalhes(x)

                    tb1 = tab1.IncluirAlterarDados("insert into tbcaixasdth (ativo, nrseqcaixa, valor, descricao, dtcad, hrcad, tipolancamento, usercad, operacao, Qtdparcelas, parcela, nrseqdocumento, Nrseqplanoconta ) values (true, " & _nrseq & "," & moeda(_detalhes(x).Valor) & ",'" & .Descricao & "', '" & hoje() & "', '" & .Hrcad & "','" & .Tipolancamento & "', '" & _usuario & "', '" & .Operacao & "', " & moeda(.Qtdparcelas) & ", " & moeda(.Parcela) & ", " & moeda(.Nrseqdocumento) & ", " & moeda(.Nrseqplanoconta) & ")")
                End With
            Next

            _mensagemerro = "Valor gravado no caixa com sucesso !"
            Return True
        Catch exentrada As Exception
            _mensagemerro = exentrada.Message
            Return False
        End Try

    End Function
End Class
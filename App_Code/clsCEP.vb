﻿Imports Microsoft.VisualBasic
Imports clsSmart

Public Class clsCEP

    Dim _endereco As String
    Dim _bairro As String
    Dim _cidade As String
    Dim _uf As String
    Dim _pais As String
    Dim _cep As String
    Dim _mensagemerro As String

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Uf As String
        Get
            Return _uf
        End Get
        Set(value As String)
            _uf = value
        End Set
    End Property

    Public Property Pais As String
        Get
            Return _pais
        End Get
        Set(value As String)
            _pais = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = sonumeros(value)
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property
End Class
Partial Public Class clscep
    Public Function buscarcep() As Boolean

        If _cep = "" Then
            _mensagemerro = "Informe um CEP válido !"
            Return False
        End If

        Try
            Dim ws As New correios.AtendeClienteService

            'Verificar o nome do endpoint no arquivo Web.config 
            Dim dados = ws.consultaCEP(_cep)

            If dados IsNot Nothing Then

                _endereco = String.Format("{0}" & vbCr & vbLf & dados.[end], dados.complemento, dados.complemento2, dados.bairro, dados.cidade, dados.uf)

                _cidade = dados.cidade
                _bairro = dados.bairro
                _uf = dados.uf
                _pais = "Brasil"

            Else
                _mensagemerro = "CEP não encontrado."
            End If
            Return True
        Catch excep As Exception
            _mensagemerro = excep.Message
            Return False
        End Try
    End Function
End Class
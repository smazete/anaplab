﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data
Public Class clsDocumentos

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbdocumentos"

    Dim _nrseq As Integer
    Dim _descricao As String
    Dim _ativo As Boolean
    Dim _nrseqempresa As Integer
    Dim _compensa As Integer
    Dim _taxa As Integer
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _dtexclui As DateTime
    Dim _userexclui As String
    Dim _exigirdth As Integer
    Dim _contacorrente As Integer

    Dim _Mensagemerro As String
    Dim _contador As Integer


    '  Private _detalhes As New List(Of clsServicosdiversos)()

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Compensa As Integer
        Get
            Return _compensa
        End Get
        Set(value As Integer)
            _compensa = value
        End Set
    End Property

    Public Property Taxa As Integer
        Get
            Return _taxa
        End Get
        Set(value As Integer)
            _taxa = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Exigirdth As Integer
        Get
            Return _exigirdth
        End Get
        Set(value As Integer)
            _exigirdth = value
        End Set
    End Property

    Public Property Contacorrente As Integer
        Get
            Return _contacorrente
        End Get
        Set(value As Integer)
            _contacorrente = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _Mensagemerro
        End Get
        Set(value As String)
            _Mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class
Partial Public Class clsDocumentos

    Public Function consultar() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _compensa = tb1.Rows(0)("compensa").ToString
                _taxa = tb1.Rows(0)("taxa").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _exigirdth = tb1.Rows(0)("exigirdth").ToString
                _contacorrente = tb1.Rows(0)("contacorrente").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarativo() As Boolean

        Try

            tb1 = tab1.conectar("select * from tbdocumentos where ativo = true order by descricao")

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _compensa = tb1.Rows(0)("compensa").ToString
                _taxa = tb1.Rows(0)("taxa").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _exigirdth = tb1.Rows(0)("exigirdth").ToString
                _contacorrente = tb1.Rows(0)("contacorrente").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarnome(Optional ativo As Boolean = True) As Boolean

        Try

            tb1 = tab1.conectar("Select * from " & _tablename & " Where descricao Like '" & _descricao & "%' and ativo= " & _ativo & "")

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _compensa = tb1.Rows(0)("compensa").ToString
                _taxa = tb1.Rows(0)("taxa").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _exigirdth = tb1.Rows(0)("exigirdth").ToString
                _contacorrente = tb1.Rows(0)("contacorrente").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
            Else

                Novo()
                _ativo = True

                _nrseqempresa = buscarsessoes("idempresaemuso")
                _compensa = 0
                _taxa = 0
                _usercad = buscarsessoes("usuario")
                _exigirdth = 0
                _contacorrente = 0
                salvar()
                consultar()
            End If



            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultarempresa() As Boolean

        Try

            tb1 = tab1.conectar("SELECT * FROM " & _tablename & " WHERE nrseqempresa = '" & _nrseqempresa & "'")

            If tb1.Rows.Count > 0 Then

                _nrseq = tb1.Rows(0)("nrseq").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _compensa = tb1.Rows(0)("compensa").ToString
                _taxa = tb1.Rows(0)("taxa").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _exigirdth = tb1.Rows(0)("exigirdth").ToString
                _contacorrente = tb1.Rows(0)("contacorrente").ToString

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, ativo, dtcad, usercad) values ('" & wcnrseqctrl & "',false,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "')")

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _Mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo=1, descricao='" & _descricao & "', nrseqempresa='" & _nrseqempresa & "', compensa='" & _compensa & "', taxa='" & _taxa & "', exigirdth='" & _exigirdth & "', contacorrente='" & _contacorrente & "' where nrseq= " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _Mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _Mensagemerro = "Please, enter a valid Funcionario!"
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _Mensagemerro = exexcluir.ToString
            Return False

        End Try

    End Function

End Class
﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Public Class clsCaixas_lancamentos

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _table As Data.DataTable
    Dim tablename As String = "tbcaixas_lancamentos"
    Dim _ativo As Boolean
    Dim _contador As Integer
    Dim _mensagemerro As String
    Dim _nrseq As Integer
    Dim _descricao As String
    Dim _valor As Integer
    Dim _operacao As String
    Dim _formapagto As String
    Dim _lancamento As String
    Dim _planocontas As String
    Dim _qtdparcelas As String
    Dim _parcelainicial As String
    Dim _nrseqctrl As String
    Dim _usercad As String
    Dim _dtcad As DateTime
    Dim _nrseqcaixa As String
    Dim _dtexclui As DateTime
    Dim _userexclui As String

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Valor As Integer
        Get
            Return _valor
        End Get
        Set(value As Integer)
            _valor = value
        End Set
    End Property

    Public Property Operacao As String
        Get
            Return _operacao
        End Get
        Set(value As String)
            _operacao = value
        End Set
    End Property

    Public Property Formapagto As String
        Get
            Return _formapagto
        End Get
        Set(value As String)
            _formapagto = value
        End Set
    End Property

    Public Property Lancamento As String
        Get
            Return _lancamento
        End Get
        Set(value As String)
            _lancamento = value
        End Set
    End Property

    Public Property Planocontas As String
        Get
            Return _planocontas
        End Get
        Set(value As String)
            _planocontas = value
        End Set
    End Property

    Public Property Qtdparcelas As String
        Get
            Return _qtdparcelas
        End Get
        Set(value As String)
            _qtdparcelas = value
        End Set
    End Property

    Public Property Parcelainicial As String
        Get
            Return _parcelainicial
        End Get
        Set(value As String)
            _parcelainicial = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As DateTime
        Get
            Return _dtcad
        End Get
        Set(value As DateTime)
            _dtcad = value
        End Set
    End Property

    Public Property Nrseqcaixa As String
        Get
            Return _nrseqcaixa
        End Get
        Set(value As String)
            _nrseqcaixa = value
        End Set
    End Property
End Class

Partial Public Class clsCaixas_lancamentos

    Public Function procurar() As Boolean
        If _nrseq = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & tablename & " where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _descricao = tb1.Rows(0)("descricao").ToString
        _valor = tb1.Rows(0)("valor").ToString
        _operacao = tb1.Rows(0)("operacao").ToString
        _formapagto = tb1.Rows(0)("formapagto").ToString
        _lancamento = tb1.Rows(0)("lancamento").ToString
        _planocontas = tb1.Rows(0)("planocontas").ToString
        _qtdparcelas = tb1.Rows(0)("qtdparcelas").ToString
        _parcelainicial = tb1.Rows(0)("parcelainicial").ToString
        _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
        _usercad = tb1.Rows(0)("usercad").ToString
        _dtcad = formatadatamysql(tb1.Rows(0)("dtcad").ToString)
        _nrseqcaixa = tb1.Rows(0)("nrseqcaixa").ToString
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & tablename & " Set ativo = True"
            If _descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(_descricao) & "'"
            End If
            If _valor <> 0 Then
                xsql &= ",valor = " & moeda(_valor)
            End If
            If _operacao <> "" Then
                xsql &= ",operacao = '" & tratatexto(_operacao) & "'"
            End If
            If _formapagto <> "" Then
                xsql &= ",formapagto = '" & tratatexto(_formapagto) & "'"
            End If
            If _lancamento <> "" Then
                xsql &= ",lancamento = '" & tratatexto(_lancamento) & "'"
            End If
            If _planocontas <> "" Then
                xsql &= ",planocontas = '" & tratatexto(_planocontas) & "'"
            End If
            If _qtdparcelas <> "" Then
                xsql &= ",qtdparcelas = '" & tratatexto(_qtdparcelas) & "'"
            End If
            If _parcelainicial <> "" Then
                xsql &= ",parcelainicial = '" & tratatexto(_parcelainicial) & "'"
            End If
            If _nrseqcaixa <> "" Then
                xsql &= ",nrseqcaixa = '" & tratatexto(_nrseqcaixa) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & tablename & " (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hojecomhora() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  " & tablename & "  where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update " & tablename & " set ativo = " & IIf(Ativo, "false, dtexclui = '" & hojecomhora() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


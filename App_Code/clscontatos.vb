﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports clssessoes
Imports clsSmart

Public Class clscontatos

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _listaclasse As New List(Of clscontatos)
    Dim _nrseq As Integer
    Dim _nomecompleto As String
    Dim _assunto As String
    Dim _email As String
    Dim _mensagem As String
    Dim _ativo As Boolean
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _userexclui As String
    Dim _dtexclui As Date

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Listaclasse As List(Of clscontatos)
        Get
            Return _listaclasse
        End Get
        Set(value As List(Of clscontatos))
            _listaclasse = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nomecompleto As String
        Get
            Return _nomecompleto
        End Get
        Set(value As String)
            _nomecompleto = value
        End Set
    End Property

    Public Property Assunto As String
        Get
            Return _assunto
        End Get
        Set(value As String)
            _assunto = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Mensagem As String
        Get
            Return _mensagem
        End Get
        Set(value As String)
            _mensagem = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property
End Class

Partial Public Class clscontatos

    Public Function Listarcontato() As Boolean
        Try
            Listaclasse.Clear()
            tb1 = tab1.conectar("select * from tbcontato where ativo = true")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaclasse.Add(New clscontatos With {.Nrseq = numeros(tb1.Rows(x)("nrseq").ToString), .Nomecompleto = tb1.Rows(x)("nomecompleto").ToString, .Assunto = tb1.Rows(x)("assunto").ToString, .Email = tb1.Rows(x)("email").ToString, .Mensagem = tb1.Rows(x)("mensagem").ToString, .Ativo = tb1.Rows(x)("ativo").ToString, .Usercad = tb1.Rows(x)("usercad").ToString, .Dtcad = valordata(tb1.Rows(x)("dtcad").ToString), .Userexclui = tb1.Rows(x)("userexclui").ToString, .Dtexclui = valordata(tb1.Rows(x)("dtexclui").ToString)})
            Next
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function procurar() As Boolean
        Try
            If _nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbcontato where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
            _nomecompleto = tb1.Rows(0)("nomecompleto").ToString
            _assunto = tb1.Rows(0)("assunto").ToString
            _email = tb1.Rows(0)("email").ToString
            _mensagem = tb1.Rows(0)("mensagem").ToString
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _usercad = tb1.Rows(0)("usercad").ToString
            _dtcad = FormatDateTime(valordata(tb1.Rows(0)("dtcad").ToString), DateFormat.ShortDate)
            _userexclui = tb1.Rows(0)("userexclui").ToString
            _dtexclui = FormatDateTime(valordata(tb1.Rows(0)("dtexclui").ToString), DateFormat.ShortDate)
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbcontato Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _nomecompleto <> "" Then
                xsql &= ",nomecompleto = '" & tratatexto(_nomecompleto) & "'"
            End If
            If _assunto <> "" Then
                xsql &= ",assunto = '" & tratatexto(_assunto) & "'"
            End If
            If _email <> "" Then
                xsql &= ",email = '" & tratatexto(_email) & "'"
            End If
            If _mensagem <> "" Then
                xsql &= ",mensagem = '" & tratatexto(_mensagem) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function enviarcontato() As Boolean
        Try
            novo()

            Dim xsql As String
            xsql = " update tbcontato Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _nomecompleto <> "" Then
                xsql &= ",nomecompleto = '" & tratatexto(_nomecompleto) & "'"
            End If
            If _assunto <> "" Then
                xsql &= ",assunto = '" & tratatexto(_assunto) & "'"
            End If
            If _email <> "" Then
                xsql &= ",email = '" & tratatexto(_email) & "'"
            End If
            If _mensagem <> "" Then
                xsql &= ",mensagem = '" & tratatexto(_mensagem) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)

            Dim xpadrao As New clsconfig_padroes

            xpadrao.Dadosdamensagemsite = Me

            xpadrao.Nome = "Envio de e-mails site contato"

            'If xpadrao.tratatextoemail() Then
            '    xmens.Corpo = xpadrao.Textoemail
            '    xmens.Assunto = xpadrao.Assuntoemail

            'End If


            ' m
            xpadrao.Destinatarios.Add("atendimento@anaplab.com.br")


            xpadrao.enviaremail()

            _mensagemerro = "O e-mail foi enviado para os destinatários do grupo !"

            Return True


            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbcontato (nrseqctrl, dtcad,  ativo) values ('" & wcnrseqctrl & "','" & hoje() & "', false)")
            tb1 = tab1.conectar("Select * from  tbcontato where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbcontato set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class



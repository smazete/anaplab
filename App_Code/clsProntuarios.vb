﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart

Public Class clsProntuarios

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _tablename As String = "tbprontuarios"
    Dim _table As Data.DataTable

    Dim _contador As Integer = 0
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nrseqmedico As Integer = 0
    Dim _nrseqespecialidade As Integer = 0
    Dim _texto As String
    Dim _nrseqcliente As Integer = 0
    Dim _data As Date
    Dim _hora As String
    Dim _nrseqtipo As Integer = 0
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _nrseqconsulta As Integer = 0
    Dim _dependente As Boolean = False
    Dim _nrseqctrl As String

    Public Sub New()
        _usercad = buscarsessoes("usuario")
    End Sub

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqmedico As Integer
        Get
            Return _nrseqmedico
        End Get
        Set(value As Integer)
            _nrseqmedico = value
        End Set
    End Property

    Public Property Nrseqespecialidade As Integer
        Get
            Return _nrseqespecialidade
        End Get
        Set(value As Integer)
            _nrseqespecialidade = value
        End Set
    End Property

    Public Property Texto As String
        Get
            Return _texto
        End Get
        Set(value As String)
            _texto = value
        End Set
    End Property

    Public Property Nrseqcliente As Integer
        Get
            Return _nrseqcliente
        End Get
        Set(value As Integer)
            _nrseqcliente = value
        End Set
    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Property Hora As String
        Get
            Return _hora
        End Get
        Set(value As String)
            _hora = value
        End Set
    End Property

    Public Property Nrseqtipo As Integer
        Get
            Return _nrseqtipo
        End Get
        Set(value As Integer)
            _nrseqtipo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Nrseqconsulta As Integer
        Get
            Return _nrseqconsulta
        End Get
        Set(value As Integer)
            _nrseqconsulta = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Dependente As Boolean
        Get
            Return _dependente
        End Get
        Set(value As Boolean)
            _dependente = value
        End Set
    End Property
End Class

Partial Public Class clsProntuarios

    Public Function procurar() As Boolean

        tb1 = tab1.conectar("select * from " & _tablename & " where texto like '%" & _texto & "%'")
        If tb1.Rows.Count = 0 Then
            _mensagemerro = "Não possui nenhum prontuario com esse conjunto de palavras"
            Return False
        End If
        _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
        _nrseqmedico = numeros(tb1.Rows(0)("nrseqmedico").ToString)
        _nrseqespecialidade = numeros(tb1.Rows(0)("nrseqespecialidade").ToString)
        _texto = tratatexto(tb1.Rows(0)("texto").ToString)
        _nrseqcliente = numeros(tb1.Rows(0)("nrseqcliente").ToString)
        _data = valordata(tb1.Rows(0)("data").ToString)
        _hora = numeros(tb1.Rows(0)("hora").ToString)
        _nrseqtipo = numeros(tb1.Rows(0)("nrseqtipo").ToString)
        _usercad = tb1.Rows(0)("usercad").ToString
        _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
        _nrseqconsulta = numeros(tb1.Rows(0)("nrseqconsulta").ToString)
        _dependente = tb1.Rows(0)("dependente").ToString
        _contador = tb1.Rows.Count
        _table = tb1
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function consultar() As Boolean
        If _nrseq = 0 Then
            _mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & _tablename & " where nrseq = " & _nrseq)
        If tb1.Rows.Count = 0 Then
            _mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        _nrseq = tb1.Rows(0)("nrseq").ToString
        _nrseqmedico = tb1.Rows(0)("nrseqmedico").ToString
        _nrseqespecialidade = tb1.Rows(0)("nrseqespecialidade").ToString
        _texto = tb1.Rows(0)("texto").ToString
        _nrseqcliente = tb1.Rows(0)("nrseqcliente").ToString
        _data = valordata(tb1.Rows(0)("data").ToString)
        _hora = tb1.Rows(0)("hora").ToString
        _nrseqtipo = tb1.Rows(0)("nrseqtipo").ToString
        _usercad = tb1.Rows(0)("usercad").ToString
        _dtcad = valordata(tb1.Rows(0)("dtcad").ToString)
        _nrseqconsulta = tb1.Rows(0)("nrseqconsulta").ToString
        _dependente = tb1.Rows(0)("dependente").ToString
        _contador = tb1.Rows.Count
        _table = tb1
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update " & _tablename & " Set texto='" & tratatexto(_texto) & "'"
            If _nrseqmedico <> 0 Then
                xsql &= ",nrseqmedico = " & numeros(_nrseqmedico)
            End If
            If _nrseqespecialidade <> 0 Then
                xsql &= ",nrseqespecialidade = " & numeros(_nrseqespecialidade)
            End If
            'If _texto <> "" Then
            '    xsql &= ",texto = '" & tratatexto(_texto) & "'"
            'End If
            If _nrseqcliente <> 0 Then
                xsql &= ",nrseqcliente = " & numeros(_nrseqcliente)
            End If
            If formatadatamysql(_data) <> "" Then
                xsql &= ",data = '" & formatadatamysql(_data) & "'"
            End If
            If _hora <> "" Then
                xsql &= ",hora = '" & tratatexto(_hora) & "'"
            End If
            If _nrseqtipo <> 0 Then
                xsql &= ",nrseqtipo = " & numeros(_nrseqtipo)
            End If
            If _nrseqconsulta <> 0 Then
                xsql &= ",nrseqconsulta = " & numeros(_nrseqconsulta)
            End If
            If logico(_dependente) IsNot Nothing Then
                xsql &= ",dependente = " & logico(_dependente)
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into " & _tablename & " (nrseqctrl, dtcad, usercad) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "')")
            tb1 = tab1.conectar("Select * from  " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    'Public Function excluir() As Boolean
    '    Try
    '        If _nrseq = 0 Then
    '            _mensagemerro = "Por favor, informe um item para excluir !"
    '            Return False
    '        End If
    '        If Not procurar() Then
    '            Return False
    '        End If
    '        tb1 = tab1.IncluirAlterarDados("update tbdocas set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", "true") & " where nrseq = " & _nrseq)
    '        Return True
    '    Catch exsalvar As Exception
    '        _mensagemerro = exsalvar.Message
    '        Return False
    '    End Try
    'End Function
End Class

﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data
Imports System.IO

Public Class clsarquivos
    'Desenvolvedor: Claudio Smart
    'Data inicial :  02/09/2019

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _caminho As String
    Dim _linhaatual As Integer = 0
    Dim _tipo As String
    Dim _grade As New Data.DataTable
    Dim _mensagemerro As String
    Dim _nrseqarquivo As Integer = 0
    Dim _nrseq As Integer = 0
    Dim _arquivo As String
    Dim _qtdreg As Integer = 0
    Dim _dtcad As Date
    Dim _usercad As String

    Public Sub New()
        If _usercad = "" Then
            versessao()
            _usercad = buscarsessoes("usuario")
            _dtcad = data()
        End If
    End Sub
    Public Sub New(usuariocarga As String, caminho As String)
        _usercad = usuariocarga
        _caminho = caminho
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Qtdreg As Integer
        Get
            Return _qtdreg
        End Get
        Set(value As Integer)
            _qtdreg = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Grade As DataTable
        Get
            Return _grade
        End Get
        Set(value As DataTable)
            _grade = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property

    Public Property Nrseqarquivo As Integer
        Get
            Return _nrseqarquivo
        End Get
        Set(value As Integer)
            _nrseqarquivo = value
        End Set
    End Property

    Public Property Linhaatual As Integer
        Get
            Return _linhaatual
        End Get
        Set(value As Integer)
            _linhaatual = value
        End Set
    End Property
End Class
Partial Public Class clsarquivos
    Public Function carregargrade() As Boolean
        Try

            _grade = tab1.conectar("select * from tbarquivos where ativo = true order by nrseq desc")

            Return True
        Catch exgrade As Exception
            _mensagemerro = exgrade.Message
            Return False
        End Try


    End Function
    Public Function excluir() As Boolean
        Try

            tb1 = tab1.IncluirAlterarDados("update tbarquivos set ativo = false where nrseq = " & _nrseqarquivo)
            carregargrade()
            Return True
        Catch exexcluir As Exception
            Return False
        End Try

    End Function
    Public Function enviar() As Boolean
        Dim wclognome As String = _caminho & "\logimporta.txt"
        wclognome = alteranome(wclognome)
        Dim log01 As New System.IO.StreamWriter(wclognome)
        Try


            Dim xconfig As New clsconfig
            xconfig.carregar()

            Dim tba As New Data.DataTable
            Dim taba As New clsBanco
            Dim _qtdduplicados As Integer = 0
            Dim _qtdcarregados As Integer = 0
            tba = taba.conectar("select * from tbarquivos where nrseq = " & _nrseqarquivo)

            If tba.Rows.Count = 0 Then
                Return False
            End If
            _arquivo = _caminho & "\" & tba.Rows(0)("arquivo").ToString
            _linhaatual = tba.Rows(0)("linhaatual").ToString
            _qtdduplicados = tba.Rows(0)("qtdduplicados").ToString
            _qtdcarregados = tba.Rows(0)("qtdcarregados").ToString
            _tipo = tba.Rows(0)("tipo").ToString

            If Not File.Exists(_caminho & "\" & tba.Rows(0)("arquivo").ToString) Then
                _mensagemerro = "O arquivo não existe nos servidores"
                Return False
            End If

            Dim tbarq As New Data.DataTable

            _qtdreg = 0
            log01.WriteLine(data() & " - " & hora() & "Iniciou o processo: " & _tipo)
            log01.WriteLine(data() & " - " & hora() & "Lendo tabela ")
            tbarq = csvToDatatable(_arquivo, ";")
            Dim tbcolaboradores As New Data.DataTable, tbx As New clsBanco
            log01.WriteLine(data() & " - " & hora() & "Leu " & tba.Rows.Count & " registros")

            If _tipo = "Gestores" Then
                'tbcolaboradores = tbx.conectar("select * from tbcolaboradores")
                For x As Integer = _linhaatual To tbarq.Rows.Count - 1
                    log01.WriteLine(data() & " - " & hora() & "Linha: " & x & " - " & tbarq.Rows(x)("nome").ToString)

                    'Dim tbproccola As Data.DataRow()
                    Dim wcnrseqgestor As Integer = 0
                    Dim xcol As New clscolaboradores(_usercad)
                    '  tbproccola = tbcolaboradores.Select(" matricula = '" & tbarq.Rows(x)("matriculagestor").ToString & "'")

                    xcol.Procurarpor = "matricula"
                    xcol.Matricula = tbarq.Rows(x)("matricula").ToString.Trim
                    xcol.carregar()

                    If xcol.Count = 0 Then
                        _qtdcarregados += 1
                        xcol.novo()
                        xcol.Nome = tbarq.Rows(x)("nome").ToString.Trim
                        xcol.Matricula = tbarq.Rows(x)("matricula").ToString.Trim
                        xcol.Usuario = tbarq.Rows(x)("matricula").ToString.Trim
                        xcol.Permissaopadrao = xconfig.Permissaogestores
                        xcol.Cidade = tbarq.Rows(x)("cidade").ToString.Trim
                        xcol.Gestor = True
                        xcol.Gravarusuario = True
                        xcol.Salvar()
                    Else
                        _qtdduplicados += 1

                    End If
                    _qtdreg += 1
                    ' falta gravar  datas
                    tb1 = tab1.IncluirAlterarDados("update tbarquivos set qtdduplicados = " & _qtdduplicados & ", qtdcarregados = " & _qtdcarregados & ", linhaatual = " & x & " where nrseq = " & tba.Rows(0)("nrseq").ToString)
                    Threading.Thread.Sleep(1000)

                Next


                'If tbarq.Rows.Count > 0 Then
                '    tb1 = tab1.IncluirAlterarDados("insert into tbarquivos (arquivo, qtdreg, dtcad, usercad, tipo, qtdduplicados, qtdcarregados) values ('" & mARQUIVO(_arquivo) & "', " & _qtdreg & ",'" & hoje() & "', '" & _usercad & "', '" & _tipo & "'," & _qtdduplicados & "," & _qtdcarregados & ")")
                'End If

            End If

            If _tipo = "Colaboradores" Then
                For x As Integer = _linhaatual To tbarq.Rows.Count - 1
                    'Dim tbproccola As Data.DataRow()
                    Dim wcnrseqgestor As Integer = 0
                    Dim xcol As New clscolaboradores(_usercad)
                    '  tbproccola = tbcolaboradores.Select(" matricula = '" & tbarq.Rows(x)("matriculagestor").ToString & "'")

                    xcol.Procurarpor = "matricula"
                    xcol.Matricula = tbarq.Rows(x)("matriculagestor").ToString.Trim
                    xcol.carregar()
                    If xcol.Matricula <> "" Then
                        If xcol.Count = 0 AndAlso xcol.Matricula <> "" Then

                            xcol.novo()
                            xcol.Nome = tbarq.Rows(x)("nomegestor").ToString.Trim
                            xcol.Matricula = tbarq.Rows(x)("matriculagestor").ToString.Trim
                            xcol.Usuario = tbarq.Rows(x)("matriculagestor").ToString.Trim
                            xcol.Cidade = tbarq.Rows(x)("cidade").ToString.Trim
                            xcol.Gestor = True
                            xcol.Gravarusuario = True
                            xcol.Salvar()
                            wcnrseqgestor = xcol.Nrseq
                        Else
                            xcol.Gestor = True
                            xcol.Salvar()
                            wcnrseqgestor = xcol.Nrseq
                        End If
                    Else
                        wcnrseqgestor = 0
                    End If
                    ' falta gravar  datas

                    xcol.Matricula = tbarq.Rows(x)("matricula").ToString.Trim
                    xcol.carregar()
                    If xcol.Count = 0 Then

                        xcol.novo()
                        xcol.Gravarusuario = False
                        xcol.Status = "Ativo"
                        xcol.Nome = tbarq.Rows(x)("nome").ToString.Trim
                        xcol.Matricula = tbarq.Rows(x)("matricula").ToString.Trim
                        xcol.Dtadmissaoatual = tbarq.Rows(x)("dtadmissao").ToString.Trim
                        xcol.Email = tbarq.Rows(x)("email").ToString.Trim
                        xcol.Horista = IIf(tbarq.Rows(x)("horista").ToString.Trim.ToUpper = "M", False, True)
                        xcol.Cargoatual = tbarq.Rows(x)("cargo").ToString.Trim
                        xcol.Codigocargoatual = tbarq.Rows(x)("codigocargo").ToString.Trim
                        xcol.Buatual = tbarq.Rows(x)("descricaobu").ToString.Trim
                        xcol.Codigobuatual = tbarq.Rows(x)("bu").ToString.Trim
                        xcol.Cidade = tbarq.Rows(x)("cidade").ToString.Trim

                        'Dim xbu As New ClsBus(_usercad)
                        'xbu.Codigobu = tbarq.Rows(x)("bu").ToString.Trim
                        'xbu.Descricao = tbarq.Rows(x)("descricaobu").ToString.Trim
                        'xcol.Listabuatual.Add(xbu)

                        'Dim xcargos As New ClsCargos(_usercad)
                        'xcargos.Codigocargo = tbarq.Rows(x)("codigocargo").ToString.Trim
                        'xcargos.Descricao = tbarq.Rows(x)("cargo").ToString.Trim
                        'xcol.Listacadgoatual.Add(xcargos)


                        '   xcol.Listabuatual.Add(New ClsBus With {.Codigobu = tbarq.Rows(x)("bu").ToString.Trim, .Descricao = tbarq.Rows(x)("descricaobu").ToString.Trim})
                        '  xcol.Listacadgoatual.Add(New ClsCargos With {.Codigocargo = tbarq.Rows(x)("codigocargo").ToString.Trim, .Descricao = tbarq.Rows(x)("descricaobu").ToString.Trim})


                        xcol.Contrato = tbarq.Rows(x)("contrato").ToString.Trim

                        xcol.Empresaatual = tbarq.Rows(x)("empresa").ToString.Trim
                        xcol.Status = tbarq.Rows(x)("status").ToString.Trim
                        xcol.Desligado = tbarq.Rows(x)("desligado").ToString.Trim

                        xcol.Nrseqgestor = wcnrseqgestor
                        xcol.Nrsequsuario = 0
                        xcol.Horista = False 'IIf(tbarq.Rows(x)("horista").ToString.Trim.ToLower = "h", True, False)
                        xcol.Gestor = False
                        xcol.Salvar()

                        _qtdcarregados += 1
                    Else
                        xcol.Gravarusuario = False
                        xcol.Status = "Ativo"
                        xcol.Nome = tbarq.Rows(x)("nome").ToString.Trim
                        xcol.Matricula = tbarq.Rows(x)("matricula").ToString.Trim
                        xcol.Dtadmissaoatual = tbarq.Rows(x)("dtadmissao").ToString.Trim
                        xcol.Email = tbarq.Rows(x)("email").ToString.Trim
                        xcol.Horista = IIf(tbarq.Rows(x)("horista").ToString.Trim.ToUpper = "M", False, True)
                        xcol.Cargoatual = tbarq.Rows(x)("cargo").ToString.Trim
                        xcol.Codigocargoatual = tbarq.Rows(x)("codigocargo").ToString.Trim
                        xcol.Buatual = tbarq.Rows(x)("descricaobu").ToString.Trim
                        xcol.Codigobuatual = tbarq.Rows(x)("bu").ToString.Trim


                        'Dim xbu As New ClsBus(_usercad)
                        'xbu.Codigobu = tbarq.Rows(x)("bu").ToString.Trim
                        'xbu.Descricao = tbarq.Rows(x)("descricaobu").ToString.Trim
                        'xcol.Listabuatual.Add(xbu)

                        'Dim xcargos As New ClsCargos(_usercad)
                        'xcargos.Codigocargo = tbarq.Rows(x)("codigocargo").ToString.Trim
                        'xcargos.Descricao = tbarq.Rows(x)("cargo").ToString.Trim
                        'xcol.Listacadgoatual.Add(xcargos)


                        '   xcol.Listabuatual.Add(New ClsBus With {.Codigobu = tbarq.Rows(x)("bu").ToString.Trim, .Descricao = tbarq.Rows(x)("descricaobu").ToString.Trim})
                        '  xcol.Listacadgoatual.Add(New ClsCargos With {.Codigocargo = tbarq.Rows(x)("codigocargo").ToString.Trim, .Descricao = tbarq.Rows(x)("descricaobu").ToString.Trim})


                        xcol.Contrato = tbarq.Rows(x)("contrato").ToString.Trim

                        xcol.Empresaatual = tbarq.Rows(x)("empresa").ToString.Trim
                        xcol.Status = tbarq.Rows(x)("status").ToString.Trim
                        xcol.Desligado = tbarq.Rows(x)("desligado").ToString.Trim

                        xcol.Nrseqgestor = wcnrseqgestor
                        xcol.Nrsequsuario = 0
                        'xcol.Horista = False 'IIf(tbarq.Rows(x)("horista").ToString.Trim.ToLower = "h", True, False)
                        'xcol.Gestor = False
                        xcol.Salvar()
                        _qtdduplicados += 1
                    End If
                    _qtdreg += 1
                    tb1 = tab1.IncluirAlterarDados("update tbarquivos set qtdduplicados = " & _qtdduplicados & ", qtdcarregados = " & _qtdcarregados & ", linhaatual = " & x & " where nrseq = " & tba.Rows(0)("nrseq").ToString)
                    Threading.Thread.Sleep(1000)
                Next




            End If

            If _tipo = "Cursos" Then
                For x As Integer = _linhaatual To tbarq.Rows.Count - 1
                    'Dim tbproccola As Data.DataRow()
                    Dim wcnrseqgestor As Integer = 0


                    Dim xcol As New clscursos(_usercad)
                    '  tbproccola = tbcolaboradores.Select(" matricula = '" & tbarq.Rows(x)("matriculagestor").ToString & "'")

                    xcol.Procurarpor = "descricao"
                    xcol.Descricao = tbarq.Rows(x)("descricao").ToString.Trim
                    xcol.carregar()

                    If xcol.Count = 0 Then

                        xcol.Novo()
                        xcol.Descricao = tbarq.Rows(x)("descricao").ToString.Trim
                        xcol.Cargahoraria = tbarq.Rows(x)("cargahoraria").ToString.Trim
                        xcol.Area = tbarq.Rows(x)("area").ToString.Trim
                        xcol.Codigo = tbarq.Rows(x)("codigo").ToString.Trim
                        xcol.salvar()
                        _qtdcarregados += 1
                    Else
                        _qtdduplicados += 1
                    End If


                    _qtdreg += 1
                    tb1 = tab1.IncluirAlterarDados("update tbarquivos set qtdduplicados = " & _qtdduplicados & ", qtdcarregados = " & _qtdcarregados & ", linhaatual = " & x & " where nrseq = " & tba.Rows(0)("nrseq").ToString)
                    Threading.Thread.Sleep(1000)
                Next

            End If

            If _tipo = "Pads" Then


                For x As Integer = _linhaatual To tbarq.Rows.Count - 1
                    If Not IsDate(tbarq.Rows(x)("data").ToString) Then tbarq.Rows(x)("data") = data()
                    'Dim tbproccola As Data.DataRow()
                    Dim wcnrseqgestor As Integer = 0
                    Dim xpad As New clspad(_usercad)

                    xpad.Anobase = CDate(tbarq.Rows(x)("data").ToString).Year
                    xpad.Matriculacolaborador = tbarq.Rows(x)("matricula").ToString
                    xpad.Objetivos = tbarq.Rows(x)("objetivos").ToString
                    xpad.PlanoDesenvolvimento = tbarq.Rows(x)("planodesenvolvimento").ToString
                    xpad.Data = tbarq.Rows(x)("data").ToString
                    xpad.Descricao = tbarq.Rows(x)("descricao").ToString
                    If xpad.importarpad() Then
                        _qtdcarregados += 1
                    End If


                    _qtdreg += 1
                    ' falta gravar  datas
                    tb1 = tab1.IncluirAlterarDados("update tbarquivos set qtdduplicados = " & _qtdduplicados & ", qtdcarregados = " & _qtdcarregados & ", linhaatual = " & x & " where nrseq = " & tba.Rows(0)("nrseq").ToString)
                    Threading.Thread.Sleep(1000)

                Next



            End If

            carregargrade()
            _mensagemerro = "Processo concluído com sucesso !"
            log01.WriteLine(data() & " - " & hora() & ": " & _mensagemerro)
            log01.Close()
            Return True

        Catch exenviar As Exception
            _mensagemerro = exenviar.Message
            log01.WriteLine(data() & " - " & hora() & "Erro: " & _mensagemerro)
            log01.close
            Return False
        End Try

    End Function
    Public Function gravararquivo() As Boolean

        Try
            Dim tbarq As New Data.DataTable
            Dim _qtdduplicados As Integer = 0
            Dim _qtdcarregados As Integer = 0
            _qtdreg = 0

            tbarq = csvToDatatable(_arquivo, ";")
            _qtdreg = tbarq.Rows.Count - 1
            tb1 = tab1.IncluirAlterarDados("insert into tbarquivos (arquivo, qtdreg, dtcad, usercad, tipo, qtdduplicados, qtdcarregados, linhaatual, ativo) values ('" & mARQUIVO(_arquivo) & "', " & _qtdreg & ",'" & hoje() & "', '" & _usercad & "', '" & _tipo & "'," & _qtdduplicados & "," & _qtdcarregados & ", 0, true)")
            carregargrade()
        Catch ex As Exception

        End Try

    End Function
End Class
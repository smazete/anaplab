﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Public Class clsAgenda_Prontuarios
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _tablename As String = "vwagendamedica_prontuarios"

    Dim _table As Data.DataTable
    Dim _contador As Integer = 0
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _prontuario As String
    Dim _nrseqconsulta As Integer = 0
    Dim _paciente As String
    Dim _nrseqpaciente As Integer
    Dim _data As DateTime
    Dim _idade As Integer
    Dim _plano As String
    Dim _nrseqplano As Integer = 0
    Dim _medico As Date
    Dim _nrseqmedico As Integer = 0
    Dim _especialidade As String
    Dim _nrseqespecialidade As Integer = 0
    Dim _titular As Boolean = False

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseqconsulta As Integer
        Get
            Return _nrseqconsulta
        End Get
        Set(value As Integer)
            _nrseqconsulta = value
        End Set
    End Property

    Public Property Paciente As String
        Get
            Return _paciente
        End Get
        Set(value As String)
            _paciente = value
        End Set
    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Property Idade As Integer
        Get
            Return _idade
        End Get
        Set(value As Integer)
            _idade = value
        End Set
    End Property

    Public Property Plano As String
        Get
            Return _plano
        End Get
        Set(value As String)
            _plano = value
        End Set
    End Property

    Public Property Nrseqplano As Integer
        Get
            Return _nrseqplano
        End Get
        Set(value As Integer)
            _nrseqplano = value
        End Set
    End Property

    Public Property Medico As Date
        Get
            Return _medico
        End Get
        Set(value As Date)
            _medico = value
        End Set
    End Property

    Public Property Nrseqmedico As Integer
        Get
            Return _nrseqmedico
        End Get
        Set(value As Integer)
            _nrseqmedico = value
        End Set
    End Property

    Public Property Especialidade As String
        Get
            Return _especialidade
        End Get
        Set(value As String)
            _especialidade = value
        End Set
    End Property

    Public Property Nrseqespecialidade As Integer
        Get
            Return _nrseqespecialidade
        End Get
        Set(value As Integer)
            _nrseqespecialidade = value
        End Set
    End Property

    Public Property Titular As Boolean
        Get
            Return _titular
        End Get
        Set(value As Boolean)
            _titular = value
        End Set
    End Property

    Public Property Nrseqpaciente As Integer
        Get
            Return _nrseqpaciente
        End Get
        Set(value As Integer)
            _nrseqpaciente = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Prontuario As String
        Get
            Return _prontuario
        End Get
        Set(value As String)
            _prontuario = value
        End Set
    End Property
End Class

Partial Public Class clsAgenda_Prontuarios

    Public Function consultar() As Boolean
        If _nrseqconsulta = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
        End If
        tb1 = tab1.conectar("select * from " & _tablename & " where nrseqconsulta = '" & _nrseqconsulta & "'")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        If tb1.Rows.Count > 0 Then
            _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
            _prontuario = tratatexto(tb1.Rows(0)("prontuario").ToString)
            _nrseqconsulta = numeros(tb1.Rows(0)("nrseqconsulta").ToString)
            _paciente = tratatexto(tb1.Rows(0)("paciente").ToString)
            _nrseqpaciente = numeros(tb1.Rows(0)("nrseqpaciente").ToString)
            _data = valordata(tb1.Rows(0)("data").ToString)
            _idade = numeros(tb1.Rows(0)("idade").ToString)
            _plano = tratatexto(tb1.Rows(0)("plano").ToString)
            _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
            _medico = valordata(tb1.Rows(0)("medico").ToString)
            _nrseqmedico = numeros(tb1.Rows(0)("nrseqmedico").ToString)
            _especialidade = tratatexto(tb1.Rows(0)("especialidade").ToString)
            _nrseqespecialidade = numeros(tb1.Rows(0)("nrseqespecialidade").ToString)
            _titular = logico(tb1.Rows(0)("titular").ToString)

        End If

        _table = tb1
        _contador = tb1.Rows.Count
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procurar() As Boolean
        If _prontuario = "" Then
            Mensagemerro = "Selecione uma consulta valida para procurar"
        End If
        tb1 = tab1.conectar("select * from " & _tablename & " where prontuario like '%" & _prontuario & "%'")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione não existe nenhum prontuario com essa consulta"
            Return False
        End If
        If tb1.Rows.Count > 0 Then
            _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
            _prontuario = tratatexto(tb1.Rows(0)("nrseqconsulta").ToString)
            _nrseqconsulta = numeros(tb1.Rows(0)("prontuario").ToString)
            _paciente = tratatexto(tb1.Rows(0)("paciente").ToString)
            _nrseqpaciente = numeros(tb1.Rows(0)("nrseqpaciente").ToString)
            _data = valordata(tb1.Rows(0)("data").ToString)
            _idade = numeros(tb1.Rows(0)("idade").ToString)
            _plano = tratatexto(tb1.Rows(0)("plano").ToString)
            _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
            _medico = valordata(tb1.Rows(0)("medico").ToString)
            _nrseqmedico = numeros(tb1.Rows(0)("nrseqmedico").ToString)
            _especialidade = tratatexto(tb1.Rows(0)("especialidade").ToString)
            _nrseqespecialidade = numeros(tb1.Rows(0)("nrseqespecialidade").ToString)
            _titular = logico(tb1.Rows(0)("titular").ToString)

        End If

        _table = tb1
        _contador = tb1.Rows.Count
        Return True
        Try
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

End Class
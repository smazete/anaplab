﻿Imports Microsoft.VisualBasic

Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsConvenios_Obs
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Public Sub New()
        '   _usercad = buscarsessao("usuario")
    End Sub

    Public convenios As New List(Of clsConvenios)
    Public categorias As New List(Of clsCategoria)

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbconvenios_obs"

    Dim _nrseq As Integer
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _dtexclui As DateTime
    Dim _userexclui As String
    Dim _obs As String
    Dim _ativo As Boolean
    Dim _nrseqconvenio As Integer
    Dim _privado As Boolean

    Dim _Mensagemerro As String
    Dim _contador As Integer

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Obs As String
        Get
            Return _obs
        End Get
        Set(value As String)
            _obs = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Nrseqconvenio As Integer
        Get
            Return _nrseqconvenio
        End Get
        Set(value As Integer)
            _nrseqconvenio = value
        End Set
    End Property

    Public Property Privado As Boolean
        Get
            Return _privado
        End Get
        Set(value As Boolean)
            _privado = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _Mensagemerro
        End Get
        Set(value As String)
            _Mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property
End Class
Partial Public Class clsConvenios_Obs

    Public Function Consultar() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & Tablename & " where nrseq = " & Nrseq)

            If tb1.Rows.Count > 0 Then

                _userexclui = tb1.Rows(0)("userexclui").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _obs = tb1.Rows(0)("obs").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _nrseqconvenio = tb1.Rows(0)("nrseqconvenio").ToString
                _privado = logico(tb1.Rows(0)("privado").ToString)

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
            End If


            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function Consultarconvenioeprivado() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqconvenio = " & _nrseqconvenio & " " & IIf(HttpContext.Current.Session("usuariomaster") = "sim", "", " and privado = false ") & " order by nrseq desc")

            If tb1.Rows.Count > 0 Then

                _userexclui = tb1.Rows(0)("userexclui").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _obs = tb1.Rows(0)("obs").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _nrseqconvenio = tb1.Rows(0)("nrseqconvenio").ToString
                _privado = logico(tb1.Rows(0)("privado").ToString)

                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try

                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
            End If


            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function Novo() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            Dim xcategoria As New clsCategoria

            tb1 = tab1.IncluirAlterarDados("INSERT INTO " & _tablename & " (dtcad, usercad, ativo, obs, nrseqconvenio, privado) VALUES ('" & hoje() & "', '" & HttpContext.Current.Session("usuario") & "', true, '" & _obs & "', " & _nrseqconvenio & "," & IIf(_privado, 1, 0))

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            Return True

        Catch exnovo As Exception
            _Mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function excluir() As Boolean

        Try
            If Nrseq = 0 Then
                Mensagemerro = "Erro ao excluir!"
                Return False
            End If

            Consultar()

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo = " & IIf(_ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _Mensagemerro = exexcluir.ToString
            Return False

        End Try

    End Function

    '    Public Function salvar() As Boolean

    '        Try

    '            tb1 = tab1.IncluirAlterarDados("update tbconvenios set ativo=true, salvo = true, nome = '" & _nome & "', endereco = '" & _endereco & "', cidade = '" & _cidade & "', uf = '" & _uf & "', numero = '" & _numero & "', compl = '" & _compl & "', cnpj = '" & _cnpj & "', bairro = '" & _bairro & "', facebook = '" & _facebook & "', twitter = '" & _twitter & "', insta = '" & _insta & "', email = '" & _email & "', url = '" & _url & "', telefonefixo = '" & _telefonefixo & "', telefonecel = '" & _telefonecel & "', cep = '" & _cep & "', nomefantasia = '" & _nomefantasia & "', whats = '" & _whats & "' where nrseq = " & Nrseq)
    '=
    '            Consultar()
    '            Return True
    '        Catch exsalvar As Exception
    '            Mensagemerro = exsalvar.ToString
    '            Return False
    '        End Try
    '    End Function

End Class
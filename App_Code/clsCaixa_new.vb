﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports System.Data

Public Class clsCaixa_new
    Enum tplancamento
        Mensalidades
        Lancamentos
        Exames
    End Enum
    ' Variaveis de Ambiente 
    Dim nrsequsuario As String = ""
    Dim nrseqempresa As String = ""
    Dim usuario As String = ""
    Dim empresaEmUso As String = ""
    Dim nrseqcaixa As String = ""

    ' Variaveis de tabelas do banco

    Dim tabelaCaixas As String = " tbcaixas"
    Dim tabelaDetalhes As String = " tbcaixasdth"
    Dim tabelaEmpresa As String = " tbempresas"
    Dim tabelaUsuario As String = " tbusuarios"
    Private v As String

    ' Construtor
    Public Sub New(nrsequsuario As String, nrseqempresa As String)
        Me.nrseqempresa = nrseqempresa
        Me.nrsequsuario = nrsequsuario
        'carregaEmpresa()
        carregaUsuario()
    End Sub

    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function

    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function

    Private Function carregaEmpresa() As Data.DataTable
        'Dim sql = "SELECT * FROM " & tabelaEmpresa & " WHERE ativo = true AND nrseq = '" & nrseqempresa & "' "
        'Dim table As Data.DataTable = findSql(sql)
        'empresaEmUso = table.Rows(0)("nome")
        'Return table
    End Function

    Private Function carregaUsuario() As Data.DataTable
        Dim sql = "SELECT * FROM " & tabelaUsuario & " WHERE ativo = true AND nrseq = '" & nrsequsuario & "' "
        Dim table As Data.DataTable = findSql(sql)
        usuario = table.Rows(0)("usuario").ToString
        Return table
    End Function

    Private Function inserirLancamento(txtdescricao As String, ddlOperacao As String, txtvalor As String, ddlPagamento As String, numcaixa As String, nrseqdocumento As String, Optional formapagto As String = "Não informado", Optional tipodelancamento As tplancamento = tplancamento.Lancamentos, Optional conteudo As String = "") As Data.DataTable
        If numcaixa <> "" AndAlso txtdescricao <> "" AndAlso ddlOperacao <> "" AndAlso txtvalor <> "" Then
            If nrseqdocumento = "" OrElse nrseqdocumento = 0 Then
                nrseqdocumento = "Null"
            End If
            Dim datatable As Data.DataTable = New Data.DataTable
            Dim nrseqctrl As String = usuario & geraCodigo()
            Dim sql As String = "INSERT INTO " & tabelaDetalhes & " (nrseqcaixa, descricao, operacao, valor, qtdparcelas, usercad, dtcad, nrseqctrl, ativo, nrseqdocumento, formapagto, tipolancamento, conteudo) VALUES "
            sql &= " ('" & numcaixa & "', '" & txtdescricao & "', '" & ddlOperacao & "', '" & txtvalor &
                "', '1','" & usuario & "', '" & formatadatamysql(DateTime.Now) & "', '" & nrseqctrl & "', 1, " &
                IIf(ddlPagamento <> "", "'" & ddlPagamento & "'", nrseqdocumento) & ",'" & formapagto & "','" & tipodelancamento & "','" & conteudo & "')"
            persist(sql)
            Return findSql("SELECT * FROM " & tabelaDetalhes & " WHERE nrseqctrl = '" & nrseqctrl & "'")
        Else
            Return New Data.DataTable
        End If
    End Function

    Public Function geraCodigo() As String
        Dim protocolo As String = DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString
        Return protocolo
    End Function

    Public Function converteDatatableToString(datatable As Data.DataTable, Optional campo As String = "nrseq") As String
        Dim palavra As String = ""
        For x As Integer = 0 To datatable.Rows.Count - 1
            If x > 0 Then
                palavra &= ", "
            End If
            palavra &= datatable.Rows(x)(campo)
        Next
        Return palavra
    End Function

    Private Function converterStringParaLista(valor As String, Optional campo As String = ",") As List(Of String)
        Dim lista As New List(Of String)
        If valor <> "" Then

            Dim split() = valor.Replace(" ", "").Split(campo)

            For Each palavra As String In split
                lista.Add(palavra)
            Next
        End If
        Return lista
    End Function

    Public Function carregaCaixaPorUsuario() As DataTable
        Dim sql As String = "Select * from tbcaixas where usuario = '" & usuario & "' and fechado = false order by nrseq desc"
        Return findSql(sql)
    End Function

    Public Function carregaCaixa(nrseq As String) As DataTable
        Dim sql As String = "Select * from tbcaixas where nrseq = '" & nrseq & "' "
        Return findSql(sql)
    End Function

    Public Function novoCaixa(Optional valorInicial As Double = 0.00) As String
        Dim table As New Data.DataTable
        Dim sql As String = ""
        table = carregaCaixaPorUsuario()
        If table.Rows.Count <> 0 Then
            nrseqcaixa = table.Rows(0)("nrseq").ToString
            Return nrseqcaixa
        Else
            Dim wcnrseqctrl As String = usuario & geraCodigo() & nrseqempresa
            sql = "insert into tbcaixas (usuario, data, ativo, fechado, vlinicial, vlfinal, nrseqctrl) values ('" & usuario & "','" & formatadatamysql(data) & "',true,false," & moeda(valorInicial) & ",0,'" & wcnrseqctrl & "')"
            table = persist(sql)
            sql = "select * from tbcaixas where nrseqctrl = '" & wcnrseqctrl & "'"
            table = findSql(sql)
            If table.Rows.Count <> 0 Then
                nrseqcaixa = table.Rows(0)("nrseq").ToString
                Return nrseqcaixa
            End If
        End If

        Return ""
    End Function

    Public Function inserirLancamento(txtdescricao As String, ddlOperacao As String, txtvalor As String, ddlPagamento As String, Optional nrseqdocumento As String = "", Optional formapagto As String = "Não informado", Optional tipodelancamento As tplancamento = tplancamento.Lancamentos, Optional conteudolancamento As String = "") As Data.DataTable
        novoCaixa()
        Return inserirLancamento(txtdescricao, ddlOperacao, txtvalor, ddlPagamento, nrseqcaixa, nrseqdocumento, formapagto, tipodelancamento, conteudolancamento)
    End Function


    Public Function atualizarValorFinal(numcaixa As String) As Boolean
        Dim tabela As New Data.DataTable
        Dim resultado As Boolean = True
        Try
            Dim lista As List(Of String) = converterStringParaLista(numcaixa)
            For Each caixa As String In lista
                Try
                    Dim sqlSoma As String = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & caixa & " )"
                    Dim soma As Double = findSql(sqlSoma).Rows(0)("soma").ToString
                    Dim sqlInicial = "SELECT * FROM " & tabelaCaixas & " WHERE nrseq = '" & caixa & "' "
                    Dim inicial As Double = findSql(sqlInicial).Rows(0)("vlinicial").ToString
                    Dim final As Double = inicial + final
                    Dim sqlUpdate As String = "UPDATE " & tabelaCaixas & " vlfinal = '" & final & "' WHERE nrseq = '" & caixa * "'"
                    tabela = persist(sqlUpdate)
                Catch ex As Exception
                    resultado = False
                End Try
            Next
        Catch ex As Exception
            resultado = False
        End Try
        Return resultado
    End Function

    Public Function fecharCaixa(usuario As String, numcaixa As String, vlinicial As String, vlfinal As String) As Boolean
        Dim tabela As New Data.DataTable
        Dim sql As String = ""
        Dim sqlSoma As String = ""
        If usuario <> "" AndAlso usuario <> "&nbsp;" Then
            If numcaixa <> "" AndAlso numcaixa <> 0 AndAlso numcaixa <> "&nbsp;" Then

                sqlSoma = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & numcaixa & " )"
                tabela = findSql(sqlSoma)
                If vlinicial = "" OrElse vlinicial = 0 OrElse vlinicial = "&nbsp;" Then
                    vlinicial = 0
                End If
                If vlfinal = "" OrElse vlfinal = 0 OrElse vlfinal = "&nbsp;" Then
                    vlfinal = IIf(tabela.Rows(0)("soma").ToString <> "", tabela.Rows(0)("soma").ToString, 0)
                End If
                ' Pegando valor pro lançamento final
                Dim vlLancamentoFinal As Double = vlfinal - vlinicial - IIf(tabela.Rows(0)("soma").ToString <> "", tabela.Rows(0)("soma").ToString, 0)
                'Definindo Operação
                Dim operacao As String = "C"
                If vlLancamentoFinal < 0 Then
                    operacao = "D"
                ElseIf vlLancamentoFinal > 0 Then
                    ' Fazendo o lançamento Final
                    Dim nrseqctrl As String = Me.usuario & geraCodigo()
                    sql = "INSERT INTO " & tabelaDetalhes & " (nrseqcaixa, descricao, operacao, valor, qtdparcelas, usercad, dtcad, nrseqctrl, ativo) VALUES "
                    sql &= " ('" & numcaixa & "', 'Lançamento via Gerencia de Caixa', '" & operacao & "', '" & vlLancamentoFinal &
                        "', '1','" & Me.usuario & "', '" & formatadatamysql(Date.Now) & "', '" & nrseqctrl & "', 1)"
                    tabela = persist(sql)
                End If
                ' Finalmente realizando o fechamento 
                tabela = findSql(sqlSoma)
                Dim soma As Double = CType(IIf(tabela.Rows(0)("soma").ToString <> "", tabela.Rows(0)("soma").ToString, 0), Double) +
                 CType(IIf(vlinicial <> "", vlinicial, 0), Double)
                sql = "update tbcaixas set fechado = true, dtfechamento = '" & formatadatamysql(data) & "', userfechado = '" & Me.usuario & "', vlfinal = '" & moeda(soma) & "', vlinicial = '" & moeda(vlinicial) & "'  where usuario = '" & Me.usuario & "' and nrseq = " & numcaixa
                tabela = persist(sql)
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    ' GET / SET

    Public Function getNrsequsuario() As String
        Return nrsequsuario
    End Function

    Public Function getNrseqempresa() As String
        Return nrseqempresa
    End Function

    Public Function getUsuario() As String
        Return usuario
    End Function

    Public Function getEmpresaEmUso() As String
        Return empresaEmUso
    End Function

    Public Function getNrseqcaixa() As String
        Return nrseqcaixa
    End Function

    Public Sub setNrsequsuario(valor As String)
        nrsequsuario = valor
    End Sub

    Public Sub setNrseqempresa(valor As String)
        nrseqempresa = valor
    End Sub

    Public Sub setUsuario(valor As String)
        usuario = valor
    End Sub

    Public Sub setEmpresaEmUso(valor As String)
        empresaEmUso = valor
    End Sub

    Public Sub setTabelaCaixas(valor As String)
        tabelaCaixas = valor
    End Sub

    Public Sub setTabelaDetalhes(valor As String)
        tabelaDetalhes = valor
    End Sub

    Public Sub setTabelaEmpresa(valor As String)
        tabelaEmpresa = valor
    End Sub

    Public Sub setTabelaUsuario(valor As String)
        tabelaUsuario = valor
    End Sub

    Public Sub setNrseqcaixa(valor As String)
        nrseqcaixa = valor
    End Sub


End Class

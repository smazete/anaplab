﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsAssociado

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _mensagemerro As String
    Dim _listaclasse As New List(Of clsAssociado)
    Dim _valorbusca As String
    Dim _nrseq As Integer
    Dim _busca As String
    Dim _matricula As String
    Dim _nome As String
    Dim _nomemae As String
    Dim _situacaopb As String
    Dim _sexo As String
    Dim _dtnasc As Date
    Dim _cpf As String
    Dim _rg As String
    Dim _cep As String
    Dim _endereco As String
    Dim _numero As String
    Dim _complemento As String
    Dim _bairro As String
    Dim _cidade As String
    Dim _estado As String
    Dim _telddd As String
    Dim _telefone As String
    Dim _email As String
    Dim _dtposse As Date
    Dim _dtaposentadoria As Date
    Dim _senha As String
    Dim _agencia As String
    Dim _contacorrente As String
    Dim _autorizacao As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo_old As Boolean
    Dim _ativo As Boolean
    Dim _matriculactrl As String
    Dim _codigo As Integer
    Dim _userexcluir As String
    Dim _dtexcluido As Date
    Dim _dddcelular As String
    Dim _celular As String
    Dim _bloqueado_old As Boolean
    Dim _bloqueado As Boolean
    Dim _dtbloqueio As Date
    Dim _userbloqueio As String
    Dim _dtdesbloqueio As Date
    Dim _userdesbloqueio As String
    Dim _ip As String
    Dim _dtsolexc As String
    Dim _motiexc As String
    Dim _dtinad As String
    Dim _filiacao As Boolean
    Dim _vivomorto As String
    Dim _negociado As Boolean
    Dim _arquivo As String
    Dim _nrseqctrl As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Listaclasse As List(Of clsAssociado)
        Get
            Return _listaclasse
        End Get
        Set(value As List(Of clsAssociado))
            _listaclasse = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(value As String)
            _matricula = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Nomemae As String
        Get
            Return _nomemae
        End Get
        Set(value As String)
            _nomemae = value
        End Set
    End Property

    Public Property Situacaopb As String
        Get
            Return _situacaopb
        End Get
        Set(value As String)
            _situacaopb = value
        End Set
    End Property

    Public Property Sexo As String
        Get
            Return _sexo
        End Get
        Set(value As String)
            _sexo = value
        End Set
    End Property

    Public Property Dtnasc As Date
        Get
            Return _dtnasc
        End Get
        Set(value As Date)
            _dtnasc = value
        End Set
    End Property

    Public Property Cpf As String
        Get
            Return _cpf
        End Get
        Set(value As String)
            _cpf = value
        End Set
    End Property

    Public Property Rg As String
        Get
            Return _rg
        End Get
        Set(value As String)
            _rg = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Numero As String
        Get
            Return _numero
        End Get
        Set(value As String)
            _numero = value
        End Set
    End Property

    Public Property Complemento As String
        Get
            Return _complemento
        End Get
        Set(value As String)
            _complemento = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Estado As String
        Get
            Return _estado
        End Get
        Set(value As String)
            _estado = value
        End Set
    End Property

    Public Property Telddd As String
        Get
            Return _telddd
        End Get
        Set(value As String)
            _telddd = value
        End Set
    End Property

    Public Property Telefone As String
        Get
            Return _telefone
        End Get
        Set(value As String)
            _telefone = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Dtposse As Date
        Get
            Return _dtposse
        End Get
        Set(value As Date)
            _dtposse = value
        End Set
    End Property

    Public Property Dtaposentadoria As Date
        Get
            Return _dtaposentadoria
        End Get
        Set(value As Date)
            _dtaposentadoria = value
        End Set
    End Property

    Public Property Senha As String
        Get
            Return _senha
        End Get
        Set(value As String)
            _senha = value
        End Set
    End Property

    Public Property Agencia As String
        Get
            Return _agencia
        End Get
        Set(value As String)
            _agencia = value
        End Set
    End Property

    Public Property Contacorrente As String
        Get
            Return _contacorrente
        End Get
        Set(value As String)
            _contacorrente = value
        End Set
    End Property

    Public Property Autorizacao As Boolean
        Get
            Return _autorizacao
        End Get
        Set(value As Boolean)
            _autorizacao = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo_old As Boolean
        Get
            Return _ativo_old
        End Get
        Set(value As Boolean)
            _ativo_old = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Matriculactrl As String
        Get
            Return _matriculactrl
        End Get
        Set(value As String)
            _matriculactrl = value
        End Set
    End Property

    Public Property Codigo As Integer
        Get
            Return _codigo
        End Get
        Set(value As Integer)
            _codigo = value
        End Set
    End Property

    Public Property Userexcluir As String
        Get
            Return _userexcluir
        End Get
        Set(value As String)
            _userexcluir = value
        End Set
    End Property

    Public Property Dtexcluido As Date
        Get
            Return _dtexcluido
        End Get
        Set(value As Date)
            _dtexcluido = value
        End Set
    End Property

    Public Property Dddcelular As String
        Get
            Return _dddcelular
        End Get
        Set(value As String)
            _dddcelular = value
        End Set
    End Property

    Public Property Celular As String
        Get
            Return _celular
        End Get
        Set(value As String)
            _celular = value
        End Set
    End Property

    Public Property Bloqueado_old As Boolean
        Get
            Return _bloqueado_old
        End Get
        Set(value As Boolean)
            _bloqueado_old = value
        End Set
    End Property

    Public Property Bloqueado As Boolean
        Get
            Return _bloqueado
        End Get
        Set(value As Boolean)
            _bloqueado = value
        End Set
    End Property

    Public Property Dtbloqueio As Date
        Get
            Return _dtbloqueio
        End Get
        Set(value As Date)
            _dtbloqueio = value
        End Set
    End Property

    Public Property Userbloqueio As String
        Get
            Return _userbloqueio
        End Get
        Set(value As String)
            _userbloqueio = value
        End Set
    End Property

    Public Property Dtdesbloqueio As Date
        Get
            Return _dtdesbloqueio
        End Get
        Set(value As Date)
            _dtdesbloqueio = value
        End Set
    End Property

    Public Property Userdesbloqueio As String
        Get
            Return _userdesbloqueio
        End Get
        Set(value As String)
            _userdesbloqueio = value
        End Set
    End Property

    Public Property Ip As String
        Get
            Return _ip
        End Get
        Set(value As String)
            _ip = value
        End Set
    End Property

    Public Property Dtsolexc As String
        Get
            Return _dtsolexc
        End Get
        Set(value As String)
            _dtsolexc = value
        End Set
    End Property

    Public Property Motiexc As String
        Get
            Return _motiexc
        End Get
        Set(value As String)
            _motiexc = value
        End Set
    End Property

    Public Property Dtinad As String
        Get
            Return _dtinad
        End Get
        Set(value As String)
            _dtinad = value
        End Set
    End Property

    Public Property Filiacao As Boolean
        Get
            Return _filiacao
        End Get
        Set(value As Boolean)
            _filiacao = value
        End Set
    End Property

    Public Property Vivomorto As String
        Get
            Return _vivomorto
        End Get
        Set(value As String)
            _vivomorto = value
        End Set
    End Property

    Public Property Negociado As Boolean
        Get
            Return _negociado
        End Get
        Set(value As Boolean)
            _negociado = value
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Busca As String
        Get
            Return _busca
        End Get
        Set(value As String)
            _busca = value
        End Set
    End Property

    Public Property Valorbusca As String
        Get
            Return _valorbusca
        End Get
        Set(value As String)
            _valorbusca = value
        End Set
    End Property
End Class

Partial Public Class clsAssociado


    Public Function ListarAssociados() As Boolean
        Try
            Listaclasse.Clear()
            tb1 = tab1.conectar("select * from tbAssociados where ativo = true")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaclasse.Add(New clsAssociado With {.Nrseq = numeros(tb1.Rows(x)("nrseq").ToString), .Matricula = tb1.Rows(x)("matricula").ToString, .Nome = tb1.Rows(x)("nome").ToString, .Nomemae = tb1.Rows(x)("nomemae").ToString, .Situacaopb = tb1.Rows(x)("situacaopb").ToString, .Sexo = tb1.Rows(x)("sexo").ToString, .Dtnasc = valordata(tb1.Rows(x)("dtnasc").ToString), .Cpf = tb1.Rows(x)("cpf").ToString, .Rg = tb1.Rows(x)("rg").ToString, .Cep = tb1.Rows(x)("cep").ToString, .Endereco = tb1.Rows(x)("endereco").ToString, .Numero = tb1.Rows(x)("numero").ToString, .Complemento = tb1.Rows(x)("complemento").ToString, .Bairro = tb1.Rows(x)("bairro").ToString, .Cidade = tb1.Rows(x)("cidade").ToString, .Estado = tb1.Rows(x)("estado").ToString, .Telddd = tb1.Rows(x)("telddd").ToString, .Telefone = tb1.Rows(x)("telefone").ToString, .Email = tb1.Rows(x)("email").ToString, .Dtposse = valordata(tb1.Rows(x)("dtposse").ToString), .Dtaposentadoria = valordata(tb1.Rows(x)("dtaposentadoria").ToString), .Senha = tb1.Rows(x)("senha").ToString, .Agencia = tb1.Rows(x)("agencia").ToString, .Contacorrente = tb1.Rows(x)("contacorrente").ToString, .Autorizacao = tb1.Rows(x)("autorizacao").ToString, .Dtcad = valordata(tb1.Rows(x)("dtcad").ToString), .Usercad = tb1.Rows(x)("usercad").ToString, .Ativo_old = tb1.Rows(x)("ativo_old").ToString, .Ativo = tb1.Rows(x)("ativo").ToString, .Matriculactrl = tb1.Rows(x)("matriculactrl").ToString, .Codigo = numeros(tb1.Rows(x)("codigo").ToString), .Userexcluir = tb1.Rows(x)("userexcluir").ToString, .Dtexcluido = valordata(tb1.Rows(x)("dtexcluido").ToString), .Dddcelular = tb1.Rows(x)("dddcelular").ToString, .Celular = tb1.Rows(x)("celular").ToString, .Bloqueado_old = tb1.Rows(x)("bloqueado_old").ToString, .Bloqueado = tb1.Rows(x)("bloqueado").ToString, .Dtbloqueio = valordata(tb1.Rows(x)("dtbloqueio").ToString), .Userbloqueio = tb1.Rows(x)("userbloqueio").ToString, .Dtdesbloqueio = valordata(tb1.Rows(x)("dtdesbloqueio").ToString), .Userdesbloqueio = tb1.Rows(x)("userdesbloqueio").ToString, .Ip = tb1.Rows(x)("ip").ToString, .Dtsolexc = tb1.Rows(x)("dtsolexc").ToString, .Motiexc = tb1.Rows(x)("motiexc").ToString, .Dtinad = tb1.Rows(x)("dtinad").ToString, .Filiacao = tb1.Rows(x)("filiacao").ToString, .Vivomorto = tb1.Rows(x)("vivomorto").ToString, .Negociado = tb1.Rows(x)("negociado").ToString, .Arquivo = tb1.Rows(x)("arquivo").ToString, .Nrseqctrl = tb1.Rows(x)("nrseqctrl").ToString})
            Next
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procurarmpormat() As Boolean

        Try
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbAssociados where matricula = '" & Matricula & "' and ativo = '1'")
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Matricula = tb1.Rows(0)("matricula").ToString

            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try

    End Function

    Public Function verificaseexiste() As Boolean

        Try

            'If Agencia = 0 Then
            ' Mensagemerro = "Selecione um item Agencia para procurar"
            ' End If
            '
            '           If Contacorrente = 0 Then
            '          Mensagemerro = "Selecione um item ContaCorrente para procurar"
            '          End If

            '      Contacorrente = Contacorrente.TrimStart("0")


            tb1 = tab1.conectar("select * from tbAssociados where contacorrente like '%" & Contacorrente & "%'")

            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Não existe este cliente nos registros"
                Return False
            End If

            Nrseq = tb1.Rows(0)("nrseq").ToString
            Matricula = tb1.Rows(0)("matricula").ToString
            Nome = tb1.Rows(0)("nome").ToString

            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try

    End Function

    Public Function procuracpf() As Boolean
        Try
            If _cpf = "" Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If

            tb1 = tab1.conectar("select * from tbAssociados where cpf = '" & _cpf & "' ")

            If tb1.Rows.Count > 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If

            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function



    Public Function procurapor() As Boolean
        Try
            If _nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If

            If _busca = "" Then
                _busca = "nrseq"
            End If
            tb1 = tab1.conectar("select * from tbAssociados where " & _busca & " like '%" & _valorbusca & "%'")
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
            _matricula = tb1.Rows(0)("matricula").ToString
            _nome = tb1.Rows(0)("nome").ToString
            _nomemae = tb1.Rows(0)("nomemae").ToString
            _situacaopb = tb1.Rows(0)("situacaopb").ToString
            _sexo = tb1.Rows(0)("sexo").ToString
            _dtnasc = FormatDateTime(valordata(tb1.Rows(0)("dtnasc").ToString), DateFormat.ShortDate)
            _cpf = tb1.Rows(0)("cpf").ToString
            _rg = tb1.Rows(0)("rg").ToString
            _cep = tb1.Rows(0)("cep").ToString
            _endereco = tb1.Rows(0)("endereco").ToString
            _numero = tb1.Rows(0)("numero").ToString
            _complemento = tb1.Rows(0)("complemento").ToString
            _bairro = tb1.Rows(0)("bairro").ToString
            _cidade = tb1.Rows(0)("cidade").ToString
            _estado = tb1.Rows(0)("estado").ToString
            _telddd = tb1.Rows(0)("telddd").ToString
            _telefone = tb1.Rows(0)("telefone").ToString
            _email = tb1.Rows(0)("email").ToString
            _dtposse = FormatDateTime(valordata(tb1.Rows(0)("dtposse").ToString), DateFormat.ShortDate)
            _dtaposentadoria = FormatDateTime(valordata(tb1.Rows(0)("dtaposentadoria").ToString), DateFormat.ShortDate)
            _senha = tb1.Rows(0)("senha").ToString
            _agencia = tb1.Rows(0)("agencia").ToString
            _contacorrente = tb1.Rows(0)("contacorrente").ToString
            _autorizacao = logico(tb1.Rows(0)("autorizacao").ToString)
            _dtcad = FormatDateTime(valordata(tb1.Rows(0)("dtcad").ToString), DateFormat.ShortDate)
            _usercad = tb1.Rows(0)("usercad").ToString
            _ativo_old = logico(tb1.Rows(0)("ativo_old").ToString)
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _matriculactrl = tb1.Rows(0)("matriculactrl").ToString
            _codigo = numeros(tb1.Rows(0)("codigo").ToString)
            _userexcluir = tb1.Rows(0)("userexcluir").ToString
            _dtexcluido = FormatDateTime(valordata(tb1.Rows(0)("dtexcluido").ToString), DateFormat.ShortDate)
            _dddcelular = tb1.Rows(0)("dddcelular").ToString
            _celular = tb1.Rows(0)("celular").ToString
            _bloqueado_old = logico(tb1.Rows(0)("bloqueado_old").ToString)
            _bloqueado = logico(tb1.Rows(0)("bloqueado").ToString)
            _dtbloqueio = FormatDateTime(valordata(tb1.Rows(0)("dtbloqueio").ToString), DateFormat.ShortDate)
            _userbloqueio = tb1.Rows(0)("userbloqueio").ToString
            _dtdesbloqueio = FormatDateTime(valordata(tb1.Rows(0)("dtdesbloqueio").ToString), DateFormat.ShortDate)
            _userdesbloqueio = tb1.Rows(0)("userdesbloqueio").ToString
            _ip = tb1.Rows(0)("ip").ToString
            _dtsolexc = tb1.Rows(0)("dtsolexc").ToString
            _motiexc = tb1.Rows(0)("motiexc").ToString
            _dtinad = tb1.Rows(0)("dtinad").ToString
            _filiacao = logico(tb1.Rows(0)("filiacao").ToString)
            _vivomorto = tb1.Rows(0)("vivomorto").ToString
            _negociado = logico(tb1.Rows(0)("negociado").ToString)
            _arquivo = tb1.Rows(0)("arquivo").ToString
            _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function verificacpfcadastrado() As Boolean
        Try
            If _nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If

            tb1 = tab1.conectar("select replace(replace(replace(cpf,'-','') ,'.','') ,',','') as cpf from tbAssociados where cpf = '" & sonumeros(Cpf) & "'")
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return True
            Else
                Return False
            End If

        Catch excons As Exception
            Return True
        End Try
    End Function

    Public Function procurar() As Boolean
        Try
            If _nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If

            tb1 = tab1.conectar("select * from tbAssociados where nrseq = " & Nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
            _matricula = tb1.Rows(0)("matricula").ToString
            _nome = tb1.Rows(0)("nome").ToString
            _nomemae = tb1.Rows(0)("nomemae").ToString
            _situacaopb = tb1.Rows(0)("situacaopb").ToString
            _sexo = tb1.Rows(0)("sexo").ToString
            _dtnasc = FormatDateTime(valordata(tb1.Rows(0)("dtnasc").ToString), DateFormat.ShortDate)
            _cpf = tb1.Rows(0)("cpf").ToString
            _rg = tb1.Rows(0)("rg").ToString
            _cep = tb1.Rows(0)("cep").ToString
            _endereco = tb1.Rows(0)("endereco").ToString
            _numero = tb1.Rows(0)("numero").ToString
            _complemento = tb1.Rows(0)("complemento").ToString
            _bairro = tb1.Rows(0)("bairro").ToString
            _cidade = tb1.Rows(0)("cidade").ToString
            _estado = tb1.Rows(0)("estado").ToString
            _telddd = tb1.Rows(0)("telddd").ToString
            _telefone = tb1.Rows(0)("telefone").ToString
            _email = tb1.Rows(0)("email").ToString
            _dtposse = FormatDateTime(valordata(tb1.Rows(0)("dtposse").ToString), DateFormat.ShortDate)
            _dtaposentadoria = FormatDateTime(valordata(tb1.Rows(0)("dtaposentadoria").ToString), DateFormat.ShortDate)
            _senha = tb1.Rows(0)("senha").ToString
            _agencia = tb1.Rows(0)("agencia").ToString
            _contacorrente = tb1.Rows(0)("contacorrente").ToString
            _autorizacao = logico(tb1.Rows(0)("autorizacao").ToString)
            _dtcad = FormatDateTime(valordata(tb1.Rows(0)("dtcad").ToString), DateFormat.ShortDate)
            _usercad = tb1.Rows(0)("usercad").ToString
            _ativo_old = logico(tb1.Rows(0)("ativo_old").ToString)
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            _matriculactrl = tb1.Rows(0)("matriculactrl").ToString
            _codigo = numeros(tb1.Rows(0)("codigo").ToString)
            _userexcluir = tb1.Rows(0)("userexcluir").ToString
            _dtexcluido = FormatDateTime(valordata(tb1.Rows(0)("dtexcluido").ToString), DateFormat.ShortDate)
            _dddcelular = tb1.Rows(0)("dddcelular").ToString
            _celular = tb1.Rows(0)("celular").ToString
            _bloqueado_old = logico(tb1.Rows(0)("bloqueado_old").ToString)
            _bloqueado = logico(tb1.Rows(0)("bloqueado").ToString)
            _dtbloqueio = FormatDateTime(valordata(tb1.Rows(0)("dtbloqueio").ToString), DateFormat.ShortDate)
            _userbloqueio = tb1.Rows(0)("userbloqueio").ToString
            _dtdesbloqueio = FormatDateTime(valordata(tb1.Rows(0)("dtdesbloqueio").ToString), DateFormat.ShortDate)
            _userdesbloqueio = tb1.Rows(0)("userdesbloqueio").ToString
            _ip = tb1.Rows(0)("ip").ToString
            _dtsolexc = tb1.Rows(0)("dtsolexc").ToString
            _motiexc = tb1.Rows(0)("motiexc").ToString
            _dtinad = tb1.Rows(0)("dtinad").ToString
            _filiacao = logico(tb1.Rows(0)("filiacao").ToString)
            _vivomorto = tb1.Rows(0)("vivomorto").ToString
            _negociado = logico(tb1.Rows(0)("negociado").ToString)
            _arquivo = tb1.Rows(0)("arquivo").ToString
            _nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString
            Return True
        Catch excons As Exception
            _mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function procuramatricula() As Boolean

        Try
            If Matricula = "" Then
                Mensagemerro = "Campo Matricula esta vazio"
                Return False
            End If
            tb1 = tab1.conectar("select * from tbAssociados where matricula = '" & Matricula & "'")
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Matricula ja foi Cadastrada"
                Return True
            Else
                Return False
            End If
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function



    Public Function novoassociado() As Boolean

        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into tbAssociados (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', true)")
            tb1 = tab1.conectar("Select * from  tbAssociados where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString

            Dim xsql As String
            xsql = " update tbAssociados Set ativo = True"
            If Nrseq <> 0 Then
                xsql &= ",nrseq = " & Nrseq
            End If
            If Matricula <> "" Then
                xsql &= ",matricula = '" & Matricula & "'"
            End If
            If Nome <> "" Then
                xsql &= ",nome = '" & tratatexto(Nome) & "'"
            End If
            If Nomemae <> "" Then
                xsql &= ",nomemae = '" & tratatexto(Nomemae) & "'"
            End If
            If Situacaopb <> "" Then
                xsql &= ",situacaopb = '" & tratatexto(Situacaopb) & "'"
            End If
            If Sexo <> "" Then
                xsql &= ",sexo = '" & tratatexto(Sexo) & "'"
            End If
            If formatadatamysql(Dtnasc) <> "" Then
                xsql &= ",dtnasc = '" & formatadatamysql(Dtnasc) & "'"
            End If
            If Cpf <> "" Then
                xsql &= ",cpf = '" & tratatexto(Cpf) & "'"
            End If
            If Rg <> "" Then
                xsql &= ",rg = '" & tratatexto(Rg) & "'"
            End If
            If Cep <> "" Then
                xsql &= ",cep = '" & tratatexto(Cep) & "'"
            End If
            If Endereco <> "" Then
                xsql &= ",endereco = '" & tratatexto(Endereco) & "'"
            End If
            If Numero <> "" Then
                xsql &= ",numero = '" & tratatexto(Numero) & "'"
            End If
            If Complemento <> "" Then
                xsql &= ",complemento = '" & tratatexto(Complemento) & "'"
            End If
            If Bairro <> "" Then
                xsql &= ",bairro = '" & tratatexto(Bairro) & "'"
            End If
            If Cidade <> "" Then
                xsql &= ",cidade = '" & tratatexto(Cidade) & "'"
            End If
            If Estado <> "" Then
                xsql &= ",estado = '" & tratatexto(Estado) & "'"
            End If
            If Celular <> "" Then
                xsql &= ",telddd = '" & tratatexto(Celular) & "'"
            End If
            If Telefone <> "" Then
                xsql &= ",telefone = '" & tratatexto(Telefone) & "'"
            End If
            If Email <> "" Then
                xsql &= ",email = '" & tratatexto(Email) & "'"
            End If
            If formatadatamysql(Dtposse) <> "" Then
                xsql &= ",dtposse = '" & formatadatamysql(Dtposse) & "'"
            End If
            If formatadatamysql(Dtaposentadoria) <> "" Then
                xsql &= ",dtaposentadoria = '" & formatadatamysql(Dtaposentadoria) & "'"
            End If
            If Senha <> "" Then
                xsql &= ",senha = '" & tratatexto(Senha) & "'"
            End If
            If Agencia <> "" Then
                xsql &= ",agencia = '" & tratatexto(Agencia) & "'"
            End If
            If Contacorrente <> "" Then
                xsql &= ",contacorrente = '" & tratatexto(Contacorrente) & "'"
            End If
            If Dddcelular <> "" Then
                xsql &= ",dddcelular = '" & tratatexto(Dddcelular) & "'"
            End If
            If Celular <> "" Then
                xsql &= ",celular = '" & tratatexto(Celular) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function

    Public Function salvar(Optional xnovo As Boolean = False) As Boolean
        Try
            If xnovo Then
                novo()
            End If

            Dim xsql As String
            xsql = " update tbAssociados Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _matricula <> "" Then
                xsql &= ",matricula = '" & _matricula & "'"
            End If
            If _nome <> "" Then
                xsql &= ",nome = '" & tratatexto(_nome) & "'"
            End If
            If _nomemae <> "" Then
                xsql &= ",nomemae = '" & tratatexto(_nomemae) & "'"
            End If
            If _situacaopb <> "" Then
                xsql &= ",situacaopb = '" & tratatexto(_situacaopb) & "'"
            End If
            If _sexo <> "" Then
                xsql &= ",sexo = '" & tratatexto(_sexo) & "'"
            End If
            If formatadatamysql(_dtnasc) <> "" Then
                xsql &= ",dtnasc = '" & formatadatamysql(_dtnasc) & "'"
            End If
            If _cpf <> "" Then
                xsql &= ",cpf = '" & tratatexto(_cpf) & "'"
            End If
            If _rg <> "" Then
                xsql &= ",rg = '" & tratatexto(_rg) & "'"
            End If
            If _cep <> "" Then
                xsql &= ",cep = '" & tratatexto(_cep) & "'"
            End If
            If _endereco <> "" Then
                xsql &= ",endereco = '" & tratatexto(_endereco) & "'"
            End If
            If _numero <> "" Then
                xsql &= ",numero = '" & tratatexto(_numero) & "'"
            End If
            If _complemento <> "" Then
                xsql &= ",complemento = '" & tratatexto(_complemento) & "'"
            End If
            If _bairro <> "" Then
                xsql &= ",bairro = '" & tratatexto(_bairro) & "'"
            End If
            If _cidade <> "" Then
                xsql &= ",cidade = '" & tratatexto(_cidade) & "'"
            End If
            If _estado <> "" Then
                xsql &= ",estado = '" & tratatexto(_estado) & "'"
            End If
            If _telddd <> "" Then
                xsql &= ",telddd = '" & tratatexto(_telddd) & "'"
            End If
            If _telefone <> "" Then
                xsql &= ",telefone = '" & tratatexto(_telefone) & "'"
            End If
            If _email <> "" Then
                xsql &= ",email = '" & tratatexto(_email) & "'"
            End If
            If formatadatamysql(_dtposse) <> "" Then
                xsql &= ",dtposse = '" & formatadatamysql(_dtposse) & "'"
            End If
            If formatadatamysql(_dtaposentadoria) <> "" Then
                xsql &= ",dtaposentadoria = '" & formatadatamysql(_dtaposentadoria) & "'"
            End If
            If _senha <> "" Then
                xsql &= ",senha = '" & tratatexto(_senha) & "'"
            End If
            If _agencia <> "" Then
                xsql &= ",agencia = '" & tratatexto(_agencia) & "'"
            End If
            If _contacorrente <> "" Then
                xsql &= ",contacorrente = '" & tratatexto(_contacorrente) & "'"
            End If
            If _autorizacao <> True Then
                xsql &= ",autorizacao = '" & logico(_autorizacao) & "'"
            End If
            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _ativo_old <> True Then
                xsql &= ",ativo_old = '" & logico(_ativo_old) & "'"
            End If
            If _ativo <> True Then
                xsql &= ",ativo = '" & logico(_ativo) & "'"
            End If
            If _matriculactrl <> "" Then
                xsql &= ",matriculactrl = '" & tratatexto(_matriculactrl) & "'"
            End If
            If _codigo <> 0 Then
                xsql &= ",codigo = " & moeda(_codigo)
            End If
            If _userexcluir <> "" Then
                xsql &= ",userexcluir = '" & tratatexto(_userexcluir) & "'"
            End If
            If formatadatamysql(_dtexcluido) <> "" Then
                xsql &= ",dtexcluido = '" & formatadatamysql(_dtexcluido) & "'"
            End If
            If formatadatamysql(_dddcelular) <> "" Then
                xsql &= ",dddcelular = '" & tratatexto(_dddcelular) & "'"
            End If
            If _celular <> "" Then
                xsql &= ",celular = '" & tratatexto(_celular) & "'"
            End If
            If _bloqueado_old <> True Then
                xsql &= ",bloqueado_old = '" & logico(_bloqueado_old) & "'"
            End If
            If _bloqueado <> True Then
                xsql &= ",bloqueado = '" & logico(_bloqueado) & "'"
            End If
            If formatadatamysql(_dtbloqueio) <> "" Then
                xsql &= ",dtbloqueio = '" & formatadatamysql(_dtbloqueio) & "'"
            End If
            If _userbloqueio <> "" Then
                xsql &= ",userbloqueio = '" & tratatexto(_userbloqueio) & "'"
            End If
            If formatadatamysql(_dtdesbloqueio) <> "" Then
                xsql &= ",dtdesbloqueio = '" & formatadatamysql(_dtdesbloqueio) & "'"
            End If
            If _userdesbloqueio <> "" Then
                xsql &= ",userdesbloqueio = '" & tratatexto(_userdesbloqueio) & "'"
            End If
            If _ip <> "" Then
                xsql &= ",ip = '" & tratatexto(_ip) & "'"
            End If
            If formatadatamysql(_dtsolexc) <> "" Then
                xsql &= ",dtsolexc = '" & tratatexto(_dtsolexc) & "'"
            End If
            If _motiexc <> "" Then
                xsql &= ",motiexc = '" & tratatexto(_motiexc) & "'"
            End If
            If formatadatamysql(_dtinad) <> "" Then
                xsql &= ",dtinad = '" & tratatexto(_dtinad) & "'"
            End If
            If _filiacao <> True Then
                xsql &= ",filiacao = '" & logico(_filiacao) & "'"
            End If
            If _vivomorto <> "" Then
                xsql &= ",vivomorto = '" & tratatexto(_vivomorto) & "'"
            End If
            If _negociado <> True Then
                xsql &= ",negociado = '" & logico(_negociado) & "'"
            End If
            If _arquivo <> "" Then
                xsql &= ",arquivo = '" & tratatexto(_arquivo) & "'"
            End If
            If _nrseqctrl <> "" Then
                xsql &= ",nrseqctrl = '" & tratatexto(_nrseqctrl) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function


    Public Function salvardoassociado() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbAssociados Set ativo = True"
            If _nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(_nrseq)
            End If
            If _matricula <> "" Then
                xsql &= ",matricula = '" & _matricula & "'"
            End If
            If _nome <> "" Then
                xsql &= ",nome = '" & tratatexto(_nome) & "'"
            End If
            If _nomemae <> "" Then
                xsql &= ",nomemae = '" & tratatexto(_nomemae) & "'"
            End If
            If _situacaopb <> "" Then
                xsql &= ",situacaopb = '" & tratatexto(_situacaopb) & "'"
            End If
            If _sexo <> "" Then
                xsql &= ",sexo = '" & tratatexto(_sexo) & "'"
            End If
            If formatadatamysql(_dtnasc) <> "" Then
                xsql &= ",dtnasc = '" & formatadatamysql(_dtnasc) & "'"
            End If
            If _cpf <> "" Then
                xsql &= ",cpf = '" & tratatexto(_cpf) & "'"
            End If
            If _rg <> "" Then
                xsql &= ",rg = '" & tratatexto(_rg) & "'"
            End If
            If _cep <> "" Then
                xsql &= ",cep = '" & tratatexto(_cep) & "'"
            End If
            If _endereco <> "" Then
                xsql &= ",endereco = '" & tratatexto(_endereco) & "'"
            End If
            If _numero <> "" Then
                xsql &= ",numero = '" & tratatexto(_numero) & "'"
            End If
            If _complemento <> "" Then
                xsql &= ",complemento = '" & tratatexto(_complemento) & "'"
            End If
            If _bairro <> "" Then
                xsql &= ",bairro = '" & tratatexto(_bairro) & "'"
            End If
            If _cidade <> "" Then
                xsql &= ",cidade = '" & tratatexto(_cidade) & "'"
            End If
            If _estado <> "" Then
                xsql &= ",estado = '" & tratatexto(_estado) & "'"
            End If
            If _telddd <> "" Then
                xsql &= ",telddd = '" & tratatexto(_telddd) & "'"
            End If
            If _telefone <> "" Then
                xsql &= ",telefone = '" & tratatexto(_telefone) & "'"
            End If
            If _email <> "" Then
                xsql &= ",email = '" & tratatexto(_email) & "'"
            End If
            If formatadatamysql(_dtposse) <> "" Then
                xsql &= ",dtposse = '" & formatadatamysql(_dtposse) & "'"
            End If
            If formatadatamysql(_dtaposentadoria) <> "" Then
                xsql &= ",dtaposentadoria = '" & formatadatamysql(_dtaposentadoria) & "'"
            End If
            If _senha <> "" Then
                xsql &= ",senha = '" & tratatexto(_senha) & "'"
            End If
            If _agencia <> "" Then
                xsql &= ",agencia = '" & tratatexto(_agencia) & "'"
            End If
            If _contacorrente <> "" Then
                xsql &= ",contacorrente = '" & tratatexto(_contacorrente) & "'"
            End If
            If _autorizacao <> True Then
                xsql &= ",autorizacao = '" & logico(_autorizacao) & "'"
            End If

            If _usercad <> "" Then
                xsql &= ",usercad = '" & tratatexto(_usercad) & "'"
            End If
            If _ativo_old <> True Then
                xsql &= ",ativo_old = '" & logico(_ativo_old) & "'"
            End If
            If _ativo <> True Then
                xsql &= ",ativo = '" & logico(_ativo) & "'"
            End If
            If _matriculactrl <> "" Then
                xsql &= ",matriculactrl = '" & tratatexto(_matriculactrl) & "'"
            End If
            If _codigo <> 0 Then
                xsql &= ",codigo = " & moeda(_codigo)
            End If
            If _userexcluir <> "" Then
                xsql &= ",userexcluir = '" & tratatexto(_userexcluir) & "'"
            End If
            If formatadatamysql(_dtexcluido) <> "" Then
                xsql &= ",dtexcluido = '" & formatadatamysql(_dtexcluido) & "'"
            End If
            If formatadatamysql(_dddcelular) <> "" Then
                xsql &= ",dddcelular = '" & tratatexto(_dddcelular) & "'"
            End If
            If _celular <> "" Then
                xsql &= ",celular = '" & tratatexto(_celular) & "'"
            End If
            If _bloqueado_old <> True Then
                xsql &= ",bloqueado_old = '" & logico(_bloqueado_old) & "'"
            End If
            If _bloqueado <> True Then
                xsql &= ",bloqueado = '" & logico(_bloqueado) & "'"
            End If
            If formatadatamysql(_dtbloqueio) <> "" Then
                xsql &= ",dtbloqueio = '" & formatadatamysql(_dtbloqueio) & "'"
            End If
            If _userbloqueio <> "" Then
                xsql &= ",userbloqueio = '" & tratatexto(_userbloqueio) & "'"
            End If
            If formatadatamysql(_dtdesbloqueio) <> "" Then
                xsql &= ",dtdesbloqueio = '" & formatadatamysql(_dtdesbloqueio) & "'"
            End If
            If _userdesbloqueio <> "" Then
                xsql &= ",userdesbloqueio = '" & tratatexto(_userdesbloqueio) & "'"
            End If
            If _ip <> "" Then
                xsql &= ",ip = '" & tratatexto(_ip) & "'"
            End If
            If formatadatamysql(_dtsolexc) <> "" Then
                xsql &= ",dtsolexc = '" & tratatexto(_dtsolexc) & "'"
            End If
            If _motiexc <> "" Then
                xsql &= ",motiexc = '" & tratatexto(_motiexc) & "'"
            End If
            If formatadatamysql(_dtinad) <> "" Then
                xsql &= ",dtinad = '" & tratatexto(_dtinad) & "'"
            End If
            If _filiacao <> True Then
                xsql &= ",filiacao = '" & logico(_filiacao) & "'"
            End If
            If _vivomorto <> "" Then
                xsql &= ",vivomorto = '" & tratatexto(_vivomorto) & "'"
            End If
            If _negociado <> True Then
                xsql &= ",negociado = '" & logico(_negociado) & "'"
            End If
            If _arquivo <> "" Then
                xsql &= ",arquivo = '" & tratatexto(_arquivo) & "'"
            End If
            If _nrseqctrl <> "" Then
                xsql &= ",nrseqctrl = '" & tratatexto(_nrseqctrl) & "'"
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function



    Public Function carregatodos() As Boolean

        tb1 = tab1.conectar("select * from tbAssociados")
        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Matricula = tb1.Rows(0)("matricula").ToString
        Nome = tb1.Rows(0)("nome").ToString
        Nomemae = tb1.Rows(0)("nomemae").ToString
        Situacaopb = tb1.Rows(0)("situacaopb").ToString
        Sexo = tb1.Rows(0)("sexo").ToString
        Dtnasc = FormatDateTime(tb1.Rows(0)("dtnasc").ToString, DateFormat.ShortDate)
        Cpf = tb1.Rows(0)("cpf").ToString
        Rg = tb1.Rows(0)("rg").ToString
        Cep = tb1.Rows(0)("cep").ToString
        Endereco = tb1.Rows(0)("endereco").ToString
        Numero = tb1.Rows(0)("numero").ToString
        Complemento = tb1.Rows(0)("complemento").ToString
        Bairro = tb1.Rows(0)("bairro").ToString
        Cidade = tb1.Rows(0)("cidade").ToString
        Estado = tb1.Rows(0)("estado").ToString
        Telddd = tb1.Rows(0)("telddd").ToString
        Telefone = tb1.Rows(0)("telefone").ToString
        Email = tb1.Rows(0)("email").ToString
        Dtposse = FormatDateTime(tb1.Rows(0)("dtposse").ToString, DateFormat.ShortDate)
        Dtaposentadoria = FormatDateTime(tb1.Rows(0)("dtaposentadoria").ToString, DateFormat.ShortDate)
        Senha = tb1.Rows(0)("senha").ToString
        Agencia = tb1.Rows(0)("agencia").ToString
        Contacorrente = tb1.Rows(0)("contacorrente").ToString
        Autorizacao = tb1.Rows(0)("autorizacao").ToString
        Dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        Usercad = tb1.Rows(0)("usercad").ToString
        Ativo_old = tb1.Rows(0)("ativo_old").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Matriculactrl = tb1.Rows(0)("matriculactrl").ToString
        Codigo = tb1.Rows(0)("codigo").ToString
        Userexcluir = tb1.Rows(0)("userexcluir").ToString
        Dtexcluido = FormatDateTime(tb1.Rows(0)("dtexcluido").ToString, DateFormat.ShortDate)
        Dddcelular = tb1.Rows(0)("dddcelular").ToString
        Celular = tb1.Rows(0)("celular").ToString
        Bloqueado_old = tb1.Rows(0)("bloqueado_old").ToString
        Bloqueado = tb1.Rows(0)("bloqueado").ToString
        Dtbloqueio = FormatDateTime(tb1.Rows(0)("dtbloqueio").ToString, DateFormat.ShortDate)
        Userbloqueio = tb1.Rows(0)("userbloqueio").ToString
        Dtdesbloqueio = FormatDateTime(tb1.Rows(0)("dtdesbloqueio").ToString, DateFormat.ShortDate)
        Userdesbloqueio = tb1.Rows(0)("userdesbloqueio").ToString
        Ip = tb1.Rows(0)("ip").ToString
        Dtsolexc = tb1.Rows(0)("dtsolexc").ToString
        Motiexc = tb1.Rows(0)("motiexc").ToString
        Dtinad = tb1.Rows(0)("dtinad").ToString
        Filiacao = tb1.Rows(0)("filiacao").ToString
        Vivomorto = tb1.Rows(0)("vivomorto").ToString
        Negociado = tb1.Rows(0)("negociado").ToString

        '   _table = tb1
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbAssociados (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & _usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbAssociados where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function CalcularDigitoModulo11(ByVal numero As String, Optional AdmiteDigZero As Boolean = False, Optional pesoinicial As Integer = 2, Optional condicaobradesco As Boolean = False) As Integer

        Dim soma As Integer = 0
        Dim peso As Integer = pesoinicial
        Dim digito As Integer
        Dim base As Integer = 9
        If condicaobradesco Then base = 7

        For I = numero.Length - 1 To 0 Step -1

            soma = soma + numero.Substring(I, 1) * peso

            If pesoinicial <> 2 Then
                If (peso > 2) Then
                    peso = peso - 1
                Else
                    peso = pesoinicial
                End If
            Else
                If (peso < base) Then
                    peso = peso + 1
                Else
                    peso = 2
                End If
            End If


        Next I


        If condicaobradesco AndAlso (soma Mod 11) = 1 Then
            ' se o resto for 1, retorna "P", represnetado como -1 no digito
            Return -1
        End If

        digito = 11 - (soma Mod 11)

        If digito > 9 Then
            digito = 0
        End If

        If Not AdmiteDigZero AndAlso digito = 0 Then
            digito = 1
        End If

        Return digito

    End Function


    Public Function reativar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If '
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbAssociados set ativo = true where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function desfiliar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If '
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbAssociados set ativo = false ,  dtexcluido = '" & hoje() & "', userexcluir = '" & Usercad & "' where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbdocas Set ativo = " & IIf(Ativo, "False, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function informarecuperarsenha() As Boolean
        Try

            Dim xpadrao As New clsconfig_padroes

            xpadrao.Dadosrecuperarsenha = Me

            xpadrao.Nome = "Envio de e-mails site contato"

            'If xpadrao.tratatextoemail() Then
            '    xmens.Corpo = xpadrao.Textoemail
            '    xmens.Assunto = xpadrao.Assuntoemail
            'End If

            xpadrao.Destinatarios.Add("" & Email & "")


            xpadrao.emailrecuperarsenha()

            '    _mensagemerro = "O e-mail foi enviado para os destinatários do grupo !"

            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function emailnovoassociado() As Boolean
        Try

            Dim xpadrao As New clsconfig_padroes

            xpadrao.Dadosrecuperarsenha = Me

            xpadrao.Nome = "Envio de e-mails site contato"

            'If xpadrao.tratatextoemail() Then
            '    xmens.Corpo = xpadrao.Textoemail
            '    xmens.Assunto = xpadrao.Assuntoemail
            'End If

            xpadrao.Destinatarios.Add("" & Email & "")


            xpadrao.emailparanovoassociado()




            '    _mensagemerro = "O e-mail foi enviado para os destinatários do grupo !"

            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function emailautoatendimento() As Boolean
        Try

            Dim xpadrao As New clsconfig_padroes

            xpadrao.Dadosrecuperarsenha = Me

            xpadrao.Nome = "Envio de e-mails site contato"

            'If xpadrao.tratatextoemail() Then
            '    xmens.Corpo = xpadrao.Textoemail
            '    xmens.Assunto = xpadrao.Assuntoemail
            'End If

            xpadrao.Destinatarios.Add("atendimento@anaplab.com.br")


            xpadrao.envionovoassociadoautoatendimento()




            '    _mensagemerro = "O e-mail foi enviado para os destinatários do grupo !"

            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function validaemailcpf() As Boolean
        Try
            If Cpf = "" Then
                Mensagemerro = "Preencha o campo de CPF"
                Return False
            End If

            If Email = "" Then
                Mensagemerro = "Preencha o campo de email"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbAssociados where cpf = '" & Cpf & "' and email = '" & Email & "' ")
            Nome = tb1.Rows(0)("nome").ToString
            Senha = tb1.Rows(0)("senha").ToString
            Email = tb1.Rows(0)("email").ToString

            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Email ou CPF invalido"
                Return False
            End If
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function recuperarsenha() As Boolean
        Try


            Dim enviaS As String
            Dim xpadrao As New clsconfig_padroes

            '        xpadrao.Dadosdamensagemsite = Me

            xpadrao.Nome = "Esqueci minha senha"

            'If xpadrao.tratatextoemail() Then
            '    xmens.Corpo = xpadrao.Textoemail
            '    xmens.Assunto = xpadrao.Assuntoemail

            'End If

            ' xpadrao.emailrecuperarsenha()

            Mensagemerro = "O e-mail foi enviado para os destinatários do grupo !"

            Return True


            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class




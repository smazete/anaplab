﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Public Class clsvotacao_eleicoes

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _mensagemerro As String
    Dim _mensagemtitulo As String = "Ops !"
    Dim _mensagemicone As String = "error"
    Dim _listaclasse As New List(Of clsvotacao_eleicoes)
    Dim _nrseq As Integer
    Dim _nrseqassociado As Integer
    Dim _dtvotacao As Date
    Dim _nrseqchapa As Integer
    Dim _hrvotacao As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Mensagemtitulo As String
        Get
            Return _mensagemtitulo
        End Get
        Set(value As String)
            _mensagemtitulo = value
        End Set
    End Property

    Public Property Mensagemicone As String
        Get
            Return _mensagemicone
        End Get
        Set(value As String)
            _mensagemicone = value
        End Set
    End Property

    Public Property Listaclasse As List(Of clsvotacao_eleicoes)
        Get
            Return _listaclasse
        End Get
        Set(value As List(Of clsvotacao_eleicoes))
            _listaclasse = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqassociado As Integer
        Get
            Return _nrseqassociado
        End Get
        Set(value As Integer)
            _nrseqassociado = value
        End Set
    End Property

    Public Property Dtvotacao As Date
        Get
            Return _dtvotacao
        End Get
        Set(value As Date)
            _dtvotacao = value
        End Set
    End Property

    Public Property Nrseqchapa As Integer
        Get
            Return _nrseqchapa
        End Get
        Set(value As Integer)
            _nrseqchapa = value
        End Set
    End Property

    Public Property Hrvotacao As String
        Get
            Return _hrvotacao
        End Get
        Set(value As String)
            _hrvotacao = value
        End Set
    End Property
End Class

Partial Public Class clsvotacao_eleicoes

    Public Function Listarvotacao_eleicoes() As Boolean
        Try
            Listaclasse.Clear()
            tb1 = tab1.conectar("select * from tbvotacao_eleicoes where ativo = true")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaclasse.Add(New clsvotacao_eleicoes With {.Nrseq = numeros(tb1.Rows(x)("nrseq").ToString), .Nrseqassociado = numeros(tb1.Rows(x)("nrseqassociado").ToString), .Dtvotacao = valordata(tb1.Rows(x)("dtvotacao").ToString), .Nrseqchapa = numeros(tb1.Rows(x)("nrseqchapa").ToString), .Hrvotacao = tb1.Rows(x)("hrvotacao").ToString})
            Next
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function procurar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbvotacao_eleicoes where nrseq = " & Nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            Nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
            Nrseqassociado = numeros(tb1.Rows(0)("nrseqassociado").ToString)
            Dtvotacao = FormatDateTime(valordata(tb1.Rows(0)("dtvotacao").ToString), DateFormat.ShortDate)
            Nrseqchapa = numeros(tb1.Rows(0)("nrseqchapa").ToString)
            Hrvotacao = tb1.Rows(0)("hrvotacao").ToString
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbvotacao_eleicoes Set ativo = True"
            If Nrseq <> 0 Then
                xsql &= ",nrseq = " & moeda(Nrseq)
            End If
            If Nrseqassociado <> 0 Then
                xsql &= ",nrseqassociado = " & moeda(Nrseqassociado)
            End If
            If Dtvotacao <> "" Then
                xsql &= ",dtvotacao = '" & formatadatamysql(Dtvotacao) & "'"
            End If
            If Nrseqchapa <> 0 Then
                xsql &= ",nrseqchapa = " & moeda(Nrseqchapa)
            End If
            If Hrvotacao <> "" Then
                xsql &= ",hrvotacao = '" & tratatexto(Hrvotacao) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            tb1 = tab1.IncluirAlterarDados("insert into tbvotacao_eleicoes (nrseqchapa, dtvotacao, hrvotacao, nrseqassociado) values ('" & _nrseqchapa & "','" & hoje() & "','" & hojemysqlcomhoras() & "', '" & _nrseqassociado & "')")
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
End Class


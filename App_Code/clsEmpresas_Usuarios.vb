﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data
Public Class clsEmpresas_Usuarios

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _table As New Data.DataTable
    Dim _tablename As String = "tbempresas_usuarios"

    Dim _nrseq As Integer
    Dim _nrseqempresa As Integer
    Dim _nrsequsuario As Integer
    Dim _ativo As Boolean
    Dim _dtcad As DateTime
    Dim _usercad As String
    Dim _dtexclui As DateTime
    Dim _userexclui As String



    Dim _Mensagemerro As String
    Dim _contador As Integer

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Tablename As String
        Get
            Return _tablename
        End Get
        Set(value As String)
            _tablename = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property


    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property


    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _Mensagemerro
        End Get
        Set(value As String)
            _Mensagemerro = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Nrsequsuario As Integer
        Get
            Return _nrsequsuario
        End Get
        Set(value As Integer)
            _nrsequsuario = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property
End Class

Partial Public Class clsEmpresas_Usuarios

    Public Function consultar() As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & " where nrseq = " & _nrseq)

            If tb1.Rows.Count > 0 Then


                _nrseqempresa = tb1.Rows(0)("nrseqempresa").ToString
                _nrsequsuario = tb1.Rows(0)("nrsequsuario").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _usercad = tb1.Rows(0)("usercad").ToString
                _userexclui = tb1.Rows(0)("userexclui").ToString


                Try
                    If tb1.Rows(0)("dtcad").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtcad").ToString
                        _dtcad = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try
                Try
                    If tb1.Rows(0)("dtexclui").GetType() IsNot GetType(DBNull) Then
                        Dim dt As Date = tb1.Rows(0)("dtexclui").ToString
                        _dtexclui = dt.ToString("yyyy-MM-dd")
                    End If
                Catch exconsultasql As Exception
                    _Mensagemerro = exconsultasql.Message
                End Try



            End If

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function


    Public Function consultarusuarios(Optional tbusuarios As Boolean = False, Optional tbempresas_usuarios As Boolean = False) As Boolean

        Try

            tb1 = tab1.conectar("select * from " & _tablename & IIf(tbusuarios, " inner join tbusuarios on tbempresas_usuarios.nrsequsuario = tbusuarios.nrseq", "") & IIf(tbempresas_usuarios, " inner join tbempresas on tbempresas_usuarios.nrseqempresa = tbempresas.nrseq", "") & " where tbempresas_usuarios.nrseqempresa = '" & _nrseqempresa & "' and tbempresas_usuarios.ativo=true")

            _contador = tb1.Rows.Count
            _table = tb1
            Return True
        Catch exconsultar As Exception
            _Mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function Novo() As Boolean

        Try


            Dim xusuarios As New clsusuarios

            tb1 = tab1.IncluirAlterarDados("insert into tbempresas_usuarios (nrseqempresa, ativo, nrsequsuario, dtcad, usercad) select  " & HttpContext.Current.Session("codigoempresa") & ",true ,nrseq,'" & hoje() & "','" & HttpContext.Current.Session("usuario") & "' from tbusuarios where ativo = true and usuario = '" & xusuarios.Usuario & "'")

            Return True

        Catch exnovo As Exception
            _Mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function salvar() As Boolean

        Try

            tb1 = tab1.IncluirAlterarDados("update " & _tablename & " set ativo=" & _ativo & ", nrseqempresa='" & _nrseqempresa & "',nrsequsuario='" & _nrsequsuario & "' where nrseq= " & _nrseq)

            Return True
        Catch exsalvar As Exception
            _Mensagemerro = exsalvar.ToString
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _Mensagemerro = "Please, enter a valid Funcionario!"
                Return False
            End If
            Dim xusuarios As New clsusuarios
            consultar()

            tb1 = tab1.IncluirAlterarDados("update tbempresas_usuarios set ativo = " & IIf(_ativo, " false, dtexclui = '" & hoje() & "',userexclui = '" & HttpContext.Current.Session("usuario") & "', nrsequsuario = (select  nrseq  from tbusuarios where ativo = true and usuario = '" & xusuarios.Usuario & "')", "true") & " where nrseqempresa = " & HttpContext.Current.Session("codigoempresa"))

            Return True
        Catch exexcluir As Exception
            _Mensagemerro = exexcluir.ToString
            Return False

        End Try

    End Function

End Class
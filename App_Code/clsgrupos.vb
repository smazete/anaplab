﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports System.Data

Public Class clsgrupos
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco

    Dim _exibirocultos As Boolean = False
    Dim _tipo As String = ""
    Dim _tabelagrupos As New Data.DataTable
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _descricao As String = ""
    Dim _ativo As Boolean = True
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _usuariosgrupos As New List(Of clsgrupos_usuarios)
    Dim _usuariosinativos As New List(Of clsusuarios)
    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Usuariosgrupos As List(Of clsgrupos_usuarios)
        Get
            Return _usuariosgrupos
        End Get
        Set(value As List(Of clsgrupos_usuarios))
            _usuariosgrupos = value
        End Set
    End Property

    Public Property Usuariosinativos As List(Of clsusuarios)
        Get
            Return _usuariosinativos
        End Get
        Set(value As List(Of clsusuarios))
            _usuariosinativos = value
        End Set
    End Property

    Public Property Tabelagrupos As DataTable
        Get
            Return _tabelagrupos
        End Get
        Set(value As DataTable)
            _tabelagrupos = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property

    Public Property Exibirocultos As Boolean
        Get
            Return _exibirocultos
        End Get
        Set(value As Boolean)
            _exibirocultos = value
        End Set
    End Property
End Class

Partial Public Class clsgrupos

    Public Function procurar(Optional xportipo As Boolean = False) As Boolean
        If xportipo Then

            tb1 = tab1.conectar("select * from tbgrupos where ativo = true and tipo = '" & _tipo & "'")
        Else
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbgrupos where nrseq = " & Nrseq)
        End If

        If tb1.Rows.Count = 0 Then
            Mensagemerro = "Selecione um item válido para procurar"
            Return False
        End If
        Nrseq = tb1.Rows(0)("nrseq").ToString
        Tipo = tb1.Rows(0)("tipo").ToString
        Descricao = tb1.Rows(0)("descricao").ToString
        Ativo = tb1.Rows(0)("ativo").ToString
        Dtcad = FormatDateTime(tb1.Rows(0)("dtcad").ToString, DateFormat.ShortDate)
        Usercad = tb1.Rows(0)("usercad").ToString
        retornarlistaativos()
        retornarlistainativos()
        Return True
        Try
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function
    Public Function adicionausuario() As Boolean

        Try

            If Usuariosgrupos.Count = 0 Then
                _mensagemerro = "Selecione ao menos um usuário para cadastrar !"
                Return False
            End If
            tbx = tabx.conectar("select vwgrupos_usuarios.* from vwgrupos_usuarios  where ativo = true and nrseqgrupo = " & _nrseq)
            For x As Integer = 0 To Usuariosgrupos.Count - 1
                Dim tbproc As Data.DataRow()
                tbproc = tbx.Select(" usuario = '" & Usuariosgrupos(x).Usuario & "'")
                If tbproc.Count = 0 Then
                    tb1 = tab1.IncluirAlterarDados("insert into tbgrupos_usuarios (nrseqgrupo, nrsequsuario, ativo, dtcad, usercad) values (" & _nrseq & ", " & Usuariosgrupos(x).Nrsequsuario & ", true, '" & hoje() & "', '" & _usercad & "')")

                End If
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try



    End Function
    Public Function removeusuarios() As Boolean

        Try

            If Usuariosgrupos.Count = 0 Then
                _mensagemerro = "Selecione ao menos um usuário para excluir !"
                Return False
            End If

            For x As Integer = 0 To Usuariosgrupos.Count - 1
                tbx = tabx.conectar("update tbgrupos_usuarios  set ativo = false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "' where ativo = true and nrsequsuario = '" & Usuariosgrupos(x).Nrsequsuario & "' and nrseqgrupo = " & _nrseq)
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try



    End Function

    Public Function retornarlistaativos() As Boolean
        Try

            Usuariosgrupos.Clear()
            tbx = tabx.conectar("select vwgrupos_usuarios.* from vwgrupos_usuarios  where ativo = true and nrseqgrupo = " & _nrseq)
            For x As Integer = 0 To tbx.Rows.Count - 1
                Usuariosgrupos.Add(New clsgrupos_usuarios With {.Ativo = True, .Usuario = tbx.Rows(x)("usuario").ToString, .Nrseq = tbx.Rows(x)("nrseq").ToString, .Nrseqgrupo = _nrseq, .Email = tbx.Rows(x)("email").ToString})
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function retornarlistainativos() As Boolean
        Try

            Usuariosinativos.Clear()
            tbx = tabx.conectar("select * from vwusuarios where ativo = true and not usuario in ( select vwgrupos_usuarios.usuario from vwgrupos_usuarios  where ativo = true and nrseqgrupo = " & _nrseq & ")")
            For x As Integer = 0 To tbx.Rows.Count - 1
                Usuariosinativos.Add(New clsusuarios With {.Ativo = True, .Usuario = tbx.Rows(x)("usuario").ToString, .Nrseq = tbx.Rows(x)("nrseq").ToString, .Email = tbx.Rows(x)("email").ToString})
            Next
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbgrupos Set ativo = True, tipo = '" & _tipo & "'"

            If Descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(Descricao) & "'"
            End If


            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole("")
            tb1 = tab1.IncluirAlterarDados("insert into tbgrupos (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbgrupos where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbgrupos set ativo = " & IIf(Ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & Usercad & "'", "true") & " where nrseq = " & Nrseq)
            mostrargrupos()
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
    Public Function mostrargrupos(Optional xturno As String = "") As Boolean
        Try


            _tabelagrupos = tab1.conectar("select * from tbgrupos where 1=1 " & IIf(xturno <> "", " and descricao = '" & xturno & "'", "") & IIf(Exibirocultos, " ", " and ativo = true ") & " order by descricao")

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class



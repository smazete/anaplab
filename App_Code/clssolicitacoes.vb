﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes

Public Class clssolicitacoes

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco

    Dim _mensagemerro As String = ""
    Dim _nrseq As Integer = 0
    Dim _nrseqpad As Integer = 0
    Dim _dtcad As Date
    Dim _usercad As String = ""
    Dim _ativo As Boolean = True
    Dim _descricao As String = ""
    Dim _liberado As Boolean = False
    Dim _hrcad As String = ""
    Dim _etapa As Integer = 1

    Dim _matriculagestor As String = ""
    Dim _matriculacolaborador As String = ""
    Dim _nomegestor As String = ""
    Dim _nomecolaborador As String = ""
    Dim _nrsequsuariogestor As Integer = 0
    Dim _emailgestor As String = ""

    Dim _nrsequsuariorespondido As Integer = 0
    Dim _contato As New List(Of clssolicitacoes_contatos)


    Public Sub New()
        versessao()

        _usercad = HttpContext.Current.Session("usuario")
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqpad As Integer
        Get
            Return _nrseqpad
        End Get
        Set(value As Integer)
            _nrseqpad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property

    Public Property Liberado As Boolean
        Get
            Return _liberado
        End Get
        Set(value As Boolean)
            _liberado = value
        End Set
    End Property

    Public Property Hrcad As String
        Get
            Return _hrcad
        End Get
        Set(value As String)
            _hrcad = value
        End Set
    End Property

    Public Property Etapa As Integer
        Get
            Return _etapa
        End Get
        Set(value As Integer)
            _etapa = value
        End Set
    End Property

    Public Property Nrsequsuariorespondido As Integer
        Get
            Return _nrsequsuariorespondido
        End Get
        Set(value As Integer)
            _nrsequsuariorespondido = value
        End Set
    End Property

    Public Property Contato As List(Of clssolicitacoes_contatos)
        Get
            Return _contato
        End Get
        Set(value As List(Of clssolicitacoes_contatos))
            _contato = value
        End Set
    End Property

    Public Property Matriculagestor As String
        Get
            Return _matriculagestor
        End Get
        Set(value As String)
            _matriculagestor = value
        End Set
    End Property

    Public Property Matriculacolaborador As String
        Get
            Return _matriculacolaborador
        End Get
        Set(value As String)
            _matriculacolaborador = value
        End Set
    End Property

    Public Property Nomegestor As String
        Get
            Return _nomegestor
        End Get
        Set(value As String)
            _nomegestor = value
        End Set
    End Property

    Public Property Nomecolaborador As String
        Get
            Return _nomecolaborador
        End Get
        Set(value As String)
            _nomecolaborador = value
        End Set
    End Property

    Public Property Nrsequsuariogestor As Integer
        Get
            Return _nrsequsuariogestor
        End Get
        Set(value As Integer)
            _nrsequsuariogestor = value
        End Set
    End Property

    Public Property Emailgestor As String
        Get
            Return _emailgestor
        End Get
        Set(value As String)
            _emailgestor = value
        End Set
    End Property
End Class
Partial Public Class clssolicitacoes

    Public Function enviar() As Boolean
        Try


            Dim wcnrseqctrl As String = gerarnrseqcontrole()


            tb1 = tab1.IncluirAlterarDados("insert into tbsolicitacoes (nrseqpad, descricao, hrcad, usercad, dtcad, ativo, etapa, liberado, nrseqctrl) values (" & Nrseqpad & ", '" & Descricao & "', '" & Hrcad & "', '" & _usercad & "','" & _dtcad & "', true, " & _etapa & ", false, '" & wcnrseqctrl & "')")
            tb1 = tab1.conectar("select * from tbsolicitacoes where nrseqctrl = '" & wcnrseqctrl & "'")

            _nrseq = tb1.Rows(0)("nrseq").ToString

            tb1 = tab1.conectar("select * from vwusuarios where permissao = 'Administradores' or permissao like 'RH%'")
            For x As Integer = 0 To tb1.Rows.Count - 1
                tbx = tabx.IncluirAlterarDados("insert into tbsolicitacoes_contatos (nrseqsol, nrsequsuario, ativo, respondido) values (" & _nrseq & ", " & tb1.Rows(x)("nrseq").ToString & ", true, false)")

                Dim xenvio As New clscaixamensagens
                xenvio.Assunto = "Solicitação de desbloqueio de metas"
                xenvio.Corpo = "O usuário " & _usercad & " solicitou o desbloqueio das metas da etapa " & _etapa & " do pad " & _nrseqpad & ". Por favor, entre no sistema para realizar o desbloqueio !"
                xenvio.destinatarios.Add(New clscaixamensagens_destinatarios With {.Nrsequsuario = tb1.Rows(x)("nrseq").ToString, .Email = tb1.Rows(x)("email").ToString, .Usuario = tb1.Rows(x)("usuario").ToString})
                xenvio.Urgente = True
                xenvio.Remetente = _usercad
                If xenvio.enviar Then

                End If
            Next

            Return True
        Catch exenviar As Exception
            Return False
        End Try



    End Function
    Public Function baixar() As Boolean
        Try

            If Not carregar() Then

                Return False
            End If


            tb1 = tab1.IncluirAlterarDados("update tbsolicitacoes set liberado = true, nrsequsuariorespondido = " & _nrsequsuariorespondido & " where nrseq = " & Nrseq)

            Dim xenvio As New clscaixamensagens
            xenvio.Assunto = "Confirmação de desbloqueio de metas"
            xenvio.Corpo = "Sua solicitação de desbloqueio foi atendida. Por favor, verifique o PAD para preencher as novas metas/objetivos !"
            xenvio.destinatarios.Add(New clscaixamensagens_destinatarios With {.Nrsequsuario = _nrsequsuariogestor, .Email = _emailgestor})
            xenvio.Urgente = True
            xenvio.Remetente = _usercad
            If xenvio.enviar Then

            End If

            Return True
        Catch exbaixxar As Exception
            _mensagemerro = exbaixxar.Message
            Return False
        End Try
    End Function
    Public Function carregar() As Boolean
        Try
            tb1 = tab1.conectar("select * from vwsolicitacoes where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "A solicitação não existe !"
                Return False
            End If

            _nrseqpad = tb1.Rows(0)("nrseqpad").ToString
            _hrcad = tb1.Rows(0)("hrcad").ToString
            _dtcad = tratadata(tb1.Rows(0)("dtcad").ToString, gravardata.gravar.recuperar)
            _descricao = tb1.Rows(0)("descricao").ToString
            _ativo = tb1.Rows(0)("ativo").ToString
            _liberado = tb1.Rows(0)("liberado").ToString
            _usercad = tb1.Rows(0)("usercad").ToString
            _matriculacolaborador = tb1.Rows(0)("matriculacolaborador").ToString
            _matriculagestor = tb1.Rows(0)("matriculagestor").ToString
            _nrsequsuariogestor = tb1.Rows(0)("nrsequsuariogestor").ToString
            _nomegestor = tb1.Rows(0)("nomegestor").ToString
            _nomecolaborador = tb1.Rows(0)("nomecolaborador").ToString
            _emailgestor = tb1.Rows(0)("emailgestor").ToString


            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class
﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports System.IO
Imports clssessoes

Public Class clsacoesjudiciais
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _mensagemerro As String
    Dim _listaclasse As New List(Of clsacoesjudiciais)
    Dim _nrseq As Integer
    Dim _titulo As String
    Dim _descricao As String
    Dim _descricaotermo As String
    Dim _arquivocontrato As String
    Dim _descricaocontrato As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _validoate As Date
    Dim _ativo As Boolean
    Dim _arquivotermo As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Listaclasse As List(Of clsacoesjudiciais)
        Get
            Return _listaclasse
        End Get
        Set(value As List(Of clsacoesjudiciais))
            _listaclasse = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Titulo As String
        Get
            Return _titulo
        End Get
        Set(value As String)
            _titulo = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Descricaotermo As String
        Get
            Return _descricaotermo
        End Get
        Set(value As String)
            _descricaotermo = value
        End Set
    End Property

    Public Property Arquivocontrato As String
        Get
            Return _arquivocontrato
        End Get
        Set(value As String)
            _arquivocontrato = value
        End Set
    End Property

    Public Property Descricaocontrato As String
        Get
            Return _descricaocontrato
        End Get
        Set(value As String)
            _descricaocontrato = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Validoate As Date
        Get
            Return _validoate
        End Get
        Set(value As Date)
            _validoate = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Arquivotermo As String
        Get
            Return _arquivotermo
        End Get
        Set(value As String)
            _arquivotermo = value
        End Set
    End Property
End Class

Partial Public Class clsacoesjudiciais

    Public Function Listaracoesjudiciais() As Boolean
        Try
            Listaclasse.Clear()
            tb1 = tab1.conectar("select * from tbacoesjudiciais where ativo = true")
            For x As Integer = 0 To tb1.Rows.Count - 1
                Listaclasse.Add(New clsacoesjudiciais With {.Nrseq = tb1.Rows(x)("nrseq").ToString, .Titulo = tb1.Rows(x)("titulo").ToString, .Descricao = tb1.Rows(x)("descricao").ToString, .Descricaotermo = tb1.Rows(x)("descricaotermo").ToString, .Arquivocontrato = tb1.Rows(x)("arquivocontrato").ToString, .Descricaocontrato = tb1.Rows(x)("descricaocontrato").ToString, .Dtcad = valordata(tb1.Rows(x)("dtcad").ToString), .Usercad = tb1.Rows(x)("usercad").ToString, .Validoate = valordata(tb1.Rows(x)("validoate").ToString), .Ativo = tb1.Rows(x)("ativo").ToString, .Arquivotermo = tb1.Rows(x)("arquivotermo").ToString})
            Next
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function


    Public Function procurar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
            End If
            tb1 = tab1.conectar("select * from tbacoesjudiciais where nrseq = " & Nrseq)
            If tb1.Rows.Count = 0 Then
                Mensagemerro = "Selecione um item válido para procurar"
                Return False
            End If
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Titulo = tb1.Rows(0)("titulo").ToString
            Descricao = tb1.Rows(0)("descricao").ToString
            Descricaotermo = tb1.Rows(0)("descricaotermo").ToString
            Arquivocontrato = tb1.Rows(0)("arquivocontrato").ToString
            Descricaocontrato = tb1.Rows(0)("descricaocontrato").ToString
            Dtcad = FormatDateTime(valordata(tb1.Rows(0)("dtcad").ToString), DateFormat.ShortDate)
            Usercad = tb1.Rows(0)("usercad").ToString
            Validoate = FormatDateTime(valordata(tb1.Rows(0)("validoate").ToString), DateFormat.ShortDate)
            Ativo = logico(tb1.Rows(0)("ativo").ToString)
            Arquivotermo = tb1.Rows(0)("arquivotermo").ToString
            Return True
        Catch excons As Exception
            Mensagemerro = excons.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbacoesjudiciais Set ativo = True"
            If Nrseq <> 0 Then
                xsql &= ", nrseq = " & moeda(Nrseq)
            End If
            If Titulo <> "" Then
                xsql &= ", titulo = '" & tratatexto(Titulo) & "'"
            End If
            If Descricao <> "" Then
                xsql &= ", descricao = '" & tratatexto(Descricao) & "'"
            End If
            If Descricaotermo <> "" Then
                xsql &= ", descricaotermo = '" & tratatexto(Descricaotermo) & "'"
            End If
            If Arquivocontrato <> "" Then
                xsql &= ", arquivocontrato = '" & tratatexto(Arquivocontrato) & "'"
            End If
            If Descricaocontrato <> "" Then
                xsql &= ", descricaocontrato = '" & tratatexto(Descricaocontrato) & "'"
            End If
            If formatadatamysql(Dtcad) <> "" Then
                xsql &= ", dtcad = '" & formatadatamysql(Dtcad) & "'"
            End If
            If Usercad <> "" Then
                xsql &= ", usercad = '" & tratatexto(Usercad) & "'"
            End If
            If formatadatamysql(Validoate) <> "" Then
                xsql &= ", validoate = '" & formatadatamysql(Validoate) & "'"
            End If
            If Ativo <> True Then
                xsql &= ", ativo = '" & logico(Ativo) & "'"
            End If
            If Arquivotermo <> "" Then
                xsql &= ", arquivotermo = '" & tratatexto(Arquivotermo) & "'"
            End If
            xsql &= " where nrseq = " & Nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbacoesjudiciais (nrseqctrl, dtcad, usercad, ativo) values ('" & wcnrseqctrl & "','" & hoje() & "','" & Usercad & "', false)")
            tb1 = tab1.conectar("Select * from  tbacoesjudiciais where nrseqctrl = '" & wcnrseqctrl & "'")
            Nrseq = tb1.Rows(0)("nrseq").ToString
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function excluir() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbacoesjudiciais set ativo = 0 where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function

    Public Function ativar() As Boolean
        Try
            If Nrseq = 0 Then
                Mensagemerro = "Por favor, informe um item para excluir !"
                Return False
            End If
            If Not procurar() Then
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbacoesjudiciais set ativo = '1' where nrseq = " & Nrseq)
            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try

    End Function

End Class

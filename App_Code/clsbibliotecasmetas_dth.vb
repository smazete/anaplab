﻿Imports Microsoft.VisualBasic
Imports clsSmart

Public Class clsbibliotecasmetas_dth

    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco

    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nrseqbiblioteca As Integer = 0
    Dim _nrseqobjetivo As Integer = 0
    Dim _descricao As String = ""
    Dim _ativo As Boolean = True
    Dim _descricaoobjetivo As String
    Dim _nomegestor As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqbiblioteca As Integer
        Get
            Return _nrseqbiblioteca
        End Get
        Set(value As Integer)
            _nrseqbiblioteca = value
        End Set
    End Property

    Public Property Nrseqobjetivo As Integer
        Get
            Return _nrseqobjetivo
        End Get
        Set(value As Integer)
            _nrseqobjetivo = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property descricaoobjetivo As String
        Get
            Return _descricaoobjetivo
        End Get
        Set(value As String)

            _descricaoobjetivo = tratatexto(value)

            If value <> "" Then
                tbx = tabx.conectar("select * from tbobjetivos where ativo = true and descricao = '" & _descricaoobjetivo & "'")
                If tbx.Rows.Count = 0 Then
                    _nrseqobjetivo = 0
                Else
                    _nrseqobjetivo = tbx.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Nomegestor As String
        Get
            Return _nomegestor
        End Get
        Set(value As String)
            _nomegestor = value
        End Set
    End Property
End Class

﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.Data

Public Class clsContasPagar

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    Dim tablename As String = "tbplanocontas"
    Dim _table As Data.DataTable
    Dim _contador As Integer = 0

    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nrseqdocumento As Integer = 0
    Dim _nrseqplano As Integer = 0
    Dim _dtvencimento As Date
    Dim _dtexecucao As Date
    Dim _dtbaixa As Date
    Dim _userbaixa As String
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _ativo As Boolean
    Dim _valor As Decimal = 0
    Dim _valorpago As Decimal = 0
    Dim _juros As Decimal = 0
    Dim _multa As Decimal = 0
    Dim _obs As String
    Dim _baixa As Boolean
    Dim _salvo As Boolean
    Dim _nrseqctrl As String
    Dim _descricao As String
    Dim _operacao As String
    Dim _qtdparcelas As Integer = 0
    Dim _parcela As Integer = 0
    Dim _desconto As Decimal = 0
    Dim _conciliado As Boolean
    Dim _dtconciliado As Date
    Dim _userconciliado As String
    Dim _solaprovacao As Boolean
    Dim _dtsolaprovacao As Date
    Dim _usersolaprovacao As String
    Dim _dtaprovada As Date
    Dim _useraprovada As String
    Dim _aprovada As Boolean
    Dim _nrseqcentrocusto As Integer = 0
    Dim _data As Date
    Dim _nrseqempresa As Integer = 0
    Dim _nrseqgrupo As Integer = 0
    Dim _nrseqcaixa As Integer = 0
    Dim _nrseqformapagto As Integer = 0

    Dim _descricaocontas As String
    Dim _descricaocentrodecusto As String
    Dim _descricaodocumento As String

    Dim _codigo As String
    Dim _grade As GridView

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqdocumento As Integer
        Get
            Return _nrseqdocumento
        End Get
        Set(value As Integer)
            _nrseqdocumento = value
        End Set
    End Property

    Public Property Nrseqplano As Integer
        Get
            Return _nrseqplano
        End Get
        Set(value As Integer)
            _nrseqplano = value
        End Set
    End Property

    Public Property Dtvencimento As Date
        Get
            Return _dtvencimento
        End Get
        Set(value As Date)
            _dtvencimento = value
        End Set
    End Property

    Public Property Dtexecucao As Date
        Get
            Return _dtexecucao
        End Get
        Set(value As Date)
            _dtexecucao = value
        End Set
    End Property

    Public Property Dtbaixa As Date
        Get
            Return _dtbaixa
        End Get
        Set(value As Date)
            _dtbaixa = value
        End Set
    End Property

    Public Property Userbaixa As String
        Get
            Return _userbaixa
        End Get
        Set(value As String)
            _userbaixa = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Valor As Decimal
        Get
            Return _valor
        End Get
        Set(value As Decimal)
            _valor = value
        End Set
    End Property

    Public Property Valorpago As Decimal
        Get
            Return _valorpago
        End Get
        Set(value As Decimal)
            _valorpago = value
        End Set
    End Property

    Public Property Juros As Decimal
        Get
            Return _juros
        End Get
        Set(value As Decimal)
            _juros = value
        End Set
    End Property

    Public Property Multa As Decimal
        Get
            Return _multa
        End Get
        Set(value As Decimal)
            _multa = value
        End Set
    End Property

    Public Property Obs As String
        Get
            Return _obs
        End Get
        Set(value As String)
            _obs = value
        End Set
    End Property

    Public Property Baixa As Boolean
        Get
            Return _baixa
        End Get
        Set(value As Boolean)
            _baixa = value
        End Set
    End Property

    Public Property Salvo As Boolean
        Get
            Return _salvo
        End Get
        Set(value As Boolean)
            _salvo = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property

    Public Property Operacao As String
        Get
            Return _operacao
        End Get
        Set(value As String)
            _operacao = value
        End Set
    End Property

    Public Property Qtdparcelas As Integer
        Get
            Return _qtdparcelas
        End Get
        Set(value As Integer)
            _qtdparcelas = value
        End Set
    End Property

    Public Property Parcela As Integer
        Get
            Return _parcela
        End Get
        Set(value As Integer)
            _parcela = value
        End Set
    End Property

    Public Property Desconto As Decimal
        Get
            Return _desconto
        End Get
        Set(value As Decimal)
            _desconto = value
        End Set
    End Property

    Public Property Conciliado As Boolean
        Get
            Return _conciliado
        End Get
        Set(value As Boolean)
            _conciliado = value
        End Set
    End Property

    Public Property Dtconciliado As Date
        Get
            Return _dtconciliado
        End Get
        Set(value As Date)
            _dtconciliado = value
        End Set
    End Property

    Public Property Userconciliado As String
        Get
            Return _userconciliado
        End Get
        Set(value As String)
            _userconciliado = value
        End Set
    End Property

    Public Property Solaprovacao As Boolean
        Get
            Return _solaprovacao
        End Get
        Set(value As Boolean)
            _solaprovacao = value
        End Set
    End Property

    Public Property Dtsolaprovacao As Date
        Get
            Return _dtsolaprovacao
        End Get
        Set(value As Date)
            _dtsolaprovacao = value
        End Set
    End Property

    Public Property Usersolaprovacao As String
        Get
            Return _usersolaprovacao
        End Get
        Set(value As String)
            _usersolaprovacao = value
        End Set
    End Property

    Public Property Dtaprovada As Date
        Get
            Return _dtaprovada
        End Get
        Set(value As Date)
            _dtaprovada = value
        End Set
    End Property

    Public Property Useraprovada As String
        Get
            Return _useraprovada
        End Get
        Set(value As String)
            _useraprovada = value
        End Set
    End Property

    Public Property Aprovada As Boolean
        Get
            Return _aprovada
        End Get
        Set(value As Boolean)
            _aprovada = value
        End Set
    End Property

    Public Property Nrseqcentrocusto As Integer
        Get
            Return _nrseqcentrocusto
        End Get
        Set(value As Integer)
            _nrseqcentrocusto = value
        End Set
    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Property Descricaocontas As String
        Get
            Return _descricaocontas
        End Get
        Set(value As String)
            _descricaocontas = value
            If value <> "" Then
                tb1 = tab1.conectar("select * from tbplanocontas where ativo = true and descricao = '" & value & "'")
                If tb1.Rows.Count = 0 Then
                    _nrseqplano = 0
                Else
                    _nrseqplano = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Descricaocentrodecusto As String
        Get
            Return _descricaocentrodecusto
        End Get
        Set(value As String)
            _descricaocentrodecusto = value
            If value <> "" Then
                tb1 = tab1.conectar("select * from tbplanocontas where ativo = true and descricao = '" & value & "'")
                If tb1.Rows.Count = 0 Then
                    _nrseqcentrocusto = 0
                Else
                    _nrseqcentrocusto = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Descricaodocumento As String
        Get
            Return _descricaodocumento
        End Get
        Set(value As String)
            _descricaodocumento = tratatexto(value)
            If value <> "" Then
                tb1 = tab1.conectar("select * from tbdocumentos where ativo = true and descricao = '" & value & "'")
                If tb1.Rows.Count = 0 Then
                    _nrseqdocumento = 0
                Else
                    _nrseqdocumento = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property
    '' BUGUEI AQUI
    Public Property Codigo As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
            If _codigo <> "" Then
                tb1 = tab1.conectar("select * from tbplanocontas where ativo = true and codigo = '" & value & "'")
                If tb1.Rows.Count = 0 Then
                    _nrseqplano = 0
                Else
                    _nrseqplano = tb1.Rows(0)("nrseq").ToString
                End If
            End If
        End Set
    End Property

    Public Property Grade As GridView
        Get
            Return _grade
        End Get
        Set(value As GridView)
            _grade = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Nrseqgrupo As Integer
        Get
            Return _nrseqgrupo
        End Get
        Set(value As Integer)
            _nrseqgrupo = value
        End Set
    End Property

    Public Property Nrseqcaixa As Integer
        Get
            Return _nrseqcaixa
        End Get
        Set(value As Integer)
            _nrseqcaixa = value
        End Set
    End Property

    Public Property Nrseqformapagto As Integer
        Get
            Return _nrseqformapagto
        End Get
        Set(value As Integer)
            _nrseqformapagto = value
        End Set
    End Property

    Public Sub New()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = valordatamysql(hoje)
        _nrseqctrl = gerarnrseqcontrole()
        _dtexclui = valordatamysql(_dtexclui)
        _dtaprovada = valordatamysql(_dtaprovada)
        _dtbaixa = valordatamysql(_dtbaixa)
        _dtconciliado = valordatamysql(_dtconciliado)
        _dtexecucao = valordatamysql(_dtexecucao)
        _dtsolaprovacao = valordatamysql(_dtsolaprovacao)
        _dtvencimento = valordatamysql(_dtvencimento)
    End Sub

End Class


Partial Public Class clsContasPagar
    Public Function Novo() As Boolean
        Try
            tb1 = tab1.IncluirAlterarDados("insert into tbcontas (ativo, salvo, dtcad, usercad, nrseqctrl) values (" & _ativo & ", " & _salvo & ", '" & hoje() & "', '" & _usercad & "','" & _nrseqctrl & "')")

            tb1 = tab1.conectar("select * from tbcontas where nrseqctrl = '" & _nrseqctrl & "'")

            Nrseq = tb1.Rows(0)("nrseq").ToString

            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function

    Public Function salvar() As Boolean
        Try
            Dim xsql As String
            xsql = " update tbcontas Set ativo = True,salvo= True"

            If numeros(_nrseqdocumento) <> 0 Then
                xsql &= ",nrseqdocumento = " & numeros(_nrseqdocumento)
            End If
            If numeros(_nrseqplano) <> 0 Then
                xsql &= ",nrseqplano = " & numeros(_nrseqplano)
            End If
            If valordatamysql(_dtvencimento) <> "" Then
                xsql &= ",dtvencimento = '" & valordatamysql(_dtvencimento) & "'"
            End If
            If valordatamysql(_dtexecucao) <> "" Then
                xsql &= ",dtexecucao = '" & valordatamysql(_dtexecucao) & "'"
            End If
            If valordatamysql(_dtbaixa) <> "" Then
                xsql &= ",dtbaixa = '" & valordatamysql(_dtbaixa) & "'"
            End If
            If _userbaixa <> "" Then
                xsql &= ",userbaixa = '" & tratatexto(_userbaixa) & "'"
            End If
            If _valor <> 0 Then
                xsql &= ",valor = '" & moeda(_valor, False) & "'"
            End If
            If _valorpago <> 0 Then
                xsql &= ",valorpago = '" & moeda(_valorpago, False) & "'"
            End If
            If _juros <> 0 Then
                xsql &= ",juros = '" & moeda(_juros, False) & "'"
            End If
            If _multa <> 0 Then
                xsql &= ",multa = '" & moeda(_multa, False) & "'"
            End If
            If _obs <> "" Then
                xsql &= ",obs = '" & tratatexto(_obs) & "'"
            End If
            If logico(_baixa) IsNot Nothing Then
                xsql &= ",baixa = " & logico(_baixa)
            End If
            If _descricao <> "" Then
                xsql &= ",descricao = '" & tratatexto(_descricao) & "'"
            End If
            If _operacao <> "" Then
                xsql &= ",operacao = '" & tratatexto(_operacao) & "'"
            End If
            If numeros(_qtdparcelas) <> 0 Then
                xsql &= ",qtdparcelas = " & numeros(_qtdparcelas)
            End If
            If numeros(_parcela) <> 0 Then
                xsql &= ",parcela = " & numeros(_parcela)
            End If
            If _desconto <> 0 Then
                xsql &= ",desconto = '" & moeda(_desconto, False) & "'"
            End If
            If logico(_conciliado) IsNot Nothing Then
                xsql &= ",conciliado = " & logico(_conciliado)
            End If
            If valordatamysql(_dtconciliado) <> "" Then
                xsql &= ",dtconciliado = '" & valordatamysql(_dtconciliado) & "'"
            End If
            If _userconciliado <> "" Then
                xsql &= ",userconciliado = '" & tratatexto(_userconciliado) & "'"
            End If
            If logico(_solaprovacao) IsNot Nothing Then
                xsql &= ",solaprovacao = " & logico(_solaprovacao)
            End If
            If valordatamysql(_dtsolaprovacao) <> "" Then
                xsql &= ",dtsolaprovacao = '" & valordatamysql(_dtsolaprovacao) & "'"
            End If
            If _usersolaprovacao <> "" Then
                xsql &= ",usersolaprovacao = '" & tratatexto(_usersolaprovacao) & "'"
            End If
            If valordatamysql(_dtaprovada) <> "" Then
                xsql &= ",dtaprovada = '" & valordatamysql(_dtaprovada) & "'"
            End If
            If _useraprovada <> "" Then
                xsql &= ",useraprovada = '" & tratatexto(_useraprovada) & "'"
            End If
            If logico(_aprovada) IsNot Nothing Then
                xsql &= ",aprovada = " & logico(_aprovada)
            End If
            If numeros(_nrseqempresa) <> 0 Then
                xsql &= ",nrseqempresa = " & numeros(_nrseqempresa)
            End If
            If numeros(_nrseqcaixa) <> 0 Then
                xsql &= ",nrseqcaixa = " & numeros(_nrseqcaixa)
            End If
            If numeros(_nrseqcentrocusto) <> 0 Then
                xsql &= ",nrseqcentrocusto = " & numeros(_nrseqcentrocusto)
            End If
            If numeros(_nrseqformapagto) <> 0 Then
                xsql &= ",nrseqformapagto = " & numeros(_nrseqformapagto)
            End If
            xsql &= " where nrseq = " & _nrseq
            tb1 = tab1.IncluirAlterarDados(xsql)
            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try


    End Function


    Public Function excluir() As Boolean

        Try
            If _nrseq = 0 Then
                _mensagemerro = "Please, enter a valid Funcionario!"
                Return False
            End If

            consultar()

            tb1 = tab1.IncluirAlterarDados("update tbcontas set ativo = " & IIf(_ativo, "false, dtexclui = '" & hoje() & "', userexclui = '" & _userexclui & "'", "true") & " where nrseq = " & _nrseq)

            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.ToString
            Return False

        End Try


    End Function
    Public Function carregargrade(grade As GridView) As Boolean
        Try

            Dim tbcontas As New Data.DataTable
            Dim tabcontas As New clsBanco
            tbcontas = tabcontas.conectar("select tbcontas.*,tbplanocontas.codigo as planodecontas, tbplanocontas.descricao as descricaocontas, tbdocumentos.descricao as descricaodoc, tbdocumentos.compensa, tbdocumentos.taxa from tbcontas inner join tbplanocontas on tbcontas.nrseqplano = tbplanocontas.nrseq inner join tbdocumentos on tbcontas.nrseqdocumento = tbdocumentos.nrseq  where tbplanocontas.codigo like '" & HttpContext.Current.Session("idempresaemuso") & "%'")
            grade.DataSource = tbcontas
            grade.DataBind()

            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False

        End Try
    End Function

    Public Function carregar(dtvenc As Date, grade As GridView) As Boolean
        Try

            Dim tbcontas As New Data.DataTable
            Dim tabcontas As New clsBanco
            tbcontas = tabcontas.conectar("select tbcontas.*,tbplanocontas.codigo as planodecontas, tbplanocontas.descricao as descricaocontas, tbdocumentos.descricao as descricaodoc, tbdocumentos.compensa, tbdocumentos.taxa from tbcontas inner join tbplanocontas on tbcontas.nrseqplano = tbplanocontas.nrseq inner join tbdocumentos on tbcontas.nrseqdocumento = tbdocumentos.nrseq  where dtvencimento = '" & valordatamysql(dtvenc) & "' and tbplanocontas.codigo like '" & HttpContext.Current.Session("idempresaemuso") & "%'")
            grade.DataSource = tbcontas
            grade.DataBind()

            Return True
        Catch exsalvar As Exception
            Mensagemerro = exsalvar.Message
            Return False

        End Try
    End Function


    Public Function carregardocumentos(dropdown As DropDownList) As Boolean

        Try
            tb1 = tab1.conectar("select * from tbdocumentos where ativo = true and salvo=true order by descricao")

            dropdown.Items.Clear()
            dropdown.Items.Add(New ListItem("", 0))
            For x As Integer = 0 To tb1.Rows.Count - 1
                dropdown.Items.Add(New ListItem(tb1.Rows(x)("descricao").ToString, tb1.Rows(x)("nrseq").ToString))
            Next
            Return True
        Catch exdoc As Exception
            Mensagemerro = exdoc.Message
            Return False
        End Try

        'ddlOperacao.Items.Add(New ListItem("Selecione a operação", ""))
        'ddlOperacao.Items.Add(New ListItem("Crédito", "C"))
        'ddlOperacao.Items.Add(New ListItem("Débito", "D"))
    End Function

    Public Function carregarFormasdePagamento(dropdown As DropDownList) As Boolean
        dropdown.Items.Clear()
        tb1 = tab1.conectar("select * from tbformapagto where ativo = true and salvo=true order by descricao")
        dropdown.Items.Add(New ListItem("", 0))
        For x As Integer = 0 To tb1.Rows.Count - 1
            dropdown.Items.Add(New ListItem(tb1.Rows(x)("descricao").ToString, tb1.Rows(x)("nrseq").ToString))
        Next
        Return True

    End Function

    Public Function carregarPlanosdeContas(dropdown As DropDownList) As Boolean
        'tb1 = tab1.conectar("select * from tbplanocontas where codigo like '" & zeros(HttpContext.Current.Session("idempresaemuso"), 2) & "%' and ativo = true order by descricao")
        tb1 = tab1.conectar("select * from tbplanocontas where ativo = true order by descricao")

        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", 0))
        For x As Integer = 0 To tb1.Rows.Count - 1
            dropdown.Items.Add(New ListItem(tb1.Rows(x)("descricao").ToString, tb1.Rows(x)("nrseq").ToString))
        Next
        Return True

    End Function

    Public Function carregartodos(Optional ativo As Boolean = True) As Boolean

        Try
            tb1 = tab1.conectar("select * from tbcontas where salvo=true and ativo = " & ativo & " order by descricao")

            If tb1.Rows.Count > 0 Then
                _table = tb1
                _contador = tb1.Rows.Count
            End If
            Return True
        Catch excarregarall As Exception
            _mensagemerro = excarregarall.Message
            Return False
        End Try

    End Function

    Public Function procurarcontasapagar(Optional ativo As Boolean = True, Optional mysqltext As String = "") As Boolean

        Try
            tb1 = tab1.conectar("select * from tbcontas where salvo=true and ativo = " & ativo & " " & IIf(mysqltext <> "", mysqltext, "") & " order by descricao")

            If tb1.Rows.Count > 0 Then
                _nrseqdocumento = numeros(tb1.Rows(0)("nrseqdocumento").ToString)
                _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
                _dtvencimento = valordatamysql(tb1.Rows(0)("dtvencimento").ToString)
                _dtexecucao = valordatamysql(tb1.Rows(0)("dtexecucao").ToString)
                _dtbaixa = valordatamysql(tb1.Rows(0)("dtbaixa").ToString)
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _dtcad = valordatamysql(tb1.Rows(0)("dtcad").ToString)
                _dtexclui = valordatamysql(tb1.Rows(0)("dtexclui").ToString)
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _valor = moeda(tb1.Rows(0)("valor").ToString, False)
                _valorpago = moeda(tb1.Rows(0)("valorpago").ToString, False)
                _juros = moeda(tb1.Rows(0)("juros").ToString)
                _multa = moeda(tb1.Rows(0)("multa").ToString)
                _obs = tb1.Rows(0)("obs").ToString
                _baixa = logico(tb1.Rows(0)("baixa").ToString)
                _salvo = logico(tb1.Rows(0)("salvo").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _operacao = tb1.Rows(0)("operacao").ToString
                _qtdparcelas = numeros(tb1.Rows(0)("qtdparcelas").ToString)
                _parcela = numeros(tb1.Rows(0)("parcela").ToString)
                _desconto = logico(tb1.Rows(0)("desconto").ToString)
                _conciliado = logico(tb1.Rows(0)("conciliado").ToString)
                _dtconciliado = valordatamysql(tb1.Rows(0)("dtconciliado").ToString)
                _userconciliado = tb1.Rows(0)("userconciliado").ToString
                _solaprovacao = logico(tb1.Rows(0)("solaprovacao").ToString)
                _dtsolaprovacao = valordatamysql(tb1.Rows(0)("dtsolaprovacao").ToString)
                _usersolaprovacao = tb1.Rows(0)("usersolaprovacao").ToString
                _dtaprovada = valordatamysql(tb1.Rows(0)("dtaprovada").ToString)
                _useraprovada = tb1.Rows(0)("useraprovada").ToString
                _aprovada = logico(tb1.Rows(0)("aprovada").ToString)
                _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
                _nrseqcaixa = numeros(tb1.Rows(0)("nrseqcaixa").ToString)
                _nrseqcentrocusto = numeros(tb1.Rows(0)("nrseqcentrocusto").ToString)
                _nrseqformapagto = numeros(tb1.Rows(0)("nrseqformapagto").ToString)
            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True
        Catch excarregarall As Exception
            _mensagemerro = excarregarall.Message
            Return False
        End Try

    End Function

    Public Function consultar(Optional ativo As Boolean = True) As Boolean
        Try
            tb1 = tab1.conectar("select * from tbcontas where ativo = true and salvo=true and nrseq=" & _nrseq)

            If tb1.Rows.Count > 0 Then
                _nrseqdocumento = numeros(tb1.Rows(0)("nrseqdocumento").ToString)
                _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
                _dtvencimento = valordatamysql(tb1.Rows(0)("dtvencimento").ToString)
                _dtexecucao = valordatamysql(tb1.Rows(0)("dtexecucao").ToString)
                _dtbaixa = valordatamysql(tb1.Rows(0)("dtbaixa").ToString)
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _dtcad = valordatamysql(tb1.Rows(0)("dtcad").ToString)
                _dtexclui = valordatamysql(tb1.Rows(0)("dtexclui").ToString)
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _valor = moeda(tb1.Rows(0)("valor").ToString, False)
                _valorpago = moeda(tb1.Rows(0)("valorpago").ToString, False)
                _juros = moeda(tb1.Rows(0)("juros").ToString)
                _multa = moeda(tb1.Rows(0)("multa").ToString)
                _obs = tb1.Rows(0)("obs").ToString
                _baixa = logico(tb1.Rows(0)("baixa").ToString)
                _salvo = logico(tb1.Rows(0)("salvo").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _operacao = tb1.Rows(0)("operacao").ToString
                _qtdparcelas = numeros(tb1.Rows(0)("qtdparcelas").ToString)
                _parcela = numeros(tb1.Rows(0)("parcela").ToString)
                _desconto = logico(tb1.Rows(0)("desconto").ToString)
                _conciliado = logico(tb1.Rows(0)("conciliado").ToString)
                _dtconciliado = valordatamysql(tb1.Rows(0)("dtconciliado").ToString)
                _userconciliado = tb1.Rows(0)("userconciliado").ToString
                _solaprovacao = logico(tb1.Rows(0)("solaprovacao").ToString)
                _dtsolaprovacao = valordatamysql(tb1.Rows(0)("dtsolaprovacao").ToString)
                _usersolaprovacao = tb1.Rows(0)("usersolaprovacao").ToString
                _dtaprovada = valordatamysql(tb1.Rows(0)("dtaprovada").ToString)
                _useraprovada = tb1.Rows(0)("useraprovada").ToString
                _aprovada = logico(tb1.Rows(0)("aprovada").ToString)
                _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
                _nrseqcaixa = numeros(tb1.Rows(0)("nrseqcaixa").ToString)
                _nrseqcentrocusto = numeros(tb1.Rows(0)("nrseqcentrocusto").ToString)
                _nrseqformapagto = numeros(tb1.Rows(0)("nrseqformapagto").ToString)
            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.Message
            Return False
        End Try

    End Function

    Public Function consultartodos(Optional ativo As Boolean = True, Optional nome As String = "") As Boolean

        Try

            tb1 = tab1.conectar("Select * from tbcontas Where salvo=true" & IIf(nome <> "", " and nome like '" & nome & "%'", "") & " and ativo= " & ativo & "")

            If tb1.Rows.Count > 0 Then
                _table = tb1
                _contador = tb1.Rows.Count
            End If
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function consultardescricao(Optional checarativo As Boolean = False, Optional consultarnrseq As Boolean = False) As Boolean

        Try

            tb1 = tab1.conectar("select * from tbcontas where salvo=true" & IIf(consultarnrseq = True, " and nrseq=" & _nrseq, " and descricao like '" & _descricao & "%'") & IIf(checarativo = True, " and ativo=" & _ativo, ""))

            If tb1.Rows.Count > 0 Then
                _nrseq = numeros(tb1.Rows(0)("nrseq").ToString)
                _nrseqdocumento = numeros(tb1.Rows(0)("nrseqdocumento").ToString)
                _nrseqplano = numeros(tb1.Rows(0)("nrseqplano").ToString)
                _dtvencimento = valordatamysql(tb1.Rows(0)("dtvencimento").ToString)
                _dtexecucao = valordatamysql(tb1.Rows(0)("dtexecucao").ToString)
                _dtbaixa = valordatamysql(tb1.Rows(0)("dtbaixa").ToString)
                _userbaixa = tb1.Rows(0)("userbaixa").ToString
                _usercad = tb1.Rows(0)("usercad").ToString
                _dtcad = valordatamysql(tb1.Rows(0)("dtcad").ToString)
                _dtexclui = valordatamysql(tb1.Rows(0)("dtexclui").ToString)
                _userexclui = tb1.Rows(0)("userexclui").ToString
                _ativo = logico(tb1.Rows(0)("ativo").ToString)
                _valor = moeda(tb1.Rows(0)("valor").ToString, True)
                _valorpago = moeda(tb1.Rows(0)("valorpago").ToString, True)
                _juros = moeda(tb1.Rows(0)("juros").ToString, True)
                _multa = moeda(tb1.Rows(0)("multa").ToString, True)
                _obs = tb1.Rows(0)("obs").ToString
                _baixa = logico(tb1.Rows(0)("baixa").ToString)
                _salvo = logico(tb1.Rows(0)("salvo").ToString)
                _descricao = tb1.Rows(0)("descricao").ToString
                _operacao = tb1.Rows(0)("operacao").ToString
                _qtdparcelas = numeros(tb1.Rows(0)("qtdparcelas").ToString)
                _parcela = numeros(tb1.Rows(0)("parcela").ToString)
                _desconto = moeda(tb1.Rows(0)("desconto").ToString, True)
                _conciliado = logico(tb1.Rows(0)("conciliado").ToString)
                _dtconciliado = valordatamysql(tb1.Rows(0)("dtconciliado").ToString)
                _userconciliado = tb1.Rows(0)("userconciliado").ToString
                _solaprovacao = logico(tb1.Rows(0)("solaprovacao").ToString)
                _dtsolaprovacao = valordatamysql(tb1.Rows(0)("dtsolaprovacao").ToString)
                _usersolaprovacao = tb1.Rows(0)("usersolaprovacao").ToString
                _dtaprovada = valordatamysql(tb1.Rows(0)("dtaprovada").ToString)
                _useraprovada = tb1.Rows(0)("useraprovada").ToString
                _aprovada = logico(tb1.Rows(0)("aprovada").ToString)
                _nrseqempresa = numeros(tb1.Rows(0)("nrseqempresa").ToString)
                _nrseqcaixa = numeros(tb1.Rows(0)("nrseqcaixa").ToString)
                _nrseqcentrocusto = numeros(tb1.Rows(0)("nrseqcentrocusto").ToString)
                _nrseqformapagto = numeros(tb1.Rows(0)("nrseqformapagto").ToString)
            End If
            _table = tb1
            _contador = tb1.Rows.Count
            Return True
        Catch exconsultar As Exception
            _mensagemerro = exconsultar.ToString
            Return False

        End Try

    End Function

    Public Function carregartodascontas(Optional ativo As Boolean = True) As Boolean

        Try
            tb1 = tab1.conectar("select * from tbcontas where salvo=true and ativo = " & ativo & " order by descricao")

            If tb1.Rows.Count > 0 Then
                _table = tb1
                _contador = tb1.Rows.Count
            End If
            Return True
        Catch excarregarall As Exception
            _mensagemerro = excarregarall.Message
            Return False
        End Try

    End Function


    Public Function carregarCentrodeCusto(dropdown As DropDownList) As Boolean
        'tb1 = tab1.conectar("select * from tbplanocontas where codigo like '" & zeros(HttpContext.Current.Session("idempresaemuso"), 2) & "%' and ativo = true order by descricao")
        tb1 = tab1.conectar("select * from tbcentrodecusto where ativo = true order by descricao")
        dropdown.Items.Clear()
        dropdown.Items.Add(New ListItem("", 0))
        For x As Integer = 0 To tb1.Rows.Count - 1
            dropdown.Items.Add(New ListItem(tb1.Rows(x)("descricao").ToString, tb1.Rows(x)("nrseq").ToString))
        Next
        Return True

    End Function


    Public Sub selectedIndexPlano(page As Page, cboplano As DropDownList, txtcodigo As TextBox)

        If cboplano.Text = "" Then Exit Sub

        tb1 = tab1.conectar("select * from tbplanocontas where ativo = true And descricao = '" & cboplano.Text & "' and codigo like '" & zeros(HttpContext.Current.Session("idempresaemuso"), 2) & "%'")
        If tb1.Rows.Count <> 0 Then
            txtcodigo.Text = tb1.Rows(0)("codigo").ToString
        End If

    End Sub




End Class
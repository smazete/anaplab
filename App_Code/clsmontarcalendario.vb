﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clsSmart
Public Class clsmontarcalendario
    Implements ITemplate


    Dim _retornarcalendario As New Data.DataTable
    Dim _data As Date
    Dim _tempohorarios As Integer = 60
    Dim _retornarhoras As New Data.DataTable
    Dim _docas As New List(Of String)
    Dim _controlehoras As New Control
    Dim _listaagenda As New clsagenda
    Dim _descricaoturno As String = ""
    Dim _hora As String
    Private _templateType As ListItemType

    'A variable to hold the column name.
    Private _columnName As String
    Dim _textohoras As String = "Vazio"
    'A variable to hold the column name.
    Private _FieldArgument As String

    Public Sub New(ByVal type As ListItemType, ByVal colname As String, ByVal parFieldArgument As String)
        _templateType = type

        'Stores the column name.
        _columnName = colname
        _FieldArgument = parFieldArgument


    End Sub
    Public Sub New()
    End Sub
    Public ReadOnly Property Retornarcalendario As DataTable
        Get
            ' Dados da tabela






            Dim tbcalendario As New Data.DataTable
            tbcalendario.Columns.Add("dia01")
            tbcalendario.Columns.Add("dia02")
            tbcalendario.Columns.Add("dia03")
            tbcalendario.Columns.Add("dia04")
            tbcalendario.Columns.Add("dia05")
            tbcalendario.Columns.Add("dia06")
            tbcalendario.Columns.Add("dia07")
            tbcalendario.Columns.Add("data01")
            tbcalendario.Columns.Add("data02")
            tbcalendario.Columns.Add("data03")
            tbcalendario.Columns.Add("data04")
            tbcalendario.Columns.Add("data05")
            tbcalendario.Columns.Add("data06")
            tbcalendario.Columns.Add("data07")

            Dim wcdiasemana As Integer = 1
            Dim wcpulou As Boolean = False
            Dim wcdia As Date
            Dim wcultimodia As Integer = CDate(zeros(1, 2) & "/" & zeros(_data.Month, 2) & "/" & _data.Year).DayOfWeek
            tbcalendario.Rows.Add()
            For x As Integer = 1 To ultimodiames(_data).Day
                wcdia = CDate(zeros(x, 2) & "/" & zeros(_data.Month, 2) & "/" & _data.Year)
                wcdiasemana = wcdia.DayOfWeek + 1


                tbcalendario.Rows(tbcalendario.Rows.Count - 1)("data" & zeros(wcdiasemana, 2)) = wcdia
                tbcalendario.Rows(tbcalendario.Rows.Count - 1)("dia" & zeros(wcdiasemana, 2)) = x
                If 6 - wcultimodia = 0 Then
                    tbcalendario.Rows.Add()

                End If
                If x + 1 <= ultimodiames(_data).Day Then
                    wcultimodia = CDate(zeros(x + 1, 2) & "/" & zeros(_data.Month, 2) & "/" & _data.Year).DayOfWeek
                End If

            Next

            Return tbcalendario
        End Get

    End Property

    Public Property Data As Date
        Get
            Return _data
        End Get
        Set(value As Date)
            _data = value
        End Set
    End Property

    Public Property Tempohorarios As Integer
        Get
            Return _tempohorarios
        End Get
        Set(value As Integer)
            _tempohorarios = value
        End Set
    End Property

    Public Property Retornarhoras As DataTable
        Get
            Dim tbhoras As New Data.DataTable
            tbhoras.Columns.Add("horario")


            For x As Integer = 7 To 20
                tbhoras.Rows.Add()
                tbhoras.Rows(tbhoras.Rows.Count - 1)("horario") = zeros(x, 2) & ":00"
            Next





            Return tbhoras
        End Get
        Set(value As DataTable)
            _retornarhoras = value
        End Set
    End Property

    Public Property Docas As List(Of String)
        Get
            Return _docas
        End Get
        Set(value As List(Of String))
            _docas = value
        End Set
    End Property

    Public Property Controlehoras As Control
        Get
            Return _controlehoras
        End Get
        Set(value As Control)
            _controlehoras = value
        End Set
    End Property

    Public Property Hora As String
        Get
            Return _hora
        End Get
        Set(value As String)
            _hora = value
        End Set
    End Property

    Public Property Textohoras As String
        Get
            Return _textohoras
        End Get
        Set(value As String)
            _textohoras = value
        End Set
    End Property

    Public Property Descricaoturno As String
        Get
            Return _descricaoturno
        End Get
        Set(value As String)
            _descricaoturno = value
        End Set
    End Property

    Public Sub GridViewTemplate()
        'Stores the template type.

    End Sub
    Public Sub InstantiateIn(container As Control) Implements ITemplate.InstantiateIn

        ' container = _controlehoras
        Select Case _templateType
            Case ListItemType.Header
                'Creates a new label control and add it to the container.
                Dim lbl As New Label()
                'Allocates the new label object.
                lbl.Text = _columnName
                'Assigns the name of the column in the lable.
                container.Controls.Add(lbl)
                'Adds the newly created label control to the container.
                Exit Select

            Case ListItemType.Item

                'Creates a new text box control and add it to the container.
                For y As Integer = 0 To _listaagenda.Agendadia.Count - 1
                    If _listaagenda.Agendadia(y).Hora = Hora Then
                        _textohoras = ""
                    End If
                Next

                Dim botao1 As New LinkButton() With {.ID = "btnver" & _FieldArgument, .CommandName = "ver", .CommandArgument = "", .OnClientClick = "clicahoras('" & _FieldArgument & "','" & .CommandArgument & "')"}
                botao1.Controls.Add(New Label With {.Text = _textohoras, .ClientIDMode = ClientIDMode.Predictable, .ID = "lbltexto" & _FieldArgument, .BorderStyle = BorderStyle.None})
                botao1.Controls.Add(New HiddenField With {.Value = "", .ID = "hdstatus" & _FieldArgument, .ClientIDMode = ClientIDMode.Predictable})
                'botao1.Controls.Add(New HiddenField With {.Value = "", .ID = "hdhora"})
                Dim objeto As New Panel
                objeto.ID = "painel" & _FieldArgument
                objeto.ClientIDMode = ClientIDMode.Static
                'Dim hddoca As New HiddenField
                'Dim hdhora As New HiddenField


                objeto.Controls.Add(botao1)
                'objeto.Controls.Add(hddoca)
                'objeto.Controls.Add(hdhora)
                ' objeto.CssClass = "bloco-agenda"

                'AddHandler botao1.Click, AddressOf (clicou())
                ' botao1.CssClass = "bloco-imagens"
                ' botao1.Text = " "
                'Allocates the new text box object.
                ' AddHandler tb1.DataBinding, New EventHandler(AddressOf tb1_DataBinding)
                'Attaches the data binding event.
                'tb1.Columns = 4;          //Creates a column with size 4.
                Dim existe As Boolean = False
                For x As Integer = 0 To container.Controls.Count - 1
                    If container.Controls(x).ID = objeto.ID Then
                        existe = True
                        Exit For
                    End If
                Next
                If Not existe Then
                    container.Controls.Add(objeto)
                End If
                'Adds the newly created textbox to the container.
                Exit Select

            Case ListItemType.EditItem
                'As, I am not using any EditItem, I didnot added any code here.
                Exit Select

            Case ListItemType.Footer
                Dim chkColumn As New CheckBox()
                chkColumn.ID = "Chk" & _columnName
                container.Controls.Add(chkColumn)
                Exit Select
        End Select
    End Sub

    Private Sub montartabela()

    End Sub

End Class

﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Imports System.IO
Imports System.Data

Public Class clsusuarios
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco

    Dim _table As Data.DataTable
    Dim _contador As Integer
    Dim tablename As String = "tbusuarios"

    Dim _mensagemerro As String
    Dim _novousuario As Boolean = True
    Dim _nrseq As Integer = 0
    Dim _nrseqmedicouser As Integer = 0
    Dim _recuperasenhaemail As String = ""
    Dim _permissao As String
    Dim _imagemperfil As String = ""
    Dim _ativo As Boolean
    Dim _usuario As String
    Dim _senha As String
    Dim _email As String
    Dim _emailantigo As String = ""
    Dim _dtcad As Date
    Dim _usercad As String = ""
    Dim _nrseqempresa As Integer = 0
    Dim _nrseqmedico As Integer = 0
    Dim _qtdtentativas As Integer = 0
    Dim _master As String
    Dim _smartcodesolucoes As String
    Dim _usuariosistema As String
    Dim _forcarlogin As Boolean
    Dim _suspenso As Boolean
    Dim _alterado As Boolean
    Dim _nomecolaborador As String
    Dim _gestor As Boolean
    Dim _colaboradores As New List(Of clscolaboradores)


    Public Sub New()
        versessao()
        _usercad = buscarsessoes("usuario")
    End Sub
    Public Sub New(usuariocarga As String)
        _usercad = usuariocarga
        _dtcad = data()
    End Sub

    Public Property Usuario As String
        Get
            Return _usuario
        End Get
        Set(value As String)
            _usuario = value
        End Set
    End Property

    Public Property Senha As String
        Get
            Return _senha
        End Get
        Set(value As String)
            _senha = value
        End Set
    End Property



    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property







    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Imagemperfil As String
        Get
            Return _imagemperfil
        End Get
        Set(value As String)
            _imagemperfil = value
        End Set
    End Property
    Public Property Permissao As String
        Get
            Return _permissao
        End Get
        Set(value As String)
            _permissao = value

        End Set
    End Property



    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Qtdtentativas As Integer
        Get
            Return _qtdtentativas
        End Get
        Set(value As Integer)
            _qtdtentativas = value
        End Set
    End Property

    Public Property Master As String
        Get
            Return _master
        End Get
        Set(value As String)
            _master = value
        End Set
    End Property

    Public Property Smartcodesolucoes As String
        Get
            Return _smartcodesolucoes
        End Get
        Set(value As String)
            _smartcodesolucoes = value
        End Set
    End Property

    Public Property Usuariosistema As String
        Get
            Return _usuariosistema
        End Get
        Set(value As String)
            _usuariosistema = value
        End Set
    End Property

    Public Property Forcarlogin As Boolean
        Get
            Return _forcarlogin
        End Get
        Set(value As Boolean)
            _forcarlogin = value
        End Set
    End Property

    Public Property Suspenso As Boolean
        Get
            Return _suspenso
        End Get
        Set(value As Boolean)
            _suspenso = value
        End Set
    End Property

    Public Property Alterado As Boolean
        Get
            Return _alterado
        End Get
        Set(value As Boolean)
            _alterado = value
        End Set
    End Property

    Public Property Nomecolaborador As String
        Get
            Return _nomecolaborador
        End Get
        Set(value As String)
            _nomecolaborador = value
        End Set
    End Property

    Public Property Gestor As Boolean
        Get
            Return _gestor
        End Get
        Set(value As Boolean)
            _gestor = value
        End Set
    End Property

    Public Property Novousuario As Boolean
        Get
            Return _novousuario
        End Get
        Set(value As Boolean)
            _novousuario = value
        End Set
    End Property

    Public Property Colaboradores As List(Of clscolaboradores)
        Get
            Return _colaboradores
        End Get
        Set(value As List(Of clscolaboradores))
            _colaboradores = value
        End Set
    End Property

    Public Property Recuperasenhaemail As String
        Get
            Return _recuperasenhaemail
        End Get
        Set(value As String)
            _recuperasenhaemail = value
        End Set
    End Property

    Public Property Emailantigo As String
        Get
            Return _emailantigo
        End Get
        Set(value As String)
            _emailantigo = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Nrseqmedico As Integer
        Get
            Return _nrseqmedico
        End Get
        Set(value As Integer)
            _nrseqmedico = value
        End Set
    End Property

    Public Property Nrseqmedicouser As Integer
        Get
            Return _nrseqmedicouser
        End Get
        Set(value As Integer)
            _nrseqmedicouser = value
        End Set
    End Property

    Public Property Table As DataTable
        Get
            Return _table
        End Get
        Set(value As DataTable)
            _table = value
        End Set
    End Property

    Public Property Contador As Integer
        Get
            Return _contador
        End Get
        Set(value As Integer)
            _contador = value
        End Set
    End Property
End Class
Partial Class clsusuarios

    Public Function salvarusuario(Optional naoenviaremail As Boolean = True) As Boolean
        Try
            If _usuario = "" Then
                _mensagemerro = "Entre com um usuário válido!"
                Return False
            End If

            If _senha = "" Then
                _mensagemerro = "Informe uma senha válida !"
                Return False
            End If
            If _email = "" Then
                _mensagemerro = "Informe um e-mail válido !"
                Return False
            End If

            If _permissao = "" Then
                _mensagemerro = "Informe uma permissão válida !"
                Return False
            End If

            tb1 = tab1.conectar("select * from vwusuarios where (usuario = '" & _usuario & "' or email = '" & _email & "') and ativo = true and not nrseq = " & _nrseq)
            If tb1.Rows.Count > 0 Then
                If _novousuario Then
                    _mensagemerro = "Esse usuário já existe no sistema. Por favor, informe um novo nome ou e-mail !"
                    Return False

                Else
                    _nrseq = tb1.Rows(0)("nrseq").ToString
                    alterarusuario()
                    Exit Function
                End If

            End If

            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into tbusuarios (usercad, dtcad, ativo, usuario, senha, permissao, imagemperfil, email, qtdtentativas, master, smartcodesolucoes, usuariosistema, forcarlogin, suspenso, alterado, nrseqctrl, dtvalidadesenha) values ('" & _usercad & "', '" & hoje() & "', true, '" & _usuario & "', '" & _senha & "', '" & _permissao & "', '" & _imagemperfil & "', '" & _email & "', 0, false, false, true, false, false, false, '" & wcnrseqctrl & "', '" & formatadatamysql(data.AddMonths(3)) & "')")

            tb1 = tab1.conectar("select * from tbusuarios where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString

            _mensagemerro = "Usuário cadastrado com sucesso !"
            If naoenviaremail Then
                Return True
            End If


            Dim tbemail As New Data.DataTable
            Dim tabemail As New clsBanco

            tbemail = tabemail.conectar("select * from vwconfig_acoes where nomepadrao = 'newuserssaved' order by nrseq")

            If tbemail.Rows.Count = 0 Then


                Dim lermensagem As New StreamReader(HttpContext.Current.Server.MapPath("~/modelosemails/") & tbemail.Rows(0)("arquivo").ToString)
                Dim htmlm As String = lermensagem.ReadToEnd
                lermensagem.Close()
                Dim email As New clsEnvioEmail


                For y As Integer = 0 To tbemail.Rows.Count - 1
                    If tbemail.Rows(y)("emailusuario").ToString = "" Then Continue For
                    email.AdicionaDestinatarios = tbemail.Rows(y)("emailusuario").ToString
                Next

                email.EhHTML = True
                email.AdicionaMensagem = htmlm.Replace("{usuario}", _usuario)

                email.AdicionaAssunto = "URGENT: New user"
                email.configpadrao()
                email.EnviarEmail()
            End If

            Return True
        Catch exuser As Exception
            _mensagemerro = exuser.Message
            Return False
        End Try

    End Function
    Public Function novo(Optional naoenviaremail As Boolean = True) As Boolean
        Try
            'If _usuario = "" Then
            '    _mensagemerro = "Entre com um usuário válido!"
            '    Return False
            'End If

            'If _senha = "" Then
            '    _mensagemerro = "Informe uma senha válida !"
            '    Return False
            'End If
            'If _email = "" Then
            '    _mensagemerro = "Informe um e-mail válido !"
            '    Return False
            'End If

            'If _permissao = "" Then
            '    _mensagemerro = "Informe uma permissão válida !"
            '    Return False
            'End If


            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into tbusuarios (usercad, dtcad, ativo, usuario, senha, permissao, imagemperfil, email, qtdtentativas, master, smartcodesolucoes, usuariosistema, forcarlogin, suspenso, alterado, nrseqctrl, dtvalidadesenha) values ('" & _usercad & "', '" & hoje() & "', true, '" & _usuario & "', '" & _senha & "', '" & _permissao & "', '" & _imagemperfil & "', '" & _email & "', 0, false, false, true, false, false, false, '" & wcnrseqctrl & "', '" & formatadatamysql(data.AddMonths(3)) & "')")

            tb1 = tab1.conectar("select * from tbusuarios where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString

            _mensagemerro = "Novo Usuário gerado com sucesso !"
            If naoenviaremail Then
                Return True
            End If


            Dim tbemail As New Data.DataTable
            Dim tabemail As New clsBanco

            tbemail = tabemail.conectar("select * from vwconfig_acoes where nomepadrao = 'newuserssaved' order by nrseq")

            If tbemail.Rows.Count = 0 Then


                Dim lermensagem As New StreamReader(HttpContext.Current.Server.MapPath("~/modelosemails/") & tbemail.Rows(0)("arquivo").ToString)
                Dim htmlm As String = lermensagem.ReadToEnd
                lermensagem.Close()
                Dim email As New clsEnvioEmail


                For y As Integer = 0 To tbemail.Rows.Count - 1
                    If tbemail.Rows(y)("emailusuario").ToString = "" Then Continue For
                    email.AdicionaDestinatarios = tbemail.Rows(y)("emailusuario").ToString
                Next

                email.EhHTML = True
                email.AdicionaMensagem = htmlm.Replace("{usuario}", _usuario)

                email.AdicionaAssunto = "URGENT: New user"
                email.configpadrao()
                email.EnviarEmail()
            End If

            Return True
        Catch exuser As Exception
            _mensagemerro = exuser.Message
            Return False
        End Try

    End Function

    Public Function alterarusuario() As Boolean
        Try

            If _nrseq = 0 Then
                _mensagemerro = "Enter a valid user ID !"
                Return False
            End If


            tb1 = tab1.conectar("select * from vwusuarios where (usuario = '" & _usuario & "' or email = '" & _email & "') and ativo = true and not nrseq =" & _nrseq)
            If tb1.Rows.Count > 0 Then
                _mensagemerro = "Esse usuário já existe no sistema. Por favor, informe um novo nome ou e-mail !"
                Return False
            End If


            tb1 = tab1.conectar("select * from vwusuarios where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "User ID " & _nrseq & " not found !"
                Return False
            End If
            Dim xsql As String = "update tbusuarios set qtdtentativas = 0, ativo = true"
            If _email <> "" Then
                xsql &= ", email = '" & tratatexto(_email) & "'"
            End If
            If _senha <> "" Then
                xsql &= ", senha = '" & tratatexto(_senha) & "'"
            End If
            If _permissao <> "" Then
                xsql &= ", permissao = '" & _permissao & "'"
            End If
            If _imagemperfil <> "" Then
                xsql &= ", imagemperfil = '" & _imagemperfil & "'"
            End If
            If _usuario <> "" Then
                xsql &= ", usuario ='" & _usuario & "'"
            End If


            tb1 = tab1.IncluirAlterarDados(xsql & " where nrseq = " & _nrseq)
            If _email <> _emailantigo Then
                tb1 = tab1.IncluirAlterarDados("update tbcolaboradores set email = '" & _email & "' where nrsequsuario = " & _nrseq)
            End If
            Return True
        Catch exuser As Exception
            _mensagemerro = exuser.Message
            Return False
        End Try

    End Function

    Public Function carregarusuario(Optional poremail As Boolean = False) As Boolean
        Try

            If poremail Then
                If Not _email.Contains("@") Then
                    tb1 = tab1.conectar("select * from vwusuarios where matricula = '" & _email & "' and ativo = true")
                Else
                    tb1 = tab1.conectar("select * from vwusuarios where email = '" & _email & "' and ativo = true")
                End If

                ' tb1 = tab1.conectar("select * from vwusuarios where email = '" & _email & "' and ativo = true")
                If tb1.Rows.Count = 0 Then
                    _mensagemerro = "O e-mail " & _email & " não existe !"
                    Return False
                End If
            Else

                tb1 = tab1.conectar("select * from vwusuarios where usuario = '" & _usuario & "'")
                If tb1.Rows.Count = 0 Then
                    _mensagemerro = "User ID " & _nrseq & " not found !"
                    Return False
                End If
            End If

            With tb1
                _usercad = .Rows(0)("usercad").ToString
                _dtcad = tratadata(.Rows(0)("dtcad").ToString, gravardata.recuperar)
                _ativo = logico(.Rows(0)("ativo").ToString)
                _usuario = .Rows(0)("usuario").ToString
                _senha = .Rows(0)("senha").ToString
                _permissao = .Rows(0)("permissao").ToString
                _imagemperfil = .Rows(0)("imagemperfil").ToString
                _email = .Rows(0)("email").ToString
                _qtdtentativas = .Rows(0)("qtdtentativas").ToString
                _master = .Rows(0)("master").ToString
                _smartcodesolucoes = .Rows(0)("smartcodesolucoes").ToString
                _usuariosistema = .Rows(0)("usuariosistema").ToString
                _forcarlogin = .Rows(0)("forcarlogin").ToString
                _suspenso = .Rows(0)("suspenso").ToString
                _alterado = logico(.Rows(0)("alterado").ToString)
                '_nomecolaborador = .Rows(0)("nomecolaborador").ToString
                '_gestor = logico(.Rows(0)("gestor").ToString)

                'Dim xcolabora As New clscolaboradores("usuario")

                'xcolabora.Nrsequsuario = _nrseq
                'xcolabora.Procurarpor = "nrsequsuario"

                'xcolabora.carregar()

                '_colaboradores.Add(xcolabora)


            End With

            _table = tb1
            _contador = tb1.Rows.Count
            Return True
        Catch exproc As Exception
            _mensagemerro = exproc.Message
            Return False
        End Try


    End Function

    Public Function procurausuario(Optional poremail As Boolean = False) As Boolean
        Try

            If poremail Then
                If Not _email.Contains("@") Then
                    tb1 = tab1.conectar("select * from vwusuarios where matricula = '" & _email & "' and ativo = true")
                Else
                    tb1 = tab1.conectar("select * from vwusuarios where email = '" & _email & "' and ativo = true")
                End If

                ' tb1 = tab1.conectar("select * from vwusuarios where email = '" & _email & "' and ativo = true")
                If tb1.Rows.Count = 0 Then
                    _mensagemerro = "O e-mail " & _email & " não existe !"
                    Return False
                End If
            Else
                If _nrseq = 0 Then
                    _mensagemerro = "Enter a valid user ID !"
                    Return False
                End If

                tb1 = tab1.conectar("select * from vwusuarios where nrseq = " & _nrseq)
                If tb1.Rows.Count = 0 Then
                    _mensagemerro = "User ID " & _nrseq & " not found !"
                    Return False
                End If
            End If

            With tb1
                _usercad = .Rows(0)("usercad").ToString
                _dtcad = tratadata(.Rows(0)("dtcad").ToString, gravardata.recuperar)
                _ativo = logico(.Rows(0)("ativo").ToString)
                _usuario = .Rows(0)("usuario").ToString
                _senha = .Rows(0)("senha").ToString
                _permissao = .Rows(0)("permissao").ToString
                _imagemperfil = .Rows(0)("imagemperfil").ToString
                _email = .Rows(0)("email").ToString
                _qtdtentativas = .Rows(0)("qtdtentativas").ToString
                _master = .Rows(0)("master").ToString
                _smartcodesolucoes = .Rows(0)("smartcodesolucoes").ToString
                _usuariosistema = .Rows(0)("usuariosistema").ToString
                _forcarlogin = .Rows(0)("forcarlogin").ToString
                _suspenso = .Rows(0)("suspenso").ToString
                _alterado = logico(.Rows(0)("alterado").ToString)
                '_nomecolaborador = .Rows(0)("nomecolaborador").ToString
                '_gestor = logico(.Rows(0)("gestor").ToString)

                'Dim xcolabora As New clscolaboradores("usuario")

                'xcolabora.Nrsequsuario = _nrseq
                'xcolabora.Procurarpor = "nrsequsuario"

                'xcolabora.carregar()

                '_colaboradores.Add(xcolabora)


            End With


            Return True
        Catch exproc As Exception
            _mensagemerro = exproc.Message
            Return False
        End Try


    End Function
    Private Function excluirusuario() As Boolean
        Try
            versessao()
            If _nrseq = 0 Then
                _mensagemerro = "Enter a valid user ID !"
                Return False
            End If

            tb1 = tab1.conectar("select * from vwusuarios where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "User ID " & _nrseq & " not found !"
                Return False
            End If
            If tb1.Rows(0)("ativo").ToString Then
                tb1 = tab1.IncluirAlterarDados("update tbusuarios set ativo = false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "' where nrseq = " & _nrseq)
                gravalog("User " & _usuario & " was deleted by " & _usercad & " at " & hoje() & " / " & hora(), "Ação")
            Else
                tb1 = tab1.IncluirAlterarDados("update tbusuarios set ativo = true where nrseq = " & _nrseq)
                gravalog("User " & _usuario & " was recovered by " & _usercad & " at " & hoje() & " / " & hora(), "Ação")
            End If


            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function

    Private Function suspenderusuario() As Boolean
        Try
            versessao()
            If _nrseq = 0 Then
                _mensagemerro = "Enter a valid user ID !"
                Return False
            End If

            tb1 = tab1.conectar("select * from vwusuarios where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "User ID " & _nrseq & " not found !"
                Return False
            End If
            If tb1.Rows(0)("ativo").ToString = "True" Then
                tb1 = tab1.IncluirAlterarDados("update tbusuarios set ativo = false, dtexclui = '" & hoje() & "', userexclui = '" & HttpContext.Current.Session("usuario") & "' where nrseq = " & _nrseq)
                gravalog("User " & _usuario & " was deleted by " & _usercad & " at " & hoje() & " / " & hora(), "Ação")
            Else
                tb1 = tab1.IncluirAlterarDados("update tbusuarios set ativo = true where nrseq = " & _nrseq)
                gravalog("User " & _usuario & " was recovered by " & _usercad & " at " & hoje() & " / " & hora(), "Ação")
            End If


            Return True
        Catch ex As Exception
            Return False
        End Try


    End Function
    Public Function alterarloginesenha() As Boolean
        Try
            Dim _usuarioantigo As String = ""
            If _nrseq = 0 Then
                _mensagemerro = "Selecione um usuário válido !"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbusuarios where usuario = '" & _usuario & "'")
            If tb1.Rows.Count > 0 Then
                _mensagemerro = "This user already exists in the system!"
                Return False
            End If
            tb1 = tab1.conectar("select * from tbusuarios where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Please, select a valid user !"
                Return False
            End If
            _usuarioantigo = tb1.Rows(0)("usuario").ToString
            Dim xsql As String = ""
            If _usuario <> "" Then
                xsql &= ", usuario = '" & _usuario & "'"
            End If
            If _senha <> "" Then
                xsql &= ", senha = '" & _senha & "'"
            End If
            tb1 = tab1.IncluirAlterarDados("update tbusuarios set ativo = true, alterado = true, dtalterado = '" & hoje() & "', useralterado = '" & _usercad & "', hralterado = '" & hora() & "' " & xsql & " where nrseq = " & _nrseq)
            tb1 = tab1.IncluirAlterarDados("update tblicitacoes set usercad = '" & _usuario & "' where usercad = '" & _usuarioantigo & "'")
            tb1 = tab1.IncluirAlterarDados("update tblicitacoes set contaceva = '" & _usuario & "' where contaceva = '" & _usuarioantigo & "'")
            tb1 = tab1.IncluirAlterarDados("update tblicitacoes set useraltera = '" & _usuario & "' where useraltera = '" & _usuarioantigo & "'")
            tb1 = tab1.IncluirAlterarDados("update tblicitacoes set userexclui = '" & _usuario & "' where userexclui = '" & _usuarioantigo & "'")
            Return True
        Catch exalterarlogin As Exception
            _mensagemerro = exalterarlogin.Message
            Return False
        End Try
    End Function
    Public Function EnviarSenha() As Boolean


        Try

            If Not procurausuario(True) Then
                Return False
            End If


            Dim texto As String = ""
            Dim xtexto As New clsconfig_padroes
            If _email.Contains("@") Then
                xtexto.Nome = "Esqueci minha senha"
                xtexto.Destinatarios.Add(_email)
                xtexto.Destinatarios.Add("alertas@smartcodesolucoes.com.br")

            Else
                xtexto.Nome = "Solicitação de senhas ADM"
                xtexto.Destinatarios.Add("alertas@smartcodesolucoes.com.br")
                tbx = tabx.conectar("select * from vwusuarios where master = true order by nrseq")
                For y As Integer = 0 To tbx.Rows.Count - 1
                    xtexto.Ocultos.Add(tbx.Rows(y)("email").ToString)
                Next

            End If

            If Not xtexto.carregar Then
                _mensagemerro = "Nenhuma mensagem padrão localizada! E-mail não enviado!"
                Return False
            Else
                xtexto.Assuntoemail = xtexto.Assunto
                xtexto.Padraoemail = ""
                xtexto.Textoemail = xtexto.Texto.Replace("{usuario}", _usuario).Replace("{senha}", _senha)
                Dim tbenviar As New Data.DataTable

                xtexto.enviaremail()
            End If



            '   email.AdicionaMensagem = texto.Replace("{usuario}", _usuario).Replace("{senha}", _senha) '"Bid " & lblnumerolicitacao.Text & " salvo ccom sucesso pelo usuário " & Session("usuario") & " !"

            ' email.AdicionaAssunto = "URGENTE: Login e senha do sistema PAD"
            ' email.configpadrao()
            'If email.EnviarEmail() Then
            '    _mensagemerro = "O e-mail foi enviado com sucesso !"
            '    Return True
            'Else
            '    _mensagemerro = "O e-mail não foi enviado !"
            '    Return False
            'End If
            _mensagemerro = "Por favor, verifique sua caixa de e-mails!"
            Return True
        Catch exEMAIL As Exception
            _mensagemerro = exEMAIL.Message
            Return False
        End Try

    End Function

    Public Function validarsenhas() As Boolean
        Try

            tb1 = tab1.conectar("select * from vwusuarios where ativo = true order by usuario")

            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim wcsenha As String = tb1.Rows(0)("senha").ToString
                Dim condchrespeciais As Boolean = False
                Dim condletramaius As Boolean = False
                Dim condnumeros As Boolean = False
                Dim condletrasminus As Boolean = False
                For y As Integer = 33 To 47
                    If wcsenha.Contains(Chr(x)) Then
                        condchrespeciais = True
                    End If
                Next
                For y As Integer = 58 To 64
                    If wcsenha.Contains(Chr(x)) Then
                        condchrespeciais = True
                    End If
                Next
                For y As Integer = 65 To 90
                    If wcsenha.Contains(Chr(x)) Then
                        condletramaius = True
                    End If
                Next
                For y As Integer = 48 To 57
                    If wcsenha.Contains(Chr(x)) Then
                        condnumeros = True
                    End If
                Next
                For y As Integer = 48 To 57
                    If wcsenha.Contains(Chr(x)) Then
                        condletrasminus = True
                    End If
                Next

                If condchrespeciais AndAlso condletramaius AndAlso condletrasminus AndAlso condnumeros Then

                Else
                    tbx = tabx.IncluirAlterarDados("update tbusuarios set alterado = false where nrseq = " & tb1.Rows(x)("nrseq").ToString)
                    Dim xtexto As New clsconfig_padroes

                    xtexto.Nome = "Validação de senhas"
                    If xtexto.carregar Then
                        xtexto.Destinatarios.Add(_email)

                        If xtexto.enviaremail Then

                        Else

                        End If

                    End If

                End If
                Threading.Thread.Sleep(100)
            Next




            Return True
        Catch exsenhas As Exception
            Return False
        End Try
    End Function

End Class

﻿
Partial Class restrito_relatorioguias
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Private Sub restrito_relatorioguias_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        tb1 = tab1.conectar("select * from tbconvenios where ativo = True order by nome")
        cbolaboratorio.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbolaboratorio.Items.Add(tb1.Rows(x)("nome").ToString)
        Next
    End Sub
    Private Sub carregar()
        Dim sql As String = "SELECT nomeempresa, sum(valorcusto) as valortotalcusto, sum(valortotal) as valortotalpago, ativo, baixado FROM dbparceriajf.vwagenda_exames where  exameativo = true and ativo = true "
        If IsDate(txtdtinicial.Text) Then
            sql &= " and data >= '" & txtdtinicial.Text & "'"
        End If
        If IsDate(txtdtfinal.Text) Then
            sql &= " and data <= '" & txtdtfinal.Text & "'"
        End If
        If chkexibirapenasbaixados.Checked Then
            sql &= " and baixado = true"
        End If
        If cbolaboratorio.Text <> "" Then
            sql &= " and nomeempresa = '" & cbolaboratorio.Text & "'"
        End If
        sql &= " group by nomeempresa,  ativo, baixado, exameativo order by nomeempresa"
        tb1 = tab1.conectar(sql)
        Session("total") = 0
        Session("totalcusto") = 0
        grade.DataSource = tb1
        grade.DataBind()
        txttotcusto.Text = FormatCurrency(Session("totalcusto"))
        txttotpago.Text = FormatCurrency(Session("total"))
        txttotsaldo.Text = FormatCurrency(Session("total") - Session("totalcusto"))

    End Sub

    Private Sub btngerar_Click(sender As Object, e As EventArgs) Handles btngerar.Click
        carregar()
    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        ' If e.Row.RowIndex < 0 Then Exit Sub
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim teste As New TableCell
            teste.Text = "teste"
            e.Row.Cells.Add(teste)
            e.Row.Cells(0).Text = "Totais"
            e.Row.Cells(2).Text = FormatCurrency(Session("total"))
        End If
        If e.Row.RowIndex < 0 Then

            Exit Sub
        End If
        Dim lblsaldo As Label = e.Row.FindControl("lblsaldo")
        Dim gradedth As GridView = e.Row.FindControl("gradedth")
        gradedth.Visible = False
        If e.Row.Cells(2).Text = "" Then e.Row.Cells(2).Text = 0
        If e.Row.Cells(1).Text = "" Then e.Row.Cells(1).Text = 0

        lblsaldo.Text = FormatCurrency(e.Row.Cells(2).Text - e.Row.Cells(1).Text)
        e.Row.Cells(2).Text = FormatCurrency(e.Row.Cells(2).Text)
        e.Row.Cells(1).Text = FormatCurrency(e.Row.Cells(1).Text)
        Session("total") += e.Row.Cells(2).Text
        Session("totalcusto") += e.Row.Cells(1).Text
        If chkexibirtodos.Checked Then
            Dim tb2 As New Data.DataTable
            Dim tab2 As New clsBanco
            Dim xsql1 As String = "select nomeempresa, sum(valorcusto) as valortotalcusto, sum(valortotal) as valortotalpago, ativo, baixado, descricao from vwagenda_exames where ativo = true and exameativo = true"
            If IsDate(txtdtinicial.Text) Then
                xsql1 &= " and data >= '" & txtdtinicial.Text & "'"
            End If
            If IsDate(txtdtfinal.Text) Then
                xsql1 &= " and data <= '" & txtdtfinal.Text & "'"
            End If
            If chkexibirapenasbaixados.Checked Then
                xsql1 &= " and baixado = true"
            End If
            xsql1 &= " and nomeempresa = '" & Server.HtmlDecode(e.Row.Cells(0).Text) & "'"
            xsql1 &= " group by nomeempresa,  ativo, baixado, descricao, exameativo order by descricao"
            tb2 = tab2.conectar(xsql1)
            gradedth.DataSource = tb2
            gradedth.DataBind()
            gradedth.Visible = True
        End If
    End Sub
End Class

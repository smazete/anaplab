﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="enviarresultado.aspx.vb" Inherits="enviarresultado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="panel panel-warning" style="margin-left: 15px;">

            <div class="panel-heading">
                <center>Envio de e-mails</center>
            </div>
            <div class="panel-body">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <Triggers >
                        <asp:PostBackTrigger ControlID="btnanexar" />
                    </Triggers>
                                <ContentTemplate >
                                           <div class="row" style="display:none" >
                                               <div class="col-lg-4">
                                                   </div>
                            <div class="col-lg-4" >
                                <div class="form-group">
                                    <center>
                                    <asp:Button ID="btncarregar" runat="server" CssClass="btn btn-danger " text ="Gerar e-mail"/>
                                    </center>
                                    </div>
                                </div>
                                               </div>
                        <div class="row" style="display:none;">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="Modelo de contrato"></asp:Label>
                                    <br />
                                    <asp:DropDownList ID="cbocontratos" runat="server" class="form-control"></asp:DropDownList>

                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btnAbrirmodelo" runat="server" Text="Abrir Selecionado" Height="33px" Width="165px" CssClass="btn btn-warning" />
                                </div>
                            </div>

                        </div>
                      
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="E-mail destinatário"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtemail" runat="server" placehorder="Digite os e-mails separados por ponto-e-virgula" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="Cópia oculta"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtcco" runat="server" class="form-control" placehorder="Digite os e-mails separados por ponto-e-virgula"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Assunto do e-mail"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtassunto" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="Label5" runat="server" Text="Anexar Arquivo"></asp:Label>
                                        <br />
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                    </div>
                                    <div class="row">
                                        <asp:GridView ID="Grade" runat="server" AutoGenerateColumns="False" CellPadding="4" Width="100%" ShowHeaderWhenEmpty="True" EmptyDataText="Nenhum arquivo encontrado !" CssClass="table table-hover" GridLines="None">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sel">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="sel" runat="server" />
                                                        <asp:HiddenField ID="hdnrseq" runat="server" Value ='<%# Bind("nrseq") %>' />
                                                        <br />
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:BoundField DataField="arquivo" HeaderText="Arquivo"></asp:BoundField>



                                                <asp:ButtonField ButtonType="Image" runat="server" CommandName="Excluir" ControlStyle-CssClass="f" ImageUrl="~\img\imgnao.png">
                                                         <ControlStyle Width="27px" Height="27px"></ControlStyle>
                                    <ItemStyle  HorizontalAlign="Center"  />


                                                </asp:ButtonField>

                                            </Columns>

                                            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                            <HeaderStyle CssClass="grade th" BackColor="black" Font-Bold="True" ForeColor="yellow" />
                                            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                            <RowStyle />
                                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" />
                                            <SortedAscendingCellStyle BackColor="#FEFCEB" />
                                            <SortedAscendingHeaderStyle BackColor="#AF0101" />
                                            <SortedDescendingCellStyle BackColor="#F6F0C0" />
                                            <SortedDescendingHeaderStyle BackColor="#7E0000" />
                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <asp:Button ID="btnanexar" runat="server" Text="Anexar" CssClass="btn btn-warning" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="Corpo do e-mail"></asp:Label>
                                    <br />
                                   <%-- <iframe src=""--%>

                                    <iframe id="framemensagem" height="420px" runat="server" class="col-lg-12"></iframe>
                                    <%--<asp:TextBox ID="txtcorpo" runat="server" Height="420px" class="form-control" AutoPostBack="True"></asp:TextBox>
                                   <ajaxToolkit:HtmlEditorExtender ID="TextBox1_HtmlEditorExtender" runat="server" BehaviorID="TextBox1_HtmlEditorExtender" TargetControlID="txtcorpo" EnableSanitization="false">
                            </ajaxToolkit:HtmlEditorExtender>--%>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                  <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                    <ContentTemplate>
                                    <asp:Button ID="btnEnviar" runat="server" Text="Enviar" CssClass="btn btn-warning" />
                                    <div id="alerta1" style="display: none;">


                                        <div class="alert alert-danger alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                            <strong>Atenção:</strong>
                                            <asp:Label ID="lblerro" runat="server" Text=""></asp:Label>

                                        </div>
                                    </div>
                     
                        </ContentTemplate> 
                                      </asp:UpdatePanel> 

                                </div>

                            </div>
                        </div>
                                                 
                   
</ContentTemplate> 
                    </asp:UpdatePanel> 
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>

                        <div style="text-align: center">
                            <asp:Image ID="Image2" ImageUrl="loading.gif" runat="server" Height="41px" Width="47px" />
                            <br />
                            Enviando !!!


                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
        </div>

</asp:Content>


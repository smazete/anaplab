﻿Imports clsSmart

Partial Class resultados
    Inherits System.Web.UI.Page

    Private Sub resultados_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub

        Dim xponto As New clsResultados

        grade.DataSource = xponto.Grade
        grade.DataBind()

        Dim xpontof As New clsresultados_finais

        gradefinal.DataSource = xpontof.Grade
        gradefinal.DataBind()

    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub btnaddresult_Click(sender As Object, e As EventArgs) Handles btnaddresult.Click
        Dim xponto As New clsResultados

        xponto.Peso = txtpontua.Text
        xponto.Descricao = txtdescricao.Text
        xponto.Usarem = cbousarem.SelectedItem.Text

        If Not xponto.adicionar Then
            sm("swal.fire({title: 'Atenção!',text: '" & xponto.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

            Exit Sub
        End If

        grade.DataSource = xponto.Grade
        grade.DataBind()
        txtpontua.Text = ""
        txtdescricao.Text = ""

    End Sub

    Private Sub btnaddfinal_Click(sender As Object, e As EventArgs) Handles btnaddfinal.Click
        Dim xponto As New clsresultados_finais
        xponto.Nrseq = sonumeros(hdnrseqfinal.Value)
        xponto.Pesoinicial = txtpontualcaoini.Text
        xponto.Pesofinal = txtpontualcaofin.Text
        xponto.Descricao = txtdescricaofinal.Text
        xponto.Usarem = cbousaremfinal.SelectedItem.Text
        xponto.Metageral = validanumero(txtmetas.Text)

        If Not xponto.adicionar Then
            sm("swal.fire({title: 'Atenção!',text: '" & xponto.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

            Exit Sub
        End If

        gradefinal.DataSource = xponto.Grade
        gradefinal.DataBind()
        txtmetas.Text = ""
        txtpontualcaoini.Text = ""
        txtpontualcaofin.Text = ""
        txtdescricaofinal.Text = ""
    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblitemgrade As Label = e.Row.FindControl("lblitemgrade")
            lblitemgrade.Text = e.Row.RowIndex + 1
        End If
    End Sub

    Private Sub grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        ' Dim xempresa As New ClsCargos
        Dim row As GridViewRow = grade.Rows(index)
        Dim hdnrseqgrade As HiddenField = row.FindControl("hdnrseqgrade")

        Dim xpontos As New clsResultados
        xpontos.Nrseq = hdnrseqgrade.Value

        If e.CommandName.ToLower = "editar" Then
            If Not xpontos.carregar() Then
                sm("swal.fire({title: 'Atenção!',text: '" & xpontos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")



                Exit Sub

            End If

            txtdescricao.Text = xpontos.Descricao
            txttextolegenda.Text = xpontos.Legenda
            txttitulolegenda.Text = xpontos.Titulolegenda
            txtpontua.Text = xpontos.Peso
            'cbousarem.Text = xpontos.


        Else
            If Not xpontos.remover() Then
                sm("swal.fire({title: 'Atenção!',text: '" & xpontos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

                Exit Sub

            End If
        End If

        grade.DataSource = xpontos.Grade
        grade.DataBind()


    End Sub

    Private Sub gradefinal_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradefinal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblitemgrade As Label = e.Row.FindControl("lblitemgrade")
            lblitemgrade.Text = e.Row.RowIndex + 1
        End If
    End Sub

    Private Sub gradefinal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradefinal.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim xempresa As New ClsCargos
        Dim row As GridViewRow = gradefinal.Rows(index)
        Dim hdnrseqgrade As HiddenField = row.FindControl("hdnrseqgrade")

        Select Case e.CommandName.ToLower
            Case Is = "excluir"
                Dim xpontos As New clsresultados_finais
                xpontos.Nrseq = hdnrseqgrade.Value

                If Not xpontos.remover() Then
                    sm("swal.fire({title: 'Atenção!',text: '" & xpontos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

                    Exit Sub

                End If
                gradefinal.DataSource = xpontos.Grade
                gradefinal.DataBind()
            Case Is = "editar"
                Dim xpontos As New clsresultados_finais
                xpontos.Nrseq = hdnrseqgrade.Value
                hdnrseqfinal.Value = hdnrseqgrade.Value
                If Not xpontos.carregar() Then
                    sm("swal.fire({title: 'Atenção!',text: '" & xpontos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

                    Exit Sub

                End If
                txtdescricaofinal.Text = xpontos.Descricao
                txtpontualcaofin.Text = xpontos.Pesofinal
                txtpontualcaoini.Text = xpontos.Pesoinicial
                cbousaremfinal.Text = xpontos.Usarem
                txtmetas.Text = xpontos.Metageral

        End Select



    End Sub
End Class

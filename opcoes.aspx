﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="opcoes.aspx.vb" Inherits="opcoes" MasterPageFile="~/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <div class="box box-primary">
        <div class="box-header">
            Configurações de Uso  
        </div>
        <div class="box-body">
    
                        <div class="row" id="divdadosfinanceiro">
                            <br />
                            <div class="col-lg-12">
                                <center>
                                       <asp:LinkButton runat="server" Text="Alterar" CssClass="btn btn-primary" ID="btnalteradadosfinanceiro"></asp:LinkButton>
                                       <asp:LinkButton runat="server" Text="Salvar" CssClass="btn btn-primary" ID="btnsalvardadosfinanceiro"></asp:LinkButton>
                                </center>
                            </div>
                            <br />
                            <div class="col-lg-4">
                                <div class="panel panel-primary  ">
                                    <div class="panel-header bg-blue"> Datas Habilitadas <span class="badge bg-blue">   <span class="badge bg-green" runat="server" id="divsim" visible="false"><asp:Label Text="SIM" runat="server"/></span><span class="badge bg-red" runat="server" id="divnao"  ><asp:Label Text="NÃO" runat="server"/></span> </span>   </div>
                                    <div class="panel-body bg-light-blue">
                                        <div class="col-lg-3">
                                            <asp:Label runat="server" Text="Dia Vencimento "></asp:Label>
                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" MaxLength="2" AutoPostBack="True" Text="10"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-3">
                                            <asp:Label runat="server" Text="Bloquear Apartir de "> </asp:Label>
                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" MaxLength="2" AutoPostBack="True" Text="31"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-6">
                                            <asp:Label runat="server" Text="Data Ultima Alteração"></asp:Label>
                                            <asp:Label runat="server">  Alterado por  <asp:Label Text="Gilvan" runat="server" /></asp:Label>
                                            <asp:TextBox ID="TextBox3" Enabled="false" type="date" runat="server" onkeyup="formataPlanoContas(this,event);" CssClass="form-control"  AutoPostBack="True"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-2">
                                            <asp:CheckBox Text="Gerar Lote" runat="server" />
                                        </div>


                                    </div>
                                </div>
                            </div>




                            <div class="col-lg-3">
                                <asp:Label runat="server" Text="Descricao"></asp:Label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtprocurar"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:CheckBox runat="server" Text="Inativo" CssClass="checkbox-inline" Checked="false" ID="chkexibirinativos" />
                            </div>
                            <div class="col-lg-2">
                                <asp:LinkButton runat="server" Text="Procurar" CssClass="btn btn-primary" ID="btnprocurar"></asp:LinkButton>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                  <asp:Label Text="Acessos dos Associandos" runat="server" />
                    <asp:CheckBoxList runat="server">
                        <asp:ListItem Text="Mensalidaes" />
                        <asp:ListItem Text="Caixa Postal " />
                        <asp:ListItem Text="Contato" />
                        <asp:ListItem Text="Editar Perfil" />
                        <asp:ListItem Text="Perfil" />
                        <asp:ListItem Text="Documentos" />
                        <asp:ListItem Text="Ações"   />
                    </asp:CheckBoxList>
<div class="col-lg-3">
                        <asp:Label runat="server" Text="email de envio"> </asp:Label>
                        <asp:TextBox ID="txtemailenvio" runat="server" CssClass="form-control" MaxLength="2" AutoPostBack="True"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <asp:Label runat="server" Text="senha email de envio"> </asp:Label>
                        <asp:TextBox ID="txtsenhaemailenvio" runat="server" CssClass="form-control" MaxLength="2" AutoPostBack="True"></asp:TextBox>
                    </div>

                    <div class="col-lg-3">
                        <asp:Label runat="server" Text="email recebimento contato"> </asp:Label>
                        <asp:TextBox ID="txtemailenviocontato" runat="server" CssClass="form-control" MaxLength="2" AutoPostBack="True"></asp:TextBox>
                    </div>
                </div>
  




    </div>

</asp:Content>

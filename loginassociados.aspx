﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="loginassociados.aspx.vb" MasterPageFile="~/MasterPage2.master"  Inherits="loginassociados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
		
@import url('https://fonts.googleapis.com/css?family=Numans');

html,body{
background-image: url('img/couple-elderly-man-old-34761.jpg');
background-size: cover;
background-repeat: no-repeat;
height: 100%;
font-family: 'Numans', sans-serif;
}

.container{
height: 100%;
align-content: center;
}

.card{
height: 370px;
margin-top: auto;
margin-bottom: auto;
width: 400px;
background-color: rgba(0,0,0,0.5) !important;
}

.social_icon span{
font-size: 60px;
margin-left: 10px;
color: #FFC312;
}

.social_icon span:hover{
color: white;
cursor: pointer;
}

.card-header h3{
color: white;
}

.social_icon{
position: absolute;
right: 20px;
top: -45px;
}

.input-group-prepend span{
width: 50px;
background-color: #FFC312;
color: black;
border:0 !important;
}

input:focus{
outline: 0 0 0 0  !important;
box-shadow: 0 0 0 0 !important;

}

.remember{
color: white;
}

.remember input
{
width: 20px;
height: 20px;
margin-left: 15px;
margin-right: 5px;
}

.login_btn{
color: black;
background-color: #FFC312;
width: 100px;
}

.login_btn:hover{
color: black;
background-color: white;
}

.links{
color: white;
}

.links a{
margin-left: 4px;
}
	@media screen and (max-width: 600px) {
		.card {
			width: auto;
			padding:15px 15px;
		}
	}
    </style>

	  <%-- sweetalert2 monitorando qual vou usar  --%>    
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
    <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="/dist/js/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>

    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">    
<div class="container">
	<br />
	<br />
	<div class="d-flex justify-content-center h-100">
		<div class="card" style="padding:3rem;">
			<div class="card-header">
				<h3> Acesso Associados</h3>
				<div class="d-flex justify-content-end social_icon">
					<span><i class="fab fa-facebook-square"></i></span>
					<span><i class="fab fa-google-plus-square"></i></span>
					<span><i class="fab fa-twitter-square"></i></span>
				</div>
			</div>
			<div class="card-body">
				<form>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<asp:TextBox ID="txtmatricula" runat="server" type="text" class="form-control" placeholder="Matricula"/>
						<p style="color: chartreuse">MATRÍCULA COM DV</p>
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<asp:TextBox  ID="txtsenha" runat="server" type="password" class="form-control" placeholder="Senha"/>
					</div>
					<div class="row align-items-center remember">
						<input type="checkbox">Lembrar-me
					</div>
					<div class="form-group">
						<asp:LinkButton ID="btnentrar" Text="Entrar" CssClass="btn btn-info" runat="server" />
					</div>
				</form>
			</div>
			<div class="card-footer">
<%--				<div class="d-flex justify-content-center links">					
Não possui uma conta?<a href="novosassociados.aspx">Associe-se</a>
				</div>--%>
				<div class="d-flex justify-content-center">
					<a href="esqueciminhasenha.aspx">Esqueceu sua senha?</a>
				</div>
			</div>
		</div>
	</div>
	
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
</div>
    </asp:Content>

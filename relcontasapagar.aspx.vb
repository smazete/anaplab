﻿Imports clsSmart
Partial Class restrito_relcontasapagar
    Inherits System.Web.UI.Page
    Dim tbplanos As New Data.DataTable
    Dim tabplanos As New clsBanco
    Private Sub restrito_relcontasapagar_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub

        Dim resultado As String = valida(Session("usuario"), mARQUIVO(Request.Path, True), Session("usuariomaster")).ToLower
        Select Case resultado
            Case Is = "login"
                Session("urlretorno") = Request.Path
                Response.Redirect("login.aspx")
                Exit Sub
            Case Is = "erro"
                Response.Redirect("ops2.aspx")
                Exit Sub
        End Select
        Session("carga") = "não"
        tbplanos = tabplanos.conectar("select * from tbdocumentos where ativo = true order by descricao")
        cboformapagto.Items.Clear()
        cboformapagto.Items.Add("")
        For x As Integer = 0 To tbplanos.Rows.Count - 1
            cboformapagto.Items.Add(tbplanos.Rows(x)("descricao").ToString)
        Next
        tbplanos = tabplanos.conectar("select * from tbplanocontas where ativo = true and codigo like '" & zeros(Session("idempresaemuso"), 2) & "%' order by descricao")
        cboplano.Items.Add("")
        For x As Integer = 0 To tbplanos.Rows.Count - 1
            cboplano.Items.Add(tbplanos.Rows(x)("descricao").ToString)
        Next


    End Sub

    Private Sub btnProcurar_Click(sender As Object, e As EventArgs) Handles btnProcurar.Click
        Session("totalbaixados") = 0
        Session("qtdbaixados") = 0
        Session("totalabertos") = 0
        Session("qtdabertos") = 0
        Session("totalddeb") = 0
        Session("qtddeb") = 0
        Session("totalcred") = 0
        Session("qtdcred") = 0
        Session("total") = 0
        Session("qtd") = 0
        Dim tbgrade As New Data.DataTable
        Dim tabgrade As New clsBanco

        Session("sql") = " 1=1 and tbplanocontas.codigo like '" & zeros(Session("idempresaemuso"), 2) & "%'"
            Session("campo") = "dtvencimento"
            If optdata_baixa.Checked Then
                Session("campo") = "dtbaixa"
            End If
            If optdata_execucao.Checked Then
                Session("campo") = "dtexecucao"
            End If
            If optdata_cadastro.Checked Then
                Session("campo") = "dtcad"
            End If

            If IsDate(txtdtinicial.Text) Then
                Session("sql") &= " and " & Session("campo") & " >= '" & txtdtinicial.Text & "'"
            End If
            If IsDate(txtdtfinal.Text) Then
                Session("sql") &= " and " & Session("campo") & " <= '" & txtdtfinal.Text & "'"
            End If
            If cboformapagto.Text <> "" Then
                Session("sql") &= " and tbdocumentos.descricao = '" & cboformapagto.Text & "'"
            End If
            If cbooperacao.Text <> "" Then
                Session("sql") &= " and operacao = '" & cbooperacao.Text & "'"
            End If
            If cboplano.Text <> "" Then
                Session("sql") &= " and tbplanocontas.descricao = '" & cboplano.Text & "'"
            End If
            If txtdescricao.Text <> "" Then
                Session("sql") &= " and descricao like '%" & txtdescricao.Text & "%'"
            End If
            If opttipo_abertas.Checked Then
                Session("sql") &= " and (baixa is null or baixa = '0')"
            End If
            If opttipo_baixadas.Checked Then
                Session("sql") &= " and baixa = '1'"
            End If
            If Not chkExibirExcluidos.Checked Then
                Session("sql") &= " and tbcontas.ativo = true "
            End If
        If optconci_nao.Checked Then
            Session("sql") &= " and (tbcontas.conciliado = false or tbcontas.conciliado is null)"
        End If
        If optconci_sim.Checked Then
            Session("sql") &= " and (tbcontas.conciliado = true)"
        End If
        If optaprov_aprovadas.Checked Then
            Session("sql") &= " and (tbcontas.aprovada = true and tbcontas.solaprovacao = true)"
        End If
        If optaprov_emaprova.Checked Then
            Session("sql") &= " and (tbcontas.aprovada = false and tbcontas.solaprovacao = true)"
        End If
        Session("sql") &= " order by tbcontas.dtvencimento"
        tbgrade = tabgrade.conectar("select tbcontas.*,tbplanocontas.codigo as planodecontas, tbplanocontas.descricao as descricaocontas, tbdocumentos.descricao as descricaodoc, tbdocumentos.compensa, tbdocumentos.taxa from tbcontas inner join tbplanocontas on tbcontas.nrseqplano = tbplanocontas.nrseq inner join tbdocumentos on tbcontas.nrseqdocumento = tbdocumentos.nrseq  where " & Session("sql"))
        If tbgrade.Rows.Count = 0 Then
            Exit Sub
        End If
        Session("sql") = "select tbcontas.*,tbplanocontas.codigo as planodecontas, tbplanocontas.descricao as descricaocontas, tbdocumentos.descricao as descricaodoc, tbdocumentos.compensa, tbdocumentos.taxa from tbcontas inner join tbplanocontas on tbcontas.nrseqplano = tbplanocontas.nrseq inner join tbdocumentos on tbcontas.nrseqdocumento = tbdocumentos.nrseq  where " & Session("sql")
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "imrpimircontas()", "imrpimircontas()", True)
    End Sub
End Class

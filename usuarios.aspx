﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="usuarios.aspx.vb" Inherits="usuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">

        function movertela() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);

            $("html, body").animate({
                scrollTop: 0
            }, 1200);


        }
        function exibiralerta1() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);

            $("html, body").animate({
                scrollTop: 0
            }, 1200);


            $('#alerta1').show();
            $('#alerta1').load();
        }
        function escondeexibiralerta1() {




            $('#alerta1').hide();
            $('#alerta1').hide();
        }
        function exibiralerta2() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);

            $("html, body").animate({
                scrollTop: 0
            }, 1200);


            $('#alerta2').show();
            $('#alerta2').load();
        }
        function escondeexibiralerta2() {




            $('#alerta2').hide();
            $('#alerta2').hide();
        }
    </script>

    <div class="row" style="margin-left: 1rem; margin-top: 1rem; margin-right: 1rem;">
        <div class="box box-primary " style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">

            <div class="box-header">
                <b>
                    <asp:Label ID="lblnrlote" runat="server" Text="Novo usuário"></asp:Label></b>
                <asp:HiddenField ID="hdopera" runat="server" />
            </div>
            <div class="box-body">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div id="alerta1" style="display: none;">

                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <strong>Atenção: Verifique os itens abaixo !</strong>
                                <asp:GridView ID="gradeerro" BackColor="Transparent" ForeColor="white" runat="server" AutoGenerateColumns="False" BorderColor="White" BorderStyle="none" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None">
                                    <Columns>


                                        <asp:BoundField DataField="erro">
                                            <ItemStyle BackColor="Transparent" ForeColor="black" />
                                        </asp:BoundField>


                                    </Columns>


                                </asp:GridView>

                            </div>
                        </div>

                        <div class="row text-align-center">
                            <div class="col-lg-12">
                                <asp:Button runat="server" ID="btnnovo" Text="Novo" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:HiddenField ID="hdnrsequsuario" runat="server" />
                                    <asp:HiddenField ID="hdemailantigo" runat="server" />
                                    <asp:Label ID="Label1" runat="server" Text="Usuário"></asp:Label>
                                    <asp:TextBox ID="txtNome" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="Senha"></asp:Label>
                                    <asp:TextBox ID="txtSenha" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="E-mail"></asp:Label>
                                    <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="Permissão"></asp:Label>
                                    <asp:DropDownList ID="cboPermissao" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>

                        <div class="row text-align-center">

                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-2">

                                <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="btn btn-success " />

                                <div id="alerta2" style="display: none;">
                                </div>

                                
                            </div>
                            <div class="col-lg-2">
                                <asp:Button ID="btncancelar" Enabled="true" runat="server" Text="Cancelar" CssClass="btn btn-danger" />
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>

                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                                    <ProgressTemplate>
                                        <div style="text-align: center">
                                            <asp:Image ID="imgload1" runat="server" Height="24px" ImageUrl="loading.gif" Width="24px" />
                                            <br />

                                            <asp:Label ID="lblmenscarregando" runat="server" Text=" Por favor aguarde! Carregando..."></asp:Label>
                                            <br />

                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>

                        <br />

                        <div class="box box-success ">
                            <div class="box box-header ">
                            </div>
                            <div class="box-body ">
                                <div class="row">
                                    <div class="col-lg-4 ">
                                        Quem você quer procurar ?
                                        <br />
                                        <asp:TextBox ID="txtprocura" runat="server" CssClass="form-control "></asp:TextBox>
                                    </div>
                                    <div class="col-lg-2 ">
                                        <br />
                                        <asp:LinkButton ID="btnprocurar" runat="server" CssClass="btn btn-primary ">Procurar</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 ">
                                        <br />
                                        <asp:CheckBox ID="chkexibirsuspensos" runat="server" Text="Exibir Apenas Suspensos" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="updategrade" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <br />
                                        <asp:GridView ID="Grade" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                            <AlternatingRowStyle BackColor="AntiqueWhite" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                                        <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("nrseq") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Opções">
                                                    <%--  <ItemTemplate >
                                                        <div class="row">
                                                              <div class="bloco-imagens" >
                                                             <asp:LinkButton ID="btnalterargrade" runat="server"><asp:Image runat="server" CommandArgument='<%#Container.DataItemIndex%>' ImageUrl="~/img/alterarverde.png" CommandName="alterar" style="width:3rem ;height:3rem;" /> </asp:LinkButton>
                                                               </div> 
                                                        </div>
                                                        <div class="row">   <br />
                                                             <div class="bloco-imagens" >
                                                              <asp:LinkButton ID="LinkButton1" runat="server"><asp:Image runat="server" CommandArgument='<%#Container.DataItemIndex%>' ImageUrl="~/img/imgnao.png" CommandName="Excluir" style="width:3rem ;height:3rem;" /> </asp:LinkButton>
                                                        </div></div> 
                                                        <div class="row">   <br />
                                                             <div class="bloco-imagens" >
                                                             <asp:LinkButton ID="LinkButton2" runat="server"><asp:Image runat="server" CommandArgument='<%#Container.DataItemIndex%>' ImageUrl="~/img/imgokboll.png" CommandName="Reativar" style="width:3rem ;height:3rem;" /> </asp:LinkButton>
                                                                 </div> 
                                                        </div>
                                                        <div class="row">
                                                            <br />
                                                             <div class="bloco-imagens" >
                                                              <asp:LinkButton ID="LinkButton3" runat="server"><asp:Image runat="server" CommandArgument='<%#Container.DataItemIndex%>' ImageUrl="~/img/emails.png" CommandName="E-mail" style="width:3rem ;height:3rem;" /> </asp:LinkButton>
                                                                 </div> 
                                                        </div>
                                                       
                                                      
                                                       
                                                    </ItemTemplate>--%>

                                                    <ItemTemplate runat="server">
                                                        <asp:LinkButton ID="Edit" runat="server" CssClass="btn btn-default btn-xs" CommandName="alterar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar">
                                                                             <i class="fas fa-edit"></i>
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Deletar">
                                           <i class="fas fa-trash-alt"></i>
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="btnusuario" runat="server" CssClass="btn btn-default btn-xs" CommandName="email" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Enviar senha por e-mail">
                                           <i class="fas fa-envelope"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnreativar" runat="server" CssClass="btn btn-default btn-xs" CommandName="suspenso" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reativar usuário">
                                            <i class="fas fa-thumbs-up"></i> 
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default btn-xs" CommandName="resetsenha" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reset de senha">
                                            <i class="fas fa-key"></i> 
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="usuario" HeaderText="Login" />
                                                <asp:BoundField DataField="nome" HeaderText="Nome" />
                                                <asp:BoundField DataField="permissao" HeaderText="Permissão" />
                                                <asp:BoundField DataField="email" HeaderText="E-mail" />
                                                <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                                <asp:BoundField DataField="suspenso" HeaderText="Suspenso" />
                                            </Columns>

                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:CheckBox ID="chkexibirinativos" runat="server" Text="Exibir Inativos" AutoPostBack="true" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="row">
                            <center>
                                <div class ="col-lg-4 ">
                                      <asp:Button ID="btnAlterar" runat="server" Text="Alterar" Width="127px" CssClass="btn btn-primary " visible="false"/>
                                    </div> 
                                  <div class ="col-lg-4 ">
                                       <asp:Button ID="btnExcluir" runat="server" Text="Excluir" Width="127px" CssClass="btn btn-danger" visible="false"/>
                                    </div> 
                                  <div class ="col-lg-4 ">
                                        <asp:Button ID="btnReativar" runat="server" Text="Reativar" Width="127px" CssClass="btn btn-primary " visible="false"/>
                                    </div> 
                                 </center>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <br />



</asp:Content>


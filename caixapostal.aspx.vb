﻿Imports System.IO
Imports clsSmart

Partial Class autoatendimento_caixapostal
    Inherits System.Web.UI.Page


    Private Sub meucadastro_Load(sender As Object, e As EventArgs) Handles Me.Load
        divgrademenssagem.Visible = False
        If IsPostBack Then

            Exit Sub
        End If
        carregagrade()

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Public Sub carregagrade()

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim consulta As String
        Dim where As String

        tb1 = tab1.conectar("select * from tbcaixapostal where ativo = 1 and nrseqassociado = '" & Session("idassociado") & "' or nrseqassociado = 0 order by dtcad asc")

        gradepostal.DataSource = tb1
        gradepostal.DataBind()

    End Sub


    Private Sub gradepostal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradepostal.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradepostal.Rows(index)

        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")

        If e.CommandName = "visualizar" Then

            Dim xCaixapostal As New clscaixapostal

            xCaixapostal.Nrseq = nrseq.Value

            If Not xCaixapostal.procurarassociados() Then
                sm("swal({title: 'Error!',text: '" & xCaixapostal.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            txtassunto.Text = xCaixapostal.Assunto

            Dim wcnomearq As String = Server.MapPath("~") & "arqshtml\cxpostal.html"
            wcnomearq = alteranome(wcnomearq)
            Dim arquivo As New StreamWriter(wcnomearq)
            arquivo.Write(xCaixapostal.Texto)
            arquivo.Close()

            zframe.Attributes.Add("src", "/arqshtml/" & mARQUIVO(wcnomearq))
            txtdtcad.Text = formatadatamysql(xCaixapostal.Dtcad, True, True)


            divgrademenssagem.Visible = True

        End If


    End Sub
End Class

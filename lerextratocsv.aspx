﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="lerextratocsv.aspx.vb" Inherits="lerextratocsv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updategeral" runat="server">
        <Triggers >
            <asp:PostBackTrigger ControlID ="btnler" />
        </Triggers>
        <ContentTemplate>

      
    <div class="box box-primary ">
        <div class="box box-header ">
            <b>Ler extrato</b>
        </div>
        <div class="box-body ">
            <div class="row">
                <div class="col-lg-4 ">
                    <br />
                    <asp:FileUpload ID="fileextrato" runat="server" />
                </div>
                <div class="col-lg-4 "><br />       
                    <asp:Button ID="btnler" runat="server" Text="Ler Extrato" CssClass="box box-warning " />
                </div>
                   <div class="col-lg-3">
                            <b><asp:Label Text="Data Baixa" runat="server" /></b>
                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtbaixa" runat="server" CssClass="form-control"></asp:TextBox>       
                        </div>
            </div>
            <div class="row">
               
                <div class="col-lg-12 ">
                    <asp:GridView ID="gradeextrato" CssClass="table table-bordered dark" runat="server">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <div runat="server" id="divinativo">
                                        <asp:CheckBox id="chkok"  runat="server" Enabled="false" />                                
                                   <asp:HiddenField ID="hdnrseqagencia" runat="server" Value='<%#Eval("agencia")%>'></asp:HiddenField>
                                    <asp:HiddenField ID="hdconta" runat="server" Value='<%#Eval("conta")%>'></asp:HiddenField>
                                    <asp:HiddenField ID="hddata" runat="server" Value='<%#Eval("data")%>'></asp:HiddenField>
                                    <asp:HiddenField ID="hdnrseqvalor" runat="server" Value='<%#Eval("valor")%>'></asp:HiddenField>
                                    <asp:HiddenField ID="hdoperacao" runat="server" Value='<%#Eval("operacao")%>'></asp:HiddenField>
                                    <asp:HiddenField ID="hdoutros" runat="server" Value='<%#Eval("outrosdados")%>'></asp:HiddenField>                                    
                                        </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div>
                    <center>
                    <asp:LinkButton ID="btnconfirmar" Text="Confirmar" CssClass="btn btn-primary" runat="server" autopostback="true"/>
                        </center>                 
                    <div class="row" id="escondeenquanto" visible="false"> 
                <div class="col-lg-2 ">
                    <label>Associado Nao Encontrado </label><br />
                    <asp:Label Text="" ID="vlnaoencontrado" runat="server" />
                </div>
                <div class="col-lg-2 ">                    
                    <label>Inseridos Para Baixa Manual</label><br />
                    <asp:Label Text="" ID="vlbaixamanual" runat="server" />
                </div>
                <div class="col-lg-2 ">
                    <label>Mensalidade Não encontrada Baixa Manual </label><br />
                    <asp:Label Text="" ID="txtdescritres" runat="server" />
                </div>
                <div class="col-lg-2 ">
                    <label>erros gerados</label><br />
                    <asp:Label Text="" ID="vlerro" runat="server" />
                </div>
                <div class="col-lg-2 ">
                    <label>baixa automatica </label><br />
                    <asp:Label Text="" ID="vlbaixaauto" runat="server" />
                </div>
                <div class="col-lg-2 ">
                    <label>Total    </label><br />
                    <asp:Label Text="" ID="txtcontadeb" runat="server" />
                </div>
                </div>

                </div>
            </div>
        </div>
    </div>
              </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


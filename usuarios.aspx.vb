﻿Imports clssessoes
Imports clsSmart
Imports System.IO

Partial Class usuarios
    Inherits System.Web.UI.Page
    Dim tbusers As New Data.DataTable
    Dim tabusers As New clsBanco

    Private Sub usuarios_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If
        limpar()
        hdnrsequsuario.Value = "0"
        habilitar(False)

        carregarusuarios(chkinativo:=False)
        Dim tbper As New Data.DataTable
        Dim tabper As New clsBanco
        tbper = tabper.conectar("select distinct descricao from tbpermissoes where ativo = true order by descricao")
        cboPermissao.Items.Clear()
        cboPermissao.Items.Add("")
        For x As Integer = 0 To tbper.Rows.Count - 1
            cboPermissao.Items.Add(tbper.Rows(x)("descricao").ToString)
        Next



    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click

        If txtNome.Text = "" Then
            sm("swal({title: 'Attention!',text: 'Campo usuario não preenchido',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        If txtSenha.Text = "" Then
            sm("swal({title: 'Attention!',text: 'Campo senha não preenchido',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        If cboPermissao.Text = "" Then
            sm("swal({title: 'Attention!',text: 'Campo Permissão não selecionado.',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        If txtemail.Text = "" Then
            sm("swal({title: 'Attention!',text: 'Campo email não preenchido.',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        Dim xuser As New clsusuarios
        xuser.Master = False
        xuser.Alterado = False
        xuser.Smartcodesolucoes = False
        xuser.Usuario = txtNome.Text
        xuser.Senha = txtSenha.Text
        xuser.Email = txtemail.Text
        xuser.Permissao = cboPermissao.Text
        xuser.Nrseq = hdnrsequsuario.Value

        If Not xuser.alterarusuario Then
            sm("swal({title: 'Attention!',text: '" & xuser.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        sm("swal({title: 'Muito Bom!',text: '" & xuser.Mensagemerro & "',type: 'success',confirmButtonText: 'OK'})", "swal")

        limpar()
        hdnrsequsuario.Value = "0"
        habilitar(False)
        cboPermissao.Text = "Administradores"
        carregarusuarios(chkinativo:=False)
        txtNome.Focus()

    End Sub

    Private Sub carregarusuarios(Optional procura As String = "", Optional chkinativo As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Dim xsql As String = "Select * from tbusuarios where"
        If chkinativo = True Then
            xsql &= " ativo = false"
        Else
            xsql &= " ativo = true"
        End If
        If chkexibirsuspensos.Checked Then
            xsql &= " and suspenso = true "
        End If
        If procura <> "" Then
            xsql &= " and (usuario like '%" & tratatexto(procura) & "%' or nome like '%" & tratatexto(procura) & "%')"
        End If
        xsql &= " order by usuario"

        tb1 = tab1.conectar(xsql)

        Grade.DataSource = tb1
        Grade.DataBind()
    End Sub





    Private Sub Grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Grade.RowCommand
        Dim existe As Boolean = False
        '  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "escondeexibiralerta2()", "escondeexibiralerta2()", True)

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)


        Dim row As GridViewRow = Grade.Rows(index)



        Dim hdnrseq As HiddenField = CType(row.FindControl("hdnrseq"), HiddenField)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        Select Case e.CommandName.ToString.ToLower
            Case Is = "excluir"
                tb1 = tab1.IncluirAlterarDados("update tbusuarios set ativo = false, dtexclui = '" & formatadatamysql(data) & "', userexclui = '" & Session("usuario") & "' where nrseq = " & hdnrseq.Value)
                carregarusuarios(chkinativo:=chkexibirinativos.Checked)
            Case Is = "suspenso"
                tb1 = tab1.IncluirAlterarDados("update tbusuarios set suspenso = false, qtdtentativas = 0 where nrseq = " & hdnrseq.Value)
                carregarusuarios(chkinativo:=chkexibirinativos.Checked)
            Case Is = "alterar"
                tb1 = tab1.conectar("select * from vwusuarios where nrseq = " & hdnrseq.Value)
                If tb1.Rows.Count <> 0 Then

                    limpar()
                    txtemail.Text = tb1.Rows(0)("email").ToString
                    txtNome.Text = tb1.Rows(0)("usuario").ToString
                    txtSenha.Text = tb1.Rows(0)("senha").ToString
                    hdemailantigo.Value = tb1.Rows(0)("email").ToString
                    hdnrsequsuario.Value = tb1.Rows(0)("nrseq").ToString
retentar:
                    Try
                        cboPermissao.Text = tb1.Rows(0)("permissao").ToString
                    Catch ex As Exception
                        cboPermissao.Items.Add(tb1.Rows(0)("permissao").ToString)
                        GoTo retentar
                    End Try
                    habilitar(True)
                    txtNome.Enabled = False

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "movertela()", "movertela()", True)
                End If
            Case Is = "reativar"
                tb1 = tab1.IncluirAlterarDados("update tbusuarios set ativo = true where nrseq = " & hdnrseq.Value)
                carregarusuarios(chkinativo:=chkexibirinativos.Checked)
            Case Is = "resetsenha"
                tb1 = tab1.IncluirAlterarDados("update tbusuarios set ativo = true, senha = 'usuario', alterado = false, suspenso = false, qtdtentativas = 0 where nrseq = " & hdnrseq.Value)
                tb1 = tab1.conectar("select * from tbusuarios where nrseq = " & hdnrseq.Value)
                If tb1.Rows.Count <> 0 Then
                    If tb1.Rows(0)("email").ToString = "" Then
                        sm("swal({title: 'Atenção!',text: 'Senha resetada com sucesso, porém o usuário não possui um e-mail válido para o envio! Preencha um e-mail válido para o usuário !',type: 'error',confirmButtonText: 'OK'})", "swal")

                        Exit Sub
                    End If
                    Dim email As New clsEnvioEmail
                    email.configpadrao()
                    ' email.AdicionaRemetente = "cte@sistemasceva.com.br"
                    '  email.AdicionaDestinatarios = "cte@sistemasceva.com.br"
                    'email.AdicionaDestinatarios = "monica.santos@cevalogistics.com"
                    '  email.AdicionaDestinatarios = "Hiandra.Mercatelli@Cevalogistics.com"
                    email.AdicionaDestinatarios = tb1.Rows(0)("email").ToString
                    '  email.AdicionaDestinatarios = row.Cells(2).Text
                    email.EhHTML = True

                    Dim texto As String = ""
                    Dim ler As New StreamReader(Server.MapPath("~\recuperasenha2.html"))
                    While ler.EndOfStream = False
                        texto &= ler.ReadLine
                    End While
                    ler.Close()

                    email.AdicionaMensagem = texto.Replace("{usuario}", tb1.Rows(0)("usuario").ToString).Replace("{senha}", tb1.Rows(0)("senha").ToString) '"Bid " & lblnumerolicitacao.Text & " salvo ccom sucesso pelo usuário " & Session("usuario") & " !"

                    email.AdicionaAssunto = "Ceva PAD - Bem vindo ao sistema !"
                    email.ConfigPorta = 587
                    ' email.usarSSL = True
                    '   email.ConfigSMTP = "smtp.sistemasceva.com.br"
                    '  email.Credenciais("cte@sistemasceva.com.br", "Sousmart@747791", "smtp.sistemasceva.com.br")
                    Dim resultado As String = email.EnviarEmail
                    If resultado Then
                        sm("swal({title: 'Que Bom!',text: 'E-mail enviado com sucesso !',type: 'success',confirmButtonText: 'OK'})", "swal")

                    Else
                        sm("swal({title: 'Atenção!',text: 'E-mail não enviado !',type: 'error',confirmButtonText: 'OK'})", "swal")

                    End If
                End If
                carregarusuarios(chkinativo:=chkexibirinativos.Checked)
            Case Is = "email"
                tb1 = tab1.conectar("select * from tbusuarios where nrseq = " & hdnrseq.Value)
                If tb1.Rows.Count <> 0 Then
                    If tb1.Rows(0)("email").ToString = "" Then
                        sm("swal({title: 'Atenção!',text: 'Preencha um e-mail válido para o usuário !',type: 'error',confirmButtonText: 'OK'})", "swal")

                        Exit Sub
                    End If
                    Dim email As New clsEnvioEmail
                    email.configpadrao()
                    ' email.AdicionaRemetente = "cte@sistemasceva.com.br"
                    '  email.AdicionaDestinatarios = "cte@sistemasceva.com.br"
                    'email.AdicionaDestinatarios = "monica.santos@cevalogistics.com"
                    '  email.AdicionaDestinatarios = "Hiandra.Mercatelli@Cevalogistics.com"
                    email.AdicionaDestinatarios = tb1.Rows(0)("email").ToString
                    '  email.AdicionaDestinatarios = row.Cells(2).Text
                    email.EhHTML = True

                    Dim texto As String = ""
                    Dim ler As New StreamReader(Server.MapPath("~\recuperasenha2.html"))
                    While ler.EndOfStream = False
                        texto &= ler.ReadLine
                    End While
                    ler.Close()

                    email.AdicionaMensagem = texto.Replace("{usuario}", tb1.Rows(0)("usuario").ToString).Replace("{senha}", tb1.Rows(0)("senha").ToString) '"Bid " & lblnumerolicitacao.Text & " salvo ccom sucesso pelo usuário " & Session("usuario") & " !"

                    email.AdicionaAssunto = "Ceva PAD - Bem vindo ao sistema !"
                    email.ConfigPorta = 587
                    ' email.usarSSL = True
                    '   email.ConfigSMTP = "smtp.sistemasceva.com.br"
                    '  email.Credenciais("cte@sistemasceva.com.br", "Sousmart@747791", "smtp.sistemasceva.com.br")
                    Dim resultado As String = email.EnviarEmail
                    If resultado Then
                        sm("swal({title: 'Que Bom!',text: 'E-mail enviado com sucesso !',type: 'success',confirmButtonText: 'OK'})", "swal")

                    Else
                        sm("swal({title: 'Atenção!',text: 'E-mail não enviado !',type: 'error',confirmButtonText: 'OK'})", "swal")

                    End If
                End If

        End Select

    End Sub

    Private Sub chkexibirinativos_CheckedChanged(sender As Object, e As EventArgs) Handles chkexibirinativos.CheckedChanged

        carregarusuarios(chkinativo:=chkexibirinativos.Checked)

    End Sub

    Private Sub Grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Grade.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim lblitemgrade As Label = e.Row.FindControl("lblitemgrade")
        lblitemgrade.Text = e.Row.RowIndex + 1
        If e.Row.Cells(6).Text = "1" Then
            e.Row.Cells(6).Text = "Sim"
            e.Row.ForeColor = System.Drawing.Color.Green
            If e.Row.Cells(7).Text = "1" Then
                e.Row.Cells(7).Text = "Sim"
                e.Row.ForeColor = System.Drawing.Color.Gray
            Else
                e.Row.Cells(7).Text = "Não"
                e.Row.ForeColor = System.Drawing.Color.Green
            End If
        Else
            e.Row.Cells(6).Text = "Não"
            e.Row.ForeColor = System.Drawing.Color.Gray
            e.Row.Font.Strikeout = True
        End If
    End Sub

    Private Sub btnprocurar_Click(sender As Object, e As EventArgs) Handles btnprocurar.Click
        carregarusuarios(txtprocura.Text, chkexibirinativos.Checked)
    End Sub

    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click

        Dim xuser As New clsusuarios
        If Not xuser.novo Then
            sm("swal({title: 'Attention!',text: '" & xuser.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        hdnrsequsuario.Value = xuser.Nrseq

        sm("swal({title: 'Muito Bom!',text: '" & xuser.Mensagemerro & "',type: 'success',confirmButtonText: 'OK'})", "swal")

        habilitar(True)
        limpar()
    End Sub

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click

        limpar()
        habilitar(False)

    End Sub

    Private Sub habilitar(Optional habilita As Boolean = True)
        txtSenha.Enabled = habilita
        txtemail.Enabled = habilita
        cboPermissao.Enabled = habilita
        btnnovo.Enabled = Not habilita
        btnSalvar.Enabled = habilita
        txtNome.Enabled = habilita
    End Sub

    Private Sub limpar()
        txtSenha.Text = ""
        txtemail.Text = ""
        If Not cboPermissao.SelectedIndex < 0 Then
            cboPermissao.SelectedIndex = 0
        End If
        txtNome.Text = ""

    End Sub

End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gerarmensalidades.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="gerarmensalidades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary">
        <div class="box-header">
            Gerar lote de Mensalidade
        </div>
        <div class="box-body">
            <div class="row ">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-1">
                    <asp:Label runat="server" Text="Dia para vencimento"></asp:Label>
                    <asp:TextBox ID="TextBox2"  runat="server" CssClass="form-control" MaxLength="2" AutoPostBack="True" Text="15"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    <asp:Label runat="server" Text="Data"></asp:Label>
                    <asp:TextBox ID="TextBox3" Enabled="false" type="date" runat="server" onkeyup="formataPlanoContas(this,event);" CssClass="form-control" MaxLength="9" AutoPostBack="True"  ></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    <asp:Label runat="server" Text="Tipo"></asp:Label>
                    <asp:DropDownList runat="server" CssClass="form-control" ID="cboTipoProcura">
                        <asp:ListItem Value="1">Matricula</asp:ListItem>
                        <asp:ListItem Value="2">Codigo</asp:ListItem>
                        <asp:ListItem Value="2">Nome</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-lg-3">
                    <asp:Label runat="server" Text="Descricao"></asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtprocurar"></asp:TextBox>
                </div>
                <div class="col-lg-1">
                    <asp:CheckBox runat="server" Text="Inativo" CssClass="checkbox-inline" Checked="false" ID="chkexibirinativos" />
                </div>
                <div class="col-lg-2">
                    <asp:LinkButton runat="server" Text="Procurar" CssClass="btn btn-primary" ID="btnprocurar"></asp:LinkButton>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <asp:GridView ID="GradeCaixas" runat="server" CssClass="table table-striped" ShowHeaderWhenEmpty="true" EmptyDataText="Não possui nenhuma caixa." AutoGenerateColumns="false" GridLines="None">
                        <Columns>
                            <asp:BoundField DataField="nrseq" HeaderText="Nr Interno" />
                            <asp:BoundField DataField="dtcad" HeaderText="Data de Cadastro" />
                            <asp:BoundField DataField="dtvencimento" HeaderText="Data de Vencimento" />
                            <asp:BoundField DataField="operacao" HeaderText="Operação" />
                            <asp:BoundField DataField="nrseqplano" HeaderText="Código" />
                            <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                            <asp:BoundField DataField="valor" HeaderText="Valor" />
                            <asp:BoundField DataField="parcela" HeaderText="Parcela" />
                            <asp:BoundField DataField="qtdparcelas" HeaderText="Parcelas" />
                            <asp:TemplateField HeaderText="Ações" runat="server">
                                <ItemTemplate runat="server">
                                    <asp:LinkButton ID="btneditar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                            <i class="fa fa-pencil-square-o"></i>
                                    </asp:LinkButton>
                                    <%-- <asp:LinkButton ID="Baixar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Baixar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                            <i class="fa fa-pencil-square-o"></i>
                                                    </asp:LinkButton>--%>
                                    <%--      <asp:LinkButton ID="btnexcluir" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Remover" CommandArgument='<%#Container.DataItemIndex%>'>
                                                        <i runat="server" id="icogradeexcluir" class="fa fa-trash-o"></i>
                                                    </asp:LinkButton>--%>
                                    <asp:HiddenField runat="server" ID="hdnnrseqgrade" Value='<%#Bind("nrseq") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                    </asp:GridView>
                </div>
            </div>

        </div>
    </div>

</asp:Content>

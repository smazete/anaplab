﻿Imports clssessoes
Imports System.IO
Imports clsSmart

Partial Class associadodocumento
    Inherits System.Web.UI.Page

    Private Sub associadodocumento_Load(sender As Object, e As EventArgs) Handles Me.Load

        divassociado.Visible = False

    End Sub

    Public Sub carregaassociados(Optional exibirinativos As Boolean = False)

        Dim xAssociados As New clsAssociado
        Dim consulta As String

        consulta = ""

        If txtbusca.Text = "" Then
            consulta = ""
            sm("swal({title: 'Error!',text: 'Essa Busca pode demorar',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        Else
            consulta = "where " & cbobusca.SelectedValue & " like '%" & txtbusca.Text & "%'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbAssociados " & consulta & "")

        gradeassociados.DataSource = tb1
        gradeassociados.DataBind()

    End Sub

    Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click

        'gradeassociados.Visible = True
        divassociado.Visible = False
        hddadosuser.Value = ""
        lblcodigo.Text = "0"
        lblmatricula.Text = "0"
        lblnome.Text = ""
        lblcpf.Text = "0"
        divgrade.Visible = True

        Dim xAssociados As New clsAssociado

        xAssociados.Nrseq = txtbusca.Text

        If Not xAssociados.procurar() Then
            sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        gradeassociados.Visible = False
        divassociado.Visible = True

        hddadosuser.Value = xAssociados.Matricula
        lblcodigo.Text = xAssociados.Nrseq
        lblmatricula.Text = xAssociados.Matricula
        lblnome.Text = xAssociados.Nome
        lblcpf.Text = xAssociados.Cpf

        carregagradedoc()



        '        carregaassociados()

    End Sub

    Private Sub gradeassociados_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeassociados.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeassociados.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim matricula As HiddenField = row.FindControl("hdmatricula")


        If e.CommandName = "usar" Then
            '    carregarNoFormulario(nrseq.Value)
            '  habilitar(True)
            '  txtidentificador.Enabled = False

            Dim xAssociados As New clsAssociado

            xAssociados.Nrseq = nrseq.Value

            If Not xAssociados.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            gradeassociados.Visible = False
            divassociado.Visible = True

            hddadosuser.Value = xAssociados.Matricula
            lblcodigo.Text = xAssociados.Nrseq
            lblmatricula.Text = xAssociados.Matricula
            lblnome.Text = xAssociados.Nome
            lblcpf.Text = xAssociados.Cpf

            carregagradedoc()


        End If

    End Sub

    Private Sub btnupload_Click(sender As Object, e As EventArgs) Handles btnupload.Click

        Dim sImageName As String = ""
        Dim descriarquivo As String = ""
        Dim codigo As String = ""
        Dim sImagePathImages As String = Server.MapPath("~\documentosassociados")

        If hddadosuser.Value = "" Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'carregue um usuario para dar continuidade',  showConfirmButton: true, })")
            Exit Sub
        End If

        Dim xDoc As New clsdocassociados

        If Not xDoc.novo Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel carregar os dados',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        codigo = xDoc.Nrseq

        If (upload01.HasFile) Then

            descriarquivo = "\MAT_" & hddadosuser.Value & txtnome.Text & codigo & Path.GetExtension(upload01.FileName)
            sImageName = sImagePathImages & descriarquivo

            ' Dim sFileExt As String = Path.GetExtension(upload01.FileName)
            ' m_sImageNameUserUpload = sImageName + sFileExt
            ' m_sImageNameGenerated = Path.Combine(sImagePathImages, m_sImageNameUserUpload)
            upload01.PostedFile.SaveAs(sImageName)


        End If

        ' Início do salvar\

        xDoc.Nrseq = codigo
        xDoc.Matricula = hddadosuser.Value
        xDoc.Nome = txtnome.Text
        xDoc.Arquivo = descriarquivo

        If Not xDoc.novosalvar Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel gravar o arquivo',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If


        sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Arquivo salvo com sucesso',  showConfirmButton: false,  timer: 2000})")

        carregagradedoc()

    End Sub

    Public Sub carregagradedoc(Optional exibirinativos As Boolean = False)


        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        tb1 = tab1.conectar("select * from tbdocumentosassociados where  ativo = '1' and  matricula = '" & hddadosuser.Value & "' order by nome asc")

        gradedoc.DataSource = tb1
        gradedoc.DataBind()

    End Sub

    Private Sub limpar()

        txtnome.Text = ""
        'txtarquivo.Text = ""
        chkativo.Checked = True
        divgrade.Visible = False

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

    Private Sub gradedoc_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradedoc.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradedoc.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim arquivo As HiddenField = row.FindControl("hdarquivo")



        If e.CommandName = "baixar" Then

            Dim wcarquivo As String = arquivo.Value
            Response.ContentType = "application/octet-stream"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment; filename=" & wcarquivo)
            Response.TransmitFile(Server.MapPath("~\documentosassociados\") & wcarquivo)
            Response.End()

            sm("swal({title: 'Atenção!',text: 'Não foi possivel Baixar o documento',type: 'error',confirmButtonText: 'OK'})", "swal")

        End If
        If e.CommandName = "excluir" Then

            Dim xDocassociados As New clsdocassociados

            xDocassociados.Nrseq = nrseq.Value

            If Not xDocassociados.excluir() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel excluir',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregagradedoc()

        End If

    End Sub



    'Private Sub gradedoc_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradedoc.RowDataBound

    '    If e.Row.RowIndex < 0 Then Exit Sub
    '    Dim div As HtmlGenericControl = e.Row.FindControl("divmostrabtn")

    '    If e.Row.Cells(5).Text = "1" Then
    '        e.Row.ForeColor = System.Drawing.Color.Black
    '        e.Row.BackColor = System.Drawing.Color.LightBlue

    '    ElseIf e.Row.Cells(5).Text = "0" Then
    '        e.Row.ForeColor = System.Drawing.Color.White
    '        e.Row.BackColor = System.Drawing.Color.Gray
    '        div.Visible = False
    '    End If

    '    If e.Row.Cells(5).Text = "0" Then
    '        e.Row.Cells(4).Text = "Excluido"
    '    End If

    'End Sub
End Class

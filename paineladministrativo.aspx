﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="paineladministrativo.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="paineladministrativo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    

    <%-- sweetalert2 monitorando qual vou usar  --%>

    <%-- sweetalert2 monitorando qual vou usar  --%>
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
    <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="/dist/js/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>

    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />

    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/JScriptmascara.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="col-md-12">
     <div class="panel panel-primary">
      <div class="panel-heading">Novos associados e stautus são atualizados ao efetuar o login. Atualize a página para se manter informado!</div>

        <div class="col-md-6">
            <div class="panel-body">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Ultimos associados cadastrados</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <asp:GridView ID="gradelimit5" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <span class="pull-right badge bg-blue">Novo</span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                        <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                        <asp:BoundField DataField="nome" HeaderText="Nome" />
                                        <asp:BoundField DataField="dtcad" HeaderText="Cadastrado" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <%--         ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||          --%>

        <div class="col-md-6">

            <div class="panel-body">

                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>

                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Status Associados</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="col-md-12">
                                    <!-- Info Boxes Style 2 -->


                                    <!-- /.info-box -->
                                    <div class="info-box bg-green">
                                        <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                                        <div class="info-box-content">
                                            <div class="col-md-3">
                                                <span class="info-box-text">Total</span>
                                                <span class="info-box-number">
                                                    <center>
                                                    <asp:Label ID="lblassoctotal" Text="..." runat="server" /></center>
                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="info-box-text">Filiados</span>
                                                <span class="info-box-number">
                                                    <center>
                                                    <asp:Label ID="lblassocativos" Text="..." runat="server" /></center>
                                                </span>
                                            </div>
                                            <div class="col-md-5">
                                                <span class="info-box-text">Desfiliados</span>
                                                <span class="info-box-number">
                                                    <center>
                                                <asp:Label ID="lblassocdesfiliados"  runat="server" /></center>
                                                </span>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 82%"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <span class="progress-description">
                                                    <asp:Label Text="STATUS ASSOCIADOS" runat="server" />
                                                </span>
                                            </div>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>

                                    <div class="info-box bg-yellow">
                                        <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

                                        <div class="info-box-content">

                                            <div class="col-md-5">
                                                <span class="info-box-text">Aposentados</span>
                                                <span class="info-box-number">
                                                    <center>
                                                <asp:Label ID="lblassocaposentado" Text="..." runat="server" /></center>
                                                </span>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="info-box-text">Ativo</span>
                                                <span class="info-box-number">
                                                    <center>
                                                <asp:Label ID="lblassocstatusativo"  runat="server" /></center>
                                                </span>
                                            </div>
                                            <div class="col-md-4">
                                                <span class="info-box-text">Pensionistas</span>
                                                <span class="info-box-number">
                                                    <center>
                                                <asp:Label ID="lblassocpensionista"  runat="server" /></center>
                                                </span>
                                            </div>
                                            <div class="progress">
                                                <div class="progress-bar" style="width: 18%"></div>
                                            </div>
                                            <span class="progress-description">
                                                <asp:Label ID="Label1" Text="SITUAÇÃO" runat="server" />
                                            </span>
                                        </div>

                                        <!-- /.info-box-content -->
                                    </div>
                                                                                                         
                                    <!-- /.info-box -->
                                    <div class="info-box bg-aqua">
                                        <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Contatos</span>
                                            <span class="info-box-number">
                                                <asp:Label ID="lblcontato" runat="server" /></span>

                                            <div class="progress">
                                                <div class="progress-bar" style="width: 100%"></div>
                                            </div>
                                            <span class="progress-description">...
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->

                                    <div class="info-box bg-red">
                                        <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">Documentos disponiveis </span>
                                            <span class="info-box-number">
                                                <asp:Label ID="lbldocumentoassociado" runat="server" /></span>

                                            <div class="progress">
                                                <div class="progress-bar" style="width: 90%"></div>
                                            </div>
                                            <span class="progress-description">...
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->

                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </div>

        <%-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| --%>

  </div>  

</div>
    <%-- |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||| --%>






</asp:Content>


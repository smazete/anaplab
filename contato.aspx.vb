﻿Imports Newtonsoft.Json
Imports clssessoes
Imports clsSmart
Imports System.IO


Partial Class contato
    Inherits System.Web.UI.Page


    Private Sub contato_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
        End If
    End Sub


    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

    Private Sub limpar()
        txtemail.Text = ""
        txtnome.Text = ""
        txtdescricao.Text = ""
        txtassunto.Text = ""
    End Sub


    Private Sub btnenviar_Click(sender As Object, e As EventArgs) Handles btnenviar.Click
        Dim informar As String
        informar = ""
        Dim xContato As New clscontatos
        Dim xEmail As New clsEmail

        If txtnome.Text = "" Then
            informar = informar + " [Nome] "
        End If

        If txtassunto.Text = "" Then
            informar = informar + " [Assunto] "
        End If

        If txtdescricao.Text = "" Then
            informar = informar + " [Mensagem] "
        End If
        If txtemail.Text = "" Then
            informar = informar + " [E-mail] "
        Else
            If validaemail(txtemail.Text) = True Then
            Else
                sm("Swal.fire({  type: 'error',  title: 'Informar um e-mail valido',  confirmButtonText: 'OK'})")
                SetFocus(txtemail.Text)
                Exit Sub
            End If
        End If

        If informar <> "" Then
            sm("Swal.fire({   type: 'warning',  title: 'Preencha os campos " & informar & "',  confirmButtonText: 'OK'})")
            Exit Sub
        End If

        xContato.Nomecompleto = txtnome.Text
        xContato.Assunto = txtassunto.Text
        xContato.Email = txtemail.Text
        xContato.Mensagem = txtdescricao.Text

        If Not xContato.enviarcontato Then
            sm("Swal.fire({ type: 'error',  title: 'Não foi possivel salvar esta ação preencha os campos assinalados',  confirmButtonText: 'OK'})")
            Exit Sub
        End If

        limpar()

        '  sm("Swal.fire({title: 'Prezado(a) " & txtnome.Text & ", sua mensagem foi enviada com sucesso.</br> Caso sua mensagem trate de assunto que careça de resposta, ou caso trate-se de solicitação de apoio ou informações, em breve a equipe de atendimento da ANAPLAB entrará em contato pelo e-mail,<b> " & txtemail.Text & " <b>, informado no formulário de contato.', showClass: { popup: 'animate__animated animate__fadeInDown'}, hideClass: { popup: 'animate__animated animate__fadeOutUp'}})")



        sm("Swal.fire({    type: 'success',  title: 'Enviado com sucesso',  confirmButtonText: 'OK'})")



    End Sub

End Class

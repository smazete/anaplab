﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="caixapostal.aspx.vb" MasterPageFile="~/MasterPage3.master" Inherits="autoatendimento_caixapostal" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Minha Caixa Postal </b>
        </div>
        <div class="box-body">
            <div class="row" id="divgrademenssagem" runat="server">
                <div class="row">
                    <div class="col-lg-10">
                        Assunto
                        <br>
                        <asp:TextBox ID="txtassunto" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                    <div class="col-lg-2">
                        Data 
                        <br>
                        <asp:TextBox ID="txtdtcad" runat="server" Type="data" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <br />
                    <div class="col-lg-12">
                        
                        <iframe runat="server" id="zframe" width="100%" height="300"></iframe>
                        <br />                      
                    </div>
                </div>
            </div>
            <div class="row">
                </hr>
            </div>
            <div class="row">
                <br>
                <asp:GridView ID="gradepostal" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText=" Total de 0 mensagens recebidas " CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                <asp:LinkButton ID="visualizar" runat="server" CssClass="btn btn-info btn-xs" CommandName="visualizar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-envelope"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="assunto" HeaderText="Assunto" />
                        <asp:BoundField DataField="dtcad" HeaderText="Data" />
                    </Columns>
                </asp:GridView>
            </div>
            
                        </ContentTemplate>
                </asp:UpdatePanel>
</asp:Content>

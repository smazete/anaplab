﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="formapagamento.aspx.vb"  MasterPageFile="~/masterpage2.master" Inherits="formapagamento" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <link rel="stylesheet" href="emitir-b.css">
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
    <script type="text/javascript" src="jquery-1.9.1.js"></script>
    <script type="text/javascript" src="core.js"></script>
    <script type="text/javascript" src="jquery.cookie.js"></script>
    <script src="js/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="js/JScriptmascara.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function exibiralerta1() {




            $('#alerta1').show();
            $('#alerta1').load();
        }
        function escondeexibiralerta1() {




            $('#alerta1').hide();
            $('#alerta1').hide();
        }

    </script>--%>

    <div class="box box-primary">
        <div class="box-header">
            Cadastro de Documentos
        </div>
        <div class="box-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <asp:Button ID="btnNovo" runat="server" Text="Novo" CssClass="btn btn-primary" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="Código"></asp:Label>
                                <asp:TextBox ID="txtcodigo" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="Descrição"></asp:Label>
                                <asp:TextBox ID="txtdescricao" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>                      
                    </div>
                    <div class="row text-align-center">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-2">
                            <asp:Button ID="btnSalvar" CssClass="btn btn-success" runat="server" Text="Salvar" Enabled="true" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Button ID="btncancelar" CssClass="btn btn-danger" runat="server" Text="Cancelar" Enabled="true" />
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:GridView ID="gradeformapg" runat="server" AutoGenerateColumns="False" CssClass="grade" ForeColor="Black" GridLines="Horizontal" ShowHeaderWhenEmpty="True" EmptyDataText="Nenhum documento cadastrado !" Style="text-align: center; margin-bottom: 0px;" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="nrseq" HeaderText="Nr Interno" />
                                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                    <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                   <asp:TemplateField HeaderText="Ações" runat="server">
                                            <ItemTemplate runat="server">
                                             
                                                   <asp:LinkButton style="margin:5px;" ToolTip="Imprimir Caixa" ID="btnimprimir" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Alterar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                    <i runat="server" id="i2" class="fa fa-arrow-circle-left"></i>
                                                </asp:LinkButton>

                                                <asp:LinkButton style="margin:5px;" ToolTip="Fechar Caixa" ID="btnfecharcaixa" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                                                            <i class="fa fa-close"></i>
                                                </asp:LinkButton>

                                                </ItemTemplate>
                                       </asp:TemplateField>
                               <%--     <asp:ButtonField ButtonType="Button" CommandName="Excluir" Text="  Remover  ">
                                        <ControlStyle CssClass="botaoredondo" />
                                        <ItemStyle CssClass="botaored" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Button" CommandName="Alterar" Text="  Alterar  ">
                                        <ControlStyle CssClass="botaoredondo" />
                                        <ItemStyle CssClass="botaored" />
                                    </asp:ButtonField>--%>
                                </Columns>
                           <%--     <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                <HeaderStyle BackColor="#990000" BorderStyle="Solid" Font-Bold="True" ForeColor="#FFFFCC" />
                                <PagerStyle BackColor="#FFFFCC" BorderStyle="Solid" BorderWidth="1px" ForeColor="#330099" HorizontalAlign="Center" />
                                <RowStyle BackColor="White" BorderStyle="Solid" BorderWidth="1px" ForeColor="#330099" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                <SortedAscendingCellStyle BackColor="#FEFCEB" />
                                <SortedAscendingHeaderStyle BackColor="#AF0101" />
                                <SortedDescendingCellStyle BackColor="#F6F0C0" />
                                <SortedDescendingHeaderStyle BackColor="#7E0000" />--%>
                            </asp:GridView>


                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </div>

    <%--    <script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.maskMoney.min.js"></script>
    <script type="text/javascript" src="/assets/js/moeda.js"></script>--%>
</asp:Content>
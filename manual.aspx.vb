﻿
Partial Class manual
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsbancosmartcode
    Private Sub manual_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub

        If Session("manualurl") <> "" Then
            frame01.Attributes.Add("src", Session("manualurl"))

        End If

        divmensagem.Style.Add("display", "none")
    End Sub

    Private Sub btnprocurar_Click(sender As Object, e As EventArgs) Handles btnprocurar.Click
        tb1 = tab1.conectar("select distinct titulo, url from vwprodutos_manualonline_tags where descricao like '%" & txtprocurar.Text & "%' or titulo like '%" & txtprocurar.Text & "%' or descricaotag like '%" & txtprocurar.Text & "%'")

        For x As Integer = 0 To tb1.Rows.Count - 1
            Dim xnode As New TreeNode
            xnode.Value = tb1.Rows(x)("url").ToString
            xnode.Text = tb1.Rows(x)("titulo").ToString
            treemanuais.Nodes.Add(xnode)
        Next

        'treemanuais.DataSource = tb1
        'treemanuais.DataBind()
    End Sub

    Private Sub treemanuais_SelectedNodeChanged(sender As Object, e As EventArgs) Handles treemanuais.SelectedNodeChanged
        frame01.Attributes.Add("src", treemanuais.SelectedValue)
    End Sub

    Private Sub optnao_CheckedChanged(sender As Object, e As EventArgs) Handles optnao.CheckedChanged
        If optnao.Checked = True Then
            divmensagem.Style.Remove("display")
        Else

            divmensagem.Style.Add("display", "none")
        End If
    End Sub

    Private Sub optsim_CheckedChanged(sender As Object, e As EventArgs) Handles optsim.CheckedChanged
        If Not optsim.Checked = True Then
            divmensagem.Style.Remove("display")
        Else

            divmensagem.Style.Add("display", "none")
        End If
    End Sub
End Class

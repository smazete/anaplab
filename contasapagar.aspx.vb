﻿Imports clsSmart
Imports clsContasPagar

Partial Class contasapaga
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    Private Sub restrito_contasapagar_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim xcontas As New clsContasPagar
        If IsPostBack Then Exit Sub

        habilitar(False)
        xcontas.carregardocumentos(cbooperacao)
        xcontas.carregarFormasdePagamento(cboformapagto)
        xcontas.carregarPlanosdeContas(cboplano)
        xcontas.carregarCentrodeCusto(cboCentroCusto)
        carregarcontasapagar()
    End Sub


    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click
        Dim xcontas As New clsContasPagar

        If Not xcontas.Novo() Then
            sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

            Exit Sub
        End If
        habilitar(True)
        txtnrseq.Text = xcontas.Nrseq
        btnSalvar.Enabled = True
        btnnovo.Enabled = False

        txtdtvencimento.Focus()
    End Sub

    Private Function carregarcontasapagar() As Boolean

        Try
            Dim xcontas As New clsContasPagar

            xcontas.Descricao = txtprocurar.Text
            xcontas.Ativo = Not chkexibirinativos.Checked

            If Not xcontas.consultardescricao(True) Then
                sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Return False
            End If

            GradeCaixas.DataSource = xcontas.Table
            GradeCaixas.DataBind()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click
        Dim xcontas As New clsContasPagar


        xcontas.Nrseq = txtnrseq.Text
        xcontas.Codigo = txtcodigo.Text
        xcontas.Valor = txtvalor.Text
        xcontas.Parcela = numeros(txtparcela.Text)
        xcontas.Qtdparcelas = numeros(txtparcelas.Text)
        xcontas.Dtexecucao = valordatamysql(txtdtexecucao.Text)
        xcontas.Dtvencimento = valordatamysql(txtdtvencimento.Text)

        If (Not cboplano.SelectedIndex < 0 OrElse cboplano.SelectedValue <> "") Then
            xcontas.Nrseqplano = tratatexto(cboplano.SelectedValue)
        End If
        If (Not cboCentroCusto.SelectedIndex < 0 OrElse cboCentroCusto.SelectedValue <> "") Then
            xcontas.Nrseqcentrocusto = tratatexto(cboCentroCusto.SelectedValue)
        End If
        If (Not cboformapagto.SelectedIndex < 0 OrElse cboformapagto.SelectedValue <> "") Then
            xcontas.Nrseqformapagto = tratatexto(cboformapagto.SelectedValue)
        End If

        If (Not cbooperacao.SelectedIndex < 0 OrElse cbooperacao.SelectedValue <> "") Then
            xcontas.Nrseqdocumento = tratatexto(cbooperacao.SelectedValue)
            If cbooperacao.SelectedItem.Text = "Crédito" Then
                xcontas.Operacao = "C"
            Else
                xcontas.Operacao = "D"
            End If
        End If

        xcontas.Solaprovacao = logico(chksolaprovacao.Checked)
        xcontas.Descricao = tratatexto(txtdescricao.Text)
        xcontas.Nrseqempresa = Session("idempresaemuso")

        If Not xcontas.salvar Then
            sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        habilitar(False)
        limpar()
        btnSalvar.Enabled = False
        btnnovo.Enabled = True

        xcontas.carregardocumentos(cbooperacao)
        xcontas.carregarFormasdePagamento(cboformapagto)
        xcontas.carregarPlanosdeContas(cboplano)
        xcontas.carregarCentrodeCusto(cboCentroCusto)

        carregarcontasapagar()

    End Sub



    Private Sub txtdtvencimento_TextChanged(sender As Object, e As EventArgs) Handles txtdtvencimento.TextChanged
        Dim xcontas As New clsContasPagar

        If IsDate(txtdtvencimento.Text) Then
            xcontas.carregar(txtdtvencimento.Text, GradeCaixas)
        End If
    End Sub
    'Private Sub cboformapagto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboformapagto.SelectedIndexChanged


    '  selectedIndexPagamentos(Page, cboformapagto)

    'End Sub

    Public Sub selectedIndexPagamentos(page As Page, cboformapagto As DropDownList)

        If cboformapagto.Text = "" Then
            Session("ehcarteira") = "nao"
            Exit Sub
        End If

        tb1 = tab1.conectar("select * from tbdocumentos where descricao = '" & cboformapagto.Text & "' and ativo = true")
        If tb1.Rows.Count = 0 Then
            sm("swal({title: 'Error!',text: 'A forma de pagamento não foi localizada !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If


    End Sub

    Private Sub cboplano_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboplano.SelectedIndexChanged
        Dim cls As New clsContasPagar

        cls.selectedIndexPlano(Page, cboplano, txtcodigo)

    End Sub


    Private Sub codigoTextChanged(page As Page, cboplano As DropDownList, txtcodigo As TextBox)

        cboplano.Items.Clear()
        If zeros(mLeft(txtcodigo.Text, 2), 2) <> zeros(Session("idempresaemuso"), 2) Then
            sm("swal({title: 'Error!',text: 'O código digitado é inválido ! O código inicial para sua empresa deverá ser " & zeros(Session("idempresaemuso"), 2) & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        tb1 = tab1.conectar("select * from tbplanocontas where ativo = true And codigo like '" & txtcodigo.Text & "%'")
        If tb1.Rows.Count <> 0 Then


            cboplano.Items.Add("")
            For x As Integer = 0 To tb1.Rows.Count - 1
                cboplano.Items.Add(tb1.Rows(x)("descricao").ToString)
            Next
            If tb1.Rows.Count = 1 Then
                cboplano.Text = tb1.Rows(0)("descricao").ToString

            End If

        End If

    End Sub

    Private Sub txtcodigo_TextChanged(sender As Object, e As EventArgs) Handles txtcodigo.TextChanged

        codigoTextChanged(Page, cboplano, txtcodigo)

    End Sub

    Private Sub GradeCaixas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GradeCaixas.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub

        Dim btnexcluir As LinkButton = e.Row.FindControl("btnexcluir")
        Dim btnrestaurar As LinkButton = e.Row.FindControl("btnrestaurar")

        Try
            e.Row.Cells(1).Text = FormatDateTime(e.Row.Cells(1).Text, DateFormat.ShortDate)
        Catch ex As Exception
            e.Row.Cells(1).Text = ""
        End Try

        Try
            e.Row.Cells(2).Text = FormatDateTime(e.Row.Cells(2).Text, DateFormat.ShortDate)
        Catch ex As Exception
            e.Row.Cells(2).Text = ""
        End Try
        If logico(e.Row.Cells(3).Text) = True Then
            e.Row.Cells(3).Text = "Sim"
            btnexcluir.Visible = True
            btnrestaurar.Visible = False
        Else
            e.Row.Cells(3).Text = "Não"
            btnexcluir.Visible = False
            btnrestaurar.Visible = True
        End If

    End Sub

    Private Sub habilitar(Optional habilita As Boolean = True)
        btnnovo.Enabled = Not habilita
        cboCentroCusto.Enabled = habilita
        cboformapagto.Enabled = habilita
        cboplano.Enabled = habilita
        txtdescricao.Enabled = habilita
        txtdtexecucao.Enabled = habilita
        txtdtvencimento.Enabled = habilita
        txtparcelas.Enabled = habilita
        txtvalor.Enabled = habilita
        txtparcela.Enabled = habilita
        cbooperacao.Enabled = habilita
        btnSalvar.Enabled = habilita
        cboformapagto.Style.Add("cssclass", "form-control")

    End Sub

    Private Sub limpar()
        txtnrseq.Text = ""
        txtcodigo.Text = ""
        txtdescricao.Text = ""
        txtvalor.Text = ""
        txtparcela.Text = ""
        txtparcelas.Text = ""
        txtdtexecucao.Text = ""
        txtdtvencimento.Text = ""

        If Not cboCentroCusto.SelectedIndex < 0 Then
            cboCentroCusto.SelectedIndex = 0
        End If
        If Not cboformapagto.SelectedIndex < 0 Then
            cboformapagto.SelectedIndex = 0
        End If
        If Not cbooperacao.SelectedIndex < 0 Then
            cbooperacao.SelectedIndex = 0
        End If
        If Not cboplano.SelectedIndex < 0 Then
            cboplano.SelectedIndex = 0
        End If

        chksolaprovacao.Checked = False
    End Sub

    Private Sub GradeCaixas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GradeCaixas.RowCommand
        Dim xcontas As New clsContasPagar
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = GradeCaixas.Rows(index)

        Dim xcaixa As New clsCaixas
        Dim hdnnrseqgrade As HiddenField = row.FindControl("hdnnrseqgrade")



        If e.CommandName.ToLower = "editar" Then
            xcontas.carregardocumentos(cbooperacao)
            xcontas.carregarFormasdePagamento(cboformapagto)
            xcontas.carregarPlanosdeContas(cboplano)
            xcontas.carregarCentrodeCusto(cboCentroCusto)

            xcontas.Nrseq = hdnnrseqgrade.Value
            xcontas.Ativo = Not chkexibirinativos.Checked

            If Not xcontas.consultardescricao(consultarnrseq:=True) Then
                sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            'Desativar a alteração se o usuario estiver excluido
            'If xplanosdecontas.Ativo = False Then
            '    habilitar(False)
            '    btnnovo.Enabled = True
            '    btnsalvar.Enabled = False
            '    limpar()
            '    sm("Swal.fire({'Atenção!','Habilite o usuario para alterar ele.','error',confirmButtonText: 'OK'})", "swal")
            '    Exit Sub
            'ElseIf xplanosdecontas.Ativo = True Then

            '    txtCodigo.Text = xplanosdecontas.Nrseq
            '    txtdescricao.Text = xplanosdecontas.Descricao
            '    txtsiglas.Text = xplanosdecontas.Codigo
            '    hdnnrseqempresa.Value = xplanosdecontas.Nrseqempresa

            '    habilitar(True)
            'End If

            If (Not cboplano.SelectedIndex < 0 OrElse cboplano.SelectedValue <> "") Then
                Try
                    cboplano.SelectedValue = xcontas.Nrseqplano
                Catch ex As Exception
                    sm("swal({title: 'Plano não carregado!',text: 'Plano registrado não existe mais.',type: 'error',confirmButtonText: 'OK'})", "swal")
                End Try
            End If
            If (Not cboCentroCusto.SelectedIndex < 0 OrElse cboCentroCusto.SelectedValue <> "") Then
                Try
                    cboCentroCusto.SelectedValue = xcontas.Nrseqcentrocusto
                Catch ex As Exception
                    sm("swal({title: 'Centro de custo não carregado!',text: 'Centro de custo registrado não existe mais.',type: 'error',confirmButtonText: 'OK'})", "swal")
                End Try
            End If
            If (Not cboformapagto.SelectedIndex < 0 OrElse cboformapagto.SelectedValue <> "") Then
                Try
                    cboformapagto.SelectedValue = xcontas.Nrseqformapagto
                Catch ex As Exception
                    sm("swal({title: 'Forma de pagamento não carregada!',text: 'Forma de pagamento registrada não existe mais.',type: 'error',confirmButtonText: 'OK'})", "swal")
                End Try
            End If

            If (Not cbooperacao.SelectedIndex < 0 OrElse cbooperacao.SelectedValue <> "") Then
                Try
                    cbooperacao.SelectedValue = xcontas.Nrseqdocumento
                Catch ex As Exception
                    sm("swal({title: 'Operação não carregada!',text: 'Operação registrada não existe mais.',type: 'error',confirmButtonText: 'OK'})", "swal")
                End Try
            End If

            txtnrseq.Text = hdnnrseqgrade.Value
            txtdescricao.Text = xcontas.Descricao
            txtvalor.Text = xcontas.Valor
            txtparcela.Text = xcontas.Parcela
            txtparcelas.Text = xcontas.Qtdparcelas
            txtdtexecucao.Text = xcontas.Dtexecucao
            txtdtvencimento.Text = xcontas.Dtvencimento
            chksolaprovacao.Checked = xcontas.Solaprovacao

            habilitar(True)

            listararquivos()
        End If
        If e.CommandName.ToLower = "restaurar" Then

            xcontas.Nrseq = hdnnrseqgrade.Value
            If Not xcontas.excluir() Then
                sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            carregarcontasapagar()

        End If
        If e.CommandName.ToLower = "excluir" Then

            xcontas.Nrseq = hdnnrseqgrade.Value
            If Not xcontas.excluir() Then
                sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            carregarcontasapagar()

        End If

    End Sub

    Private Sub btnprocurar_Click(sender As Object, e As EventArgs) Handles btnprocurar.Click
        Dim xcontasapagar As New clsContasPagar
        xcontasapagar.carregardocumentos(cbooperacao)
        xcontasapagar.carregarFormasdePagamento(cboformapagto)
        xcontasapagar.carregarPlanosdeContas(cboplano)
        xcontasapagar.carregarCentrodeCusto(cboCentroCusto)
        carregarcontasapagar()

    End Sub

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click
        limpar()
        habilitar(False)
    End Sub


    Private Sub btncarregarxls_Click(sender As Object, e As EventArgs) Handles btncarregarxls.Click

        If txtnrseq.Text = "" OrElse txtnrseq.Text = "0" Then
            sm("swal({title: 'Atenção!',text: 'Nenhuma conta selecionada',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        Dim tbarq As New Data.DataTable
        Dim tabarq As New clsBanco
        Dim uploadedFile As HttpPostedFile = filearq.PostedFile
        Dim FileName As String = "", nomecompleto As String = ""
        Dim FolderPath As String = ConfigurationManager.AppSettings("FolderPath")
        Dim qtdlinhas As Integer = 0
        'Dim FilePath As String '= Server.MapPath("~\importacaocarteira\" & FileName)
        If uploadedFile Is Nothing Then Exit Sub
        If uploadedFile.FileName = "" Then Exit Sub


        'FilePath = Server.MapPath("~\arquivos")
        FileName = alteranome(Server.MapPath("~") & "arquivos\contasapagar\arquivo." & encontrachr(uploadedFile.FileName, ".", 0))
        nomecompleto = FileName
        'FilePath = Server.MapPath("~\arquivos\contasapagar\" & FileName)
        Try
            filearq.SaveAs(nomecompleto)
            '  processarxml(FilePath)
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tbarq = tabarq.IncluirAlterarDados("insert into tbcontas_arq (arquivo, dtcad, usercad, ativo, linhas, processado,nrseqcontasapagar) values ('" & mARQUIVO(FileName) & "', '" & formatadatamysql(clsSmart.data) & "', '" & Session("usuario") & "', true," & qtdlinhas & ", False,'" & txtnrseq.Text & "')")
            listararquivos()
            btnprocessararq.Enabled = True
            'Session("filepath") = FilePath
            'processarxml()

            ' Dim teste As New Threading.Thread(AddressOf processarxml)
            'teste.Start()
        Catch ex As Exception
            Dim lixo As String = ex.Message
            lixo &= "1"
        End Try
    End Sub
    Private Sub listararquivos()
        Dim tbarq As New Data.DataTable
        Dim tabarq As New clsBanco
        If txtnrseq.Text = "" OrElse txtnrseq.Text = "0" Then
            sm("swal({title: 'Atenção!',text: 'Nenhuma conta selecionada',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        tbarq = tabarq.conectar("select * from tbcontas_arq where nrseqcontasapagar='" & txtnrseq.Text & "' and ativo=true order by nrseq desc")
        gradearquivos.DataSource = tbarq
        gradearquivos.DataBind()
    End Sub
    Private Sub processar()
        Dim japrocessado As String = ""
        For Each row As GridViewRow In gradearquivos.Rows
            Dim sel As CheckBox = row.FindControl("sel")
            Dim lblnrseq As HiddenField = row.FindControl("lblnrseq")
            Dim processado As HiddenField = row.FindControl("hdnprocessado")
            Dim arquivo As HiddenField = row.FindControl("hdnarquivo")

            If sel.Checked Then
                If logico(processado.Value) = True Then
                    japrocessado = japrocessado + arquivo.Value + " , "
                Else
                    Dim tb1 As New Data.DataTable
                    Dim tab1 As New clsBanco
                    Dim qtdlinhas As Integer = processarxml(row.Cells(1).Text, lblnrseq.Value, hdconexao.Value)
                    tb1 = tab1.IncluirAlterarDados("update tbcontas_arq set linhas = " & qtdlinhas & ", processado = " & IIf(qtdlinhas = -1, "False", "True") & ", dtprocessado = '" & valordatamysql(clsSmart.data) & "', userprocessado = '" & Session("usuario") & "' where nrseq = " & lblnrseq.Value)
                End If
            End If
        Next
        If japrocessado <> "" Then
            japrocessado = japrocessado.Remove(japrocessado.Length - 2)
            sm("swal({title: 'Atenção!',text: 'o Arquivo " & japrocessado & " já foi processado.',type: 'error',confirmButtonText: 'OK'})", "swal")
        End If
        listararquivos()

    End Sub
    Private Function processarxml(arquivo As String, nrseqarquivo As Integer, xconexao As String) As Integer
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim tbarquivo As New Data.DataTable
        Dim tbprocs As New Data.DataTable
        Dim tabprocs As New clsBanco
        Dim wclinhaatual As Integer = 0
        Dim wcsessao As String = xconexao
        tb1 = tab1.conectar("select linhaatual from tbcontas_arq  where  nrseq = " & nrseqarquivo)
        Dim linhapular As Integer = 0
        If tb1.Rows.Count > 0 Then
            If tb1.Rows(0)("linhaatual").ToString <> "" Then
                linhapular = tb1.Rows(0)("linhaatual").ToString
            End If
        End If
        ''''''''''      Dim conexao_Excel As String = "Provider=Microsoft.Jet.OleDb.4.0;data source=" & Server.MapPath("~") & "arquivos\contasapagar\" & arquivo & ";Extended Properties=Excel 8.0;"



        'Dim conexao As OleDbConnection = New OleDbConnection(conexao_Excel)
        'Dim da As OleDbDataAdapter
        'da = New OleDbDataAdapter("Select * From [importa$]", conexao)



        'da.Fill(tbarquivo)

        tbarquivo = csvToDatatable(Server.MapPath("~") & "arquivos\contasapagar\" & arquivo, ";", alteranome(Server.MapPath("~") & "logerros\contasapagar.txt"))


        tbprocs = tabprocs.conectar("select * from tbcontas where nrseq = '" & txtnrseq.Text & "' and ativo = true AND nrseqempresa = '" & Session("idempresaemuso") & "' ")

        Dim linha As Integer = 0

        For x As Integer = 0 To tbarquivo.Rows.Count - 1
            If x < linhapular Then
                Continue For
            End If
            linha = x + 1
        Next
        tb1 = tab1.IncluirAlterarDados("update tbcontas_arq set linhaatual = " & linha & ", linhas = " & tbarquivo.Rows.Count & "  where  nrseq = " & nrseqarquivo)
    End Function
    Private Sub btnprocessararq_Click(sender As Object, e As EventArgs) Handles btnprocessararq.Click
        hdconexao.Value = Session("conexao")
        Dim thprocessar As New Threading.Thread(AddressOf processar)
        thprocessar.Start()
        listararquivos()
    End Sub

    Private Sub gradearquivos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradearquivos.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        If logico(e.Row.Cells(4).Text) = True Then
            e.Row.Cells(4).Text = "Sim"
            e.Row.Style.Add("background-color", "#90ee90")
        Else
            e.Row.Cells(4).Text = "Não"
            e.Row.Style.Add("background-color", "#FFCCCB")

        End If
        If e.Row.Cells(5).Text <> "&nbsp;" AndAlso e.Row.Cells(5).Text <> " " AndAlso e.Row.Cells(5) IsNot Nothing Then
            e.Row.Cells(5).Text = FormatDateTime(e.Row.Cells(5).Text, DateFormat.ShortDate)
        End If
        If e.Row.Cells(2).Text <> "&nbsp" AndAlso e.Row.Cells(2).Text <> " " AndAlso e.Row.Cells(2) IsNot Nothing Then
            e.Row.Cells(2).Text = FormatDateTime(e.Row.Cells(2).Text, DateFormat.ShortDate)
        End If
    End Sub

    Private Sub btnatualizar_Click(sender As Object, e As EventArgs) Handles btnatualizar.Click
        listararquivos()
    End Sub

    Private Sub gradearquivos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradearquivos.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradearquivos.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnnrseq")
        Dim arquivo As HiddenField = row.FindControl("hdnarquivo")
        Dim hdnprocessado As HiddenField = row.FindControl("hdnprocessado")
        If e.CommandName.ToLower = "excluir" Then
            tb1 = tab1.IncluirAlterarDados("update tbcontas_arq set ativo=False where nrseq = " & nrseq.Value)
            listararquivos()
        End If
        If e.CommandName.ToLower = "naoprocessar" Then
            If hdnprocessado.Value = False Then
                sm("swal({title: 'Atenção!',text: 'o Arquivo " & arquivo.Value & " ainda não foi processado.',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If
            tb1 = tab1.IncluirAlterarDados("update tbcontas_arq set processado=False where nrseq = " & nrseq.Value)
            listararquivos()
        End If
    End Sub
End Class

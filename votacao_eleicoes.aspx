﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageEleicoes.master" AutoEventWireup="false" CodeFile="votacao_eleicoes.aspx.vb" Inherits="votacao_eleicoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Eleições 2020</b>
        </div>
        <div class="box-body">
            <div class="panel-body">
                <asp:UpdatePanel ID="UpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel">
                            <ProgressTemplate>
                                <div class="UpdatePG_Centro">
                                    <asp:Label ID="lblaguarde2" runat="server" Font-Size="15" Text="Aguarde! Carregando..." ForeColor="White" Font-Bold="true"></asp:Label>
                                    <br />
                                    <br />
                                    <img src="/img/loaddouble01.gif" style="height: 10rem; width: 10rem;" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                       
                        <div id="divgeral" runat="server">
                             <div class="row">
                            <div class="col-md-12 text-center">
                                Olá, prezado(a)
                                <asp:Label ID="lblassociado" runat="server" ForeColor="Green"></asp:Label>! Seja bem vindo(a)!
                            <br />
                                <asp:Label ID="lbllbemvindo" runat="server" text="Por favor, selecione seu candidato e pressione Confirmar!"></asp:Label>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 "></div> 
                            <div class="col-md-10 ">
                                <br />
                                <br />
                                <asp:Repeater ID="rptchapas" runat="server">
                                    <ItemTemplate>
                                        <div class="col-lg-3">
                                           
                                                <div runat="server" id="divchapa">
                                                     <asp:LinkButton CssClass="small-box-footer" CommandArgument='<%#Container.ItemIndex%>' CommandName="votar" ID="btnvotar" runat="server">
                                                    <div class="inner">
                                                        <span>
                                                            <p><asp:Label ID="lblchapafixa" runat="server" Text="Chapa"></asp:Label> </p>
                                                            <h3><%# DataBinder.Eval(Container.DataItem, "numero") %></h3>
                                                        </span>
                                                        <p>  <%# DataBinder.Eval(Container.DataItem, "descricao") %></p>
                                                    </div>
                                                    <asp:HiddenField ID="hdnrseqrpt" runat="server" Value='<%#Bind("nrseq") %>' />
                                                    <asp:HiddenField ID="hdnvotourpt" runat="server" Value='<%#Bind("votou") %>' />
                                                    <asp:HiddenField ID="hdntemvotorpt" runat="server" Value='<%#Bind("temvoto") %>' />
                                                    <asp:HiddenField ID="hdnchaparpt" runat="server" Value='<%#Bind("numero") %>' />
                                                    <i class="fa fa-check-circle-o"></i>Votar
                                                          </asp:LinkButton>
                                                </div>
                                           
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div id="divconfirmar" runat="server" style="display:none">
                            <div class="row">
                                <div class="col-lg-12 ">

                                    <hr style="border-color:cadetblue " />
                                </div>
                            </div>
                              <div class="row">
                            <br />
                            <div class="col-md-6 text-center">
                                <asp:HiddenField ID="hdnrseqsel" runat="server" />
                                <asp:HiddenField ID="hdchapa" runat="server" />
                                <asp:LinkButton ID="btnconfirmar" runat="server" CssClass="btn btn-success ">Confirmar</asp:LinkButton>
                            </div>
                            <div class="col-md-6 text-center">
                                 <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger ">Cancelar</asp:LinkButton>
                            </div>
                        </div>
                        </div>
                      </div>
                        <div id="divfinal" runat="server">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                               <span style="font-size:14px">     Muito obrigado por sua participação! </span>
                                    <br />

                                </div>
                            </div>
                      </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>


﻿<%@ Page Title="" Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="relatorioguias.aspx.vb" Inherits="restrito_relatorioguias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <br />

    <script language="javascript" type="text/javascript">
        function movescroll() {



            $("html, body").animate({
                scrollTop: $($.attr(this, '#inicio')).offset().top
            }, 1200);

            $("html, body").animate({
                scrollTop: 0
            }, 1200);
            $("html, body").animate({
                scrollTop: 0
            }, 1200);

        }
        function exibiralerta1() {


            $('#alerta1').show();
            $('#alerta1').load();
        }
        function escondeexibiralerta1() {




            $('#alerta1').hide();
            $('#alerta1').hide();
        }
    </script>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--Animation library for notifications   -- >
                 <link href="assets/css/animate.min.css" rel="stylesheet" />

                 <!--Light Bootstrap Table core CSS    -- >
                     <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet" />


                 


    <!--Fonts and icons     -- >
             <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
                 <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
                     <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

                     <!--   Core JS Files   -->

    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>
    <!--Trabalhando com BootStrap-->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>

    <%--  
aqui--%>



    <link href="css/defalt.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="js/jquery.quicksearch.js" type="text/javascript"></script>
    <script src="js/JScriptmascara.js" type="text/javascript"></script>


    

    <br />
    <asp:UpdatePanel ID="panelgeral" runat="server">
        <ContentTemplate>
            <div class="box box-primary" style="margin-left: 5px; margin-right: 5px;">
                <div class="box-header" >
                    <center>Relatório de Guias Emitidas</center>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-2">
                            Data Inicial
                            <br />
                            <asp:TextBox ID="txtdtinicial" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                        </div>
                         <div class="col-lg-2">
                            Data Final
                            <br />
                            <asp:TextBox ID="txtdtfinal" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                        </div>
                         <div class="col-lg-2">
                            Laboratório
                            <br />
                            <asp:DropDownList ID="cbolaboratorio" runat="server" CssClass="form-control" TextMode="Date"></asp:DropDownList>
                        </div>
                         <div class="col-lg-2">
                            <asp:CheckBox ID="chkexibirtodos" runat="server" text="Exibir exames" />
                            <br />
                            <asp:CheckBox ID="chkexibirapenasbaixados" runat="server" text="Exibir apenas baixados" />
                        </div>
                         <div class="col-lg-2">
                              <br />
                            <asp:Button ID="btngerar" runat="server" Text="Gerar" CssClass="btn btn-primary "/>
                           
                           
                        </div>
                       <%--  <div class="col-lg-2">
                              <br />
                            <asp:Button ID="btnExcel" runat="server" Text="Excel" CssClass="btn btn-danger"/>
                           
                           
                        </div>--%>
                        </div>
                    <div class="row">
                        <br />
                               <asp:GridView ID="grade" runat="server" class="table " ShowHeaderWhenEmpty="true" EmptyDataText="Sem guias no período" AutoGenerateColumns="false" ForeColor="#333333" GridLines="None">
                               <%-- <HeaderStyle BackColor="#990033" ForeColor="White" />--%>
                                <Columns>
                                   
                                    <asp:BoundField DataField="nomeempresa" HeaderText="Conveniado" />
                                     <asp:BoundField DataField="valortotalcusto" HeaderText="Valor Custo" />
                                    <asp:BoundField DataField="valortotalpago" HeaderText="Valor Pago" />
                                    <asp:TemplateField HeaderText="Saldo">
                                        <ItemTemplate >
                                            <asp:Label ID="lblsaldo" runat="server" ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField >
                                        <ItemTemplate >
                                            
                                              <asp:GridView ID="gradedth" runat="server" class="table " ShowHeaderWhenEmpty="true" EmptyDataText="Sem exames" AutoGenerateColumns="false" ForeColor="#333333" GridLines="None" Visible="false" ShowFooter="true">
                               <%-- <HeaderStyle BackColor="#990033" ForeColor="White" />--%>
                                <Columns>
                                     <asp:BoundField DataField="descricao" HeaderText="Exame" />
                                    <asp:BoundField DataField="valortotalpago" HeaderText="Valor Pago" />
                                    </Columns> 
                                                  </asp:GridView> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  <%--  <asp:ButtonField ButtonType="Image" CommandName="Abrir" HeaderText="Open" ImageUrl="~/img/imgokboll.png">
                                        <ControlStyle Width="27px" Height="27px"></ControlStyle>

                                    </asp:ButtonField>--%>
                                    <%-- <asp:ButtonField ButtonType="Image" CommandName="Abrir" HeaderText="Open" ImageUrl="~/img/imgnao.png">
                                        <ControlStyle Width="27px" Height="27px"></ControlStyle>

                                    </asp:ButtonField>--%>
                                </Columns>
                                    <FooterStyle BackColor="darkred" ForeColor="white" />
                            </asp:GridView>
                    </div>
                    <br />
                     <div class="row">
                         <div class="col-lg-2 ">
                             Total Custo
                             <br />
                             <asp:Label ID="txttotcusto" runat="server" ></asp:Label>
                         </div>
                          <div class="col-lg-2 ">
                             Total Pago
                             <br />
                             <asp:Label ID="txttotpago" runat="server" ></asp:Label>
                         </div>
                          <div class="col-lg-2 ">
                             Total Saldo
                             <br />
                             <asp:Label ID="txttotsaldo" runat="server"></asp:Label>
                         </div>
                         </div> 
                    <div class="row">
                        <br />
                         <div class="col-lg-12">
                              <br />
                             <center>
                            <asp:Button ID="btnImprimir" runat="server" Text="Imprimir" CssClass="btn btn-primary "/>
                                 </center>
                           
                           
                        </div>
                    </div>
                    </div> 
                </div> 
            </ContentTemplate> 
        </asp:UpdatePanel> 
    
</asp:Content>


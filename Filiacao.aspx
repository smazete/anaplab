﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage2.master" CodeFile="Filiacao.aspx.vb" Inherits="Filiacao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Dados para Filiação </b>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:LinkButton ID="btnnovo" runat="server" CssClass="btn btn-warning " Visible="false"> <i class="fas fa-plus-circle"></i>Novo</asp:LinkButton>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="box box-primary">            
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="Matricula" runat="server"></asp:Label>
                                <asp:TextBox ID="txtmatricula" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <asp:Label Text="Nome" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label Text="E-mail" runat="server"></asp:Label>
                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label Text="Dtnasc" runat="server"></asp:Label>
                                <asp:TextBox type="date" ID="txtdtnasc" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="CPF" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcpf" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="RG" runat="server"></asp:Label>
                                <asp:TextBox ID="txtrg" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="Telddd" runat="server"></asp:Label>
                                <asp:TextBox ID="txttelddd" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="Telefone" runat="server"></asp:Label>
                                <asp:TextBox ID="txttelefone" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-1">
                                <asp:Label Text="Sexo" runat="server"></asp:Label>
                                <asp:TextBox ID="txtsexo" runat="server" CssClass="form-control" Text="M"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="CEP" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcep" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-1">
                                <asp:Label Text="Estado" runat="server"></asp:Label>
                                <asp:TextBox ID="txtestado" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label Text="Cidade" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcidade" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label Text="Endereço" runat="server"></asp:Label>
                                <asp:TextBox ID="txtendereco" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label Text="Bairro" runat="server"></asp:Label>
                                <asp:TextBox ID="txtbairro" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-1">
                                <asp:Label Text="Numero" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="Complemento" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcomplemento" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <asp:Label Text="Nome Mãe" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnomemae" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <b>Dados de Acesso </b>
                                    </div>
                                    <div class="card-body">
                                        <br />
                                        <div class="col-lg-4">
                                            <b>
                                                <asp:Label Text="USUARIO:" runat="server"></asp:Label></b>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:TextBox ID="txtmatricula2" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-4">
                                            <b>
                                                <asp:Label Text="SENHA:" runat="server"></asp:Label></b>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:TextBox type="password" ID="txtsenha" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <b>Dados do Associado </b>
                                    </div>
                                    <div class="card-body bg-aqua">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label ID="lblsituaçao" Text="Situação" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox ID="txtsituacaopb" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label Text="Data posse" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox type="date" ID="txtdtposse" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label Text="Data Aposentadoria" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox type="date" ID="txtdtaposentadoria" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <b>Dados Bancarios</b>
                                        </div>
                                        <div class="card-body">
                                            <br />
                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label Text="Agencia" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox ID="txtAgencia" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label Text="Conta corrente" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox ID="txtcontacorrente" runat="server" CssClass="form-control"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label runat="server" >Manifesto meu interesse em associar-me à ANAPLAB, a partir desta data, e providenciarei a confirmação da autorização das TRANSFERÊNCIAS AUTOMÁTICAS das mensalidades em minha conta corrente acima identificada, conforme orientações expedidas pela TESOURARIA.
Valor da Mensalidade: R$ 20,00 (vinte reais)

Ao clicar em "ENVIAR" você terá completado o cadastro e receberá, em até 48 horas, um e-mail com as instruções para aderir ao débito da mensalidade em conta corrente. Depois de concluída essa etapa, os benefícios estarão disponíveis.
Clique no botão ENVIAR, abaixo, para concluir seu cadastro.</label>

                        </div>

                    </div>
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
        </div>
        </div>
</asp:Content>


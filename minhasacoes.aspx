﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="minhasacoes.aspx.vb" MasterPageFile="~/MasterPage3.master" Inherits="autoatendimento_minhasacoes" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="col-md-2"></div>
    <div class="col-md-8">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
                <div class="widget-user-image">
                    <img class="img-circle" src="img/alerta01.png" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Orientações</h3>
                <h5 class="widget-user-desc">Visualização Ações</h5>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <li><a href="#"><span class="pull-left badge bg-warning"><b>></b></span> ANOTE O NÚMERO DO SEU PROCESSO ANTES DE CONSULTÁ-LO.</a> </li>
                    <li><a href="#"><span class="pull-left badge bg-warning"><b>></b></span> Clique no link <b>CONSULTAR PROCESSO</b>, ao lado do número. </a></li>
                    <li><a href="#"><span class="pull-left badge bg-red">*</span> Cada link redireciona à pagina do judiciário onde se deu o respectivo ajuizamento. (siga atentamente as instruções da página  para efetuar a consulta). </a></li>
                </ul>
            </div>
        </div>
        <!-- /.widget-user -->

        <div class="box box-primary widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
                <div class="widget-user-image">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Minhas Ações</h3>
                <h5 class="widget-user-desc"></h5>
            </div>
            <div class="box-footer no-padding">
                <asp:GridView ID="grade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma ação vinculada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="nome" HeaderText="Nome" />
                      <asp:BoundField DataField="numero" HeaderText="Número" /> 
                        <asp:TemplateField HeaderText="Consultar Processo">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdlink" runat="server" Value='<%#Bind("link")%>'></asp:HiddenField>                            
                                <a runat="server" href='<%# Eval("link") %>' target="_blank" dir="auto">
                                    <asp:Label Text='<%# Eval("link") %>'  runat="server" />
                                </a>       
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

    <div class="col-md-2"></div>


</asp:Content>

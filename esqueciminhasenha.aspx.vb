﻿Imports Newtonsoft.Json
Imports clssessoes
Imports clsSmart
Imports System.IO

Partial Class esqueciminhasenha
    Inherits System.Web.UI.Page
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

     Private Sub btnenviar_Click(sender As Object, e As EventArgs) Handles btnenviar.Click
        Dim xEsenha As New clsAssociado

        xEsenha.Email = txtemail.Text
        xEsenha.Cpf = sonumeros(txtcpf.Text)


        If Not xEsenha.validaemailcpf Then
            sm("Swal.fire({ type: 'error',  title: 'CPF ou E-mail invalidos',  confirmButtonText: 'OK'})")
            Exit Sub
        Else
            If Not xEsenha.recuperarsenha Then
                sm("Swal.fire({ type: 'error',  title: 'Não foi possivel salvar esta ação preencha os campos assinalados',  confirmButtonText: 'OK'})")
                Exit Sub
            Else
                If Not xEsenha.informarecuperarsenha Then
                    Exit Sub
                End If

                sm("Swal.fire({ type: 'success',  title: 'A senha foi enviada para o e-mail cadastrado. ',  confirmButtonText: 'OK'})")

            End If
        End If



    End Sub
End Class

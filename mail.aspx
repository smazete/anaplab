﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="mail.aspx.vb" Inherits="mail" ValidateRequest="false"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row" style="margin-left: 1rem; margin-top: 1rem; margin-right: 1rem;">

        <div class="box box-primary " style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
            <div class="box box-header ">
                <div class="row">
                    <div class="col-lg-1 ">
                        <asp:HiddenField  ID="hdtabela"  runat="server" />
                        <img src="/img/emails.png" style="width: 4rem; height: 3.5rem;" />
                    </div>
                    <div class="col-lg-10 text-center">
                  <asp:Label runat="server" Text="Caixa de Mensagens" Font-Size="Large" ></asp:Label>
                    </div>
                </div>

            </div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">

                <ContentTemplate>
                    <div class="box-body">
                        <div class="row" runat="server" id="divpainel">
                            <div class="col-lg-2 col-sm-4 col-md-4 " runat="server" id="divnovamensagem">
                                <asp:Button ID="btnnova" runat="server" Text="Nova Mensagem" CssClass="btn btn-primary " />
                                <asp:HiddenField ID="hdcaixasel" runat="server" />
                            </div>
                        </div>
                        <div class="row" runat="server" id="divnova">
                            <div class="col-lg-12 col-sm-12 col-md-12 ">
                                <div class="box box-warning" style="margin-left: 1rem; margin-right: 1rem">

                                    <div class="box-header ">
                                        <b>
                                            <asp:Label id="lblfrasenovamensagem" runat="server" text="Nova Mensagem"></asp:Label>
                                            </b>
                                    </div>
                                    <div class="box-body">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnanexar" />
                                           <%--    <asp:AsyncPostBackTrigger ControlID="TextBox1_HtmlEditorExtender" EventName="OnImageUploadComplete" />--%>
                                            </Triggers>
                                            <ContentTemplate>

                                                 <div class="row" runat="server" id="divremetente">
                                                    <div class="col-lg-12">
                                                        Remetente
                                                        <br />
                                                        <asp:Label ID="lbllremetente" runat="server" CssClass="form-control "></asp:Label>
                                                    </div>
                                                    </div>
                                                <div class="row" style="margin-left:1rem;margin-right:1rem;">
                                                    <div class="col-lg-12 text-center ">
                                                        <asp:Label ID ="lblseparar" Text="Separar os endereços usando ponto-e-vírgula (;). Clique em Buscar para listar os usuários do sistema." runat="server" ForeColor="Red"></asp:Label>
                                                    </div>
                                                    </div> 
                                                <div class="row">
                                                    <div class="col-lg-5">
                                                        
                                                            <asp:Label ID="Label1" runat="server" Text="E-mail destinatário"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtemail" runat="server" placehorder="Digite os e-mails separados por ponto-e-virgula" class="form-control"></asp:TextBox>
                                                              <ajaxToolkit:AutoCompleteExtender TargetControlID="txtemail" ID="autousuario" runat="server" ServiceMethod="getdestinatarios" MinimumPrefixLength="2" CompletionSetCount="10"    EnableCaching="false"  CompletionInterval ="1"   FirstRowSelected = "true"> </ajaxToolkit:AutoCompleteExtender>
                                                        <div class="row">
                                                            <div class="col-lg-12 ">
                                                                 <asp:HiddenField ID="hdemails" runat="server" />
                            <asp:Repeater ID="rptdestinatarios" runat="server">
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblusuariorpt" CssClass="label label-success " Text='<%#Bind("usuario") %>'></asp:Label><asp:LinkButton ID="btnexcluiremailrpt" runat="server" CommandName="excluir" CssClass="badge " style="background-color:red;color:white  ">  <i class="fa fa-trash-o" ></i> </asp:LinkButton>
                                </ItemTemplate>
                            </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-1 text-center">
                                                        <br />
                                                        <asp:LinkButton ID="btnenderecos" runat="server" CssClass="btn btn-success "> <asp:Image runat="server" ImageUrl="~/img/add.png" style="width:2rem;height:2rem;"/>  Adicionar</asp:LinkButton>

                                                    </div>
                                                     
                                                     <div class="col-lg-5">
                                                        
                                                            <asp:Label ID="Label3" runat="server" Text="Cópia oculta"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtcco" runat="server" placehorder="Digite os e-mails separados por ponto-e-virgula" class="form-control"></asp:TextBox>
                                                              <ajaxToolkit:AutoCompleteExtender TargetControlID="txtcco" ID="AutoCompleteExtender1" runat="server" ServiceMethod="getdestinatarios" MinimumPrefixLength="2" CompletionSetCount="10"    EnableCaching="false"  CompletionInterval ="1"   FirstRowSelected = "true"> </ajaxToolkit:AutoCompleteExtender>
                                                        <div class="row">
                                                            <div class="col-lg-12 ">
                                                                 <asp:HiddenField ID="hdcopia" runat="server" />
                            <asp:Repeater ID="Repeater1" runat="server">
                                <ItemTemplate >
                                    <asp:Label runat="server" ID="lblusuarioccrpt" CssClass="label label-success " Text='<%#Bind("usuario") %>'></asp:Label><asp:LinkButton ID="btnexcluiremailrpt" runat="server" CommandName="excluir" CssClass="badge " style="background-color:red;color:white  ">  <i class="fa fa-trash-o" ></i> </asp:LinkButton>
                                </ItemTemplate>
                            </asp:Repeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row" runat="server" id="divaddusers">
                                                    <div class="box box-danger" style="margin-left: 1rem; margin-right: 1rem">

                                                        <div class="box-header">
                                                            <div class="row">
                                                            <div class="col-sm-11">
                                                            <center>Adicionar Destinatários</center>
                                                                </div> 
                                                            <div class="col-sm-1">
                                                             <asp:LinkButton ID="btnFecharViaEnergia" runat="server" CssClass="pull-right" >
                                             <i class="fa fa-eye-slash"></i>
                                                                
                                    </asp:LinkButton>
                                                                </div> </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <div class="col-lg-4">
                                                                Usuário
                                                        <br />
                                                                <asp:DropDownList ID="cbousuario" runat="server" CssClass="form-control"></asp:DropDownList>
                                                            </div>
                                                            <div class="col-lg-4">


                                                                <asp:Button ID="btnadddest" CssClass="btn btn-primary col-lg-4" Text="Destinatário" runat="server" />
                                                                <br />
                                                                <br />
                                                                <asp:Button ID="btnadddestoculto" CssClass="btn btn-primary col-lg-4" runat="server" Text="Oculto" />



                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label2" runat="server" Text="Assunto do e-mail"></asp:Label>
                                                            <br />
                                                            <asp:TextBox ID="txtassunto" class="form-control" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <asp:Label ID="Label5" runat="server" Text="Anexar Arquivo"></asp:Label>
                                                                <br />
                                                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                                            </div>
                                                            <div class="row">
                                                                <br />
                                                                <asp:GridView ID="gradeanexos" runat="server" AutoGenerateColumns="False" CellPadding="4" Width="100%" ShowHeaderWhenEmpty="True" EmptyDataText="Nenhum arquivo encontrado !" CssClass="table table-hover" GridLines="None">
                                                                    <Columns>
                                                                        <asp:TemplateField >
                                                                            <ItemTemplate>
                                                                                 <span class="badge" style="background: #1E90FF">
                                    <asp:Label ID="lblitem" runat="server" ></asp:Label>
                                </span>
                                                                                <asp:HiddenField ID="hdcaminho" runat="server" value='<%#Bind("caminho") %>'/>
                                                                                  <asp:HiddenField ID="hdnrseqarquivo" runat="server" value='<%#Bind("nrseqarquivo") %>'/>
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:BoundField DataField="arquivo" HeaderText="Arquivo"></asp:BoundField>

                                                                         <asp:ButtonField ButtonType="Image" runat="server" CommandName="Baixar"  ImageUrl="~\img\down01.png">
                                                                            <ControlStyle Width="27px" Height="27px"></ControlStyle>
                                                                            <ItemStyle HorizontalAlign="Center" />


                                                                        </asp:ButtonField>

                                                                        <asp:ButtonField ButtonType="Image" runat="server" CommandName="Excluir"  ImageUrl="~\img\imgnao.png">
                                                                            <ControlStyle Width="27px" Height="27px"></ControlStyle>
                                                                            <ItemStyle HorizontalAlign="Center" />


                                                                        </asp:ButtonField>

                                                                    </Columns>

                                                                   
                                                                </asp:GridView>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <asp:Button ID="btnanexar" runat="server" Text="Anexar" CssClass="btn btn-warning" />
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row" id="divprepararmensagem" runat="server">
                                                    <div class="col-lg-12 text-center">
                                                        <br />
                                                        <asp:LinkButton  ID="btnprepararmensagem" runat="server" CssClass="btn btn-success "  > <asp:Image runat="server" style="height:2rem;width:2rem" ImageUrl="~/img/alterarverde.png" /> Gerar Mensagem de envio</asp:LinkButton> 
                                                    </div>
                                                </div>
                                                <div class="row" id="divcorpo" runat="server">
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label4" runat="server" Text="Corpo do e-mail"></asp:Label>
                                                            <br />
                                                            <%-- <iframe src=""--%>

                                                            <asp:TextBox ID="txtcorpo" runat="server" Height="400px" Width="100%" TextMode="MultiLine" Columns="50" 
        Rows="10" ClientIDMode="Static"></asp:TextBox>
                             <ajaxToolkit:HtmlEditorExtender ID="TextBox1_HtmlEditorExtender" runat="server"  TargetControlID="txtcorpo"   OnImageUploadComplete="saveimage" ClientIDMode="Static" EnableSanitization="false">
                                 <Toolbar> 
                <ajaxToolkit:Undo />
                <ajaxToolkit:Redo />
                <ajaxToolkit:Bold />
                <ajaxToolkit:Italic />
                <ajaxToolkit:Underline />
                <ajaxToolkit:StrikeThrough />
                <ajaxToolkit:Subscript />
                <ajaxToolkit:Superscript />
                <ajaxToolkit:JustifyLeft />
                <ajaxToolkit:JustifyCenter />
                <ajaxToolkit:JustifyRight />
                <ajaxToolkit:JustifyFull />
                <ajaxToolkit:InsertOrderedList />
                <ajaxToolkit:InsertUnorderedList />
                <ajaxToolkit:CreateLink />
                <ajaxToolkit:UnLink />
                <ajaxToolkit:RemoveFormat />
                <ajaxToolkit:SelectAll />
                <ajaxToolkit:UnSelect />
                <ajaxToolkit:Delete />
                <ajaxToolkit:Cut />
                <ajaxToolkit:Copy />
                <ajaxToolkit:Paste />
                <ajaxToolkit:BackgroundColorSelector />
                <ajaxToolkit:ForeColorSelector />
                <ajaxToolkit:FontNameSelector />
                <ajaxToolkit:FontSizeSelector />
                <ajaxToolkit:Indent />
                <ajaxToolkit:Outdent />
                <ajaxToolkit:InsertHorizontalRule />
                <ajaxToolkit:HorizontalSeparator />
                <ajaxToolkit:InsertImage />

                </Toolbar>


                             </ajaxToolkit:HtmlEditorExtender>
                                                            <%--<asp:TextBox ID="txtcorpo" runat="server" Height="420px" class="form-control" AutoPostBack="True"></asp:TextBox>
                                   <ajaxToolkit:HtmlEditorExtender ID="TextBox1_HtmlEditorExtender" runat="server" BehaviorID="TextBox1_HtmlEditorExtender" TargetControlID="txtcorpo" EnableSanitization="false">
                            </ajaxToolkit:HtmlEditorExtender>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="divbotoesfinalizarmensagem" runat="server">
                                                    <div class="col-lg-12 ">
                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                        <Triggers >
                                                            <asp:PostBackTrigger ControlID="gradeanexos" />
                                                        </Triggers>
                                                        <ContentTemplate>
                                                            <div class="row">

                                                                <div class="col-lg-2 text-center" runat="server" id="divenviar">
                                                                    
                                 
                        <br />
                                    <asp:Button ID="btnEnviar" runat="server" Text="Enviar" CssClass="btn btn-warning col-lg-12" />
                                
                                                                </div>
                                                                 <div class="col-lg-2 text-center" runat="server" id="divresponder">
                                                                   
                                 
                        <br />
                                    <asp:Button ID="btnresponder" runat="server" Text="Responder" CssClass="btn btn-warning col-lg-12" />
                                
                                                                </div>
                                                                 <div class="col-lg-2 text-center" runat="server" id="divencaminhar">
                                                                   
                                 
                        <br />
                                    <asp:Button ID="btnEncaminhar" runat="server" Text="Encaminhar" CssClass="btn btn-success col-lg-12" />
                                
                                                                </div>
                                                                <div class="col-lg-2 text-center">
                                                                  
                                 
                        <br />
                                    <asp:Button ID="btncancelnova" runat="server" Text="Cancelar" CssClass="btn btn-danger col-lg-12" />
                              





                                                                </div>

                                                            </div>
                                                            <div id="alerta1" style="display: none;">


                                                                <div class="alert alert-danger alert-dismissable">
                                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                                    <strong>Atenção:</strong>
                                                                    <asp:Label ID="lblerro" runat="server" Text=""></asp:Label>

                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                        </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row" runat="server" id="divlista">
                        <div class="col-lg-12 col-sm-12 col-md-12 ">
                          
                            <div class="row" style="margin-left:1rem;margin-right:1rem;">
                                <div class="col-lg-12 ">
                                <div class="box box-primary " >
                                    <div class="box-body ">

                                   
                                <div class="col-lg-2 col-sm-2 col-md-2 " style="margin-right: 1rem; margin-left: 1rem;">
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 col-md-12">
                                            <br />
                                            <asp:LinkButton ID="btncaixa_entrada" runat="server" CssClass="btn btn-primary col-lg-12">
                             <i class="fa fa-inbox"></i>   Entrada
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 col-md-12">
                                            <br />
                                            <asp:LinkButton ID="btncaixa_enviadas" runat="server" CssClass="btn btn-success col-lg-12">
                              <i class="fa fa-arrow-up" ></i>  Enviadas
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 col-md-12">
                                            <br />
                                            <asp:LinkButton ID="btncaixa_rascunho" runat="server" CssClass="btn btn-default col-lg-12">
                             <i class="fa fa-bookmark-o" ></i>   Rascunhos
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 col-md-12">
                                            <br />
                                            <asp:LinkButton ID="btncaixa_excluidas" runat="server" CssClass="btn btn-danger col-lg-12">
                             <i class="fa fa-trash" ></i>   Excluídas
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-8 col-sm-8 col-md-8 ">
                                    <asp:GridView ID="Grade" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="table" EmptyDataText="Nenhuma mensagem encontrada!" ShowHeaderWhenEmpty="true">

                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Abrir" ImageUrl="~/img/abrir02.png">
                                                 <ControlStyle Width="27px" Height="27px"></ControlStyle>
                                            </asp:ButtonField>

                                            <asp:TemplateField HeaderText="Remetente">

                                                <ItemTemplate>
                                                    <div style="width: 150px">
                                                      <asp:HiddenField ID="hdnrseq" runat ="server" Value ='<%# Bind("nrseq") %>' />
                                                         <asp:HiddenField ID="hdnrseqdest" runat ="server" Value ='<%# Bind("nrseqdest") %>' />
                                                          <asp:HiddenField ID="hdlida" runat ="server" Value ='<%# Bind("lida") %>' />
                                                        
                                                        <center>
                                                              <asp:Label ID="lblremetente" runat="server" Text='<%# Bind("nomeremetente") %>'></asp:Label>
                                                        <%--</a>--%>
                                                        <br />
                                        <asp:HiddenField runat="server" ID="hdimagemremetente" Value = '<%# Bind("imagemperfilremetente") %>'/>
               <asp:image ID="imagemremetente" runat="server" ImageUrl="~/img/amigo.png" ClientIDMode="static" class="img-responsive img-circle" Style="border-color:blue;border:solid;" Width="50px" Height="50px"></asp:image> 
                                                      </center>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Destinatário">

                                                <ItemTemplate>
                                                    <div style="width: 150px">
                                                       
                                                        <center>
                                                             <asp:Label ID="lbldestinatario" runat="server" Text='<%# Bind("nomedestinatario") %>'></asp:Label>
                                                        <%--</a>--%>
                                                        <br />
                                        <asp:HiddenField runat="server" ID="hdimagemdestinatario" Value = '<%# Bind("imagemperfildestinatario") %>'/>
               <asp:image ID="imagemdestinatario" runat="server" ImageUrl="~/img/amigo.png" ClientIDMode="static" class="img-responsive img-circle" Style="border-color:blue;border:solid;" Width="50px" Height="50px"></asp:image> 
                                                      </center>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Data" DataField="dtcad" />
                                            <asp:BoundField HeaderText="Assunto" DataField="assunto" />
                                            <asp:TemplateField >
                                                <ItemTemplate >
                                                    <asp:Image ID="imganexosgrade" runat="server" ImageUrl="~/img/iconeclips.png" style="width:2rem;height:2rem;"/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:ButtonField ButtonType="Image" CommandName="excluir" ImageUrl="~/img/imgnao.png">
                                                 <ControlStyle Width="27px" Height="27px"></ControlStyle>

                                            </asp:ButtonField>


                                        </Columns>
                                    </asp:GridView>
                                </div>
 </div>
                                </div>
                            </div>
                                </div>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 ">
                                    <hr />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 ">
                                    <%--<asp:Label runat="server" ID="lblaviso" Text="Algum texto aqui !"></asp:Label>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>


</asp:Content>


﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="erro.aspx.vb" Inherits="erro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
body, html {
    height: 100%;
    margin: 0;
}

.bg {
    /* The image used */
    background-image: url('http://www.smartcodesolucoes.com.br/img/erro.png');

    /* Full height */
    height: 100%; 

    /* Center and scale the image nicely */
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.windowstatusmodelo {
            border-width: 2px;
            border-style: solid;
            border-color: black;
            background-color: rgba(0, 0, 0, 0.4);
            
            width: 800px;
            height: 300px;
            position: absolute;
            margin-top: 10px;
            margin-left: 10px;
            left: 400px;
            top: 150px;
            z-index: 9999;
            text-align: left;
            -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
        }
</style>
</head>
   
<body class="bg">  
    <form id="form1" runat="server">
    <div class="windowstatusmodelo">
    
        <br />
        <br />
        <div style="margin-left:100px">
        <asp:TextBox ID="txtmensagem" runat="server" class="form-control" BackColor="Transparent" ForeColor="GreenYellow" Font-Size="X-Large" BorderStyle="none" Width="600px" Height="150px" TextMode="MultiLine"  style="text-align:center;"></asp:TextBox>
    </div>
    </div>
    </form>
</body>
</html>

﻿Imports clssessoes
Imports System.IO
Imports clsSmart

Partial Class upload_documentos
    Inherits System.Web.UI.Page


    Private Sub upload_documentos_Load(sender As Object, e As EventArgs) Handles Me.Load
        carregagradeacoes()
    End Sub


    Private Sub btnupload_Click(sender As Object, e As EventArgs) Handles btnupload.Click

        Dim sImageName As String = ""
        Dim descriarquivo As String = ""
        Dim sImagePathImages As String = Server.MapPath("~\balancetes")
        'nome arquivo 


        If (upload01.HasFile) Then

            descriarquivo = "\ARQ_" & txtnome.Text & cbotipo.Text & Path.GetExtension(upload01.FileName)
            sImageName = sImagePathImages & descriarquivo

            ' Dim sFileExt As String = Path.GetExtension(upload01.FileName)
            ' m_sImageNameUserUpload = sImageName + sFileExt
            ' m_sImageNameGenerated = Path.Combine(sImagePathImages, m_sImageNameUserUpload)

            upload01.PostedFile.SaveAs(sImageName)
        End If

        ' Início do salvar\
        Dim xBalancete As New clsbalancete

        xBalancete.Nome = txtnome.Text
        xBalancete.Arquivo = descriarquivo
        xBalancete.Obs = txtobs.Text
        xBalancete.Tipo = cbotipo.Text

        If Not xBalancete.novosalvar Then
            sm("swal({title: 'Atenção!',text: '" & xBalancete.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        limpar()

    End Sub

    Public Sub carregagradeacoes(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbdocumentoanaplab where ativo = '1' ")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        grade.DataSource = tb1
        grade.DataBind()

    End Sub



    Private Sub limpar()
        txtnome.Text = ""
        txtarquivo.Text = ""
        txtativo.Text = ""
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grade.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grade.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim arquivo As HiddenField = row.FindControl("hdarquivo")


        If e.CommandName = "excluir" Then

            Dim xDocassociados As New clsbalancete

            xDocassociados.Nrseq = nrseq.Value

            If Not xDocassociados.excluir() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel excluir',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'successo',  title: 'Excluido com Sucesso',  showConfirmButton: false,  timer: 2000})")

            carregagradeacoes()

        End If
    End Sub
End Class

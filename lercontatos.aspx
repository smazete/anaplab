﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="lercontatos.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="lercontatos" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updateemails" runat="server">
        <ContentTemplate>

            <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
                
                <div class="box box-header">
                    <b>Ler mensagem de contato</b>
                </div>
                <%-- carrega grade de  mensagem --%>
                <%-- recebe mensagem --%>
                <div class="box-body">
                    <div class="row">

                        <div class="panel panel-info">
                            <div class="panel panel-body">
                                <div class="col-lg-6">
                                    
                <div class="row">
                    <br>
                    <asp:GridView ID="gradecontato" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma mensagem de contato enviada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>' CssClass="badge"></asp:Label>
                                    <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                           <asp:LinkButton ID="usar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="usar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="nrseq" HeaderText="Código" />
                            <asp:BoundField DataField="assunto" HeaderText="assunto" />
                            <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                            <asp:BoundField DataField="dtcad" HeaderText="dtcad" />

                        </Columns>
                    </asp:GridView>
                </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="row"><asp:Label ID="lbldata" Text="00/00/0000" runat="server" CssClass="close" /></div> 
                                            <asp:Label runat="server">NOME COMPLETO</asp:Label><br />
                                        
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtnome" />
                                    </div>
                                    <div class="row">
                                        <asp:Label runat="server">ASSUNTO</asp:Label><br />
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtassunto" />
                                    </div>
                                    <div class="row">
                                        <asp:Label runat="server">E-MAIL</asp:Label><br />
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtemail" />
                                    </div>
                                    <div class="row">
                                        <asp:Label runat="server">MENSAGEM</asp:Label><br />
                                        <asp:TextBox Style="height: 136px;" CssClass="form-control" runat="server" ID="txtdescricao" TextMode="MultiLine" />
                                    </div>
                                    <br />
                                    <div class="row">
                                        <center>
                                <asp:LinkButton cssclass="btn btn-primary" Text="Responder" runat="server"  id="btnenviar" visible="false"/>
                                 </center>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data

Public Class clsResultados

    'Desenvolvedor: Claudio Smart
    'Data inicial :  23/08/2019
    'Data Atualização :  23/08/2019 - por Claudio

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _grade As New Data.DataTable
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _descricao As String
    Dim _usarem As String
    Dim _peso As Integer = 0
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo As Boolean
    Dim _userexclui As String
    Dim _dtexclui As Date
    Dim _titulolegenda As String
    Dim _legenda As String


    Public Sub New()
        versessao()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property

    Public Property Usarem As String
        Get
            Return _usarem
        End Get
        Set(value As String)
            _usarem = value
        End Set
    End Property

    Public Property Peso As Integer
        Get
            Return _peso
        End Get
        Set(value As Integer)
            _peso = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public ReadOnly Property Grade As DataTable
        Get

            carregargrade()

            Return _grade
        End Get

    End Property

    Public Property Titulolegenda As String
        Get
            Return _titulolegenda
        End Get
        Set(value As String)
            _titulolegenda = tratatexto(value, False, 50)
        End Set
    End Property

    Public Property Legenda As String
        Get
            Return _legenda
        End Get
        Set(value As String)
            _legenda = tratatexto(value)
        End Set
    End Property
End Class
Partial Public Class clsResultados



    Public Function carregar() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Informe um numero valido !"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbresultados where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Nenhum resultado localizado !"
                Return False
            End If

            _legenda = tb1.Rows(0)("legenda").ToString
            _peso = tb1.Rows(0)("peso").ToString
            _descricao = tb1.Rows(0)("descricao").ToString
            _titulolegenda = tb1.Rows(0)("titulolegenda").ToString


            Return True
        Catch excarregar As Exception
            _mensagemerro = excarregar.Message
            Return False
        End Try

    End Function

    Public Function adicionar() As Boolean
        Try
            If _descricao = "" Then
                _mensagemerro = "Informe uma descrição válida !"
                Return False
            End If
            If _peso = 0 Then
                _mensagemerro = "Digite uma pontuação válida !"
                Return False
            End If
            If _nrseq = 0 Then
                tb1 = tab1.conectar("select * from tbresultados where ativo = true and descricao = '" & _descricao & "'")
                If tb1.Rows.Count > 0 Then
                    _mensagemerro = "Essa descrição já existe! Informe uma descrição válida !"
                    Return False
                End If
                If tb1.Rows.Count > 0 Then
                    Dim xsql As String = "update tbresultados set ativo = true"
                    If _descricao <> "" Then
                        xsql &= ", descricao = '" & _descricao & "'"
                    End If
                    If _peso <> 0 Then
                        xsql &= ", peso = " & moeda(_peso)
                    End If
                    xsql &= " where ativo = true and descricao = '" & _descricao & "'"
                    tb1 = tab1.IncluirAlterarDados(xsql)
                Else
                    tb1 = tab1.IncluirAlterarDados("insert into tbresultados (descricao, usarem, peso, ativo, dtcad, usercad, legenda, titulolegenda) values ('" & _descricao & "', '" & _usarem & "', " & moeda(Peso) & ", true, '" & hoje() & "', '" & _usercad & "','" & _legenda & "','" & _titulolegenda & "')")
                End If


            Else
                tb1 = tab1.conectar("select * from tbresultados where nrseq = " & _nrseq)
                If tb1.Rows.Count > 0 Then
                    Dim xsql As String = "update tbresultados set ativo = true"
                    If _descricao <> "" Then
                        xsql &= ", descricao = '" & _descricao & "'"
                    End If
                    If _peso <> 0 Then
                        xsql &= ", peso = " & moeda(_peso)
                    End If
                    xsql &= " where nrseq = " & _nrseq
                    tb1 = tab1.IncluirAlterarDados(xsql)
                Else
                    tb1 = tab1.IncluirAlterarDados("insert into tbresultados (descricao, usarem, peso, ativo, dtcad, usercad, legenda, titulolegenda) values ('" & _descricao & "', '" & _usarem & "', " & moeda(Peso) & ", true, '" & hoje() & "', '" & _usercad & "','" & _legenda & "','" & _titulolegenda & "')")
                End If
            End If

            _mensagemerro = "Salvo com sucesso !"
            Return True
        Catch exadd As Exception
            _mensagemerro = exadd.Message
            Return False
        End Try
    End Function

    Public Function remover() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Informe nrseq válido para remover !"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbresultados set ativo = false, dtexclui =  '" & hoje() & "', userexclui =  '" & _usercad & "' where nrseq = " & _nrseq)

            Return True
        Catch exadd As Exception
            _mensagemerro = exadd.Message
            Return False
        End Try
    End Function
    Public Function carregargrade() As Boolean
        Try
            _grade = tab1.conectar("select * from tbresultados where ativo = true order by peso")
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class
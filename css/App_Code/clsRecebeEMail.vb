﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Net.Sockets
Imports System.Text
Imports System.IO
Public Class clsRecebeEMail
    Private Shared Server As TcpClient
    Private Shared NetStrm As NetworkStream
    Private Shared RdStrm As StreamReader
    Dim _usuario As String = "ceva@smartcodesolucoes.com.br"
    Public Property Usuario() As String
        Get
            Return _usuario
        End Get
        Set(ByVal value As String)
            _usuario = value
        End Set
    End Property

    Dim _Senha As String = "Sousmart@747791"
    Public Property Senha() As String
        Get
            Return _Senha
        End Get
        Set(ByVal value As String)
            _Senha = value
        End Set
    End Property

    Dim _Servidor As String = "smartcodesolucoes.com.br"
    Public Property Servidor() As String
        Get
            Return _Servidor
        End Get
        Set(ByVal value As String)
            _Servidor = value
        End Set
    End Property
    Public Function connect() As Integer
        Dim POP3Account As String
        POP3Account = _Servidor
        If POP3Account.Trim = "" Then Exit Function

        Server = New TcpClient(POP3Account.Trim, 110)
        NetStrm = Server.GetStream
        RdStrm = New StreamReader(Server.GetStream)


        Dim user As String
        user = _usuario
        Dim data As String = "USER " + user.Trim + vbCrLf
        Dim szData() As Byte = System.Text.Encoding.ASCII.GetBytes(data.ToCharArray())
        NetStrm.Write(szData, 0, szData.Length)
        Dim POPResponse As String
        POPResponse = RdStrm.ReadLine
        If POPResponse.Substring(0, 4) = "-ERR" Then
            MsgBox("Invalid user Name")
            Return -1
        End If
        Dim password As String
        password = _Senha
        data = "PASS " & password & vbCrLf
        szData = System.Text.Encoding.ASCII.GetBytes(data.ToCharArray())
        NetStrm.Write(szData, 0, szData.Length)
        POPResponse = RdStrm.ReadLine
        If POPResponse.Substring(0, 3).Contains("ERR") Then
            MsgBox("Invalid Passwprd")
            Return (-1)
        End If
        data = "STAT" + vbCrLf
        szData = System.Text.Encoding.ASCII.GetBytes(data.ToCharArray())
        NetStrm.Write(szData, 0, szData.Length)
        POPResponse = RdStrm.ReadLine
        If POPResponse.Substring(0, 3).Contains("ERR") Then
            MsgBox("could not log your in")
            Return -1
        End If
        data = "Stat" + vbCrLf
        szData = System.Text.Encoding.ASCII.GetBytes(data.ToCharArray())
        NetStrm.Write(szData, 0, szData.Length)
        POPResponse = RdStrm.ReadLine
        If POPResponse.Substring(0, 3).Contains("ERR") Then
            MsgBox("could not log your in")
            Return -1
        End If
        Dim parts() As String
        parts = POPResponse.Split(" ")
        Dim messages, totsize As Integer

        'messages = parts(3)
        messages = CInt(parts(1))
        Return messages



    End Function
    Public Function DeleteMessage(ByVal msgIndex As Integer)
        Dim data As String = "DELE " & msgIndex.ToString & vbCrLf
        Dim SzData() As Byte = System.Text.Encoding.ASCII.GetBytes(data.ToCharArray())
        NetStrm.Write(SzData, 0, SzData.Length)
        Dim tmpString As String = RdStrm.ReadLine()
        If tmpString.Substring(0, 3).Contains("ERR") Then
            MsgBox("Could Not Delete Message")
            Return (-1)
        Else
            Return 11
        End If
    End Function
    Public Function Quit()
        Dim data As String = "Quit " & vbCrLf
        Dim szData() As Byte = System.Text.Encoding.ASCII.GetBytes(data.ToCharArray())
        NetStrm.Write(szData, 0, szData.Length)
        Dim tmpString As String = RdStrm.ReadLine()
    End Function
    Public Structure Message
        Dim _From As String
        Dim _To As String
        Dim _Date As String
        Dim _Subject As String
        Dim _CC As String
        Dim _BCC As String
        Dim _Body As String
        Dim _Received As String
        Dim _codigo As String
    End Structure
    Public Function CreateFromText(ByVal strMessage As String) As Message
        Dim Mssg As New Message
        Dim brkPos As Integer
        Dim Header As String
        Dim Headers() As String
        Dim Body As String
        Dim vField As Object
        Dim strHeader As String
        Dim HeaderName As String
        Dim HeaderValue As String


        brkPos = InStr(1, strMessage, vbCrLf & vbCrLf)
        If brkPos Then
            Header = strMessage.Substring(0, brkPos - 1)
            Body = strMessage.Substring(brkPos + 1,
            strMessage.Length - Header.Length - 3)
            Mssg._Body = Body
        Else
            Throw New Exception("Invalid Message Format")
            Exit Function
        End If
        Headers = Split(Header, vbCrLf)
        Dim _header As String
        For Each _header In Headers
            brkPos = _header.IndexOf(":")
            If brkPos >= 0 Then
                HeaderName = _header.Substring(0, brkPos)
            Else
                HeaderName = ""
            End If
            HeaderValue = _header.Substring(brkPos + 1)
            If HeaderValue.Contains("#") Then
                Mssg._codigo = HeaderValue
            End If
            Select Case HeaderName.ToLower
                Case "#"
                    Mssg._codigo = HeaderValue
                Case "received"
                    Mssg._Received = HeaderValue
                Case "from"
                    Mssg._From = HeaderValue
                Case "to"
                    Mssg._To = HeaderValue
                Case "cc"
                    Mssg._CC = HeaderValue
                Case "bcc"
                    Mssg._BCC = HeaderValue
                Case "subject"
                    Mssg._Subject = HeaderValue
                Case "date"
                    Mssg._Date = HeaderValue
            End Select
        Next
        Return Mssg
    End Function
    Public Function GetMessage(ByVal msgindex As Integer) As String
        Dim tmpString As String
        Dim Data As String
        Dim SzData() As Byte
        Dim msg As String = ""
        Try
            Data = "RETR " & msgindex.ToString & vbCrLf
            SzData = System.Text.Encoding.UTF8.GetBytes(Data.ToCharArray())
            'SzData = System.Text.Encoding.ASCII.GetBytes(Data.ToCharArray())
            NetStrm.Write(SzData, 0, SzData.Length)
            tmpString = RdStrm.ReadLine()
            If Not tmpString.Substring(0, 3).Contains("ERR") Then
                While (tmpString <> ".")
                    Try
                        msg &= tmpString
                        tmpString = RdStrm.ReadLine
                    Catch ex As Exception

                    End Try

                End While
            End If
        Catch exc As InvalidOperationException
            ' MsgBox("Message Retrival Failed: " & vbCrLf & Err.ToString())
        End Try
        Return msg
    End Function
End Class

'Add new module in your project and add this code in to your module only copy and paste no need to do any of changes....

'----------------------------------------------------------------------------------



'Public Class FrmInbox

'        Inherits System.Windows.Forms.Form


'        Dim Server As TcpClient
'        Dim NetStrm As NetworkStream
'        Dim RdStrm As StreamReader



'        Private Sub CmdDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdDownload.Click

'            Dim i As Integer
'            Dim msg As POP3Message1.Message
'            Dim msgstring As String
'            Dim messages1 As Integer
'            Me.Cursor = Cursors.WaitCursor


'            messages1 = connect()

'            If messages1 = -1 Then
'                Me.Cursor = Cursors.Default
'                Exit Sub
'            End If
'            Dim originalCaption As String = Me.Text
'            For i = 1 To messages1
'                Me.Text = "Downloading Message " & i.ToString & "/" &
'                messages1.ToString
'                Dim msgitem As New ListViewItem
'                msgstring = GetMessage(i)
'                msg = CreateFromText(msgstring)
'                msgitem.Text = msg._From
'                msgitem.SubItems.Add(msg._Subject)
'                msgitem.SubItems.Add(msg._Date)
'                ListView1.Items.Add(msgitem)
'                TextBox1.AppendText(msg._Body & vbCrLf)
'            Next
'            Me.Text = originalCaption
'            Me.Cursor = Cursors.Default

'        End Sub



'    'Private Sub ListView1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'    '    Dim messages As Integer
'    '    'Dim objPOP3 As POP3Message
'    '    TextBox1.Text = GetMessage(ListView1.SelectedIndices(0) + 1)

'    'End Sub


'    'Private Sub ListView1_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ListView1.KeyUp
'    '        If e.KeyCode = Keys.Delete Then
'    '            If DeleteMessage(ListView1.SelectedIndices(0) + 1) >= 0 Then
'    '                ListView1.Items(ListView1.SelectedIndices(0)).Text = "DELETED"
'    '                ListView1.Items(ListView1.SelectedIndices(0)).SubItems.Clear()
'    '                MsgBox("Your Email Message is Deleted", MsgBoxStyle.Information, "Delete Message")
'    '            End If
'    '        End If
'    '    End Sub

'    'Private Sub CmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdClose.Click
'    '        Call Quit()
'    '        ListView1.Items.Clear()
'    '    End Sub

'End Class

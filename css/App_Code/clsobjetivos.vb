﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Public Class clsobjetivos

    'Desenvolvedor: Bryan Christen
    'Data inicial:23/08/2019
    'Data atualização:23/08/2019 'Alterado por:Bryan

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _descricao As String
    Dim _tipo As String ' 
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo As Boolean
    Dim _userexclui As String
    Dim _dtexclui As Date
    Dim _nrseqctrl As String
    Dim _conteudo As String

    Public Sub New()
        versessao()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Tipo As String
        Get
            Return _tipo
        End Get
        Set(value As String)
            _tipo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Conteudo As String
        Get
            Return _conteudo
        End Get
        Set(value As String)
            _conteudo = value
        End Set
    End Property
End Class
Partial Public Class clsobjetivos
    Public Function novo() As Boolean
        Try

            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into tbobjetivos (ativo, dtcad, usercad, nrseqctrl) values (false, '" & hoje() & "','" & Usercad & "', '" & wcnrseqctrl & "')")

            tb1 = tab1.conectar("select * from tbobjetivos where nrseqctrl = '" & wcnrseqctrl & "'")

            Nrseq = tb1.Rows(0)("nrseq").ToString
            Nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString

            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function
    Public Function salvar() As Boolean
        Try

            If _nrseq = 0 Then
                _mensagemerro = "Selecione um núemro válido !"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbobjetivos set descricao = '" & _descricao & "', ativo = true, tipo = '" & _tipo & "' where nrseq = " & _nrseq)

            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function
    Public Function excluir() As Boolean
        Try

            If Not carregar() Then
                Return False
            End If

            If _ativo Then
                tb1 = tab1.IncluirAlterarDados("update tbobjetivos set ativo = false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "' where nrseq = " & _nrseq)
            Else
                tb1 = tab1.IncluirAlterarDados("update tbobjetivos set ativo = true where nrseq = " & _nrseq)
            End If


            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

    Public Function carregar() As Boolean
        Try

            If _nrseq = 0 Then
                _mensagemerro = "Selecione um núemro válido !"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("select * from  tbobjetivos  where nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Curso não localizado !"
                Return False
            End If

            With tb1
                _descricao = .Rows(0)("descricao").ToString
                _tipo = .Rows(0)("tipo").ToString
                _ativo = .Rows(0)("ativo").ToString
                _usercad = .Rows(0)("usercad").ToString
                _dtcad = .Rows(0)("dtcad").ToString
            End With


            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function

End Class
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports clsSmart

Public Class clsEnvioEmail
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _mensagemerro As Boolean

    Dim _emailenviado As Boolean = False
    Dim tentativas As Integer = 0
    Dim mensagem As New MailMessage
    Dim arquivos As Attachment
    Dim origem As MailAddress
    Dim destino As MailAddress
    Dim envio As New SmtpClient
    Dim log As StreamWriter
    Dim _usuariosistema As String = ""
    Dim _usarssl As String
    Dim _mensagemerroemail As String
    Dim _local As String
    Dim _mypassword As String
    Dim _myuser As String
    Dim _gravarinterno As Boolean = False
    Dim arquivosanexados(100) As String
    Public Sub New()
        clssessoes.versessao()
        _usuariosistema = HttpContext.Current.Session("usuario")
        Try
            tb1 = tab1.conectar("select * from tbusuarios where usuariosistema = true")
            If tb1.Rows.Count > 0 Then
                _usuariosistema = tb1.Rows(0)("usuario").ToString
            End If
        Catch ex As Exception

        End Try


        _local = HttpContext.Current.Server.MapPath("~\logemails\logemail") & Directory.GetFiles(HttpContext.Current.Server.MapPath("~\logemails"), "*.log").Length + 1 & ".log"
        Try



            log = New StreamWriter(_local)
            log.WriteLine("Iniciou a montagem do email")

            mensagem.Attachments.Clear()
            mensagem.Bcc.Clear()
            mensagem.CC.Clear()
            mensagem.To.Clear()
        Catch ex As Exception
            Dim teste As String = ex.Message
        End Try
    End Sub

    Public Sub New(xusuario As String)
        _usuariosistema = xusuario
        Try
            tb1 = tab1.conectar("select * from tbusuarios where usuariosistema = true")
            If tb1.Rows.Count > 0 Then
                _usuariosistema = tb1.Rows(0)("usuario").ToString
            End If
        Catch exemail As Exception

        End Try


        _local = HttpContext.Current.Server.MapPath("~\logemails\logemail") & Directory.GetFiles(HttpContext.Current.Server.MapPath("~\logemails"), "*.log").Length + 1 & ".log"
        Try



            log = New StreamWriter(_local)
            log.WriteLine("Iniciou a montagem do email")

            mensagem.Attachments.Clear()
            mensagem.Bcc.Clear()
            mensagem.CC.Clear()
            mensagem.To.Clear()
        Catch ex As Exception
            Dim teste As String = ex.Message
        End Try
    End Sub

    Public Sub configpadrao()
        origem = New MailAddress("cevapad@smartcodesolucoes.com.br")
        mensagem.From = origem
        _myuser = "cevapad@smartcodesolucoes.com.br"
        _mypassword = "Sousmart@747791"
        envio.Port = 587
        envio.Host = "smtp.smartcodesolucoes.com.br"
        envio.UseDefaultCredentials = False
        envio.Credentials = New NetworkCredential(_myuser, _mypassword)
        log.WriteLine("Configurou as credenciais : usuario:" & _myuser & " senha: " & _mypassword)

    End Sub
    Public Property mensagemerro() As String
        Set(ByVal value As String)
            log.WriteLine("Apresentou erro :" & _mensagemerroemail)
            _mensagemerroemail = value
        End Set
        Get
            Return _mensagemerroemail
        End Get
    End Property


    Public WriteOnly Property responderpara() As String
        Set(ByVal value As String)
            log.WriteLine("Definiu o caminho de responder para : " & value)
            destino = New MailAddress(value)
            mensagem.ReplyToList.Add(destino)
        End Set

    End Property

    Public Sub Credenciais(ByVal myuser As String, ByVal mypassword As String, ByVal mydominio As String)
        _myuser = myuser
        _mypassword = mypassword
        envio.UseDefaultCredentials = False
        envio.Credentials = New NetworkCredential(myuser, mypassword)
        log.WriteLine("Configurou as credenciais : usuario:" & myuser & " senha: " & mypassword)
    End Sub
    Public WriteOnly Property ConfigPorta() As String
        Set(ByVal value As String)
            log.WriteLine("Configurou a porta : " & value)
            envio.Port = value
        End Set
    End Property
    Public WriteOnly Property AdicionaDestinatariosocultos() As String
        Set(ByVal value As String)
            If Not value Is Nothing AndAlso value.Contains("@") Then
                destino = New MailAddress(value)
                log.WriteLine("Adicionou destinat�rio oculto : " & value)
                mensagem.Bcc.Add(destino)
            End If
        End Set
    End Property
    Public WriteOnly Property usarSSL() As Boolean

        Set(ByVal value As Boolean)
            log.WriteLine("Usar SSL : " & IIf(value, "Sim", "N�o"))
            If value = True Then

                '    envio.UseDefaultCredentials = True
                envio.EnableSsl = True
            Else
                log.WriteLine("Adicionou destinat�rio : " & value)
                '  envio.UseDefaultCredentials = True
                envio.EnableSsl = False
            End If
        End Set
    End Property
    Public Property emailenviado() As Boolean
        Set(ByVal value As Boolean)
            _emailenviado = value
        End Set
        Get
            Return _emailenviado
        End Get
    End Property



    Public WriteOnly Property ConfigSMTP() As String
        Set(ByVal value As String)
            log.WriteLine("Adicionou o config SMTP : " & value.ToLower)
            envio.Host = value.ToLower
            'client.UseDefaultCredentials = true;
            'client.EnableSsl = True

        End Set
    End Property
    Public Property AdicionaAssunto() As String
        Set(ByVal value As String)
            Dim valortemp As String = value
            Dim meuservidor As New clsBanco
            If meuservidor.servidor = "191.252.3.96" Then
                valortemp = "E-MAIL TEST! PLEASE IGNORE THIS MESSAGE! THANKS! " & valortemp
            End If
            log.WriteLine("Adicionou o assunto : " & value)
            mensagem.Subject = valortemp
        End Set
        Get
            Return mensagem.Subject
        End Get
    End Property
    Public Property AdicionaMensagem() As String
        Get
            Return mensagem.Body
        End Get
        Set(ByVal value As String)
            log.WriteLine("Adicionou a mensagem : " & value)
            mensagem.Body = value
        End Set

    End Property
    Public WriteOnly Property AdicionaAnexos() As String
        Set(ByVal value As String)
            'log.WriteLine("Adicionou o anexo : " & value)
            'For x As Integer = 0 To arquivosanexados.Count - 1
            '    If arquivosanexados(x) = "" OrElse arquivosanexados(x) Is Nothing Then
            '        arquivosanexados(x) = mARQUIVO(value)
            '        Try
            '            My.Computer.Network.UploadFile(value, "ftp://emails.smartcodesolucoes.com.br/logemails/" & mARQUIVO(value), "usrsmartftp", "Codesmart@747791")
            '        Catch ex As Exception
            '            Dim erro As String = ex.Message
            '        End Try

            '        Exit Property
            '    End If
            'Next
            'My.Computer.Network.UploadFile(value, "ftp://emails.smartcodesolucoes.com.br/logemails" & "/" & mARQUIVO(value), "usrsmartftp", "Codesmart@747791")
            If File.Exists(value) Then
                Dim wcvalor As New Attachment(value)
                mensagem.Attachments.Add(wcvalor)
            End If
            'arquivosanexados.Add(value)
        End Set
    End Property
    Dim _QtdAnexos As Integer

    Public ReadOnly Property QtdAnexos() As Integer
        Get
            Return mensagem.Attachments.Count
        End Get

    End Property

    'Dim _listaanexos As List

    'Public ReadOnly Property QtdAnexos() As Integer
    '    Get
    '        Return mensagem.Attachments.Count
    '    End Get

    'End Property

    Public WriteOnly Property AdicionaDestinatarios() As String
        Set(ByVal value As String)
            If Not value Is Nothing AndAlso value.Contains("@") Then
                Try
                    log.WriteLine("Adicionou o destinat�rio : " & value)
                    destino = New MailAddress(value)
                    mensagem.To.Add(destino)
                Catch ex As Exception

                End Try

            End If
        End Set
    End Property
    Public WriteOnly Property AdicionarCopiaPara() As String
        Set(ByVal value As String)
            log.WriteLine("Adicionou o destinat�rio c�pia : " & value)
            destino = New MailAddress(value)
            mensagem.CC.Add(destino)
        End Set
    End Property
    Public Property EhHTML() As Boolean
        Set(ByVal value As Boolean)
            log.WriteLine("O e-mail � HTML ? " & IIf(value, "Sim", "N�o"))
            mensagem.IsBodyHtml = value
        End Set
        Get
            Return mensagem.IsBodyHtml
        End Get
    End Property
    Public WriteOnly Property AdicionaRemetente() As String
        Set(ByVal value As String)
            log.WriteLine("Adicionou o remetente : " & value)
            origem = New MailAddress(value)
            mensagem.From = origem
        End Set
    End Property

    Public Property Gravarinterno As Boolean
        Get
            Return _gravarinterno
        End Get
        Set(value As Boolean)
            _gravarinterno = value
        End Set
    End Property

    Public Property Mensagemerro1 As Boolean
        Get
            Return _mensagemerro
        End Get
        Set(value As Boolean)
            _mensagemerro = value
        End Set
    End Property

    Public Function EnviarEmail() As Boolean
        'GoTo novamente

        'Dim webmail As New envioemails.email
        'Dim destinatarios(mensagem.To.Count + mensagem.CC.Count + mensagem.Bcc.Count) As String
        'Dim destinatariosocultos(mensagem.Bcc.Count) As String
        'Dim arquivos(mensagem.Attachments.Count) As String

        'For x As Integer = 0 To mensagem.To.Count - 1
        '    destinatarios(destinatarios.Count - 1) = mensagem.To(x).Address
        'Next
        'For x As Integer = 0 To mensagem.CC.Count - 1
        '    destinatarios(destinatarios.Count - 1) = mensagem.CC(x).Address
        'Next
        'For x As Integer = 0 To mensagem.Bcc.Count - 1
        '    destinatariosocultos(destinatariosocultos.Count - 1) = mensagem.Bcc(x).Address
        'Next
        ''For x As Integer = 0 To mensagem.Attachments.Count - 1
        ''    arquivosanexados(arquivosanexados.Count - 1) = mensagem.Attachments(x).Name
        ''Next

        'Dim resultado As Boolean = webmail.envioemail(_myuser, _mypassword, envio.Host, mensagem.Subject, mensagem.Body, destinatarios, destinatariosocultos, arquivosanexados)
        'log.WriteLine("Resultado do envio: " & IIf(resultado, "Enviado", "N�o enviado"))




        'log.Close()

        'Return resultado

        ' Exit Function
        emailenviado = False
        Dim resultado As Boolean = False
        envio.Port = 587
novamente:
        Try
            mensagem.Bcc.Add("alertas@smartcodesolucoes.com.br")
            'envio.EnableSsl = True
            envio.Send(mensagem)
            emailenviado = True
            log.WriteLine("E-mail enviado !")
        Catch ex As ArgumentException
            log.WriteLine("E-mail n�o enviado ! (" & ex.Message & ")")
            mensagemerro = ex.Message
            emailenviado = False
        Catch ex2 As InvalidOperationException
            log.WriteLine("E-mail n�o enviado ! (" & ex2.Message & ")")
            mensagemerro = ex2.Message
        Catch ex3 As SmtpException
            log.WriteLine("E-mail n�o enviado ! (" & ex3.Message & ")")
            mensagemerro = ex3.Message
            'Catch ex4 As SmtpFailedRecipientException
            '    log.WriteLine("E-mail n�o enviado ! (" & ex4.Message & ")")
            '    mensagemerro = ex4.Message

            'Select Case tentativas
            '    Case Is = 0

            '        tentativas += 1
            '        envio.Port = 587
            '        GoTo novamente
            '    Case Is = 1

            '        tentativas += 1
            '        envio.Port = 496
            '        GoTo novamente
            '    Case Else
            '        '   enviarcopiaerro(ex.Message, "Envio de e-mails")
            'End Select

            emailenviado = False
        End Try

        If (emailenviado AndAlso _gravarinterno) AndAlso _usuariosistema <> "" Then
            Dim xmens As New clscaixamensagens
            xmens.Remetente = _usuariosistema
            xmens.Origemdeemailenviado = True
            xmens.Assunto = AdicionaAssunto
            xmens.Corpo = AdicionaMensagem
            For x As Integer = 0 To mensagem.Attachments.Count - 1
                Dim xdocs As New clscaixamensagens_anexos
                xdocs.Arquivo = mensagem.Attachments(x).Name
                xdocs.Caminho = HttpContext.Current.Server.MapPath("~") & "\arquivos\temp"
                xdocs.Nrseqmensagem = -1
                xmens.anexos.Add(xdocs)
            Next


            For x As Integer = 0 To mensagem.To.Count - 1
                Dim xdest As New clscaixamensagens_destinatarios
                xdest.Usuario = mensagem.To(x).Address
                xmens.destinatarios.Add(xdest)
            Next
            For x As Integer = 0 To mensagem.CC.Count - 1
                Dim xdest As New clscaixamensagens_destinatarios
                xdest.Usuario = mensagem.To(x).Address
                xmens.destinatarios.Add(xdest)
            Next

            xmens.enviar()
        End If

        log.WriteLine("Resultado do envio: " & IIf(emailenviado, "Enviado", "N�o enviado:" & mensagemerro))
        log.Close()
        Return emailenviado

    End Function
    Public Function mensagenspadroes() As Boolean
        Try

            tb1 = tab1.conectar("select * from vwconfig_acoes where interno = 'COMMENT'")

            If tb1.Rows.Count = 0 Then
                Mensagemerro1 = "No destinatary found !"
                Return False
            End If

            If Not System.IO.File.Exists(tb1.Rows(0)("arquivo").ToString) Then
                Mensagemerro1 = "No message found !"
                Return False
            End If

            For x As Integer = 0 To tb1.Rows.Count - 1
                Dim wcemail As String = ""
                Select Case tb1.Rows(x)("usuario").ToString
                    Case Is = ""
                        wcemail = HttpContext.Current.Session("email")

                    Case Else
                        wcemail = tb1.Rows(x)("email").ToString
                End Select
                If validaemail(tb1.Rows(x)("email").ToString) Then
                    If tb1.Rows(x)("local").ToString.ToLower = "copia" Then
                        mensagem.CC.Add(wcemail)
                    Else

                        mensagem.Bcc.Add(wcemail)
                    End If
                End If
            Next

            mensagem.IsBodyHtml = True
            mensagem.Subject = tb1.Rows(0)("descricao").ToString

            Dim xler As New StreamReader(HttpContext.Current.Server.MapPath("~\modelosemails") & tb1.Rows(0)("arquivo").ToString)

            mensagem.Body = xler.ReadToEnd

            xler.Close()



            Return True
        Catch exemail As Exception
            Mensagemerro1 = exemail.Message
            Return False
        End Try

    End Function
End Class

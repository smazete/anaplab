﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data

Public Class clsresultados_finais
    'Desenvolvedor: Claudio Smart
    'Data inicial :  23/08/2019
    'Data Atualização :  23/08/2019 - por Claudio

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _grade As New Data.DataTable
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _descricao As String
    Dim _usarem As String
    Dim _pesoinicial As Decimal = 0
    Dim _pesofinal As Decimal = 0
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo As Boolean
    Dim _userexclui As String
    Dim _dtexclui As Date

    Dim _metageral As Decimal = 0

    Public Sub New()
        versessao()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property

    Public Property Usarem As String
        Get
            Return _usarem
        End Get
        Set(value As String)
            _usarem = value
        End Set
    End Property



    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Pesoinicial As Decimal
        Get
            Return _pesoinicial
        End Get
        Set(value As Decimal)
            _pesoinicial = value
        End Set
    End Property

    Public Property Pesofinal As Decimal
        Get
            Return _pesofinal
        End Get
        Set(value As Decimal)
            _pesofinal = value
        End Set
    End Property

    Public ReadOnly Property Grade As DataTable
        Get

            carregargrade()

            Return _grade
        End Get

    End Property

    Public Property Metageral As Decimal
        Get
            Return _metageral
        End Get
        Set(value As Decimal)
            _metageral = value
        End Set
    End Property
End Class
Partial Public Class clsresultados_finais
    Public Function adicionar() As Boolean
        Try
            If _descricao = "" Then
                _mensagemerro = "Informe uma descrição válida !"
                Return False
            End If
            If _pesoinicial = 0 Then
                _mensagemerro = "Digite uma pontuação inicial válida !"
                Return False
            End If
            If _pesofinal = 0 Then
                _mensagemerro = "Digite uma pontuação final válida !"
                Return False
            End If
            If _nrseq = 0 Then
                tb1 = tab1.conectar("select * from tbresultados_finais where ativo = true and descricao = '" & _descricao & "'")
                If tb1.Rows.Count > 0 Then
                    _nrseq = tb1.Rows(0)("nrseq").ToString
                    If Not Me.alterar() Then
                        Return False
                    End If
                    Return True

                End If


                tb1 = tab1.IncluirAlterarDados("insert into tbresultados_finais (descricao, usarem, pesoinicial, pesofinal, ativo, dtcad, usercad, metageral) values ('" & _descricao & "', '" & _usarem & "', " & moeda(_pesoinicial) & ", " & moeda(_pesofinal) & ", true, '" & hoje() & "', '" & _usercad & "', " & moeda(_metageral) & ")")
                _mensagemerro = "Salvo com sucesso !"
            Else
                tb1 = tab1.conectar("select * from tbresultados_finais where nrseq = " & _nrseq)
                If tb1.Rows.Count > 0 Then
                    Dim xsql As String = "update tbresultados_finais set ativo = true, pesoinicial = " & moeda(_pesoinicial) & ", pesofinal = " & moeda(_pesofinal) & ", metageral = " & moeda(_metageral)
                    If _descricao <> "" Then
                        xsql &= ", descricao = '" & _descricao & "'"
                    End If
                    If _usarem <> "" Then
                        xsql &= ", usarem = '" & _usarem & "'"
                    End If

                    xsql &= " where nrseq = " & _nrseq


                    tb1 = tab1.IncluirAlterarDados(xsql)
                    _mensagemerro = "Salvo com sucesso !"
                Else
                    tb1 = tab1.IncluirAlterarDados("insert into tbresultados_finais (descricao, usarem, pesoinicial, pesofinal, ativo, dtcad, usercad, metageral) values ('" & _descricao & "', '" & _usarem & "', " & moeda(_pesoinicial) & ", " & moeda(_pesofinal) & ", true, '" & hoje() & "', '" & _usercad & "'," & moeda(_metageral) & ")")

                End If
            End If

            Return True
        Catch exadd As Exception
            _mensagemerro = exadd.Message
            Return False
        End Try
    End Function
    Public Function carregar() As Boolean
        Try

            tb1 = tab1.conectar("select * from tbresultados_finais where ativo = true and nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Essa descrição não existe! Informe uma descrição válida !"
                Return False
            End If
            With tb1
                _pesoinicial = .Rows(0)("pesoinicial").ToString
                _ativo = logico(.Rows(0)("ativo").ToString)
                _pesofinal = .Rows(0)("pesofinal").ToString
                _usarem = .Rows(0)("usarem").ToString
                _descricao = .Rows(0)("descricao").ToString
            End With



            Return True
        Catch exadd As Exception
            _mensagemerro = exadd.Message
            Return False
        End Try
    End Function
    Public Function alterar() As Boolean
        Try

            tb1 = tab1.conectar("select * from tbresultados_finais where ativo = true and nrseq = " & _nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Essa descrição não existe! Informe uma descrição válida !"
                Return False
            End If

            Dim xsql As String = "update tbresultados_finais set ativo = true"
            If _descricao <> "" Then
                xsql &= " , descricao = '" & _descricao & "'"
            End If
            If _usarem <> "" Then
                xsql &= " , usarem = '" & _usarem & "'"
            End If
            If _pesoinicial <> 0 Then
                xsql &= " , pesoinicial = " & moeda(_pesoinicial) & ""
            End If
            If _pesofinal <> 0 Then
                xsql &= " , pesofinal = " & moeda(_pesofinal) & ""
            End If
            If _metageral <> 0 Then
                xsql &= " , metageral = " & moeda(_metageral) & ""
            End If

            xsql &= " where nrseq = " & _nrseq

            tb1 = tab1.IncluirAlterarDados(xsql)





            Return True
        Catch exadd As Exception
            _mensagemerro = exadd.Message
            Return False
        End Try
    End Function
    Public Function remover() As Boolean
        Try
            If _nrseq = 0 Then
                _mensagemerro = "Informe nrseq válido para remover !"
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbresultados_finais set ativo = false, dtexclui =  '" & hoje() & "', userexclui =  '" & _usercad & "' where nrseq = " & _nrseq)

            Return True
        Catch exadd As Exception
            _mensagemerro = exadd.Message
            Return False
        End Try
    End Function
    Public Function carregargrade() As Boolean
        Try
            _grade = tab1.conectar("select * from tbresultados_finais where ativo = true order by pesoinicial")
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class

Imports System.Net.Mail
Imports System.IO
Imports clsSmart
Imports SelectPdf
Public Class clsEmail

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Public tipos As New List(Of String)()
    Public padroes As New List(Of clsEmail_PadroesDados)()
    Dim _mensagemerro As String
    Dim _tipodeemail As String
    Dim _nrlote As Integer
    Dim _resultado As String

    Dim _tabela As String

    Dim _arquivo As String
    Dim _assunto As String
    Dim _nrseq As Integer

    Public Sub New()


        tipos.Add("APROVA��O")
        tipos.Add("REPROVA��O")
        tipos.Add("NOVO USU�RIO")

    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Tipodeemail As String
        Get
            Return _tipodeemail
        End Get
        Set(value As String)
            _tipodeemail = value.ToString.ToLower
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Tabela As String
        Get
            Return _tabela
        End Get
        Set(value As String)
            _tabela = value
        End Set
    End Property

    Public Property Nrlote As Integer
        Get
            Return _nrlote
        End Get
        Set(value As Integer)
            _nrlote = value
        End Set
    End Property

    Public Property Resultado As String
        Get
            Return _resultado
        End Get
        Set(value As String)
            _resultado = value
        End Set
    End Property
End Class
Partial Public Class clsEmail
    Public Function populapadroes(padrao As String, valor As String, tabela As String, nrseq As Integer)


        If padroes.Count = 0 Then

            Dim xpadrao As New clsEmail_PadroesDados

            xpadrao.Chave = "{bid}"
            xpadrao.Valor = "nrseq"
            xpadrao.Tabela = "vwlicitacoes"
            xpadrao.Nrseq = 0

            padroes.Add(xpadrao)

            '   Dim xpadrao As New clsEmail_PadroesDados

            xpadrao.Chave = "{bid}"
            xpadrao.Valor = "nrseq"
            xpadrao.Tabela = "vwlicitacoes"
            xpadrao.Nrseq = 0

            padroes.Add(xpadrao)
        Else
        End If


    End Function
    Public Function enviarmensagem() As Boolean
        Try
            If _tipodeemail = "" Then
                _mensagemerro = "Defina um tipo de email"
                Return False
            End If
            'If padroes.Count = 0 Then
            '    _mensagemerro = "carregue padroes"
            '    Return False
            'End If
            Dim tbdest As New Data.DataTable
            Dim tabdest As New clsBanco
            tb1 = tab1.conectar("select * from vwconfig_acoes where ativo = true and nome = '" & _tipodeemail & "'")
            If tb1.Rows.Count = 0 Then
                Return False
            End If
            _arquivo = HttpContext.Current.Server.MapPath("~/modelosemails") & "\" & tb1.Rows(0)("arquivo").ToString
            _assunto = IIf(tb1.Rows(0)("assuntoemail").ToString = "", "No subject", tb1.Rows(0)("assuntoemail").ToString)
            If Not File.Exists(_arquivo) Then
                Return False
            End If


            tbdest = tabdest.conectar("select * from vwconfig_acoes where interno = '" & _tipodeemail & "' and ativo = true")


            Dim lermensagem As New StreamReader(_arquivo)
            Dim htmlm As String = lermensagem.ReadToEnd
            lermensagem.Close()


            Dim email As New clsEnvioEmail

            For x As Integer = 0 To tbdest.Rows.Count - 1
                If tbdest.Rows(x)("emailusuario").ToString.Contains("@") Then
                    email.AdicionaDestinatarios = (tbdest.Rows(x)("emailusuario").ToString)
                Else
                    If HttpContext.Current.Session("email").ToString.Contains("@") Then
                        email.AdicionaDestinatarios = HttpContext.Current.Session("email")
                    End If

                End If
            Next



            'email.AdicionaRemetente = "cevabids@smartcodesolucoes.com.br"
            email.AdicionaDestinatarios = "alertas@smartcodesolucoes.com.br"

            email.EhHTML = True

            email.AdicionaMensagem = trocadadostexto(htmlm, HttpContext.Current.Session("usuario"))

            email.AdicionaAssunto = trocadadostexto(_assunto, HttpContext.Current.Session("usuario"))
            email.configpadrao()
            email.Gravarinterno = True
            email.EnviarEmail()



            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function trocadadostexto(texto As String, pusuario As String) As String
        Dim tbtemp As New Data.DataTable
        Dim bid As String = ""
        Dim datedue As String = ""
        Dim tabtemp As New clsBanco
        Dim nomecliente As String = ""
        Dim cpfcliente As String = ""
        Dim textotabelacliente As String = ""
        Dim valorboleto As Decimal = 0
        Dim link As String = ""
        Dim grade As String = ""
        Dim nomearquivo As String = ""
        Dim xusuario As String = IIf(pusuario = "", "", pusuario)




        '    If tbtemp.Rows.Count = 0 Then Return ""
        'textotabelacliente = "<table> <tr> <td> teste </td> <td> teste 2 </td> <tr> <td> claudio </td> <td> claudio 2 </td> </table>"
        Return texto.Replace("{tabela}", _tabela).Replace("{nrlote}", _nrlote).Replace("{logo}", "http://cevacte.sistemasceva.com.br/ceva_logo.png").Replace("{resultado}", _resultado)


    End Function
End Class
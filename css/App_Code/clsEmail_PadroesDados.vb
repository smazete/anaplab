﻿Imports Microsoft.VisualBasic

Public Class clsEmail_PadroesDados

    Dim _chave As String
    Dim _valor As String
    Dim _tabela As String
    Dim _nrseq As Integer

    Public Property Chave As String
        Get
            Return _chave
        End Get
        Set(value As String)
            _chave = value
        End Set
    End Property

    Public Property Valor As String
        Get
            Return _valor
        End Get
        Set(value As String)
            _valor = value
        End Set
    End Property

    Public Property Tabela As String
        Get
            Return _tabela
        End Get
        Set(value As String)
            _tabela = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property
End Class

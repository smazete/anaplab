﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports System.IO
Public Class clsbancosmartcode
    Dim _versaoteste As Boolean = False
    Private _banco_conexao As MySql.Data.MySqlClient.MySqlConnection
    Private _banco_comando As MySql.Data.MySqlClient.MySqlCommand
    Private _banco_adaptador As MySql.Data.MySqlClient.MySqlDataAdapter
    Private _tabela_dataset As New Data.DataSet
    Private _tabela_nome As String
    Private _sql As String



    'servidorweb Copia
    ' Protected _dados_servidor As String = "191.252.3.96" ' Teste
    '
    Protected _dados_servidor As String = "191.252.1.3"
    'Protected _dados_banco As String = "dbquotesystem"
    Protected _dados_banco As String = "dbsmartcode"
    Protected _dados_usuario As String = "usrtoby"
    Protected _dados_senha As String = "Sousmart@747791"

    'servidorlocal
    'Protected _dados_servidor As String = "localhost"
    'Protected _dados_banco As String = "dbbids"
    'Protected _dados_usuario As String = "root"
    'Protected _dados_senha As String = "1234"
    Public Overridable ReadOnly Property versaoteste As Boolean
        Get
            Return _versaoteste
        End Get
    End Property
    Public Overridable ReadOnly Property servidorusado As String
        Get

            Dim tbtesteserver As New Data.DataTable
            tbtesteserver = Me.conectar("select * from tbusuarios")
            If tbtesteserver.Rows.Count > 0 Then
                Return _dados_servidor & " online"
            Else
                Return _dados_servidor & " offline"
            End If
        End Get
    End Property
    Public Overridable ReadOnly Property servidor As String
        Get
            Return sonumeros(_dados_servidor) & "-" & _dados_banco
        End Get
    End Property

    Public Overridable ReadOnly Property conexao As String
        Get
            Return "server=" & _dados_servidor & ";Database=" & _dados_banco & ";UID=" & _dados_usuario & ";PWD=" & _dados_senha & ";PORT = 3306;allow zero datetime=true"
        End Get
    End Property
    Public Property ExecutaSql() As String
        Get
            Return _sql
        End Get
        Set(ByVal value As String)
            _sql = value
        End Set
    End Property
    Public ReadOnly Property conectar(Optional sqlquery As String = "") As Data.DataTable
        Get
            Dim tentativas As Integer = 0
            Dim errocomopcoes As Boolean = False
            If sqlquery <> "" Then _sql = sqlquery
            If _sql = "" Then Return Nothing
retesta_select:
            If _tabela_dataset.Tables.Count > 0 Then
                _tabela_dataset.Tables.Clear()
            End If

            _tabela_dataset.Clear()
            _banco_conexao = New MySql.Data.MySqlClient.MySqlConnection(conexao)
            _banco_comando = New MySql.Data.MySqlClient.MySqlCommand(_sql, _banco_conexao)
            _banco_adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter(_banco_comando)
            'rs.Open(_sql, _strcon)
            Try
                _banco_adaptador.Fill(_tabela_dataset)
                If _sql.Substring(0, 1).ToUpper = "S" Then
                    NomeDaTabela = _tabela_dataset.Tables(0).TableName

                    _tabela_dataset.Tables(0).Namespace = _sql
                    Return _tabela_dataset.Tables(0)
                End If
            Catch ex As Exception
                If ex.Message.ToLower.Contains("failed because connected host has failed to respond") OrElse
                        ex.Message.ToLower.Contains("unable to connect to any of the") OrElse
                        ex.Message.ToLower.Contains("authentication to host") OrElse
                        ex.Message.ToLower.Contains("timeout expired") OrElse
                        ex.Message.ToLower.Contains("timeout in io ope") Then
                    Threading.Thread.Sleep(10000)

                    tentativas += 1
                    If tentativas < 3 Then
                        GoTo retesta_select
                    Else
                        errocomopcoes = True
                    End If
                End If

                Dim resultadoenvioemail As String
                Try
                    Dim envio As New clsEnvioEmail
                    envio.configpadrao()
                    envio.AdicionaDestinatarios = "suporte@smartcodesolucoes.com.br"
                    envio.AdicionaDestinatarios = "claudiosmart@smartcodesoluces.com.br"
                    envio.AdicionaAssunto = "Erro sistema QuoteSystem - " & Data() & " / " & hora()
                    envio.EhHTML = True
                    Dim xmensagem As String = "<html>"

                    xmensagem &= "<br> <Center> <img src=""http://www.smartcodesolucoes.com.br/img/simbolo.jpg"" width=""37px"" height=""32px""> <br> "
                    xmensagem &= "<span color=red> Relatório de erros </span> </center> <br>"
                    xmensagem &= "<hr>"
                    xmensagem &= "<br> Usuário:" & HttpContext.Current.Session("usuario")
                    xmensagem &= "<br> Email:" & HttpContext.Current.Session("email")
                    xmensagem &= "<br> Erro:" & ex.Message
                    xmensagem &= "<br> Linha do erro: " & ex.LineNumber
                    xmensagem &= "<br> Link do help: " & ex.HelpLink
                    xmensagem &= "<br> Página: " & HttpContext.Current.Request.Url.AbsoluteUri
                    xmensagem &= "<br> Expressão: " & _sql

                    xmensagem &= "</html>"

                    envio.AdicionaMensagem = xmensagem
                    If envio.EnviarEmail Then
                        resultadoenvioemail = "E-mail enviado com sucesso !"
                    Else
                        resultadoenvioemail = "E-mail não enviado !"
                    End If
                Catch exemail As Exception
                    resultadoenvioemail = "E-mail não enviado !" & exemail.Message
                End Try


                Dim logerro As New StreamWriter(HttpContext.Current.Server.MapPath("~") & "logerros\errosql" & sonumeros(HttpContext.Current.Request.UserHostAddress) & sonumeros(Data) & sonumeros(hora(True)) & ".txt")
                logerro.WriteLine(_sql)
                logerro.WriteLine("Log de erro - Data: " & Data() & " / Hora: " & hora(True) & " Mensagem: " & ex.Message)
                logerro.WriteLine("Linha do erro: " & ex.LineNumber)
                logerro.WriteLine("Link do help: " & ex.HelpLink)
                logerro.WriteLine("Página: " & HttpContext.Current.Request.Url.AbsoluteUri)
                logerro.WriteLine("Usuário: " & HttpContext.Current.Session("usuario"))
                logerro.WriteLine("Resultado notificação por e-mail " & resultadoenvioemail)
                logerro.Close()
                HttpContext.Current.Session("sql") = _sql

                If errocomopcoes Then
                    HttpContext.Current.Session("erro") = "Possivelmente, esse erro é devido a um congestionamento em sua rede interna. Podemos repetir a operação, clicando na opção retentar !"
                    HttpContext.Current.Session("opcao") = "Sim"
                    HttpContext.Current.Session("voltar") = HttpContext.Current.Request.Url.AbsoluteUri

                Else
                    HttpContext.Current.Session("opcao") = "Não"
                    HttpContext.Current.Session("erro") = "Erro de acesso à base de dados !"
                End If
                HttpContext.Current.Response.Redirect("\erro.aspx", True)
                'If Not ex.Message.Contains("Unable to connect to any of the specified MySQL host") Then
                '        HttpContext.Current.Response.Redirect("\erro.aspx?mensagem=Erro de acesso à base de dados !" & ex.Message, True)
                '    Else
                '        HttpContext.Current.Response.Redirect("\erro.aspx?mensagem=Estamos enfrentado problemas técnicos no datacenter. O problema será solucionado em até 2 horas! Pedimos desculpas pelos transtornos causados!", True)
                '    End If


            Finally
                '_banco_adaptador = Nothing
                _banco_conexao.Close()
            End Try



            Return Nothing
        End Get
    End Property
    Public ReadOnly Property IncluirAlterarDados(Optional sqlquery As String = "") As Data.DataTable
        Get
            Dim tentativas As Integer = 0
            Dim errocomopcoes As Boolean = False
            If sqlquery <> "" Then _sql = sqlquery
            If _sql = "" Then Return Nothing
retesta_select:
            Try

                If _tabela_dataset.Tables.Count > 0 Then
                    _tabela_dataset.Tables(0).Rows.Clear()
                    _tabela_dataset.Tables(0).Columns.Clear()
                End If
                _tabela_dataset.Clear()
                _banco_conexao = New MySql.Data.MySqlClient.MySqlConnection(conexao)
                _banco_comando = New MySql.Data.MySqlClient.MySqlCommand(_sql, _banco_conexao)
                _banco_adaptador = New MySql.Data.MySqlClient.MySqlDataAdapter(_banco_comando)
                'rs.Open(_sql, _strcon)
                _banco_adaptador.Fill(_tabela_dataset)
                If _sql.Substring(0, 1).ToUpper = "S" Then
                    NomeDaTabela = _tabela_dataset.Tables(0).TableName
                Else
                    _tabela_dataset.Tables.Add()
                    _tabela_dataset.Tables(0).Namespace = _sql
                    _tabela_dataset.Tables(0).Columns.Add("operacao")
                    _tabela_dataset.Tables(0).Rows.Add()
                    _tabela_dataset.Tables(0).Rows(_tabela_dataset.Tables(0).Rows.Count - 1)("operacao") = _sql

                End If


                Return _tabela_dataset.Tables(0)
            Catch ex As Exception
                If ex.Message.ToLower.Contains("failed because connected host has failed to respond") OrElse
                        ex.Message.ToLower.Contains("unable to connect to any of the") OrElse
                        ex.Message.ToLower.Contains("authentication to host") OrElse
                        ex.Message.ToLower.Contains("timeout expired") OrElse
                        ex.Message.ToLower.Contains("timeout in io ope") Then
                    Threading.Thread.Sleep(10000)

                    tentativas += 1
                    If tentativas < 3 Then
                        GoTo retesta_select
                    Else
                        errocomopcoes = True
                    End If
                End If
                Dim resultadoenvioemail As String
                Try
                    Dim envio As New clsEnvioEmail
                    envio.configpadrao()
                    envio.AdicionaDestinatarios = "suporte@smartcodesolucoes.com.br"
                    envio.AdicionaDestinatarios = "alertas@smartcodesoluces.com.br"
                    envio.AdicionaAssunto = "Erro sistema Bids - " & Data() & " / " & hora()
                    envio.EhHTML = True
                    Dim xmensagem As String = "<html>"

                    xmensagem &= "<br> <Center> <img src=""http://www.smartcodesolucoes.com.br/img/simbolo.jpg"" width=""37px"" height=""32px""> <br> "
                    xmensagem &= "<span color=red> Relatório de erros </span> </center> <br>"
                    xmensagem &= "<hr>"
                    xmensagem &= "<br> Usuário:" & HttpContext.Current.Session("usuario")
                    xmensagem &= "<br> Email:" & HttpContext.Current.Session("email")
                    xmensagem &= "<br> Erro:" & ex.Message
                    xmensagem &= "<br> Linha do erro: " & ex.LineNumber
                    xmensagem &= "<br> Link do help: " & ex.HelpLink
                    xmensagem &= "<br> Página: " & HttpContext.Current.Request.Url.AbsoluteUri
                    xmensagem &= "<br> Expressão: " & _sql

                    xmensagem &= "</html>"

                    envio.AdicionaMensagem = xmensagem
                    If envio.EnviarEmail Then
                        resultadoenvioemail = "E-mail enviado com sucesso !"
                    Else
                        resultadoenvioemail = "E-mail não enviado !"
                    End If
                Catch exemail As Exception
                    resultadoenvioemail = "E-mail não enviado !" & exemail.Message
                End Try


                Dim logerro As New StreamWriter(HttpContext.Current.Server.MapPath("~") & "logerros\errosql" & sonumeros(HttpContext.Current.Request.UserHostAddress) & sonumeros(Data) & sonumeros(hora(True)) & ".txt")
                logerro.WriteLine(_sql)
                logerro.WriteLine("Log de erro - Data: " & Data() & " / Hora: " & hora(True) & " Mensagem: " & ex.Message)
                logerro.WriteLine("Linha do erro: " & ex.LineNumber)
                logerro.WriteLine("Link do help: " & ex.HelpLink)
                logerro.WriteLine("Página: " & HttpContext.Current.Request.Url.Segments(HttpContext.Current.Request.Url.Segments.Count - 1))
                logerro.WriteLine("Usuário: " & HttpContext.Current.Session("usuario"))
                logerro.WriteLine("Resultado notificação por e-mail " & resultadoenvioemail)
                logerro.Close()
                If errocomopcoes Then
                    HttpContext.Current.Session("erro") = "Possivelmente, esse erro é devido a um congestionamento em sua rede interna. Podemos repetir a operação, clicando na opção retentar !"
                    HttpContext.Current.Session("opcao") = "Sim"
                    HttpContext.Current.Session("voltar") = HttpContext.Current.Request.Url.AbsoluteUri

                Else
                    HttpContext.Current.Session("opcao") = "Não"
                    HttpContext.Current.Session("erro") = "Erro de acesso à base de dados !"
                End If
                HttpContext.Current.Response.Redirect("\erro.aspx", True)
                '   HttpContext.Current.Response.Redirect("\erro.aspx?mensagem=Erro de acesso à base de dados !" & ex.Message, True)
            Finally
                _banco_conexao.Close()
            End Try
        End Get
    End Property

    Public Property NomeDaTabela() As String
        Get
            Return _tabela_nome
        End Get
        Set(ByVal value As String)
            _tabela_nome = value
        End Set
    End Property

    'Public ReadOnly Property atualizar() As Boolean
    '    Get
    '        _adaptador.Update(_tabela, "tab_teste")
    '        Return True
    '    End Get
    'End Property
End Class

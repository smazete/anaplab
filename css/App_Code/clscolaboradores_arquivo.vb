﻿Imports System.Data
Imports Microsoft.VisualBasic

Public Class clscolaboradores_arquivo

    Dim _mensagemerro As String
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _nrseq As Integer
    Dim _nrseqvendedor As Integer
    Dim _arquivo As String
    Dim _arqcompleto As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _descricao As String
    Dim _ativo As Integer
    Dim _dtexclui As Date
    Dim _userexclui As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Tb11 As DataTable
        Get
            Return tb1
        End Get
        Set(value As DataTable)
            tb1 = value
        End Set
    End Property

    Public Property Tab11 As clsBanco
        Get
            Return tab1
        End Get
        Set(value As clsBanco)
            tab1 = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqvendedor As Integer
        Get
            Return _nrseqvendedor
        End Get
        Set(value As Integer)
            _nrseqvendedor = value
        End Set
    End Property

    Public Property Arquivo As String
        Get
            Return _arquivo
        End Get
        Set(value As String)
            _arquivo = value
        End Set
    End Property

    Public Property Arqcompleto As String
        Get
            Return _arqcompleto
        End Get
        Set(value As String)
            _arqcompleto = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Ativo As Integer
        Get
            Return _ativo
        End Get
        Set(value As Integer)
            _ativo = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property
End Class

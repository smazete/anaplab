﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Public Class clspad_cursos
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _nrseq As Integer = 0
    Dim _nrseqpad As Integer
    Dim _ativo As Boolean
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _anobase As Integer
    Dim _descricao As String
    Dim _cursoativo As Boolean
    Dim _cargahoraria As String
    Dim _nrseqcurso As Integer
    Dim _mensagemerro As String
    Dim _concluido As Boolean
    Dim _dtconcluido As Date = data()

    Public Sub New()
        versessao()
        Usercad = HttpContext.Current.Session("usuario")
        Dtcad = data()
    End Sub

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqpad As Integer
        Get
            Return _nrseqpad
        End Get
        Set(value As Integer)
            _nrseqpad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Anobase As Integer
        Get
            Return _anobase
        End Get
        Set(value As Integer)
            _anobase = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = value
        End Set
    End Property

    Public Property Cursoativo As Boolean
        Get
            Return _cursoativo
        End Get
        Set(value As Boolean)
            _cursoativo = value
        End Set
    End Property

    Public Property Cargahoraria As String
        Get
            Return _cargahoraria
        End Get
        Set(value As String)
            _cargahoraria = value
        End Set
    End Property

    Public Property Nrseqcurso As Integer
        Get
            Return _nrseqcurso
        End Get
        Set(value As Integer)
            _nrseqcurso = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Concluido As Boolean
        Get
            Return _concluido
        End Get
        Set(value As Boolean)
            _concluido = value
        End Set
    End Property

    Public Property Dtconcluido As Date
        Get
            Return _dtconcluido
        End Get
        Set(value As Date)
            _dtconcluido = value
        End Set
    End Property
End Class

﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Public Class clspad_competencias

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim _naoprocurar As Boolean = False
    Dim _justificativa As String = ""
    Dim _mensagemerro As String
    Dim _nrseqpad As Integer
    Dim _nrseq As Integer = 0
    Dim _nrseqobjetivo As Integer
    Dim _nrseqavaliacao As Integer
    Dim _nrseqresultado As Integer = 0
    Dim _usercad As String
    Dim _dtcad As Date
    Dim _ativo As Boolean
    Dim _Descricaoobjetivo As String
    Dim _Descricaoresultado As String

    Public Sub New()
        versessao()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseqpad As Integer
        Get
            Return _nrseqpad
        End Get
        Set(value As Integer)
            _nrseqpad = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqobjetivo As Integer
        Get
            Return _nrseqobjetivo
        End Get
        Set(value As Integer)
            _nrseqobjetivo = value
        End Set
    End Property

    Public Property Nrseqavaliacao As Integer
        Get
            Return _nrseqavaliacao
        End Get
        Set(value As Integer)
            _nrseqavaliacao = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Descricaoobjetivo As String
        Get
            Return _Descricaoobjetivo
        End Get
        Set(value As String)
            _Descricaoobjetivo = value
        End Set
    End Property

    Public Property Descricaoresultado As String
        Get
            Return _Descricaoresultado
        End Get
        Set(value As String)
            _Descricaoresultado = value
            If Not Naoprocurar AndAlso _Descricaoresultado <> "" Then
                _nrseqresultado = 0
                tbx = tabx.conectar("select * from tbresultados where (usarem = 'Competencia' or usarem = 'Ambos') and ativo = true and descricao = '" & _Descricaoresultado & "'")
                If tbx.Rows.Count > 0 Then
                    _nrseqresultado = tbx.Rows(0)("nrseq").ToString

                End If
            End If
        End Set
    End Property

    Public Property Nrseqresultado As Integer
        Get
            Return _nrseqresultado
        End Get
        Set(value As Integer)
            _nrseqresultado = value
        End Set
    End Property

    Public Property Justificativa As String
        Get
            Return _justificativa
        End Get
        Set(value As String)

            _justificativa = tratatexto(value)
        End Set
    End Property

    Public Property Naoprocurar As Boolean
        Get
            Return _naoprocurar
        End Get
        Set(value As Boolean)
            _naoprocurar = value
        End Set
    End Property
End Class

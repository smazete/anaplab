﻿Imports System.Data
Imports Microsoft.VisualBasic

Public Class clscolaboradores

    Dim _mensagemerro As String
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim _nome As String
    Dim _senha As String
    Dim _tel1 As String
    Dim _email As String
    Dim _ativo As Integer
    Dim _dtcad As Date
    Dim _nrseq As Integer
    Dim _nrseqctrl As String
    Dim _comissaovenda As String
    Dim _comissaomanu As String
    Dim _comissaotreinamento As String
    Dim _vendedor As Integer
    Dim _agencia As String
    Dim _conta As String
    Dim _cpf As String
    Dim _complemento As String
    Dim _banco As String
    Dim _dtexclui As Date
    Dim _userexclui As String
    Dim _tel2 As String
    Dim _grupo As String
    Dim _cargo As String
    Dim _endereco As String
    Dim _bairro As String
    Dim _cidade As String
    Dim _cep As String
    Dim _rg As String
    Dim _usuario As String
    Dim _emailsmart As String
    Dim _dtadm As Date
    Dim _dtdem As Date
    Dim _tbvendedorescol As String
    Dim _uf As String
    Dim _nrseqempresa As Integer
    Dim _imagemperfil As String
    Dim _salvo As Integer
    Dim _nrsequsuario As Integer
    Dim _pais As String

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Tb11 As DataTable
        Get
            Return tb1
        End Get
        Set(value As DataTable)
            tb1 = value
        End Set
    End Property

    Public Property Tab11 As clsBanco
        Get
            Return tab1
        End Get
        Set(value As clsBanco)
            tab1 = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = value
        End Set
    End Property

    Public Property Senha As String
        Get
            Return _senha
        End Get
        Set(value As String)
            _senha = value
        End Set
    End Property

    Public Property Tel1 As String
        Get
            Return _tel1
        End Get
        Set(value As String)
            _tel1 = value
        End Set
    End Property

    Public Property Email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property Ativo As Integer
        Get
            Return _ativo
        End Get
        Set(value As Integer)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Comissaovenda As String
        Get
            Return _comissaovenda
        End Get
        Set(value As String)
            _comissaovenda = value
        End Set
    End Property

    Public Property Comissaomanu As String
        Get
            Return _comissaomanu
        End Get
        Set(value As String)
            _comissaomanu = value
        End Set
    End Property

    Public Property Comissaotreinamento As String
        Get
            Return _comissaotreinamento
        End Get
        Set(value As String)
            _comissaotreinamento = value
        End Set
    End Property

    Public Property Vendedor As Integer
        Get
            Return _vendedor
        End Get
        Set(value As Integer)
            _vendedor = value
        End Set
    End Property

    Public Property Agencia As String
        Get
            Return _agencia
        End Get
        Set(value As String)
            _agencia = value
        End Set
    End Property

    Public Property Conta As String
        Get
            Return _conta
        End Get
        Set(value As String)
            _conta = value
        End Set
    End Property

    Public Property Cpf As String
        Get
            Return _cpf
        End Get
        Set(value As String)
            _cpf = value
        End Set
    End Property

    Public Property Complemento As String
        Get
            Return _complemento
        End Get
        Set(value As String)
            _complemento = value
        End Set
    End Property

    Public Property Banco As String
        Get
            Return _banco
        End Get
        Set(value As String)
            _banco = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Tel2 As String
        Get
            Return _tel2
        End Get
        Set(value As String)
            _tel2 = value
        End Set
    End Property

    Public Property Grupo As String
        Get
            Return _grupo
        End Get
        Set(value As String)
            _grupo = value
        End Set
    End Property

    Public Property Cargo As String
        Get
            Return _cargo
        End Get
        Set(value As String)
            _cargo = value
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = value
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = value
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Rg As String
        Get
            Return _rg
        End Get
        Set(value As String)
            _rg = value
        End Set
    End Property

    Public Property Usuario As String
        Get
            Return _usuario
        End Get
        Set(value As String)
            _usuario = value
        End Set
    End Property

    Public Property Emailsmart As String
        Get
            Return _emailsmart
        End Get
        Set(value As String)
            _emailsmart = value
        End Set
    End Property

    Public Property Dtadm As Date
        Get
            Return _dtadm
        End Get
        Set(value As Date)
            _dtadm = value
        End Set
    End Property

    Public Property Dtdem As Date
        Get
            Return _dtdem
        End Get
        Set(value As Date)
            _dtdem = value
        End Set
    End Property

    Public Property Tbvendedorescol As String
        Get
            Return _tbvendedorescol
        End Get
        Set(value As String)
            _tbvendedorescol = value
        End Set
    End Property

    Public Property Uf As String
        Get
            Return _uf
        End Get
        Set(value As String)
            _uf = value
        End Set
    End Property

    Public Property Nrseqempresa As Integer
        Get
            Return _nrseqempresa
        End Get
        Set(value As Integer)
            _nrseqempresa = value
        End Set
    End Property

    Public Property Imagemperfil As String
        Get
            Return _imagemperfil
        End Get
        Set(value As String)
            _imagemperfil = value
        End Set
    End Property

    Public Property Salvo As Integer
        Get
            Return _salvo
        End Get
        Set(value As Integer)
            _salvo = value
        End Set
    End Property

    Public Property Nrsequsuario As Integer
        Get
            Return _nrsequsuario
        End Get
        Set(value As Integer)
            _nrsequsuario = value
        End Set
    End Property

    Public Property Pais As String
        Get
            Return _pais
        End Get
        Set(value As String)
            _pais = value
        End Set
    End Property
End Class

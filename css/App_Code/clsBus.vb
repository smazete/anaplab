﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Public Class ClsBus

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _codigobu As String = ""
    Dim _descricao As String
    Dim _ativo As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _userexclui As String
    Dim _dtexclui As String
    Dim _nrseqctrl As String


    Public Sub New()
        versessao()
        Usercad = HttpContext.Current.Session("usuario")
        Dtcad = data()
    End Sub
    Public Sub New(xusuario As String)
        Usercad = xusuario
        Dtcad = data()
    End Sub
    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property



    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = tratatexto(value)
        End Set
    End Property

    Public Property Dtexclui As String
        Get
            Return _dtexclui
        End Get
        Set(value As String)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Codigobu As String
        Get
            Return _codigobu
        End Get
        Set(value As String)
            _codigobu = tratatexto(value)
        End Set
    End Property
End Class
Partial Public Class ClsBus
    Public Function novo() As Boolean
        Try

            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into tbbus (ativo, dtcad, usercad, nrseqctrl) values (false, '" & hoje() & "','" & Usercad & "', '" & wcnrseqctrl & "')")

            tb1 = tab1.conectar("select * from tbbus where nrseqctrl = '" & wcnrseqctrl & "'")

            Nrseq = tb1.Rows(0)("nrseq").ToString
            Nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString

            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function


    Public Function Salvar() As Boolean
        Try

            If Nrseq = 0 Then
                _mensagemerro = "Selecione uma empresa para alterar !"
                Return False
            End If

            If _descricao = "" Then
                _mensagemerro = "Digite uma descrição para salvar !"
                Return False
            End If

            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("Update tbbus Set descricao ='" & _descricao & "',ativo= True,dtcad='" & _dtcad & "',usercad='" & _usercad & "',dtexclui='" & _dtexclui & "',userexclui='" & _userexclui & "', codigobu = '" & _codigobu & "' where nrseqctrl = '" & _nrseqctrl & "' ")
            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function
    Public Function excluir() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            If Not carregar() Then
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("Update tbbus ativo = " & IIf(Not _ativo, "True", "False") & ", dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "' where nrseqctrl = '" & _nrseqctrl & "' ")
            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function

    Public Function carregar() As Boolean
        Try

            If Nrseq = 0 Then
                _mensagemerro = "Selecione uma empresa para procurar !"
                Return False
            End If
            tb1 = tab1.conectar("select *  from tbbus where nrseq = " & Nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "o Bus  não existe !"
                Return False
            End If
            With tb1
                _descricao = .Rows(0)("descricao").ToString
                _ativo = .Rows(0)("ativo").ToString
                _dtcad = .Rows(0)("dtcad").ToString
                _usercad = .Rows(0)("usercad").ToString
                _dtexclui = .Rows(0)("dtexclui").ToString
                _userexclui = .Rows(0)("userexclui").ToString
                Nrseq = .Rows(0)("nrseq").ToString
                _codigobu = .Rows(0)("codigobu").ToString
                'puxar todos os campos



            End With


        Catch exconsulta As Exception
            _mensagemerro = exconsulta.Message
            Return False
        End Try

    End Function



End Class
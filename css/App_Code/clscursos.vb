﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart

Public Class clscursos

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco

    Dim _count As Integer = 0
    Dim _procurarpor As String
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _descricao As String = ""
    Dim _cargahoraria As String
    Dim _area As String
    Dim _codigo As String
    Dim _nrseqareaorigem As Integer = 0
    Dim _url As String
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _ativo As Boolean
    Dim _userexclui As String
    Dim _dtexclui As Date
    Dim _nrseqctrl As String

    Public Sub New()
        versessao()
        _usercad = HttpContext.Current.Session("usuario")
        _dtcad = data()
    End Sub
    Public Sub New(xusercad As String)
        _usercad = xusercad
        _dtcad = data()
    End Sub
    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property

    Public Property Cargahoraria As String
        Get
            Return _cargahoraria
        End Get
        Set(value As String)
            _cargahoraria = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Dtexclui As Date
        Get
            Return _dtexclui
        End Get
        Set(value As Date)
            _dtexclui = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Area As String
        Get
            Return _area
        End Get
        Set(value As String)
            _area = value
            Dim xqtdtestes As Integer = 1
Novoteste:
            tbx = tabx.conectar("select * from tbareasorigens where descricao = '" & tratatexto(value) & "'")
            If tbx.Rows.Count > 0 Then
                _nrseqareaorigem = tbx.Rows(0)("nrseq").ToString
            Else
                Dim xarea As New clsareasorigens(_usercad)
                xarea.Descricao = value
                xarea.salvar()

                xqtdtestes += 1
                If xqtdtestes > 3 Then
                    _nrseqareaorigem = 0
                    Exit Property
                End If
                GoTo Novoteste
            End If
        End Set
    End Property

    Public Property Codigo As String
        Get
            Return _codigo
        End Get
        Set(value As String)
            _codigo = value
        End Set
    End Property

    Public Property Nrseqareaorigem As Integer
        Get
            Return _nrseqareaorigem
        End Get
        Set(value As Integer)
            _nrseqareaorigem = value
        End Set
    End Property

    Public Property Procurarpor As String
        Get
            Return _procurarpor
        End Get
        Set(value As String)
            _procurarpor = value
        End Set
    End Property

    Public Property Count As Integer
        Get
            Return _count
        End Get
        Set(value As Integer)
            _count = value
        End Set
    End Property

    Public Property Url As String
        Get
            Return _url
        End Get
        Set(value As String)
            _url = tratatexto(value)
        End Set
    End Property
End Class

Partial Public Class clscursos
    Public Function Novo() As Boolean
        Try

            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbcursos (nrseqctrl, ativo, dtcad, usercad) values ('" & wcnrseqctrl & "', false, '" & hoje() & "', '" & _usercad & "')")
            tb1 = tab1.conectar("select * from tbcursos where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString


            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function
    Public Function salvar() As Boolean
        Try

            If _nrseq = 0 Then
                _mensagemerro = "Selecione um núemro válido !"
                Return False
            End If
            Dim xsql As String = "update tbcursos set ativo = true"
            If _descricao <> "" Then
                xsql &= ", descricao = '" & _descricao & "'"
            End If
            If _nrseqareaorigem <> 0 Then
                xsql &= ", nrseqareaorigem = '" & _nrseqareaorigem & "'"
            End If
            If _codigo <> "" Then
                xsql &= ", codigo = '" & _codigo & "'"
            End If
            If _url <> "" Then
                xsql &= ", url = '" & _url & "'"
            End If
            If _cargahoraria <> "" Then
                xsql &= ", cargahoraria = '" & _cargahoraria & "'"
            End If
            xsql &= "where nrseq = " & _nrseq


            tb1 = tab1.IncluirAlterarDados(xsql)

            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function
    Public Function excluir() As Boolean
        Try

            If Not carregar() Then
                Return False
            End If

            If _ativo Then
                tb1 = tab1.IncluirAlterarDados("update tbcursos set ativo = false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "' where nrseq = " & _nrseq)
            Else
                tb1 = tab1.IncluirAlterarDados("update tbcursos set ativo = true where nrseq = " & _nrseq)
            End If


            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function
    Public Function carregar() As Boolean
        Try

            If _procurarpor = "nrseq" Then

                If _nrseq = 0 Then
                    _mensagemerro = "Selecione um numero válido !"
                    Return False
                End If

                tb1 = tab1.IncluirAlterarDados("select * from  vwcursos  where nrseq = " & _nrseq)
            Else
                tb1 = tab1.IncluirAlterarDados("select * from  vwcursos  where descricao = '" & _descricao & "'")
            End If

            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Curso não localizado !"
                Return False
            End If

            _count = tb1.Rows.Count
            With tb1
                _nrseq = .Rows(0)("nrseq").ToString
                _descricao = .Rows(0)("descricao").ToString
                _cargahoraria = .Rows(0)("cargahoraria").ToString
                _ativo = .Rows(0)("ativo").ToString
                _usercad = .Rows(0)("usercad").ToString
                _dtcad = .Rows(0)("dtcad").ToString
                _area = .Rows(0)("area").ToString
                _codigo = .Rows(0)("codigo").ToString
                _area = .Rows(0)("area").ToString
                _nrseqareaorigem = tratanumeros(.Rows(0)("nrseqareaorigem").ToString)
                _url = .Rows(0)("url").ToString
            End With


            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function
End Class
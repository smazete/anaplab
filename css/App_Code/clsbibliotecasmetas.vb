﻿Imports Microsoft.VisualBasic
Imports clsSmart

Public Class clsbibliotecasmetas

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim _dth As New List(Of clsbibliotecasmetas_dth)

    Dim _alteracao As Boolean = False
    Dim _nome As String
    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0
    Dim _nrseqgestor As Integer = 0
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String

    Public Sub New()
        _usercad = HttpContext.Current.Session("usuario")
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property

    Public Property Nrseqgestor As Integer
        Get
            Return _nrseqgestor
        End Get
        Set(value As Integer)
            _nrseqgestor = value
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Dth As List(Of clsbibliotecasmetas_dth)
        Get
            Return _dth
        End Get
        Set(value As List(Of clsbibliotecasmetas_dth))
            _dth = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Nome As String
        Get
            Return _nome
        End Get
        Set(value As String)
            _nome = tratatexto(value)
        End Set
    End Property

    Public Property Alteracao As Boolean
        Get
            Return _alteracao
        End Get
        Set(value As Boolean)
            _alteracao = value
        End Set
    End Property
End Class
Partial Public Class clsbibliotecasmetas

    Public Function novo() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            tb1 = tab1.IncluirAlterarDados("insert into tbbibliotecasmetas (nrseqctrl, ativo, usercad) values ('" & wcnrseqctrl & "', false, '" & Usercad & "')")
            tb1 = tab1.conectar("select * from tbbibliotecasmetas where nrseqctrl = '" & wcnrseqctrl & "'")
            _nrseq = tb1.Rows(0)("nrseq").ToString



            tbx = tabx.conectar("select * from tbobjetivos where tipo = 'Objetivo' and ativo = true order by descricao")
            For x As Integer = 0 To tbx.Rows.Count - 1
                Dim xdth As New clsbibliotecasmetas_dth
                xdth.Ativo = True
                xdth.descricaoobjetivo = tbx.Rows(x)("descricao").ToString
                xdth.Nrseq = 0
                xdth.Descricao = ""
                xdth.Nomegestor = _usercad
                xdth.Nrseqbiblioteca = _nrseq
                xdth.Nrseqobjetivo = tbx.Rows(x)("nrseq").ToString

                _dth.Add(xdth)


            Next


            tbx = tabx.conectar("select * from vwbibliotecasmetas_dth where nrseqbiblioteca = " & _nrseq)
            For x As Integer = 0 To Dth.Count - 1
                Dim tbproc As Data.DataRow()
                tbproc = tbx.Select(" descricaoobjetivo = '" & Dth(x).descricaoobjetivo & "'")
                If tbproc.Count = 0 Then
                    tb1 = tab1.IncluirAlterarDados("insert into tbbibliotecasmetas_dth (descricao, nrseqobjetivo, ativo, nrseqbiblioteca, dtcad, usercad) values ('" & Dth(x).Descricao & "', " & Dth(x).Nrseqobjetivo & ", true, " & _nrseq & ", '" & hoje() & "', '" & _usercad & "')")

                Else

                    '  tb1 = tab1.IncluirAlterarDados("insert into tbbibliotecasmetas_dth set descricao = '" & Dth(x).Descricao & "', ativo = true where nrseq = " & Dth(x).Nrseq)
                End If
            Next
            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function
    Public Function salvar()

        Try
            If _nome = "" Then
                _mensagemerro = "Por favor, informe um nome válido para essa biblioteca. !"
                Return False
            Else

                'tb1 = tab1.conectar("select * from tbbibliotecasmetas where ativo = true and nrseqgestor = " & _nrseqgestor & " and nome = '" & _nome & "'")
                'If Not Alteracao AndAlso tb1.Rows.Count <> 0 Then
                '    _mensagemerro = "Esse nome já foi definido para outro padrão!"
                '    Return False
                'End If
            End If
            If _nrseq = 0 Then
                _mensagemerro = "selecione uma biblioteca válida !"
                Return False
            End If
            If _nrseqgestor = 0 Then
                _mensagemerro = "selecione um gestor válido !"
                Return False
            End If
            If _dth.Count = 0 Then
                _mensagemerro = "Nenhum objetivo em sua biblioteca para salvar !"
                Return False
            End If
            tb1 = tab1.IncluirAlterarDados("update tbbibliotecasmetas set ativo = true, nrseqgestor = " & _nrseqgestor & ", nome = '" & _nome & "' where nrseq = " & _nrseq)

            tbx = tabx.conectar("select * from vwbibliotecasmetas_dth where nrseqbiblioteca = " & _nrseq)
            'For x As Integer = 0 To tbx.Rows.Count - 1
            '    Dim xdth As New clsbibliotecasmetas_dth
            '    xdth.Ativo = tbx.Rows(x)("ativo").ToString
            '    xdth.descricaoobjetivo = tbx.Rows(x)("descricaoobjetivo").ToString
            '    xdth.Nrseq = tbx.Rows(x)("nrseq").ToString
            '    xdth.Descricao = tbx.Rows(x)("descricao").ToString
            '    xdth.Nomegestor = tbx.Rows(x)("nomegestor").ToString
            '    xdth.Nrseqbiblioteca = tbx.Rows(x)("nrseqbiblioteca").ToString
            '    xdth.Nrseqobjetivo = tbx.Rows(x)("nrseqobjetivo").ToString

            '    _dth.Add(xdth)


            'Next
            For x As Integer = 0 To Dth.Count - 1
                Dim tbproc As Data.DataRow()
                tbproc = tbx.Select(" descricaoobjetivo = '" & Dth(x).descricaoobjetivo & "'")
                If tbproc.Count = 0 Then
                    tb1 = tab1.IncluirAlterarDados("insert into tbbibliotecasmetas_dth (descricao, nrseqobjetivo, ativo, nrseqbiblioteca, dtcad, usercad) values ('" & Dth(x).Descricao & "', " & Dth(x).Nrseqobjetivo & ", true, " & _nrseq & ", '" & hoje() & "', '" & _usercad & "')")

                Else

                    tb1 = tab1.IncluirAlterarDados("update tbbibliotecasmetas_dth set descricao = '" & Dth(x).Descricao & "', ativo = true where nrseqobjetivo = " & tbproc(0)("nrseqobjetivo") & " and nrseqbiblioteca = " & _nrseq)
                End If
            Next

            Return True
        Catch exsalvar As Exception
            _mensagemerro = exsalvar.Message
            Return False
        End Try
    End Function
    Public Function excluir() As Boolean
        Try

            If Not carregar() Then
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbbibliotecasmetas set ativo = " & IIf(_ativo, " false, dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "'", " true ") & " where nrseq = " & _nrseq)


            Return True
        Catch ex As Exception
            _mensagemerro = ex.Message
            Return False
        End Try
    End Function
    Public Function carregar() As Boolean
        Try

            If _nrseq = 0 Then
                _mensagemerro = "Selecione uma biblioteca válida !"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbbibliotecasmetas where nrseq = " & _nrseq)

            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Nenhum biblioteca localizada !"
                Return False
            End If

            _nrseqgestor = tb1.Rows(0)("nrseqgestor").ToString
            _ativo = tb1.Rows(0)("ativo").ToString
            _nome = tb1.Rows(0)("nome").ToString

            tbx = tabx.conectar("select * from vwbibliotecasmetas_dth where nrseqbiblioteca = " & _nrseq)
            For x As Integer = 0 To tbx.Rows.Count - 1
                Dim xdth As New clsbibliotecasmetas_dth
                xdth.Ativo = tbx.Rows(x)("ativo").ToString
                xdth.descricaoobjetivo = tbx.Rows(x)("descricaoobjetivo").ToString
                xdth.Nrseq = tbx.Rows(x)("nrseq").ToString
                xdth.Descricao = tbx.Rows(x)("descricao").ToString
                xdth.Nomegestor = tbx.Rows(x)("nomegestor").ToString
                xdth.Nrseqbiblioteca = tbx.Rows(x)("nrseqbiblioteca").ToString
                xdth.Nrseqobjetivo = tbx.Rows(x)("nrseqobjetivo").ToString

                _dth.Add(xdth)


            Next

            Return True
        Catch ex As Exception
            _mensagemerro = ex.Message
            Return False
        End Try


    End Function

    Public Function carregargrade(Optional exibirexcluidos As Boolean = False) As Data.DataTable
        Return tab1.conectar("select * from tbbibliotecasmetas where nrseqgestor = " & _nrseqgestor & " and  " & IIf(exibirexcluidos, " 1=1 ", "  ativo = true  ") & " order by nome")
    End Function
End Class
﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports System.IO
Public Class clssessoes
    Inherits System.Web.UI.Page
    Private Shared _mensagemerro As String
    Public Shared cookieName As String = "NewClinica"
    Private Shared _listacookiessistema As New List(Of String)

    Public Sub New()

    End Sub

    Public Shared Sub versessao()
        cookie_sessao()

        If HttpContext.Current.Session("nrseqcolaborador") = "" Then
            HttpContext.Current.Session("nrseqcolaborador") = 0
        End If
        ' Se for html / pdf
        If mARQUIVO(HttpContext.Current.Request.Path, True) = "gerarpad.aspx" Then Exit Sub

        Exit Sub

        Dim resultado As String = validarusuario(HttpContext.Current.Session("id"), HttpContext.Current.Server.HtmlDecode(HttpContext.Current.Session("permissao")), mARQUIVO(HttpContext.Current.Request.Path, True), "").ToLower
        Select Case resultado
            Case Is = "login"

                If Not HttpContext.Current.Request.Cookies.Get(cookieName) Is Nothing Then
                    'If (HttpContext.Current.Request.Cookies.Get(cookieName)("permissao") = "" OrElse HttpContext.Current.Request.Cookies.Get(cookieName)("permissao") Is Nothing) Then
                    '    HttpContext.Current.Response.Redirect("login.aspx")
                    '    Exit Sub
                    'Else
                    HttpContext.Current.Session("urlretorno") = HttpContext.Current.Request.Path


                    HttpContext.Current.Response.Redirect("login.aspx")
                    'End If



                Else
                    HttpContext.Current.Session("urlretorno") = HttpContext.Current.Request.Path


                    HttpContext.Current.Response.Redirect("login.aspx")
                End If
                Exit Sub
            Case Is = "suspenso"
                HttpContext.Current.Response.Redirect("erro.aspx?mensagem=Seu acesso ao sistema foi suspenso!")
            Case Is = "seguranca"
                If Not HttpContext.Current.Request.Path.ToLower.Contains("seguranca.aspx") Then
                    HttpContext.Current.Response.Redirect("seguranca.aspx")
                End If
            'Case Is = "completardadosagente"
            '    If Not HttpContext.Current.Request.Path.ToLower.Contains("registro.aspx") Then
            '        HttpContext.Current.Response.Redirect("registro.aspx")
            '    End If
            Case Is = "semacesso"
                If Not HttpContext.Current.Request.Path.ToLower.Contains("acessonegado.aspx") Then
                    Dim logerro As New StreamWriter(HttpContext.Current.Server.MapPath("~") & "\logerros\erroacesso" & sonumeros(HttpContext.Current.Request.UserHostAddress) & sonumeros(data) & sonumeros(hora(True)) & ".txt")

                    logerro.WriteLine("Log de erro - Data: " & data() & " / Hora: " & hora(True) & " Acesso Negado ")
                    logerro.WriteLine("Usuário: " & HttpContext.Current.Session("usuario"))
                    logerro.WriteLine("Página: " & mARQUIVO(HttpContext.Current.Request.Path, True))
                    logerro.WriteLine("Permissão: " & HttpContext.Current.Server.HtmlDecode(HttpContext.Current.Session("permissao")))

                    logerro.Close()


                    HttpContext.Current.Response.Redirect("acessonegado.aspx")
                End If

            Case Is = "erro"

                Exit Sub
        End Select


    End Sub

    Private Shared Sub carregacookies()
        Try
            _listacookiessistema.Clear()
            _listacookiessistema.Add("id")
            _listacookiessistema.Add("idusuario")
            _listacookiessistema.Add("usuario")
            _listacookiessistema.Add("nomeusuario")
            _listacookiessistema.Add("usuario")
            _listacookiessistema.Add("email")
            _listacookiessistema.Add("permissao")
            _listacookiessistema.Add("usuariomaster")
            _listacookiessistema.Add("imagemperfil")
            _listacookiessistema.Add("urlretorno")
            _listacookiessistema.Add("usertransp")
            _listacookiessistema.Add("nomecliente")
            _listacookiessistema.Add("enderecocliente")
            _listacookiessistema.Add("bairrocliente")
            _listacookiessistema.Add("cidadecliente")
            _listacookiessistema.Add("ufcliente")
            _listacookiessistema.Add("paiscliente")
            _listacookiessistema.Add("telefonecliente")
            _listacookiessistema.Add("logocliente")
            _listacookiessistema.Add("corpadrao")
            _listacookiessistema.Add("gestor")
            _listacookiessistema.Add("master")
            _listacookiessistema.Add("sql")
            _listacookiessistema.Add("nrseqcolaborador")
            _listacookiessistema.Add("novasenhaem")
            _listacookiessistema.Add("anobaseuso")
        Catch ex As Exception

        End Try




    End Sub

    Public Shared Sub limparCookieTemporario()



        carregacookies()
        Dim xCoockie As HttpCookie
        xCoockie = verificarCookies()

        For x As Integer = 0 To _listacookiessistema.Count - 1
            Try
                If _listacookiessistema(x) Is Nothing Then Exit For

                If xCoockie.Values(_listacookiessistema(x)) Is Nothing Then
                    xCoockie.Values(_listacookiessistema(x)) = ""


                End If

                HttpContext.Current.Session(_listacookiessistema(x)) = HttpContext.Current.Server.HtmlDecode(xCoockie.Values(_listacookiessistema(x)))
            Catch extam As Exception
                Exit For
            End Try



        Next

        xCoockie.Expires = DateTime.Now.AddDays(1)
        HttpContext.Current.Response.Cookies.Add(xCoockie)
    End Sub

    Public Shared Sub cookie_sessao(Optional chave As String = "")

        carregacookies()
        Dim xCoockie As HttpCookie
        xCoockie = verificarCookies()

        For x As Integer = 0 To _listacookiessistema.Count - 1
            Try
                If _listacookiessistema(x) Is Nothing Then Exit For
                If chave = "" Then
                    If xCoockie.Values(_listacookiessistema(x)) Is Nothing Then
                        xCoockie.Values(_listacookiessistema(x)) = ""


                    End If

                    HttpContext.Current.Session(_listacookiessistema(x)) = HttpContext.Current.Server.HtmlDecode(xCoockie.Values(_listacookiessistema(x)))
                Else
                    If _listacookiessistema(x).ToLower = chave.ToLower Then
                        HttpContext.Current.Session(_listacookiessistema(x)) = HttpContext.Current.Server.HtmlDecode(xCoockie.Values(_listacookiessistema(x)))
                    End If
                End If

            Catch extam As Exception
                Exit For
            End Try

        Next

        xCoockie.Expires = DateTime.Now.AddDays(4)
        HttpContext.Current.Response.Cookies.Add(xCoockie)
    End Sub



    Public Shared Function gravarcookie(chave As String, valor As String) As Boolean

        Dim xCoockie As HttpCookie
        xCoockie = verificarCookies()


        xCoockie.Values(chave) = ""
        xCoockie.Values.Add(chave, valor)
        HttpContext.Current.Response.AppendCookie(xCoockie)

        'Dim aCookie As New HttpCookie("cevabids")
        'aCookie.Values("id") = HttpContext.Current.Session("id")
        'aCookie.Values("lastVisit") = DateTime.Now.ToString()
        'aCookie.Values("usuario") = HttpContext.Current.Session("usuario")
        'aCookie.Values("idusuario") = HttpContext.Current.Session("idusuario")
        'aCookie.Values("permissao") = HttpContext.Current.Session("permissao")
        'aCookie.Values("nomeusuario") = HttpContext.Current.Session("nomeusuario")
        'aCookie.Values("email") = HttpContext.Current.Session("email")
        'aCookie.Values("filial") = HttpContext.Current.Session("filial")
        'aCookie.Values("grupo") = HttpContext.Current.Session("grupo")
        'aCookie.Values("pais") = HttpContext.Current.Session("pais")
        'aCookie.Values("nrseqagente") = HttpContext.Current.Session("nrseqagente")
        'aCookie.Values("agente") = HttpContext.Current.Session("agente")
        'aCookie.Values("nrseqgrupo") = HttpContext.Current.Session("nrseqgrupo")
        'aCookie.Values("nrseqfilial") = HttpContext.Current.Session("nrseqfilial")
        'aCookie.Values("imagemperfil") = HttpContext.Current.Session("imagemperfil")


        'aCookie.Expires = DateTime.Now.AddDays(4)
        'HttpContext.Current.Response.Cookies.Add(aCookie)

    End Function


    Public Shared Function buscarsessoes(chave As String) As String

        carregacookies()

        Dim xCoockie As HttpCookie
        xCoockie = verificarCookies()

        For x As Integer = 0 To _listacookiessistema.Count - 1
            If _listacookiessistema(x) Is Nothing Then Exit For

            If xCoockie.Values(_listacookiessistema(x)) Is Nothing Then
                xCoockie.Values(_listacookiessistema(x)) = ""


            End If




        Next

        Dim valorretorno As String = ""
        If chave <> "" Then
            valorretorno = xCoockie.Values(chave)
        End If

        xCoockie.Expires = DateTime.Now.AddDays(30)
        HttpContext.Current.Response.Cookies.Add(xCoockie)

        Return valorretorno

    End Function

    Private Shared Function verificarCookies() As HttpCookie
        Try
            If HttpContext.Current.Request.Cookies(cookieName) Is Nothing Then
                Dim aCookie As New HttpCookie(cookieName)

                aCookie.Expires = DateTime.Now.AddDays(30)
                HttpContext.Current.Response.Cookies.Add(aCookie)
                Return aCookie
            Else

                Return HttpContext.Current.Request.Cookies(cookieName)
            End If
        Catch excoockie As Exception
            _mensagemerro = excoockie.Message
            Return Nothing
        End Try

    End Function


    Public Shared Sub versessao2()



        Dim resultado As String = valida(HttpContext.Current.Session("usuario"), mARQUIVO(HttpContext.Current.Request.Path, True), HttpContext.Current.Session("usuariomaster")).ToLower
        Select Case resultado
            Case Is = "login"

                If Not HttpContext.Current.Request.Cookies.Get("cevapad") Is Nothing Then
                    HttpContext.Current.Session("id") = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.Cookies.Get("cevapad")("id"))
                    HttpContext.Current.Session("usuariomaster") = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.Cookies.Get("cevapad")("usuariomaster"))
                    HttpContext.Current.Session("usuario") = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.Cookies.Get("cevapad")("usuario"))
                    HttpContext.Current.Session("idusuario") = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.Cookies.Get("cevapad")("idusuario"))
                    HttpContext.Current.Session("usertransp") = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.Cookies.Get("cevapad")("usertransp"))
                    HttpContext.Current.Session("permissao") = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.Cookies.Get("cevapad")("permissao"))
                    HttpContext.Current.Session("permissao") = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.Cookies.Get("cevapad")("permissao"))
                    'aCookie.Values("nomecliente") = HttpContext.Current.Session("nomecliente")
                    'aCookie.Values("enderecocliente") = HttpContext.Current.Session("enderecocliente")
                    'aCookie.Values("bairrocliente") = HttpContext.Current.Session("bairrocliente")
                    'aCookie.Values("cidadecliente") = HttpContext.Current.Session("cidadecliente")
                    'aCookie.Values("ufcliente") = HttpContext.Current.Session("ufcliente")
                    'aCookie.Values("paiscliente") = HttpContext.Current.Session("paiscliente")
                    'aCookie.Values("telefonecliente") = HttpContext.Current.Session("telefonecliente")
                    'aCookie.Values("logocliente") = HttpContext.Current.Session("logocliente")
                    'aCookie.Values("corpadrao") = HttpContext.Current.Session("corpadrao")
                Else
                    HttpContext.Current.Session("urlretorno") = HttpContext.Current.Request.Path


                    HttpContext.Current.Response.Redirect("login.aspx")
                End If
                Exit Sub
            Case Is = "erro"

                Exit Sub
        End Select


    End Sub
End Class

﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Imports System.Data

Public Class clsareasorigens

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    Dim _mensagemerro As String
    Dim _descricao As String
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String

    Public Sub New()
        versessao()
        _usercad = buscarsessoes("usercad")
    End Sub

    Public Sub New(xusercad As String)
        _usercad = xusercad
    End Sub

    Public Property Descricao As String
        Get
            Return _descricao
        End Get
        Set(value As String)
            _descricao = tratatexto(value)
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = value
        End Set
    End Property

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property


End Class
Partial Public Class clsareasorigens

    Public Function salvar() As Boolean
        Try

            If _descricao = "" Then
                _mensagemerro = "Informe uma descrição válida para a área !"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbareasorigens where descricao = '" & _descricao & "'")
            If tb1.Rows.Count = 0 Then
                tb1 = tab1.IncluirAlterarDados("insert into tbareasorigens (descricao, ativo, dtcad, usercad) values ('" & _descricao & "', true, '" & hoje() & "', '" & _usercad & "')")

            Else
                tb1 = tab1.IncluirAlterarDados("update tbareasorigens set ativo = true where descricao = '" & _descricao & "'")

            End If


            Return True
        Catch exsalbvar As Exception
            Return False
        End Try

    End Function

    Public Function excluir() As Boolean
        Try

            If Not carregar() Then
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("update tbareasorigens set ativo = " & IIf(Ativo, " False", " True") & " where descricao = '" & _descricao & "'")

            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.Message
            Return False
        End Try
    End Function
    Public Function carregar() As Boolean
        Try
            If _descricao = "" Then
                _mensagemerro = "Informe uma descrição válida para a área !"
                Return False
            End If

            tb1 = tab1.conectar("select * from tbareasorigens where descricao = '" & _descricao & "'")
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "Nenhuma área de origem localizada com essa descrição !"
                Return False
            End If
            _ativo = logico(tb1.Rows(0)("ativo").ToString)
            Return True
        Catch exexcluir As Exception
            _mensagemerro = exexcluir.Message
            Return False
        End Try
    End Function

    Public Function grade() As Data.DataTable
        tb1 = tab1.conectar("select * from tbareasorigens where ativo = true order by descricao")
        Return tb1
    End Function

End Class
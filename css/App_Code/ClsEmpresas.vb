﻿Imports Microsoft.VisualBasic
Imports clssessoes
Imports clsSmart
Public Class ClsEmpresas

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Dim _mensagemerro As String
    Dim _nrseq As Integer = 0

    Dim _cep As String
    Dim _cnpj As String

    Dim _empresa As String
    Dim _endereco As String
    Dim _cidade As String
    Dim _bairro As String
    Dim _uf As String
    Dim _ativo As Boolean
    Dim _dtcad As Date
    Dim _usercad As String
    Dim _nrseqctrl As String
    Dim _dtexclui As String
    Dim _userexclui As String


    Public Sub New()
        versessao()
        Usercad = HttpContext.Current.Session("usuario")
        Dtcad = data()
    End Sub

    Public Property Mensagemerro As String
        Get
            Return _mensagemerro
        End Get
        Set(value As String)
            _mensagemerro = value
        End Set
    End Property

    Public Property Empresa As String
        Get
            Return _empresa
        End Get
        Set(value As String)
            _empresa = tratatexto(value)
        End Set
    End Property

    Public Property Endereco As String
        Get
            Return _endereco
        End Get
        Set(value As String)
            _endereco = tratatexto(value)
        End Set
    End Property

    Public Property Cidade As String
        Get
            Return _cidade
        End Get
        Set(value As String)
            _cidade = tratatexto(value)
        End Set
    End Property

    Public Property Bairro As String
        Get
            Return _bairro
        End Get
        Set(value As String)
            _bairro = tratatexto(value)
        End Set
    End Property

    Public Property Uf As String
        Get
            Return _uf
        End Get
        Set(value As String)
            _uf = tratatexto(value)
        End Set
    End Property

    Public Property Ativo As Boolean
        Get
            Return _ativo
        End Get
        Set(value As Boolean)
            _ativo = value
        End Set
    End Property

    Public Property Dtcad As Date
        Get
            Return _dtcad
        End Get
        Set(value As Date)
            _dtcad = value
        End Set
    End Property

    Public Property Usercad As String
        Get
            Return _usercad
        End Get
        Set(value As String)
            _usercad = tratatexto(value)
        End Set
    End Property

    Public Property Dtexclui As String
        Get
            Return _dtexclui
        End Get
        Set(value As String)
            _dtexclui = value
        End Set
    End Property

    Public Property Userexclui As String
        Get
            Return _userexclui
        End Get
        Set(value As String)
            _userexclui = value
        End Set
    End Property

    Public Property Cep As String
        Get
            Return _cep
        End Get
        Set(value As String)
            _cep = value
        End Set
    End Property

    Public Property Nrseqctrl As String
        Get
            Return _nrseqctrl
        End Get
        Set(value As String)
            _nrseqctrl = value
        End Set
    End Property

    Public Property Cnpj As String
        Get
            Return _cnpj
        End Get
        Set(value As String)
            _cnpj = value
        End Set
    End Property

    Public Property Nrseq As Integer
        Get
            Return _nrseq
        End Get
        Set(value As Integer)
            _nrseq = value
        End Set
    End Property
End Class
Partial Public Class ClsEmpresas
    Public Function novo() As Boolean
        Try

            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("insert into tbempresas (ativo, dtcad, usercad, nrseqctrl) values (false, '" & hoje() & "','" & Usercad & "', '" & wcnrseqctrl & "')")

            tb1 = tab1.conectar("select * from tbempresas where nrseqctrl = '" & wcnrseqctrl & "'")

            Nrseq = tb1.Rows(0)("nrseq").ToString
            Nrseqctrl = tb1.Rows(0)("nrseqctrl").ToString

            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try

    End Function


    Public Function Salvar() As Boolean
        Try

            If Nrseq = 0 Then
                _mensagemerro = "Selecione uma empresa para alterar !"
                Return False
            End If

            If _empresa = "" Then
                _mensagemerro = "Digite uma empresa para salvar !"
                Return False
            End If

            Dim wcnrseqctrl As String = gerarnrseqcontrole()

            tb1 = tab1.IncluirAlterarDados("Update tbempresas Set empresa ='" & _empresa & "',endereco ='" & _endereco & "' ,cep = '" & _cep & "', cnpj = '" & _cnpj & "',cidade='" & _cidade & "',bairro='" & _bairro & "',uf='" & _uf & "',ativo='" & _ativo & "',dtcad='" & _dtcad & "',usercad='" & _usercad & "',dtexclui='" & _dtexclui & "',userexclui='" & _userexclui & "' where nrseqctrl = '" & _nrseqctrl & "' ")
            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function
    Public Function excluir() As Boolean
        Try
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            If Not carregar() Then
                Return False
            End If

            tb1 = tab1.IncluirAlterarDados("Update tbempresas ativo = " & IIf(Not _ativo, "True", "False") & ", dtexclui = '" & hoje() & "', userexclui = '" & _usercad & "' where nrseqctrl = '" & _nrseqctrl & "' ")
            Return True
        Catch exnovo As Exception
            _mensagemerro = exnovo.Message
            Return False
        End Try
    End Function

    Public Function editar(grade)
        tb1 = tab1.conectar("select * from tbempresas where nrseq = " & grade.Cells(0).Text)


        _nrseq = tb1.Rows(0)("nrseq").ToString
        _empresa = tb1.Rows(0)("empresa").ToString
        _cnpj = tb1.Rows(0)("cnpj").ToString
        _cep = tb1.Rows(0)("cep").ToString
        _endereco = tb1.Rows(0)("endereco").ToString
        _cidade = tb1.Rows(0)("cidade").ToString
        _bairro = tb1.Rows(0)("bairro").ToString
        _uf = tb1.Rows(0)("uf").ToString
    End Function

    Public Function carregar() As Boolean
        Try

            If Nrseq = 0 Then
                _mensagemerro = "Selecione uma empresa para procurar !"
                Return False
            End If
            tb1 = tab1.conectar("select *  from tbempresas where nrseq = " & Nrseq)
            If tb1.Rows.Count = 0 Then
                _mensagemerro = "A empresa  não existe !"
                Return False
            End If
            With tb1
                _cnpj = .Rows(0)("cnpj").ToString
                _cep = .Rows(0)("cep").ToString
                _empresa = .Rows(0)("empresa").ToString
                _endereco = .Rows(0)("endereco").ToString
                _cidade = .Rows(0)("cidade").ToString
                _bairro = .Rows(0)("bairro").ToString
                _uf = .Rows(0)("uf").ToString
                _ativo = .Rows(0)("ativo").ToString
                _dtcad = .Rows(0)("dtcad").ToString
                _usercad = .Rows(0)("usercad").ToString
                _dtexclui = .Rows(0)("dtexclui").ToString
                _userexclui = .Rows(0)("userexclui").ToString
                Nrseq = .Rows(0)("nrseq").ToString
                'puxar todos os campos



            End With


        Catch exconsulta As Exception
            _mensagemerro = exconsulta.Message
            Return False
        End Try

    End Function



End Class
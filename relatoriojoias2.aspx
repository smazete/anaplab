﻿<%@ Page Language="VB" AutoEventWireup="false"  MasterPageFile="~/masterpage.master" CodeFile="relatoriojoias2.aspx.vb" Inherits="relatoriojoias2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script runat="server">

        Sub exportarh(ByVal Source As Object, ByVal E As EventArgs)

            txtnomearq.Text = "joias2" & Date.Now.Date.ToString.Replace("/", "").Trim()
            txtnomearq.Text = Date.Now.Date.ToString.Replace(":", "").Trim()

            If txtnomearq.Text = "" Then
                txtnomearq.BackColor = Drawing.Color.Red
                Exit Sub
            End If

            'exportarExcel(dg, txtnomearq.Text)

            exportarExcel(gradeex, txtnomearq.Text)



        End Sub

        Sub exportarExcel(grid As DataGrid, saveAsFile As String)

            ' O linite de linhas do Excel é  65536
            If grid.Items.Count + 1 < 65536 Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & saveAsFile & ".xls")
                ' Remover caracteres do header - Content-Type
                HttpContext.Current.Response.Charset = ""
                'HttpContext.Current.Response.WriteFile("style.txt")
                ' desabilita o  view state.
                grid.EnableViewState = False
                Dim tw As New System.IO.StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grid.RenderControl(hw)
                ' Escrever o html no navegador
                HttpContext.Current.Response.Write(tw.ToString())
                ' termina o response
                HttpContext.Current.Response.End()

            Else
                HttpContext.Current.Response.Write("Muitas linhas para exportar para o Excel !!!")
            End If

        End Sub
    </script>

    <script type="text/javascript">

        function irlink(linkativo) {
            window.open(linkativo, '_blank');
        }

        function irlinkmesmpagina(linkativo) {
            window.location.href = linkativo;
            //window.open(linkativo);
        }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Relatorio de Joias 2 </b>
        </div>


        <div class="box box-header">
        </div>
        <asp:UpdatePanel runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="btnver" />
                <asp:PostBackTrigger ControlID="btnexport" />
            </Triggers>
            <ContentTemplate>
        <div class="row">
            <div class=" col-lg-12" runat="server">
               
                <div class="col-lg-2">
                    <b>ID</b>
                    <asp:TextBox runat="server" ID="txtid" CssClass="form-control" MaxLength="4" />
                </div>
                <div class="col-lg-2">
                    
                    <b>Iguinorar</b><br />
                    <asp:CheckBox ID="chkdt" Text="Data" runat="server" />
                       </div>

                <div class="col-lg-2">
                    <b>
                        <asp:Label Text="Mês" runat="server" /></b>
                    <asp:DropDownList ID="cbomes" CssClass="form-control" runat="server">
                        <asp:ListItem Text="Janeiro" Value="1" />
                        <asp:ListItem Text="Fevereiro" Value="2" />
                        <asp:ListItem Text="Março" Value="3" />
                        <asp:ListItem Text="Abril" Value="4" />
                        <asp:ListItem Text="Maio" Value="5" />
                        <asp:ListItem Text="Junho" Value="6" />
                        <asp:ListItem Text="Julho" Value="7" />
                        <asp:ListItem Text="Agosto" Value="8" />
                        <asp:ListItem Text="Setembro" Value="9" />
                        <asp:ListItem Text="Outubro" Value="10" />
                        <asp:ListItem Text="Novembro" Value="11" />
                        <asp:ListItem Text="Dezembro" Value="12" />
                    </asp:DropDownList>
                </div>
                <div class="col-lg-2">
                    <b>Ano</b>
                    <asp:TextBox runat="server" ID="txtano" CssClass="form-control" MaxLength="4" />
                </div>
                <div class="col-lg-2">
                    <b>
                        <asp:Label Text="Status " runat="server" /></b>
                    <asp:DropDownList runat="server" CssClass="form-control" ID="cbostatus">
                        <asp:ListItem Text="Todas" Value="4" />
                        <asp:ListItem Text="Aberta" Value="0" />
                        <asp:ListItem Text="Pago" Value="1" />
                    </asp:DropDownList>
                </div>

                <div class="col-lg-2">
                    <br />
                    <asp:Button runat="server" CssClass="btn btn-primary" ID="btnver" Text="Carregar" AutoPostBack="true" />
                </div>

                <div class="col-lg-2">
                    <br />
                    <asp:Label ID="txtnomearq" runat="server"></asp:Label>
                    <asp:Button runat="server" CssClass="btn btn-success" ID="btnexport" Text="Exportar Excel" Autopostback="true" OnClick="exportarh" Visible="false" />

                </div>



            </div>


        </div>
</ContentTemplate>
        </asp:UpdatePanel>
        <div class="row">
            <br>
            <%-- Grade de mensalidades --%>
            <div class="box-header with-border">
                <h3 class="box-title"></h3>

                <div class="box-tools pull-right">
                    <!-- Buttons, labels, and many other things can be placed here! -->
                    <!-- Here is a label for example -->
                </div>
                <!-- /.box-tools -->
            </div>

            <asp:DataGrid ID="grademensalidade" runat="server" CssClass=" table table-striped" AutoGenerateColumns="False" ShowFooter="True">
                <AlternatingItemStyle BackColor="White" />
                <Columns>
                    <asp:BoundColumn DataField="nrsequser" HeaderText="Nrseq" />
                    <asp:BoundColumn DataField="matricula" HeaderText="matricula" />
                    <asp:BoundColumn DataField="nome" HeaderText="Cliente" />
                    <asp:BoundColumn DataField="mes" HeaderText="Mes" />
                    <asp:BoundColumn DataField="ano" HeaderText="Ano" />
                    <asp:TemplateColumn HeaderText="Valor">
                        <ItemTemplate>
                            <asp:Label ID="lblsif" runat="server"></asp:Label>
                            <asp:Label ID="lblvalordg" runat="server" Text='<%#Bind("valor") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lbltotvalordg" runat="server" Text="0"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="statuspg" HeaderText="Status" />
                    <asp:BoundColumn DataField="dtpg" HeaderText="dtpg" />
                </Columns>
            </asp:DataGrid>

            <%-- 
            
            Grade EXCEL

            --%>
            <div runat="server" visible="false">
            <asp:DataGrid ID="gradeex" runat="server" BackColor="WhiteSmoke" BorderColor="Black" CellPadding="0" CellSpacing="1" Font-Name="Arial" Font-Names="Arial" Font-Size="8pt" HeaderStyle-BackColor="#0000FF" Headerstyle-Font-Name="Arial" HeaderStyle-Font-Size="10" HeaderStyle-ForeColor="#FFFFFF" Height="148px" Style="margin-right: 0px" AutoGenerateColumns="False" ShowFooter="True">
                <Columns>
                    
                    <asp:BoundColumn DataField="nrsequser" HeaderText="Nrseq" />
                    <asp:BoundColumn DataField="matricula" HeaderText="matricula" />
                    <asp:BoundColumn DataField="nome" HeaderText="Cliente" />
                    <asp:BoundColumn DataField="mes" HeaderText="Mes" />
                    <asp:BoundColumn DataField="ano" HeaderText="Ano" />
                    <asp:TemplateColumn HeaderText="Valor">
                        <ItemTemplate>
                            <asp:Label ID="lblsif" runat="server"></asp:Label>
                            <asp:Label ID="lblvalordg" runat="server" Text='<%#Bind("valor") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lbltotvalordg" runat="server" Text="0"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="statuspg" HeaderText="Status" />
                    <asp:BoundColumn DataField="dtpg" HeaderText="dtpg" />
                </Columns>
            </asp:DataGrid>
        </div>

        </div>

    </div>

</asp:Content>

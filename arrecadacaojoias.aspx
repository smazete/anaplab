﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="arrecadacaojoias.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="arrecadacaojoias" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="col-md-6">

        <div class="panel-body">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row box-title">
                                ARRECADAÇÃO JOIAS   
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">

                            <div class="row">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>

                                        <div class="col-lg-5">
                                            <asp:DropDownList runat="server" ID="cbomes" CssClass="form-control">
                                                <asp:ListItem />
                                                <asp:ListItem Text="Janeiro" Value="1" />
                                                <asp:ListItem Text="Fevereiro" Value="2" />
                                                <asp:ListItem Text="Março" Value="3" />
                                                <asp:ListItem Text="Abril" Value="4" />
                                                <asp:ListItem Text="Maio" Value="5" Selected="True" />
                                                <asp:ListItem Text="Junho" Value="6" />
                                                <asp:ListItem Text="Julho" Value="7" />
                                                <asp:ListItem Text="Agosto" Value="8" />
                                                <asp:ListItem Text="Setembro" Value="9" />
                                                <asp:ListItem Text="Outubro" Value="10" />
                                                <asp:ListItem Text="Novembro" Value="11" />
                                                <asp:ListItem Text="Dezembro" Value="12" />
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-lg-3">
                                            <asp:TextBox runat="server" ID="txtbuscajoias" CssClass="form-control" Text="2020" />
                                        </div>

                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-2">
                                            <asp:LinkButton Text="Busca" CssClass="btn btn-primary" runat="server" ID="btnbuscandojoias"></asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <br />
                            </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-condensed">
                                <tbody>
                                    <%-- <tr>
                                            <th style="width: 10px">#</th>
                                            <th>PARTICIPANTES TOTAIS</th>
                                            <th style="width: 40px">
                                                <asp:Label ID="lblparticipantesgerais" Text="0" runat="server" /></th>
                                          
                                        </tr>--%>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>PARTICIPANTES</th>
                                        <th style="width: 40px">
                                            <asp:Label ID="lblparticipantes" Text="0" runat="server" /></th>
                                        <%--  <asp:Label ID="lblparticipantes" Text="0" runat="server" /></th>--%>
                                    </tr>
                                    <tr>
                                        <td>#.</td>
                                        <td>FILIADOS ADIMPLENTES</td>

                                        <td><span class="badge bg-blue">
                                            <asp:Label ID="lblarrecadajoias" Text="0" runat="server" /></span></td>
                                    </tr>
                                    <tr>
                                        <td>#.</td>
                                        <td>FILIADOS ISENTOS </td>

                                        <td><span class="badge bg-blue">
                                            <asp:Label ID="lblarrecadaisentos" Text="0" runat="server" /></span></td>
                                    </tr>
                                    <tr>
                                        <td>#.</td>
                                        <td>FILIADOS INADIMPLENTES</td>

                                        <td><span class="badge bg-blue">
                                            <asp:Label ID="lblinadimplentes" Text="0" runat="server" /></span></td>
                                    </tr>
                                    <tr>
                                        <td>#.</td>
                                        <td>DESFILIADOS ADIMPLENTES</td>
                                        <td><span class="badge bg-light-blue">
                                            <asp:Label ID="lbldesfiliadosadiplentes" Text="0" runat="server" /></span></td>
                                    </tr>
                                    <tr>
                                        <td>#.</td>
                                        <td>VALOR ARRECADADO</td>
                                        <td><span class="badge bg-blue">R$:  
                                                <asp:Label ID="lblvalorfinal" runat="server" onkeyup="formataValor(this,event);" AutoPostBack="true"></asp:Label>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="col-md-6">

        <div class="panel-body panel-primary ">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row box-title">
                                EXTRATO CONSOLIDADO   
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                            <div class="row">
                                <div class="box-body">

                                    <div class="box-body no-padding">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>Pagamentos:
                                                <asp:Label ID="lblpagamentosextrato" Text="text" runat="server" /></th>
                                                    <th style="width: 40px">
                                                </tr>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>Total Geral:
                                                <asp:Label ID="lbltotalgeral" Text="text" runat="server" /></th>
                                                    <th style="width: 40px">
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>


                                <div class="box-body">
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>Comissões (Acumulado)</th>
                                                </tr>
                                                <tr>
                                                    <td>#.</td>
                                                    <td>ANAPLAB</td>
                                                    <td><span class="badge bg-blue">R$: 
                                                <asp:Label ID="lblanaplab" Text="0" runat="server" /></span></td>
                                                </tr>
                                                <tr>
                                                    <td>#.</td>
                                                    <td>STAMATO</td>
                                                    <td><span class="badge bg-blue">R$: 
                                                <asp:Label ID="lblstamato" Text="0" runat="server" /></span></td>
                                                </tr>
                                                <tr>
                                                    <td>#.</td>
                                                    <td>NELSON KUSTER</td>

                                                    <td><span class="badge bg-blue">R$: 
                                                <asp:Label ID="lblnelson" Text="0" runat="server" /></span></td>
                                                </tr>
                                                <tr>
                                                    <td>#.</td>
                                                    <td><b>TOTAL DE COMISSÕES</b></td>
                                                    <td><span class="badge bg-light-blue">R$: 
                                                <asp:Label ID="lblcomissao" Text="0" runat="server" /></span></td>
                                                </tr>
                                                <tr>
                                                    <td>#.</td>
                                                    <td><b>TOTAL GERAL ARRECADADO</b></td>
                                                    <td><span class="badge bg-blue">R$: 
                                                <asp:Label ID="lblgeralarrecadado" runat="server" onkeyup="formataValor(this,event);" AutoPostBack="true"></asp:Label>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>


</asp:Content>

﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage2.master" MasterPageFile="~/MasterPage.master"  CodeFile="home.aspx.vb" Inherits="home" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

                <div class="col-md-4">
                    <p>pois entao</p>
                </div>
                <div class="col-md-8">

     <div style="text-align:center;box-shadow:rgba(71,74,76,0.64) 0px 0px 5px 5px;border-radius:10px;padding:10px;background-color:rgb(251,251,248)">
 <div style="background-color:rgb(238,238,238);box-shadow:rgba(71,74,76,0.64) 0px 0px 0px 0px;border-radius:5px;padding:10px;line-height:normal">
          <span style="background-color:rgb(251,251,248)"><font size="3">
            <p ><b><u><span>À PREVI – CAIXA DE PREVIDÊNCIA DOS FUNC. DO BB</span></u></b></p>
              </font></span>
         
      </div>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span><b><u>Rio de Janeiro – RJ.</u></b></br></span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span><b>Senhor Presidente,</b></br></br></span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span> <b><u>Assunto: Suspensão de cobrança do ES por 03 meses</u></b></br></span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span> <b><u>Razões</u></b></br></span></p>
 <p ><span>&nbsp;</span></p>
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>
 Tendo em vista a inusitada vigência da pandemia do covid-19 e a necessidade premente de um colchão de liquidez para enfrentarmos a crise generalizada, a ANAPLAB se une às demais solicitações consideradas humanitárias de suspensão temporária na cobrança das prestações do Empréstimo Simples, prática já adotada por esse fundo em ocasiões anteriores de necessidade muito menor que a atual.</span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Na prática, a “suspensão” já existe de outra forma, na realização de renovação do ES a cada seis parcelas pagas. Como existem quatro modalidades (A,B,C,D), após a carência, ocorre novo empréstimo, porém, ONERADO pelo IOF e taxa de administração. Com a suspensão da cobrança o beneficiário não paga tais taxas, o que é um ganho financeiro; e a PREVI não sofre qualquer prejuízo, apenas deixa de receber a taxa de administração uma vez que o IOF vai para os cofres do governo.</span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Outros fundos, como o FUNCEF e o PETROS, já suspenderam. Outro fator favorável é que nosso limite nas operações com participantes está bem abaixo dos 15% permitidos pelo Regulamento. Portanto, se não há outras razões impeditivas, solicitamos excepcionalmente aplicar tal medida, haja vista que nem todos os tomadores de ES desejam o benefício, mas somente aqueles que OPTAREM pela suspensão.</span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Embora o fundo já tenha se manifestado a respeito do tema, insistimos na RECONSIDERAÇÃO, tendo em conta a situação de penúria de muitos assistidos.</span><span></span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Nesses Termos,</br></span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span> Pedimos Deferimento.</span></p>
 
 <img src="images/assinaturaaa.png" >
 
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Ari Zanella – Presidente Administrativo – ANAPLAB.</span></p>
  </div> 
 <div style="text-align:center;box-shadow:rgba(71,74,76,0.64) 0px 0px 5px 5px;border-radius:10px;padding:10px;background-color:rgb(251,251,248)">
 <div style="background-color:rgb(238,238,238);box-shadow:rgba(71,74,76,0.64) 0px 0px 0px 0px;border-radius:5px;padding:10px;line-height:normal">
          <span style="background-color:rgb(251,251,248)"><font size="3">
            <p ><b><u><span>COMUNICADO ANAPLAB.</span></u></b></p>
              </font></span>
         
      </div>
 
 <p ><span>&nbsp;</span></p>
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>
 Comunicamos aos nossos associados que devido a considerável número de inadimplência nas mensalidades, única fonte de renda da associação, implantamos o bloqueio de acesso ao autoatendimento para todos os associados que não estão em dia com suas obrigações sociais.</span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Informamos que, uma vez regularizada a pendência, o acesso se dará automaticamente, sem necessidade de qualquer ajuste.</span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Pedimos gentilmente a compreensão de todos no sentido de manter suas mensalidades em dia pois as nossas despesas judiciais são elevadas.</span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Qualquer esclarecimento adicional poderá ser obtido através de nossos endereços eletrônicos, a saber:</span><span></span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>atendimento@anaplab.com.br </br>
tesouraria@anaplab.com.br
</span></p>
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span> Atenciosamente,</span></p>
 
 
 
 <p dir="ltr" style="line-height:1.7999999999999998;text-align:justify;margin-top:0pt;margin-bottom:10pt"><span>Presidente ANAPLAB</span></p>
  </div>
</div>







     
</asp:Content>

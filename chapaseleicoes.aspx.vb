﻿Imports clssessoes
Imports System.IO
Imports clsSmart
Partial Class chapaseleicoes
    Inherits System.Web.UI.Page
    Private Sub chapaseleicoes_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        habilitar(False)
        btnsalvar.Visible = False
        carregagradeacoes()
    End Sub

    Private Sub limpar()
        txtnumero.Text = ""
        txtdescricao.Text = ""
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub habilitar(valor As Boolean)
        txtnumero.Enabled = valor
        txtdescricao.Enabled = valor
    End Sub

    Private Sub btnnova_Click(sender As Object, e As EventArgs) Handles btnnova.Click
        Dim xChapas As New clschapas_eleicoes
        If Not xChapas.novo Then
            sm("swal({title: 'Atenção!',text: '" & xChapas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        hdnrseq.Value = xChapas.Nrseq
        habilitar(True)
        btnnova.Visible = False
        btnsalvar.Visible = True
    End Sub

    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click
        Dim xclasse As New clschapas_eleicoes
        xclasse.Nrseq = hdnrseq.Value
        xclasse.Numero = txtnumero.Text
        xclasse.Descricao = txtdescricao.Text
        If Not xclasse.salvar Then
            sm("swal({title: 'Atenção!',text: '" & xclasse.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        limpar()
        habilitar(False)
        btnnova.Visible = True
        btnsalvar.Visible = False
        carregagradeacoes()
    End Sub

    Public Sub carregagradeacoes(Optional exibirinativos As Boolean = False)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbchapas_eleicoes where ativo = true and descricao like '%" & txtbusca.Text & "%'")

        gradeacoes.DataSource = tb1
        gradeacoes.DataBind()
    End Sub

    Private Sub btnbuscar_Click(sender As Object, e As EventArgs) Handles btnbuscar.Click
        carregagradeacoes()
    End Sub

    Private Sub gradeacoes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeacoes.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeacoes.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "editar" Then
            Dim xChapas As New clschapas_eleicoes

            xChapas.Nrseq = nrseq.Value

            If Not xChapas.procurar() Then
                sm("swal({title: 'Error!',text: '" & xChapas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            hdnrseq.Value = xChapas.Nrseq
            txtnumero.Text = xChapas.Numero
            txtdescricao.Text = xChapas.Descricao
            habilitar(True)
            btnnova.Visible = False
            btnsalvar.Visible = True
        ElseIf e.CommandName = "excluir" Then
            Dim xChapas As New clschapas_eleicoes

            xChapas.Nrseq = nrseq.Value

            If Not xChapas.excluir() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Ação cancelada com sucesso',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregagradeacoes()
        End If
    End Sub

    Private Sub gradeacoes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeacoes.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If

        e.Row.ForeColor = System.Drawing.Color.Black
        e.Row.BackColor = System.Drawing.Color.LightBlue

    End Sub
End Class

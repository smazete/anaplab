const months = [
    "Janeiro",
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro"
];

const weekdays = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];



// Váriavel principal
let date = new Date();

// Função que retorna a data atual do calendário 
function getCurrentDate(element, asString) {
    if (element) {
        if (asString) {
            return element.textContent = weekdays[date.getDay()] + ', ' + date.getDate() + " de " + months[date.getMonth()] + " de " + date.getFullYear();
        }
        return element.value = date.toISOString().substr(0, 10);
    }
    return date;
}


// Função principal que gera o calendário

function changeDatecustom(data) {
date = new Date(data+"EDT");
    generateCalendar();
}

function generateCalendar() {

    // Pega um calendário e se já existir o remove
    const calendar = document.getElementById('calendar');
    if (calendar) {
        calendar.remove();
    }

    // Cria a tabela que será armazenada as datas
    const table = document.createElement("table");
    table.id = "calendar";

    // Cria os headers referentes aos dias da semana
    const trHeader = document.createElement('tr');
    trHeader.className = 'weekends';
    weekdays.map(week => {
        const th = document.createElement('th');
        const w = document.createTextNode(week.substring(0, 3));
        th.appendChild(w);
        trHeader.appendChild(th);
    });

    // Adiciona os headers na tabela
    table.appendChild(trHeader);

    //Pega o dia da semana do primeiro dia do mês
    const weekDay = new Date(
        date.getFullYear(),
        date.getMonth(),
        1
    ).getDay();

    //Pega o ultimo dia do mês
    const lastDay = new Date(
        date.getFullYear(),
        date.getMonth() + 1,
        0
    ).getDate();

    let tr = document.createElement("tr");
    let td = '';
    let empty = '';
    let btn = document.createElement('button');
    let week = 0;

    // Se o dia da semana do primeiro dia do mês for maior que 0(primeiro dia da semana);
    while (week < weekDay) {
        td = document.createElement("td");
        empty = document.createTextNode(' ');
        td.appendChild(empty);
        tr.appendChild(td);
        week++;
    }

    // Vai percorrer do 1º até o ultimo dia do mês
    for (let i = 1; i <= lastDay;) {
        // Enquanto o dia da semana for < 7, ele vai adicionar colunas na linha da semana
        while (week < 7) {
            td = document.createElement('td');
            let text = document.createTextNode(i);
            btn = document.createElement('button');
            btn.className = "btn-day";
            btn.addEventListener('click', function (e) { e.preventDefault(); changeDate(this); });
            week++;



            // Controle para ele parar exatamente no ultimo dia
            if (i <= lastDay) {
                i++;
                btn.appendChild(text);
                td.appendChild(btn);
            } else {
                text = document.createTextNode(' ');
                td.appendChild(text);
            }
            tr.appendChild(td);
        }
        // Adiciona a linha na tabela
        table.appendChild(tr);

        // Cria uma nova linha para ser usada
        tr = document.createElement("tr");

        // Reseta o contador de dias da semana
        week = 0;
    }
    // Adiciona a tabela a div que ela deve pertencer
    const content = document.getElementById('table');
    content.appendChild(table);
    changeActive();
    changeHeader(date);
    document.getElementById('txtdate').textContent = date.toISOString();
    getCurrentDate(document.getElementById("currentDate"), true);
    getCurrentDate(document.getElementById("txtdate"), false);
}

// Altera a data atráves do formulário
function setDate(form) {
        if (form.value != "" && form.value != null) {
            let newDate = new Date(form.value);
            date = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            generateCalendar();
            $("#hdnnrseqdayconsulta").val(form.value);
            $("#btnsourcerptconsultas").click();
            return false;
        } else if (form != "" && form != null) {
            let newDate = new Date(form);
            date = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
            generateCalendar();
            $("#hdnnrseqdayconsulta").val(form);
            $("#btnsourcerptconsultas").click();
            return false;
        }
}

// Método Muda o mês e o ano do topo do calendário
function changeHeader(dateHeader) {
    const month = document.getElementById("month-header");
    if (month.childNodes[0]) {
        month.removeChild(month.childNodes[0]);
    }
    const headerMonth = document.createElement("h1");
    const textMonth = document.createTextNode(months[dateHeader.getMonth()].substring(0, 3) + " " + dateHeader.getFullYear());
    headerMonth.appendChild(textMonth);
    month.appendChild(headerMonth);
}

// Função para mudar a cor do botão do dia que está ativo
function changeActive() {
    let btnList = document.querySelectorAll('button.active');
    btnList.forEach(btn => {
        btn.classList.remove('active');
    });
    btnList = document.getElementsByClassName('btn-day');
    for (let i = 0; i < btnList.length; i++) {
        const btn = btnList[i];
        if (btn.textContent === (date.getDate()).toString()) {
            btn.classList.add('active');
        }
    }
}

// Função que pega a data atual
function resetDate() {
    date = new Date();
    generateCalendar();
}

// Muda a data pelo numero do botão clicado
function changeDate(button) {
    let newDay = parseInt(button.textContent);
    date = new Date(date.getFullYear(), date.getMonth(), newDay);
    generateCalendar();
}



// Funções de avançar e retroceder mês e dia
function nextMonth() {
    date = new Date(date.getFullYear(), date.getMonth() + 1, 1);
    generateCalendar(date);
}

function prevMonth() {
    date = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    generateCalendar(date);
}


function prevDay() {
    date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1);
    generateCalendar();
}

function nextDay() {
    date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1);
    generateCalendar();
}


$(document).on("click", function (e) {

    if (!$(e.target).is("#consultasinfo")) {
        $("#consultasinfo").removeClass("active");
    }
    

});

function openconsultasinfo() {
  
    var nrseqconsulta;
    var date = $(".textdate").val();
    nrseqconsulta = date;
    $("#hdnnrseqdayconsulta").val(date);
    $("#btnsourcerptconsultas").click();

    var infochange = localStorage.getItem("btn-info-inner");

    if (infochange === 0) {
        localStorage.setItem("btn-info-inner", $(this).html());
    }

    if (infochange !== $(this).html()) {
        $("#consultasinfo").removeClass("active");
        setTimeout(
            function () {
                $("#consultasinfo").addClass("active");
            }, 200);
    } else {
        if ($("#consultasinfo").hasClass("active")) {
            $("#consultasinfo").removeClass("active");
        } else {
            $("#consultasinfo").addClass("active");
        }
    }

    localStorage.setItem("btn-info-inner", $(this).html());
}

function setactiveconsultasinfo() {
    $("#consultasinfo").addClass("active");
}

//$(document).on("click", ".btn-day.active", function (e) {

//    var nrseqconsulta;
//    var date = $(".textdate").val();
//    nrseqconsulta = date;
//    $("#hdnnrseqdayconsulta").val(date);
   

//    var infochange = localStorage.getItem("btn-info-inner");

//    if (infochange === 0) {
//        localStorage.setItem("btn-info-inner", $(this).html());
//    }

//    if (infochange !== $(this).html()) {
//        $("#consultasinfo").removeClass("active");
//        setTimeout(
//            function () {
//                $("#consultasinfo").addClass("active");
//            }, 200);
//    } else {
//        if ($("#consultasinfo").hasClass("active")) {
//            $("#consultasinfo").removeClass("active");
//        } else {
//            $("#consultasinfo").addClass("active");
//        }
//    }

//    localStorage.setItem("btn-info-inner", $(this).html());
//    $("#btnsourcerptconsultas").click();
//});


$(document).on("click", "#btnsearchdate", function (e) {
    openconsultasinfo();
});

$(document).on("click", ".btn-day", function (e) {
    openconsultasinfo();
});



//$(document).on("click", ".btn-day.active", function (e) {

//    var nrseqconsulta;
//    var date = $("#txtdate").val();
//    nrseqconsulta = date;
//    $("#hdnnrseqdayconsulta").val(date);
//    $("#btnsourcerptconsultas").click();

//    var infochange = localStorage.getItem("btn-info-inner");

//    if (infochange === 0) {
//        localStorage.setItem("btn-info-inner", $(this).html());
//    }

//    if (infochange !== $(this).html()) {
//        $("#consultasinfo").removeClass("active");
//        setTimeout(
//            function () {
//                $("#consultasinfo").addClass("active");
//            }, 200);
//    } else {
//        if ($("#consultasinfo").hasClass("active")) {
//            $("#consultasinfo").removeClass("active");
//        } else {
//            $("#consultasinfo").addClass("active");
//            __doPostBack("#uptconsultasinfo", '');
//        }
//    }

//    localStorage.setItem("btn-info-inner", $(this).html());


//});

$(document).ready(function () {
    $(".btncolorsearchcliente").each(function () {
        var colorbg = $(this).css("background-color");
        $("#cboTipoProcura").append("<option name='optcolor' style='background-color:" + colorbg + ";color:" + colorbg + ";'></option>");
    });


    $("#cboTipoProcura").change(function (e) {

        var selectedCountry = $(this).children("option:selected").css("background-color")
        $("#cboTipoProcura").css("background-color", selectedCountry);
    });

    $("#divdependentes").hide();

});

$(document).on("click", "#btndependente", function () {
    hiderepeaterdependente();
    hidegradedependente();
    if ($("#divdependentes").is(":visible")) {
        $("#divdependentes").hide("slow");
    } else {
        $("#divdependentes").show("slow");
    }

});

/*$(document).on("click","#btnadddependentes",function(){
  if ($("#divrptdependentes").is(":hidden")){
         $("#divgrddependentes").hide("slow");
        $("#divrptdependentes").show("slow");
    }
});*/

$(document).on("click", "#btnchecardependentes", function () {
    if ($("#divgrddependentes").is(":hidden")) {
        $("#divrptdependentes").hide("slow");
        $("#divgrddependentes").show("slow");
    }

});

$(document).on("click", "#grdeditar", function () {
    $("#divgrddependentes").hide("slow");
    $("#divrptdependentes").show("slow");

});


function dependentesalvo(nrseq) {

    $("#hdnnrseqdependente").each(function () {
        if ($(this).val() == nrseq) {
            $(this).closest("#containerdependentes").hide("Slow");
        }
    });
}




function hiderepeaterdependente() {
    $("#divrptdependentes").hide("slow");
}

function hidegradedependente() {
    $("#divgrddependentes").hide("slow");
}

function showrepeaterdependente() {
    $("#divgrddependentes").hide("slow");
    $("#divrptdependentes").show("slow");
}

function showgradedependente() {
    $("#divgrddependentes").show("slow");
}

function fecharlancamentos() {
    $("#divlancar").hide("slow");
}

function abrirlancamentos() {
    $("#divlancar").show("slow");
}


function abrircaixa() {
    $("#divcaixas").show("slow");
}
function fecharcaixa() {
    $("#divcaixas").hide("slow");
}

function hidebtnremovelancamentos() {
    $(".btnexcluilancamento").attr("style", "display:none");
}

function showbtnremovelancamentos() {
    $(".btnexcluilancamento").attr("style", "display:block");
}

$('.datepicker').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy'
});

$(document).ready(function(){
    $('.datepicker').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy'
});



});
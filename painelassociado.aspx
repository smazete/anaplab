﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="painelassociado.aspx.vb" MasterPageFile="~/masterpage3.master" Inherits="autoatendimento_painelassociado" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b> Dados de Associados </b>
        </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Dados Pessoais</h3>
                        <div class="box-tools pull-right">
                            <!-- Buttons, labels, and many other things can be placed here! -->

                            <!-- Here is a label for example -->
                            <span class="label label-primary">
                                <asp:HiddenField runat="server" ID="hdnrseq" />
                                <asp:Label ID="txtnrseq" Text="Nrseq" runat="server">0</asp:Label></span>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <center>
                            <asp:LinkButton ID="btnmensalidade" runat="server" CssClass="btn btn-success btn-xs"  >
                            <i class="fa fa-pencil-square-o"></i> Mensalidade</asp:LinkButton>
                              <asp:LinkButton ID="btnjoias" runat="server" CssClass="btn btn-primary btn-xs" >
                            <i class="fa fa-diamond"></i> Joias </asp:LinkButton>
                              <asp:LinkButton ID="btndocumentos" runat="server" CssClass="btn btn-warning btn-xs"  >
                            <i class="fa fa-files-o"></i> Documentos </asp:LinkButton>
                              <asp:LinkButton ID="btnacoes" runat="server" CssClass="btn btn-info btn-xs" >
                            <i class="fa fa-balance-scale"></i> Ações</asp:LinkButton>
                              <asp:LinkButton ID="btnnegociar" runat="server" CssClass="btn btn-danger btn-xs" >
                            <i class="fa fa-pencil-square-o"></i> Negociar</asp:LinkButton>
                                </center>

                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="Matricula" runat="server"></asp:Label>
                                <asp:TextBox ID="txtmatricula" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-4">
                                <asp:Label Text="Nome" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="E-mail" runat="server"></asp:Label>
                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Nascimento" runat="server"></asp:Label>
                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtnasc" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="CPF" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcpf" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="RG" runat="server"></asp:Label>
                                <asp:TextBox ID="txtrg" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Telddd" runat="server"></asp:Label>
                                <asp:TextBox ID="txttelddd" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Telefone" runat="server"></asp:Label>
                                <asp:TextBox ID="txttelefone" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:Label Text="Sexo" runat="server"></asp:Label>
                                <asp:TextBox ID="txtsexo" runat="server" CssClass="form-control" Text="M"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="CEP" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcep" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:Label Text="Estado" runat="server"></asp:Label>
                                <asp:TextBox ID="txtestado" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label Text="Cidade" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcidade" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Endereço" runat="server"></asp:Label>
                                <asp:TextBox ID="txtendereco" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Bairro" runat="server"></asp:Label>
                                <asp:TextBox ID="txtbairro" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:Label Text="Numero" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Complemento" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcomplemento" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-4">
                                <asp:Label Text="Nome Mãe" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnomemae" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <b>Dados de Acesso </b>
                                    </div>
                                    <div class="card-body">
                                        <br />
                                        <div class="col-lg-4">
                                            <b>
                                                <asp:Label Text="USUARIO:" runat="server"></asp:Label></b>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:TextBox ID="txtmatricula2" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-lg-4">
                                            <b>
                                                <asp:Label Text="SENHA:" runat="server"></asp:Label></b>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:TextBox type="password" ID="txtsenha" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header">
                                        <b>Dados do Associado </b>
                                    </div>
                                    <div class="card-body bg-aqua">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label ID="lblsituaçao" Text="Situação" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox ID="txtsituacaopb" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label Text="Data posse" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtposse" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                                <b>
                                                    <asp:Label Text="Data Aposentadoria" runat="server"></asp:Label>
                                                </b>
                                            </div>
                                            <div class="col-lg-8">
                                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtaposentadoria" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-lg-4">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <b>Dados Bancarios</b>
                                        </div>
                                        <div class="card-body">
                                            <br />
                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label Text="Agencia" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox ID="txtAgencia" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <b>
                                                        <asp:Label Text="Conta corrente" runat="server"></asp:Label>
                                                    </b>
                                                </div>
                                                <div class="col-lg-8">
                                                    <asp:TextBox ID="txtcontacorrente" runat="server" CssClass="form-control"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>
            <!-- /.box-body -->

      </asp:Content>
﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="tipojoias.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="tipojoias" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
<div class="box box-header">
<b> TIPO DE JOIAS </b>
</div>
<div class="box-body">
<div class="row">
<div class="col-lg-12 text-center">
<asp:LinkButton ID = "btnnovo" runat="server" CssClass="btn btn-warning "> <i class="fas fa-plus-circle"></i>Novo</asp:LinkButton>
    <asp:HiddenField runat="server" ID="hdnrseq" />    
<br>
</div>
</div>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>

<div class="row">
<div class="col-lg-2">
Nrseq
<br>
<asp:TextBox id="txtnrseq" runat="server" cssclass="form-control" Enabled="false" ></asp:TextBox>
</div>
<div class="col-lg-2">
Descricao
<br>
<asp:TextBox id="txtdescricao" runat="server" cssclass="form-control" ></asp:TextBox>
</div>
<div class="col-lg-2">
Valor
<br>
<asp:TextBox id="txtvalor" runat="server" cssclass="form-control" ></asp:TextBox>
</div>
<div class="col-lg-2">
Dtcad
<br>
<asp:TextBox id="txtdtcad" runat="server" Enabled="false" cssclass="form-control" ></asp:TextBox>
</div>
</div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
<div class="row">
<div class="col-lg-12 text-center">
<br>
<asp:LinkButton ID = "btnsalvar" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i>Salvar</asp:LinkButton>
</div>
</div>
<div class="row">
<hr />
</div>
<div class="row">
<br>
 <asp:GridView ID ="grade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma doca cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
<columns>
<asp:TemplateField >
 <ItemTemplate>
<asp:HiddenField ID="hdnrseqgrade" runat="server" value='<%#Bind("nrseq")%>'></asp:HiddenField>
 
 </ItemTemplate>
</asp:TemplateField >
                                <asp:BoundField DataField="nrseq" HeaderText="Nrseq" />
                                <asp:BoundField DataField="descricao" HeaderText="Descricao" />
                                <asp:BoundField DataField="valor" HeaderText="Valor" />
                                <asp:BoundField DataField="ativo" HeaderText="Ativo" />
    
<asp:TemplateField >
 <ItemTemplate>
    <div runat="server" id="divativar">

                                       <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-edit"></i></asp:LinkButton>                                
                              
                                     
                                   <asp:LinkButton ID="cancelar" runat="server" CssClass="btn btn-danger btn-xs" CommandName="cancelar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-close"></i></asp:LinkButton>
                                </div>
                                <div runat="server" id="divcancelado">
                                      <asp:LinkButton ID="ativar" runat="server" CssClass="btn btn-success btn-xs" CommandName="ativar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-check"></i></asp:LinkButton>

                                </div>
 </ItemTemplate>
</asp:TemplateField >
 </columns>
</asp:GridView >
</div>
</div>
</div>


</asp:Content>
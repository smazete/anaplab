﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gerarlotemensalidade.aspx.vb" Inherits="gerarlotemensalidade"  MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary">
        <div class="box box-header ">
            <b> Gerar Mensalidades</b>
        </div>
        <div class="box-body"> 
                    <br />
                    <div class="box box-primary">
                        <div class="box-header">
                            Procurar cliente
                        </div>
                        <div class="box-body">
                            <div class="row ">
                                <div class="col-lg-2">
                                </div>

                                <div class="col-lg-2 fix-txt">
                                    <asp:Label runat="server" Text="Tipo"></asp:Label>
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="cboTipoProcura">
                                        <asp:ListItem Value="1">Matricula</asp:ListItem>
                                        <asp:ListItem Value="2">Codigo</asp:ListItem>
                                        <asp:ListItem Value="2">Nome</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="col-lg-3 fix-txt">
                                    <asp:Label runat="server" Text="Descricao"></asp:Label>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtprocurar"></asp:TextBox>
                                </div>
                                <div class="col-lg-1">
                                    <asp:CheckBox runat="server" Text="Inativo" CssClass="checkbox-inline" Checked="false" ID="chkexibirinativos" />
                                </div>
                                <div class="col-lg-2">
                                    <asp:LinkButton runat="server" Text="Procurar" CssClass="btn btn-primary" ID="btnprocurar"></asp:LinkButton>
                                </div>

                                <div class="col-lg-2"></div>

                            </div>
                            <br />



                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:GridView ID="GradeCaixas" runat="server" CssClass="table table-striped" ShowHeaderWhenEmpty="true" EmptyDataText="Não possui nenhuma caixa." AutoGenerateColumns="false" GridLines="None">
                                        <Columns>
                                            <asp:BoundField DataField="nrseq" HeaderText="Nr Interno" />
                                            <asp:BoundField DataField="dtcad" HeaderText="Data de Cadastro" />
                                            <asp:BoundField DataField="dtvencimento" HeaderText="Data de Vencimento" />
                                            <asp:BoundField DataField="operacao" HeaderText="Operação" />
                                            <asp:BoundField DataField="nrseqplano" HeaderText="Código" />
                                            <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                            <asp:BoundField DataField="valor" HeaderText="Valor" />
                                            <asp:BoundField DataField="parcela" HeaderText="Parcela" />
                                            <asp:BoundField DataField="qtdparcelas" HeaderText="Parcelas" />
                                            <asp:TemplateField HeaderText="Ações" runat="server">
                                                <ItemTemplate runat="server">
                                                     <asp:LinkButton ID="btneditar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                            <i class="fa fa-pencil-square-o"></i>
                                                    </asp:LinkButton>
                                                   <%-- <asp:LinkButton ID="Baixar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Baixar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                            <i class="fa fa-pencil-square-o"></i>
                                                    </asp:LinkButton>--%>
                                              <%--      <asp:LinkButton ID="btnexcluir" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Remover" CommandArgument='<%#Container.DataItemIndex%>'>
                                                        <i runat="server" id="icogradeexcluir" class="fa fa-trash-o"></i>
                                                    </asp:LinkButton>--%>
                                                    <asp:HiddenField runat="server" ID="hdnnrseqgrade" Value='<%#Bind("nrseq") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

<%--                    <div class="row text-align-center">
                        <div class="col-lg-12">
                            <asp:Button ID="btnnovo" runat="server" Text="Novo" CssClass="btn btn-primary" Enabled="true" />
                        </div>
                    </div>--%>
                    <br />
                    <div class="row">
                        <div class="col-lg-1">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Codigo"></asp:Label>
                                <asp:TextBox runat="server" ID="txtnrseq" Enabled="false" CssClass="form-control"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hdnnrseq" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Data Cadastro"></asp:Label>
                            <asp:TextBox ID="txtdtcadastro" Enabled="false" type="date" runat="server" onkeyup="formataPlanoContas(this,event);" cssclass="form-control" MaxLength="9" AutoPostBack="True"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Data Vencimento"></asp:Label>
                            <asp:TextBox ID="dtvencimento" Enabled="false" type="date" runat="server" onkeyup="formataPlanoContas(this,event);" cssclass="form-control" MaxLength="9" AutoPostBack="True"></asp:TextBox>
                        </div>

                        <div class="col-lg-3">
                            <asp:Label runat="server" Text="Status"></asp:Label>
                             <asp:TextBox ID="txtstatus" Enabled="false" runat="server"  cssclass="form-control" MaxLength="9" AutoPostBack="True"></asp:TextBox>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label runat="server" Text="parcelas"></asp:Label>
                            <asp:DropDownList ID="cboformapagto" runat="server" Enabled="false" cssclass="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>                        
                        <div class="col-lg-1">
                            <asp:CheckBox ID="chksolaprovacao" CssClass="checkbox-inline fix-btn" Text="Iniciar mes atual" runat="server"></asp:CheckBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Valor"></asp:Label>
                             <asp:TextBox ID="TextBox1" Enabled="false" runat="server"  cssclass="form-control" MaxLength="9" AutoPostBack="True"></asp:TextBox>
                        </div>
                    </div>
                    <br />

                    <div class="row">  

                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:FileUpload runat="server" ID="Filedocumento" CssClass="custom-file-input" />
                        </div>
<%--                        <div class="col-lg-6">
                            <asp:Button ID="btnanexar" runat="server" Text="Anexar Documentos" CssClass="btn btn-warning" Enabled="true" />
                        </div>--%>
                    </div>
                    <br />

                    <div class="row text-align-center">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-2">
                            <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="btn btn-success" Enabled="false" />
                        </div>

                        <div class="col-lg-2">
                            <asp:Button ID="btncancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger " Enabled="true" />


                        </div>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    
</asp:Content>
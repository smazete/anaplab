﻿Imports clsSmart
Imports System.IO
Imports smboletos

Partial Class lerextratocsv
    Inherits System.Web.UI.Page

    Private Sub lerextratocsv_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If

        abastecer()
    End Sub

    Private Sub btnler_Click(sender As Object, e As EventArgs) Handles btnler.Click
        If fileextrato.HasFile Then
            Dim wcnomedestino As String = alteranome(Server.MapPath("~") & "arquivos\arquivo.csv")
            fileextrato.SaveAs(wcnomedestino)
            Dim wcconteudo As String = ""
            Dim xler As New StreamReader(wcnomedestino, Encoding.UTF8)

            While xler.EndOfStream = False
                wcconteudo &= (xler.ReadLine() & Chr(10))
            End While
            xler.Close()

            Dim xbol As New clsBoletos
            If xbol.LerEntradas(wcconteudo) Then
                gradeextrato.DataSource = xbol.ListaLancamentos
                gradeextrato.DataBind()

                'xbol.ListaLancamentos(x).Valor

                For x As Integer = 0 To xbol.ListaLancamentos.Count - 1
                    '       Dim xMatricula As String = xbol.ListaLancamentos(x).Dth(0).Matricula
                    '       Dim xAgencia As String = xbol.ListaLancamentos(x).Dth(0).Agencia
                    '      Dim xNome As String = xbol.ListaLancamentos(x).Dth(0).Nome
                Next
            End If
        End If
    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub gradeextrato_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeextrato.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeextrato.Rows(index)
        Dim agencia As HiddenField = row.FindControl("hdnrseqagencia")
        Dim conta As HiddenField = row.FindControl("hdnrseqconta")

    End Sub

    Private Sub gradeextrato_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeextrato.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If

        Dim div As HtmlGenericControl = e.Row.FindControl("divinativo")
        Dim chkok As CheckBox = e.Row.FindControl("chkok")



        If e.Row.Cells(5).Text = "C" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(5).Text = "D" Then
            div.Visible = False
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightSalmon
        End If



        If e.Row.Cells(6).Text = "20" Then
            e.Row.Cells(8).Text = "Automatico"
            e.Row.BackColor = System.Drawing.Color.LightGreen
            chkok.Checked = True
        Else
            e.Row.Cells(8).Text = "Manual"
            chkok.Checked = False
        End If

        If e.Row.Cells(7).Text <> "" Then
            e.Row.Cells(7).Text = Replace(e.Row.Cells(7).Text, "-", "")
        End If
        If e.Row.Cells(1).Text = "S A L D O" Then
            e.Row.Cells(8).Text = "invalida"
            div.Visible = False
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.Gray
        End If

    End Sub


    Private Sub btnconfirmar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnconfirmar.Click

        Dim descrifim As String = ""
        Dim vlnaoencontradox As String = ""
        Dim vlbaixamanualx As String = ""
        Dim vlbaixaautox As String = ""
        Dim vlerrox As String = ""
        Dim descridois As String = ""
        Dim descritres As String = ""
        Dim obssalva As String = ""
        Dim contdeb As Double
        Dim um As Double = 1
        Dim proximo As String

        Try


            For Each row As GridViewRow In gradeextrato.Rows
                Dim agencia As HiddenField = row.FindControl("hdnrseqagencia")
                Dim Contacorrente As HiddenField = row.FindControl("hdoutros")
                Dim valor As HiddenField = row.FindControl("hdnrseqvalor")
                Dim operacao As HiddenField = row.FindControl("hdoperacao")
                Dim chkok As CheckBox = row.FindControl("chkok")

                If valor.Value <> "0" Then
                    If chkok.Checked = "true" Then

                        'operasão for igual a debito nao faz interação com o sistema

                        If operacao.Value = "D" Then
                            contdeb = contdeb + um
                        Else

                            Dim xAssociados As New clsAssociado
                            Dim xMensalidade As New clsmensalidades
                            Dim ximportcsv As New clsimportcsv

                            xAssociados.Agencia = agencia.Value
                            Dim valores As Integer = valor.Value
                            xAssociados.Contacorrente = Replace(Contacorrente.Value, "-", "")
                            proximo = ""
                            'verifica se a contacorrente existe e se existir  pula = proximo estara vazio
                            If Not xAssociados.verificaseexiste() Then

                                vlnaoencontradox = vlnaoencontradox + "A:" & agencia.Value & "|C:" + Contacorrente.Value + "</br>"
                                obssalva = "Agencia ou Conta Não corresponde a um cadastro"
                                ximportcsv.Obs = obssalva
                                ximportcsv.Nomecliente = "Não encontrado"
                                ximportcsv.Agencia = agencia.Value
                                ximportcsv.Contacorrente = Contacorrente.Value
                                ximportcsv.Data = txtdtbaixa.Text
                                xMensalidade.Dtpg = txtdtbaixa.Text
                                ximportcsv.Operacao = operacao.Value
                                ' ximportcsv.Outrosdados = outrosdados.Value
                                ximportcsv.Valor = valores

                                If Not ximportcsv.salvarmanual() Then
                                    vlerrox = vlerrox + "erromanual" + agencia.Value + Contacorrente.Value + "</br>"
                                    proximo = "false"
                                End If

                                vlbaixamanualx = vlbaixamanualx + "A:" & agencia.Value & "|C:" + Contacorrente.Value + "</br>"
                                proximo = "false"
                            End If

                            If proximo = "" Then
                                xMensalidade.Nrseqcliente = xAssociados.Nrseq
                                xMensalidade.Valor = valor.Value

                                Dim nomeenquanto As String = xAssociados.Nome

                                If valores <> "20" Then
                                    descrifim = descrifim + nomeenquanto + "</br>"
                                    obssalva = "O valor de: '" & valor.Value & "' não pode ser automatico referente ao cliente:'" & nomeenquanto & "'"
                                Else

                                    ximportcsv.Nrseqcliente = xAssociados.Nrseq
                                    ximportcsv.Nomecliente = xAssociados.Nome
                                    'ximportcsv.Outrosdados = outrosdados.Value
                                    ximportcsv.Agencia = agencia.Value
                                    ximportcsv.Contacorrente = Contacorrente.Value
                                    ximportcsv.Data = txtdtbaixa.Text
                                    xMensalidade.Dtpg = txtdtbaixa.Text
                                    ximportcsv.Valor = valores
                                    ximportcsv.Operacao = operacao.Value
                                    ximportcsv.Obs = obssalva

                                    'ximportcsv.Nomearquivo = 

                                    ' pega  as informações dos dados da importação do cliente e validada se consguiu encontrar uma mensalidade retorna mensalidade e salva com as demais infomrações da tabela  salvar auto ja tem salvarmensalidadeimportada
                                    If Not xMensalidade.salvarimporta() Then
                                        ximportcsv.Ativo = True
                                    End If

                                    xMensalidade.Nrseqcliente = xMensalidade.Nrseqcliente

                                    ximportcsv.Ativo = False

                                    ximportcsv.Nrseqmensalidade = xMensalidade.Nrseq


                                    vlbaixaautox = vlbaixaautox + agencia.Value + Contacorrente.Value + "</br>"

                                    If Not ximportcsv.salvarauto() Then
                                        vlerrox = vlerrox + "erroauto" + agencia.Value + Contacorrente.Value + "</br>"
                                    End If



                                End If

                            End If



                        End If

                    Else

                        'operasão for igual a debito nao faz interação com o sistema

                        If operacao.Value = "D" Then
                            contdeb = contdeb + um
                        Else

                            Dim xAssociados As New clsAssociado
                            Dim xMensalidade As New clsmensalidades
                            Dim ximportcsv As New clsimportcsv

                            xAssociados.Agencia = agencia.Value
                            Dim valores As Integer = valor.Value
                            xAssociados.Contacorrente = Replace(Contacorrente.Value, "-", "")

                            If Not xAssociados.verificaseexiste() Then

                                vlnaoencontradox = vlnaoencontradox + "A:" & agencia.Value & "|C:" + Contacorrente.Value + "</br>"
                                obssalva = "Agencia ou Conta Não corresponde a um cadastro"
                                ximportcsv.Obs = obssalva
                                ximportcsv.Nomecliente = "Não encontrado"
                                ximportcsv.Agencia = agencia.Value
                                ximportcsv.Contacorrente = Contacorrente.Value
                                ximportcsv.Data = txtdtbaixa.Text
                                xMensalidade.Dtpg = txtdtbaixa.Text
                                ximportcsv.Operacao = operacao.Value
                                ' ximportcsv.Outrosdados = outrosdados.Value
                                ximportcsv.Valor = valor.Value

                            End If


                            ximportcsv.Nrseqcliente = xAssociados.Nrseq
                            ximportcsv.Nomecliente = xAssociados.Nome
                            'ximportcsv.Outrosdados = outrosdados.Value
                            ximportcsv.Agencia = agencia.Value
                            ximportcsv.Contacorrente = Contacorrente.Value
                            ximportcsv.Data = txtdtbaixa.Text
                            xMensalidade.Dtpg = txtdtbaixa.Text
                            ximportcsv.Valor = valores
                            ximportcsv.Operacao = operacao.Value
                            ximportcsv.Obs = obssalva

                            If Not ximportcsv.salvarmanual() Then
                                vlerrox = vlerrox + "erromanual" + agencia.Value + Contacorrente.Value + "</br>"
                            End If

                            vlbaixamanualx = vlbaixamanualx + agencia.Value + Contacorrente.Value + "</br>"

                        End If

                    End If
                End If
            Next

            vlnaoencontrado.Text = vlnaoencontradox
            vlbaixaauto.Text = vlbaixaautox
            vlbaixamanual.Text = vlbaixamanualx
            vlerro.Text = vlerrox
            txtcontadeb.Text = contdeb


            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Valores nao podem ser carregados" & descrifim & "',  showConfirmButton: ok})")

        Catch ex As Exception
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Valores nao podem ser carregados" & descrifim & "',  showConfirmButton: ok})")
        End Try
    End Sub

    Private Sub abastecer()
        Dim dateInMay As New System.DateTime(1993, 5, 31, 12, 14, 0)

        txtdtbaixa.Text = FormatDateTime(hoje())

    End Sub
End Class

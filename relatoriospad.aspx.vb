﻿Imports System.IO
Imports clsSmart

Partial Class relatoriospad
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Private Sub painel_Load(sender As Object, e As EventArgs) Handles Me.Load


        If IsPostBack Then Exit Sub
        carregarcolaboradores()
        carregarcidade()
        carregagestores()
        carregaravaliacoes()
        carregarsituacoes()

        If Session("gestor") = "Sim" Then
            divgestor.Style.Add("display", "none")
        End If
        '   txtano.Text = data.Year
    End Sub
    Private Sub carregarsituacoes()
        tb1 = tab1.conectar("select distinct status from vwcolaboradores order by status")
        chksituacoes.Items.Clear()
        For x As Integer = 0 To tb1.Rows.Count - 1
            chksituacoes.Items.Add(tb1.Rows(x)("status").ToString)
        Next
    End Sub
    Private Sub carregarcidade()
        tb1 = tab1.conectar("select distinct empresa from vwcolaboradores order by empresa")
        cbocidade.Items.Clear()
        cbocidade.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbocidade.Items.Add(tb1.Rows(x)("empresa").ToString)
        Next
    End Sub
    Private Sub carregaravaliacoes()
        cboavaliacaofinal.Items.Clear()
        Dim xsql As String
        xsql = "select distinct descricao from tbresultados_finais where ativo = true  order by descricao "

        tb1 = tab1.conectar(xsql)
        cboavaliacaofinal.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cboavaliacaofinal.Items.Add(tb1.Rows(x)("descricao").ToString)
        Next
    End Sub
    Private Sub carregagestores()
        Dim xsql As String
        xsql = "select distinct nome from vwcolaboradores where gestor = true   "
        If Not Session("master") = "Sim" Then
            xsql &= " And nrseqgestor = " & Session("idusuario")
        End If
        xsql &= " order by nome"
        tb1 = tab1.conectar(xsql)
        cbogestor.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbogestor.Items.Add(tb1.Rows(x)("nome").ToString)
        Next
    End Sub
    Private Sub carregarcolaboradores()
        Dim xsql As String
        xsql = "select distinct nome from vwcolaboradores where 1=1   "
        If Not Session("master") = "Sim" Then
            xsql &= " And nrseqgestor = " & Session("idusuario")
        End If
        xsql &= " order by nome"
        tb1 = tab1.conectar(xsql)
        txtcolaborador.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            txtcolaborador.Items.Add(tb1.Rows(x)("nome").ToString)
        Next
    End Sub
    Private Sub btnProcurar_Click(sender As Object, e As EventArgs) Handles btnProcurar.Click



        Dim xsql As String = "select * from vwpad where ativo = true "
        ' Dim xsql As String = "select * from vwpad where 1=1 "
        If Session("gestor") = "Sim" AndAlso Session("master") = "Não" Then
            xsql &= " and nrseqgestor = " & Session("nrseqcolaborador")

        End If

        Dim xsqlsituacao As String = ""
        If chksituacoes.Items.Count > 0 Then
            For x As Integer = 0 To chksituacoes.Items.Count - 1
                If chksituacoes.Items(x).Selected Then
                    xsqlsituacao &= " status = '" & chksituacoes.Items(x).Text & "' or "
                End If
            Next
            xsql &= " and (" & xsqlsituacao.Substring(0, xsqlsituacao.Length - 3) & ")"
        End If

        If cbocidade.Text <> "" Then
            xsql &= " and cidade = '" & cbocidade.Text & "'"
        End If
        If cbogestor.Text <> "" Then
            xsql &= " and nomegestor = '" & cbogestor.Text & "'"
        End If
        If cboavaliacaofinal.Text <> "" Then
            xsql &= " and descricaoresultado = '" & cboavaliacaofinal.Text & "'"
        End If
        If txtcolaborador.Text <> "" Then
            xsql &= " and nomecolaborador like '%" & tratatexto(txtcolaborador.Text) & "%'"
        End If
        If sonumeros(txtano.Text).Length <> 0 Then
            xsql &= " and anobase = " & sonumeros(txtano.Text)
        End If





        Select Case cboetapas.SelectedItem.Text.ToLower
            Case Is = "Com Objetivos".ToLower
                xsql &= " and totalavaliacoes1 > 0 "
            Case Is = "Sem Objetivos".ToLower
                xsql &= " and nrseq is null"
            Case Is = "Com revisão de meio de ano".ToLower
                xsql &= " and totalavaliacoes2 > 0 "
            Case Is = "Sem revisão de meio de ano".ToLower
                xsql &= " and totalavaliacoes2 = 0  "
            Case Is = "Com revisão de final de ano".ToLower
                xsql &= " and totalavaliacoes3 > 0"
            Case Is = "Sem revisão de meio de ano".ToLower
                xsql &= " and totalavaliacoes3 = 0"
        End Select
        xsql &= " order by nomecolaborador"
        tb1 = tab1.conectar(xsql)
        If tb1.Rows.Count = 0 Then
            sm("swal.fire({title: 'Ops!',text: 'Desculpe, nenhum PAD encontrado com essas informações !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        grade.DataSource = tb1
        grade.DataBind()

        dgexcel.DataSource = tb1
        dgexcel.DataBind()
    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        Dim lblitem As Label = e.Row.FindControl("lblitem")
        Dim lbldtadmissao As Label = e.Row.FindControl("lbldtadmissao")
        lblitem.Text = e.Row.RowIndex + 1
        '
        lbldtadmissao.Text = e.Row.DataItem("dtadmissao").ToString
        lbldtadmissao.Text = FormatDateTime(lbldtadmissao.Text, DateFormat.ShortDate)

        If e.Row.Cells(5).Text > 0 Then
            e.Row.Cells(5).Text = "Sim"
        Else
            e.Row.Cells(5).Text = "Não"
        End If
        If e.Row.Cells(6).Text > 0 Then
            e.Row.Cells(6).Text = "Sim"
        Else
            e.Row.Cells(6).Text = "Não"
        End If
        If e.Row.Cells(7).Text > 0 Then
            e.Row.Cells(7).Text = "Sim"
        Else
            e.Row.Cells(7).Text = "Não"
        End If
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        Dim xsql As String = "select * from vwpad where 1=1 "
        ' Dim xsql As String = "select * from vwpad where 1=1 "
        If Session("gestor") = "Sim" AndAlso Session("master") = "Não" Then
            xsql &= " and nrseqgestor = " & Session("nrseqcolaborador")

        End If

        Dim xsqlsituacao As String = ""
        If chksituacoes.Items.Count > 0 Then
            For x As Integer = 0 To chksituacoes.Items.Count - 1
                xsqlsituacao &= " status = '" & chksituacoes.Items(x).Text & "' or "
            Next
            xsql &= " and (" & xsqlsituacao.Substring(0, xsqlsituacao.Length - 3) & ")"
        End If

        If cbocidade.Text <> "" Then
            xsql &= " and cidade = '" & cbocidade.Text & "'"
        End If
        If cbogestor.Text <> "" Then
            xsql &= " and nomegestor = '" & cbogestor.Text & "'"
        End If
        If cboavaliacaofinal.Text <> "" Then
            xsql &= " and descricaoresultado = '" & cboavaliacaofinal.Text & "'"
        End If
        If txtcolaborador.Text <> "" Then
            xsql &= " and nomecolaborador like '%" & tratatexto(txtcolaborador.Text) & "%'"
        End If
        If sonumeros(txtano.Text).Length <> 0 Then
            xsql &= " and anobase = " & sonumeros(txtano.Text)
        End If





        Select Case cboetapas.SelectedItem.Text.ToLower
            Case Is = "Com Objetivos".ToLower
                xsql &= " and totalavaliacoes1 > 0 "
            Case Is = "Sem Objetivos".ToLower
                xsql &= " and nrseq is null"
            Case Is = "Com revisão de meio de ano".ToLower
                xsql &= " and totalavaliacoes2 > 0 "
            Case Is = "Sem revisão de meio de ano".ToLower
                xsql &= " and totalavaliacoes2 = 0  "
            Case Is = "Com revisão de final de ano".ToLower
                xsql &= " and totalavaliacoes3 > 0"
            Case Is = "Sem revisão de meio de ano".ToLower
                xsql &= " and totalavaliacoes3 = 0"
        End Select
        xsql &= " order by nomecolaborador"
        tb1 = tab1.conectar(xsql)
        If tb1.Rows.Count = 0 Then
            sm("swal.fire({title: 'Ops!',text: 'Desculpe, nenhum PAD encontrado com essas informações !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        grade.DataSource = tb1
        grade.DataBind()

        dgexcel.DataSource = tb1
        dgexcel.DataBind()
        Session("sql") = xsql
        clssessoes.gravarcookie("sql", xsql)
        clssessoes.versessao()
        Dim link As String
        link = "irlink('" & Request.Url.OriginalString.ToString.Replace(Request.Url.PathAndQuery, "") & "/imprimirRelPad.aspx');"
        sm(link, link)
    End Sub

    Private Sub dgexcel_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgexcel.ItemDataBound
        If e.Item.ItemIndex < 0 Then Exit Sub

        If IsDate(e.Item.Cells(8).Text) Then
            If (CDate(e.Item.Cells(8).Text).Month >= 10 And CDate(e.Item.Cells(8).Text).Year = Session("anobaseuso")) OrElse CDate(e.Item.Cells(8).Text).Year > Session("anobaseuso") Then
                e.Item.Cells(11).Text = "Nao se aplica"
                e.Item.Cells(12).Text = "Nao se aplica"
                e.Item.Cells(13).Text = "Não se aplica"
                e.Item.Cells(10).Text = "Não se aplica"
                e.Item.ForeColor = System.Drawing.Color.Red
                Exit Sub
            End If
        End If



        If e.Item.Cells(11).Text > 0 Then
            e.Item.Cells(11).Text = "Sim"
        Else

            e.Item.Cells(11).Text = "Nao"
        End If
        If e.Item.Cells(12).Text > 0 Then
            e.Item.Cells(12).Text = "Sim"
        Else
            e.Item.Cells(12).Text = "Nao"
        End If
        If e.Item.Cells(13).Text > 0 Then
            e.Item.Cells(13).Text = "Sim"
        Else
            e.Item.Cells(13).Text = "Nao"
        End If
        If e.Item.Cells(13).Text = "Sim" AndAlso e.Item.Cells(12).Text = "Sim" AndAlso e.Item.Cells(11).Text = "Sim" Then
            'e.Item.ForeColor = System.Drawing.Color.White
            'e.Item.BackColor = System.Drawing.Color.Green
        Else
            'e.Item.ForeColor = System.Drawing.Color.White
            'e.Item.BackColor = System.Drawing.Color.Red
            'For x As Integer = 11 To 13
            '    If e.Item.Cells(x).Text = "Sim" Then
            '        e.Item.Cells(x).BackColor = System.Drawing.Color.Green
            '    End If
            'Next
        End If
        e.Item.Cells(15).Text = Server.HtmlEncode(e.Item.Cells(15).Text.ToUpper)
    End Sub

    Private Sub btnexcel_Click(sender As Object, e As EventArgs) Handles btnexcel.Click


        Exit Sub
        Dim xsql As String = "select * from vwpad_relatorios3 where 1=1 "
        ' Dim xsql As String = "select * from vwpad where 1=1 "
        If Session("gestor") = "Sim" AndAlso Session("master") = "Não" Then
            xsql &= " and nrseqgestor = " & Session("nrseqcolaborador")

        End If
        If cbocidade.Text <> "" Then
            xsql &= " and empresa = '" & cbocidade.Text & "'"
        End If
        If cbogestor.Text <> "" Then
            xsql &= " and nomegestor = '" & cbogestor.Text & "'"
        End If
        If cboavaliacaofinal.Text <> "" Then
            xsql &= " and descricaoresultado = '" & cboavaliacaofinal.Text & "'"
        End If
        If txtcolaborador.Text <> "" Then
            xsql &= " and nomecolaborador like '%" & tratatexto(txtcolaborador.Text) & "%'"
        End If
        If sonumeros(txtano.Text).Length <> 0 Then
            xsql &= " and anobase = " & sonumeros(txtano.Text)
        End If


        Dim xpad As New clspad

        xpad.Anobase = txtano.Text

        xpad.relatorio()


        dgexcel.DataSource = tb1
        dgexcel.DataBind()
    End Sub
End Class

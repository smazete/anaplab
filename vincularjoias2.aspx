﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="vincularjoias2.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="vincularjoias2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Controle Joias 2 </b>
        </div>
        <div class="box-body">
            <%-- escolhe associado --%>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>

                    <div class="col-lg-6">
                        <div class="row" runat="server" id="divcliente" visible="true">

                            <div class="col-lg-3">
                                <asp:Label Text="Buscar Por" runat="server" />
                                <asp:DropDownList runat="server" CssClass="form-control" ID="cbobusca">
                                    <asp:ListItem Text="Codigo" Value="nrseq" />
                                    <asp:ListItem Text="Conta" Value="contacorrente" />
                                    <asp:ListItem Text="Nome" Value="nome" />
                                    <asp:ListItem Text="Matricula" Value="matricula" />
                                    <asp:ListItem Text="CPF" Value="cpf" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label Text="Buscar Cliente" runat="server" />
                                <asp:TextBox runat="server" ID="txtbusca" CssClass="form-control" />
                            </div>
                            <div class="col-lg-2 text-center">
                                <br>
                                <asp:LinkButton ID="btnbuscarassociado" runat="server" CssClass="btn btn-primary"><i class="fa fa-search"></i>Procurar</asp:LinkButton>
                            </div>
                        </div>
                        <div class="row" runat="server" id="divgradeassociados">
                            <asp:GridView ID="gradeassociados" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdmatricula" runat="server" Value='<%#Bind("matricula")%>'></asp:HiddenField>
                                            <asp:LinkButton ID="usar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="usar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                    <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                    <asp:BoundField DataField="nome" HeaderText="Nome" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="row" runat="server" id="divjoiascliente">
                            <asp:GridView ID="gradejoias" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma joia existente para este cliente !" CssClass="table " GridLines="none" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
                                            <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                            <asp:HiddenField ID="hdnrsequser" runat="server" Value='<%#Bind("nrsequser")%>'></asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                    <asp:BoundField DataField="ativo" HeaderText="Ativo" Visible="false" />
                                    <asp:BoundField DataField="nome" HeaderText="Cliente" />
                                    <asp:BoundField DataField="statuspg" HeaderText="Status" />
                                    <asp:BoundField DataField="mes" HeaderText="Mes" />
                                    <asp:BoundField DataField="ano" HeaderText="Ano" />
                                    <asp:BoundField DataField="descricaotipojoia" HeaderText="Descricao Joia" Visible="false" />
                             
                                </Columns>
                            </asp:GridView>
                        </div>



                    </div>


                    <%-- 
                
                metade da te dados doassociado selecionado
                
                
                    --%>
                    <div class="col-lg-6">

                        <div class="row" id="divassociado" runat="server" visible="false">
                            <asp:HiddenField runat="server" ID="hdnruser" />
                            <div class="col-md-12">
                                Nome
                        <asp:Label ID="lblnome" Text="nome sobrenome" runat="server" CssClass="form-control" Enabled="false" />
                            </div>

                            <div class="col-md-2">
                                Código
                        <asp:Label ID="lblcodigo" Text="0" runat="server" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="col-md-4">
                                CPF
                        <asp:Label ID="lblcpf" Text="000.000.000-00" runat="server" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="col-md-3">
                                Matricula
                        <asp:Label ID="lblmatricula" Text="0" runat="server" CssClass="form-control" Enabled="false" />
                            </div>

                            <div class="col-md-3">
                                  <p>  <asp:CheckBox ID="chkciente" runat="server" AutoPostBack="true" />Estou ciente</p>          
                                    <asp:LinkButton ID="btntodos" runat="server" CssClass="btn btn-danger" Enabled="false"> <i class="fa fa-ban"></i> Excluir</asp:LinkButton>
                            </div>

                        </div>
                        <div id="divgerarjoias" runat="server" class="row" visible="false">
                            <div class="col-md-3">
                                <asp:Label Text="Mes Inicio" runat="server" />
                                <asp:DropDownList runat="server" CssClass="form-control" ID="cbomes">
                                    <asp:ListItem />
                                    <asp:ListItem Text="Janeiro" Value="1" />
                                    <asp:ListItem Text="Fevereiro" Value="2" />
                                    <asp:ListItem Text="Março" Value="3" />
                                    <asp:ListItem Text="Abril" Value="4" />
                                    <asp:ListItem Text="Maio" Value="5" />
                                    <asp:ListItem Text="Junho" Value="6" />
                                    <asp:ListItem Text="Julho" Value="7" />
                                    <asp:ListItem Text="Agosto" Value="8" />
                                    <asp:ListItem Text="Setembro" Value="9" />
                                    <asp:ListItem Text="Outubro" Value="10" />
                                    <asp:ListItem Text="Novembro" Value="11" />
                                    <asp:ListItem Text="Dezembro" Value="12" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <asp:Label Text="Ano" runat="server" />
                                <asp:TextBox ID="txtano" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <asp:Label Text="QT Parcela" runat="server" />
                                <asp:TextBox ID="txtparcela" Text="5" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-md-2">
                                <br />
                                <asp:LinkButton ID="btngerajoias" runat="server" CssClass="btn btn-success" Enabled="false"><i class="fa fa-dollar"></i> Gerar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </div>



</asp:Content>

﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage.master" CodeFile="relatorioclientes.aspx.vb" Inherits="relatorioclientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function fecharmodelo() {
            //$('.fechar').click(function (ev) {

            $(".windowstatusmodelo").hide();
            $(".windowstatusmodelo").closest;

            //}

        }
        function exibirmodelo() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);
            $('#enviaremail').show();
            $('#enviaremail').load();

        }
        function fecharmodalstatus() {
            //$('.fechar').click(function (ev) {
            $("#mascara").hide();
            $(".windowstatus").hide();
            $(".infoconsulta").hide();
            $(".windowstatus").closest;

            //}

        }
        function exibirnovostatus() {
            var alturaTela = $(document).height();
            var larguraTela = $(window).width();

            // colocando o fundo preto
            $('#mascara').css({ 'width': larguraTela, 'height': alturaTela });
            $('#mascara').fadeIn(1000);
            $('#mascara').fadeTo("slow", 0.8);

            //      alert('Registro gravado com sucesso2!');

            var left = ($(window).width() / 2) - ($('#alterarststatus').width() / 4);
            var top = ($(window).height() / 2) - ($('#alterarststatus').height() / 4);

            $('#alterarststatus').css({ 'left': left });
            //$('#embarquemaritimo').css({ 'margin-top': '430px' });
            $('#alterarststatus').show();
            $('#alterarststatus').load();
        }
        function fecharmodalmsg() {
            //$('.fechar').click(function (ev) {
            $("#mascara").hide();
            $(".windowmsg").hide();
            $(".infoconsulta").hide();
            $(".windowmsg").closest;

            //}

        }
        function exibirnovomsg() {
            var alturaTela = $(document).height();
            var larguraTela = $(window).width();

            // colocando o fundo preto
            $('#mascara').css({ 'width': larguraTela, 'height': alturaTela });
            $('#mascara').fadeIn(1000);
            $('#mascara').fadeTo("slow", 0.8);

            //      alert('Registro gravado com sucesso2!');

            var left = ($(window).width() / 2) - ($('#msg').width() / 4);
            var top = ($(window).height() / 2) - ($('#msg').height() / 4);

            $('#msg').css({ 'left': left });
            //$('#embarquemaritimo').css({ 'margin-top': '430px' });
            $('#msg').show();
            $('#msg').load();
        }
        function fecharenviado() {
            //$('.fechar').click(function (ev) {
            $("#mascara").hide();
            $(".windowstatus").hide();
            $(".infoconsulta").hide();
            $(".windowstatus").closest;

            //}

        }
        function exibirenviado() {
            var alturaTela = $(document).height();
            var larguraTela = $(window).width();

            // colocando o fundo preto
            $('#mascara').css({ 'width': larguraTela, 'height': alturaTela });
            $('#mascara').fadeIn(1000);
            $('#mascara').fadeTo("slow", 0.8);

            //      alert('Registro gravado com sucesso2!');

            var left = ($(window).width() / 2) - ($('#enviado').width() / 4);
            var top = ($(window).height() / 2) - ($('#enviado').height() / 4);

            $('#enviado').css({ 'left': left });
            //$('#embarquemaritimo').css({ 'margin-top': '430px' });
            $('#enviado').show();
            $('#enviado').load();
        }
        function fecharmodallogin() {
            //$('.fechar').click(function (ev) {
            $("#mascara").hide();
            $(".windowstatus").hide();
            $(".infoconsulta").hide();
            $(".windowstatus").closest;

            //}

        }
        function exibirlogin() {
            var alturaTela = $(document).height();
            var larguraTela = $(window).width();

            // colocando o fundo preto
            $('#mascara').css({ 'width': larguraTela, 'height': alturaTela });
            $('#mascara').fadeIn(1000);
            $('#mascara').fadeTo("slow", 0.8);

            //      alert('Registro gravado com sucesso2!');

            var left = ($(window).width() / 2) - ($('#alterarststatus').width() / 4);
            var top = ($(window).height() / 2) - ($('#alterarststatus').height() / 4);

            $('#loginnovo').css({ 'left': left });
            //$('#embarquemaritimo').css({ 'margin-top': '430px' });

            $('#loginnovo').load('loginmodal.aspx#loginnovo');
            $('#loginnovo').show();
        }
        function exibiralerta1() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);


            $('#alerta1').show();
            $('#alerta1').load();
        }
        function escondeexibiralerta1() {




            $('#alerta1').hide();
            $('#alerta1').hide();
        }
    </script>


    <script runat="server">

        Sub exportarh(ByVal Source As Object, ByVal E As EventArgs)


            Dim tb1 As New Data.DataTable, tbdep As New Data.DataTable
            Dim tab1 As New clsBanco, tabdep As New clsBanco
            Dim tbx As New Data.DataTable
            tbx.Columns.Add("matricula")
            tbx.Columns.Add("nome")
            tbx.Columns.Add("tipo")
            tbx.Columns.Add("logradouro")
            tbx.Columns.Add("numero")
            tbx.Columns.Add("complemento")
            tbx.Columns.Add("bairro")
            tbx.Columns.Add("cidade")
            tbx.Columns.Add("cep")
            tbx.Columns.Add("ativo")
            tbx.Columns.Add("suspenso")
            tbx.Columns.Add("negativado")
            tbx.Columns.Add("plano")
            tbx.Columns.Add("cpf")
            tbx.Columns.Add("dtnasc")
            tbx.Columns.Add("dtcad")



            Dim xsql As String = "select * from tbclientes where (nome <> '' or not nome is null)"
            Dim wcdata As String = "dtcad"
            If optdata_negativacao.Checked Then
                wcdata = "dtnegativado"
            End If
            If optdata_suspensao.Checked Then
                wcdata = "dtsuspenso"
            End If
            If optdata_venda.Checked Then
                wcdata = "dtvenda"
            End If

            If IsDate(txtdtinicial.Text) Then
                xsql &= " and " & wcdata & " >= '" & clsSmart.formatadatamysql(txtdtinicial.Text) & "'"
            End If
            If IsDate(txtdtfinal.Text) Then
                xsql &= " and " & wcdata & " <= '" & clsSmart.formatadatamysql(txtdtfinal.Text) & "'"
            End If

            If optsituacao_ativos.Checked Then
                xsql &= " and liberado = true "
            End If
            If optsituacao_cancelados.Checked Then
                xsql &= " and liberado = false "
            End If

            If chknegativados.Checked Then
                xsql &= " and negativado = true"
            End If

            If chkSuspensos.Checked Then
                xsql &= " and suspenso = true"
            End If

            If optordem_matricula.Checked Then

                xsql &= " order by matricula"
            End If

            If optordem_nome.Checked Then

                xsql &= " order by nome"
            End If

            If optordem_data.Checked Then

                xsql &= " order by dtcad"
            End If

            tb1 = tab1.conectar(xsql)
            tbdep = tabdep.conectar("select * from tbdependentes where (nome <> '' or not nome is null)")

            For x As Integer = 0 To tb1.Rows.Count - 1
                tbx.Rows.Add()
                tbx.Rows(tbx.Rows.Count - 1)("matricula") = tb1.Rows(x)("matricula").ToString
                tbx.Rows(tbx.Rows.Count - 1)("nome") = tb1.Rows(x)("nome").ToString
                tbx.Rows(tbx.Rows.Count - 1)("logradouro") = tb1.Rows(x)("logradouro").ToString
                tbx.Rows(tbx.Rows.Count - 1)("numero") = tb1.Rows(x)("numero").ToString
                tbx.Rows(tbx.Rows.Count - 1)("complemento") = tb1.Rows(x)("complemento").ToString
                tbx.Rows(tbx.Rows.Count - 1)("bairro") = tb1.Rows(x)("bairro").ToString
                tbx.Rows(tbx.Rows.Count - 1)("cidade") = tb1.Rows(x)("cidade").ToString
                tbx.Rows(tbx.Rows.Count - 1)("dtcad") = tb1.Rows(x)("dtcad").ToString
                tbx.Rows(tbx.Rows.Count - 1)("dtnasc") = tb1.Rows(x)("dtnasc").ToString
                tbx.Rows(tbx.Rows.Count - 1)("plano") = tb1.Rows(x)("plano").ToString
                tbx.Rows(tbx.Rows.Count - 1)("cpf") = tb1.Rows(x)("cpf").ToString
                tbx.Rows(tbx.Rows.Count - 1)("cep") = tb1.Rows(x)("cep").ToString
                tbx.Rows(tbx.Rows.Count - 1)("Tipo") = "Titular"
                tbx.Rows(tbx.Rows.Count - 1)("ativo") = clsSmart.logico(tb1.Rows(x)("liberado").ToString, True)
                tbx.Rows(tbx.Rows.Count - 1)("suspenso") = clsSmart.logico(tb1.Rows(x)("suspenso").ToString, True)
                tbx.Rows(tbx.Rows.Count - 1)("negativado") = clsSmart.logico(tb1.Rows(x)("negativado").ToString, True)
                If chkexibirdependentes.Checked Then
                    Dim tbproc As Data.DataRow()
                    tbproc = tbdep.Select(" matricula = '" & tb1.Rows(x)("matricula").ToString & "'")
                    If tbproc.Count > 0 Then
                        For y As Integer = 0 To tbproc.Count - 1
                            tbx.Rows.Add()
                            tbx.Rows(tbx.Rows.Count - 1)("matricula") = tbproc(y)("matricula").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("nome") = tbproc(y)("nome").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("logradouro") = tbproc(y)("endereco").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("numero") = tbproc(y)("numero").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("complemento") = tbproc(y)("complemento").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("bairro") = tbproc(y)("bairro").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("cidade") = tbproc(y)("cidade").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("cep") = tbproc(y)("cep").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("dtcad") = tbproc(y)("dtcad").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("dtnasc") = tbproc(y)("dtnasc").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("plano") = tbproc(y)("plano").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("cpf") = tbproc(y)("cpf").ToString
                            tbx.Rows(tbx.Rows.Count - 1)("tipo") = "Dependente"
                            tbx.Rows(tbx.Rows.Count - 1)("ativo") = clsSmart.logico(tbproc(y)("ativo").ToString, True)
                            tbx.Rows(tbx.Rows.Count - 1)("suspenso") = clsSmart.logico(tb1.Rows(x)("suspenso").ToString, True)
                            tbx.Rows(tbx.Rows.Count - 1)("negativado") = clsSmart.logico(tb1.Rows(x)("negativado").ToString, True)
                        Next
                    End If
                End If
            Next




            gdvProcuraDeCarnes.DataSource = tbx
            gdvProcuraDeCarnes.DataBind()

            txtnomearq.Text = "mensalidades" & Date.Now.Date.ToString.Replace("/", "").Trim()
            txtnomearq.Text = Date.Now.Date.ToString.Replace(":", "").Trim()

            If txtnomearq.Text = "" Then
                txtnomearq.BackColor = Drawing.Color.Red
                Exit Sub
            End If
            'exportarExcel(dg, txtnomearq.Text)
            exportarExcel(gdvProcuraDeCarnes, txtnomearq.Text)
        End Sub

        Sub exportarExcel(grid As DataGrid, saveAsFile As String)




            ' O linite de linhas do Excel é  65536
            If grid.Items.Count + 1 < 65536 Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & saveAsFile & ".xls")
                ' Remover caracteres do header - Content-Type
                HttpContext.Current.Response.Charset = ""
                'HttpContext.Current.Response.WriteFile("style.txt")
                ' desabilita o  view state.
                grid.EnableViewState = False
                Dim tw As New System.IO.StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grid.RenderControl(hw)
                ' Escrever o html no navegador
                HttpContext.Current.Response.Write(tw.ToString())
                ' termina o response
                HttpContext.Current.Response.End()

            Else
                HttpContext.Current.Response.Write("Muitas linhas para exportar para o Excel !!!")
            End If

        End Sub
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container-fluid">
        <div class="box box-primary" style="margin-left: 0.5rem; margin-top: 0.5rem; margin-right: 0.5rem; -webkit-box-shadow: 0px 0px 18px 0px rgba(50, 50, 50, 0.75); -moz-box-shadow: 0px 0px 18px 0px rgba(50, 50, 50, 0.75); box-shadow: 0px 0px 18px 0px rgba(50, 50, 50, 0.75);">
            <div class="box-header ">
                <b>Relatório de Clientes</b>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-4 ">
                        <div class="box box-success">
                            <div class="box-header">
                                Filtrar Por data (Opcional)
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    Data Inicial
                                                <br />
                                    <asp:TextBox ID="txtdtinicial" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-6">
                                    Data Final<br />
                                    <asp:TextBox ID="txtdtfinal" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-4 ">
                                    <asp:RadioButton CssClass="radio-inline" ID="optdata_cadastro" runat="server" Text="Cadastro" GroupName="datafiltro" Checked="true" />
                                </div>
                                <div class="col-lg-4 ">
                                    <asp:RadioButton CssClass="radio-inline" ID="optdata_venda" runat="server" Text="Venda" GroupName="datafiltro" />
                                </div>
                                <%--  <div class="col-lg-4 ">
                                        <asp:radiobutton CssClass="radio-inline" ID="optdata_pparcela" runat="server" Text="Primeira Pagto" GroupName="datafiltro" />
                                    </div>--%>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 ">
                                    <asp:RadioButton CssClass="radio-inline" ID="optdata_negativacao" runat="server" Text="Negativação" GroupName="datafiltro" />
                                </div>
                                <div class="col-lg-4 ">
                                    <asp:RadioButton CssClass="radio-inline" ID="optdata_suspensao" runat="server" Text="Suspensão" GroupName="datafiltro" />
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 ">
                        <div class="box box-warning">
                            <div class="box-header ">
                                Situação
                            </div>
                            <div class="box-body ">
                                <div class="row">
                                    <div class="col-lg-4 ">
                                        <asp:RadioButton CssClass="radio-inline" ID="optsituacao_todos" runat="server" Checked="true" GroupName="situacao" Text="Todos" />
                                    </div>
                                    <div class="col-lg-4 ">
                                        <asp:RadioButton CssClass="radio-inline" ID="optsituacao_ativos" runat="server" GroupName="situacao" Text="Ativos" />
                                    </div>
                                    <div class="col-lg-4 ">
                                        <asp:RadioButton CssClass="radio-inline" ID="optsituacao_cancelados" runat="server" GroupName="situacao" Text="Cancelados" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 ">
                                        <asp:CheckBox CssClass="checkbox-inline" ID="chkSuspensos" runat="server" Text="Suspensos" />
                                    </div>
                                    <div class="col-lg-6 ">
                                        <asp:CheckBox CssClass="checkbox-inline" ID="chknegativados" runat="server" Text="Negativados" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 text-left">
                                        <asp:CheckBox CssClass="checkbox-inline" ID="chkexibirdependentes" runat="server" Text="Exibir Dependentes" />
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="box box-primary">
                            <div class="box-header ">
                                Ordenar por
                            </div>
                            <div class="box-body ">
                                <div class="row">
                                    <div class="col-lg-4 ">
                                        <asp:RadioButton CssClass="radio-inline" ID="optordem_matricula" runat="server" Text="Matricula" GroupName="ordem" Checked="true" />
                                    </div>
                                    <div class="col-lg-4 ">
                                        <asp:RadioButton CssClass="radio-inline" ID="optordem_nome" runat="server" Text="Nome" GroupName="ordem" />
                                    </div>
                                    <div class="col-lg-4 ">
                                        <asp:RadioButton CssClass="radio-inline" ID="optordem_data" runat="server" Text="Data" GroupName="ordem" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



                <div class="row">
                    <div class="col-lg-6 text-center" style="display: none">
                        <br />
                        <asp:LinkButton runat="server" CssClass="btn btn-primary  " ID="btnImprimir" AutoPostBack="true"> <asp:Image runat="server" style="width:2rem ;height:2rem;" ImageUrl="~/img/print01.png" />Imprimir</asp:LinkButton>
                    </div>

                    <div class="col-lg-6 text-center ">
                        <br />
                        <asp:Label ID="txtnomearq" runat="server"></asp:Label>
                        <asp:LinkButton runat="server" CssClass="btn btn-success  " ID="btnexport" AutoPostBack="true" OnClick="exportarh"> <asp:Image runat="server" style="width:2rem ;height:2rem;" ImageUrl="~/img/excel.png" />Exportar Excel</asp:LinkButton>


                    </div>
                </div>
            </div>
            <br />
        </div>
        <div style="display: none">

            <asp:DataGrid ID="gdvProcuraDeCarnes" runat="server" BackColor="WhiteSmoke" BorderColor="Black"
                CellPadding="0" CellSpacing="1" Font-Name="Arial" Font-Names="Arial"
                Font-Size="8pt" HeaderStyle-BackColor="#0000FF"
                Headerstyle-Font-Name="Arial" HeaderStyle-Font-Size="10"
                HeaderStyle-ForeColor="#FFFFFF" Height="148px" Style="margin-right: 0px"
                AutoGenerateColumns="False" ShowFooter="True">
                <AlternatingItemStyle BackColor="White" />
                <Columns>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:Label ID="lblitemgrade" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="matricula" HeaderText="Matricula" />
                    <asp:BoundColumn DataField="nome" HeaderText="Nome" />
                    <asp:BoundColumn DataField="tipo" HeaderText="Tipo" />
                    <asp:BoundColumn DataField="plano" HeaderText="Plano" />
                    <asp:BoundColumn DataField="cpf" HeaderText="CPF" />
                    <asp:BoundColumn DataField="dtnasc" HeaderText="Data nascimento" />
                    <asp:BoundColumn DataField="dtcad" HeaderText="Data Cadastro" />
                    <asp:BoundColumn DataField="ativo" HeaderText="Ativo" />
                    <asp:BoundColumn DataField="suspenso" HeaderText="Suspenso" />
                    <asp:BoundColumn DataField="logradouro" HeaderText="Endereço" />
                    <asp:BoundColumn DataField="numero" HeaderText="Numero" />
                    <asp:BoundColumn DataField="complemento" HeaderText="Complemento" />
                    <asp:BoundColumn DataField="bairro" HeaderText="bairro" />
                    <asp:BoundColumn DataField="cidade" HeaderText="Cidade" />
                    <asp:BoundColumn DataField="cep" HeaderText="cep" />
                </Columns>
            </asp:DataGrid>
        </div>
    </div>












</asp:Content>

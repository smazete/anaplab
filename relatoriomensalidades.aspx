﻿<%@ Page Language="VB" AutoEventWireup="false"  MasterPageFile="~/masterpage.master"  CodeFile="relatoriomensalidades.aspx.vb" Inherits="relatoriomensalidades" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

      <script runat="server">

        Sub exportarh(ByVal Source As Object, ByVal E As EventArgs)

            txtnomearq.Text = "mensalidade" & Date.Now.Date.ToString.Replace("/", "").Trim()
            txtnomearq.Text = Date.Now.Date.ToString.Replace(":", "").Trim()

            If txtnomearq.Text = "" Then
                txtnomearq.BackColor = Drawing.Color.Red
                Exit Sub
            End If

            'exportarExcel(dg, txtnomearq.Text)

            exportarExcel(gradeex, txtnomearq.Text)



        End Sub

        Sub exportarExcel(grid As DataGrid, saveAsFile As String)

            ' O linite de linhas do Excel é  65536
            If grid.Items.Count + 1 < 65536 Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & saveAsFile & ".xls")
                ' Remover caracteres do header - Content-Type
                HttpContext.Current.Response.Charset = ""
                'HttpContext.Current.Response.WriteFile("style.txt")
                ' desabilita o  view state.
                grid.EnableViewState = False
                Dim tw As New System.IO.StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grid.RenderControl(hw)
                ' Escrever o html no navegador
                HttpContext.Current.Response.Write(tw.ToString())
                ' termina o response
                HttpContext.Current.Response.End()

            Else
                HttpContext.Current.Response.Write("Muitas linhas para exportar para o Excel !!!")
            End If

        End Sub
    </script>

    <script type="text/javascript">








        function irlink(linkativo) {



            window.open(linkativo, '_blank');

        }

        function irlinkmesmpagina(linkativo) {

            window.location.href = linkativo;

            //window.open(linkativo);

        }

    </script>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Relatorio de Mensalidades </b>
        </div>


            <div class="box box-header">
                
        
            </div>
            <div class="row">
                <div class="col-lg-8 table-bordered">
                    <div class=" col-lg-12" runat="server"  >
                        <div class="col-lg-3" >
                            <b>
                                <asp:Label Text="Mês" runat="server" /></b>
                            <asp:DropDownList  ID="cbomes" CssClass="form-control"  runat="server">
                                <asp:ListItem Text="Janeiro" Value="1" />
                                <asp:ListItem Text="Fevereiro"  Value="2"/>
                                <asp:ListItem Text="Março"  Value="3"/>
                                <asp:ListItem Text="Abril"  Value="4"/>
                                <asp:ListItem Text="Maio"  Value="5"/>
                                <asp:ListItem Text="Junho"  Value="6"/>
                                <asp:ListItem Text="Julho"  Value="7"/>
                                <asp:ListItem Text="Agosto"  Value="8"/>
                                <asp:ListItem Text="Setembro"  Value="9"/>
                                <asp:ListItem Text="Outubro"  Value="10"/>
                                <asp:ListItem Text="Novembro"  Value="11"/>
                                <asp:ListItem Text="Dezembro"  Value="12"/>
                            </asp:DropDownList>

                        </div>
                        <div class="col-lg-3" >
                            <b>Ano</b>
                            <asp:TextBox runat="server" ID="txtano" CssClass="form-control" MaxLength="4" />
                        </div>
                        <div class="col-lg-4" >
                            <b>
                                <asp:Label Text="Status " runat="server" /></b>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="cbostatus">
                                <asp:ListItem Text="Todas" Value="4" />
                                <asp:ListItem Text="Aberta" Value="0" />
                                <asp:ListItem Text="Pago" Value="1" />
                                <asp:ListItem Text="Isento" Value="3" />
                                <asp:ListItem Text="Estornada" Value="2" />
                            </asp:DropDownList>
                        </div>
                            <div class="row">

             <%--   <div class="col-lg-2">
                    <asp:Button runat="server" CssClass="btn btn-primary btn-lg" ID="btnImprimir" Text="Imprimir" AutoPostBack="true" />
                </div>--%>

                <div class="col-lg-2">
                    <asp:Label ID="txtnomearq" runat="server"></asp:Label>
                    <asp:Button runat="server" CssClass="btn btn-success btn-lg" ID="btnexport" Text="Exportar Excel" Autopostback="true" OnClick="exportarh" />
                </div>

            </div>

                     
                    </div>

                </div>
                
            </div>
  
    <div class="row">
        <br>
        <%-- Grade de mensalidades --%>
           <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <!-- Buttons, labels, and many other things can be placed here! -->

                            <!-- Here is a label for example -->
                            
                        </div>
                        <!-- /.box-tools -->
                    </div>
        <asp:GridView ID="grademensalidade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma mensalidade encontrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                        <asp:HiddenField ID="hdvalor" runat="server" Value='<%#Bind("valor")%>'></asp:HiddenField>
                        <asp:LinkButton ID="visualizar" runat="server" CssClass="btn btn-info " CommandName="visualizar" CommandArgument='<%#Container.DataItemIndex%>' Visible="false">
                            <i class="fa fa-eye"></i></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                <asp:BoundField DataField="nome" HeaderText="Cliente" />
                <asp:BoundField DataField="Ativo" HeaderText="Ativo" />
                <asp:BoundField DataField="statuspg" HeaderText="Status" />
                <asp:BoundField DataField="mes" HeaderText="Mes" />
                <asp:BoundField DataField="ano" HeaderText="Ano" />
                <asp:BoundField DataField="Valor" HeaderText="Valor" />
                <asp:BoundField DataField="dtpg" HeaderText="Data" />
            </Columns>
        </asp:GridView>


        <%-- 
            
            Grade EXCEL

            --%>
       
        <asp:DataGrid ID="gradeex" runat="server" BackColor="WhiteSmoke" BorderColor="Black" CellPadding="0" CellSpacing="1" Font-Name="Arial" Font-Names="Arial" Font-Size="8pt" HeaderStyle-BackColor="#0000FF" Headerstyle-Font-Name="Arial" HeaderStyle-Font-Size="10" HeaderStyle-ForeColor="#FFFFFF" Height="148px" Style="margin-right: 0px" AutoGenerateColumns="False" ShowFooter="True">
            <Columns>                             
                <asp:BoundColumn DataField="matricula" HeaderText="Matricula" />
                <asp:BoundColumn DataField="nome" HeaderText="Cliente" />
                <asp:BoundColumn DataField="Ativo" HeaderText="Ativo" />
                <asp:BoundColumn DataField="statuspg" HeaderText="Status" />
                <asp:BoundColumn DataField="mes" HeaderText="Mes" />
                <asp:BoundColumn DataField="ano" HeaderText="Ano" />
                <asp:BoundColumn DataField="Valor" HeaderText="Valor" />
                <asp:BoundColumn DataField="dtpg" HeaderText="Data" />
            </Columns>
        </asp:DataGrid>
     
    </div>
          </div>
</asp:Content>
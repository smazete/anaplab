﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeFile="relatorioassociados.aspx.vb" Inherits="relatorioassociados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <script runat="server">

        Sub exportarh(ByVal Source As Object, ByVal E As EventArgs)

            If dgexcel.Items.Count = 0 Then
                Exit Sub
            End If

            Dim wcarquivo As String = "relatorios" & Date.Now.ToString.Replace("/", "").Replace(":", "").Replace(" ", "").Trim()

            exportarExcel(dgexcel, wcarquivo)
        End Sub

        Sub exportarExcel(grid As DataGrid, saveAsFile As String)

            ' O linite de linhas do Excel é  65536
            If grid.Items.Count + 1 < 65536 Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & saveAsFile & ".xls")
                ' Remover caracteres do header - Content-Type
                HttpContext.Current.Response.Charset = ""
                'HttpContext.Current.Response.WriteFile("style.txt")
                ' desabilita o  view state.
                grid.EnableViewState = False
                Dim tw As New System.IO.StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grid.RenderControl(hw)
                ' Escrever o html no navegador
                HttpContext.Current.Response.Write(tw.ToString())
                ' termina o response
                HttpContext.Current.Response.End()

            Else
                HttpContext.Current.Response.Write("Muitas linhas para exportar para o Excel !!!")
            End If

        End Sub
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Panel runat="server">
        <div class="panel panel-primary">
            <div class="panel header-content">
                <asp:Label Text="Relatorio Associados" runat="server" />
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2">
                        <asp:LinkButton ID="btnadinplentes" runat="server" CssClass="btn btn-primary btn-lg">
                            <i class="fa fa-search"></i> Associados</asp:LinkButton>
                    </div>
                 <%--   <div class="col-lg-2">
                        <asp:LinkButton ID="btnbinadimplentes" runat="server" CssClass="btn btn-primary btn-lg">
                            <i class="fa fa-search"></i> Inadimplentes</asp:LinkButton>
                    </div>--%>
                    <div class="col-lg-2">
                        <asp:LinkButton ID="btndesfiliados" runat="server" CssClass="btn btn-primary btn-lg">
                            <i class="fa fa-search"></i> Desfiliados </asp:LinkButton>
                    </div>
                    <div class="col-lg-2"></div>   <div class="col-lg-4  text-center">
                    <br />
                    <asp:LinkButton ID="btnexcel" runat="server" OnClick="exportarh" CssClass="btn btn-success lg"><i class="fa fa-file-excel-o"></i>Excel</asp:LinkButton>
                </div>

                </div>
                <br />
                <div class="row" runat="server" id="divgrade">
            
                <br />
                <asp:GridView ID="gradeassociados" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="nrseq" HeaderText="Código" />
                        <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                        <asp:BoundField DataField="nome" HeaderText="Nome" />
                        <asp:BoundField DataField="cpf" HeaderText="CPF" />
                        <asp:BoundField DataField="telefone" HeaderText="Telefone" />
                    </Columns>
                </asp:GridView>

                <div style="display: none">
                        <asp:DataGrid ID="dgexcel" runat="server" BackColor="WhiteSmoke" BorderColor="Black"
                            CellPadding="0" CellSpacing="1" Font-Name="Arial" Font-Names="Arial"
                            Font-Size="8pt" HeaderStyle-BackColor="#0000FF"
                            Headerstyle-Font-Name="Arial" HeaderStyle-Font-Size="10"
                            HeaderStyle-ForeColor="#FFFFFF" Height="148px" Style="margin-right: 0px"
                            AutoGenerateColumns="False" ShowFooter="True">
                            <AlternatingItemStyle BackColor="White" />
                            <Columns>

                                <asp:BoundColumn DataField="nrseq" HeaderText="Código" />
                                <asp:BoundColumn DataField="matricula" HeaderText="Matricula" />
                                <asp:BoundColumn DataField="nome" HeaderText="Nome" />
                                <asp:BoundColumn DataField="cpf" HeaderText="CPF" />
                                <asp:BoundColumn DataField="telefone" HeaderText="Telefone" />

                            </Columns>
                        </asp:DataGrid>
                    </div>

                    
          

                </div>
        </div>
        </div>
    </asp:Panel>



</asp:Content>

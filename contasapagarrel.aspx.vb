﻿Imports clsSmart
Imports clsContasPagar

Partial Class restrito_contasapagar
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    Private Sub restrito_contasapagar_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim xcontas As New clsContasPagar
        If IsPostBack Then Exit Sub

        habilitar(False)

        xcontas.carregardocumentos(cbooperacao)
        xcontas.carregarFormasdePagamento(cboformapagto)
        xcontas.carregarPlanosdeContas(cboplano)
        xcontas.carregarCentrodeCusto(cboCentroCusto)
        If Not xcontas.carregartodos() Then
            sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        Gradecontas.DataSource = xcontas.Table
        Gradecontas.DataBind()
    End Sub


    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click
        Dim xcontas As New clsContasPagar

        If Not xcontas.Novo() Then
            sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

            Exit Sub
        End If
        habilitar(True)
        txtnrseq.Text = xcontas.Nrseq
        btnSalvar.Enabled = True
        btnnovo.Enabled = False
        hdnnrseq.Value = xcontas.Nrseq
        txtdtvencimento.Focus()
    End Sub

    Private Function carregardocumentos() As Boolean
        Try
            Dim xdocumentos As New clsDocumentos

            If Not xdocumentos.consultarativo() Then
                Exit Function
            End If

            cbooperacao.Items.Clear()
            cbooperacao.Items.Add(New ListItem("Selecione o Funcionário", ""))
            For x As Integer = 0 To xdocumentos.Contador - 1
                cbooperacao.Items.Add(New ListItem(xdocumentos.Table.Rows(x)("descricao").ToString, xdocumentos.Table.Rows(x)("nrseq").ToString))
            Next

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function carregarcontasapagar() As Boolean

        Try
            Dim xcontas As New clsContasPagar

            If Not xcontas.carregartodos(Not chkexibirinativos.Checked) Then
                sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click
        Dim xcontas As New clsContasPagar



        xcontas.Nrseq = hdnnrseq.Value

        If Not xcontas.consultar() Then
            sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        xcontas.Codigo = txtcodigo.Text
        xcontas.Descricaocontas = tratatexto(cboplano.SelectedValue)
        xcontas.Descricaocentrodecusto = tratatexto(cboCentroCusto.SelectedValue)
        xcontas.Valor = txtvalor.Text
        xcontas.Parcela = sonumeros(txtparcela.Text)
        xcontas.Qtdparcelas = sonumeros(txtparcelas.Text)
        xcontas.Dtexecucao = txtdtexecucao.Text
        xcontas.Dtvencimento = txtdtvencimento.Text
        xcontas.Descricaodocumento = tratatexto(cboplano.SelectedValue)
        xcontas.Operacao = tratatexto(cboformapagto.SelectedValue)
        xcontas.Solaprovacao = logico(chksolaprovacao.Checked)
        xcontas.Descricao = tratatexto(txtdescricao.Text)
        xcontas.Nrseqempresa = Session("idempresaemuso")
        xcontas.Nrseqcentrocusto = cboCentroCusto.SelectedValue
        xcontas.Nrseqdocumento = cbooperacao.SelectedValue
        xcontas.Nrseqplano = cboplano.SelectedValue
        xcontas.Nrseqformapagto = cboformapagto.SelectedValue

        If Not xcontas.salvar Then
            sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        habilitar(False)
        limpar()
        btnSalvar.Enabled = False
        btnnovo.Enabled = True

        xcontas.carregarFormasdePagamento(cboformapagto)
        xcontas.carregarFormasdePagamento(cbooperacao)
        xcontas.carregarPlanosdeContas(cboplano)
        xcontas.carregarCentrodeCusto(cboCentroCusto)
        If Not xcontas.carregartodascontas() Then
            sm("swal({title: 'Atenção!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        Gradecontas.DataSource = xcontas.Table
        Gradecontas.DataBind()
    End Sub



    Private Sub txtdtvencimento_TextChanged(sender As Object, e As EventArgs) Handles txtdtvencimento.TextChanged
        Dim xcontas As New clsContasPagar

        If IsDate(txtdtvencimento.Text) Then
            xcontas.carregar(txtdtvencimento.Text, Gradecontas)
        End If
    End Sub
    Private Sub cboformapagto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboformapagto.SelectedIndexChanged


        selectedIndexPagamentos(Page, cboformapagto)

    End Sub

    Public Sub selectedIndexPagamentos(page As Page, cboformapagto As DropDownList)

        If cboformapagto.Text = "" Then
            Session("ehcarteira") = "nao"
            Exit Sub
        End If

        tb1 = tab1.conectar("select * from tbdocumentos where descricao = '" & cboformapagto.Text & "' and ativo = true")
        If tb1.Rows.Count = 0 Then
            sm("swal({title: 'Error!',text: 'A forma de pagamento não foi localizada !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If


    End Sub

    Private Sub cboplano_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboplano.SelectedIndexChanged
        Dim cls As New clsContasPagar

        cls.selectedIndexPlano(Page, cboplano, txtcodigo)

    End Sub


    Private Sub codigoTextChanged(page As Page, cboplano As DropDownList, txtcodigo As TextBox)

        cboplano.Items.Clear()
        If zeros(mLeft(txtcodigo.Text, 2), 2) <> zeros(Session("idempresaemuso"), 2) Then
            sm("swal({title: 'Error!',text: 'O código digitado é inválido ! O código inicial para sua empresa deverá ser " & zeros(Session("idempresaemuso"), 2) & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        tb1 = tab1.conectar("select * from tbplanocontas where ativo = true And codigo like '" & txtcodigo.Text & "%'")
        If tb1.Rows.Count <> 0 Then


            cboplano.Items.Add("")
            For x As Integer = 0 To tb1.Rows.Count - 1
                cboplano.Items.Add(tb1.Rows(x)("descricao").ToString)
            Next
            If tb1.Rows.Count = 1 Then
                cboplano.Text = tb1.Rows(0)("descricao").ToString

            End If

        End If

    End Sub

    Private Sub txtcodigo_TextChanged(sender As Object, e As EventArgs) Handles txtcodigo.TextChanged

        codigoTextChanged(Page, cboplano, txtcodigo)

    End Sub

    Private Sub Gradecontas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles Gradecontas.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub

        Dim icoexcluir As HtmlGenericControl = e.Row.FindControl("icogradeexcluir")
        Dim btnexcluir As LinkButton = e.Row.FindControl("btnexcluir")


        e.Row.Cells(1).Text = formatadatamysql2(e.Row.Cells(1).Text, True)
        e.Row.Cells(2).Text = formatadatamysql2(e.Row.Cells(2).Text, True)
        If e.Row.Cells(5).Text = 1 Then
            e.Row.Cells(5).Text = "Sim"
            icoexcluir.Attributes.Remove("class")
            icoexcluir.Attributes.Add("class", "fa fa-trash-o")
        Else
            e.Row.Cells(5).Text = "Não"
            icoexcluir.Attributes.Remove("class")
            icoexcluir.Attributes.Add("class", "fa fa-undo")
        End If

        Dim xcontas As New clsContasPagar
        If chkexibirinativos.Checked = "true" Then
            If Not xcontas.consultardescricao(False) Then
                sm("swal({title: 'Error!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            Dim faicon As HtmlGenericControl = e.Row.FindControl("faicon")
            If Not faicon Is Nothing Then
                faicon.Attributes.Remove("class")
                faicon.Attributes.Add("class", "fa fa-history")

            End If

        Else
            If chkexibirinativos.Checked = "false" Then
                If Not xcontas.consultardescricao() Then
                    sm("swal({title: 'Error!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                    Exit Sub
                End If

                Dim faicon As HtmlGenericControl = e.Row.FindControl("faicon")

                If Not faicon Is Nothing Then
                    faicon.Attributes.Remove("class")
                    faicon.Attributes.Add("class", "fa fa-history")

                End If

            End If

        End If


    End Sub

    Private Sub habilitar(Optional habilita As Boolean = True)
        btnnovo.Enabled = Not habilita
        cboCentroCusto.Enabled = habilita
        cboformapagto.Enabled = habilita
        cboplano.Enabled = habilita
        txtdescricao.Enabled = habilita
        txtdtexecucao.Enabled = habilita
        txtdtvencimento.Enabled = habilita
        txtparcelas.Enabled = habilita
        txtvalor.Enabled = habilita
        txtparcela.Enabled = habilita
        cbooperacao.Enabled = habilita
        btnSalvar.Enabled = habilita
        cboformapagto.Style.Add("cssclass", "form-control")

    End Sub

    Private Sub limpar()
        txtcodigo.Text = ""
        txtdescricao.Text = ""
        txtdtexecucao.Text = ""
        txtdtvencimento.Text = ""
        txtparcelas.Text = ""
        txtvalor.Text = ""
        txtparcela.Text = ""
        If Not cbooperacao.SelectedIndex < 0 Then
            cbooperacao.SelectedIndex = 0
        End If
        If Not cboCentroCusto.SelectedIndex < 0 Then
            cboCentroCusto.SelectedIndex = 0
        End If
        If Not cboplano.SelectedIndex < 0 Then
            cboplano.SelectedIndex = 0
        End If
        If Not cboformapagto.SelectedIndex < 0 Then
            cboformapagto.SelectedIndex = 0
        End If

        txtnrseq.Text = ""
    End Sub

    Private Sub Gradecontas_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Gradecontas.RowCommand
        Dim xcontas As New clsContasPagar
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = Gradecontas.Rows(index)

        Dim xcaixa As New clsCaixas
        Dim nrseq As HiddenField = row.FindControl("hdnnrseqgrade")



        If e.CommandName.ToLower = "editar" Then

            xcontas.carregardocumentos(cbooperacao)
            xcontas.carregarFormasdePagamento(cboformapagto)
            xcontas.carregarPlanosdeContas(cboplano)
            xcontas.carregarCentrodeCusto(cboCentroCusto)

            xcontas.Nrseq = nrseq.Value

            If Not xcontas.consultar() Then
                sm("swal({title 'Error!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            'Desativar a alteração se o usuario estiver excluido
            If xcontas.Ativo = False Then
                habilitar(False)
                limpar()
                sm("Swal.fire({'Atenção!','Habilite o usuario para alterar ele.','error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            ElseIf xcontas.Ativo = True Then
                limpar()
                txtnrseq.Text = xcontas.Nrseq
                cboformapagto.SelectedValue = xcontas.Nrseqformapagto
                cbooperacao.SelectedValue = xcontas.Nrseqdocumento
                cboplano.SelectedValue = xcontas.Nrseqplano
                cboCentroCusto.SelectedValue = xcontas.Nrseqcentrocusto
                txtdescricao.Text = xcontas.Descricao
                txtvalor.Text = moeda(xcontas.Valor, True)
                txtparcela.Text = xcontas.Parcela
                txtparcelas.Text = xcontas.Qtdparcelas
                txtdtexecucao.Text = xcontas.Dtexecucao
                txtdtvencimento.Text = xcontas.Dtvencimento
                cbooperacao.SelectedValue = xcontas.Nrseqdocumento
                chksolaprovacao.Checked = logico(xcontas.Solaprovacao)
                hdnnrseq.Value = xcontas.Nrseq

                habilitar(True)
            End If

        End If
        If e.CommandName = "deletar" Then
            xcontas.Nrseq = row.Cells(0).Text
            If Not xcontas.excluir Then
                Exit Sub
            End If
            If Not xcontas.consultartodos(Not chkexibirinativos.Checked, txtprocurar.Text) Then
                Exit Sub
            End If

            Gradecontas.DataSource = xcontas.Table
            Gradecontas.DataBind()

        End If



    End Sub

    Private Sub btnprocurar_Click(sender As Object, e As EventArgs) Handles btnprocurar.Click
        Dim xcontas As New clsContasPagar
        xcontas.Descricao = txtprocurar.Text

        If chkexibirinativos.Checked Then
            If Not xcontas.consultardescricao(False) Then
                sm("swal({title: 'Error!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

        Else
            If Not xcontas.consultardescricao() Then
                sm("swal({title: 'Error!',text: '" & xcontas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If
        End If

        Gradecontas.DataSource = xcontas.Table
        Gradecontas.DataBind()
    End Sub
End Class

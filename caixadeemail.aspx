﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="caixadeemail.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="caixadeemail" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="box box-primary" style="margin-left: 1rem; margin-top: 1rem; margin-right: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box-header">
           <h2> CAIXA POSTAL</h2>
        </div>
        <div class="box-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <div class="box-body">
                        <asp:UpdatePanel ID="updateemails" runat="server">
                            <ContentTemplate>
                                <%-- envio privatvo --%>
                                <div class="row" runat="server" id="divprivado" visible="true">
                                    <div class="row">
                                        <div class="col-lg-4">
              <asp:Label Text="Buscar Por" runat="server" /> 
              <asp:DropDownList runat="server"  CssClass="form-control" ID="cbobusca" >                  
                  <asp:ListItem Text="Codigo" Value="nrseq" />
                  <asp:ListItem Text="Nome" Value="nome" /> 
                  <asp:ListItem Text="Matricula"  Value="matricula"/>
                  <asp:ListItem Text="CPF" Value="cpf" />
              </asp:DropDownList>
                                </div>              
                            <div class="col-lg-6">
                                <br />
              <asp:TextBox runat="server" id="txtbusca" CssClass="form-control" />  
                                </div>
               
                <div class="col-lg-2 text-center">
                     <br>
                    <asp:LinkButton ID="btnbuscando" runat="server" CssClass="btn btn-primary "> <i class="fa fa-searc"></i> Buscar</asp:LinkButton>                
                </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <asp:GridView ID="gradeassociados" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                                        <asp:LinkButton ID="usar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="usar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                                <asp:BoundField DataField="nome" HeaderText="Nome" />
                                                <asp:BoundField DataField="cpf" HeaderText="CPF" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                </div><br /><br /><br /><br />
                                <div class="box box-primary" runat="server" id="divpublica" visible="true">   <div class="box-header">
                                    <asp:Label Text="CONFIG ENVIO" runat="server" />
        </div> <div class="col-lg-3">
                                        <b>
                                            <asp:Label Text="Privado" runat="server" /><br />
                                        </b>
                                        <asp:CheckBox ID="chkenvmod" runat="server"   Enabled="false" />
                                     
                                    </div>
                                    <div class="col-lg-3">
                                        <b>
                                            <asp:Label Text="Somente Ativos" runat="server" /><br />
                                        </b>
                                        <asp:CheckBox runat="server" Checked="true" Enabled="false" />
                                        Associados Ativos
                                    </div>
                                    <div class="col-lg-3">
                                        <b>
                                            <asp:Label Text="Somente Uma " runat="server" /><br />
                                        </b>
                                        <asp:CheckBox runat="server"  Enabled="false" />
                                        Visualização
                                    </div>
                                    <div class="col-lg-3">
                                        <b>
                                            <asp:Label Text="Disponivel até" runat="server" /></b>
                                        <asp:TextBox runat="server" CssClass="form-control" type="date" Enabled="false"></asp:TextBox>
                                    </div>
                                    
                                    <div class="row">
                                        <asp:HiddenField runat="server" ID="hddadosuser" />
                                        <div class="col-md-2">
                                            Código
                        <asp:Label ID="lblcodigo" Text="0" runat="server" CssClass="form-control" />
                                        </div>
                                        <div class="col-md-2">
                                            Matricula
                        <asp:Label ID="lblmatricula" Text="0" runat="server" CssClass="form-control" />
                                        </div>
                                        <div class="col-md-4">
                                            Nome
                        <asp:Label ID="lblnome" Text="nome sobrenome" runat="server" CssClass="form-control" />
                                        </div>
                                        <div class="col-md-3">
                                            CPF
                        <asp:Label ID="lblcpf" Text="000.000.000-00" runat="server" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <asp:UpdateProgress ID="UpdateProgress4" runat="server" AssociatedUpdatePanelID="updateemails">
                                        <ProgressTemplate>

                                            <div style="text-align: center">
                                                <asp:Image ImageUrl="~/img/loading.gif" runat="server" Height="41px" Width="47px" />

                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                                <div id="divtexto" runat="server">
                                    <br />
                                    <div class="row">
                                        <div class="col-lg-12">
                                 <b>   <asp:Label Text="ASSUNTO:" runat="server" /></b>
                                    <asp:TextBox runat="server" id="txtassunto" CssClass="form-control"/>
                               <b>
                                    <asp:Label Text="MENSAGEM:" runat="server" /></b>
                                            <asp:TextBox ID="txtcorpo" TextMode="MultiLine" Style="margin-right: 10px; margin-left: 10px;" Height="420px" runat="server" Width="100%" Rows="100">
                                            </asp:TextBox>
                                            <ajaxToolkit:HtmlEditorExtender ID="TextBox1_HtmlEditorExtender" runat="server" TargetControlID="txtcorpo" EnableSanitization="false">
                                            </ajaxToolkit:HtmlEditorExtender>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 text-center "></div>
                                    <div class="col-lg-4 text-center ">
                                        <br />
                                        <asp:Button ID="btnenviar" runat="server" Text="Salvar" class="btn btn-warning " />
                                    </div>



                                </div>
                                <div id="myTesteEmail" class="modal fade" role="dialog">
                                    <div class="modal-dialog" style="width: 80vw; height: 80vh;">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center">
                                                    <asp:Label ID="Label1" runat="server" CssClass="label-success"></asp:Label></h4>
                                            </div>
                                            <div class="modal-body">


                                                <asp:UpdatePanel ID="updatedocas" runat="server">
                                                    <ContentTemplate>


                                                        <div class="row">
                                                            <div class="col-lg-12 text-center">
                                                                <b>Assunto do E-mail</b>
                                                                <br />
                                                                <asp:Label ID="lbltesteemail" runat="server">
                                                                </asp:Label>
                                                                <br />
                                                                <b>Corpo do e-mail</b>
                                                                <br />
                                                                <center>
                                                  
                                              <iframe id="frameteste"  runat="server" style="width:70vw;height:30vw">

                                              </iframe>
                                                  </center>

                                                                <%--  <ajaxToolkit:HtmlEditorExtender ID="HtmlEditorExtender1" runat="server" TargetControlID="txttesteemail" EnableSanitization="false">
                                                </ajaxToolkit:HtmlEditorExtender>--%>
                                                            </div>
</div>


                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>





                    </div>

                </ContentTemplate>

            </asp:UpdatePanel>
        </div>
    </div>



    <script>

        $('.my-colorpicker2').colorpicker();

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
</asp:Content>

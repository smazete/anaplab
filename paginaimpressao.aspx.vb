﻿Imports System.IO
Imports clsSmart

Partial Class paginaimpressao
    Inherits System.Web.UI.Page
    Dim wcvalortotal As Decimal = 0
    Private Sub paginaimpressao_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        lbldtinicial.Text = Session("datainicial")
        lbldatafinal.Text = Session("datafinal")
        lblData.Text = "Emissão:" & FormatDateTime(data, DateFormat.GeneralDate)
        Dim tbrel As New Data.DataTable
        Dim tabrel As New clsBanco
        wcvalortotal = 0
        tbrel = tabrel.conectar(Session("sql"))

        GradeLotes.DataSource = tbrel
        GradeLotes.DataBind()
        lblqtd.Text = GradeLotes.Rows.Count
        lblvltotal.Text = FormatCurrency(wcvalortotal)

    End Sub
    Protected Sub GradeLotes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GradeLotes.SelectedIndexChanged

    End Sub

    Private Sub GradeLotes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GradeLotes.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        If IsDate(e.Row.Cells(11).Text) Then
            e.Row.Cells(11).Text = FormatDateTime(e.Row.Cells(11).Text, DateFormat.ShortDate)
        End If
        If Server.HtmlDecode(e.Row.Cells(10).Text).Trim = "" Then e.Row.Cells(10).Text = 0
        wcvalortotal += CType(e.Row.Cells(10).Text, Decimal)
    End Sub
End Class

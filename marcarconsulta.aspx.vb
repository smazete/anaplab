﻿Imports clssessoes
Imports clsSmart
Imports System.IO

Partial Class marcarconsulta
    Inherits System.Web.UI.Page

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbmed As String = "tbmedicos"

    Private Sub marcarconsulta_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If
        lblmedicos.Text = "Médico"
        lblespecialidades.Text = "Especialidade"
        divprocedimento.Visible = False
        divagendamedica.Visible = True

        hdchaveconsulta.Value = ""
        buscaplano()
        buscadependentes()
        cbomedicos.Items.Add("")
        Dim xmedicos As New clsmedicos
        xmedicos.listarmedicos()
        For x As Integer = 0 To xmedicos.Listamedicos.Count - 1
            cbomedicos.Items.Add(xmedicos.Listamedicos(x).Nome)
        Next

        habilitarclientesmodal(False)
        mostradependente.Style.Add("display", "none")

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

    Private Sub buscaplano()

        Dim tbplanos As New Data.DataTable
        Dim tabplanos As New clsBanco

        Dim cont As Integer
        tbplanos = tabplanos.conectar("select nrseq,descricao from tbplanos where ativo = true order by descricao")
        cboplano.Items.Clear()
        cboplano.Items.Add("")
        cboplanoconvenio.Items.Clear()
        cboplanoconvenio.Items.Add("")
        cboplanocliente.Items.Clear()
        cboplanocliente.Items.Add("")

        cont = cbomedicos.Items.Count

        For x As Integer = 0 To tbplanos.Rows.Count - 1
            cboplano.Items.Add(tbplanos.Rows(x)("descricao").ToString)
            cboplano.Items(cboplano.Items.Count - 1).Value = tbplanos.Rows(x)("nrseq").ToString

            cboplanoconvenio.Items.Add(tbplanos.Rows(x)("descricao").ToString)
            cboplanoconvenio.Items(cboplano.Items.Count - 1).Value = tbplanos.Rows(x)("nrseq").ToString

            cboplanocliente.Items.Add(New ListItem(tbplanos.Rows(x)("descricao").ToString, tbplanos.Rows(x)("nrseq").ToString))

        Next
    End Sub



    Private Sub carregamedicos()

        Dim tbplanos As New Data.DataTable
        Dim tabplanos As New clsBanco

        Dim cont As Integer

        tbplanos = tabplanos.conectar("select cod,nome from tbmedicos where ativo = true")
        cbomedicos.Items.Clear()
        cbomedicos.Items.Add("")

        cont = cbomedicos.Items.Count
        For x As Integer = 0 To tbplanos.Rows.Count - 1
            cbomedicos.Items.Add(tbplanos.Rows(x)("nome").ToString)
            cbomedicos.Items(cbomedicos.Items.Count - 1).Value = tbplanos.Rows(x)("cod").ToString
        Next

    End Sub
    Private Sub carregaconvenio()

        Dim tbconv As New Data.DataTable
        Dim tabconv As New clsBanco

        Dim cont As Integer

        tbconv = tabconv.conectar("select nrseq, nome from tbconvenios where ativo = true")
        cbomedicos.Items.Clear()
        cbomedicos.Items.Add("")

        cont = cbomedicos.Items.Count

        For x As Integer = 0 To tbconv.Rows.Count - 1
            cbomedicos.Items.Add(tbconv.Rows(x)("nome").ToString)
            cbomedicos.Items(cbomedicos.Items.Count - 1).Value = tbconv.Rows(x)("nrseq").ToString
        Next

    End Sub

    Private Sub buscadependentes()

        Dim tbplanos As New Data.DataTable
        Dim tabplanos As New clsBanco

        Dim cont As Integer

        tbplanos = tabplanos.conectar("select * from tbdependentes where ativo = true and associado='" & hdnrseqcliente.Value & "'")
        cbodependente.Items.Clear()
        cbodependente.Items.Add("")

        cont = cbomedicos.Items.Count
        For x As Integer = 0 To tbplanos.Rows.Count - 1
            cbodependente.Items.Add(New ListItem(tbplanos.Rows(x)("nome").ToString, tbplanos.Rows(x)("nrseq").ToString))
        Next

    End Sub

    Private Sub verificafin()

    End Sub

    'Private Sub habilitaretapas(etapa)

    '    Select Case etapa.ToString.ToLower

    '        Case Is = "0"

    '            btnstart.Enabled = True
    '            cboplano.Enabled = True
    '            cboservico.Enabled = True
    '            txtcliente.Enabled = False
    '            cboprocurarcliente.Enabled = False

    '        Case Is = "1"

    '            btnstart.Enabled = False
    '            cboplano.Enabled = False
    '            cboservico.Enabled = False
    '            '------ clientes 
    '            txtcliente.Enabled = True
    '            cboprocurarcliente.Enabled = True
    '            cbodependente.Enabled = True
    '            btncancelar1.Enabled = True
    '            btnescolha1.Enabled = True
    '            btnnovocliente.Enabled = True
    '            btnhabilitadependente.Enabled = True
    '            btnbuscacliente.Enabled = True
    '            'medicos
    '            cbomedicos.Enabled = False
    '            cboespecialidades.Enabled = False
    '            btnescolha2.Enabled = False
    '            btncancelar2.Enabled = False
    '            txtcaiteiraconv.Enabled = False
    '            cboplanoconvenio.Enabled = False
    '      '      verificafin()

    '        Case Is = "2"

    '            btnstart.Enabled = False
    '            cboplano.Enabled = False
    '            cboservico.Enabled = False
    '            '------ clientes 
    '            txtcliente.Enabled = False
    '            cboprocurarcliente.Enabled = False
    '            cbodependente.Enabled = False
    '            btncancelar1.Enabled = False
    '            btnescolha1.Enabled = False
    '            btnnovocliente.Enabled = False
    '            btnhabilitadependente.Enabled = False
    '            btnbuscacliente.Enabled = False
    '            'medicos
    '            cbomedicos.Enabled = True
    '            cboespecialidades.Enabled = True
    '            btnescolha2.Enabled = True
    '            btncancelar2.Enabled = True
    '            txtcaiteiraconv.Enabled = True
    '            cboplanoconvenio.Enabled = True
    '            btnescolha3.Enabled = True
    '            btncancelar3.Enabled = True

    '        Case Is = "3"

    '            btnstart.Enabled = False
    '            cboplano.Enabled = False
    '            cboservico.Enabled = False
    '            '---- clientes 
    '            txtcliente.Enabled = False
    '            cboprocurarcliente.Enabled = False
    '            cbodependente.Enabled = False
    '            btncancelar1.Enabled = False
    '            btnescolha1.Enabled = False
    '            btnnovocliente.Enabled = False
    '            btnhabilitadependente.Enabled = False
    '            btnbuscacliente.Enabled = False
    '            'medicos 
    '            cbomedicos.Enabled = False
    '            cboespecialidades.Enabled = False
    '            btnescolha2.Enabled = False
    '            btncancelar2.Enabled = False
    '            txtcaiteiraconv.Enabled = False
    '            cboplanoconvenio.Enabled = False

    '        Case Is = "4"

    '            btnstart.Enabled = True
    '            cboplano.Enabled = True
    '            cboservico.Enabled = True
    '            '---- clientes 
    '            txtcliente.Enabled = False
    '            cboprocurarcliente.Enabled = False
    '            cbodependente.Enabled = False
    '            btncancelar1.Enabled = False
    '            btnescolha1.Enabled = False
    '            btnnovocliente.Enabled = False
    '            btnhabilitadependente.Enabled = False
    '            btnbuscacliente.Enabled = False
    '            '---- medicos
    '            cbomedicos.Enabled = False
    '            cboespecialidades.Enabled = False
    '            btnescolha2.Enabled = False
    '            btncancelar2.Enabled = False
    '            txtcaiteiraconv.Enabled = False
    '            cboplanoconvenio.Enabled = False

    '    End Select

    'End Sub


    'Private Sub btncancelar1_Click(sender As Object, e As EventArgs) Handles btncancelar1.Click
    '        habilitaretapas(4)
    'End Sub
    'Private Sub btnhabilitadependente_Click(sender As Object, e As EventArgs) Handles btnhabilitadependente.Click
    '    mostradependente.Visible = True
    '    cboprocurarcliente.Enabled = True
    '    txtcliente.Enabled = False
    '    habilitaretapas(1)
    'End Sub
    '   Private Sub btnvoltadependente_Click(sender As Object, e As EventArgs) Handles btnvoltadependente.Click
    '   mostradependente.Visible = False
    '   End Sub

    Private Sub btnbuscacliente_Click(sender As Object, e As EventArgs) Handles btnbuscacliente.Click
        carregagradeclientes()
    End Sub

    Private Sub carregagradeclientes(Optional procura As String = "", Optional inativos As Boolean = False)

        Dim tbclientes As New Data.DataTable
        Dim tabclientes As New clsBanco

        tbclientes = tabclientes.conectar("Select * FROM tbclientes where " & cboprocurarcliente.Text & " like '%" & txtcliente.Text & "%' and liberado = '1'")

        gradeclientes.DataSource = tbclientes
        gradeclientes.DataBind()

        mostradependente.Style.Add("display", "none")
        chkusardependente.Checked = False

        hdnrseqcliente.Value = ""
        divmostragrade.Visible = True

    End Sub

    Private Sub carregahorariosmedicos(Optional procura As String = "", Optional inativos As Boolean = False)
        Dim consulta As String
        Dim valor As String
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        If dtagendamedica.Text <> "" Then
            valor = DiaSemana(dtagendamedica.Text)
            consulta = " and diasemana = '" & valor & "'"
        End If


        tb1 = tab1.conectar("Select * FROM tbmedicos_horarios where nrseqmedico = '" & hdacao.Value & "' and ativo = true")

        gradeohorarios.DataSource = tb1
        gradeohorarios.DataBind()

    End Sub
    Private Sub gradeclientes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeclientes.RowCommand

        Dim existe As Boolean = False

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)

        Dim xMedicos As New clsmedicos

        Dim row As GridViewRow = gradeclientes.Rows(index)
        Dim hdnrseq As HiddenField = CType(row.FindControl("hdnrseq"), HiddenField)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Select Case e.CommandName.ToString.ToLower
            Case Is = "selecionar"

                Dim xClientes As New clsclientes
                hdnrseqcliente.Value = hdnrseq.Value

                xClientes.nrseq = hdnrseq.Value

                If Not xClientes.agendacarregacliente() Then
                    sm("Swal.fire({icon: 'error',title: 'Não foi possivel efetuar esta ação...',text: 'Contate o Administrador do Sistema',})")
                    Exit Sub
                End If

                xClientes.nrseq = hdnrseq.Value
                hdnrseqcliente.Value = xClientes.nrseq
                lblretmatricula.Text = xClientes.Matricula
                txtcliente.Text = xClientes.Nome

                divmostragrade.Visible = False
                'buscarfinanceiro()
        End Select

    End Sub

    'Private Sub btnescolha2_Click(sender As Object, e As EventArgs) Handles btnescolha2.Click

    '    Dim tbescolha As New Data.DataTable
    '    Dim tabescolha As New clsBanco


    '    Dim cont As Integer

    '    tbescolha = tabescolha.conectar("SELECT A.descricao as descricao, B.nrseq  as nrseq FROM tbmedicosespecialidades as B inner join tbespecialidades as A on B.nrseqespecialidade = A.cod where B.nrseqmedico = '" & cbomedicos.SelectedValue & "' and B.ativo = true order by A.descricao asc")

    '    cboespecialidades.Items.Clear()
    '    cboespecialidades.Items.Add("")

    '    cont = cboespecialidades.Items.Count

    '    For x As Integer = 0 To tbescolha.Rows.Count - 1
    '        cboespecialidades.Items.Add(New ListItem(tbescolha.Rows(x)("descricao").ToString, tbescolha.Rows(x)("nrseqespecialidade").ToString))
    '    Next

    '    hdacao.Value = cbomedicos.SelectedValue
    '    soteste.Text = hdacao.Value
    '    carregahorariosmedicos()



    '    ''   cbomedicos.Enabled = False


    'End Sub

    'Private Sub btncancelar2_Click(sender As Object, e As EventArgs) Handles btncancelar2.Click
    '    ''''   habilitaretapas(1)
    'End Sub

    Private Sub gradeohorarios_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeohorarios.RowCommand
        Dim existe As Boolean = False


        Dim index As Integer = Convert.ToInt32(e.CommandArgument)

        Dim xMedicos As New clsmedicos
        Dim row As GridViewRow = gradeohorarios.Rows(index)
        Dim hdnrseq As HiddenField = CType(row.FindControl("hdnrseq"), HiddenField)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco



        Dim horaagendar As HiddenField = row.FindControl("hora")



        Select Case e.CommandName.ToString.ToLower
            Case Is = "selecionar"

                Dim xAgenda As New clsAgenda_medica
                Dim informar As String
                Dim Mov As String


                If cboplano.SelectedValue = "" Then
                    informar = informar + " [Plano] "
                End If

                'If cboservico.SelectedValue = "" Then
                '    informar = informar + " [Serviço] "
                'End If

                If hdnrseqcliente.Value = "" Then
                    informar = informar + " [Cliente] "
                End If
                If cbomedicos.SelectedValue = "" Then
                    informar = informar + " [Médico] "
                End If

                If cboespecialidades.SelectedValue = "" Then
                    informar = informar + " [Especialidade] "
                End If

                If dtagendamedica.Text = "" Then
                    informar = informar + " [Data] "
                End If

                If txtvalor.Text = "" Then
                    informar = informar + " [Valores] "
                End If




                If informar <> "" Then
                    sm("Swal.fire({icon: 'error',title: ' O Campo " & informar & " ',text: 'Preencha os campos que estão faltando ',})")
                    Exit Sub
                End If

                If Not xAgenda.novo() Then
                    sm("Swal.fire({icon: 'error',title: 'Erro ao inserir novo horario',text: 'Contate o Administrador do Sistema',})")
                    Exit Sub
                End If

                Mov = xAgenda.Nrseq

                xAgenda.Nrseq = Mov
                xAgenda.Nrseqmedico = cbomedicos.SelectedValue

                If cbodependente.SelectedValue <> "" Then
                    xAgenda.Nrseqdependente = cbodependente.SelectedValue
                End If

                xAgenda.Nrseqpaciente = hdnrseqcliente.Value
                xAgenda.Nrseqplano = cboplano.SelectedValue
                xAgenda.Nrseqservico = 1 'cboservico.SelectedValue
                xAgenda.Valor = txtvalor.Text
                ' xAgenda.Valorrep = txtvalortotal.Text
                xAgenda.Nrseqespecialidade = cboespecialidades.SelectedValue
                xAgenda.Data = dtagendamedica.Text
                xAgenda.Hora = horaagendar.Value

                If Not xAgenda.salvarmarcarconsulta() Then
                    sm("Swal.fire({icon: 'error',title: 'Não foi possivel salvar na agenda',text: 'Contate o Administrador do Sistema',})")
                    Exit Sub
                End If
                sm("Swal.fire({  position: 'center',  icon: 'success',  title: 'Agendamento Efetuado com Sucesso',  showConfirmButton: false,  timer: 2000})")





                '        xClientes.Nrseq = hdnrseq.Value
                '        Dim xClientes As New clsClientes
                '        hdnrseqcliente.Value = hdnrseq.Value

                '        xClientes.Nrseq = hdnrseq.Value

                '        If Not xClientes.consultagendamedica() Then
                '            sm("Swal.fire({icon: 'error',title: 'Não foi possivel Evetuar esta ação...',text: 'Contate o Administrador do Sistema',})")
                '            Exit Sub
                '        End If

                '        xClientes.Nrseq = hdnrseq.Value

                '        hdnrseqcliente.Value = xClientes.Nrseq
                '        lblretmatricula.Text = xClientes.Matricula
                '        txtcliente.Text = xClientes.Nome

                '        divmostragrade.Visible = False

                '        'buscarfinanceiro()

        End Select

    End Sub
    Public Shared Function DiaSemana(ByVal xdata As Date) As String

        Dim dia As Integer = xdata.DayOfWeek

        dia = dia + 1
        If dia > 6 Then
            dia = 0
        End If

        Select Case dia
            Case Is = 0
                '    Return "Domingo"
                Return "0"
            Case Is = 1
                '   Return "Segunda"
                Return "1"
            Case Is = 2
                '   Return "Terça"
                Return "2"
            Case Is = 3
                'Return "Quarta"
                Return "3"
            Case Is = 4
                'Return "Quinta"
                Return "4"
            Case Is = 5
                '  Return "Sexta"
                Return "5"
            Case Is = 6
                'Return "Sábado"
                Return "6"
        End Select

        Return ""

    End Function
    Private Sub executarmodal(Optional arquivo As String = "", Optional tabela As Data.DataTable = Nothing)
        mdvisualizar.Style.Add("display", "none")
        mdarquivos.Style.Add("display", "none")
        'If arquivo = "" Then
        '    GradeArquivos.DataSource = tabela
        '    GradeArquivos.DataBind()
        '    mdarquivos.Style.Remove("display")
        'Else
        mdvisualizar.Style.Remove("display")
        '    frameimpressao.Attributes.Add("src", arquivo)
        'End If

        sm("$(""#myModal"").modal()")

    End Sub

    Private Sub gradeohorarios_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeohorarios.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub

        If e.Row.Cells(2).Text <> "" Then
            e.Row.ForeColor = System.Drawing.Color.White
            e.Row.BackColor = System.Drawing.Color.LightSeaGreen
        Else
            e.Row.ForeColor = System.Drawing.Color.White
            e.Row.BackColor = System.Drawing.Color.Red
        End If
    End Sub

    Private Sub chkusardependente_CheckedChanged(sender As Object, e As EventArgs) Handles chkusardependente.CheckedChanged
        If hdnrseqcliente.Value = "" Then
            sm("Swal.fire({icon: 'error',title: 'Cliente não foi selecionado.',text: 'Por favor, selecione um cliente para processeguir',})")
            chkusardependente.Checked = False
            Exit Sub
        End If

        If chkusardependente.Checked Then
            mostradependente.Style.Remove("display")
            buscadependentes()
        Else
            mostradependente.Style.Add("display", "none")
        End If
    End Sub

    Private Sub cbomedicos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbomedicos.SelectedIndexChanged
        If cbomedicos.Text = "" Then Exit Sub


        If cboplano.Text = "" Then
            sm("swal({title: 'Ops!',text: 'Nenhum plano selecionado.',type: 'error',confirmButtonText: 'OK'})", "swal")
            If Not cbomedicos.SelectedIndex < 0 Then
                cbomedicos.SelectedIndex = 0
            End If
            If Not cboespecialidades.SelectedIndex < 0 Then
                cboespecialidades.SelectedIndex = 0
            End If
            If Not cboplanoconvenio.SelectedIndex < 0 Then
                cboplanoconvenio.SelectedIndex = 0
            End If
            Exit Sub
        End If

        Dim xmedico As New clsmedicos
        xmedico.Nome = cbomedicos.SelectedItem.Text
        If Not xmedico.consultarMedicos(True) Then
            sm("swal({title: 'Ops!',text: 'Não localizamos o profissional !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        cboespecialidades.Items.Clear()
        cboespecialidades.Items.Add("")

        For x As Integer = 0 To xmedico.Listaespecialiadades.Count - 1
            If xmedico.Listaespecialiadades(x).Plano = cboplano.SelectedItem.Text Then
                cboespecialidades.Items.Add(New ListItem(xmedico.Listaespecialiadades(x).Descricao, xmedico.Listaespecialiadades(x).Nrseqespecialidade))
            End If
        Next
        If cboespecialidades.Items.Count = 1 Then
            sm("swal({title: 'Atenção!',text: 'Nenhuma especialidade localizada para esse medico nesse plano !',type: 'warning',confirmButtonText: 'OK'})", "swal")
        End If
    End Sub

    Private Sub cboespecialidades_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboespecialidades.SelectedIndexChanged
        If cbomedicos.Text = "" Then Exit Sub
        Dim xespe As New clsMedicosEspecialidades
        xespe.Nomemedico = cbomedicos.SelectedItem.Text
        xespe.Descricao = cboespecialidades.SelectedItem.Text
        xespe.Plano = cboplano.SelectedItem.Text
        If Not xespe.procurarvalor Then
            sm("swal({title: 'Atenção!',text: 'Essa especialidade não foi localizada para esse plano !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        hdnvlrepasse.Value = xespe.Vlrepasse
        txtvalor.Text = FormatCurrency(xespe.Valorespecialidades)

        txtsomadovalor.Text = CDbl(numeros(txtmpvaltotal.Text) + numeros(txtvalor.Text))
    End Sub

    Private Sub dtagendamedica_TextChanged(sender As Object, e As EventArgs) Handles dtagendamedica.TextChanged
        Dim xagenda As New clsAgenda_medica
        xagenda.Nomemedico = cbomedicos.SelectedItem.Text
        xagenda.Data = dtagendamedica.Text
        xagenda.carregaragendaDiaMedico(dtagendamedica.Text)
        gradeohorarios.DataSource = xagenda.Agendadia
        gradeohorarios.DataBind()
    End Sub

    Private Sub btnReativar_Click(sender As Object, e As EventArgs) Handles btnReativar.Click

    End Sub

    Private Sub btnnovocliente_Click(sender As Object, e As EventArgs) Handles btnnovocliente.Click
        executarmodal("")
    End Sub

    Private Sub btnsalvarconsulta_Click(sender As Object, e As EventArgs) Handles btnsalvarconsulta.Click
        Dim paciente As String = txtcliente.Text
        Dim ehdependente As Boolean = False
        'If cbodependente.Text <> "" AndAlso Not cbodependente.SelectedItem.Text = "" Then
        '    paciente = cbodependente.SelectedItem.Text
        '    ehdependente = True
        'End If
        Dim xcons As New clsAgenda_medica
        xcons.Retorno = chkretorno.Checked
        Dim informatela As String
        If hdnrseqcliente.Value = "" Then
            informatela = informatela + " [Cliente] "
        End If
        If cbomedicos.SelectedValue = "" Then
            informatela = informatela + " [Medico] "
        End If
        If cboplano.Text = "" Then
            informatela = informatela + " [Plano] "
        End If

        If cboespecialidades.SelectedValue = "" Then
            informatela = informatela + " [Especialidade] "
        End If
        If dtagendamedica.Text = "" Then
            informatela = informatela + " [Data] "
        End If

        If informatela <> "" Then
            sm("Swal.fire({icon: 'error',title: 'Por favor, informar: " & informatela & " ',text: 'para poder salvar a consulta ',})")
            Exit Sub
        End If

        xcons.Retorno = chkretorno.Checked
        xcons.Primeiraconsulta = chkprimeiraconsulta.Checked
        xcons.Nrseqpaciente = hdnrseqcliente.Value
        xcons.Nomemedico = cbomedicos.SelectedItem.Text
        xcons.Data = dtagendamedica.Text
        xcons.Paciente = paciente
        xcons.Valor = numeros(txtsomadovalor.Text)
        xcons.Plano = cboplano.SelectedItem.Text
        xcons.Especialidade = cboespecialidades.SelectedItem.Text
        xcons.Chave = hdchaveconsulta.Value
        xcons.Nrseqplano = cboplano.SelectedValue
        xcons.Nrseqempresa = Session("idempresaemuso")
        xcons.Nrseqespecialidade = cboespecialidades.SelectedValue
        xcons.Usermarcouconsulta = Session("usuario")
        xcons.Valorrep = hdnvlrepasse.Value
        If cbodependente.SelectedValue <> "" AndAlso cbodependente IsNot Nothing Then
            xcons.Paciente = cbodependente.SelectedItem.Text
            xcons.Ehtitular = False
            xcons.Nrseqdependente = cbodependente.SelectedValue
        Else
            xcons.Paciente = txtcliente.Text
            xcons.Ehtitular = True
        End If

        If sonumeros(txtencaixe.Text) = 0 Then
            For Each row As GridViewRow In gradeohorarios.Rows
                Dim chkselgrade As CheckBox = row.FindControl("chkselgrade")
                If chkselgrade.Checked Then
                    xcons.Hora = row.Cells(1).Text
                    Exit For
                End If
            Next
        Else
            xcons.Hora = txtencaixe.Text
        End If

        If Not xcons.marcarconsulta() Then
            sm("swal({title: 'Atenção!',text: '" & xcons.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        sm("Swal.fire({icon: 'Success',title: 'Agendamento realizado com Sucesso',text: 'Limpando para novo agendamento',})")


        Response.Redirect("marcarconsulta.aspx")

    End Sub

    Private Sub limpamarcacao()

        hdchaveconsulta.Value = ""
        cboplano.SelectedValue = ""
        hdnrseqcliente.Value = ""
        cbomedicos.SelectedItem.Text = ""
        dtagendamedica.Text = ""
        txtvalor.Text = ""
        cboplano.Text = ""
        cboespecialidades.SelectedItem.Text = ""
        hdchaveconsulta.Value = ""

    End Sub

    Private Sub resetmodalproc()
        sm("$(""#modalprocedimentos"").modal('hide');")
        sm("$('body')removeClass('modal-open');")
        sm("$('.modal-backdrop').remove();")
        sm("$(""#modalprocedimentos"").modal();")
    End Sub

    Private Sub hidemodalproc()
        sm("$(""#modalprocedimentos"").modal('hide');")
        sm("$('body')removeClass('modal-open');")
        sm("$('.modal-backdrop').remove();")
    End Sub

    Private Sub openmodalproc()
        sm("$(""#modalprocedimentos"").modal();")
    End Sub

    Private Sub resetmodal()
        sm("$(""#myModal"").modal('hide');")
        sm("$('body')removeClass('modal-open');")
        sm("$('.modal-backdrop').remove();")
        sm("$(""#myModal"").modal()")
        sm("$(""#modalprocedimentos"").modal();")
    End Sub

    Private Sub hidemodal()
        sm("$(""#myModal"").modal('hide');")
        sm("$('body')removeClass('modal-open');")
        sm("$('.modal-backdrop').remove();")
    End Sub


    Private Sub txtcpfcliente_TextChanged(sender As Object, e As EventArgs) Handles txtcpfcliente.TextChanged
        'If txtcpfcliente.Text <> "" Then
        '    txtnomecliente.Text = ""
        '    txtnomecliente.Enabled = False
        '    cboplanocliente.Text = ""
        'End If
        'If txtcpfcliente.Text = "" Then
        '    gradeclientesmodal.DataSource = Nothing
        '    gradeclientesmodal.DataBind()
        '    txtnomecliente.Enabled = True
        'End If

        resetmodal()

    End Sub

    Private Sub txtnomecliente_TextChanged(sender As Object, e As EventArgs) Handles txtnomecliente.TextChanged
        'If txtnomecliente.Text <> "" Then
        '    txtcpfcliente.Text = ""
        '    txtcpfcliente.Enabled = False
        '    cboplanocliente.Text = ""
        'End If
        'If txtnomecliente.Text = "" Then
        '    gradeclientesmodal.DataSource = Nothing
        '    gradeclientesmodal.DataBind()
        '    txtcpfcliente.Enabled = True
        'End If

        resetmodal()
    End Sub

    Private Sub gradeclientesmodal_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeclientesmodal.RowDataBound

    End Sub

    Private Sub gradeclientesmodal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeclientesmodal.RowCommand

        Dim existe As Boolean = False

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)

        Dim xMedicos As New clsmedicos

        Dim row As GridViewRow = gradeclientesmodal.Rows(index)
        Dim hdnrseq As HiddenField = CType(row.FindControl("hdnrseq"), HiddenField)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Select Case e.CommandName.ToString.ToLower

            Case Is = "selecionar"

                Dim xclientes As New clsclientes

                xclientes.nrseq = hdnrseq.Value



                If Not xclientes.consultar Then

                    hidemodal()


                    Exit Sub
                End If

                hdnnrseqmodal.Value = xclientes.nrseq
                txtnomecliente.Text = xclientes.Nome
                txtcpfcliente.Text = xclientes.Cpf
                txtmatriculacliente.Text = xclientes.Matricula
                txtdtnascimentocliente.Text = xclientes.Dtnasc
                txtcelularcliente.Text = xclientes.Telcelular
                txttelcliente.Text = xclientes.Telresidencia
                cboplanocliente.SelectedValue = xclientes.Plano

                cboplanocliente.Enabled = False

                habilitarclientesmodal(True)
                resetmodal()
                'buscarfinanceiro()
        End Select
    End Sub


    Private Sub habilitarclientesmodal(Optional habilita As Boolean = True)
        '  txtmatriculacliente.Enabled = habilita
        txtdtnascimentocliente.Enabled = habilita
        txttelcliente.Enabled = habilita
        txtcelularcliente.Enabled = habilita
        btnprocurarcliente.Enabled = Not habilita
    End Sub


    Private Sub btnprocurarcliente_Click(sender As Object, e As EventArgs) Handles btnprocurarcliente.Click

        gradeclientesmodal.DataSource = Nothing
        gradeclientesmodal.DataBind()

        Dim xClientes As New clsclientes

        Dim consulta As String = ""


        If txtcpfcliente.Text <> "" AndAlso txtnomecliente.Text <> "" Then
            resetmodal()
            sm("Swal.fire({icon: 'error',title: 'Escolha um dos campos para consultar.',text: 'Preencha o campo nome ou cpf!',})")
            txtcpfcliente.Text = ""
            txtnomecliente.Text = ""
            Exit Sub
        End If

        habilitarclientesmodal(False)
        If txtnomecliente.Text <> "" Then
            xClientes.Nome = txtnomecliente.Text
            If Not xClientes.consultarnomecpf(True) Then
                resetmodal()
                sm("Swal.fire({icon: 'error',title: '" & xClientes.Mensagemerro & " ',text: 'Para criar um novo cliente com esses dados por favor, selecione um plano.',})")
                cboplanocliente.Enabled = True
                Exit Sub
            End If
        End If

        If txtcpfcliente.Text <> "" Then
            xClientes.Cpf = txtcpfcliente.Text
            If Not xClientes.consultarnomecpf(False, True) Then
                resetmodal()
                sm("Swal.fire({icon: 'error',title: '" & xClientes.Mensagemerro & " ',text: ' Para criar um novo cliente com esses dados por favor, selecione um plano.',})")
                cboplanocliente.Enabled = True
                Exit Sub
            End If
        End If

        If txtcpfcliente.Text = "" AndAlso txtnomecliente.Text = "" Then
            resetmodal()
            sm("Swal.fire({icon: 'error',title: 'Campos não preenchidos',text: 'Informe o nome ou o cpf do cliente para a pesquisa!',})")
            Exit Sub
        End If

        If xClientes.Contador > 0 Then

            txtnomecliente.Enabled = True
            txtcpfcliente.Enabled = True

            cboplanocliente.Enabled = False

            resetmodal()

            gradeclientesmodal.DataSource = xClientes.Table
            gradeclientesmodal.DataBind()
        Else

            sm("Swal.fire({icon: 'warning',title: 'Cliente informado não existe!',text: 'Selecione um plano para criar um novo cliente!',})")
            cboplanocliente.Enabled = True
            gradeclientesmodal.DataSource = tb1
            gradeclientesmodal.DataBind()
            resetmodal()

        End If
    End Sub

    Private Sub btnsalvarnovocliente_Click(sender As Object, e As EventArgs) Handles btnsalvarnovocliente.Click

        Dim xclientes As New clsclientes

        xclientes.nrseq = hdnnrseqmodal.Value


        If Not xclientes.consultar() Then

            sm("Swal.fire({icon: 'danger',title: '" & xclientes.Mensagemerro & "',text: 'Erro ao salvar o cliente!',})")
            Exit Sub
        End If

        xclientes.Matricula = txtmatriculacliente.Text
        xclientes.Nome = txtnomecliente.Text
        xclientes.Cpf = txtcpfcliente.Text
        xclientes.Dtnasc = txtdtnascimentocliente.Text
        xclientes.Telcelular = txtcelularcliente.Text
        xclientes.Telresidencia = txttelcliente.Text
        xclientes.Plano = cboplanocliente.SelectedValue

        If Not xclientes.salvar() Then
            sm("Swal.fire({icon: 'error',title: 'Não foi possivel efetuar esta ação...',text: 'Contate o Administrador do Sistema',})")
            Exit Sub
        End If

        If Not xclientes.consultar() Then
            sm("Swal.fire({icon: 'error',title: 'Não foi possivel efetuar esta ação...',text: 'Contate o Administrador do Sistema',})")
            Exit Sub
        End If

        cboplano.Enabled = False

        gradeclientesmodal.DataSource = xclientes.Table
        gradeclientesmodal.DataBind()

        limparmodal()
        habilitarclientesmodal(False)
        buscaplano()
        hidemodal()
    End Sub

    Private Sub limparmodal()
        txtmatriculacliente.Text = ""
        txtcpfcliente.Text = ""
        txtdtnascimentocliente.Text = ""
        txtnomecliente.Text = ""
        If cboplanocliente.SelectedIndex > 0 Then
            cboplanocliente.SelectedIndex = 0
        End If

        txttelcliente.Text = ""
        txtcelularcliente.Text = ""

    End Sub

    Private Sub cboplanocliente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboplanocliente.SelectedIndexChanged

        resetmodal()
        btnprocurarcliente.Enabled = False
        If hdnnrseqmodal.Value <> "" Then
            Exit Sub
        End If

        If gradeclientesmodal.Rows.Count > 0 Then
            Exit Sub
        End If

        Dim xclientes As New clsclientes


        If Not xclientes.Novo() Then

            sm("Swal.fire({icon: 'error',title: 'Não foi possivel efetuar esta ação...',text: 'Contate o Administrador do Sistema',})")

            Exit Sub
        End If


        hdnnrseqmodal.Value = xclientes.nrseq
        xclientes.Plano = cboplanocliente.SelectedValue

        If Not xclientes.geramatricula() Then
            Exit Sub
        End If


        txtcpfcliente.Enabled = True
        txtnomecliente.Enabled = True
        txtcpfcliente.AutoPostBack = False
        txtnomecliente.AutoPostBack = False


        cboplanocliente.Enabled = False

        txtmatriculacliente.Enabled = False
        txtmatriculacliente.Text = xclientes.Matricula

        habilitarclientesmodal(True)
    End Sub

    Private Sub brnprocedimento_Click(sender As Object, e As EventArgs) Handles brnprocedimento.Click
        If cboplano.SelectedValue = "" Then

            sm("Swal.fire({icon: 'error',title: 'Não foi possivel efetuar esta ação...',text: 'Selecione um Plano',})")
            Exit Sub
        End If

        If hdchaveconsulta.Value = "" Then
            hdchaveconsulta.Value = gerarnrseqcontrole()
        End If

        hdnrseqplanomodal.Value = cboplano.SelectedItem.Text
        cbompplan.Text = cboplano.SelectedItem.Text
        lblrchave.Text = hdchaveconsulta.Value

        carregaconvmodal()

        openmodalproc()
    End Sub


    Private Sub carregaconvmodal()

        Dim tbconv As New Data.DataTable
        Dim tabconv As New clsBanco

        Dim cont As Integer

        tbconv = tabconv.conectar("select nrseq, nome from tbconvenios where ativo = true")
        cbompconv.Items.Clear()
        cbompconv.Items.Add("")

        cont = cbompconv.Items.Count
        For x As Integer = 0 To tbconv.Rows.Count - 1
            cbompconv.Items.Add(tbconv.Rows(x)("nome").ToString)
            cbompconv.Items(cbompconv.Items.Count - 1).Value = tbconv.Rows(x)("nrseq").ToString
        Next

        resetmodalproc()


    End Sub

    Private Sub grademodalproc()

        'Dim xProcedimentos As New clsmedicosprocedimentos

        'xProcedimentos.Nrseq = hdchaveconsulta.Value

        'If Not xProcedimentos.carregarprocedimentosmodal() Then
        '    sm("Swal.fire({icon: 'error',title: 'Não foi possivel efetuar esta ação...',text: 'Contate o Administrador do Sistema',})")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select P.descricao AS Procedimento ,  C.nome AS Convenio , AP.chave AS chave, AP.nrseq AS nrseq, AP.valor As Valor from tbagendamedica_procedimentos AS AP join  tbprocedimentos  AS P on AP.nrseqprocedimento = P.nrseq join  tbconvenios  AS C on AP.nrseqconvenio = C.nrseq where AP.chave = '" & hdchaveconsulta.Value & "'")

        gradempproced.DataSource = tb1
        gradempproced.DataBind()


    End Sub

    Private Sub valortotalmodalproc()

        tb1 = tab1.conectar("select sum(valor) AS valor from tbagendamedica_procedimentos where chave = '" & hdchaveconsulta.Value & "'")

        txtmpvaltotal.Text = tb1.Rows(0)("valor").ToString

    End Sub

    Private Sub cbompconv_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbompconv.SelectedIndexChanged

        Dim tbconv As New Data.DataTable
        Dim tabconv As New clsBanco

        Dim cont As Integer

        tbconv = tabconv.conectar("select nrseq , descricao  from tbprocedimentos where ativo = true and convenio = '" & cbompconv.SelectedValue & "'")
        cbompproc.Items.Clear()
        cbompproc.Items.Add("")

        cont = cbompproc.Items.Count
        For x As Integer = 0 To tbconv.Rows.Count - 1
            cbompproc.Items.Add(tbconv.Rows(x)("descricao").ToString)
            cbompproc.Items(cbompproc.Items.Count - 1).Value = tbconv.Rows(x)("nrseq").ToString
        Next

        resetmodalproc()
    End Sub

    Private Sub cbompproc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbompproc.SelectedIndexChanged

        Dim xProcedimentos As New clsProcedimentos

        xProcedimentos.Nrseq = cbompproc.SelectedValue

        If Not xProcedimentos.medicosprocedimentos() Then
            sm("Swal.fire({icon: 'error',title: 'Não foi possivel Selecionar o procedimento',text: 'Contate o Administrador do Sistema',})")
            Exit Sub
        End If

        txtmodalproccodigo.Text = xProcedimentos.Nrseq
        txtmodalprocdescricao.Text = xProcedimentos.Descricao
        txtmodalprocvalorcusto.Text = xProcedimentos.Valorcusto
        txtmodalprocvalorfinal.Text = xProcedimentos.Valorfinal


        resetmodalproc()
    End Sub

    Private Sub btnaddmodalproc_Click(sender As Object, e As EventArgs) Handles btnaddmodalproc.Click
        Dim xMP As New clsmedicosprocedimentos
        Dim tela As String = ""

        If cbompconv.SelectedValue = "" Then
            tela = tela + " [Convenio] "
        End If

        If cbompproc.SelectedValue = "" Then
            tela = tela + " [procedimento] "
        End If

        If tela <> "" Then
            sm("Swal.fire({icon: 'error',title: 'Por favor Informar " & tela & "',text: 'para prosseguir',})")
            resetmodalproc()
            Exit Sub
        End If
        xMP.Chave = hdchaveconsulta.Value
        xMP.Nrseqconvenio = cbompconv.SelectedValue
        xMP.Nrseqprocedimento = cbompproc.SelectedValue
        xMP.Valormp = txtmodalprocvalorfinal.Text


        If Not xMP.novamp() Then
            sm("Swal.fire({icon: 'error',title: 'Não foi possivel adicionar Procedimento',text: 'Contate o Administrador do Sistema',})")
            Exit Sub
        End If

        grademodalproc()
        valortotalmodalproc()
        resetmodalproc()
    End Sub

    Private Sub gradempproced_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradempproced.RowDataBound


    End Sub

    Private Sub gradempproced_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradempproced.RowCommand

        Dim existe As Boolean = False
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)

        Dim row As GridViewRow = gradempproced.Rows(index)
        Dim hdnrseq As HiddenField = CType(row.FindControl("hdnrseq"), HiddenField)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Select Case e.CommandName.ToString.ToLower

            Case Is = "deletar"

                Dim xMP As New clsmedicosprocedimentos

                xMP.Nrseq = hdnrseq.Value

                If Not xMP.deletar1 Then
                    sm("Swal.fire({icon: 'erro',title: 'Não Foi possivel Excluir este Procedimento ',text: 'Contate o administrador do sistema',})")
                    resetmodalproc()
                    Exit Sub
                End If

                grademodalproc()
                valortotalmodalproc()
                resetmodalproc()

        End Select
    End Sub

    Private Sub btnmpsalvar_Click(sender As Object, e As EventArgs) Handles btnmpsalvar.Click
        sm("Swal.fire({icon: 'Success',title: 'Procedimentos Adicionados',text: 'Para salvar favor confirmar a consulta',})")

        txtvalorprocedimentos.Text = txtmpvaltotal.Text

        txtsomadovalor.Text = CDbl(numeros(txtmpvaltotal.Text) + numeros(txtvalor.Text))

        hidemodalproc()

    End Sub

    Private Sub btnmplimpar_Click(sender As Object, e As EventArgs) Handles btnmplimpar.Click

        Dim xMP As New clsmedicosprocedimentos

        xMP.Chave = hdchaveconsulta.Value

        If Not xMP.deletartodos Then
            sm("Swal.fire({icon: 'erro',title: 'Não Foi possivel Limpar este Procedimento ',text: 'Contate o administrador do sistema',})")
            resetmodalproc()
            Exit Sub
        End If

        sm("Swal.fire({icon: 'info',title: 'Procedimentos Excliudos',text: '',})")

        grademodalproc()
        valortotalmodalproc()
        resetmodalproc()

    End Sub

    Private Sub cboespecialidades_TextChanged(sender As Object, e As EventArgs) Handles cboespecialidades.TextChanged
        Dim xmedicosespecialidades As New clsMedicosEspecialidades

        xmedicosespecialidades.Nrseq = cboespecialidades.SelectedValue

        If Not xmedicosespecialidades.procurar() Then
            Exit Sub
        End If

        hdnvlrepasse.Value = xmedicosespecialidades.Vlrepasse

    End Sub

    Private Sub cboplano_TextChanged(sender As Object, e As EventArgs) Handles cboplano.TextChanged
        If cboplano.Text = "" Then
            sm("Swal.fire({icon: 'info',title: 'Selecione o Plano e o Serviço',text: 'Campo em branco ',})")
            Exit Sub
        End If

        lblmedicos.Text = "Médico"
        lblespecialidades.Text = "Especialidade"
        divprocedimento.Visible = False
        divagendamedica.Visible = True
        ''      habilitaretapas(1)
    End Sub
End Class

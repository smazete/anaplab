﻿<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="procedimentos.aspx.vb" Inherits="restrito_procedimentos" EnableSessionState="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../js/JScriptmascara.js" type="text/javascript"></script>
    <style>
        .lupa {
            margin-top: 26px;
            margin-left: 10px;
        }

        .center {
            text-align: center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updategeral" runat="server">
        <ContentTemplate>

            <div class="box box-primary" style="moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
                <div class="box box-header "><b>Cadastro de Procedimentos</b></div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:HiddenField ID="hdconexao" runat="server" />
                            Plano
                            <br />
                            <asp:DropDownList runat="server" ID="ddlPlano" CssClass="form-control" ClientIDMode="Static" AutoPostBack="true"></asp:DropDownList>
                        </div>
                        <div class="col-lg-3">
                            Convênio
                            <br />
                            <asp:DropDownList runat="server" ID="ddlConvenio" CssClass="form-control" ClientIDMode="Static"></asp:DropDownList>
                        </div>
                        <div class="col-lg-3">

                            <asp:LinkButton runat="server" ID="btnprocura" CssClass="btn btn-danger lupa" ClientIDMode="Static"><i class="fa fa-search"></i> Carregar</asp:LinkButton>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updategeral">
                            <ProgressTemplate>
                                <img src="../img/carregando1.gif" style="width: 3rem; height: 3rem;" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <b>Detalhe de Procedimentos</b>
                            </div>
                            <div class="box-body">

                                <asp:UpdatePanel ID="updatepanel3" runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                                <br />
                                                <asp:Button runat="server" CssClass="btn btn-primary center-block " ID="btnnovo" Text="Novo" />
                                                <br />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class=" col-lg-2">
                                                Código<br />
                                                <asp:TextBox runat="server" type="text" class="form-control" ID="txtcodigo" ClientIDMode="Static" ReadOnly="true" />
                                            </div>
                                            <div class="col-lg-5">
                                                Descrição
                                                <br />
                                                <asp:TextBox runat="server" type="text" class="form-control" ID="txtdescricao" ClientIDMode="Static" />
                                            </div>
                                            <div class="col-lg-2">
                                                Valor de Custo<br />
                                                <asp:TextBox runat="server" type="text" class="form-control moedasemnegativo" ID="txtvalorcusto" onkeyup="formataValor(this,event);" ClientIDMode="Static" />
                                            </div>
                                            <div class="col-lg-2">
                                                Valor Final<br />
                                                <asp:TextBox runat="server" class="form-control" ID="txtvalorfinal" ClientIDMode="Static" AutoPostBack="true" onkeyup="formataValor(this,event);" OnTextChanged="valoraltera_TextChanged" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                Código AMB<br />
                                                <asp:TextBox runat="server" type="text" class="form-control" ID="txtcodamb" ClientIDMode="Static" />
                                            </div>
                                            <div class="col-lg-2">
                                                Código TUSS<br />
                                                <asp:TextBox runat="server" type="text" class="form-control" ID="txtcodtuss" ClientIDMode="Static" />
                                            </div>

                                            <asp:HiddenField ID="HdnSelectedRowIndex" runat="server" />
                                            <asp:TextBox type="hidden" ID="hdnCount" runat="server" ClientIDMode="static" class="hide" />
                                            <div class="col-lg-2">
                                                % Cobertura Plano<br />
                                                <asp:TextBox runat="server" CssClass="form-control porcentagem" ID="txt_percente_cob_plano" ClientIDMode="Static"
                                                    AutoPostBack="true" OnTextChanged="valoraltera_TextChanged" onkeyup="formataValor(this,event);"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-2">
                                                R$ Cobertura do Plano<br />
                                                <asp:TextBox ReadOnly="true" runat="server" CssClass="form-control moedasemnegativo" ID="txt_valor_cob_plano" ClientIDMode="Static"
                                                    AutoPostBack="true" OnTextChanged="valoraltera_TextChanged" onkeyup="formataValor(this,event);"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-2">
                                                % Paciente<br />
                                                <asp:TextBox ReadOnly="true" runat="server" CssClass="form-control porcentagem" ID="txt_percente_paciente" ClientIDMode="Static"
                                                    AutoPostBack="true" OnTextChanged="valoraltera_TextChanged" onkeyup="formataValor(this,event);"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-2">
                                                R$ Paciente<br />
                                                <asp:TextBox runat="server" CssClass="form-control moedasemnegativo" ID="txt_valor_paciente" ReadOnly="true" ClientIDMode="Static"
                                                    AutoPostBack="true" OnTextChanged="valoraltera_TextChanged" onkeyup="formataValor(this,event);"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <br />
                                                <asp:CheckBox runat="server" CssClass="checkbox-inline" ID="cboincluivalor" Text="Não incluir o valor no caixa do funcionário" Enabled="false" />

                                            </div>
                                        </div>
                                        <div class="row text-align-center">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-2">
                                                <br />
                                                <asp:Button runat="server" CssClass="btn btn-success" ID="btnsalvar" Text="Salvar" />
                                            </div>
                                            <div class="col-lg-2">
                                                <br />
                                                <asp:Button runat="server" CssClass="btn btn-danger" ID="btncancelar" Text="Cancelar" />
                                            </div>
                                            <div class="col-lg-4"></div>



                                        </div>
                                        <br />
                                        <div class="row" id="divprocedimentos" runat="server">
                                            <div class="col-lg-12 ">
                                                <div class="box box-primary">
                                                    <div class="box-header "><b>Lista de Procedimentos</b></div>
                                                    <div class="box-body">

                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="gradeprocedimentos" runat="server" CssClass="table" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum procedimento carregado !">
                                                                                <Columns>

                                                                                    <asp:BoundField DataField="nrseq" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                                                    <asp:BoundField DataField="nrseq" HeaderText="codigo" />
                                                                                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />

                                                                                    <asp:TemplateField HeaderText="Valor Custo">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="valorcusto" runat="server" ClientIDMode="Static" CssClass="form-control moedasemnegativo" Text='<%# Bind("valorcusto") %>'></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="% Paciente">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="valorpaciente" runat="server" ClientIDMode="Static" CssClass="form-control porcentagem" Text='<%# Bind("valorpaciente") %>'></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Valor Final">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="valorfinal" runat="server" ClientIDMode="Static" CssClass="form-control moedasemnegativo" Text='<%# Bind("valorfinal") %>'></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>



                                                                                    <asp:BoundField DataField="dtcad" HeaderText="Data de cadastro" />
                                                                                    <asp:BoundField DataField="valorcusto" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                                                    <asp:BoundField DataField="valorpaciente" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                                                    <asp:BoundField DataField="valorfinal" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                                                    <asp:TemplateField HeaderText="Ações" runat="server">
                                                                                        <ItemTemplate runat="server">
                                                                                            <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                Alterar
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="deletar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                Deletar
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton ID="imprimir" runat="server" CssClass="btn btn-default btn-xs" CommandName="imprimir" CommandArgument='<%#Container.DataItemIndex%>'>
                                                Imprimir
                                                                                            </asp:LinkButton>
                                                                                            <asp:HiddenField runat="server" ID="hdnnrseq" Value='<%#Bind("nrseq") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                </Columns>
                                                                            </asp:GridView>
                                                                            <div class="row">
                                                                                <div class="col-lg-4"></div>
                                                                                <div class="col-lg-2">
                                                                                    <asp:Button runat="server" CssClass="btn btn-success" ID="btnsalvargdv" Text="Salvar Alterações" />
                                                                                </div>
                                                                                <div class="col-lg-2">
                                                                                    <asp:Button runat="server" CssClass="btn btn-warning" ID="btnimprimirtodosproc" Text="Imprimir" />
                                                                                </div>
                                                                                <div class="col-lg-4"></div>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="box box-primary">
                                                                    <div class="box-header "><b>Reajustar Procedimentos</b></div>
                                                                    <div class="box-body">
                                                                        <div class="row">

                                                                            <div class="col-lg-5">
                                                                                <label for="txtvalorcusto">Valor de Custo<span style="color: red">*</span></label>
                                                                                <asp:TextBox runat="server" type="text" class="form-control porcentagem" ID="txtvalorcustoreajuste" ClientIDMode="Static" />
                                                                            </div>
                                                                            <div class="col-lg-5">
                                                                                <label for="txtvalorcusto">Valor de Final<span style="color: red">*</span></label>
                                                                                <asp:TextBox runat="server" type="text" class="form-control porcentagem" ID="txtvalorfinalreajuste" ClientIDMode="Static" />
                                                                            </div>
                                                                            <div class="col-lg-2">
                                                                                <br />
                                                                                <asp:LinkButton ID="btnReajustar" runat="server" cleintidmode="static" CssClass="btn btn-primary">Reajustar</asp:LinkButton>
                                                                            </div>


                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="box box-primary">
                                                                    <div class="box-header "><b>Carregar Planilha</b></div>
                                                                    <div class="box-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 ">

                                                                                <asp:UpdatePanel ID="updategradex" runat="server">
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btncarregarxls" />
                                                                                    </Triggers>
                                                                                    <ContentTemplate>

                                                                                        <div class="row">
                                                                                            <div class="col-lg-6">
                                                                                                <label for="cboplano">Plano</label>
                                                                                                <asp:DropDownList runat="server" ID="cboplano" CssClass="form-control" ClientIDMode="Static" AutoPostBack="true"></asp:DropDownList>

                                                                                            </div>
                                                                                            <div class="col-lg-6">
                                                                                                <label for="cboconvenio">Convênio</label>
                                                                                                <asp:DropDownList runat="server" ID="cboconvenio" CssClass="form-control" ClientIDMode="Static" OnSelectedIndexChanged="procuraconvenio" AutoPostBack="true"></asp:DropDownList>
                                                                                                <asp:HiddenField ID="hdconvenio" runat="server" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6">
                                                                                                <br />
                                                                                                <asp:FileUpload ID="FileUpload1" runat="server" />

                                                                                            </div>
                                                                                            <div class="col-lg-6 text-center">

                                                                                                <br />
                                                                                                <asp:Button ID="btncarregarxls" runat="server" CssClass="btn btn-primary " Text="Carregar Planilha" />

                                                                                            </div>
                                                                                        </div>
                                                                                        <br />
                                                                                        <div class="row">
                                                                                            <div class="col-lg-12 ">

                                                                                                <asp:GridView ID="gradearquivos" CssClass="table table-striped" runat="server" AutoGenerateColumns="false" GridLines="none" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum arquivo carregado na campanha" Width="100%">
                                                                                                    <Columns>
                                                                                                        <asp:TemplateField HeaderText="Sel">

                                                                                                            <ItemTemplate>
                                                                                                                <asp:CheckBox ID="sel" runat="server" />

                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>

                                                                                                        <asp:BoundField DataField="arquivo" HeaderText="Arquivo" />
                                                                                                        <asp:BoundField DataField="dtcad" HeaderText="Cadastrado em" />
                                                                                                        <asp:BoundField DataField="usercad" HeaderText="Cadastrado por" />
                                                                                                        <asp:BoundField DataField="processado" HeaderText="Processado" />
                                                                                                        <asp:BoundField DataField="dtprocessado" HeaderText="Processado em" />
                                                                                                        <asp:BoundField DataField="userprocessado" HeaderText="Processado por" />
                                                                                                        <asp:TemplateField HeaderText="Linhas">
                                                                                                            <ItemTemplate>
                                                                                                                <asp:HiddenField ID="lblnrseq" runat="server" Value='<%# Bind("nrseq") %>' />
                                                                                                                <span class="badge" style="background: #1E90FF">
                                                                                                                    <asp:Label ForeColor="white" Font-Size="large" ID="lbllinhas" runat="server" Text='<%# Bind("linhas") %>'></asp:Label>
                                                                                                                </span>/
                                                  <span class="badge" style="background: #1E90FF">
                                                      <asp:Label ForeColor="white" Font-Size="large" ID="lbllinhaatual" runat="server" Text='<%# Bind("linhaatual") %>'></asp:Label>
                                                  </span>

                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                        <asp:ButtonField ButtonType="Image" runat="server" HeaderText="Excluir" CommandName="excluir" ImageUrl="~\img\excluir.png">
                                                                                                            <ControlStyle Width="27px" Height="27px"></ControlStyle>

                                                                                                        </asp:ButtonField>
                                                                                                        <asp:ButtonField ButtonType="Image" runat="server" HeaderText="Não processar" CommandName="naoprocessar" ImageUrl="~/img/icon-voltar.png">
                                                                                                            <ControlStyle Width="27px" Height="27px"></ControlStyle>
                                                                                                        </asp:ButtonField>
                                                                                                    </Columns>
                                                                                                    <%-- <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
           <HeaderStyle BackColor="#A02150" Font-Bold="True" Font-Names="Calibri" Font-Size="11pt" ForeColor="White" HorizontalAlign="Left"></HeaderStyle>--%>
                                                                                                    <%--<PagerStyle BackColor="#FFFFCC" BorderStyle="Solid" BorderWidth="1px" ForeColor="#330099" HorizontalAlign="Center" />--%>

                                                                                                    <%--  <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />--%>
                                                                                                </asp:GridView>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 text-center">
                                                                                                <br />
                                                                                                <asp:Button ID="btnprocessararq" runat="server" CssClass="btn btn-primary " Text="Processar Arquivo" />
                                                                                            </div>
                                                                                            <div class="col-lg-6 text-center">
                                                                                                <br />
                                                                                                <asp:Button ID="btnatualizar" runat="server" CssClass="btn btn-primary " Text="Atualizar" />
                                                                                            </div>
                                                                                        </div>
                                                                                        </div>

            </div>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <br />

                                                <div class="row" id="rowErro2" runat="server" style="display: none">
                                                    <br />
                                                    <div class="col-lg-3">
                                                    </div>
                                                    <div class="alert alert-danger col-lg-6" role="alert">
                                                        <p>
                                                            <center>
                                        <strong>Aconteceu algo de errado<br /></strong> 
                                        <strong id="lblErro2" runat="server"></strong><br /> 
                                    </center>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                        </div>
                   
                                        </div>
                                <div class="row" id="rowError" runat="server" style="display: none">
                                    <br />
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="alert alert-danger col-lg-6" role="alert">
                                        <p>
                                            <center>
                                            <strong>Aconteceu algo de errado<br /></strong> 
                                            Os campos: <strong id="lblErro" runat="server"></strong><br /> são de preenchimento obrigatório!
                                        </center>
                                        </p>
                                    </div>
                                </div>
                                        </div>
                    </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>



                            </div>

                        </div>
                    </div>
                </div>





            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.maskMoney.min.js"></script>

    <script type="text/javascript" src="../assets/js/moeda.js"></script>
    <script type="text/javascript" src="../assets/js/telefone.js"></script>
    <script type="text/javascript" src="../assets/js/cpfCnpj.js"></script>
</asp:Content>

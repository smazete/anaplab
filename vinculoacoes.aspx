﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="vinculoacoes.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="vinculoacoes" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Ações Judiciais </b>
        </div>
        <div class="box-body" id="divparticipantes" runat="server">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
<div class="col-md-12">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user" >
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-black" style="background: url('../img/direito.jpg') center center;">
                <asp:HiddenField runat="server" ID="hdnrseqacoesvinculos"   />
                <asp:LinkButton Text="Trocar"  runat="server" CssClass="close" ></asp:LinkButton>
              <h3 class="widget-user-username">
                  <asp:Label ID="txtnomeacao2" Text="Nome Ação" runat="server" /></h3>
              <h5 class="widget-user-desc">N°
                  <asp:Label ID="txtnumeracao2"  Text="123456" runat="server" /></h5>
            </div>
            <div class="widget-user-image">
              <img style="border-top-style:hidden; border-left-style:hidden; border-right-style:hidden; height:100px; width:12
0px;  " src="../img/tuf.png" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><asp:Label ID="txtparticipantes" Text="0" runat="server" /></h5>
                    <span class="description-text">Participantes</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><asp:Label ID="txtcancelados" Text="0" runat="server" /></h5>
                    <span class="description-text">Cancelados</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header"><asp:Label ID="txtstatus" Text="ATIVA" runat="server" /></h5>
                    <span class="description-text">Status</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
              
                <div class="col-lg-6">
                             <div class="col-lg-4">
              <asp:Label Text="Buscar Por" runat="server" /> 
              <asp:DropDownList runat="server"  CssClass="form-control" ID="cbobuscaassociado" >                  
                  <asp:ListItem Text="Codigo" Value="nrseq" />
                  <asp:ListItem Text="Nome" Value="nome" /> 
                  <asp:ListItem Text="Matricula"  Value="matricula"/>
                  <asp:ListItem Text="CPF" Value="cpf" />
              </asp:DropDownList>
                                </div>              
                            <div class="col-lg-6">
                                <br />
              <asp:TextBox runat="server" id="txtbuscaassociado" CssClass="form-control" />  
                                </div>
               
                <div class="col-lg-2 text-center">
                     <br>
                    <asp:LinkButton ID="btnbuscacliente" runat="server" CssClass="btn btn-primary "> <i class="fa fa-searc"></i> Buscar</asp:LinkButton>                
                </div>
                    
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                    <asp:GridView ID="gradeassociados" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HiddenField ID="hdmatri" runat="server" Value='<%#Bind("matricula")%>'></asp:HiddenField>
                            <asp:LinkButton ID="selecionar" runat="server" CssClass="btn btn-success btn-xs" commandName="selecionar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-plus-square"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                    <asp:BoundField DataField="nome" HeaderText="Nome" />
                    <asp:BoundField DataField="cpf" HeaderText="CPF" />
                </Columns>
            </asp:GridView>
                            </ContentTemplate>
                    </asp:UpdatePanel>
                </div>                
              <div class="col-lg-6">
                  <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                    <asp:GridView ID="gradeacoesvinculos" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                    <asp:BoundField DataField="nome" HeaderText="Nome" />
                    <asp:BoundField DataField="cpf" HeaderText="CPF" />         
                    <asp:BoundField DataField="ativo" HeaderText="Ativo" />         
                    <asp:TemplateField HeaderText="Retirar">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                            <asp:HiddenField ID="hdmatricula" runat="server" Value='<%#Bind("matricula")%>'></asp:HiddenField>
                            <div runat="server" id="divretirar">
                            <asp:LinkButton ID="retirar" runat="server" CssClass="btn btn-danger btn-xs" CommandName="retirar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-close"></i></asp:LinkButton>
                                </div>
                               <div runat="server" id="divativar">
                            <asp:LinkButton ID="ativar" runat="server" CssClass="btn btn-success btn-xs" CommandName="ativar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-check-square-o"></i></asp:LinkButton>
                                </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                            </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
          </div>
          <!-- /.widget-user -->
        </div>
                    
                </ContentTemplate>
            </asp:UpdatePanel>
            
        </div>

        <div class="box-body" id="divpadrão" runat="server">
            <div class="row">
                <div class="col-lg-2">
                    <asp:Label Text="Nrseq" runat="server" />
                    <br>
                    <asp:TextBox ID="txtnrseq" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                </div>
                <div class="col-lg-4">
                    <asp:Label Text="Nome" runat="server" />
                    <br>
                    <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-3">
                    <asp:Label Text="Numero" runat="server" />
                    <br>
                    <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    <asp:Label Text="Data" runat="server" />
                    <br>
                    <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdata" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-1">
                    <asp:Label Text="Ativo" runat="server" />
                    <br>
                    <asp:CheckBox ID="chkativo" runat="server"></asp:CheckBox>
                </div>

                <div class="col-lg-3">
                    <asp:Label Text="Justica" runat="server" />
                    <br>  
                    <asp:TextBox ID="txtjustica" runat="server" CssClass="form-control" Enabled="true"></asp:TextBox>
             
                    <asp:DropDownList ID="cbojustica" runat="server" CssClass="form-control" Visible="false">
                        <asp:ListItem ></asp:ListItem>
                        <asp:ListItem Value="JUSTIÇA FEDERAL">JUSTIÇA FEDERAL</asp:ListItem>
                        <asp:ListItem Value="TJ AC">TJ AC</asp:ListItem>
                        <asp:ListItem Value="TJ AL">TJ AL</asp:ListItem>
                        <asp:ListItem Value="TJ AP">TJ AP</asp:ListItem>
                        <asp:ListItem Value="TJ AM">TJ AM</asp:ListItem>
                        <asp:ListItem Value="TJ BA">TJ BA</asp:ListItem>
                        <asp:ListItem Value="TJ CE">TJ CE</asp:ListItem>
                        <asp:ListItem Value="TJ DF">TJ DF</asp:ListItem>
                        <asp:ListItem Value="TJ ES">TJ ES</asp:ListItem>
                        <asp:ListItem Value="TJ EX">TJ EX</asp:ListItem>
                        <asp:ListItem Value="TJ GO">TJ GO</asp:ListItem>
                        <asp:ListItem Value="TJ MA">TJ MA</asp:ListItem>
                        <asp:ListItem Value="TJ MT">TJ MT</asp:ListItem>
                        <asp:ListItem Value="TJ MS">TJ MS</asp:ListItem>
                        <asp:ListItem Value="TJ MG">TJ MG</asp:ListItem>
                        <asp:ListItem Value="TJ PA">TJ PA</asp:ListItem>
                        <asp:ListItem Value="TJ PB">TJ PB</asp:ListItem>
                        <asp:ListItem Value="TJ PR">TJ PR</asp:ListItem>
                        <asp:ListItem Value="TJ PE">TJ PE</asp:ListItem>
                        <asp:ListItem Value="TJ PI">TJ PI</asp:ListItem>
                        <asp:ListItem Value="TJ RJ">TJ RJ</asp:ListItem>
                        <asp:ListItem Value="TJ RN">TJ RN</asp:ListItem>
                        <asp:ListItem Value="TJ RS">TJ RS</asp:ListItem>
                        <asp:ListItem Value="TJ RO">TJ RO</asp:ListItem>
                        <asp:ListItem Value="TJ RR">TJ RR</asp:ListItem>
                        <asp:ListItem Value="TJ SC">TJ SC</asp:ListItem>
                        <asp:ListItem Value="TJ SP">TJ SP</asp:ListItem>
                        <asp:ListItem Value="TJ SE">TJ SE</asp:ListItem>
                        <asp:ListItem Value="TJ TO">TJ TO</asp:ListItem>
                        <asp:ListItem Value="TRT RIO">TRT RIO</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-lg-7">
                    <asp:Label Text="Link" runat="server" />
                    <br>
                    <asp:TextBox ID="txtlink" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <br>
                    <asp:LinkButton ID="btnsalvar" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i>Salvar</asp:LinkButton>
                </div>
            </div>
            <div class="row">
                </hr>
            </div>
            <div class="col-lg-4">
                <asp:Label Text="Buscar Por" runat="server" />
                <asp:DropDownList runat="server" CssClass="form-control" ID="cbobusca">
                    <asp:ListItem Text="Codigo" Value="nrseq" />
                    <asp:ListItem Text="Nome" Value="nome" />
                    <asp:ListItem Text="Numero" Value="numero" />
                </asp:DropDownList>
            </div>
            <div class="col-lg-6">
                <br />
                <asp:TextBox runat="server" ID="txtbusca" CssClass="form-control" />
            </div>

            <div class="col-lg-2 text-center">
                <br>
                <asp:LinkButton ID="btnbuscando" runat="server" CssClass="btn btn-primary "> <i class="fa fa-searc"></i> Buscar </asp:LinkButton>
            </div>

            <div class="row">
                <br>

                <asp:GridView ID="gradeacoes" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma doca cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <%--<asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>--%>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                           
                                   <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
      
</ItemTemplate>
</asp:TemplateField>
                        
                                <asp:BoundField DataField="nrseq" HeaderText="Código" />
                        <asp:TemplateField HeaderText="Link" >
                            <ItemTemplate>
                                 <asp:HyperLink ID="HlFile" runat="server" 
         NavigateUrl='<%# Eval("link") %>'  Target="_blank"><i class="fa fa-chrome"></i>
    </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>  <asp:TemplateField HeaderText="Usar" >
                            <ItemTemplate>
                                   <asp:LinkButton ID="escolha" runat="server" CssClass="btn btn-success  btn-xs" CommandName="escolha" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-balance-scale"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                                 

                                <asp:BoundField DataField="nome" HeaderText="Nome" />
                                <asp:BoundField DataField="numero" HeaderText="Numero" />
                                <asp:BoundField DataField="Justica" HeaderText="Justica" />
                                <asp:BoundField DataField="ativo" HeaderText="Status" />
                        
                        
                    </Columns>
                </asp:GridView>
            </div>
            </div>
          
        </div>
                  </ContentTemplate>
        </asp:UpdatePanel>

            </asp:Content>
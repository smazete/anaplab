﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="relcontasapagar.aspx.vb" Inherits="restrito_relcontasapagar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script lang="javascript" type="text/javascript">
        function imrpimircontas() {
            window.open('imprimircontas.aspx', '_blank')
        }
    </script>
    <div class="box box-primary">

        <div class="box-header">
            Relatório Contas a Pagar e Receber
        </div>
        <div class="box-body">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div id="alerta1" style="display: none;">


                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Atenção:</strong>
                            <asp:Label ID="lblerroconta1" runat="server" Text="Por favor, selecione uma carteira de clientes válida !"></asp:Label>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Label runat="server" Text="Data Inicial"></asp:Label>
                                    <asp:TextBox ID="txtdtinicial" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" runat="server" class="form-control" AutoPostBack="true"></asp:TextBox>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label runat="server" Text="Data Final"></asp:Label>
                                    <asp:TextBox ID="txtdtfinal" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" runat="server" class="form-control" AutoPostBack="true"></asp:TextBox>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <asp:RadioButton ID="optdata_vencimento" CssClass="radio-inline" Text="Data de Vencimento" runat="server" GroupName="filtro" Checked="True" />
                                            <br />
                                            <asp:RadioButton ID="optdata_baixa" CssClass="radio-inline" Text="Data de Baixa" runat="server" GroupName="filtro" />
                                        </div>
                                        <div class="col-lg-6">
                                            <asp:RadioButton ID="optdata_cadastro" CssClass="radio-inline" Text="Data de Cadastro" runat="server" GroupName="filtro" />
                                            <br />
                                            <asp:RadioButton ID="optdata_execucao" CssClass="radio-inline" Text="Data de Execução" runat="server" GroupName="filtro" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-8">
                                    <asp:Label runat="server" Text="Forma Pagto"></asp:Label>
                                    <asp:DropDownList ID="cboformapagto" runat="server" class="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-lg-4">
                                    <asp:CheckBox ID="chkExibirExcluidos" CssClass="checkbox-inline fix-check" Text="Exibir Excluídos" runat="server" />
                                </div>
                                <div class="col-lg-12">
                                    <div class="col-lg-4">
                                        <asp:RadioButton ID="opttipo_todas" CssClass="radio-inline" Text="Todas" runat="server" GroupName="tipo" Checked="True" />
                                        <br />
                                        <asp:RadioButton ID="opttipo_baixadas" CssClass="radio-inline" Text="Baixadas" runat="server" GroupName="tipo" />
                                        <br />
                                        <asp:RadioButton ID="opttipo_abertas" CssClass="radio-inline" Text="Em aberto" runat="server" GroupName="tipo" />
                                    </div>
                                    <div class="col-lg-4">
                                        <asp:RadioButton ID="optconci_todos" CssClass="radio-inline" Text="Todas" runat="server" GroupName="conci" Checked="True" />
                                        <br />
                                        <asp:RadioButton ID="optconci_sim" CssClass="radio-inline" Text="Conciliadas" runat="server" GroupName="conci" />
                                        <br />
                                        <asp:RadioButton ID="optconci_nao" CssClass="radio-inline" Text="Não conciliadas" runat="server" GroupName="conci" />
                                    </div>
                                    <div class="col-lg-4">
                                        <asp:RadioButton ID="optaprov_todas" CssClass="radio-inline" Text="Todas" runat="server" GroupName="aprov" Checked="True" />
                                        <br />
                                        <asp:RadioButton ID="optaprov_emaprova" CssClass="radio-inline" Text="Em aprovação" runat="server" GroupName="aprov" />
                                        <br />
                                        <asp:RadioButton ID="optaprov_aprovadas" CssClass="radio-inline" Text="Aprovadas" runat="server" GroupName="aprov" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Lançamento"></asp:Label>
                            <asp:DropDownList ID="cbooperacao" runat="server" class="form-control">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Crédito</asp:ListItem>
                                <asp:ListItem>Débito</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-5">
                            <asp:Label runat="server" Text="Plano de contas"></asp:Label>
                            <asp:DropDownList ID="cboplano" runat="server" class="form-control" AutoPostBack="true"></asp:DropDownList>
                        </div>
                        <div class="col-lg-5">
                            <asp:Label runat="server" Text="Descrição"></asp:Label>
                            <asp:TextBox ID="txtdescricao" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <%-- <div class="col-lg-2">
                            <div class="form-group">
                              
                                <br />

                            </div>
                        </div>--%>
                    </div>
                    <br />
                    <div class="row text-align-center">
                        <div class="col-lg-12">
                            <asp:Button ID="btnProcurar" runat="server" Text="Imprimir" CssClass="btn btn-primary" Enabled="true" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>


﻿Imports System.IO
Imports AjaxControlToolkit
Imports clsSmart
Partial Class envioporemail
    Inherits System.Web.UI.Page
    Dim tbemails As New Data.DataTable
    Dim tabemails As New clsBanco
    Dim html As String
    Private Sub envioporemail_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub

        btncarregar.Visible = False
        Dim wcnrseqctrl As String = gerarnrseqcontrole()
        tbemails = tabemails.IncluirAlterarDados("insert into tbemailsenviados (dtcad, usercad, enviado, nrseqctrl) values ('" & formatadatamysql(data) & "', '" & Session("usuario") & "', false, '" & wcnrseqctrl & "')")
        tbemails = tabemails.conectar("select * from tbemailsenviados where nrseqctrl = '" & wcnrseqctrl & "'")


        Session("nrseqemail") = tbemails.Rows(0)("nrseq").ToString

        If Session("anexos") <> "" Then
            Dim _listaemails As String() = separaremails(Session("anexos"))

            For y As Integer = 0 To _listaemails.Length - 1
                If Not _listaemails(y) Is Nothing Then
                    tbemails = tabemails.IncluirAlterarDados("insert into tbemailsenviados_arqs (nrseqemail, ativo, arquivo, dtcad, usercad) values (" & Session("nrseqemail") & ",true,'" & mARQUIVO(_listaemails(y)) & "','" & formatadatamysql(data) & "','" & Session("usuario") & "')")
                End If

            Next
        End If


        txtemail.Text = Session("emailpara")
        txtcco.Text = "ceva@smartcodesolucoes.com.br"
        txtassunto.Text = Session("assunto")
        ' Session("minhapagina") = "mensagens\rel150520181505.html"
        If File.Exists(Server.MapPath("~\" & Session("minhapagina"))) Then
            ' Session("arquivo") = tbcontratos.Rows(0)("arquivo").ToString

            Dim lermensagem As New StreamReader(Server.MapPath("~\" & Session("minhapagina")))
            html = lermensagem.ReadToEnd
            lermensagem.Close()

            Dim mensagem As HtmlControl
            mensagem = Me.FindControl("framemensagem")
            mensagem.Attributes("src") = Session("minhapagina")

            ' txtcorpo.Text = (html)
        End If

        Dim tbbol As New Data.DataTable
        Dim tabbol As New clsBanco
        tbbol = tabbol.conectar("select * from tbemailsenviados_arqs where ativo = true and nrseqemail = " & Session("nrseqemail") & " order by nrseq desc")
        Grade.DataSource = tbbol
        Grade.DataBind()




    End Sub

    Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "escondeexibiralerta1()", "escondeexibiralerta1()", True)
        Dim novoemail As New clsEnvioEmail
        novoemail.EhHTML = True

        novoemail.configpadrao()

        'Dim lermensagem As New StreamReader(_CaminhoArquivo & "\mensagens\smart.html")
        'Dim html As String = lermensagem.ReadToEnd
        'lermensagem.Close()
        'Dim FilePath As String = Server.MapPath("~\modcontratos\")

        'If Session("arquivo") = "" Then
        '    Session("arquivo") = "email_" & Directory.GetFiles(FilePath).Length + 1 & ".html".Replace(" ", "")
        'End If
        'If File.Exists(Server.MapPath("~\mensagens\claudio.html")) Then
        '    File.Delete(Server.MapPath("~\mensagens\claudio.html"))
        'End If
        'Dim lermensagem2 As New StreamWriter(Server.MapPath("~\mensagens\claudio.html"))
        'lermensagem2.WriteLine(txtcorpo.Text)
        'lermensagem2.Close()

        ' Dim ler As New StreamReader(Server.MapPath("~\mensagens\claudio.html"))

        ' Dim lermensagem As New StreamReader(Server.MapPath("~\" & Session("minhapagina")))
        If File.Exists(Server.MapPath("~\" & Session("minhapagina"))) Then
            ' Session("arquivo") = tbcontratos.Rows(0)("arquivo").ToString

            Dim lermensagem As New StreamReader(Server.MapPath("~\" & Session("minhapagina")))
            html = lermensagem.ReadToEnd
            lermensagem.Close()



            ' txtcorpo.Text = (html)
        End If

        novoemail.AdicionaMensagem = html 'txtcorpo.Text
        ' lermensagem.Close()
        '  novoemail.AdicionaMensagem = ler.ReadToEnd

        ' ler.Close()


        'novoemail.AdicionaRemetente = "ceva@smartcodesolucoes.com.br"

        Dim _listaemails As String()

        '  novoemail.AdicionaDestinatariosocultos = "karlahey_direito@yahoo.com.br"
        If txtcco.Text <> "" Then
            _listaemails = separaremails(txtcco.Text)

            For y As Integer = 0 To _listaemails.Length - 1
                If Not _listaemails(y) Is Nothing AndAlso _listaemails(y).Contains("@") Then
                    novoemail.AdicionaDestinatariosocultos = _listaemails(y)
                End If

            Next
        End If
        If txtemail.Text <> "" Then

            _listaemails = separaremails(txtemail.Text)

            For y As Integer = 0 To _listaemails.Length - 1
                If Not _listaemails(y) Is Nothing AndAlso _listaemails(y).Contains("@") Then
                    novoemail.AdicionaDestinatarios = _listaemails(y)
                End If
            Next


        End If
        '   novoemail.AdicionaDestinatarios = email
        ' novoemail.Credenciais("ceva@smartcodesolucoes.com.br", "Sousmart@747791", "smtp.smartcodesolucoes.com.br")
        novoemail.AdicionaAssunto = txtassunto.Text
        novoemail.ConfigPorta = 587
        'novoemail.usarSSL = False
        '  novoemail.ConfigSMTP = "smtp.smartcodesolucoes.com.br"
        'If arquivoanexo <> "" Then


        novoemail.AdicionaAssunto = txtassunto.Text
        If Session("anexos") <> "" Then
            _listaemails = separaremails(Session("anexos"))

            For y As Integer = 0 To _listaemails.Length - 1
                If Not _listaemails(y) Is Nothing AndAlso File.Exists(_listaemails(y)) Then
                    novoemail.AdicionaAnexos = _listaemails(y)
                End If
            Next
        End If


        Dim result As Boolean = novoemail.EnviarEmail()
        If result Then
            tbemails = tabemails.IncluirAlterarDados("update tbemailsenviados set enviado = true, codigo = '" & Session("codigoenvio") & "' where nrseq = " & Session("nrseqemail"))
            lblerro.Text = "E-mail enviado com sucesso! Feche a janela para prosseguir !"
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibiralerta1()", "exibiralerta1()", True)
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "fecharmodaldocsparent1()", "fecharmodaldocsparent1()", True)
            ' ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "mefechar()", "mefechar()", True)
        Else
            tbemails = tabemails.IncluirAlterarDados("update tbemailsenviados set erro = true, codigo = '" & Session("codigoenvio") & "' where nrseq = " & Session("nrseqemail"))
            lblerro.Text = "Falha no envio do e-mail !"
            '  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "AlertBox", "BootstrapDialog.alert('Falha no envio do email');", True)
            '  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibiralerta1()", "exibiralerta1()", True)
        End If

        Response.Redirect("loteaprovado.aspx")
        'Response.Redirect("default.aspx")

    End Sub

    Private Sub btnAbrirmodelo_Click(sender As Object, e As EventArgs) Handles btnAbrirmodelo.Click

        'If cbocontratos.Text = "" Then
        '    Exit Sub
        'End If
        'If Session("codaprov") Is Nothing Then
        '    Session("codaprov") = 0
        'End If
        'Dim tbcontratos As New Data.DataTable
        'Dim tabcontratos As New clsBanco
        'Dim FilePath As String = Server.MapPath("~\modcontratos\")
        'tbcontratos = tabcontratos.conectar("select * from tbmodeloscontratos where ativo = true and descricao = '" & cbocontratos.Text & "'")
        'If tbcontratos.Rows.Count <> 0 Then
        '    If File.Exists(FilePath & tbcontratos.Rows(0)("arquivo").ToString) Then
        '        Session("arquivo") = tbcontratos.Rows(0)("arquivo").ToString
        '        Dim html As String
        '        Dim lermensagem As New StreamReader(Server.MapPath("~\modcontratos\" & Session("arquivo")))
        '        html = lermensagem.ReadToEnd
        '        lermensagem.Close()

        '        txtcorpo.Text = Server.HtmlDecode(trocadados(html, IIf(Session("boleto") = "", 0, Session("boleto")), IIf(Session("nrconta") = "", 0, Session("nrconta")), Session("codaprov")))
        '    End If
        'End If
    End Sub

    Private Sub btnanexar_Click(sender As Object, e As EventArgs) Handles btnanexar.Click
        Dim tbarq As New Data.DataTable
        Dim tabarq As New clsBanco
        Dim uploadedFile As HttpPostedFile = FileUpload1.PostedFile
        Dim FileName As String = "", nomecompleto As String = ""
        Dim FolderPath As String = ConfigurationManager.AppSettings("FolderPath")

        If FileUpload1.FileName = "" Then

            Exit Sub
        End If
        FileName = FileUpload1.FileName
        Dim FilePath As String = Server.MapPath("~\arquivoslotes\boletos\" & FileName)



        FilePath = Server.MapPath("~\arquivoslotes\boletos")
        FileName = encontrachr(uploadedFile.FileName.Replace(" ", ""), ".", 1) & "_" & Directory.GetFiles(FilePath).Length + 1 & "." & mRight(uploadedFile.FileName, 3)
        nomecompleto = FilePath & FileName
        FilePath = Server.MapPath("~\arquivoslotes\boletos\" & FileName)

        FileUpload1.SaveAs(FilePath)
        Dim wcnrseqctrl As String = gerarnrseqcontrole()
        tbarq = tabarq.IncluirAlterarDados("insert into tbemailsenviados_arqs (arquivo, dtcad, usercad, ativo, nrseqemail) values ('" & mARQUIVO(FileName) & "', '" & formatadatamysql(data) & "', '" & Session("usuario") & "', true," & Session("nrseqemail") & ")")
        tbarq = tabarq.conectar("select * from tbemailsenviados_arqs where nrseqemail = " & Session("nrseqemail"))
        Grade.DataSource = tbarq
        Grade.DataBind()
    End Sub

    Private Sub btncarregar_Click(sender As Object, e As EventArgs) Handles btncarregar.Click
        btncarregar.Visible = False
        Dim wcnrseqctrl As String = gerarnrseqcontrole()
        tbemails = tabemails.IncluirAlterarDados("insert into tbemailsenviados (dtcad, usercad, enviado, nrseqctrl) values ('" & formatadatamysql(data) & "', '" & Session("usuario") & "', false, '" & wcnrseqctrl & "')")
        tbemails = tabemails.conectar("select * from tbemailsenviados where nrseqctrl = '" & wcnrseqctrl & "'")


        Session("nrseqemail") = tbemails.Rows(0)("nrseq").ToString

        If Session("anexos") <> "" Then
            Dim _listaemails As String() = separaremails(Session("anexos"))

            For y As Integer = 0 To _listaemails.Length - 1
                tbemails = tabemails.IncluirAlterarDados("insert into tbemailsenviados_arqs (nrseqemail, ativo, arquivo, dtcad, usercad) values (" & Session("nrseqemail") & ",true,'" & _listaemails(y) & "','" & formatadatamysql(data) & "','" & Session("usuario") & "')")

            Next
        End If


        txtemail.Text = Session("emailpara")
        txtcco.Text = "ceva@smartcodesolucoes.com.br"
        txtassunto.Text = Session("assunto")
        Session("minhapagina") = "mensagens\rel150520181505.html"
        If File.Exists(Server.MapPath("~\" & Session("minhapagina"))) Then
            ' Session("arquivo") = tbcontratos.Rows(0)("arquivo").ToString
            Dim html As String
            Dim lermensagem As New StreamReader(Server.MapPath("~\" & Session("minhapagina")))
            html = lermensagem.ReadToEnd
            lermensagem.Close()

            '  txtcorpo.Text = (html)
        End If

        Dim tbbol As New Data.DataTable
        Dim tabbol As New clsBanco
        tbbol = tabbol.conectar("select * from tbemailsenviados_arqs where ativo = true and nrseqemail = " & Session("nrseqemail") & " order by nrseq desc")
        Grade.DataSource = tbbol
        Grade.DataBind()
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibiremail1()", "exibiremail1()", True)
    End Sub

    Private Sub Grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles Grade.RowCommand
        Dim existe As Boolean = False


        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim txtlinha As Integer

        Dim row As GridViewRow = Grade.Rows(index)
        txtlinha = index


        Dim xyz As Integer = 0
        Dim linha As GridViewRow = Grade.Rows(txtlinha)


        Dim hdnrseq As HiddenField = CType(row.FindControl("hdnrseq"), HiddenField)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        Select Case e.CommandName.ToString.ToLower
            Case Is = "excluir"
                tb1 = tab1.IncluirAlterarDados("update tbemailsenviados_arqs set ativo = false where nrseq = " & hdnrseq.Value)

                tb1 = tab1.conectar("select * from tbemailsenviados_arqs where ativo = true and nrseqemail = " & Session("nrseqemail") & " order by nrseq desc")
                Grade.DataSource = tb1
                Grade.DataBind()
        End Select
    End Sub



    'Private Sub TextBox1_HtmlEditorExtender_ImageUploadComplete(sender As Object, e As AjaxFileUploadEventArgs) Handles TextBox1_HtmlEditorExtender.ImageUploadComplete
    '    Dim fullpath As String
    '    fullpath = Server.MapPath("~/boletos/") + e.FileName

    '    Dim ajaxFileUpload = sender
    '    TextBox1_HtmlEditorExtender.AjaxFileUpload.SaveAs(fullpath)
    '    '   TextBox1_HtmlEditorExtender.AjaxFileUpload.SaveAs(fullpath)

    '    '   tbnotes.Text += String.Format("<img src =" & fullpath & " Width=200px Heigh=200px>")
    '    '   tbnotes.Text = e.PostedUrl("<img src =" & fullpath & " Width=200px Heigh=200px>")
    '    e.PostedUrl = Page.ResolveUrl("<img src =" & fullpath & " Width=200px Heigh=200px>" + e.FileName)
    'End Sub
End Class

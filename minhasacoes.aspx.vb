﻿Imports System.IO
Imports clsSmart

Partial Class autoatendimento_minhasacoes
    Inherits System.Web.UI.Page

    Private Sub minhasacoes_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("idassociado") = "" Then

            Response.Redirect("loginassociados.aspx")
        End If

        If IsPostBack Then
            Exit Sub
        End If
        carregagrade()

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Public Sub carregagrade()

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim consulta As String
        Dim where As String

        tb1 = tab1.conectar("select * from tbacoes_detalhes inner join tbacoes on tbacoes_detalhes.nrseqacoes = tbacoes.nrseq where tbacoes_detalhes.ativo = 1 and tbacoes_detalhes.matricula = '" & Session("idmatricula") & "' and  tbacoes.ativo = '1' order by  tbacoes.nrseq desc")

        grade.DataSource = tb1
        grade.DataBind()



    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If


        '     If e.Row.Cells(4).Text = "1" Then
        '    e.Row.Cells(4).Text = "Encerrado"
        '   e.Row.ForeColor = System.Drawing.Color.Black
        '  e.Row.BackColor = System.Drawing.Color.LightBlue
        '
        ' Else 
        'e.Row.Cells(4).Text = "Andamento"
       ' End If



    End Sub


    Private Sub grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grade.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim link As HiddenField = row.FindControl("hdlink")

        If e.CommandName = "link" Then

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Carregada ',  showConfirmButton: false,  timer: 1200})")

        End If

    End Sub
    '  Public Sub carregaassociados(Optional exibirinativos As Boolean = False)

    '    Dim xAssociados As New clsAssociado

    '    xAssociados.Nrseq = Session("idassociado")

    '    If Not xAssociados.procurar() Then
    '        sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
    '        Exit Sub
    '    End If


    '    '   txtnrseq.Text = xAssociados.Nrseq
    '    txtmatricula.Text = xAssociados.Matricula
    '    txtnome.Text = xAssociados.Nome
    '    txtnomemae.Text = xAssociados.Nomemae
    '    txtsituacaopb.Text = xAssociados.Situacaopb
    '    txtsexo.Text = xAssociados.Sexo
    '    txtdtnasc.Text = formatadatamysql(xAssociados.Dtnasc, True, True)
    '    txtcpf.Text = xAssociados.Cpf
    '    txtrg.Text = xAssociados.Rg
    '    txtcep.Text = xAssociados.Cep
    '    txtendereco.Text = xAssociados.Endereco
    '    txtnumero.Text = xAssociados.Numero
    '    txtcomplemento.Text = xAssociados.Complemento
    '    txtbairro.Text = xAssociados.Bairro
    '    txtcidade.Text = xAssociados.Cidade
    '    txtestado.Text = xAssociados.Estado
    '    txttelddd.Text = xAssociados.Telddd
    '    txttelefone.Text = xAssociados.Telefone
    '    txtemail.Text = xAssociados.Email
    '    txtdtposse.Text = formatadatamysql(xAssociados.Dtposse, True, True)
    '    txtdtaposentadoria.Text = formatadatamysql(xAssociados.Dtaposentadoria, True, True)
    '    txtsenha.Text = xAssociados.Senha
    '    txtAgencia.Text = xAssociados.Agencia
    '    txtcontacorrente.Text = xAssociados.Contacorrente

    'End Sub

    'Private Sub grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grade.RowCommand
    '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '    Dim row As GridViewRow = grade.Rows(index)
    '    Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
    '    If e.CommandName = "visualizar" Then



    '        Dim xCaixapostal As New clscaixapostal


    '        xCaixapostal.Nrseq = Session("idassociado")

    '        If Not xCaixapostal.procurar() Then
    '            sm("swal({title: 'Error!',text: '" & xCaixapostal.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
    '            Exit Sub
    '        End If

    '        txtassunto.Text = xCaixapostal.Assunto
    '        txttexto.Text = xCaixapostal.Texto
    '        txtdtcad.Text = formatadatamysql(xCaixapostal.Dtcad, True, True)
    '        ' dados historico

    '        'txtdtcad.Text = formatadatamysql(xAcoes.Dtcad, True, True)
    '        'txtusercad.Text = xAcoes.Usercad

    '        '   sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Carregada ',  showConfirmButton: false,  timer: 1200})")
    '        '    carregamensalidades()

    '        divgrademenssagem.Visible = True

    '    End If
    'End Sub

End Class

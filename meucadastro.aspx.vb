﻿Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoesautoatendimento
Imports System.IO
Imports System.Data

Partial Class meucadastro
     
    Inherits System.Web.UI.Page


    Private Sub meucadastro_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("idassociado") = "" Then

            Response.Redirect("loginassociados.aspx")
        End If

        If IsPostBack Then
            Exit Sub
        End If
        carregaassociados()

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub


    Public Sub carregaassociados(Optional exibirinativos As Boolean = False)

        Dim xAssociados As New clsAssociado

        xAssociados.Nrseq = Session("idassociado")

        If Not xAssociados.procurar() Then
            sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If


        '   txtnrseq.Text = xAssociados.Nrseq
        txtmatricula.Text = xAssociados.Matricula
        txtnome.Text = xAssociados.Nome
        txtnomemae.Text = xAssociados.Nomemae
        txtsituacaopb.Text = xAssociados.Situacaopb
        txtsexo.Text = xAssociados.Sexo
        txtdtnasc.Text = xAssociados.Dtnasc
        txtcpf.Text = xAssociados.Cpf
        txtrg.Text = xAssociados.Rg
        txtcep.Text = xAssociados.Cep
        txtendereco.Text = xAssociados.Endereco
        txtnumero.Text = xAssociados.Numero
        txtcomplemento.Text = xAssociados.Complemento
        txtbairro.Text = xAssociados.Bairro
        txtcidade.Text = xAssociados.Cidade
        txtestado.Text = xAssociados.Estado
        txtcelular.Text = xAssociados.Celular
        txttelefone.Text = xAssociados.Telefone
        txtemail.Text = xAssociados.Email
        txtdtposse.Text = xAssociados.Dtposse
        txtdtaposentadoria.Text = xAssociados.Dtaposentadoria
        txtsenha.Text = xAssociados.Senha
        txtAgencia.Text = xAssociados.Agencia
        txtcontacorrente.Text = xAssociados.Contacorrente

    End Sub


    Private Sub txtcep_TextChanged(sender As Object, e As EventArgs) Handles txtcep.TextChanged
        Dim xcep As String = sonumeros(txtcep.Text)
        If xcep = "" Then
            txtcep.Text = "CEP NAO INFORMADO"
            txtcep.Focus()
            Exit Sub

        End If

        Try
            Dim ws As New correios.AtendeClienteService

            'Verificar o nome do endpoint no arquivo Web.config 
            Dim dados = ws.consultaCEP(xcep)

            If dados IsNot Nothing Then

                txtendereco.Text = String.Format("{0}" & vbCr & vbLf & dados.[end], dados.complemento, dados.complemento2, dados.bairro, dados.cidade, dados.uf)

                'System.Threading.Thread.Sleep(2000)
                Dim ender As String = dados.end
                Dim cidade As String = dados.cidade
                Dim bairro As String = dados.bairro
                Dim uf As String = dados.uf

                txtcidade.Text = dados.cidade
                txtbairro.Text = dados.bairro
                txtestado.Text = dados.uf
                txtnumero.Focus()
            Else
                txtcep.Text = "CEP não encontrado."
            End If

        Catch ex As Exception
            txtcidade.Text = "Cidade não encontrada."
            txtendereco.Text = "Endereço não encontrado."
        End Try
    End Sub



    Private Sub atualizacadastro()

        Dim campos As String

        If Session("idassociado") = "" Then
            Exit Sub
        End If


        Dim xclasse As New clsAssociado
        xclasse.Nrseq = Session("idassociado")
        xclasse.Matricula = txtmatricula.Text
        xclasse.Nome = txtnome.Text
        xclasse.Nomemae = txtnomemae.Text
        xclasse.Situacaopb = txtsituacaopb.Text
        xclasse.Sexo = txtsexo.Text
        xclasse.Dtnasc = txtdtnasc.Text
        xclasse.Cpf = txtcpf.Text
        xclasse.Rg = txtrg.Text
        xclasse.Cep = txtcep.Text
        xclasse.Endereco = txtendereco.Text
        xclasse.Numero = txtnumero.Text
        xclasse.Complemento = txtcomplemento.Text
        xclasse.Bairro = txtbairro.Text
        xclasse.Cidade = txtcidade.Text
        xclasse.Estado = txtestado.Text
        'xclasse.Telddd = txttelddd.text
        xclasse.Telefone = txttelefone.Text
        xclasse.Email = txtemail.Text
        xclasse.Dtposse = txtdtposse.Text
        xclasse.Dtaposentadoria = txtdtaposentadoria.Text
        xclasse.Senha = txtsenha.Text
        xclasse.Agencia = txtAgencia.Text
        xclasse.Contacorrente = txtcontacorrente.Text
        ' xclasse.Autorizacao = txtautorizacao.text
        ' xclasse.Dtcad = txtdtcad.text
        ' xclasse.Usercad = txtusercad.text
        'xclasse.Ativo_old = txtativo_old.text
        'xclasse.Ativo = txtativo.text
        'xclasse.Matriculactrl = txtmatriculactrl.text
        'xclasse.Codigo = txtcodigo.text
        '  xclasse.Userexcluir = txtuserexcluir.text
        ' xclasse.Dtexcluido = txtdtexcluido.text
        '   xclasse.Dddcelular = txtdddcelular.text
        xclasse.Celular = txtcelular.Text
        '   xclasse.Bloqueado_old = txtbloqueado_old.text
        '   xclasse.Bloqueado = txtbloqueado.text
        '   xclasse.Dtbloqueio = txtdtbloqueio.text
        '   xclasse.Userbloqueio = txtuserbloqueio.text
        '   xclasse.Dtdesbloqueio = txtdtdesbloqueio.text
        '   xclasse.Userdesbloqueio = txtuserdesbloqueio.text
        '    xclasse.Ip = txtip.text
        '    xclasse.Dtsolexc = txtdtsolexc.text
        '    xclasse.Motiexc = txtmotiexc.text
        '   xclasse.Dtinad = txtdtinad.text
        '  xclasse.Filiacao = txtfiliacao.text
        ' xclasse.Vivomorto = txtvivomorto.text
        'xclasse.Negociado = txtnegociado.text
        'xclasse.Arquivo = txtarquivo.text
        ' xclasse.Nrseqctrl = txtnrseqctrl.text


        If Not xclasse.salvardoassociado Then
            sm("Swal.fire({ type: 'success',  title: 'Não Foi possivel salvar a alteração. ',  confirmButtonText: 'OK'})")
            Exit Sub
        End If

        sm("Swal.fire({ type: 'success',  title: 'Ateração efetuada com sucesso. ',  confirmButtonText: 'OK'})")




    End Sub

    Private Sub btnatualizar_Click(sender As Object, e As EventArgs) Handles btnatualizar.Click
        atualizacadastro()
    End Sub
End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="baixamanual.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="baixamanual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script runat="server">

        Sub exportarh(ByVal Source As Object, ByVal E As EventArgs)

            If dgexcel.Items.Count = 0 Then
                Exit Sub
            End If

            Dim wcarquivo As String = "baixamanual" & Date.Now.ToString.Replace("/", "").Replace(":", "").Replace(" ", "").Trim()

            exportarExcel(dgexcel, wcarquivo)
        End Sub

        Sub exportarExcel(grid As DataGrid, saveAsFile As String)

            ' O linite de linhas do Excel é  65536
            If grid.Items.Count + 1 < 65536 Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & saveAsFile & ".xls")
                ' Remover caracteres do header - Content-Type
                HttpContext.Current.Response.Charset = ""
                'HttpContext.Current.Response.WriteFile("style.txt")
                ' desabilita o  view state.
                grid.EnableViewState = False
                Dim tw As New System.IO.StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grid.RenderControl(hw)
                ' Escrever o html no navegador
                HttpContext.Current.Response.Write(tw.ToString())
                ' termina o response
                HttpContext.Current.Response.End()

            Else
                HttpContext.Current.Response.Write("Muitas linhas para exportar para o Excel !!!")
            End If

        End Sub
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Menu Baixa Manual</b>
        </div>
        <div class="box-body">
            <%-- envio privatvo --%>
            <div class="row" runat="server" id="divprivado" visible="true">
                <div class="row">
                    <div class="col-lg-4" visible="false">
                        <asp:Label Text="Buscar Por" runat="server" />
                        <asp:DropDownList runat="server" CssClass="form-control" ID="cbobusca">
                            <asp:ListItem Text="nrseqcliente" Value="nrseqcliente" />
                            <asp:ListItem Text="Nome" Value="nome" />
                            <asp:ListItem Text="Matricula" Value="matricula" />
                            <asp:ListItem Text="CPF" Value="cpf" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-lg-3">
                        <b>
                            <asp:Label Text="Data de carregamento do arquivo" runat="server" /></b>
                        <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtbaixa" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <%--       <div class="col-lg-6">
                                <br />
              <asp:TextBox runat="server" id="txtbusca" CssClass="form-control" />  
                                </div>--%>

                    <div class="col-lg-2 text-center">
                        <br>
                        <asp:LinkButton ID="btnbuscando" runat="server" CssClass="btn btn-primary "> <i class="fa fa-searc"></i> Buscar</asp:LinkButton>
                    </div>
                </div>
            </div>



            <div class="row">
                <br>
                <asp:GridView ID="grademanual" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro encontrado" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>' CssClass="badge"></asp:Label>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="contacorrente" HeaderText="Conta" />
                        <asp:BoundField DataField="nrseqcliente" HeaderText="Nrseq " />
                        <asp:BoundField DataField="nomecliente" HeaderText="Cliente" />
                        <asp:BoundField DataField="valor" HeaderText="Valor" />
                        <asp:BoundField DataField="vinculada" HeaderText="Vínculo" />

                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <div runat="server" id="divacoes">
                                    <asp:LinkButton ID="vincular" runat="server" CssClass="btn btn-primary btn-xs" CommandName="vincular" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>

                                    <asp:LinkButton ID="cancelar" runat="server" CssClass="btn btn-danger btn-xs" CommandName="cancelar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <div style="display: none">
                    <asp:DataGrid ID="dgexcel" runat="server" BackColor="WhiteSmoke" BorderColor="Black"
                        CellPadding="0" CellSpacing="1" Font-Name="Arial" Font-Names="Arial"
                        Font-Size="8pt" HeaderStyle-BackColor="#0000FF"
                        Headerstyle-Font-Name="Arial" HeaderStyle-Font-Size="10"
                        HeaderStyle-ForeColor="#FFFFFF" Height="148px" Style="margin-right: 0px"
                        AutoGenerateColumns="False" ShowFooter="True">
                        <AlternatingItemStyle BackColor="White" />
                        <Columns>
                            <asp:BoundColumn DataField="contacorrente" HeaderText="Conta" />
                            <asp:BoundColumn DataField="nrseqcliente" HeaderText="Nrseq " />
                            <asp:BoundColumn DataField="nomecliente" HeaderText="Cliente" />
                            <asp:BoundColumn DataField="valor" HeaderText="Valor" />
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="col-lg-6  text-center">
                    <br />
                    <asp:LinkButton ID="btnexcel" runat="server" OnClick="exportarh" CssClass="btn btn-success " autopostback="true"><i class="fa fa-file-excel-o"></i>Excel</asp:LinkButton>
                </div>

            </div>


        </div>
    </div>
</asp:Content>

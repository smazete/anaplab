﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage.master" CodeFile="relatorioaniversariante.aspx.vb" Inherits="relatorioaniversariante" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script runat="server">

        Sub exportarh(ByVal Source As Object, ByVal E As EventArgs)

            txtnomearq.Text = "Aniv" & Date.Now.Date.ToString.Replace("/", "").Trim()
            txtnomearq.Text = Date.Now.Date.ToString.Replace(":", "").Trim()

            If txtnomearq.Text = "" Then
                txtnomearq.BackColor = Drawing.Color.Red
                Exit Sub
            End If

            'exportarExcel(dg, txtnomearq.Text)

            exportarExcel(dgexcel, txtnomearq.Text)



        End Sub

        Sub exportarExcel(grid As DataGrid, saveAsFile As String)

            ' O linite de linhas do Excel é  65536
            If grid.Items.Count + 1 < 65536 Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & saveAsFile & ".xls")
                ' Remover caracteres do header - Content-Type
                HttpContext.Current.Response.Charset = ""
                'HttpContext.Current.Response.WriteFile("style.txt")
                ' desabilita o  view state.
                grid.EnableViewState = False
                Dim tw As New System.IO.StringWriter()
                Dim hw As New System.Web.UI.HtmlTextWriter(tw)
                grid.RenderControl(hw)
                ' Escrever o html no navegador
                HttpContext.Current.Response.Write(tw.ToString())
                ' termina o response
                HttpContext.Current.Response.End()

            Else
                HttpContext.Current.Response.Write("Muitas linhas para exportar para o Excel !!!")
            End If

        End Sub
    </script>

    <script type="text/javascript">








        function irlink(linkativo) {



            window.open(linkativo, '_blank');

        }

        function irlinkmesmpagina(linkativo) {

            window.location.href = linkativo;

            //window.open(linkativo);

        }

    </script>



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header ">
            <b>Relatório Aniversariante</b>
        </div>
        <asp:UpdatePanel runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="btnexcel" />
            </Triggers>

            <ContentTemplate>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <label id="Label2" for="txtdtinicial">Data Inicial</label>
                            <asp:TextBox runat="server" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdtinicial" ClientIDMode="Static" />
                        </div>
                        <div class="col-lg-3">
                            <label id="Label3" for="txtdtinicial">Data Final</label>
                            <asp:TextBox runat="server" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID="txtdtfinal" ClientIDMode="Static" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:CheckBox AutoPostBack="true" CssClass="checkbox-inline" runat="server" ID="chkexibiraniversariantesdomes" Text="Aniversariantes do mes" />
                        </div>
                    </div>

                    <br />
                    <div class="row">

                        <div class="col-lg-3">
                            <asp:Button runat="server" CssClass="btn btn-primary btn-lg" ID="btnImprimir" Text="Imprimir" AutoPostBack="true" />
                        </div>

                        <div class="col-lg-4">
                            <asp:Label ID="txtnomearq" runat="server"></asp:Label>
                            
                    <asp:LinkButton ID="btnexcel" runat="server" OnClick="exportarh" CssClass="btn btn-success "><i class="fa fa-file-excel-o"></i>Excel</asp:LinkButton>
                        </div>
                        
                        <div class="col-lg-4">
                            
                    <asp:LinkButton ID="btnbuscar" runat="server"  CssClass="btn btn-success "><i class="fa fa-file-excel-o"></i>Buscar</asp:LinkButton>
                        </div>
                    </div>

                    <br />
                </div>

                    <asp:GridView ID="gradeaniversariante" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="nome" HeaderText="Nome" />
                        <asp:BoundField DataField="dtnasc" HeaderText="Data de Nascimento" />
                <asp:BoundField DataField="telefone" HeaderText="Tel Residencial" />
                <asp:BoundField DataField="email" HeaderText="E-mail" />
                    </Columns>
                </asp:GridView>

                <div style="display: none">
                        <asp:DataGrid ID="dgexcel" runat="server" BackColor="WhiteSmoke" BorderColor="Black"
                            CellPadding="0" CellSpacing="1" Font-Name="Arial" Font-Names="Arial"
                            Font-Size="8pt" HeaderStyle-BackColor="#0000FF"
                            Headerstyle-Font-Name="Arial" HeaderStyle-Font-Size="10"
                            HeaderStyle-ForeColor="#FFFFFF" Height="148px" Style="margin-right: 0px"
                            AutoGenerateColumns="False" ShowFooter="True">
                            <AlternatingItemStyle BackColor="White" />
                            <Columns>
                                
                        <asp:BoundColumn DataField="nome" HeaderText="Associado" />
                        <asp:BoundColumn DataField="dtnasc" HeaderText="Data de Nascimento" />
                <asp:BoundColumn DataField="telefone" HeaderText="Tel Residencial" />
                <asp:BoundColumn DataField="email" HeaderText="E-mail" />

                            </Columns>
                        </asp:DataGrid>
                    </div>


               

            </ContentTemplate>
        </asp:UpdatePanel>



    </div>
</asp:Content>

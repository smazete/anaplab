﻿
Partial Class lercontatos
    Inherits System.Web.UI.Page

    Private Sub lercontatos_Load(sender As Object, e As EventArgs) Handles Me.Load
        carregacontato()
    End Sub


    Public Sub carregacontato(Optional exibirinativos As Boolean = False)
        'Dim xempresas As New clsEmpresas
        Dim xAssociados As New clsconfig

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        tb1 = tab1.conectar("select * from tbcontato order by ativo asc ")


        gradecontato.DataSource = tb1
        gradecontato.DataBind()

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub gradecontato_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradecontato.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradecontato.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "usar" Then

            Dim xContato As New clscontatos

            xContato.Nrseq = nrseq.Value

            If Not xContato.procurar() Then
                sm("swal({title: 'Error!',text: '" & xContato.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            xContato.Nrseq = xContato.Nrseq
            txtassunto.Text = xContato.Assunto
            txtdescricao.Text = xContato.Mensagem
            txtemail.Text = xContato.Email
            txtnome.Text = xContato.Nomecompleto
            lbldata.Text = xContato.Dtcad
        End If

    End Sub
End Class

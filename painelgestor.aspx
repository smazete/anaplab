﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="painelgestor.aspx.vb" Inherits="painelgestor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="updatepainel" runat="server">
        <ContentTemplate >

       
        <div class="panel panel-primary" style="-webkit-box-shadow: 0px 0px 18px 0px rgba(50, 50, 50, 0.75); text-align: center; -moz-box-shadow: 0px 0px 18px 0px rgba(50, 50, 50, 0.75); box-shadow: 0px 0px 18px 0px rgba(50, 50, 50, 0.75);" id="notify03">
                <div class="panel-heading" style="background-color: #181B34; color: white" runat="server" id="div1">
                    <div class="row">
                        <div class="col-lg-1 ">
                            <asp:Image runat="server" ImageUrl="~/img/atencao.png" Style="width: 2.5rem; height: 2.5rem;" />
                        </div>
                        <div class="col-lg-10 text-center">
                            <asp:Label ID="Label6" runat="server" Text="Painel Geral"></asp:Label>
                        </div>
                        <div class="col-lg-1 text-center">
                            <a href="#" class="fechar" onclick="escondenotify01()"><span class="glyphicon glyphicon-remove-circle " style="background-color: transparent; color: white;"></span></a>
                        </div>

                    </div>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 ">
                            <div class="box box-danger">
                                <div class="box box-header">
                                    <b><asp:Label ID="lblano" runat="server"></asp:Label> </b>
                                    <asp:HiddenField ID="hdcolaboradoratual" runat="server" />
                                         <asp:Repeater runat="server" ID="rptatual">
                                                                <ItemTemplate >
                                                               <div class="row" style="color: white">
                                                                <div class="col-lg-12 ">
                                                                    <div class="info-box bg-green" id="divbox" runat="server">
                                                        <span class="info-box-icon"><i class="fa fa-user-circle  "></i></span>
                                                        <div class="info-box-content">
                                                            <span class="info-box-text">
                                                                <asp:Label ID="Label1" runat="server" Text='<%#Bind("nomecolaborador") %>'> </asp:Label>
                                                            </span>
                                                            <span class="info-box-text">
                                                                <div class="row">
                                                                   
                                                                   <div class="col-lg-10  text-left ">
                                                                       <asp:HiddenField ID="hdhorista" runat="server" value='<%#Bind("horista") %>'></asp:HiddenField>
                                                                           <asp:Label ID="Label2" Font-Size="Small" runat="server" Text='<%#Bind("cargo") %>'></asp:Label>
                                                                    </div>
                                                                     <div class="col-lg-2 text-center">
                                                                           <asp:Label ID="lbldatarpt" Font-Size="Small" runat="server" Text='<%#Bind("matriculacolaborador") %>'></asp:Label>
                                                                    </div>
                                                                </div>
                                                             </span>
                                                            <!-- The progress section is optional -->
                                                            <div class="progress">
                                                                <div class="progress-bar" style="width: 100%" id="divprogress" runat="server"></div>
                                                            </div>
                                                            <span class="progress-description">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        Objetivos
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        Meio de ano
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        Final de ano
                                                                    </div>
                                                                <div class="col-lg-3">
                                                                        Cursos
                                                                    </div>
                                                                </div>
                                                                <span class="info-box-number">
                                                                    <div class="row">
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblobjrpt" runat="server" Text='<%#Bind("totalavaliacoes1") %>'></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblmeiorpy" runat="server" Text='<%#Bind("totalavaliacoes2") %>'></asp:Label>
                                                                        </div>
                                                                      
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblfinalrpt" runat="server" Text='<%#Bind("totalavaliacoes3") %>'></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblcursos" runat="server" Text='<%#Bind("cursos") %>'></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <!-- /.info-box-content -->
                                                    </div>
</div> 
                                                                </div>
                                                                    </itemtemplate>
                                                                    </asp:Repeater>   
                                                             
                                </div>
                                <div class="box-body ">
                                     <div class="row">
                                         <div class="col-lg-2 text-center">
                                             <asp:LinkButton ID="btnpainelgestor" runat="server" type="button" class="brilhar"    title="Minha Equipe"><i class="fa fa-search-plus"></i>
                                                    </asp:LinkButton>
                                             </div> 
                                        <div class="col-lg-8 text-center">
                                            <asp:Label ID="lblnome" runat="server" CssClass="label-success "></asp:Label>
                                            <br />
                                            </div> 
                                         </div> 
                                      <div class="row">
                                <div class="col-lg-12 text-center">
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatepainel">
                                        <ProgressTemplate>
                                            Carregando...
                                                <br />
                                            <img src="../img/loaddouble01.gif" style="width: 9rem; height: 9rem;" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                                    <div class="row">
                                        <div class="col-lg-12 ">
                                            <asp:GridView ID="gradepainelgeral" runat="server" CssClass="table" GridLines="None" AutoGenerateColumns="false" EmptyDataText="No projects" ShowHeaderWhenEmpty="true" ClientIDMode="Static">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Gestores Diretos" ItemStyle-BackColor="#181B34">
                                                        <ItemTemplate>
                                                          
                                                               <asp:Repeater runat="server" ID="rptdadosgestor">
                                                                <ItemTemplate >
                                                               <div class="row" style="color: white">
                                                                <div class="col-lg-12 ">
                                                                    <div class="info-box bg-green" id="divbox" runat="server">
                                                        <span class="info-box-icon"><i class="fa fa-user-circle  "></i></span>
                                                        <div class="info-box-content">
                                                            <span class="info-box-text">
                                                                <asp:Label ID="Label1" runat="server" Text='<%#Bind("nomecolaborador") %>'> </asp:Label>
                                                            </span>
                                                            <span class="info-box-text">
                                                                <div class="row">
                                                                   
                                                                   <div class="col-lg-10  text-left ">
                                                                       <asp:HiddenField ID="hdhorista" runat="server" value='<%#Bind("horista") %>'></asp:HiddenField>
                                                                           <asp:Label ID="Label2" Font-Size="Small" runat="server" Text='<%#Bind("descricaocargo") %>'></asp:Label>
                                                                    </div>
                                                                     <div class="col-lg-2 text-center">
                                                                           <asp:Label ID="lbldatarpt" Font-Size="Small" runat="server" Text='<%#Bind("matriculacolaborador") %>'></asp:Label>
                                                                    </div>
                                                                </div>
                                                             </span>
                                                            <!-- The progress section is optional -->
                                                            <div class="progress">
                                                                <div class="progress-bar" style="width: 100%" id="divprogress" runat="server"></div>
                                                            </div>
                                                            <span class="progress-description">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        Objetivos
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        Meio de ano
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        Final de ano
                                                                    </div>
                                                                <div class="col-lg-3">
                                                                        Cursos
                                                                    </div>
                                                                </div>
                                                                <span class="info-box-number">
                                                                    <div class="row">
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblobjrpt" runat="server" Text='<%#Bind("totalavaliacoes1") %>'></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblmeiorpy" runat="server" Text='<%#Bind("totalavaliacoes2") %>'></asp:Label>
                                                                        </div>
                                                                      
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblfinalrpt" runat="server" Text='<%#Bind("totalavaliacoes3") %>'></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblcursos" runat="server" Text='<%#Bind("totalcursos") %>'></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <!-- /.info-box-content -->
                                                    </div>
</div> 
                                                                </div>
                                                                    </itemtemplate>
                                                                    </asp:Repeater> 
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Equipe" ItemStyle-BackColor="#F8F8FF">
                                                        <ItemTemplate>

                                                            <asp:Repeater runat="server" ID="rptsegundonivel">
                                                              
                                                                <ItemTemplate >

                                                               

                                                            <%-- <div class="row" style="color: white">
                                                                <div class="col-lg-12 ">--%>
                                                                    <div class="info-box bg-green" id="divbox" runat="server">
                                                        <span class="info-box-icon"><i class="fa fa-user-circle  "></i></span>
                                                        <div class="info-box-content">
                                                            <span class="info-box-text">
                                                                <div class="row">
                                                                    <div class="col-lg-2 "></div>
                                                                    <div class="col-lg-8 text-center">
                                                                         <asp:Label ID="lblnomeusuariogradepainel" runat="server" Text='<%#Bind("nomecolaborador") %>'> </asp:Label>

                                                                    </div>
                                                                    <div class="col-lg-2 text-center">
                                                                        <asp:LinkButton ID="btngestorrpt" runat="server" CommandArgument='<%#Container.ItemIndex %>' OnClick="rptcommand1" class="brilhartransparente" style="color:white;"><i class="fa fa-users  " ></i></asp:LinkButton>
                                                                        <asp:HiddenField ID="hdpainelgradenrseq" runat="server" Value='<%#Bind("nrseqcolaborador") %>' />
                                                                         <asp:HiddenField ID="hdgestorrpt" runat="server" Value='<%#Bind("gestor") %>' />
                                                                    </div>
                                                                </div>
                                                                
                                                               
                                                            </span>
                                                            <span class="info-box-text">
                                                               <div class="row">
                                                                   
                                                                    
                                                                    <div class="col-lg-10   text-left">
                                                                          <asp:HiddenField ID="hdhorista"   runat="server" value='<%#Bind("horista") %>'></asp:HiddenField>
                                                                           <asp:Label ID="Label2" Font-Size="Small" runat="server" Text='<%#Bind("descricaocargo") %>'></asp:Label>
                                                                    </div>
                                                                    <div class="col-lg-2 text-center">
                                                                           <asp:Label ID="lbldatarpt" Font-Size="Small" runat="server" Text='<%#Bind("matriculacolaborador") %>'></asp:Label>
                                                                    </div>
                                                                </div></span>
                                                            <!-- The progress section is optional -->
                                                            <div class="progress">
                                                                <div class="progress-bar" style="width: 100%" id="divprogress" runat="server"></div>
                                                            </div>
                                                            <span class="progress-description">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        Objetivos
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        Meio de ano
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        Final de ano
                                                                    </div>
                                                                <div class="col-lg-3">
                                                                        Cursos
                                                                    </div>
                                                                </div>
                                                                <span class="info-box-number">
                                                                    <div class="row">
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblobjrpt" runat="server" Text='<%#Bind("totalavaliacoes1") %>'></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblmeiorpy" runat="server" Text='<%#Bind("totalavaliacoes2") %>'></asp:Label>
                                                                        </div>
                                                                      
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblfinalrpt" runat="server" Text='<%#Bind("totalavaliacoes3") %>'></asp:Label>
                                                                        </div>
                                                                        <div class="col-lg-3">
                                                                            <asp:Label ID="lblcursos" runat="server" Text='<%#Bind("totalcursos") %>'></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <!-- /.info-box-content -->
                                                    </div>
<%--</div> 
                                                                </div> --%>
                                                             </ItemTemplate>
                                                            </asp:Repeater>
                                                          <%-- <asp:TreeView ID="arvore" runat="server"></asp:TreeView>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                  
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>


                                    <asp:Repeater runat="server" ID="rptpainel">
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-lg-8 ">
                                                    <asp:Image ID="imgclienterpt" runat="server" Style="width: 10rem; height: 10rem;" ImageUrl="~/img/imgcliente.png" />
                                                </div>
                                                <div class="col-lg-4 ">
                                                    <asp:Label ID="lbldtvalidaderpt" CssClass="label-warning " runat="server" Text='<%#Bind("dtvalidade") %>'> </asp:Label>

                                                    <br />
                                                    <asp:Label ID="Label8" CssClass="label-success " runat="server" Text='<%#Bind("nomeusuario") %>'> </asp:Label>
                                                    <br />
                                                    <asp:Label ID="Label7" CssClass="label-danger " runat="server" Text='<%#Bind("filial") %>'> </asp:Label>
                                                    <br />
                                                    <asp:Label ID="Label9" CssClass="label-primary " runat="server" Text='<%#Bind("descstatus") %>'> </asp:Label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <asp:Label ID="lblclienterpt" CssClass="label-warning " runat="server" Text='<%#Bind("nomecliente") %>'> </asp:Label>
                                                </div>
                                            </div>

                                            <br />



                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>



                        </div>
                        <div class="col-lg-4 ">
                        </div>
                        <div class="col-lg-4 ">
                        </div>
                    </div>


                </div>
            </div>
     </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


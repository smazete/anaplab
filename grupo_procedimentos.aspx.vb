﻿Imports System.Data
Imports System.IO
Imports clsSmart

Partial Class restrito_grupo_procedimentos
    Inherits System.Web.UI.Page
    Dim tabps As New clsBanco
    Dim tbps As New Data.DataTable
    Dim tabelaEmpresa As String = " tbempresas"
    Dim sql As String

    Private Function desativar()
        ddlConvenio.Enabled = False
        btnaddconv.Enabled = False
        ddlProcedimentos.Enabled = False
        btnaddproc.Enabled = False
    End Function

    Private Function ativarConv()
        ddlConvenio.Enabled = True
        btnaddconv.Enabled = True
    End Function

    Private Function ativarProc()
        ddlProcedimentos.Enabled = True
        btnaddproc.Enabled = True
    End Function

    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function
    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function

    Private Function carregaEmpresa() As Data.DataTable
        Dim nrseqempresa As String = Session("idempresaemuso")
        Dim sql = "SELECT * FROM " & tabelaEmpresa & " WHERE ativo = true AND nrseq = '" & nrseqempresa & "' "
        Return findSql(sql)
    End Function

    Private Function adicionaConvenio()
        ddlConvenio.Items.Clear()

        sql = "select * from tbconvenios where ativo = True AND nrseqempresa = '" & Session("idempresaemuso") & "'"


        Dim table As DataTable = findSql(sql)

        ddlConvenio.Items.Insert(0, New ListItem("Selecione", ""))

        If table.Rows.Count > 0 Then
            For x As Integer = 0 To table.Rows.Count - 1
                ddlConvenio.Items.Add(New ListItem(table.Rows(x)("nomefantasia").ToString, table.Rows(x)("nrseq").ToString))
            Next
        End If
    End Function

    Private Function adicionaProcedimentos()
        ddlProcedimentos.Items.Clear()

        sql = "select * from tbprocedimento_map where convenio = '" & ddlConvenio.SelectedValue & "' AND ativo = True"


        Dim table As DataTable = findSql(sql)

        ddlProcedimentos.Items.Insert(0, New ListItem("Selecione", ""))

        If table.Rows.Count > 0 Then
            For x As Integer = 0 To table.Rows.Count - 1
                ddlProcedimentos.Items.Add(New ListItem(table.Rows(x)("descricao").ToString, table.Rows(x)("nrseq").ToString))
            Next
        End If
    End Function

    Private Sub restrito_grupo_procedimentos_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack Then Exit Sub

        Session("empresaemuso") = "" ' carregaEmpresa().Rows(0)("nome").ToString

        desativar()
        adicionaConvenio()
    End Sub

    Private Sub btncriar_Click(sender As Object, e As EventArgs) Handles btncriar.Click

        If txtgrupo.Value = "" Then

            Dim erro As String = ""
            ' Quantas você quiser dessa coisa... pra cada campo
            erro = verificaCampo(txtgrupo.Value, "Descrição", erro)

            If erro <> "" Then
                rowError.Style.Remove("display")
                lblErro.InnerHtml = erro
            End If
        Else
            Dim wcnrseqctrl As String = gerarnrseqcontrole()
            sql = "INSERT INTO tbprocedimentos_grupos (descricao, usercad, dtcad, nrseqempresa, nrseqctrl) VALUES ('" & txtgrupo.Value & "', '" & Session("usuario") & "', '" & formatadatamysql(Date.Now.Date) & "', " & Session("idempresaemuso") & ", '" & wcnrseqctrl & "')"

            persist(sql)

            sql = "select * from tbprocedimentos_grupos where nrseqctrl = '" & wcnrseqctrl & "'"

            Dim tbgrupo As DataTable = findSql(sql)

            If tbgrupo.Rows.Count <> 0 Then
                Session("nrseqgrupo") = tbgrupo.Rows(0)("nrseq").ToString
            Else
                Session("nrseqgrupo") = 1
            End If
            ativarConv()
        End If

    End Sub

    Private Sub btnaddconv_Click(sender As Object, e As EventArgs) Handles btnaddconv.Click
        Dim wcnrseqctrl As String = gerarnrseqcontrole()

        sql = "INSERT INTO tbprocedimentos_grupos_conv (nrseqconveniado, ativo, usercad, dtcad, nrseqgrupo, nrseqctrl) VALUES ('" & ddlConvenio.SelectedValue & "', 1, '" & Session("usuario") & "', '" & formatadatamysql(Date.Now.Date) & "', " & Session("nrseqgrupo") & ", '" & wcnrseqctrl & "')"

        persist(sql)

        sql = "select * from tbprocedimentos_grupos_conv where nrseqctrl = '" & wcnrseqctrl & "'"

        Dim tbgrupo1 As DataTable = findSql(sql)

        If tbgrupo1.Rows.Count <> 0 Then
            Session("nrseqgrupoconv") = tbgrupo1.Rows(0)("nrseq").ToString
            adicionaProcedimentos()
        Else
            Session("nrseqgrupoconv") = 1
        End If

        ativarProc()
    End Sub

    Private Sub ddlConvenio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlConvenio.SelectedIndexChanged
        adicionaProcedimentos()
    End Sub

    Private Sub btnaddproc_Click(sender As Object, e As EventArgs) Handles btnaddproc.Click

        Dim wcnrseqctrl As String = gerarnrseqcontrole()

        sql = "INSERT INTO tbprocedimentos_grupo_dth ( nrseqgrupoconv, nrseqprocedimento,  ativo, usercad, dtcad, nrseqctrl ) VALUES (" & Session("nrseqgrupoconv") & " , " & ddlProcedimentos.SelectedValue & ", 1, '" & Session("usuario") & "', '" & formatadatamysql(Date.Now.Date) & "', '" & wcnrseqctrl & "')"

        persist(sql)

        sql = "select * from tbprocedimentos_grupo_dth where nrseqctrl = '" & wcnrseqctrl & "'"

        Dim tbgrupo2 As DataTable = findSql(sql)

        If tbgrupo2.Rows.Count <> 0 Then
            Session("nrseqgrupodth") = tbgrupo2.Rows(0)("nrseq").ToString
        Else
            Session("nrseqgrupodth") = 1
        End If

        MontaTabela()
    End Sub

    Public Function converteDatatableToString(datatable As DataTable, Optional campo As String = "nrseq") As String
        Dim palavra As String = ""
        For x As Integer = 0 To datatable.Rows.Count - 1
            If x > 0 Then
                palavra &= ", "
            End If
            palavra &= datatable.Rows(x)(campo)
        Next
        Return palavra
    End Function

    Private Function MontaTabela()

        sql = "select pgd.nrseq, p.descricao, c.nome from tbprocedimentos_grupos as pg Inner join tbprocedimentos_grupos_conv as pgc on  pg.nrseq = pgc.nrseqgrupo inner join tbprocedimentos_grupo_dth as pgd on pgd.nrseqgrupoconv = pgc.nrseq inner join tbprocedimento_map as p on p.nrseq = pgd.nrseqprocedimento inner join tbconvenios as c on c.nrseq = pgc.nrseqconveniado where pgd.ativo = true and pg.nrseq = " & Session("nrseqgrupo").ToString

        Dim tb As DataTable = findSql(sql)

        gdvListaExames.DataSource = tb
        gdvListaExames.DataBind()

    End Function

    Private Sub gdvListaExames_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gdvListaExames.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gdvListaExames.Rows(index)

        If e.CommandName = "deletar" Then

            Dim nrseq As String = row.Cells(0).Text

            sql = "UPDATE tbprocedimentos_grupo_dth SET ativo = NULL WHERE  nrseq = " & nrseq

            persist(sql)



            Dim tb As DataTable = findSql("select pgd.nrseq, p.descricao from tbprocedimentos_grupos as pg Inner join tbprocedimentos_grupos_conv as pgc on  pg.nrseq = pgc.nrseqgrupo inner join tbprocedimentos_grupo_dth as pgd on pgd.nrseqgrupoconv = pgc.nrseq inner join tbprocedimento_map as p on p.nrseq = pgd.nrseqprocedimento where pgd.ativo = true and pg.nrseq = " & Session("nrseqgrupo").ToString)


                gdvListaExames.DataSource = tb
                gdvListaExames.DataBind()


        End If
    End Sub

    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click

        If txtgrupo.Value = "" Then
            Dim erro As String = ""
            ' Quantas você quiser dessa coisa... pra cada campo
            erro = verificaCampo(txtgrupo.Value, "Descrição", erro)

            If erro <> "" Then
                rowError.Style.Remove("display")
                lblErro.InnerHtml = erro
            End If
        Else

            sql = "UPDATE tbprocedimentos_grupos SET ativo = 1, descricao = '" & txtgrupo.Value & "' WHERE  nrseq = " & Session("nrseqgrupo")

            persist(sql)

            ddlConvenio.Enabled = False
            btnaddconv.Enabled = False
            ddlProcedimentos.Enabled = False
            btnaddproc.Enabled = False
            txtgrupo.Value = ""
            ddlProcedimentos.SelectedValue = ""
            ddlConvenio.SelectedValue = ""
            btnsalvar.Enabled = False

            gdvListaExames.DataSource = Nothing
            gdvListaExames.DataBind()

            rowError.Style.Add("display", "none")

        End If

    End Sub

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click

        ddlConvenio.Enabled = False
        btnaddconv.Enabled = False
        ddlProcedimentos.Enabled = False
        btnaddproc.Enabled = False
        txtgrupo.Value = ""
        ddlProcedimentos.SelectedValue = ""
        ddlConvenio.SelectedValue = ""
        btnsalvar.Enabled = False

    End Sub

    Private Sub btnprocura_Click(sender As Object, e As EventArgs) Handles btnprocura.Click
        sql = "SELECT * FROM tbprocedimentos_grupos WHERE descricao LIKE '%" & txtprocura.Value & "%' AND ativo = true AND nrseqempresa = " & Session("idempresaemuso") & ""
        Dim tb As DataTable = findSql(sql)

        gdvListaGrupos.DataSource = tb
        gdvListaGrupos.DataBind()
    End Sub

    Private Sub gdvListaGrupos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gdvListaGrupos.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gdvListaGrupos.Rows(index)

        If e.CommandName = "editar" Then

            Dim nrseq As String = row.Cells(0).Text

            Dim tb As DataTable = findSql("SELECT * FROM tbprocedimentos_grupos WHERE nrseq = " & nrseq & "")

            Session("nrseqgrupo") = tb.Rows(0)("nrseq")
            txtgrupo.Value = tb.Rows(0)("descricao")

            sql = "select pgd.nrseq, p.descricao, c.nome from tbprocedimentos_grupos as pg Inner join tbprocedimentos_grupos_conv as pgc on  pg.nrseq = pgc.nrseqgrupo inner join tbprocedimentos_grupo_dth as pgd on pgd.nrseqgrupoconv = pgc.nrseq inner join tbprocedimento_map as p on p.nrseq = pgd.nrseqprocedimento inner join tbconvenios as c on c.nrseq = pgc.nrseqconveniado where pgd.ativo = true and pg.nrseq = " & Session("nrseqgrupo").ToString

            Dim tb2 As DataTable = findSql(sql)


            gdvListaExames.DataSource = tb2
            gdvListaExames.DataBind()

            ativarConv()

            btnsalvar.Enabled = True



        End If

        If e.CommandName = "deletar" Then

            Dim nrseq As String = row.Cells(0).Text

            Dim tb As DataTable = findSql("SELECT * FROM tbprocedimentos_grupos WHERE nrseq = " & nrseq & "")

            sql = "UPDATE tbprocedimentos_grupos SET ativo = 0 WHERE  nrseq = " & tb.Rows(0)("nrseq")

            persist(sql)

            Dim tb2 As DataTable = findSql("SELECT * FROM tbprocedimentos_grupos WHERE descricao LIKE '%" & txtprocura.Value & "%' AND nrseqempresa = " & Session("idempresaemuso") & "")

            gdvListaGrupos.DataSource = tb2
            gdvListaGrupos.DataBind()

        End If
    End Sub

    Public Function verificaCampo(input As String, campo As String, erro As String) As String
        If input = "" Then
            If erro <> "" Then
                erro &= ",<br>"
            Else
                erro &= "<br>"
            End If
            erro &= campo
        End If
        Return erro
    End Function
End Class

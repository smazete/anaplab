﻿Imports System.Data
Imports clsSmart

Partial Class relatorioaniversariante
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    Private Sub cbofunc_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack Then Exit Sub
        chkexibiraniversariantesdomes.Checked = False
    End Sub


    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        Dim link As String = ""

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        tb1 = tab1.conectar("select * from tbAssociados where ativo = true and (month(dtnasc) >= " & CDate(txtdtinicial.Text).Month & " and day(dtnasc) >= " & CDate(txtdtinicial.Text).Day & ") and (month(dtnasc) <= " & CDate(txtdtfinal.Text).Month & " and day(dtnasc) <= " & CDate(txtdtfinal.Text).Day & ") order by dtnasc")

        Session("datainicial") = txtdtinicial.Text
        Session("datafinal") = txtdtfinal.Text
        Session("sql") = tb1

        link = "irlink('" & Request.Url.OriginalString.ToString.Replace(Request.Url.PathAndQuery, "") & "/relatorios/aniversariantes.aspx');"

        sm(link, link)

    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub carregagrade()
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        tb1 = tab1.conectar("select * from tbAssociados where ativo = true and (month(dtnasc) >= " & CDate(txtdtinicial.Text).Month & " and day(dtnasc) >= " & CDate(txtdtinicial.Text).Day & ") and (month(dtnasc) <= " & CDate(txtdtfinal.Text).Month & " and day(dtnasc) <= " & CDate(txtdtfinal.Text).Day & ") order by dtnasc")


        gradeaniversariante.DataSource = tb1
        gradeaniversariante.DataBind()

        dgexcel.DataSource = tb1
        dgexcel.DataBind()

        sm("exportarh", "exportarh")
    End Sub
    Private Sub limpar()
        txtdtfinal.Text = ""
        txtdtinicial.Text = ""

    End Sub
    Private Sub habilitar(Optional habilita As Boolean = True)
        txtdtfinal.Enabled = habilita
        txtdtinicial.Enabled = habilita
    End Sub

    Private Sub chkexibiraniversariantesdomes_CheckedChanged(sender As Object, e As EventArgs) Handles chkexibiraniversariantesdomes.CheckedChanged
        If chkexibiraniversariantesdomes.Checked = True Then
            habilitar(False)
            limpar()
        Else
            habilitar()
        End If
    End Sub

    Private Sub btnbuscar_Click(sender As Object, e As EventArgs) Handles btnbuscar.Click
        carregagrade()
    End Sub
End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="meusdocumentos.aspx.vb" MasterPageFile="~/MasterPage3.master" Inherits="autoatendimento_meusdocumentos" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   

    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="box box-primary widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
                <div class="widget-user-image">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Download</h3>
                <h5 class="widget-user-desc">Arquivos vinculados ao cliente.<asp:label  runat="server" /> </h5>
            </div>
            <div class="box-footer no-padding">
                <asp:GridView ID="grade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum documento cadastrado !" CssClass="table table-striped bg-gray" GridLines="none" AutoGenerateColumns="false" HeaderStyle="Black" >
                    <Columns>
                        <asp:TemplateField HeaderText="Download">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                <asp:HiddenField ID="hdarquivo" runat="server" Value='<%#Bind("arquivo")%>'></asp:HiddenField>
                                <asp:LinkButton ID="baixar" runat="server" CssClass="btn btn-info btn-xs" CommandName="baixar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-download" ></i></asp:LinkButton>

<%--                                 <asp:LinkButton ID="link" runat="server" CssClass="btn btn-info btn-xs" CommandName="link" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-external-link" ></i></asp:LinkButton>--%>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="nome" HeaderText="Descricao" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

    <div class="col-md-2"></div>
</asp:Content>
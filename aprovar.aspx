﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/masterpage.master" CodeFile="aprovar.aspx.vb" Inherits="aprovar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Aprovação Novos Associados</b>
            <div class="close">
                <asp:CheckBox Text="Rejeitados" runat="server" ID="chkrefresh" />
                <asp:LinkButton ID="btnbuscar" runat="server" CssClass="btn btn-primary btn-sm"> <i class="fa fa-refresh"></i></asp:LinkButton>
            </div>
        </div>
        <div class="row">
            <asp:GridView ID="gradeaprovar" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                            <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' autopostback>
                            <i class="fa fa-eye"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="nrseq" HeaderText="Código" />
                    <asp:BoundField DataField="nome" HeaderText="Nome" />
                    <asp:BoundField DataField="cpf" HeaderText="CPF" />
                    <asp:BoundField DataField="telefone" HeaderText="Telefone" />
                    <asp:BoundField DataField="email" HeaderText="Email" />
                    <asp:BoundField DataField="dtcad" HeaderText="Registro" />
                </Columns>
            </asp:GridView>
        </div>


        <%-- selecionado --%>


        <div class="box-body" id="divmostradados" runat="server">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:LinkButton ID="btnnovo" runat="server" CssClass="btn btn-warning " Visible="false"> <i class="fa fa-plus-circle"></i> Novo</asp:LinkButton>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Dados Pessoais</h3>
                        <div class="box-tools pull-right">
                            <!-- Buttons, labels, and many other things can be placed here! -->

                            <!-- Here is a label for example -->
                            <span class="label label-primary">
                                <asp:HiddenField runat="server" ID="hdnrseq" />
                                <asp:Label ID="txtnrseq" Text="Nrseq" runat="server">0</asp:Label></span>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="Matricula" runat="server"></asp:Label>
                                <asp:TextBox ID="txtmatricula" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-4">
                                <asp:Label Text="Nome" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="E-mail" runat="server"></asp:Label>
                                <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Nascimento" runat="server"></asp:Label>
                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtnasc" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-2">
                                <asp:Label Text="CPF" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcpf" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="RG" runat="server"></asp:Label>
                                <asp:TextBox ID="txtrg" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Telddd" runat="server"></asp:Label>
                                <asp:TextBox ID="txttelddd" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Telefone" runat="server"></asp:Label>
                                <asp:TextBox ID="txttelefone" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:Label Text="Sexo" runat="server"></asp:Label>
                                <asp:TextBox ID="txtsexo" runat="server" CssClass="form-control" Text="M"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="CEP" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcep" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:Label Text="Estado" runat="server"></asp:Label>
                                <asp:TextBox ID="txtestado" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label Text="Cidade" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcidade" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Endereço" runat="server"></asp:Label>
                                <asp:TextBox ID="txtendereco" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label Text="Bairro" runat="server"></asp:Label>
                                <asp:TextBox ID="txtbairro" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:Label Text="Numero" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnumero" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label Text="Complemento" runat="server"></asp:Label>
                                <asp:TextBox ID="txtcomplemento" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-4">
                                <asp:Label Text="Nome Mãe" runat="server"></asp:Label>
                                <asp:TextBox ID="txtnomemae" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>

                        </div>

                    </div>
                    <div class="row">


                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <b>Dados de Acesso </b>
                                </div>
                                <div class="card-body">
                                    <br />
                                    <div class="col-lg-4">
                                        <b>
                                            <asp:Label Text="USUARIO:" runat="server"></asp:Label></b>
                                    </div>
                                    <div class="col-lg-8">
                                        <asp:TextBox ID="txtmatricula2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4">
                                        <b>
                                            <asp:Label Text="SENHA:" runat="server"></asp:Label></b>
                                    </div>
                                    <div class="col-lg-8">
                                        <asp:TextBox type="password" ID="txtsenha" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <b>Dados do Associado </b>
                                </div>
                                <div class="card-body bg-aqua">
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <b>
                                                <asp:Label ID="lblsituaçao" Text="Situação" runat="server"></asp:Label>
                                            </b>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:TextBox ID="txtsituacaopb" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <b>
                                                <asp:Label Text="Data posse" runat="server"></asp:Label>
                                            </b>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtposse" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                            <b>
                                                <asp:Label Text="Data Aposentadoria" runat="server"></asp:Label>
                                            </b>
                                        </div>
                                        <div class="col-lg-8">
                                            <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtaposentadoria" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">

                            <div class="card" >

                                <div class="col-lg-12" style="background-image: url('img/cardana4.png'); background-repeat: no-repeat; background-position: center; border-radius: 25px;">
                                    <div class="card-header">
                                        <b style="color: white">.  Dados Bancarios</b>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="card-body">

                                            <br />
                                            <asp:Label Style="color: white" Text="Agencia" runat="server"></asp:Label><br />
                                            <asp:TextBox Style="color: white; background-color: black" ID="txtAgencia" runat="server"></asp:TextBox><br />
                                            <asp:Label Style="color: white" Text="Conta" runat="server"></asp:Label>
                                            <br />

                                            <b>
                                                <asp:TextBox Style="color: white; background-color: black" ID="txtcontacorrente" runat="server"></asp:TextBox></b>

                                            <p>.    @Anaplab</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-9">
                    <span class="badge bg-blue-active">
                        <asp:CheckBox runat="server" Checked />
                        Gerar mensalidade Anual</span>
                    <span class="badge bg-blue-gradient">
                        <asp:CheckBox runat="server" />
                        Isentar Mes Atual"</span>
                    <span class="badge bg-blue-gradient">
                        <asp:CheckBox runat="server" Checked />
                        Libera Acesso Autoatendimento</span>
                </div>
                <div class="col-lg-3">
                    <asp:LinkButton Text="Rejeitar" runat="server" ID="btnrejeitar" CssClass="btn btn-warning" />
                    <asp:LinkButton Text="Aprovar" runat="server" ID="btnaprovar" CssClass="btn btn-success" />
                </div>
            </div>

        </div>
        <!-- /.box-body -->




    </div>
    <!-- /.box -->
</asp:Content>

﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeFile="Medicos.aspx.vb" Inherits="Medicos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        function carregahoras() {
            var qtditens = 0;
            var qtd = $("#gradehoras tr").length + 1;
            for (var i = 2; i < qtd; i++) {
                var tr = $("#gradehoras tr:nth-child(" + i + ")");
                $(tr).find("input[name='hdcoordenada_" + (i - 2) + "']").value = "12:00"
                $(tr).find("input[name='hdhora_" + (i - 2) + "']").value = "12:00"


            }

            document.getElementById("lblqtdnotas").value = qtditens;
        }

        function clicahoras(linha, hora) {
            //$(".btnvazio").on("click", function (e) {
            //    e.preventDefault();
            //});
            $("#divhoras").hide();
            //$("#divconfirma").show("slow");

            document.getElementById("txthorasel").value = hora
            document.getElementById("txtdocasel").value = linha
            //if (document.getElementById("ContentPlaceHolder1_gradehoras_lbltexto" + linha + "_" + hora).innerHTML != "Vazio") {
            //    swal({ title: 'Atenção!', text: "Esse horário já está preenchido !", type: 'error', confirmButtonText: 'OK' });
            //    document.getElementById("txthorasel").value = "Já preenchido"
            //} else {
            //    document.getElementById("ContentPlaceHolder1_gradehoras_lbltexto" + linha + "_" + hora).innerHTML = "Selecionado"
            //    //var tr = $("#gradehoras tr:nth-child(" + linha + 2 + ")");
            //    //$(tr).find("input[id*='lbltexto" + linha + "_" + hora + "']").val("teste");
            //}

            //$('#horas').css({ 'display': 'none' });
            //$('#horas').hide();
            //$('#divconfirma').css({ 'display': 'normal' });
            //$('#divconfirma').show();

            //  swal({ title: 'Atenção!', text:"Linha:" + hora +  '; Coluna ' + linha, type: 'error', confirmButtonText: 'OK' });

            //var qtditens = 0;
            //var qtd = $("#gradehoras tr").length + 1;
            //for (var i = 2; i < qtd; i++) {
            //    var tr = $("#gradehoras tr:nth-child(" + i + ")");
            //  tr.getElementById("lbltexto2_2").value="teste do claudio"

            //}

            //document.getElementById("lblqtdnotas").value = qtditens;
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--  -->

    <div class="box box-info" style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box-header with-border">
            <h3 class="box-title">Buscar Profissional</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-lg-4 ">
                    Buscar Profissional por :
                                        <br />
                    <asp:TextBox ID="txtprocura" runat="server" CssClass="form-control "></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <asp:Label ID="Label17" runat="server" Text="Filtrar pro:"></asp:Label>
                        <br />
                        <asp:DropDownList ID="cbobusca" runat="server" class="form-control">
                            <asp:ListItem Text="Nome" Value="nome"></asp:ListItem>
                            <asp:ListItem Text="Celular" Value="celular"></asp:ListItem>
                            <asp:ListItem Text="cod" Value="cod">Código</asp:ListItem>
                            <asp:ListItem Text="CRM" Value="crm"></asp:ListItem>
                            <asp:ListItem Text="CPF" Value="cpf"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-2 ">
                    <asp:CheckBox ID="chkexibirsuspensos" runat="server" Text="Exibir Suspensos" />
                    <br />
                    <asp:CheckBox ID="chkexibirexcluidos" runat="server" Text="Exibir Excluidos" />
                    <br />
                    <asp:CheckBox ID="chkexibirtodos" runat="server" Text="Exibir Todos" />
                </div>

                <div class="col-lg-2 ">
                    <asp:LinkButton ID="btnprocurar" runat="server" CssClass="btn btn-primary ">Buscar</asp:LinkButton>
                </div>

                <div class="col-lg-2 ">
                    <asp:LinkButton ID="btnnovomedico" runat="server" CssClass="btn btn-success ">Novo</asp:LinkButton>
                </div>
            </div>
                                   <asp:UpdatePanel ID="updategrade" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-12">
                            <br />
                            <asp:GridView ID="gradeprofissionais" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                <AlternatingRowStyle BackColor="AntiqueWhite" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                            <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("cod") %>' />

                                            <asp:LinkButton ID="Edit" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar"> 
                                            <i class="fas fa-edit"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="cod" HeaderText="Código" />
                                    <asp:BoundField DataField="nome" HeaderText="Profissional" />
                                    <asp:BoundField DataField="celular" HeaderText="Celular" />
                                    <asp:BoundField DataField="email" HeaderText="E-mail" />

                                    <asp:TemplateField HeaderText="Opções">
                                        <ItemTemplate runat="server">

                                            <asp:LinkButton ID="envioagenda" runat="server" CssClass="btn btn-default btn-xs" CommandName="envioagenda" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Enviar agenda por e-mail">
                                           <i class="fas fa-envelope"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnhorarios" runat="server" CssClass="btn btn-default btn-xs" CommandName="horario" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Horario">
                                           <i class="fas fa-clock"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="especialidades" runat="server" CssClass="btn btn-default btn-xs" CommandName="especialidades" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Especialidade">
                                           <i class="fas fa-heartbeat"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="agenda" runat="server" CssClass="btn btn-default btn-xs" CommandName="agenda" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Agenda">
                                           <i class="fas fa-calendar-plus-o"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="bloquear" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Bloquear ou Desbloquear">
                                           <i class="fa fa-user"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="suspenso" runat="server" CssClass="btn btn-default btn-xs" CommandName="suspenso" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Desativar usuário">
                                            <i class="fas fa-thumbs-down"></i> 
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="reativar" runat="server" CssClass="btn btn-default btn-xs" CommandName="reativar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reativar usuário">
                                            <i class="fas fa-thumbs-up"></i> 
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="resetsenha" runat="server" CssClass="btn btn-default btn-xs" CommandName="resetsenha" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reset de senha">
                                            <i class="fas fa-key"></i> 
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="senhapemail" runat="server" CssClass="btn btn-default btn-xs" CommandName="senhapemail" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Enviar senha por e-mail">
                                           <i class="fas fa-envelope"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="row">
            <center>
                                <div class ="col-lg-4 ">
                                      <asp:Button ID="btnAlterar" runat="server" Text="Alterar" Width="127px" CssClass="btn btn-primary " visible="false"/>
                                    </div> 
                                  <div class ="col-lg-4 ">
                                       <asp:Button ID="btnExcluir" runat="server" Text="Excluir" Width="127px" CssClass="btn btn-danger" visible="false"/>
                                    </div> 
                                  <div class ="col-lg-4 ">
                                        <asp:Button ID="btnReativar" runat="server" Text="Reativar" Width="127px" CssClass="btn btn-primary " visible="false"/>
                                    </div> 
                                 </center>
        </div>
        <!-- /.box-body -->
        <!-- /.box-footer -->
    </div>

    <!-- Cadastro novo Medico -->
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div runat="server" id="divmostradados">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <asp:HiddenField ID="hidcod" runat="server" Visible="true" />
                        <h3 class="box-title">
                            <asp:Label ID="lblnovomedico" runat="server"></asp:Label><asp:Label ID="lblstatusmedico" Style="color: grey" runat="server"></asp:Label></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="Nome"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtnome" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="CPF"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCpf" AutoPostBack="true" onkeyup="formataCPF(this,event);" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="Dt Nasc"></asp:Label>
                                    <br />
                                    <asp:TextBox onkeyup="formataData(this,event);" ID="txtDtnasc" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="Email"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="Celular"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCelular" runat="server" onkeyup="formataTelefone(this,event);" class="form-control"  MaxLength="14" ></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <!-- endereço -->
                        <div class="row">
                            <asp:UpdatePanel ID="updatecep" runat="server">
                                <ContentTemplate>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <asp:Label ID="Label7" runat="server" Text="Cep"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCep" runat="server" AutoPostBack="true" onkeyup="formataCEP(this,event);" class="form-control" MaxLength="9"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server" Text="Endereço"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtEndereco" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="Bairro"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtBairro" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <asp:Label ID="Label6" runat="server" Text="Cidade"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtCidade" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <asp:Label ID="Label25" runat="server" Text="UF"></asp:Label>
                                            <br />
                                            <asp:TextBox ID="txtEstado" runat="server" MaxLength="2" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="updatecep">
                                    <ProgressTemplate>
                                        <center>
                                              <img src="../img/loadred.gif" style="height:3rem;width:3rem;" />
                                                    </center>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                        <b>Dados profissionais </b>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="CRM"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCrm" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" Text="UF CRM"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtUfcrm" runat="server" MaxLength="2" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <asp:Label ID="Label19" runat="server" Text="Minutos Agenda"></asp:Label>
                                    <br />
                                    <asp:DropDownList ID="cbominutosagenda" runat="server" MaxLength="2" class="form-control">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>0</asp:ListItem>
                                        <asp:ListItem>15</asp:ListItem>
                                        <asp:ListItem>30</asp:ListItem>
                                        <asp:ListItem>60</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3" style="display: none">
                                <div class="form-group">
                                    <asp:Label ID="Label12" runat="server" Text="Contato"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtcontato" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label13" runat="server" Text="Tel Comercial"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtCepcomercial" runat="server" AutoPostBack="true" onkeyup="formataTelefone(this,event);" class="form-control"  MaxLength="15" ></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label18" runat="server" Text="Status Acesso"></asp:Label>
                                    <br />
                                    <asp:CheckBox runat="server" ID="chkativouser" Text=" Ativo" Checked />
                                    <asp:CheckBox runat="server" ID="CheckBox2" Text=" Ferias" />
                                    <asp:CheckBox runat="server" ID="CheckBox3" Text=" Bloqueado" />
                                </div>
                            </div>
                        </div>

                        <!-- Dados Para Acesso  -->

                        <b>Dados de Acesso ao Sistema </b>
                        <span class="row">

                             <span class="badge">
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label14" runat="server" Text="Usuario"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtLogin" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label15" runat="server" Text="Senha"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtsenha" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <asp:Label ID="Label16" runat="server" Text="Permissão"></asp:Label>
                                    <br />
                                    <asp:DropDownList ID="cbopermissoes" runat="server" class="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                    <asp:LinkButton runat="server" ID="btnsalvaruser" class="btn  btn-primary "> <i class="fa fa-key" aria-hidden="true"></i>  Salvar</asp:LinkButton>
                            </div>
                            </span>


                            <%--  --%>
                        </div>

                        <div class="row" id="mostrarhorarios">
                            <asp:GridView ID="gridhorarios" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                <AlternatingRowStyle BackColor="AntiqueWhite" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                            <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("nrseq") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Opções">

                                        <ItemTemplate runat="server">
                                            <asp:LinkButton ID="Edit" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar">                                                                     <i class="fas fa-edit"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Deletar">
                                           <i class="fas fa-trash-alt"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnusuario" runat="server" CssClass="btn btn-default btn-xs" CommandName="email" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Enviar senha por e-mail">
                                           <i class="fas fa-envelope"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnreativar" runat="server" CssClass="btn btn-default btn-xs" CommandName="suspenso" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reativar usuário">
                                            <i class="fas fa-thumbs-up"></i> 
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default btn-xs" CommandName="resetsenha" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reset de senha">
                                  <i class="fas fa-key"></i> 
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="usuario" HeaderText="Login" />
                                    <asp:BoundField DataField="nomecolaborador" HeaderText="Nome" />
                                    <asp:BoundField DataField="permissao" HeaderText="Permissão" />
                                    <asp:BoundField DataField="emailcolaborador" HeaderText="E-mail" />
                                    <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                    <asp:BoundField DataField="suspenso" HeaderText="Suspenso" />
                                </Columns>

                            </asp:GridView>
                        </div>



                        <!-- /.box-body -->
                        <div class="box-footer clearfix" style="">
                            <div class="row">
                                <div class="col-lg-2 ">
                                    <asp:LinkButton runat="server" ID="btnsalvarmedico" class="btn  btn-success "> <i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar</asp:LinkButton>
                                </div>
                                <div class="col-lg-2 ">
                                    <asp:LinkButton runat="server" ID="btnCancelarnovo" class="btn  btn-danger "> <i class="fa fa-ban" aria-hidden="true"></i> Cancelar</asp:LinkButton>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatepanel1">
                                        <ProgressTemplate>
                                            <center>
                                              <img src="../img/loadred.gif" style="height:3rem;width:3rem;" />
                                                    </center>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- DIV HORARIOS -->

            <div class="row" style="display: none">
                <div class="col-lg-2 ">
                    <asp:TextBox ID="txthorasel" runat="server"></asp:TextBox>
                    <asp:TextBox ID="txtdocasel" runat="server"></asp:TextBox>
                    <asp:Label ID="lbldoca" runat="server"></asp:Label>
                    <asp:TextBox ID="txtdiasemana" runat="server"></asp:TextBox>
                    <asp:TextBox ID="lbldiasemana" runat="server"></asp:TextBox>
                    <br />
                    <asp:Button ID="btncarregar" runat="server" Text="Teste " />
                </div>
            </div>


            <div class="box box-primary " runat="server" id="divhorarios">
                <div class="box box-header text-center">
                    <b></b>
                    <asp:Label ID="Label24" Text="Grade de Atendimento" runat="server" CssClass="label-success"></asp:Label><b>
                </div>
                <div class="box-primary ">
                    <asp:HiddenField ID="hdnrseqmedico" runat="server" />
                    <div class="row" style="overflow: scroll">
                        <div class="col-lg-12 ">
                            <asp:GridView ID="gradehoras" runat="server" AutoGenerateColumns="false" CssClass="table" GridLines="None">
                                <AlternatingRowStyle BackColor="WhiteSmoke" />
                                <Columns>
                                    <asp:BoundField HeaderText="Horário" DataField="horario" />
                                    <%--  <asp:ButtonField Text="aqui" ButtonType="Button" CommandName="aqui" HeaderText="teste" />--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="box-footer clearfix" style="">
                        <div class="row">
                            <div class="col-lg-6 text-center">
                                <asp:LinkButton ID="btnsalvarhoras" runat="server" CssClass="btn btn-success "><i class="fas fa-save"></i> Salvar </asp:LinkButton>
                            </div>
                            <div class="col-lg-6 text-center">
                                <asp:LinkButton ID="btnvoltarhoras" runat="server" CssClass="btn btn-danger "><i class="fas fa-arrow-left"></i> Voltar </asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="updatepanel1">
                                    <ProgressTemplate>
                                        <center>
                                              <img src="../img/loadred.gif" style="height:3rem;width:3rem;" />
                                                    </center>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



        </ContentTemplate>

    </asp:UpdatePanel>



</asp:Content>

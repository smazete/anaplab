﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="arrecadacaomensalidades.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="arrecadacaomensalidades" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="col-md-6">
        <div class="panel-body">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row box-title">
                                MENSALIDADES   
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-5">
                                    <asp:DropDownList runat="server" ID="cbomensalidadesmes" CssClass="form-control">
                                        <asp:ListItem />
                                        <asp:ListItem Text="Janeiro" Value="1" selected="True"   />
                                        <asp:ListItem Text="Fevereiro" Value="2" />
                                        <asp:ListItem Text="Março" Value="3"     />
                                        <asp:ListItem Text="Abril" Value="4"     />
                                        <asp:ListItem Text="Maio" Value="5"      />
                                        <asp:ListItem Text="Junho" Value="6"     />
                                        <asp:ListItem Text="Julho" Value="7"     />
                                        <asp:ListItem Text="Agosto" Value="8"    />
                                        <asp:ListItem Text="Setembro" Value="9"  />
                                        <asp:ListItem Text="Outubro" Value="10"   />
                                        <asp:ListItem Text="Novembro" Value="11" />
                                        <asp:ListItem Text="Dezembro" Value="12" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="txtanomensalidade" Text="" CssClass="form-control" />
                                </div>

                                <div class="col-lg-2"></div>

                                <div class="col-lg-2">
                                    <asp:LinkButton Text="Busca" CssClass="btn btn-primary" runat="server" ID="btnbuscarmensalidades"></asp:LinkButton>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <table class="table table-condensed">
                                    <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>PARTICIPANTES</th>
                                            <th style="width: 40px">
                                                <asp:Label ID="lblparticipantesmensalidade" Text="0" runat="server" /></th>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>FILIADOS ADIMPLENTES</td>

                                            <td><span class="badge bg-blue">
                                                <asp:Label ID="lbladiplentesmensaldiades" Text="0" runat="server" /></span></td>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>FILIADOS ISENTOS </td>

                                            <td><span class="badge bg-blue">
                                                <asp:Label ID="lblisentosmensalidades" Text="0" runat="server" /></span></td>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>FILIADOS INADIMPLENTES</td>

                                            <td><span class="badge bg-blue">
                                                <asp:Label ID="lblinadimplentesmensalidades" Text="0" runat="server" /></span></td>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>DESFILIADOS ADIMPLENTES</td>
                                            <td><span class="badge bg-light-blue">
                                                <asp:Label ID="lbldesfiliadoadimplentes" Text="0" runat="server" /></span></td>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>VALOR ARRECADADO</td>
                                            <td><span class="badge bg-blue">R$:  
                                                <asp:Label ID="vltotalmesmensalidade"  onkeyup="formataValor(this,event);" runat="server"></asp:Label>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    

    <div class="col-md-6" runat="server" visible="false" >
        <div class="panel-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row box-title">
                                EXTRATO CONSOLIDADO   
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-5">
                                    <asp:DropDownList runat="server" ID="DropDownList1" CssClass="form-control">
                                        <asp:ListItem />
                                        <asp:ListItem Text="Janeiro" Value="1" />
                                        <asp:ListItem Text="Fevereiro" Value="2" />
                                        <asp:ListItem Text="Março" Value="3" />
                                        <asp:ListItem Text="Abril" Value="4" />
                                        <asp:ListItem Text="Maio" Value="5" />
                                        <asp:ListItem Text="Junho" Value="6" />
                                        <asp:ListItem Text="Julho" Value="7" />
                                        <asp:ListItem Text="Agosto" Value="8" />
                                        <asp:ListItem Text="Setembro" Value="9" Selected="True" />
                                        <asp:ListItem Text="Outubro" Value="10" />
                                        <asp:ListItem Text="Novembro" Value="11" />
                                        <asp:ListItem Text="Dezembro" Value="12" />
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-3">
                                    <asp:TextBox runat="server" ID="TextBox1" Text="2020" CssClass="form-control" />
                                </div>

                                <div class="col-lg-2"></div>

                                <div class="col-lg-2">
                                    <asp:LinkButton Text="Busca" CssClass="btn btn-primary" runat="server" ID="LinkButton1"></asp:LinkButton>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <table class="table table-condensed">
                                    <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>PARTICIPANTES</th>
                                            <th style="width: 40px">
                                                <asp:Label ID="Label1" Text="0" runat="server" /></th>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>FILIADOS ADIMPLENTES</td>

                                            <td><span class="badge bg-blue">
                                                <asp:Label ID="Label2" Text="0" runat="server" /></span></td>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>FILIADOS ISENTOS </td>

                                            <td><span class="badge bg-blue">
                                                <asp:Label ID="Label3" Text="0" runat="server" /></span></td>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>FILIADOS INADIMPLENTES</td>

                                            <td><span class="badge bg-blue">
                                                <asp:Label ID="Label4" Text="0" runat="server" /></span></td>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>DESFILIADOS ADIMPLENTES</td>
                                            <td><span class="badge bg-light-blue">
                                                <asp:Label ID="Label5" Text="0" runat="server" /></span></td>
                                        </tr>
                                        <tr>
                                            <td>#.</td>
                                            <td>VALOR ARRECADADO</td>
                                            <td><span class="badge bg-blue">R$:  
                                                <asp:Label ID="Label6" runat="server"></asp:Label>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>

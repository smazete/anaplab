﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPrint.master" AutoEventWireup="false" CodeFile="imprimirRelPad.aspx.vb" Inherits="imprimirRelPad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" Runat="Server">
    <div class="row">
        <div class="col-lg-12 ">
        <table border="0" width ="100%">
            <tr >
            <td><center> <asp:image ImageUrl ="/img/Cevanovo.png"  runat="server" ID="imlogo" /></center></td>
                <td><center><asp:Label ID="Label1" runat="server"  text="Relatório PAD"   Font-Size="X-Large"   ></asp:Label> </center> </td>
                <td><center> Emissão:   <asp:Label ID="lblemissao" runat="server"    ></asp:Label>
            <br />
            Usuário: <asp:Label ID="lblusuario" runat="server"    ></asp:Label></center></td>
                </tr>
        </table>
            </div>
         
    </div>
    <div class="row">
        <div class="col-lg-12">
            <br />
                   <asp:GridView ID="grade" runat="server" AutoGenerateColumns="False" CellPadding="4" Width="100%" PageSize="200" ShowHeaderWhenEmpty="True" CssClass="table" ForeColor="Black" GridLines="Horizontal" AllowPaging="True">
                                                <Columns>
                                                    <asp:TemplateField  ItemStyle-HorizontalAlign="Center">

                                                        <ItemTemplate>

                                                           <%-- <span class="badge" style="background: #1E90FF">
                                                                <asp:Label ID="lblitem" runat="server"></asp:Label>
                                                            </span>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Colaborador">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lblcolaborador" runat="server" Font-Size="Large" Text='<%# Bind("matriculacolaborador") %>'></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblnomecola" runat="server" Font-Size="Large" Text='<%# Bind("nomecolaborador") %>'></asp:Label><br />
                                                              Admitido em:<asp:Label ID="lbldtadmissao" runat="server" Font-Size="Large" Text='<%# Bind("dtadmissao") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Gestor ">
                                                        <ItemTemplate>



                                                            <asp:Label ID="lblgestor" runat="server" Font-Size="Large" Text='<%# Bind("matriculagestor") %>'></asp:Label>

                                                            <asp:Label ID="lblnomegestor" runat="server" Font-Size="Large" Text='<%# Bind("nomegestor") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- <asp:BoundField DataField="nrseq" HeaderText="Lote Ceva" />
                    <asp:TemplateField >
                        <ItemTemplate >
                           
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                                                    <%--<asp:BoundField DataField="loteempresa" HeaderText="Nr Fatura" />--%>



                                                    <asp:BoundField DataField="anobase" HeaderText="Ano Base" />
                                                    <asp:BoundField DataField="descricaoresultado" HeaderText="Resultado" />
                                                    <asp:BoundField HeaderText="Objetivos" DataField="totalavaliacoes1" />
                                                    <asp:BoundField HeaderText="Meio de ano" DataField="totalavaliacoes2" />
                                                    <asp:BoundField HeaderText="Final de Ano" DataField="totalavaliacoes3" />
                                                    <asp:BoundField HeaderText="Treinamentos" DataField="totalcursos" />


                                                </Columns>


                                            </asp:GridView>

            </div> 

        </div> 
</asp:Content>


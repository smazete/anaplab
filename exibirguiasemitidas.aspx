﻿<%@ Page Language="VB" MasterPageFile="~/masterpage.master" AutoEventWireup="false" CodeFile="exibirguiasemitidas.aspx.vb" Inherits="restrito_emitir_procedimentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <script src="../js/JScriptmascara.js" type="text/javascript"></script>
    <script type="text/javascript">
        function test(cidade) {



            alert(cidade);

        }

        function irlink(linkativo) {



            window.open('/restrito/imprimir_exames.aspx?i=' + linkativo, '_blank');

        }

        function irlinkmesmpagina(linkativo) {

            window.location.href = linkativo;

            //window.open(linkativo);

        }

    </script>
    <style>
        .btngerar {
            margin-top: 25px;
            margin-left: 32px;
        }

        .btnok {
            margin-top: 25px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updatepanel3" runat="server">
        <ContentTemplate>
            <br />
            <br />
            <div class="container-fluid">


                <div class="panel panel-primary">
                    <div class="panel-heading text-center">Minhas Guias Emitidas</div>
                    <div class="panel-body">

                        <div class="container-fluid">
                            <div class="panel panel-primary">
                                <div class="panel-heading text-center"></div>
                                <div class="panel-body">
                                    <div class="form-inline">
                                        <div class="row">
                                            <div>
                                                <asp:UpdatePanel ID="updatepanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gdvAgenda" runat="server" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None">
                                                            <Columns>
                                                                <asp:BoundField DataField="nrseq" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                                <asp:BoundField DataField="serial" HeaderText="Serial" />
                                                                <asp:BoundField DataField="data" HeaderText="Data" />
                                                                <asp:BoundField DataField="hora" HeaderText="Hora" />
                                                                <asp:BoundField DataField="valortotal" HeaderText="Valor Total" />
                                                                <asp:BoundField DataField="emitido" HeaderText="Emitido" />
                                                                <asp:BoundField DataField="gerado" HeaderText="Gerado" />
                                                                <asp:TemplateField HeaderText="Ações" runat="server">
                                                                    <ItemTemplate runat="server">
                                                                        <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar">
                                                                        <i class="fas fa-eye"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div>
                                <asp:UpdatePanel ID="updatepanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gdvExames" runat="server" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sel">
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="sel" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="aenrseq" HeaderText="#" ItemStyle-CssClass="hide" HeaderStyle-CssClass="hide" />
                                                <asp:BoundField DataField="pdescricao" HeaderText="Procedimento" />
                                                <asp:BoundField DataField="cnome" HeaderText="Convênio" />
                                                <asp:BoundField DataField="aevalorproced" HeaderText="Valor Procedimento" />
                                                <asp:BoundField DataField="aevalorplano" HeaderText="Valor Plano" />
                                                <asp:BoundField DataField="aevalorcliente" HeaderText="Valor Cliente" />

                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>






</asp:Content>

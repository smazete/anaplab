﻿Imports clssessoes
Imports System.IO
Imports clsSmart
Partial Class configuracoes
    Inherits System.Web.UI.Page
    Dim tbconfig As New Data.DataTable, tb1 As New Data.DataTable
    Dim tabconfig As New clsBanco, tab1 As New clsBanco
    Dim tbcampos As New Data.DataTable
    Dim tabcampos As New clsBanco
    Private Sub configuracoes_Load(sender As Object, e As EventArgs) Handles Me.Load
        DirectCast(DirectCast(Page.Master, MasterPage).FindControl("panelprincipal"), Panel).DefaultButton = ""

        If IsPostBack Then
            Exit Sub
        End If

        Dim xtags As New clsconfig_padroes
        
        rpttags.DataSource = xtags.listatags()
        rpttags.DataBind()

        ' carregarcliente()
        ' carregarestacionamentos()

    End Sub
    Private Sub carregarcliente()

        Dim xconf As New clsconfig
        xconf.carregar()

        txtnomecliente.Text = xconf.Nomecliente
        txtenderecocliente.Text = xconf.Endereco
        txtcep.Text = xconf.Cep
        txtcnpjcliente.Text = xconf.Cnpj
        txtbairrocliente.Text = xconf.Bairro
        txtcidadecliente.Text = xconf.Cidade
        txtpaiscliente.Text = xconf.Pais
        txttelefonecliente.Text = xconf.Telefone
        txtufcliente.Text = xconf.Uf
        txtcor.Value = xconf.Corpadrao

        If File.Exists(Server.MapPath("~") & "social\" & xconf.Logo) Then
            imglogocliente.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & xconf.Logo)
            Dim xred As New clsredimensiona
            xred.Max = 11
            xred.Arquivo = (Server.MapPath("~") & "social\" & xconf.Logo)

            If xred.redimensionar Then
                imglogocliente.Style.Remove("height")
                imglogocliente.Style.Remove("width")
                imglogocliente.Style.Add("height", xred.Novaaltura & "rem")
                imglogocliente.Style.Add("width", xred.Novalargura & "rem")
            End If
        End If

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

    Private Sub btnsalvar1_Click(sender As Object, e As EventArgs) Handles btnsalvar1.Click
        Dim xclasse As New clsconfig_padroes
        xclasse.Nome = cbotipotexto.SelectedItem.Text
        xclasse.Texto = txtcorpo.Text
        xclasse.Assunto = txtassunto.Text
        If Not xclasse.salvarpadroes Then
            sm("swal({title: 'Atenção!',text: '" & xclasse.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        txtassunto.Text = ""
        txtcorpo.Text = ""
        cbotipotexto.Text = ""
        sm("swal({title: 'Opa!',text: 'Padrão salvo com sucesso !',type: 'success',confirmButtonText: 'OK'})", "swal")
    End Sub

    Private Sub imglogocliente_Click(sender As Object, e As ImageClickEventArgs) Handles imglogocliente.Click
        Session("chave") = ""
        Session("origem") = "empresa"
        Response.Redirect("/loadimage.aspx")
    End Sub

    Private Sub btncarregarlogo_Click(sender As Object, e As EventArgs) Handles btncarregarlogo.Click
        Session("chave") = ""
        Session("origem") = "empresa"
        Response.Redirect("/loadimage.aspx")
    End Sub

    Private Sub txtcep_TextChanged(sender As Object, e As EventArgs) Handles txtcep.TextChanged
        Dim xcep As New clsCEP
        xcep.Cep = txtcep.Text

        If Not xcep.buscarcep() Then
            sm("swal({title: 'Attention!',text:'" & xcep.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        txtenderecocliente.Text = xcep.Endereco
        txtcidadecliente.Text = xcep.Cidade
        txtbairrocliente.Text = xcep.Bairro
        txtufcliente.Text = xcep.Uf
        txtpaiscliente.Text = xcep.Pais
    End Sub

    Private Sub btnsalvarcliente_Click(sender As Object, e As EventArgs) Handles btnsalvarcliente.Click
        Dim xconf As New clsconfig
        xconf.Nomecliente = txtnomecliente.Text
        xconf.Endereco = txtenderecocliente.Text
        xconf.Cep = txtcep.Text
        xconf.Cnpj = txtcnpjcliente.Text
        xconf.Bairro = txtbairrocliente.Text
        xconf.Cidade = txtcidadecliente.Text
        xconf.Pais = txtpaiscliente.Text
        xconf.Telefone = txttelefonecliente.Text
        xconf.Corpadrao = txtcor.Value
        If Not xconf.salvar Then
            sm("swal({title: 'Attention!',text:'" & xconf.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        sm("swal({title: 'Very Good!',text:'" & xconf.Mensagemerro & "',type: 'success',confirmButtonText: 'OK'})", "swal")
    End Sub

    Private Sub cbotipotexto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbotipotexto.SelectedIndexChanged
        If cbotipotexto.Text = "" Then
            Exit Sub
        End If
        Dim xclasse As New clsconfig_padroes
        xclasse.Nome = cbotipotexto.SelectedItem.Text
        xclasse.carregar()
        txtassunto.Text = xclasse.Assunto
        txtcorpo.Text = xclasse.Texto
    End Sub

End Class

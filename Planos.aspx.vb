﻿Imports System.Data
Imports clsSmart
Partial Class restrito_planos
    Inherits System.Web.UI.Page

    Public Function verificaCampo(input As String, campo As String, erro As String) As String
        If input = "" Then
            If erro <> "" Then
                erro &= ",<br>"
            Else
                erro &= "<br>"
            End If
            erro &= campo
        End If
        Return erro
    End Function

    Private Function converteDatatableToString(datatable As DataTable, Optional campo As String = "nrseq") As String
        Dim palavra As String = ""

        For x As Integer = 0 To datatable.Rows.Count - 1
            If x > 0 Then
                palavra &= ", "
            End If
            palavra &= datatable.Rows(x)(campo)
        Next

        Return palavra
    End Function

    Public Function geraCodigo() As String
        Dim protocolo As String = DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString
        Return protocolo
    End Function

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub habilitar(Optional habilita As Boolean = True)
        btnNovo.Enabled = Not habilita
        txtdescricao.enabled = habilita
        txtidentificador.enabled = habilita
        cboPadraoContrato.Enabled = habilita
        txtnomenorecibo.Enabled = habilita
        cboBancoPadrao.Enabled = habilita
        cboCarteiraPadrao.Enabled = habilita
        txtcnpj.Enabled = habilita
        txttelefone.Enabled = habilita
        txtendereco.Enabled = habilita
        txtcidade.Enabled = habilita
        txtuf.Enabled = habilita
        chkIgnorarSaltoDeMatricula.Enabled = habilita
        chkUsarComoPlanoPadrao.Enabled = habilita
        txtTextoCarteirinhaVerso.Enabled = habilita

        btnSalvar.Enabled = habilita
    End Sub

    Private Sub limpar()
        hdnnrseq.Value = ""
        txtcodigo.Text = ""
        txtdescricao.Text = ""
        txtidentificador.Text = ""
        txtnomenorecibo.Text = ""

        Try
            cboPadraoContrato.SelectedIndex = 0
        Catch ex As Exception
        End Try
        Try
            cboBancoPadrao.SelectedValue = 0
        Catch ex As Exception
        End Try
        Try
            cboCarteiraPadrao.SelectedValue = 0
        Catch ex As Exception
        End Try

        txtcnpj.Text = ""
        txttelefone.Text = ""
        txtendereco.Text = ""
        txtcidade.Text = ""
        txtuf.Text = ""
        chkIgnorarSaltoDeMatricula.Checked = False
        chkUsarComoPlanoPadrao.Checked = False

        txtTextoCarteirinhaVerso.Text = ""
    End Sub

    Public Sub carregagradeplanos(Optional exibirinativos As Boolean = False)
        'Dim xempresas As New clsEmpresas
        Dim xplanos As New clsPlanos

        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome
        xplanos.Ativo = Not chkexibirinativos.Checked

        If Not xplanos.consultartodos() Then
            sm("swal({title: 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If


        gradeplanos.DataSource = xplanos.Table
        gradeplanos.DataBind()

    End Sub

    Public Sub carregarNoFormulario(nrseq As String)

        Dim xplanos As New clsPlanos


        xplanos.Descricao = txtdescricao.Text

        xplanos.Ativo = Not chkexibirinativos.Checked

        If Not xplanos.procurar(True, True) Then
            sm("swal({title: 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        If xplanos.Contador > 0 Then
            hdnnrseq.Value = xplanos.Nrseq
            txtcodigo.Text = xplanos.Nrseq
            txtdescricao.Text = xplanos.Descricao
            txtidentificador.Text = xplanos.Identificador
            txtnomenorecibo.Text = xplanos.Nomerecibo

            Try
                cboPadraoContrato.SelectedIndex = xplanos.Padraocontrato
            Catch ex As Exception
            End Try
            Try
                cboBancoPadrao.SelectedValue = xplanos.Bancopadrao
            Catch ex As Exception
            End Try
            Try
                cboCarteiraPadrao.SelectedValue = xplanos.Carteirapadrao
            Catch ex As Exception
            End Try

            txtcnpj.Text = xplanos.Cnpj
            txttelefone.Text = xplanos.Telcontato
            txtendereco.Text = xplanos.Endereco
            txtcidade.Text = xplanos.Cidade
            txtuf.Text = xplanos.Uf
            chkIgnorarSaltoDeMatricula.Checked = logico(xplanos.Ignorarsaltodematricula)
            chkUsarComoPlanoPadrao.Checked = logico(xplanos.Planopadrao)
            txtTextoCarteirinhaVerso.Text = xplanos.Textocarteirinhaverso

            sm("$('html, body').animate({ scrollTop: 0 }, 1200);", "scrollTop")
        End If
    End Sub

    Private Sub restrito_gerenciacaixa_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            habilitar(False)
        End If

        carregagradeplanos()


        sm("formataTelefoneMask()", "formataTelefoneMask")
        sm("formataCnpjCpf()", "formataCnpjCpf")
    End Sub

    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click
        Dim xplanos As New clsPlanos
        If hdnnrseq.Value = "" Then
            hdnnrseq.Value = txtcodigo.Text
        End If
        xplanos.Nrseq = hdnnrseq.Value
        xplanos.Descricao = txtdescricao.Text
        xplanos.Identificador = txtidentificador.Text
        xplanos.Padraocontrato = cboPadraoContrato.SelectedValue
        xplanos.Nomerecibo = txtnomenorecibo.Text
        xplanos.Bancopadrao = cboBancoPadrao.SelectedValue
        xplanos.Carteirapadrao = cboCarteiraPadrao.SelectedValue
        xplanos.Cnpj = txtcnpj.Text
        xplanos.Telcontato = txttelefone.Text
        xplanos.Endereco = txtendereco.Text
        xplanos.Cidade = txtcidade.Text
        xplanos.Uf = txtuf.Text
        xplanos.Ignorarsaltodematricula = chkIgnorarSaltoDeMatricula.Checked
        xplanos.Planopadrao = chkUsarComoPlanoPadrao.Checked
        xplanos.Textocarteirinhaverso = txtTextoCarteirinhaVerso.Text

        If Not xplanos.salvar() Then
            sm("swal({title: 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        carregagradeplanos()

        habilitar(False)
        limpar()

    End Sub

    Private Sub btnNovo_Click(sender As Object, e As EventArgs) Handles btnNovo.Click
        Dim xplanos As New clsPlanos
        Dim xempresas As New ClsEmpresas

        If Not xplanos.novo() Then
            sm("swal({title: 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        hdnnrseq.Value = xplanos.Nrseq
        txtcodigo.Text = xplanos.Nrseq
        habilitar(True)

    End Sub

    Private Sub gradeplanos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeplanos.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeplanos.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnnrseqgrade")
        If e.CommandName = "editar" Then
            carregarNoFormulario(nrseq.Value)
            habilitar(True)
            txtidentificador.Enabled = False
        End If
        If e.CommandName = "deletar" Then
            Dim xplanos As New clsPlanos

            xplanos.Nrseq = nrseq.Value

            If Not xplanos.excluir() Then
                sm("swal({title: 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            carregagradeplanos()
        End If
    End Sub

    Private Sub gradeplanos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeplanos.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub

        Dim icoexcluir As HtmlGenericControl = e.Row.FindControl("icogradeexcluir")
        Dim btnexcluir As LinkButton = e.Row.FindControl("btnexcluir")
        If e.Row.Cells(3).Text = "1" Then
            e.Row.Cells(3).Text = "Sim"
            icoexcluir.Attributes.Remove("class")
            icoexcluir.Attributes.Add("class", "fa fa-trash-o")
        Else
            e.Row.Cells(3).Text = "Não"
            icoexcluir.Attributes.Remove("class")
            icoexcluir.Attributes.Add("class", "fa fa-undo")
        End If

        If e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Sim"
        Else
            e.Row.Cells(4).Text = "Não"
        End If


    End Sub



    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click
        limpar()
        habilitar(False)
        btnNovo.Enabled = True
        btnSalvar.Enabled = False

    End Sub

    Private Sub btnprocurar_Click(sender As Object, e As EventArgs) Handles btnprocurar.Click
        Dim xplanos As New clsPlanos

        'If cboprocurar.SelectedValue = "descricao" Then

        xplanos.Descricao = txtprocurar.Text
        xplanos.Ativo = Not chkexibirinativos.Checked


        If Not xplanos.procurar() Then
            sm("swal({title 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        gradeplanos.DataSource = xplanos.Table
        gradeplanos.DataBind()

        'End If
        'If cboprocurar.SelectedValue = "cnpj" Then
        '    xplanos.Cnpj = txtprocurar.Text

        '    If Not xplanos.procurar(CNPJ:=True) Then
        '        sm("swal({title: 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '        Exit Sub
        '    End If

        '    gradeplanos.DataSource = xplanos.Table
        '    gradeplanos.DataBind()


        'End If
    End Sub
End Class

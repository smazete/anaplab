﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="contato.aspx.vb" MasterPageFile="~/MasterPage2.master"   Inherits="contato" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    

    <%-- sweetalert2 monitorando qual vou usar  --%>

    <%-- sweetalert2 monitorando qual vou usar  --%>
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
    <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="/dist/js/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>

    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />

    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/JScriptmascara.js" type="text/javascript"></script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

       <div class="card" style="padding-top:15px;">
        <div class="card-header">
            <br />
            <center><h3><asp:label text="Entre em contato." runat="server" /></h3></center>
        </div>
    
           <div class="card-body">
           <br />
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <div class="row">
                            <b><p style="color:blue; font-size:24px;" >Preencha todos os campos.</p></b>
                                    </div>
                               <div class="row">
                               <b>     <asp:Label runat="server" >NOME COMPLETO</asp:label><span style="color:red">*</span><br /></b>
                                    <asp:TextBox CssClass="form-control" runat="server" id="txtnome"/>
                                </div>
                                <div class="row">
                                  <b>  <asp:Label runat="server" >ASSUNTO</asp:label><span style="color:red">*</span><br /></b>
                                    <asp:TextBox CssClass="form-control" runat="server" id="txtassunto" />
                                </div>
                                <div class="row">
                                <b><asp:Label runat="server" >E-MAIL</asp:label><span style="color:red">*</span><br /></b>
                                    <asp:TextBox CssClass="form-control" runat="server" id="txtemail"/>
                                </div>
                                <div class="row">
                               <b><asp:Label runat="server" >MENSAGEM</asp:label><span style="color:red">*</span><br /></b>
                                    <asp:TextBox  style="  height: 136px;" CssClass="form-control" runat="server" id="txtdescricao" TextMode="MultiLine" />
                                </div>
                                <br />
                                <div class="row"><center>
                                <asp:LinkButton cssclass="btn btn-primary" Text="ENVIAR" runat="server"  id="btnenviar"/>
                                 </center>
                                </div>
                                <br />
                                <br />
                            </div>

                            <div class="col-lg-3"></div>
        
       
       </div>
       </div>


</asp:Content>

﻿Imports System.IO
Imports clsSmart
Imports System.Windows
Imports System.IO.Compression


Partial Class documentos
    Inherits System.Web.UI.Page


    Private Sub documentos_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Exit Sub
        End If

        carregarpt()
    End Sub


    Public Sub carregarpt()
        Dim xBalanco As New clsbalancete
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbdocumentoanaplab where ativo = true ")

        gradebalanco.DataSource = tb1
        gradebalanco.DataBind()

    End Sub


    Private Sub btnmostrabalancete_Click(sender As Object, e As EventArgs) Handles btnmostrabalancete.Click



        '      If divbalancete.Visible = False Then
        '
        '       divbalancete.Visible = True
        '      Else
        '      divbalancete.Visible = False
        '     End If
    End Sub



    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub gradebalanco_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradebalanco.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradebalanco.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim arquivo As HiddenField = row.FindControl("hdarquivo")

        If e.CommandName = "baixar" Then

            Dim wcarquivo As String = arquivo.Value
            Response.ContentType = "application/octet-stream"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment; filename=" & wcarquivo)
            Response.TransmitFile(Server.MapPath("~\balancetes\") & wcarquivo)
            Response.End()

        Else
            sm("swal({title: 'Atenção!',text: 'Não foi possivel Baixar o Documento',type: 'error',confirmButtonText: 'OK'})", "swal")
        End If
    End Sub
End Class

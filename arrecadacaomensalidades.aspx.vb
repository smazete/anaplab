﻿Imports clssessoes
Imports clsSmart
Imports System.Web.UI.DataVisualization.Charting



Partial Class arrecadacaomensalidades
    Inherits System.Web.UI.Page

    Public Sub carregaarrecadaçãomensalidades(Optional exibirinativos As Boolean = False)
        Dim joiasmes As String = cbomensalidadesmes.SelectedValue
        Dim joiasano As String = txtanomensalidade.Text

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco


        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where tbmensalidades.ano =  '" & joiasano & "' and tbmensalidades.mes = '" & joiasmes & "'   and tbAssociados.ativo = 1 group by nrseqcliente  ")

        lblparticipantesmensalidade.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where tbmensalidades.ano =  '" & joiasano & "' and tbmensalidades.mes = '" & joiasmes & "'   and tbAssociados.ativo = 1 and tbmensalidades.statuspg = 1 group by nrseqcliente  ")

        lbladiplentesmensaldiades.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where tbmensalidades.ano =  '" & joiasano & "' and tbmensalidades.mes = '" & joiasmes & "'  and tbmensalidades.statuspg = 3 and tbAssociados.ativo = 1   group by nrseqcliente ")

        lblisentosmensalidades.Text = tb1.Rows.Count.ToString()


        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where tbmensalidades.ano =  '" & joiasano & "' and tbmensalidades.mes = '" & joiasmes & "'  and tbmensalidades.statuspg <> 1 and tbmensalidades.statuspg <> 3 and tbAssociados.ativo = 1  group by nrseqcliente ")

        lblinadimplentesmensalidades.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where tbmensalidades.ano =  '" & joiasano & "' and tbmensalidades.mes = '" & joiasmes & "'  and tbmensalidades.statuspg = 1 and tbAssociados.ativo = 0 group by nrseqcliente  ")

        lbldesfiliadoadimplentes.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where tbmensalidades.ano =  '" & joiasano & "' and tbmensalidades.mes = '" & joiasmes & "'  and tbmensalidades.statuspg = 1 and tbAssociados.ativo = 0 group by nrseqcliente")


        vltotalmesmensalidade.Text = FormatCurrency(lbladiplentesmensaldiades.Text * 20)

    End Sub

    Private Sub btnbuscarmensalidades_Click(sender As Object, e As EventArgs) Handles btnbuscarmensalidades.Click
        carregaarrecadaçãomensalidades()
    End Sub

    Private Sub arrecadacaomensalidades_Load(sender As Object, e As EventArgs) Handles Me.Load
        If txtanomensalidade.Text = "" Then

            cbomensalidadesmes.SelectedValue = Date.Now.Month
            txtanomensalidade.Text = DateTime.Now.ToString("yyyy")
        Else

        End If
        carregaarrecadaçãomensalidades()
    End Sub


End Class

﻿<%@ Page Title="" Language="VB" MasterPageFile="~/arearestrita/Principal.master" AutoEventWireup="false" CodeFile="EnvioAniversariante.aspx.vb" Inherits="arearestrita_EnvioAniversariante" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .style6
    {
        color: #3333FF;
    }
        .MsoNormal
        {
            width: 560px;
            height: 17px;
            margin-bottom: 9px;
            margin-right: 38px;
        }
        </style>
    <link href="../style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div __designer:mapid="36" style="width: 802px">
        <div style="border: 2px dotted #000080; color: #FFFFFF; background-color: #333399; height: 70px;" 
            __designer:mapid="37">
            
                <h3 __designer:mapid="38">Enviar Mensagem aos Associados</h3>
                <p __designer:mapid="39">&nbsp;</p>
        </div>
            <asp:Button ID="btnvisualizar" runat="server" Text="Visualizar mensagem" 
    Width="293px" BackColor="#333399" ForeColor="White" Height="43px" 
            CssClass="visualizaimagem" 
            style="color: #FFFFFF; background-color: #333399; margin-bottom: 0px;" />
        <br __designer:mapid="41" />
        <br __designer:mapid="42" />
        <asp:Panel ID="paneldata" runat="server" Height="50px" 
            Width="471px">
            <span id="D" >Data do Aniversario<br />
            <asp:TextBox ID="txtdtaniversario" runat="server"></asp:TextBox>
            &nbsp; Informe o dia do aniversario formato <strong>dia/mes/ano</strong></span></asp:Panel>
        <span id="D0" class="style6" __designer:mapid="221">
        <br __designer:mapid="44" />
            <asp:Button ID="btncarregar" runat="server" Text="Carregar" 
    Width="185px" BackColor="#333399" ForeColor="White" Height="42px" />
        &nbsp;<asp:Label ID="lblinfocarregar" runat="server" 
            Text="Clique em Carregar para mostrar todos os associados com o perfil informado" 
            Visible="False"></asp:Label>
        <br __designer:mapid="47" />
        </span>
        <asp:CheckBox ID="chkalterarnome" runat="server" 
            Text="incluir nome e matricula na mensagem" Checked="True" Visible="False" />
        <br __designer:mapid="48" />
        <asp:GridView ID="Grade" runat="server" 
            BorderColor="#CCCCCC" BorderStyle="Double" CaptionAlign="Left" ForeColor="Black" 
            Height="16px"
           
            style="font-family: arial; text-align: left" Width="792px" Font-Bold="True">
            <HeaderStyle HorizontalAlign="Left" />
            <Columns>
                <asp:TemplateField HeaderText="Sel">
                    <EditItemTemplate>
                        <asp:TextBox ID="text" runat="server" width="10" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="sel" runat="server" Height="21px" width="48px" />
                        &nbsp;
                    </ItemTemplate>
                    <ControlStyle BorderStyle="Groove" Width="25px" />
                    <FooterStyle Width="10px" />
                    <HeaderStyle Width="20px" />
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BorderStyle="None" />
            <HeaderStyle BackColor="Control" Font-Bold="True" Font-Italic="True" 
                ForeColor="#407BD2" Wrap="True" />
        </asp:GridView>
            <asp:Button ID="btnEnviaEmail" Visible ="false"  runat="server" style="color: #FFFFFF; background-color: #333399; margin-bottom: 1px;" 
    Text="EnviarMensagem" Width="163px" Height="42px" CssClass="botaomensagem" />
            <asp:Button ID="btnrelatorio" runat="server" CssClass="botaomensagem" 
                Height="42px" style="color: #FFFFFF; background-color: #333399;" 
                Text="Mostrar Relatorio" Visible="False" Width="163px" />
        <br />
            <asp:Label ID="lblinfo" runat="server" 
                style="font-family: Arial, Helvetica, sans-serif; font-size: xx-large; color: #0000CC" 
                Text="Mensagem Enviada Com Sucesso." Visible="False"></asp:Label>
        <br __designer:mapid="4a" />
    <br __designer:mapid="4b" />
<asp:TextBox ID="txtDe" runat="server" Height="22px" Width="279px" Visible="False">atendimento@anaplab.com.br</asp:TextBox>
    <br __designer:mapid="4d" />
<asp:TextBox ID="txtPara" runat="server" Height="22px" Width="279px" Visible="False">julio.smartcode@hotmail.com</asp:TextBox>
<asp:TextBox ID="txtBcc" runat="server" Height="22px" Width="279px" Visible="False"></asp:TextBox>
<asp:TextBox ID="txtCc" runat="server" Height="22px" Width="279px" Visible="False"></asp:TextBox>
<asp:TextBox ID="TxtAssunto" runat="server" Width="279px" Visible="False">teste</asp:TextBox>
        <br __designer:mapid="52" />
        <asp:Panel ID="panelenvio" runat="server" Width="559px">
            &nbsp;&nbsp;<asp:CheckBox ID="chkemail" runat="server" Text="Enviar no Email" Checked="True" Visible="False" />
            <br />
            &nbsp;&nbsp;<asp:CheckBox ID="chkenviaremailcopia" runat="server" 
                Text="Enviar cópia para o email do Atendimento" Checked="True" Visible="False" />
            <br />
        </asp:Panel>
        <br __designer:mapid="55" />
&nbsp;&nbsp;<asp:Panel ID="panelrelatorio" runat="server" 
            style="color: #333399; background-color: #FFFFCC" Width="729px" 
            Visible="False">
            <br />
            Mesagem:<asp:Label ID="lblmensagem" runat="server" Text="Label"></asp:Label>
            <br />
            Data de envio:<asp:Label ID="lbldataenvio" runat="server" Text="lbldataenvio"></asp:Label>
            <br />
            Total enviados:<asp:Label ID="lbltotalenviado" runat="server" Text="Label"></asp:Label>
            <br />
         Total de Erros:<asp:Label ID="Label2" runat="server" 
                Text="Label"></asp:Label>          
            <br />

            <br />
            <asp:Button ID="Button1" runat="server" Height="45px" 
                style="color: #0000CC; background-color: #FFCC99" 
                Text="Mostrar Associado com erro:" Width="226px" />
            <br />
            <br />
            <br />
            <asp:ListBox ID="listerro" runat="server" Height="155px" Visible="False" 
                Width="702px"></asp:ListBox>
            <br />
            <br />
            
        </asp:Panel>
        <br __designer:mapid="57" />
<br __designer:mapid="58" />
<asp:Label ID="lblmsg" runat="server" Font-Bold="True" ForeColor="Red" 
    Height="38px" Width="393px"></asp:Label>
<br __designer:mapid="5a" />
        <asp:Label ID="lblcodmensagem" runat="server" Text="codigomensagem" 
            Visible="False"></asp:Label>
        <br __designer:mapid="5c" />
        <asp:TextBox ID="txtmensagem" Text ="&lt;div style =&quot;text-align:center &quot; &gt;&lt;span style =&quot;color: #0000CC;&quot;&gt;Bom dia [NOME], &lt;/br&gt; &lt;/br&gt; &lt;/span&gt;&lt;br /&gt;&amp;nbsp;&lt;div style=&quot;text-align: center;&quot;&gt;&lt;img alt=&quot;&quot; class=&quot;style1&quot;  src=&quot;http://www.anaplab.com.br/img/Felizaniversarioanaplab.png&quot; /&gt;&lt;/div&gt;&lt;/div&gt; " runat="server" TextMode="MultiLine" 
            Visible="False" Height="48px" Width="762px"></asp:TextBox>
<br __designer:mapid="5e" />


    </div>


    <br />


</asp:Content>


﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage2.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="/dist/js/sweetalert2.all.min.js"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- Hero Header --><header class="hero-header" style="background-image: url('img/oldhap.jpg'); ">
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in ">Seja bem-vindo a Anaplab</div>
               <%-- <div class="intro-heading">Frase de impacto </div>--%>
               <a  href="loginassociados.aspx" id="btnlogar" class="tn btn-primary btn-lg">ENTRAR</a>               
            </div>
        </div>
    </header>
    <!-- Call to Action -->
    <%--    <div class="sec-alternative" style="background-image: url(img/odd_background.png)">
        <div class="container cta">
            <div class="row">
                <div class="col-md-8">
                    <p><h7 style="font-size: 30px">SEJA NOSSO <span>ASSOCIADO!</span><h7>
                    </p>
                </div>
                <div class="col-md-3">
                    <div class="cta-button">
                        <asp:LinkButton   runat="server" id="btnassociar1" class="btn btn-primary"><i class="fa fa-users"></i> ASSOCIAR-SE </asp:LinkButton> 
                    </div>
                </div>

                <div class="col-md-1"><span><a href="#"> <i class="far fa-envelope"></i> Email contato@anaplab.com.br </a></span>
                </div>
            </div>
        </div>
    </div>--%>
    <br />
    <br />
    <%-- Documento anaplab informa --%>






    <br />
      <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 content wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
            <asp:Panel runat="server">
                <center><asp:Label runat="server"  style="font-size: 36px; fon"><b><h3><u>CASSI – Redução das coparticipações e revisão da LIMACA. <br />
                    O seu apoio é muito importante. Apoie esta causa.<br />
Assine o abaixo-assinado.<br />
<a href="https://peticaopublica.com.br/?pi=BR118066" text-decoration-color: blue;>peticaopublica.com.br</a>
</u></h3></b></asp:Label></center>

            </asp:Panel><br />
            <p>Pessoal, Bom dia.</br>
           Ontem a noite foi lançado o abaixo-assinado dirigido ao Conselho Deliberativo da CASSI para redução das coparticipações e reinclusão na LIMACA dos mais de 1800 medicamentos retirados na última revisão.<br />       
           627 colegas já aderiram ao abaixo-assinado por entender, assim como nós, que o superavit da CASSI do ano de 2020, R$ 1.141 milhões, permite que essa solicitação seja atendida, SEM comprometer a situação Financeira da CASSI, proporcionando um alívio nas despesas de muitos colegas que, por seu estado de saúde, necessitam realizar mais consultas e adquirir medicamentos.</p>
            <p>Acesse o link abaixo, leia o abaixo-assinado e, se concordar com essa ideia, junte-se a nós nessa justa reinvindicação.</p>
            <p><b>Atenção: ao assinar o abaixo-assinado você receberá um e-mail para confirmação da sua assinatura.</b></p>
            <p>Estamos juntos.</p>
            <p><b>Fausto Pereira <br />
            Carlos Vasco  <br />
           Benedito Resende - Bené  <br />
          Adelmo Vianna  <br />
         Marta Tramm  <br />
           Beth Bueno </b></p>
            
            <p>CASSI - Redução das coparticipações e revisão da LIMACA. </p>

            <a href="https://peticaopublica.com.br/?pi=BR118066"> peticaopublica.com.br </a>
            <br />
            <br />
            <br />
            <br />
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 content wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
            <asp:Panel runat="server">
                <center><asp:Label runat="server"  style="font-size: 36px; text-decoration-color: black"><b><h3>COMUNICADO ANAPLAB.</h3></b></asp:Label></center>

            </asp:Panel>

            <p>Comunicamos aos nossos associados que devido a considerável número de inadimplência nas mensalidades, única fonte de renda da associação, implantamos o bloqueio de acesso ao autoatendimento para todos os associados que não estão em dia com suas obrigações sociais.<br>
                Informamos que, uma vez regularizada a pendência, o acesso se dará automaticamente, sem necessidade de qualquer ajuste.<br>
                Pedimos gentilmente a compreensão de todos no sentido de manter suas mensalidades em dia pois as nossas despesas judiciais são elevadas.<br>
                Qualquer esclarecimento adicional poderá ser obtido através de nossos endereços eletrônicos, a saber:<br />
            </p>
            <br>
            <a>atendimento@anaplab.com.br </a>
            <br />
            <a>tesouraria@anaplab.com.br</a>
            <br />
            <p>atenciosamente,</p>

            <p></p>
            <%--<p><img src="img/assinatura0304.png" height="120" width="210"  alt="Minha Figura"></p>--%>
            <p>Presidente – ANAPLAB.</p>
            <p></p>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <!-- Intro -->

    <%--    <section class="page-section" id="intro">
        <div class="container intro">
            <div class="row margin-bottom-50">
                <section class="page-section-no-padding">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="container col-md-6">
                                <div class="row">
                                    <div class="col-md-7 col-md-offset-4 spotlight-container">
                                        <h2 class="title-section"><span class="title-regular">Tenha um</span> Objetivo <span class="title-regular">na vida e lute sempre para alcançá-lo!</span></h2>
                                        <hr class="title-underline" />
                                        <p>
                                            A Anaplab junto com sua equipe busca atender de maneira...
                                        </p>
                                        <a href="maquina3.html" class="btn btn-primary">Saiba Mais</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 spotlight-img-cont" style="background-image: url(img/confianca.jpg)"> </div>
                        </div>
                    </div>
                </section>
                </div>
                </div>
        </section>
    --%>
    <!-- Full Spotlight right
                <section class="page-section-no-padding">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="container col-md-6 col-md-push-6">
                                <div class="row">
                                    <div class="col-md-7 col-md-offset-1 spotlight-container">
                                        <h2 class="title-section"><span class="title-regular">FULL RIGHT</span><br/>SPOTLIGHT</h2>
                                        <hr class="title-underline" />
                                        <p>
                                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                                        </p>
                                        <a href="maquina4.html" class="btn btn-primary">More Information</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-pull-6 spotlight-img-cont" style="background-image: url(img/machine4.jpg)"> </div>
                        </div>
                    </div>
                </section>
-->




    <!-- Our Services -->
    <br>
    <section class="page-section " style="padding-bottom: 10px;">
        <div class="container ">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title-section"><span class="title-regular">Equipe <span>ANAPLAB!</span> Atuando com transparência.</h2>
                    <hr class="title-underline" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 ">
                    <div class="col-xs-2 box-icon tex">
                        <div>
                            <img src="img/a2.png" height="40" width="40" alt="Minha Figura"></div>
                    </div>
                    <div class="col-xs-10">
                        <h4>Transparência</h4>
                        <h5></h5>
                    </div>
                    <div class="col-md-12">
                        <p>
                            Todos os documentos da ANAPLAB estão disponíveis para download.
                        </p>
                    </div>
                </div>

                <div class="col-md-3 ">
                    <div class="col-xs-2 box-icon">
                        <div>
                            <img src="img/a1.png" height="40" width="40" alt="Minha Figura"></div>
                    </div>
                    <div class="col-xs-10">
                        <h4>ACESSIBILIDADE</h4>
                        <h5></h5>
                    </div>
                    <div class="col-md-12">
                        <p>
                            Os sites são corretamente concebidos, desenvolvidos e editados, todos os usuários podem ter igual acesso à informação e funcionalidade
                        </p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="col-xs-2 box-icon">
                        <div>
                            <img src="img/a4.png" height="40" width="40" alt="Minha Figura"></div>
                    </div>
                    <div class="col-xs-10">
                        <h4>SEGURANÇA</h4>
                        <h5></h5>
                    </div>
                    <div class="col-xs-12">
                        <p>
                            A anaplab protege sua informações seguindo o decreto  10.046/19  Lei geral de proteção de dados
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Our Clients -->
    <section class="page-section" style="padding-bottom: 10px;">
        <div class="container">
            <div class="row">
                <h2 class="title-section"><span class="title-regular">Conheça alguns dos nossos <span style="color: #83dc48">colaboradores</span>. </span></h2>
                <hr class="title-underline" />


            </div>
            <div class="row">
                <p>
                    <h3>Uma das principais metas da ANAPLAB é primar pela GESTÃO COMPARTILHADA e pela TRANSPARÊNCIA absoluta dos seus atos administrativos.</h3>
                </p>
                <p>
                    <h3>Em respeito aos nossos associados divulgamos, abaixo, a relação de prestadores de serviços contratados. Nenhum dos donos das empresas contratadas possui relação de parentesco, ou qualquer tipo de influência, entre os diretores da ANAPLAB. Fechamos as parcerias levando-se em consideração apenas a qualidade dos serviços prestados e o menor preço.</h3>
                </p>
            </div>
            <div class="col-md-8 text-center">
                <div class="row">
                    <div class="col-md-4">
                        <br>
                        <br>
                        <img src="img/grl/logocliente3.png" alt="" class="img-responsive" />
                    </div>

                    <div class="col-md-4">
                        <br>
                        <br>
                        <img src="img/grl/logocliente1.png" alt="" class="img-responsive" />
                    </div>
                </div>
            </div>
            <br />
            <br />
            <p>
                <h1>
                    <br />
                    "EQUIPE ANAPLAB"</h1>
            </p>
        </div>

        <br />
    </section>




</asp:Content>


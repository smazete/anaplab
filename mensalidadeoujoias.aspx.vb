﻿
Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoesautoatendimento
Imports System.IO
Imports System.Data

Partial Class mensalidadeoujoias
    Inherits System.Web.UI.Page


    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Private Sub mensalidadeoujoias_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("idassociado") = "" Then

            Response.Redirect("loginassociados.aspx")
        End If

        If IsPostBack Then

            Exit Sub
        End If

        mensalidades()
        verificajoias()
        verificajoias2()
    End Sub

    'acoes de botoes para mensalidade ou joias











    Private Sub verificajoias()

        Dim count As Integer = 0

        tb1 = tab1.conectar("select * from tbmensalidade_joias where nrsequser = '" & Session("idassociado") & "' order by ano, mes asc ")

        If tb1.Rows.Count > 0 Then
            divjoiasmenu.Visible = True
        Else

            divjoiasmenu.Visible = False

        End If

        txtjoias.Text = tb1.Rows.Count.ToString()
    End Sub


    Private Sub verificajoias2()

        Dim count As Integer = 0

        tb1 = tab1.conectar("select * from tbjoias_2 where nrsequser = '" & Session("idassociado") & "' order by ano, mes asc ")

        If tb1.Rows.Count > 0 Then
            divjoias2menu.Visible = True
        Else

            divjoias2menu.Visible = False

        End If

        txtjoias2.Text = tb1.Rows.Count.ToString()
    End Sub


    Private Sub mensalidades()
        Dim count As Integer

        tb1 = tab1.conectar("select * from tbmensalidades where nrseqcliente = '" & Session("idassociado") & "' ")

        txtmensalidades.Text = tb1.Rows.Count.ToString()
    End Sub




    Public Sub carregajoias2(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.nrsequser = " & Session("idassociado") & "")



        gradejoias2.DataSource = tb1
        gradejoias2.DataBind()

    End Sub


    Public Sub carregajoias(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.nrsequser = " & Session("idassociado") & "")



        gradejoias.DataSource = tb1
        gradejoias.DataBind()

    End Sub


    Private Sub grademensalidades_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grademensalidades.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(3).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightSalmon
        Else
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If
        'controle mes 
        If e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Janeiro"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Fevereiro"
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.Cells(4).Text = "Março"
        ElseIf e.Row.Cells(4).Text = "4" Then
            e.Row.Cells(4).Text = "Abril"
        ElseIf e.Row.Cells(4).Text = "5" Then
            e.Row.Cells(4).Text = "Maio"
        ElseIf e.Row.Cells(4).Text = "6" Then
            e.Row.Cells(4).Text = "Junho"
        ElseIf e.Row.Cells(4).Text = "7" Then
            e.Row.Cells(4).Text = "Julho"
        ElseIf e.Row.Cells(4).Text = "8" Then
            e.Row.Cells(4).Text = "Agosto"
        ElseIf e.Row.Cells(4).Text = "9" Then
            e.Row.Cells(4).Text = "Setembro"
        ElseIf e.Row.Cells(4).Text = "10" Then
            e.Row.Cells(4).Text = "Outubro"
        ElseIf e.Row.Cells(4).Text = "11" Then
            e.Row.Cells(4).Text = "Novembro"
        ElseIf e.Row.Cells(4).Text = "12" Then
            e.Row.Cells(4).Text = "Dezembro"

        End If


        If e.Row.Cells(3).Text = "0" Then
            e.Row.Cells(3).Text = "Aberto"
        ElseIf e.Row.Cells(3).Text = "1" Then
            e.Row.Cells(3).Text = "Pago"
        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.Cells(3).Text = "Estornado"
        Else
            e.Row.Cells(3).Text = ""
        End If




        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If


    End Sub


    Public Sub carregamensalidades(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim xMensalidades As New clsmensalidades

        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where tbmensalidades.nrseqcliente = " & Session("idassociado") & " order by tbmensalidades.ano, tbmensalidades.mes asc")

        grademensalidades.DataSource = tb1
        grademensalidades.DataBind()

    End Sub


    Private Sub gradejoias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradejoias.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(3).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightSalmon
        Else
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If
        'controle mes 
        If e.Row.Cells(6).Text = "1" Then
            e.Row.Cells(6).Text = "Janeiro"
        ElseIf e.Row.Cells(6).Text = "2" Then
            e.Row.Cells(6).Text = "Fevereiro"
        ElseIf e.Row.Cells(6).Text = "3" Then
            e.Row.Cells(6).Text = "Março"
        ElseIf e.Row.Cells(6).Text = "4" Then
            e.Row.Cells(6).Text = "Abril"
        ElseIf e.Row.Cells(6).Text = "5" Then
            e.Row.Cells(6).Text = "Maio"
        ElseIf e.Row.Cells(6).Text = "6" Then
            e.Row.Cells(6).Text = "Junho"
        ElseIf e.Row.Cells(6).Text = "7" Then
            e.Row.Cells(6).Text = "Julho"
        ElseIf e.Row.Cells(6).Text = "8" Then
            e.Row.Cells(6).Text = "Agosto"
        ElseIf e.Row.Cells(6).Text = "9" Then
            e.Row.Cells(6).Text = "Setembro"
        ElseIf e.Row.Cells(6).Text = "10" Then
            e.Row.Cells(6).Text = "Outubro"
        ElseIf e.Row.Cells(6).Text = "11" Then
            e.Row.Cells(6).Text = "Novembro"
        ElseIf e.Row.Cells(6).Text = "12" Then
            e.Row.Cells(6).Text = "Dezembro"

        End If


        If e.Row.Cells(5).Text = "0" Then
            e.Row.Cells(5).Text = "Aberto"
        ElseIf e.Row.Cells(5).Text = "1" Then
            e.Row.Cells(5).Text = "Pago"
        ElseIf e.Row.Cells(5).Text = "2" Then
            e.Row.Cells(5).Text = "Estornado"
        ElseIf e.Row.Cells(5).Text = "3" Then
            e.Row.Cells(5).Text = "Isento"
        End If




        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If


    End Sub

    Private Sub gradejoias2_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradejoias2.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(3).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightSalmon
        Else
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If
        'controle mes 
        If e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Janeiro"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Fevereiro"
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.Cells(4).Text = "Março"
        ElseIf e.Row.Cells(4).Text = "4" Then
            e.Row.Cells(4).Text = "Abril"
        ElseIf e.Row.Cells(4).Text = "5" Then
            e.Row.Cells(4).Text = "Maio"
        ElseIf e.Row.Cells(4).Text = "6" Then
            e.Row.Cells(4).Text = "Junho"
        ElseIf e.Row.Cells(4).Text = "7" Then
            e.Row.Cells(4).Text = "Julho"
        ElseIf e.Row.Cells(4).Text = "8" Then
            e.Row.Cells(4).Text = "Agosto"
        ElseIf e.Row.Cells(4).Text = "9" Then
            e.Row.Cells(4).Text = "Setembro"
        ElseIf e.Row.Cells(4).Text = "10" Then
            e.Row.Cells(4).Text = "Outubro"
        ElseIf e.Row.Cells(4).Text = "11" Then
            e.Row.Cells(4).Text = "Novembro"
        ElseIf e.Row.Cells(4).Text = "12" Then
            e.Row.Cells(4).Text = "Dezembro"

        End If


        If e.Row.Cells(3).Text = "0" Then
            e.Row.Cells(3).Text = "Aberto"
        ElseIf e.Row.Cells(3).Text = "1" Then
            e.Row.Cells(3).Text = "Pago"
        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.Cells(3).Text = "Estornado"
        ElseIf e.Row.Cells(3).Text = "3" Then
            e.Row.Cells(3).Text = "Isento"
        End If




        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If


    End Sub

    Private Sub btnmensalidades_Click(sender As Object, e As EventArgs) Handles btnmensalidades.Click
        divdejoias.Visible = False
        divdejoias2.Visible = False
        divdemensalidades.Visible = True
        carregamensalidades()
    End Sub

    Private Sub btnjoias_Click(sender As Object, e As EventArgs) Handles btnjoias.Click
        divdejoias.Visible = True
        divdejoias2.Visible = False
        divdemensalidades.Visible = False
        carregajoias()
    End Sub


    Private Sub btnjoias2_Click(sender As Object, e As EventArgs) Handles btnjoias2.Click
        divdejoias.Visible = False
        divdejoias2.Visible = True
        divdemensalidades.Visible = False
        carregajoias2()
    End Sub
End Class

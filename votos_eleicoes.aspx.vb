﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports clsSmart
Imports clssessoes
Partial Class votos_eleicoes
    Inherits System.Web.UI.Page
    Private Sub votos_eleicoes_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbAssociados where ativo = '1' ")
        lblassocativos.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbvotacao_eleicoes")
        lblttvotos.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbvotacao_eleicoes WHERE nrseqchapa LIKE '1' OR nrseqchapa LIKE '2'")
        lblvotosbrancos.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("SELECT numero, descricao, (SELECT count(nrseq) FROM tbvotacao_eleicoes WHERE nrseqchapa = V.nrseq) as votos FROM tbchapas_eleicoes V WHERE ativo = true ORDER BY numero ASC")

        rptchapas.DataSource = tb1
        rptchapas.DataBind()
    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub rptchapas_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptchapas.ItemDataBound
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Dim div As HtmlControl = e.Item.FindControl("divprogress")
        Dim votos As Label = e.Item.FindControl("lblvotos")
        tb1 = tab1.conectar("select * from tbvotacao_eleicoes")
        Dim totalvototos As Integer = tb1.Rows.Count
        Dim votoschapa As Integer = numeros(votos.Text)
        Dim porcento As Integer = (votoschapa * 100) / totalvototos

        div.Attributes.Add("style", "width: " & porcento & "%")

    End Sub
End Class

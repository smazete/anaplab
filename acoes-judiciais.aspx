﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage2.master" CodeFile="acoes-judiciais.aspx.vb" Inherits="acoes_judiciais" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }

            .btn-circle.btn-lg {
                width: 50px;
                height: 50px;
                padding: 10px 16px;
                font-size: 18px;
                line-height: 1.33;
                border-radius: 25px;
            }

            .btn-circle.btn-xl {
                width: 70px;
                height: 70px;
                padding: 10px 16px;
                font-size: 24px;
                line-height: 1.33;
                border-radius: 35px;
            }

        .content-vandal {
            padding: 10px 16px;
            line-height: 1.33;
            border-radius: 35px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server">
        <Triggers >
            <asp:PostBackTrigger ControlID="btntermo" />
            <asp:PostBackTrigger ControlID="btncontrato" />
        </Triggers>
        <ContentTemplate>

    <div class="col-lg-12 text-center margin-top-40">
        <br />
        <h3>AÇÕES JUDICIAIS </h3>
        <br />
        <div class="progress">
               <div class="progress-bar bg-info " style="width: 100%; color:black;"></div>
                                </div>
    </div>

    <div class="row text-center" runat="server" id="divrpt">
        <div class="col-md-2 "></div>
        <div class="col-md-8 text-center">
            <asp:Repeater runat="server" ID="rptacoes">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="col-md-3">
                        <asp:HiddenField ID="hdnrseqacao" runat="server" Value='<%#Bind("nrseq") %>'></asp:HiddenField>
                        <div class="col-md-12">
                            <asp:LinkButton ID="abrir" runat="server" CssClass="btn btn-info btn-circle btn-lg" CommandName="abrir"><i class="glyphicon glyphicon-share"></i></asp:LinkButton>
                            <div class="content-vandal bg-info">
                                <span class="info-card-text">
                                    <asp:Label ID="txtlbltitulo" runat="server" Font-Bold="true" Text='<%#Bind("titulo") %>' Style="color: black"></asp:Label></span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                <%--<span class="progress-description"><p>
                    Adicionado Dia <asp:Label ID="lbldtcad" runat="server" Font-Bold="true" Text='<%#Bind("dtcad") %>' style="color: black"></asp:Label></p>
                  </span>--%>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="col-md-2"></div>
    </div>
    <br />
            <div class="row " style="margin-left: 15%; margin-right: 15%;" runat="server" id="divacaoaberta">
                <asp:HiddenField ID="hdselecaoacao" runat="server" />
                <div class="row">
                <div class="row">
                    <div id="divtermo" class="col-md-2">
                        <asp:LinkButton ID="btntermo" runat="server" CssClass="btn btn-default btn-lg bg-info" autopostback="true"><i class="glyphicon glyphicon-share"> TERMO</i></asp:LinkButton>
                    </div>
                        <div id="divprocuracao" class="col-md-2">
                        <asp:LinkButton ID="btncontrato" CssClass="btn btn-default btn-lg" runat="server" autopostback="true"><i class="glyphicon glyphicon-share" a> CONTRATO</i></asp:LinkButton>
                    </div>
                    <div  class="col-md-6"></div>
                    <div  class="col-md-2 ">
                        <asp:LinkButton ID="btnvoltar" CssClass="btn btn-default btn-lg close" runat="server" ><i class="glyphicon glyphicon-random"></i></asp:LinkButton>
                    </div>
                </div>
                    <br />

                    <div class="col-lg-12">
                        <iframe runat="server" id="zframe" width="100%" height="900"></iframe>
                        <br />
                    </div>
                </div>

                <br />
            </div>

    <div class="row" runat="server" id="divinformativo">
        <div class="col-lg-2 text-center"></div>
        <div class="col-lg-8">
            <p style="font-size: 26px; color: black">
                <img src="img/balanca.jpg" alt="Alternate Text"  />
                Todas as ações judiciais patrocinadas pela ANAPLAB são inteiramente isentas de custas/taxas. Em caso de insucesso, os valores das sucumbências são suportados pela Associação.

                Os honorários pagos aos advogados contratados são feitos com base na tabela disponibilizada pela OAB RJ.

                Os valores das mensalidades são destinados, preferencialmente, para a manutenção das ações judiciais.
                <br />
                <br />
                <b class="text-left">Ari Zanella</b>
                <br />
                <b class="text-left">Presidente Administrativo</b>
                <br />
                <br />
            </p>
        </div>
        <div class="col-lg-2 text-left"></div>
    </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeFile="Planos.aspx.vb" Inherits="restrito_planos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:HiddenField runat="server" ID="hdnnrseq" />
    <asp:UpdatePanel runat="server">
        <ContentTemplate>


            <div class="box box-primary" style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);" id="inicio">
                <div class="box box-header ">
                    <b>Cadastro de Planos</b>
                </div>
                <div class="box-body">
                    <%--<div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#cadastro" aria-controls="cadastro" role="tab" data-toggle="tab">Cadastro</a>
                                </li>
                                <li role="presentation">
                                    <a href="#distribuicao" aria-controls="distribuicao" role="tab" data-toggle="tab">Distribuição de receitas</a>
                                </li>
                            </ul>
                        </div>
                    </div>--%>
                    <%--                    <div class="row">
                        <div class="col-lg-12">--%>
                    <%--<div class="tab-content">--%>
                    <%--  <div role="tabpanel" class="tab-pane active" id="cadastro">--%>

                    <div class="row text-align-center">
                        <div class="col-lg-12">
                            <asp:LinkButton ID="btnNovo" Text="Novo" ClientIDMode="Static" runat="server" CssClass="btn btn-primary"></asp:LinkButton>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Código" for="txtcodigo"></asp:Label>
                            <asp:TextBox type="text" ID="txtcodigo" runat="server" ClientIDMode="static"
                                CssClass="form-control" Enabled="false" />
                        </div>
                        <div class="col-lg-6">
                            <asp:Label runat="server" Text="Descrição" for="txtdescricao"></asp:Label>
                            <asp:TextBox type="text" ID="txtdescricao" runat="server" ClientIDMode="static"
                                CssClass="form-control" Enabled="false" />
                        </div>

                        <div class="col-lg-4">
                            <asp:Label runat="server" Text="Identificador" for="txtidentificador"></asp:Label>
                            <asp:TextBox type="text" ID="txtidentificador" runat="server" ClientIDMode="static"
                                CssClass="form-control" Enabled="false" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Padrão de Contrato" for="ddlPadraoContrato"></asp:Label>
                            <asp:DropDownList ID="cboPadraoContrato" runat="server" AutoPostBack="true"
                                CssClass="form-control" ClientIDMode="Static" Enabled="false">
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-4">
                            <asp:Label runat="server" Text="Nome no Recibo do Carnê" for="txtnomenorecibo"></asp:Label>
                            <asp:TextBox type="text" ID="txtnomenorecibo" runat="server" ClientIDMode="static"
                                CssClass="form-control" Enabled="false" />
                        </div>
                        <div class="col-lg-3">
                            <asp:Label runat="server" Text="Banco Padrão Emissão Bol." for="ddlBancoPadrao"></asp:Label>
                            <asp:DropDownList ID="cboBancoPadrao" runat="server" AutoPostBack="true"
                                CssClass="form-control" ClientIDMode="Static" Enabled="false">
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label runat="server" Text="Carteira Padrão Emissão Bol." for="ddlCarteiraPadrao"></asp:Label>
                            <asp:DropDownList ID="cboCarteiraPadrao" runat="server" AutoPostBack="true"
                                CssClass="form-control" ClientIDMode="Static" Enabled="false">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="CNPJ" for="txtcnpj"></asp:Label>
                            <asp:TextBox type="text" ID="txtcnpj" runat="server" ClientIDMode="static"
                                CssClass="form-control" MaxLength="18" onkeyup="formataCnpjCpf(this,event);" Enabled="false" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Telefone de Contato" for="txttelefone"></asp:Label>
                            <asp:TextBox type="text" ID="txttelefone" runat="server" ClientIDMode="static"
                                CssClass="form-control" MaxLength="14" onkeyup="formataTelefone(this,event);" Enabled="false" />
                        </div>
                        <div class="col-lg-8">
                            <asp:Label runat="server" Text="Endereço Completo" for="txtendereco"></asp:Label>
                            <asp:TextBox type="text" ID="txtendereco" runat="server" ClientIDMode="static"
                                CssClass="form-control" Enabled="false" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <asp:Label runat="server" Text="Cidade" for="txtcidade"></asp:Label>
                            <asp:TextBox type="text" ID="txtcidade" runat="server" ClientIDMode="static"
                                CssClass="form-control" Enabled="false" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="UF" for="txtuf"></asp:Label>
                            <asp:TextBox type="text" ID="txtuf" runat="server" ClientIDMode="static"
                                CssClass="form-control" Enabled="false" />
                        </div>
                        <div class="col-lg-6">
                            <div class="row">

                                <div class="col-lg-5">
                                    <asp:CheckBox Text="Ignorar Salto de Matrícula" CssClass="checkbox-inline fix-check" ID="chkIgnorarSaltoDeMatricula" runat="server" Enabled="false" />
                                </div>
                                <div class="col-lg-5">
                                    <asp:CheckBox Text="Usar como Plano Padrão" CssClass="checkbox-inline fix-check" ID="chkUsarComoPlanoPadrao" runat="server" Enabled="false" />
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <asp:Label runat="server" Text="Texto Carteirinha Verso" for="txtTextoCarteirinhaVerso"></asp:Label>
                            <asp:TextBox TextMode="MultiLine" ID="txtTextoCarteirinhaVerso" runat="server" ClientIDMode="static" CssClass="form-control" Rows="5" Enabled="false">
                            </asp:TextBox>
                        </div>
                    </div>

                    <br />
                    <div class="row text-align-center">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-2">
                            <asp:LinkButton runat="server" CssClass="btn btn-success" Text="Salvar" ID="btnSalvar">
                            </asp:LinkButton>
                        </div>
                        <div class="col-lg-2">
                            <asp:LinkButton runat="server" CssClass="btn btn-danger" Text="Cancelar" ID="btncancelar">
                            </asp:LinkButton>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                    <br />
                    <div class="box box-primary">
                        <div class="box box-header">
                            Procurar
                        </div>
                        <div class="box-body">
                            <div class="row ">
                                <div class="col-lg-3"></div>

                                <div class="col-lg-3 fix-txt">
                                    <asp:Label runat="server" Text="Descrição"></asp:Label>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtprocurar"></asp:TextBox>
                                </div>
                                <div class="col-lg-1">
                                    <asp:CheckBox runat="server" Text="Inativo" CssClass="checkbox-inline" Checked="false" ID="chkexibirinativos" />
                                </div>
                                <div class="col-lg-2">
                                    <asp:LinkButton runat="server" Text="Procurar" CssClass="btn btn-primary" ID="btnprocurar"></asp:LinkButton>
                                </div>

                                <div class="col-lg-3"></div>

                            </div>
                            <br />
                            <div class="row">

                                <div class="col-lg-12">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>

                                            <asp:GridView ID="gradeplanos" ShowHeaderWhenEmpty="true" EmptyDataText="Não possui nenhum plano para essa consulta" runat="server" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None">
                                                <Columns>
                                                    <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                                    <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                                    <asp:BoundField DataField="planocontrato" HeaderText="Plano Contrato" />
                                                    <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                                    <asp:BoundField DataField="planopadrao" HeaderText="Padrão" />
                                                    <asp:BoundField DataField="identificador" HeaderText="Identificador" />
                                                    <asp:BoundField DataField="textocarteirinhaverso" HeaderText="Texto Carteira" />
                                                    <asp:TemplateField HeaderText="Ações" runat="server">
                                                        <ItemTemplate runat="server">
                                                            <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                                    <i class="fa fa-pencil-square-o"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnexcluir" runat="server" CssClass="btn btn-danger btn-xs" CommandName="deletar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                                <i runat="server" id="icogradeexcluir" class="fa fa-trash-o"></i>
                                                            </asp:LinkButton>
                                                            <asp:HiddenField runat="server" ID="hdnnrseqgrade" Value='<%#Bind("nrseq") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <%--       </div>--%>
                        <%--        <div role="tabpanel" class="tab-pane" id="distribuicao">
                                    <div class="box box-primary">
                                        <div class="box-body">
                                            <div class="row">
                                                <br />
                                            </div>

                                        </div>
                                    </div>
                                </div>--%>
                        <%--    </div>--%>
                        <%--     </div>
                    </div>--%>
                    </div>

                </div>
                <%--  <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <div id="enviaremail" class="windowstatusmodelo">
                <div class="btfecharholder">
                    <%--<div class="btfechar" onclick="fecharmodalfichaatendimento()">
                    <a href="#" class="fechar" onclick="fecharmodelo()">X</a>
                    <br />
                    <br />
                </div>
                <div>
                    <iframe src="dependentes.aspx" class="frameemail"></iframe>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
                --%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

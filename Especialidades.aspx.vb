﻿Imports clssessoes
Imports clsSmart
Imports System.IO

Partial Class Especialidades
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tabelaesp As String = "tbespecialidades"

    Private Sub Especialidade_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack Then
            Exit Sub
        End If

        divmostrardados.Visible = False

        If divmostrardados.Visible = False Then
            mgradep.Visible = False
        Else
            mgradep.Visible = True
        End If

        cboplano.Enabled = True
        btnconfirmar.Visible = True
        btnconfirmar.Enabled = True

        If cboplano.Text = "" Then
            buscaplano()
        End If

        descriprepa()

    End Sub

    Private Sub limpar()
        txtdescricaopreparatorio.Text = ""
        divdescricaopreparatorios.Visible = False
        txtcod.Text = ""
        txtespecialidade.Text = ""
        txtvalor.Text = ""
        txtnovaconsultaem.Text = ""
        cbopadraoprontuario.Text = ""
        chkIgnoraRetorno.Checked = False
        chkctrlFinan.Checked = False
        chkretorno.Checked = False
        chkimprimirguia.Checked = False
        chkignorarvalor.Checked = False
        chkmedtrab.Checked = False
        txtcbo.Text = ""
        cbodescripadrao.Text = ""
        chkignorarvalor.Checked = False
        chkalterarespecialidade.Checked = False
        chkpreparatorio.Checked = False
        txtTuss.Text = ""
    End Sub
    Private Sub descriprepa()
        If chkpreparatorio.Checked = True Then
            divdescricaopreparatorios.Visible = True
        Else
            divdescricaopreparatorios.Visible = False
        End If
    End Sub
    Private Sub trocabtn(valor As Boolean)

    End Sub
    Private Sub gradep()
        Dim xespecialidades As New clsespecialiades

        xespecialidades.Plano = cboplano.SelectedItem.Text

        If Not xespecialidades.consultarplanodesc(Not chkexibirinativos.Checked, True) Then
            sm("swal({title: 'Atenção!',text: '" & xespecialidades.Mensagemerro & "', type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        grade.DataSource = xespecialidades.Table
        grade.DataBind()


    End Sub

    Private Sub buscaplano()

        Dim tbplanos As New Data.DataTable
        Dim tabplanos As New clsBanco

        tbplanos = tabplanos.conectar("select * from tbplanos where ativo = true order by descricao asc")

        Dim cont As Integer

        cboplano.Items.Clear()
        cboplano.Items.Add("")

        cont = cboplano.Items.Count

        For x As Integer = 0 To tbplanos.Rows.Count - 1
            cboplano.Items.Add(tbplanos.Rows(x)("descricao").ToString)
            cboplano.Items(cboplano.Items.Count - 1).Value = tbplanos.Rows(x)("nrseq").ToString
        Next


    End Sub

    Private Sub chkpreparatorio_CheckedChanged(sender As Object, e As EventArgs) Handles chkpreparatorio.CheckedChanged

        If chkpreparatorio.Checked = True Then
            divdescricaopreparatorios.Visible = True
        Else
            divdescricaopreparatorios.Visible = False
        End If
    End Sub

    Private Sub habilitar(valor As Boolean)
        txtcod.Enabled = valor
        txtespecialidade.Enabled = valor
        txtvalor.Enabled = valor
        cbopadraoprontuario.Enabled = valor
        txtnovaconsultaem.Enabled = valor
        chkIgnoraRetorno.Enabled = valor
        chkctrlFinan.Enabled = valor
        chkretorno.Enabled = valor
        chkimprimirguia.Enabled = valor
        chkignorarvalor.Enabled = valor
        chkmedtrab.Enabled = valor
        txtcbo.Enabled = valor
        cbodescripadrao.Enabled = valor
        chkignorarvalor.Enabled = valor
        chkalterarespecialidade.Enabled = valor
        chkpreparatorio.Enabled = valor
        txtTuss.Enabled = valor
        txtdescricaopreparatorio.Enabled = valor
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub
    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click

        Dim xclasse As New clsespecialiades

        If Not xclasse.novo Then
            sm("swal({title: 'Atenção!',text: 'Dados Divergentes ', type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        limpar()
        hidcod.Value = xclasse.Cod
        txtcod.Text = hidcod.Value
        txtcod.ReadOnly = True
        habilitar(True)

        btnsalvar.Enabled = True

    End Sub

    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click

        Dim xespecialidades As New clsespecialiades

        If txtcod.Text = "" Then
            sm("Swal.fire({'Atenção!','Código não informado.','error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        xespecialidades.Cod = txtcod.Text
        xespecialidades.Descricao = txtespecialidade.Text
        xespecialidades.Valor = txtvalor.Text
        xespecialidades.Plano = cboplano.SelectedItem.Text
        xespecialidades.Nrseqplano = cboplano.Text
        xespecialidades.Ignoraretorno = chkIgnoraRetorno.Checked
        xespecialidades.Ctrlfinan = chkctrlFinan.Checked
        xespecialidades.Retorno = chkretorno.Checked
        xespecialidades.Imprimirguia = chkimprimirguia.Checked
        xespecialidades.Medtrab = chkmedtrab.Checked

        If txtnovaconsultaem.Text <> "" Then
            xespecialidades.Novaconsultaem = txtnovaconsultaem.Text
        End If

        xespecialidades.Consultaapostermino = chkpreparatorio.Checked
        xespecialidades.Padrao = cbopadraoprontuario.Text
        xespecialidades.Ignorarvalor = chkignorarvalor.Checked
        xespecialidades.Descricaopadraorecebimentos = txtdescricaopreparatorio.Text
        xespecialidades.Cbo = txtcbo.Text
        xespecialidades.Alterarespecialidade = chkalterarespecialidade.Checked
        xespecialidades.Tuss = txtTuss.Text
        xespecialidades.Nrseqempresa = Session("idempresa")


        If Not xespecialidades.salvar Then
            sm("Swal.fire({'Atenção!','Erro o preenchimento da ficha','error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        limpar()
        habilitar(False)

        gradep()
        mgradep.Visible = True

        btnnovo.Enabled = True
        btnsalvar.Enabled = False

        sm("swal({title: 'Atenção!',text: 'Especialidade Salva com Sucesso!' ,type: 'success' ,confirmButtonText: 'OK'})", "swal")


    End Sub

    Private Sub btnconfirmar_Click(sender As Object, e As EventArgs) Handles btnconfirmar.Click

        cboplano.Enabled = False
        divmostrardados.Visible = True
        btntrocar.Enabled = True
        btntrocar.Visible = True
        btnconfirmar.Visible = False
        btnconfirmar.Enabled = False
        chkexibirinativos.Visible = True
        mgradep.Visible = True
        gradep()
        divprocurarespec.Visible = True

    End Sub

    Private Sub btntrocar_Click(sender As Object, e As EventArgs) Handles btntrocar.Click

        divmostrardados.Visible = False

        limpar()

        cboplano.Enabled = True
        btntrocar.Enabled = False
        btntrocar.Visible = False
        btnconfirmar.Visible = True
        btnconfirmar.Enabled = True

        mgradep.Visible = False

    End Sub

    Private Sub Grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grade.RowCommand
        Dim existe As Boolean = False

        '  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "escondeexibiralerta2()", "escondeexibiralerta2()", True)

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)

        Dim xMedicos As New clsmedicos
        Dim row As GridViewRow = grade.Rows(index)
        Dim hdnrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        If e.CommandName.ToString.ToLower = "selecionar" Then
            Dim xespecialidades As New clsespecialiades
            xespecialidades.Cod = hdnrseq.Value
            If Not xespecialidades.procurar Then
                sm("Swal.fire({'Atenção!','" & xespecialidades.Mensagemerro & "','error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            limpar()

            txtcod.Text = xespecialidades.Cod
            txtespecialidade.Text = xespecialidades.Descricao
            txtvalor.Text = xespecialidades.Valor
            txtnovaconsultaem.Text = xespecialidades.Novaconsultaem
            cbopadraoprontuario.Text = xespecialidades.Padrao
            txtcbo.Text = xespecialidades.Cbo
            chkIgnoraRetorno.Checked = xespecialidades.Ignoraretorno
            chkctrlFinan.Checked = xespecialidades.Ctrlfinan
            chkignorarvalor.Checked = xespecialidades.Ignorarvalor
            chkretorno.Checked = xespecialidades.Retorno
            chkimprimirguia.Checked = xespecialidades.Imprimirguia
            chkmedtrab.Checked = xespecialidades.Medtrab
            chkpreparatorio.Checked = xespecialidades.Consultaapostermino
            chkalterarespecialidade.Checked = xespecialidades.Alterarespecialidade

            If xespecialidades.Consultaapostermino = True Then
                divdescricaopreparatorios.Visible = True
                txtdescricaopreparatorio.Text = xespecialidades.Descricaopadraorecebimentos
            Else
                divdescricaopreparatorios.Visible = False
            End If

            habilitar(True)

            btnnovo.Enabled = False
            btnsalvar.Enabled = True

        End If

        If e.CommandName.ToString.ToLower = "excluir" Then
            Dim xespecialidades As New clsespecialiades
            xespecialidades.Cod = hdnrseq.Value
            If Not xespecialidades.excluir Then
                sm("Swal.fire({'Atenção!','" & xespecialidades.Mensagemerro & "','error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            gradep()
        End If

        If e.CommandName.ToString.ToLower = "reativar" Then
            Dim xespecialidades As New clsespecialiades
            xespecialidades.Cod = hdnrseq.Value
            If Not xespecialidades.excluir Then
                sm("Swal.fire({'Atenção!','" & xespecialidades.Mensagemerro & "','error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            gradep()
        End If
    End Sub


    'Private Sub carregaespecialidades(Optional procura As String = "", Optional inativos As Boolean = False)

    '    If cboplano.SelectedValue = "" Then
    '        sm("Swal.fire({icon: 'error',title: 'Selecione um Plano',text: 'Para proceguir',})")
    '    End If
    '    tb1 = tab1.conectar("Select * FROM tbespecialidades where plano = '" & cboplano.SelectedItem.Text & "' and ativo = true order by descricao asc")

    '    'Dim cont As Integer

    '    'cont = tb1.Items.Count

    '    gradeespecialidades.DataSource = tb1
    '    gradeespecialidades.DataBind()

    'End Sub

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click
        limpar()

        habilitar(False)

        btnnovo.Enabled = True
        btnsalvar.Enabled = False

    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub

        Dim btnreativar As LinkButton = e.Row.FindControl("btnreativar")
        Dim btnexcluir As LinkButton = e.Row.FindControl("btnexcluir")

        If logico(e.Row.Cells(2).Text) = True Then
            e.Row.Cells(2).Text = "Sim"
            btnreativar.Visible = False
            btnexcluir.Visible = True
        Else
            e.Row.Cells(2).Text = "Não"
            btnreativar.Visible = True
            btnexcluir.Visible = False
        End If


    End Sub

    Private Sub chkexibirinativos_CheckedChanged(sender As Object, e As EventArgs) Handles chkexibirinativos.CheckedChanged
        gradep()
    End Sub
End Class

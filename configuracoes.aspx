﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="configuracoes.aspx.vb" Inherits="configuracoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">





    <div class="box box-primary" style="margin-left: 1rem; margin-top: 1rem; margin-right: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">

        <div class="box box-header ">
            <b>Configurações diversas</b>
            
        </div>
        <div class="box-body">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-12 ">
                            <div class="box box-primary">
                                <div class="box box-header">
                                    <b>Visual identity & License Information</b>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-1 "></div>
                                        <div class="col-lg-3 " style="background-color: antiquewhite; padding: 3rem;">
                                            <div class="row">
                                                <div class="col-lg-12 text-center">
                                                    <asp:LinkButton ID="btncarregarlogo" runat="server" CssClass="btn btn-success "> <i class="fa fa-photo"></i>  Alterar Logo  </asp:LinkButton>
                                                    <br />

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 text-center">
                                                    <br />
                                                    <asp:ImageButton runat="server" ID="imglogocliente" ImageUrl="~/img/padlogo.png" Style="width: 11rem; height: 11rem;" alt="User Image" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-inline">
                                                    <label for="txtcor">Color<strong style="color: red;">*</strong></label>&nbsp;&nbsp;
                                        <div class="input-group my-colorpicker2">
                                            <input type="hidden" runat="server" id="txtcor" clientidmode="static" class="form-control" value="#000">

                                            <div class="input-group-addon">
                                                <i></i>
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 ">
                                            <asp:UpdatePanel ID="updateclientes" runat="server">
                                                <ContentTemplate>

                                                    <div class="row">
                                                        <div class="col-lg-8 ">
                                                            Cliente
                                            <br />
                                                            <asp:TextBox ID="txtnomecliente" runat="server" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4 ">
                                                            CNPJ
                                            <br />
                                                            <asp:TextBox ID="txtcnpjcliente" onkeyup="formataCNPJ(this,event);" runat="server" MaxLength="17" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-3 ">
                                                            CEP
                                            <br />
                                                            <asp:TextBox ID="txtcep" AutoPostBack="true" MaxLength="9" onkeyup="formataCEP(this,event);" runat="server" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-9 ">
                                                            Endereço
                                            <br />
                                                            <asp:TextBox ID="txtenderecocliente" runat="server" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 ">
                                                            Bairro
                                            <br />
                                                            <asp:TextBox ID="txtbairrocliente" runat="server" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-6 ">
                                                            Cidade
                                            <br />
                                                            <asp:TextBox ID="txtcidadecliente" runat="server" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-2 ">
                                                            UF
                                            <br />
                                                            <asp:TextBox ID="txtufcliente" runat="server" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 ">
                                                            Pais
                                            <br />
                                                            <asp:TextBox ID="txtpaiscliente" runat="server" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4 ">
                                                            Telefone
                                            <br />
                                                            <asp:TextBox ID="txttelefonecliente" onkeyup="formataTelefone(this,event);" runat="server" CssClass="form-control "></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4 ">
                                                            <br />
                                                            <asp:LinkButton ID="btnsalvarcliente" runat="server" CssClass="btn btn-success "><asp:Image ImageUrl="~/img/salvar01.png" style="width:2rem;height:2rem;" runat="server"/> Save </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                   
                    <div class="box box-danger">

                        <div class="box box-header  ">
                            <b>Textos dos e-mails
                            </b>

                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-3">
                                    <asp:Label ID="Label2" runat="server" Text="Tipo de Texto:"></asp:Label>
                                    <br />
                                    <asp:DropDownList ID="cbotipotexto" runat="server" CssClass="form-control" AutoPostBack="true">
                                        <%--<asp:ListItem>(NOVO)</asp:ListItem>--%>
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>Envio de e-mails site contato</asp:ListItem>
                                       
                                        <asp:ListItem>Esqueci minha senha</asp:ListItem>
                                      
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-3 ">
                                    Assunto do E-mail
                            <br />
                                    <asp:TextBox ID="txtassunto" runat="server" CssClass="form-control "></asp:TextBox>
                                </div>
                                <div class="col-lg-2 text-center ">
                                    <br />
                                    <asp:Button ID="btnopen" runat="server" Text="Abrir" class="btn btn-primary" />
                                </div>
                                <div class="col-lg-4 ">
                                    <div class="box box-primary ">
                                        <div class="box-header ">
                                            Use as tags abaixo para configurar no texto
                                        </div>
                                        <div class="box-body ">
                                            <div class="row" style="padding: 1rem;">

                                                <asp:Repeater ID="rpttags" runat="server">
                                                    <ItemTemplate>
                                                        <div class="text-left ">
                                                            <asp:Label ID="lbltags" runat="server" Text='<%#Bind("tag") %>' CssClass="label label-success"></asp:Label>
                                                            <br />
                                                        </div>
                                                    </ItemTemplate>

                                                </asp:Repeater>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divtexto" runat="server">
                                <br />
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:TextBox ID="txtcorpo" Style="margin-right: 10px; margin-left: 10px;" Height="420px" runat="server" Width="100%">
                                        </asp:TextBox>
                                        <ajaxToolkit:HtmlEditorExtender ID="TextBox1_HtmlEditorExtender" runat="server" TargetControlID="txtcorpo" EnableSanitization="false">
                                        </ajaxToolkit:HtmlEditorExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-lg-4 text-center ">
                                    <br />
                                    <asp:Button ID="btnsalvar1" runat="server" Text="Salvar" class="btn btn-danger col-lg-4" />
                                </div>
                                <%-- <div class="col-lg-4 text-center">
                                    <br />
                                    <asp:Button ID="btnapagamodelo" runat="server" Text="Excluir" class="btn btn-danger col-lg-4" />
                                </div>--%>
                            </div>
                    


                        </div>
                    </div>
                </ContentTemplate>

            </asp:UpdatePanel>
        </div>
    </div>

    

    <script>

        $('.my-colorpicker2').colorpicker();

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
</asp:Content>


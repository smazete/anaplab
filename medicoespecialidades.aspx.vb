﻿
Imports Microsoft.VisualBasic
Imports System.Data
Imports clssessoes
Imports clsSmart
Partial Class medicoespecialidades
    Inherits System.Web.UI.Page

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub medicoespecialidades_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack Then Exit Sub

        limpar()
        habilitar(False)
        cbomedicosc()
        cbplanosc()


    End Sub

    Private Sub cbplanosc()
        Dim xplanos As New clsPlanos

        xplanos.Ativo = True

        If Not xplanos.consultartodos() Then
            sm("swal({title: 'Atenção!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        cboplano.Items.Clear()
        cboplano.Items.Add("")

        For x As Integer = 0 To xplanos.Contador - 1
            cboplano.Items.Add(New ListItem(xplanos.Table.Rows(x)("descricao").ToString, xplanos.Table.Rows(x)("nrseq").ToString))
        Next
    End Sub

    Private Sub cbomedicosc()
        Dim xmedicos As New clsmedicos


        If Not xmedicos.consultarTodos() Then
            sm("swal({title: 'Atenção!',text: '" & xmedicos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        cbomedicos.Items.Clear()
        cbomedicos.Items.Add("")

        For x As Integer = 0 To xmedicos.Contador - 1
            cbomedicos.Items.Add(New ListItem(xmedicos.Table.Rows(x)("nome").ToString, xmedicos.Table.Rows(x)("cod").ToString))
        Next
    End Sub

    Private Sub cboespecialidadec()
        Dim xespecialidades As New clsespecialiades

        If cboplano.SelectedIndex <= 0 Then
            sm("swal({title: 'Atenção!',text: 'Selecione um plano para carregar as especialidades',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        Dim tbespecilidade As New Data.DataTable
        Dim tabespecialidade As New clsBanco
        cboespecialidades.Items.Clear()
        cboespecialidades.Items.Add("")

        xespecialidades.Nrseqplano = cboplano.SelectedValue

        If Not xespecialidades.consultarplano() Then
            sm("swal({title: 'Atenção!',text: '" & xespecialidades.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        For x As Integer = 0 To xespecialidades.Contador - 1
            cboespecialidades.Items.Add(New ListItem(xespecialidades.Table.Rows(x)("descricao").ToString, xespecialidades.Table.Rows(x)("cod").ToString))
        Next

    End Sub


    Private Sub limpar(Optional limpartudo As Boolean = False)
        txtvlconsulta.Text = ""
        txtvlrepasse.Text = ""
        txtpercrepasse.Text = ""
        txtenviadosite.Text = ""
        If limpartudo = True Then
            If cboplano.SelectedIndex > 0 Then
                cboplano.SelectedIndex = 0
            End If
            If cbomedicos.SelectedIndex > 0 Then
                cbomedicos.SelectedIndex = 0
            End If
            If cboespecialidades.SelectedIndex > 0 Then
                cboespecialidades.SelectedIndex = 0
            End If
            rptservicos.DataSource = Nothing
            rptservicos.DataBind()
        End If
    End Sub

    Private Sub btnnovo_Click(sender As Object, e As EventArgs) Handles btnnovo.Click
        txtpercrepasse.Enabled = True
        txtvlrepasse.Enabled = True
        Dim xespe As New clsMedicosEspecialidades
        xespe.Nomemedico = cbomedicos.SelectedItem.Text
        xespe.carregartodaspormedico()
        rptservicos.DataSource = xespe.Listaespecialidades
        rptservicos.DataBind()
        txtvlrepasse.Text = FormatCurrency(0)
        txtpercrepasse.Text = 0
        btnsalvar.Enabled = True
    End Sub

    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click

        ' Início do salvar
        Dim xMedicos_Especialidades As New clsMedicosEspecialidades
        xMedicos_Especialidades.Nomemedico = cbomedicos.SelectedItem.Text
        'xMedicos_Especialidades.Nrseqmedico = cbomedicos.SelectedValue
        xMedicos_Especialidades.Descricao = cboespecialidades.SelectedItem.Text
        xMedicos_Especialidades.Vlrepasse = txtvlrepasse.Text
        xMedicos_Especialidades.Plano = cboplano.SelectedItem.Text
        xMedicos_Especialidades.Percrepasse = txtpercrepasse.Text
        'xMedicos_Especialidades.Enviadosite = txtenviadosite.Text
        xMedicos_Especialidades.Nrseqempresa = Session("idempresaemuso")
        If Not xMedicos_Especialidades.salvar() Then
            sm("swal({title: 'Atenção!',text: '" & xMedicos_Especialidades.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        xMedicos_Especialidades.Nomemedico = cbomedicos.SelectedItem.Text
        xMedicos_Especialidades.carregartodaspormedico()
        rptservicos.DataSource = xMedicos_Especialidades.Listaespecialidades
        rptservicos.DataBind()
        limpar(True)
        habilitar(False)

        ' Fim do salvar

    End Sub

    Private Sub habilitar(Optional habilita As Boolean = True)
        btnnovo.Enabled = Not habilita
        cboplano.Enabled = Not habilita
        cbomedicos.Enabled = Not habilita
        cboespecialidades.Enabled = Not habilita
        txtvlconsulta.Enabled = habilita
        txtpercrepasse.Enabled = habilita
        txtvlrepasse.Enabled = habilita
        btnsalvar.Enabled = habilita
    End Sub

    Private Sub cboespecialidades_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboespecialidades.SelectedIndexChanged
        If cboespecialidades.Text = "" Then Exit Sub

        Dim xespe As New clsespecialiades

        xespe.Descricao = cboespecialidades.SelectedItem.Text
        If Not xespe.procurar(True) Then
            sm("swal.fire({title: 'Ops!',text: '" & xespe.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        txtvlconsulta.Text = FormatCurrency(xespe.Valor)

    End Sub

    Private Sub txtpercrepasse_TextChanged(sender As Object, e As EventArgs) Handles txtpercrepasse.TextChanged
        alteravalores(sender)
    End Sub

    Public Sub alteravalores(sender As Object)
        Dim valortotal As Decimal = 0
        Dim porce As Decimal = 0
        Dim valorrep As Decimal = 0
        valortotal = numeros(txtvlconsulta.Text)
        If sender.id = "txtpercrepasse" Then
            porce = numeros(txtpercrepasse.Text)
            valorrep = valortotal * porce / 100
            txtvlrepasse.Text = FormatCurrency(valorrep)
        Else
            valorrep = numeros(txtvlrepasse.Text)
            porce = valorrep * 100 / valortotal
            txtpercrepasse.Text = FormatNumber(porce, 2)
        End If

    End Sub

    Private Sub txtvlrepasse_TextChanged(sender As Object, e As EventArgs) Handles txtvlrepasse.TextChanged
        alteravalores(sender)
    End Sub

    Private Sub cbomedicos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbomedicos.SelectedIndexChanged
        Dim xespe As New clsMedicosEspecialidades
        xespe.Nomemedico = cbomedicos.SelectedItem.Text
        xespe.carregartodaspormedico()
        rptservicos.DataSource = xespe.Listaespecialidades
        rptservicos.DataBind()

    End Sub

    Private Sub rptservicos_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptservicos.ItemCommand

        Dim hdnrseqrpt As HiddenField = e.Item.FindControl("hdnrseqrpt")
        Dim xespec As New clsMedicosEspecialidades
        xespec.Nrseq = hdnrseqrpt.Value
        If e.CommandName = "excluir" Then
            xespec.excluir()
            xespec.Nomemedico = cbomedicos.SelectedItem.Text
            xespec.carregartodaspormedico()
            rptservicos.DataSource = xespec.Listaespecialidades
            rptservicos.DataBind()

        End If
        If e.CommandName = "editar" Then

            xespec.Nrseq = hdnrseqrpt.Value

            If Not xespec.editar() Then
                sm("Swal.fire({icon: 'error',title: 'Não foi possivel efetuar esta ação...',text: 'Contate o Administrador do Sistema',})")
                Exit Sub
            End If


            cbomedicos.SelectedValue = xespec.Nrseqmedico
            'cbomedicos.SelectedItem.Text = xespec.Nomemedico
            cboespecialidades.SelectedValue = xespec.Nrseqespecialidade
            txtvlrepasse.Text = xespec.Vlrepasse
            cboplano.SelectedItem.Text = xespec.Plano
            txtpercrepasse.Text = xespec.Percrepasse
            txtvlconsulta.Text = xespec.Valorespecialidades



        End If
    End Sub

    Private Sub rptservicos_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptservicos.ItemDataBound
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblvalorrpt As Label = e.Item.FindControl("lblvalorrpt")
            Dim lblvalorconsrpt As Label = e.Item.FindControl("lblvalorconsrpt")
            lblvalorconsrpt.Text = FormatCurrency(lblvalorconsrpt.Text)
            lblvalorrpt.Text = FormatCurrency(lblvalorrpt.Text)
        End If
    End Sub

    Private Sub cboplano_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboplano.SelectedIndexChanged
        cboespecialidadec()
    End Sub

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click
        limpar(True)
        habilitar(False)
    End Sub
End Class

﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="~/contasapagar.aspx.vb" Inherits="contasapaga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary">
        <div class="box box-header ">
            <b>Lançamento de Contas à Pagar</b>
        </div>
        <div class="box-body">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <div class="row text-align-center">
                        <div class="col-lg-12">
                            <asp:Button ID="btnnovo" runat="server" Text="Novo" CssClass="btn btn-primary" Enabled="true" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-1">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Cod"></asp:Label>
                                <asp:TextBox runat="server" ID="txtnrseq" Enabled="false" CssClass="form-control"></asp:TextBox>
                                <asp:HiddenField runat="server" ID="hdnnrseq" />
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Lançamento"></asp:Label>
                            <asp:TextBox ID="txtcodigo" Enabled="false" runat="server" onkeyup="formataPlanoContas(this,event);" CssClass="form-control" MaxLength="9" AutoPostBack="True"></asp:TextBox>
                        </div>

                        <div class="col-lg-3">
                            <asp:Label runat="server" Text="Plano de contas"></asp:Label>
                            <asp:DropDownList ID="cboplano" runat="server" CssClass="form-control" AutoPostBack="true" />
                        </div>

                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Centro de custo"></asp:Label>
                            <asp:DropDownList ID="cboCentroCusto" runat="server" CssClass="form-control" AutoPostBack="true" />
                        </div>

                        <div class="col-lg-4">
                            <asp:Label runat="server" Text="Descrição"></asp:Label>
                            <asp:TextBox ID="txtdescricao" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Valor"></asp:Label>
                            <asp:TextBox ID="txtvalor" runat="server" CssClass="form-control" onkeyup="formataValor(this,event);"></asp:TextBox>
                        </div>

                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Parcela inicial"></asp:Label>
                            <asp:TextBox ID="txtparcela" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-2">
                            <asp:Label runat="server" Text="Quant. parcelas"></asp:Label>
                            <asp:TextBox ID="txtparcelas" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-3">
                            <asp:Label runat="server" Text="Data Execução"></asp:Label>
                            <asp:TextBox ID="txtdtexecucao" onkeyup="formataData(this,event);" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-3">
                            <asp:Label runat="server" Text="Data do vencimento"></asp:Label>
                            <asp:TextBox ID="txtdtvencimento" runat="server" CssClass="form-control" MaxLength="10" onkeyup="formataData(this,event);"></asp:TextBox>
                        </div>



                    </div>
                    <br />

                    <div class="row">

                        <div class="col-lg-3">
                            <asp:Label runat="server" Text="Forma de pagamento"></asp:Label>
                            <asp:DropDownList ID="cboformapagto" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>

                        <div class="col-lg-3">
                            <asp:Label runat="server" Text="Operação"></asp:Label>
                            <asp:DropDownList ID="cbooperacao" Enabled="false" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>

                        <div class="col-lg-3">
                            <asp:CheckBox ID="chksolaprovacao" CssClass="checkbox-inline fix-btn" Text="Emitir Solicitação de Aprovação" runat="server"></asp:CheckBox>
                        </div>
                    </div>
                    <br />
                 
                            <div class="box box-primary">
                            <asp:HiddenField ID="hdconexao" runat="server" />

                                <div class="box-header "><b>Carregar arquivo</b></div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-12 ">

                                            <asp:UpdatePanel ID="updategradex" runat="server">
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btncarregarxls" />
                                                </Triggers>
                                                <ContentTemplate>

                                                 
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <br />
                                                            <asp:FileUpload ID="filearq" runat="server" />
                                                        </div>
                                                        <div class="col-lg-6 text-center">

                                                            <br />
                                                            <asp:Button ID="btncarregarxls" runat="server" CssClass="btn btn-success" Text="Adicionar arquivo" />

                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-lg-12 ">

                                                            <asp:GridView ID="gradearquivos" CssClass="table table-striped" runat="server" AutoGenerateColumns="false" GridLines="none" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum arquivo registrado nessa conta" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Sel">

                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="sel" runat="server" />

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:BoundField DataField="arquivo" HeaderText="Arquivo" />
                                                                    <asp:BoundField DataField="dtcad" HeaderText="Cadastrado em" />
                                                                    <asp:BoundField DataField="usercad" HeaderText="Cadastrado por" />
                                                                    <asp:BoundField DataField="processado" HeaderText="Processado" />
                                                                    <asp:BoundField DataField="dtprocessado" HeaderText="Processado em" />
                                                                    <asp:BoundField DataField="userprocessado" HeaderText="Processado por" />
                                                                    <asp:TemplateField HeaderText="Linhas">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="lblnrseq" runat="server" Value='<%#Bind("nrseq") %>' />
                                                                            <span class="badge" style="background: rgba(255, 255, 255, 0.00);padding:5px;">
                                                                                <asp:Label ForeColor="black" Font-Size="large" ID="lbllinhas" runat="server" Text='<%#Bind("linhas") %>'></asp:Label>
                                                                            </span> /
                                                  <span class="badge" style="background: rgba(255, 255, 255, 0.00);padding:5px;">
                                                      <asp:Label ForeColor="black" Font-Size="large" ID="lbllinhaatual" runat="server" Text='<%#Bind("linhaatual") %>'></asp:Label>
                                                  </span>
                                                                            <asp:HiddenField runat="server" ID="hdnprocessado" Value='<%#Bind("processado") %>' />
                                                                                    <asp:HiddenField runat="server" ID="hdnarquivo" Value='<%#Bind("arquivo") %>' />
                                                                            <asp:HiddenField runat="server" ID="hdnnrseq" Value='<%#Eval("nrseq") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:ButtonField ButtonType="Image" runat="server" HeaderText="Excluir" CommandName="excluir" ImageUrl="~\img\excluir.png">
                                                                        <ControlStyle Width="27px" Height="27px"></ControlStyle>


                                                                    </asp:ButtonField>
                                                                          <asp:ButtonField ButtonType="Image" runat="server" HeaderText="Não processar" CommandName="naoprocessar" ImageUrl="~/img/icon-voltar.png">
                                                                        <ControlStyle Width="27px" Height="27px"></ControlStyle>


                                                                    </asp:ButtonField>
                                                                </Columns>
                                                                <%-- <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
           <HeaderStyle BackColor="#A02150" Font-Bold="True" Font-Names="Calibri" Font-Size="11pt" ForeColor="White" HorizontalAlign="Left"></HeaderStyle>--%>
                                                                <%--<PagerStyle BackColor="#FFFFCC" BorderStyle="Solid" BorderWidth="1px" ForeColor="#330099" HorizontalAlign="Center" />--%>

                                                                <%--  <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />--%>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6 text-center">
                                                            <br />
                                                            <asp:Button ID="btnprocessararq" runat="server" CssClass="btn btn-primary " Text="Processar Arquivo" />
                                                        </div>
                                                        <div class="col-lg-6 text-center">
                                                            <br />
                                                            <asp:Button ID="btnatualizar" runat="server" CssClass="btn btn-warning " Text="Atualizar" />
                                                        </div>
                                                    </div>
                                                    </div>

            </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <br />

                    <div class="row text-align-center">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-2">
                            <asp:Button ID="btnSalvar" runat="server" Text="Salvar" CssClass="btn btn-success" Enabled="false" />
                        </div>

                        <div class="col-lg-2">
                            <asp:Button ID="btncancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger " Enabled="true" />


                        </div>
                        <div class="col-lg-4"></div>

                    </div>
                    <br />
                    <div class="box box-primary">
                        <div class="box-header">
                            Procurar caixa
                        </div>
                        <div class="box-body">
                            <div class="row ">
                                <div class="col-lg-2"></div>

                                <div class="col-lg-2 fix-txt">
                                    <asp:Label runat="server" Text="Tipo"></asp:Label>
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="cboTipoProcura">
                                        <asp:ListItem Value="1">caixa</asp:ListItem>
                                        <asp:ListItem Value="2">Codigo</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <div class="col-lg-3 fix-txt">
                                    <asp:Label runat="server" Text="Funcionario"></asp:Label>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtprocurar"></asp:TextBox>
                                </div>
                                <div class="col-lg-1">
                                    <asp:CheckBox runat="server" Text="Inativo" CssClass="checkbox-inline" Checked="false" ID="chkexibirinativos" />
                                </div>
                                <div class="col-lg-2">
                                    <asp:LinkButton runat="server" Text="Procurar" CssClass="btn btn-primary" ID="btnprocurar"></asp:LinkButton>
                                </div>

                                <div class="col-lg-2"></div>

                            </div>
                            <br />



                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:GridView ID="GradeCaixas" runat="server" CssClass="table table-striped" ShowHeaderWhenEmpty="true" EmptyDataText="Não possui nenhuma caixa." AutoGenerateColumns="false" GridLines="None">
                                        <Columns>
                                            <asp:BoundField DataField="nrseq" HeaderText="Nr Interno" />
                                            <asp:BoundField DataField="dtcad" HeaderText="Data de Cadastro" />
                                            <asp:BoundField DataField="dtvencimento" HeaderText="Data de Vencimento" />
                                            <asp:BoundField DataField="ativo" HeaderText="Ativo" />
                                            <asp:BoundField DataField="operacao" HeaderText="Operação" />
                                            <asp:BoundField DataField="nrseqplano" HeaderText="Código" />
                                            <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                            <asp:BoundField DataField="valor" HeaderText="Valor" />
                                            <asp:BoundField DataField="parcela" HeaderText="Parcela" />
                                            <asp:BoundField DataField="qtdparcelas" HeaderText="Parcelas" />
                                            <asp:TemplateField HeaderText="Ações" runat="server">
                                                <ItemTemplate runat="server">
                                                    <asp:LinkButton ID="btneditar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                            <i class="fa fa-pencil-square-o"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton Visible="false" ID="btnrestaurar" runat="server" CssClass="btn btn-xs btn-warning" CommandName="restaurar" CommandArgument='<%#Container.DataItemIndex%>'>
                                                            <i class="fa fa-undo"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnexcluir" runat="server" CssClass="btn btn-danger btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                                                        <i runat="server" id="icogradeexcluir" class="fa fa-trash-o"></i>
                                                    </asp:LinkButton>
                                                    <asp:HiddenField runat="server" ID="hdnnrseqgrade" Value='<%#Eval("nrseq") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>





                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>


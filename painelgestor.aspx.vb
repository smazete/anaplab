﻿Imports clsSmart

Partial Class painelgestor
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco
    Dim tbx As New Data.DataTable
    Dim tabx As New clsBanco
    Dim tbp As New Data.DataTable
    Dim tabp As New clsBanco
    Private Sub painelgestor_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        lblano.Text = IIf(data.Month = 1, data.AddMonths(-1).Year, data.Year)
        If Session("nrseqcolaborador") = 0 OrElse Session("nrseqcolaborador") = "" Then
            Exit Sub
        End If
        lblnome.Text = Session("usuario")
        hdcolaboradoratual.Value = Session("nrseqcolaborador")
        montardados()



    End Sub
    Private Sub montardados()




        tb1 = tab1.conectar("select * from tbcolaboradores where nrseqgestor = " & hdcolaboradoratual.Value)

        gradepainelgeral.DataSource = tb1
        gradepainelgeral.DataBind()
        For Each row As GridViewRow In gradepainelgeral.Rows
            Dim rptsegundonivel As Repeater = row.FindControl("rptsegundonivel")
            For Each item As RepeaterItem In rptsegundonivel.Items
                Dim divbox As Object = item.FindControl("divbox")
                Dim divprogress As Object = item.FindControl("divprogress")
                Dim lblobjrpt As Label = item.FindControl("lblobjrpt")
                Dim lblfinalrpt As Label = item.FindControl("lblfinalrpt")
                Dim lblmeiorpy As Label = item.FindControl("lblmeiorpy")
                Dim btngestorrpt As LinkButton = item.FindControl("btngestorrpt")
                Dim hdgestorrpt As HiddenField = item.FindControl("hdgestorrpt")
                Dim hdhorista As HiddenField = item.FindControl("hdhorista")
                btngestorrpt.Visible = False
                If logico(hdgestorrpt.Value) = "True" Then
                    btngestorrpt.Visible = True
                End If
                btngestorrpt.CommandName = row.RowIndex
                If logico(hdhorista.Value) Then
                    If lblobjrpt.Text = 0 Then
                        divbox.Attributes.remove("class")
                        divbox.Attributes.add("class", "info-box bg-red")
                        divprogress.style.remove("width")
                        divprogress.style.add("width", "0%")
                    End If
                    If lblobjrpt.Text > 0 AndAlso lblmeiorpy.Text = 0 AndAlso lblfinalrpt.Text = 0 Then
                        divbox.Attributes.remove("class")
                        divbox.Attributes.add("class", "info-box bg-yellow")
                        divprogress.style.remove("width")
                        divprogress.style.add("width", "33%")
                    End If
                    If lblobjrpt.Text > 0 AndAlso lblmeiorpy.Text > 0 AndAlso lblfinalrpt.Text = 0 Then
                        divbox.Attributes.remove("class")
                        divbox.Attributes.add("class", "info-box bg-blue")
                        divprogress.style.remove("width")
                        divprogress.style.add("width", "66%")
                    End If
                    If lblobjrpt.Text > 0 AndAlso lblmeiorpy.Text > 0 AndAlso lblfinalrpt.Text > 0 Then
                        divbox.Attributes.remove("class")
                        divbox.Attributes.add("class", "info-box bg-green")
                        divprogress.style.remove("width")
                        divprogress.style.add("width", "100%")
                    End If
                Else
                    lblobjrpt.Text = "N/A"
                    lblmeiorpy.Text = "N/A"
                    lblfinalrpt.Text = "N/A"
                    divbox.Attributes.remove("class")
                    divbox.Attributes.add("class", "info-box bg-green")
                    divprogress.style.remove("width")
                    divprogress.style.add("width", "100%")
                End If
                'lblmeiorpy
                'lblfinalrpt
                'lblcursos

            Next
        Next
    End Sub

    Private Sub gradepainelgeral_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradepainelgeral.RowDataBound


        Select Case e.Row.RowType
            Case Is = DataControlRowType.Header
                tbx = tabx.conectar("select * from tbcolaboradores")
                tbp = tabp.conectar("SELECT * FROM vwpad_painelgestor where ativo = true and anobase = " & lblano.Text)


            Case Is = DataControlRowType.DataRow
                Dim tbproc As Data.DataRow()
                Dim arvore As TreeView = e.Row.FindControl("arvore")
                Dim rptsegundonivel As Repeater = e.Row.FindControl("rptsegundonivel")
                Dim rptdadosgestor As Repeater = e.Row.FindControl("rptdadosgestor")
                '    Dim rptpainelgrade As Repeater = e.Row.FindControl("rptpainelgrade")

                tbproc = tbp.Select(" nrseqcolaborador = " & e.Row.DataItem("nrseq"))

                If tbproc.Count > 0 Then
                    rptdadosgestor.DataSource = clonarlinhas(tbproc)
                    rptdadosgestor.DataBind()
                    For Each item As RepeaterItem In rptdadosgestor.Items
                        Dim divbox As Object = item.FindControl("divbox")
                        Dim divprogress As Object = item.FindControl("divprogress")
                        Dim lblobjrpt As Label = item.FindControl("lblobjrpt")
                        Dim lblfinalrpt As Label = item.FindControl("lblfinalrpt")
                        Dim lblmeiorpy As Label = item.FindControl("lblmeiorpy")
                        Dim hdhorista As HiddenField = item.FindControl("hdhorista")
                        If logico(hdhorista.Value) Then
                            If lblobjrpt.Text = 0 Then
                                divbox.Attributes.remove("class")
                                divbox.Attributes.add("class", "info-box bg-red")
                                divprogress.style.remove("width")
                                divprogress.style.add("width", "0%")
                            End If
                            If lblobjrpt.Text > 0 AndAlso lblmeiorpy.Text = 0 AndAlso lblfinalrpt.Text = 0 Then
                                divbox.Attributes.remove("class")
                                divbox.Attributes.add("class", "info-box bg-yellow")
                                divprogress.style.remove("width")
                                divprogress.style.add("width", "33%")
                            End If
                            If lblobjrpt.Text > 0 AndAlso lblmeiorpy.Text > 0 AndAlso lblfinalrpt.Text = 0 Then
                                divbox.Attributes.remove("class")
                                divbox.Attributes.add("class", "info-box bg-blue")
                                divprogress.style.remove("width")
                                divprogress.style.add("width", "66%")
                            End If
                            If lblobjrpt.Text > 0 AndAlso lblmeiorpy.Text > 0 AndAlso lblfinalrpt.Text > 0 Then
                                divbox.Attributes.remove("class")
                                divbox.Attributes.add("class", "info-box bg-green")
                                divprogress.style.remove("width")
                                divprogress.style.add("width", "100%")
                            End If
                        Else
                            lblobjrpt.Text = "N/A"
                            lblmeiorpy.Text = "N/A"
                            lblfinalrpt.Text = "N/A"
                            divbox.Attributes.remove("class")
                            divbox.Attributes.add("class", "info-box bg-green")
                            divprogress.style.remove("width")
                            divprogress.style.add("width", "100%")
                        End If

                        'lblmeiorpy
                        'lblfinalrpt
                        'lblcursos

                    Next
                End If



                tbproc = tbp.Select(" nrseqgestor = " & e.Row.DataItem("nrseq"))
                If tbproc.Count > 0 Then
                    '   arvore.NodeStyle.ForeColor = Drawing.Color.Black

                    rptsegundonivel.DataSource = clonarlinhas(tbproc)
                    rptsegundonivel.DataBind()
                    '  preenchearvore(clonarlinhas(tbproc), arvore)

                    'Dim gradex As New GridView
                    'gradex.AutoGenerateColumns = False
                    'gradex.GridLines = GridLines.None
                    'gradex.Attributes.Add("cssclass", "table")
                    'gradex.ShowHeader = False

                    'Dim campo1 As DataControlField

                    'Dim campox As New BoundColumn
                    'campox.DataField = "nome"




                    'gradex.Columns.Add(campo1)

                    'e.Row.Controls.Add(gradex)


                    'rptpainelgrade.DataSource = clonarlinhas(tbproc)
                    'rptpainelgrade.DataBind()
                End If
        End Select
    End Sub
    Private Sub preenchearvore(tabela As Data.DataTable, arvore As TreeView)

        Dim tbt As New Data.DataTable

        For x As Integer = 0 To tabela.Rows.Count - 1
            Dim xnode As New TreeNode
            xnode.Expanded = True
            xnode.Value = tabela.Rows(x)("nome").ToString

            Dim tbproc As Data.DataRow()
            '    Dim rptpainelgrade As Repeater = e.Row.FindControl("rptpainelgrade")
            tbproc = tbx.Select(" nrseqgestor = " & tabela.Rows(x)("nrseq").ToString)
            If tbproc.Count > 0 Then
                preenchenode(clonarlinhas(tbproc), xnode)
            End If
            arvore.Nodes.Add(xnode)
        Next

    End Sub
    Private Sub preenchenode(tabela As Data.DataTable, arvore As TreeNode)
        Dim tbt As New Data.DataTable

        For x As Integer = 0 To tabela.Rows.Count - 1
            Dim xnode As New TreeNode
            xnode.Value = tabela.Rows(x)("nome").ToString
            Dim tbproc As Data.DataRow()
            '    Dim rptpainelgrade As Repeater = e.Row.FindControl("rptpainelgrade")
            tbproc = tbx.Select(" nrseqgestor = " & tabela.Rows(x)("nrseq").ToString)
            If tbproc.Count > 0 Then
                preenchenode(clonarlinhas(tbproc), xnode)
            End If
            arvore.ChildNodes.Add(xnode)
        Next
    End Sub
    Public Sub rptcommand1(sender As Object, e As EventArgs)


        ' If e.CommandName.ToLower = "abrir" Then
        For Each row As GridViewRow In gradepainelgeral.Rows

            Dim rptsegundonivel As Repeater = row.FindControl("rptsegundonivel")



            For Each item As RepeaterItem In rptsegundonivel.Items
                '    Dim hdlinhagrade As HiddenField = item.FindControl("hdlinhagrade")
                Dim btngestorrpt As LinkButton = item.FindControl("btngestorrpt")
                Dim hdpainelgradenrseq As HiddenField = item.FindControl("hdpainelgradenrseq")
                Dim lblnomeusuariogradepainel As Label = item.FindControl("lblnomeusuariogradepainel")
                If sender.CommandName = row.RowIndex Then
                    If btngestorrpt.CommandArgument = sender.CommandArgument Then
                        'gravalog("View the BID the " & hdpainelgradenrseq.Value & " in follow up form", "Action")
                        hdcolaboradoratual.Value = hdpainelgradenrseq.Value
                        lblnome.Text = lblnomeusuariogradepainel.Text
                        montardados()
                        Exit Sub
                    End If
                End If



            Next
        Next
        sm("swal({title: 'Attention!',text: '" & sender.CommandArgument & "',type: 'error',confirmButtonText: 'OK'})", "swal")

        ' End If
    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub btnpainelgestor_Click(sender As Object, e As EventArgs) Handles btnpainelgestor.Click
        lblano.Text = IIf(data.Month = 1, data.AddMonths(-1).Year, data.Year)
        If Session("nrseqcolaborador") = "" Then
            Exit Sub
        End If
        lblnome.Text = Session("usuario")
        hdcolaboradoratual.Value = Session("nrseqcolaborador")
        montardados()
    End Sub

    Private Sub gradepainelgeral_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradepainelgeral.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradepainelgeral.Rows(index)

        Dim rptsegundonivel As Repeater = row.FindControl("rptsegundonivel")

        Dim itemx As RepeaterItem = rptsegundonivel.NamingContainer
    End Sub
End Class

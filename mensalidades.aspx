﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mensalidades.aspx.vb" MasterPageFile="~/masterpage.master" Inherits="mensalidades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Controle de Mensalidades </b>
        </div>

        <div class="box-body" visible="false" runat="server">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <asp:LinkButton ID="btnnovo" runat="server" CssClass="btn btn-warning "> <i class="fas fa-plus-circle"></i>Novo</asp:LinkButton>
                    <br>
                </div>
            </div>
            <asp:UpdatePanel ID="updateemails" runat="server">
                <ContentTemplate>
                    <%-- envio privatvo --%>
                    <div class="row" runat="server" id="divprivado" visible="true">
                        <div class="row">
                            <div class="col-lg-4">
                                <asp:Label Text="Buscar Por" runat="server" />
                                <asp:DropDownList runat="server" CssClass="form-control" ID="DropDownList1">
                                    <asp:ListItem Text="Codigo" Value="nrseq" />
                                    <asp:ListItem Text="Nome" Value="nome" />
                                    <asp:ListItem Text="Matricula" Value="matricula" />
                                    <asp:ListItem Text="CPF" Value="cpf" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-6">
                                <br />
                                <asp:TextBox runat="server" ID="txtbusca" CssClass="form-control" />
                            </div>

                            <div class="col-lg-2 text-center">
                                <br>
                                <asp:LinkButton ID="btnbuscar" runat="server" CssClass="btn btn-primary"><i class="fa fa-searc"></i> Buscar</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <asp:GridView ID="gradeassociados" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhum registro Encontrado !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                        <asp:LinkButton ID="usar" runat="server" CssClass="btn btn-primary btn-xs" CommandName="usar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                <asp:BoundField DataField="nome" HeaderText="Nome" />
                                <asp:BoundField DataField="cpf" HeaderText="CPF" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="row">
                <div class="col-lg-2">
                    Nrseq
                    <br>
                    <asp:TextBox ID="txtnrseq" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Nrseqcliente
                    <br>
                    <asp:TextBox ID="txtnrseqcliente" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-1">
                    Mes
                    <br>
                    <asp:TextBox ID="txtmes" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Ano
                    <br>
                    <asp:TextBox ID="txtano" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Statuspg
                    <br>
                    <asp:TextBox ID="txtstatuspg" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Frmpg
                    <br>
                    <asp:TextBox ID="txtfrmpg" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Dtpg
                    <br>
                    <asp:TextBox ID="txtdtpg" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Statusfiliacao
                    <br>
                    <asp:TextBox ID="txtstatusfiliacao" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Dtemissao
                    <br>
                    <asp:TextBox ID="txtdtemissao" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    Dtvencimente
                    <br>
                    <asp:TextBox ID="txtdtvencimente" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <%--                <div class="col-lg-2">
                    Arquivo
                    <br>
                    <asp:TextBox ID="txtarquivo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>--%>
                <div class="col-lg-2">

                    <asp:CheckBox ID="txtautomatico" runat="server" Text=" Automatico"></asp:CheckBox>
                    <br />
                    <asp:CheckBox ID="txtpago" runat="server" Text=" Pago"></asp:CheckBox>
                </div>
                <div class="col-lg-2">
                    <asp:CheckBox ID="txtsegundavia" runat="server" Text=" Segunda via"></asp:CheckBox><br />
                    <asp:CheckBox ID="txtestornado" runat="server" Text=" Estornado"></asp:CheckBox>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <br>
                    <asp:LinkButton ID="btnsalvar" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i>Salvar</asp:LinkButton>
                </div>
            </div>
            <div class="row">
                <br>
            </div>
        </div>

            <div class="box box-header">
                
        <div class="row" runat="server" visible="false">
            <div class="col-lg-2"></div>
            <div class="col-lg-2 with-border">
                <asp:LinkButton ID="btncomgerarlote" runat="server" CssClass="btn btn-primary btn-sm" title="Voce não tem permissao para executar esta ação"> Gerar Lote
                                <i class=""></i></asp:LinkButton>
                <asp:LinkButton ID="btngerarlote" runat="server" CssClass="tn-outline-primary btn-sm" data-toggle="modal" data-target=".modalgerarlote">
                                <i class="fa fa-info-circle"></i></asp:LinkButton>
            </div>

            <div class="col-lg-2">
                <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-primary btn-sm" Text="Gerarlote">
                                <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton5" runat="server" CssClass="tn-outline-primary btn-sm" data-toggle="modal" data-target=".modalbaixaperiodo">
                                <i class="fa fa-info-circle"></i></asp:LinkButton>
            </div>

            <div class="col-lg-2">
                <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-primary btn-sm" Text="Gerarlote">
                                <i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                <asp:LinkButton ID="LinkButton6" runat="server" CssClass="tn-outline-primary btn-sm" data-toggle="modal" data-target=".modalbaixaperiodo">
                                <i class="fa fa-info-circle"></i></asp:LinkButton>
            </div>
        </div>
   
            </div>
            <div class="row">
                <div class="col-lg-8 table-bordered">
                    <div class=" col-lg-12">
                        <div class="col-lg-3">
                            <b>
                                <asp:Label Text="Buscar Por" runat="server" /></b>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="cbobusca">                                
                                    <asp:ListItem Text="Conta" Value="contacorrente" />
                                <asp:ListItem Text="Codigo" Value="nrseqcliente" />
                                <asp:ListItem Text="Nome" Value="nome" />
                                <asp:ListItem Text="Matricula" Value="matricula" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-6">
                            <b>
                                <asp:Label Text="Descrição" runat="server" /></b>
                            <asp:TextBox runat="server" ID="txtdescricao" CssClass="form-control" />
                        </div>
                        
                        <div class="col-lg-3">
                            <b><asp:Label Text="Data Baixa" runat="server" /></b>
                                <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdtbaixa" runat="server" CssClass="form-control"></asp:TextBox>       
                        </div>
                    </div>
                    <div class=" col-lg-12" visible="false" runat="server"  >
                        <div class="col-lg-2" visible="false">
                            <b>
                                <asp:Label Text="Mês" runat="server" /></b>
                            <asp:TextBox runat="server" ID="txtmesbusca" CssClass="form-control" MaxLength="2"  />

                        </div>
                        <div class="col-lg-2" visible="false">
                            <b>Ano</b>
                            <asp:TextBox runat="server" ID="txtanobusca" CssClass="form-control" MaxLength="4" />
                        </div>
                        <div class="col-lg-3" visible="false">
                            <b>
                                <asp:Label Text="Status " runat="server" /></b>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="cbostatus">
                                <asp:ListItem Text="Todas" Value="4" />
                                <asp:ListItem Text="Aberta" Value="0" />
                                <asp:ListItem Text="Pago" Value="1" />
                                <asp:ListItem Text="Isento" Value="3" />
                                <asp:ListItem Text="Estornada" Value="2" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-3" visible="false">
                            <b>
                                <asp:Label Text="F.pagamento" runat="server" /></b>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="cboformapg" Enabled="false">
                                <asp:ListItem Text="Todos" Value="4" />
                                <asp:ListItem Text="Boleto" Value="0" />
                                <asp:ListItem Text="Debito" Value="1" />
                                <asp:ListItem Text="Transferencia" Value="2" />
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <div class=" col-lg-2">
                    <div class="col-lg-12" visible="false" runat="server">
                        <b>
                            <asp:Label Text="Selecionar" runat="server" /></b>
                        <asp:RadioButtonList runat="server" ID="radstatus">
                            <asp:ListItem Text="Todas" Value="4" title="Buscar por todas as mensalidades" Selected />
                            <asp:ListItem Text="Ativas" Value="1" title="Buscar somente Mensalidades ativas" />
                            <asp:ListItem Text="Excluidas" Value="0" title="Buscar somente mensalidades excluidas" />

                        </asp:RadioButtonList>
                    </div>
                </div>

                <div class=" col-lg-2">
                    <br />
                    <asp:LinkButton ID="btnbuscando" runat="server" CssClass="btn btn-primary"> <i class="fa fa-search"></i> Buscar </asp:LinkButton>
                </div>
            </div>
  
    <div class="row">
        <br>
        <%-- Grade de mensalidades --%>
           <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right">
                            <!-- Buttons, labels, and many other things can be placed here! -->

                            <!-- Here is a label for example -->
                            <span class="label label-primary">
                                <asp:HiddenField runat="server" ID="hdnrseq" />
                                <asp:HiddenField runat="server" ID="hdmatricula" />
                                <asp:Label ID="lblncodigo" Text="Nrseq" runat="server" style="font-size:18px;">0</asp:Label></span>
                        </div>
                        <!-- /.box-tools -->
                    </div>
        <asp:GridView ID="grademensalidade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma mensalidade encontrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                        <asp:HiddenField ID="hdvalor" runat="server" Value='<%#Bind("valor")%>'></asp:HiddenField>
                        <asp:LinkButton ID="visualizar" runat="server" CssClass="btn btn-info " CommandName="visualizar" CommandArgument='<%#Container.DataItemIndex%>' Visible="false">
                            <i class="fa fa-eye"></i></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                <asp:BoundField DataField="nome" HeaderText="Cliente" />
                <asp:BoundField DataField="Ativo" HeaderText="Ativo" />
                <asp:BoundField DataField="statuspg" HeaderText="Status" />
                <asp:BoundField DataField="mes" HeaderText="Mes" />
                <asp:BoundField DataField="ano" HeaderText="Ano" />
                <asp:BoundField DataField="Valor" HeaderText="Valor" />
                <asp:BoundField DataField="dtpg" HeaderText="Data" />
                
                <asp:TemplateField>
                    <ItemTemplate>
                <p>
                                        <asp:CheckBox id="chkok"  runat="server" /> 
                    </p>
                        <div runat="server" id="divtexto">
                            <asp:LinkButton ID="btnativo" runat="server" CssClass="btn btn-primary btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>Ativar
                            <i class="fa fa-check"></i> </asp:LinkButton>
                        </div>
                        <div runat="server" id="divbuttons">
                            <asp:LinkButton ID="aberto" runat="server" CssClass="btn btn-default " CommandName="aberto" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-ban"></i> Aberto</asp:LinkButton>
                            <%--                                    <asp:LinkButton ID="excluir" runat="server" CssClass="btn btn-danger " CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-ban"></i> Cancelar</asp:LinkButton>--%>
                            <asp:LinkButton ID="pagar" runat="server" CssClass="btn btn-success  " CommandName="pagar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-dollar"></i> Baixa</asp:LinkButton>
                            <asp:LinkButton ID="estornar" runat="server" CssClass="btn btn-warning " CommandName="estornar" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-step-backward"></i> Estornar</asp:LinkButton>
                            <asp:LinkButton ID="isento" runat="server" CssClass="btn btn-primary" CommandName="isento" CommandArgument='<%#Container.DataItemIndex%>'>
                            <i class="fa fa-step-backward"></i> Isento</asp:LinkButton>
                        </div>

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <center>
        <asp:LinkButton Text="Baixar selecionados" runat="server" CssClass="btn btn-success btn-lg" ID="btnbaixaselect" />

            </center>
    </div>

    <%-- gerar lote modal --%>

    <div class="modal fade modalgerarlote" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    Gerar Lote
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    como usar a o botão
                </div>
            </div>
        </div>
    </div>
    <%-- Baixa Por Periodo --%>
    <div class="modal fade modalbaixaperiodo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    Baixa Por Periodo
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    como usar a o botão
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿Imports clssessoes
Imports System.IO
Imports clsSmart
Partial Class votacao_eleicoes
    Inherits System.Web.UI.Page
    Private Sub votacao_eleicoes_Load(sender As Object, e As EventArgs) Handles Me.Load
        divfinal.Style.Add("display", "none")
        carregar()
        lblassociado.Text = Session("associado")




    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub rptchapas_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptchapas.ItemCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim ri As RepeaterItem = rptchapas.Items(index)
        Dim nrseq As HiddenField = ri.FindControl("hdnrseqrpt")
        Dim chapa As HiddenField = ri.FindControl("hdnchaparpt")

        If e.CommandName = "votar" Then
            hdnrseqsel.Value = nrseq.Value
            divconfirmar.Style.Remove("display")
        End If
    End Sub

    Private Sub rptchapas_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptchapas.ItemDataBound
        Dim div As HtmlControl = e.Item.FindControl("divchapa")
        Dim btn As LinkButton = e.Item.FindControl("btnvotar")
        Dim votou As HiddenField = e.Item.FindControl("hdnvotourpt")
        Dim temvoto As HiddenField = e.Item.FindControl("hdntemvotorpt")
        Dim hdnchaparpt As HiddenField = e.Item.FindControl("hdnchaparpt")
        Dim lblchapafixa As Label = e.Item.FindControl("lblchapafixa")
        Dim v As Integer = votou.Value
        Dim tv As Integer = temvoto.Value
        If sonumeros(hdnchaparpt.Value) = "0" Then
            lblchapafixa.Visible = False
        End If
        If v = 1 Then
            div.Attributes.Add("class", "small-box bg-green")
            btn.Visible = False
        ElseIf tv = 1 Then
            div.Attributes.Add("class", "small-box bg-gray")
            btn.Visible = False
        Else
            div.Attributes.Add("class", "small-box bg-aqua")
        End If
    End Sub

    Private Sub carregar()
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim nrseqassociado = Session("idassociado")

        tb1 = tab1.conectar("SELECT numero, descricao, nrseq, (SELECT count(nrseq) FROM tbvotacao_eleicoes WHERE nrseqassociado = '" & nrseqassociado & "' AND nrseqchapa = V.nrseq) As votou, (SELECT count(nrseq) FROM tbvotacao_eleicoes WHERE nrseqassociado = '" & nrseqassociado & "') As temvoto FROM tbchapas_eleicoes V WHERE ativo = true  OR numero LIKE 'BRANCO' OR numero LIKE 'NULO' ORDER BY numero ASC")

        rptchapas.DataSource = tb1
        rptchapas.DataBind()

        If tb1.Rows.Count > 0 AndAlso tb1.Rows(0)("temvoto").ToString = "1" Then
            divfinal.Style.Remove("display")
            divgeral.Style.Add("display", "none")
        Else
            tb1 = tab1.conectar("SELECT * FROM  tbmensalidades where nrseqcliente = " & nrseqassociado & " and statuspg = 0 and (mes <= 10 and ano <= 2020)")
            If tb1.Rows.Count > 0 Then
                rptchapas.DataSource = Nothing
                rptchapas.DataBind()
                rptchapas.Visible = False
                lbllbemvindo.Text = "Ops! Desculpe, você possui mensalidades em aberto! Regularize para votar! "
            Else
                lbllbemvindo.Text = "Por favor, selecione seu candidato e pressione Confirmar! "
            End If
        End If
    End Sub

    Private Sub btnconfirmar_Click(sender As Object, e As EventArgs) Handles btnconfirmar.Click
        Dim xvotos As New clsvotacao_eleicoes
        xvotos.Nrseqchapa = hdnrseqsel.Value
        xvotos.Nrseqassociado = Session("idassociado")
        If Not xvotos.novo() Then
            Exit Sub
        End If
        divconfirmar.Style.Remove("display")

        Dim uEncode As New UnicodeEncoding()
        Dim bytClearString() As Byte = uEncode.GetBytes(Session("idmatricula"))
        Dim sha As New _
        System.Security.Cryptography.SHA1Managed()
        Dim hash() As Byte = sha.ComputeHash(bytClearString)

        Dim wcnome As String = alteranome(Server.MapPath("~") & "eleicoes\chaves\" & sonumeros(data).Trim & sonumeros(hora(True)).Trim & hdchapa.Value.Trim & ".txt")

        Dim file As New System.IO.StreamWriter(wcnome)

        file.WriteLine(Convert.ToBase64String(hash) & " - " & HttpContext.Current.Request.UserHostAddress & " - " & hojecomhora())
        file.Close()
        sm("swal.fire({title: 'Sucesso!',text: 'Voto efetuado com sucesso',type: 'success',confirmButtonText: 'OK'})", "swal")
        carregar()
        divfinal.Style.Remove("display")
        divgeral.Style.Add("display", "none")
    End Sub

    Private Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        divconfirmar.Style.Add("display", "none")
    End Sub
End Class

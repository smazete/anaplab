﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="seguranca.aspx.vb" Inherits="seguranca" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <div class="box box-danger" style="margin-left: 1rem; margin-right: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header ">
            <b>Alterar minha senha</b>
        </div>
        <asp:UpdatePanel ID="updatesenha" runat="server">
            <ContentTemplate>
                <div class="box-body">

                    <br />
                    <br />

                    <asp:HiddenField ID="hdorigem" runat="server" />
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            Atenção,
            <asp:Label ID="lblusuario" runat="server" Text="usuario"></asp:Label>
                            : Por razões de segurança, por favor altere sua senha !
                        </div>
                    </div>
                    <br />
                    <br />

                    <div class="row">
                        <div class="col-lg-2 ">
                            <asp:Image ImageUrl="../img/mulher3.png" runat="server" />
                        </div>
                        <div class="col-lg-4 " style="">
                           Por favor, observe as condições abaixo:<br />
                            <br />
                            <asp:Image ID="imgNumerosLetras" runat="server" ImageUrl="~/IMG/button_cancel.png" />
                            &nbsp;Números e letras<br />
                            <asp:Image ID="imgSimbolos" runat="server" ImageUrl="~/IMG/button_cancel.png" />
                            &nbsp;Símbolos especiais<br />
                            <asp:Image ID="imgLetraMaiuscula" runat="server" ImageUrl="~/IMG/button_cancel.png" />
                            &nbsp;Uma letra maiúscula<br />
                            <asp:Image ID="imgSenhasConferem" runat="server" ImageUrl="~/IMG/button_cancel.png" />
                            &nbsp;As senhas digitadas não conferem<br />
                            <asp:Image ID="imgSenhaAntiga" runat="server" ImageUrl="~/IMG/button_cancel.png" />
                            &nbsp;A senha antiga não confere
                        </div>
                        <div class="col-lg-4 ">
                            <asp:Label ID="Label1" runat="server" Text="Senha antiga:" ForeColor="black"></asp:Label>
                            <asp:TextBox ID="txtSenhaAntiga" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Label ID="Label2" runat="server" Text="Nova senha:" ForeColor="black"></asp:Label>
                            <asp:TextBox ID="txtSenhaNova" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <br />
                            <br />
                            <asp:Label ID="Label3" runat="server" Text="Confirme nova senha:" ForeColor="black"></asp:Label>

                            <asp:TextBox ID="txtConfirmar" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <br />
                            <br />
                            <div class="text-center">
                                   <asp:Button ID="btnConfirmar" runat="server" Text="Sim, altere minha senha, por favor!" CssClass="btn btn-danger " />
                                 <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatesenha">
                                            <ProgressTemplate >
                                                <br />
                                                <center>
                                              <img src="../img/carregando1.gif" width="50px" height="50px" />
                                                    </center>
                                                </ProgressTemplate>
                                        </asp:UpdateProgress>
                            </div>
                           
                        </div>
                    </div>
                    <div class="row" style="background: black; color: gold;">
                        <div class="col-lg-12 text-center">
                            <br />
                            <asp:Label ID="Label4" runat="server" Text="SmartCode Soluções em Softwares"></asp:Label>
                            <br />
                            <asp:Label ID="Label5" runat="server" Text="Suport: +55 (41) 3385-1270"></asp:Label>
                            <br />
                            <asp:Label ID="Label6" runat="server" Text="E-mail: suporte@smartcodesolucoes.com.br"></asp:Label>
                            <br />
                        </div>
                    </div>
                  
                    <br />
                    <br />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>


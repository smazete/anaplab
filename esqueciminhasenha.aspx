﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="esqueciminhasenha.aspx.vb" MasterPageFile="~/MasterPage2.master" Inherits="esqueciminhasenha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
    <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="/dist/js/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>

    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />

    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/JScriptmascara.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="col-lg-2 "></div>
        <div class="col-lg-8 ">
            <br>
            <br>
            <br>
            <center>
          <div style="text-align:center"><h3> Associação Nacional dos Participantes do PB1 da PREVI - ANAPLAB</h3></div>    </center>
            <br>
            <br>
            <br>
            <center><b><asp:label style="font-size:20px; color:blue; " Text="Pedido de envio de senha." runat="server" /></b></center>
            <div class="row">
                <h3>
                    <asp:Label Text="CPF:" runat="server" style="color:darkslateblue" />
                </h3>
                <div class="col-lg-6 ">
                      <asp:TextBox runat="server" type="text" CssClass="form-control cnpjcpf" ID="txtcpf" ClientIDMode="Static" onkeyup="formataCnpjCpf(this,event);" MaxLength="14"></asp:TextBox>
                </div>
            </div>
            <div class="row">
                <h3>
                    <asp:Label Text="E-mail cadastrado:" runat="server" style="color:darkslateblue" />
                </h3>
                <div class="col-lg-8 ">
                    <asp:TextBox runat="server" ID="txtemail" CssClass="form-control" />
                </div>
            </div>
            <br />
            <center>
                <asp:LinkButton id="btnenviar" Text="Enviar" CssClass="btn btn-info" runat="server" />
                                                        </center>
            <br />
            <br />
            <br />
            <br />
            <br />


        </div>
        <br>
        <div class="col-lg-2"></div>

    </div>


</asp:Content>


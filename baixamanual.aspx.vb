﻿Imports clsSmart
Imports System.IO
Imports smboletos

Partial Class baixamanual
    Inherits System.Web.UI.Page

    Private Sub baixamanual_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then
            Exit Sub
        End If
        abastecer()
        carregabaixas()
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub

    Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click
        carregabaixas()
    End Sub

    Private Sub abastecer()
        Dim dateInMay As New System.DateTime(1993, 5, 31, 12, 14, 0)

        txtdtbaixa.Text = FormatDateTime(hoje())

    End Sub

    Public Sub carregabaixas()
        Dim consulta As String

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco



        tb1 = tab1.conectar("select * from tbimportcsv Where data = '" & formatadatamysql(txtdtbaixa.Text) & "' and nrseqcliente <> '' and nrseqmensalidade is null ")


        grademanual.DataSource = tb1
        grademanual.DataBind()

        dgexcel.DataSource = tb1
        dgexcel.DataBind()


    End Sub

    Private Sub grademanual_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grademanual.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grademanual.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseq")

        If e.CommandName = "excluir" Then

            Dim ximportcsv As New clsimportcsv

            ximportcsv.Nrseq = nrseq.Value

            If Not ximportcsv.procurar() Then
                sm("swal({title: 'Error!',text: '" & ximportcsv.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If
        End If

        If e.CommandName = "mensalidade" Then

            Dim xAssociados As New clsAssociado

            xAssociados.Nrseq = nrseq.Value

            If Not xAssociados.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If
        End If


        If e.CommandName = "Joias" Then

            Dim xAssociados As New clsAssociado

            xAssociados.Nrseq = nrseq.Value

            If Not xAssociados.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

        End If

    End Sub


    Private Sub grademanuale_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grademanual.RowDataBound
        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        Dim acoes As HtmlGenericControl = e.Row.FindControl("divacoes")

        If e.Row.Cells(5).Text = "1" Then
            e.Row.Cells(5).Text = "VINCULADO"
            acoes.Visible = False
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        ElseIf e.Row.Cells(5).Text = "0" Then
            e.Row.Cells(5).Text = "SEM VINCULO"
            acoes.Visible = True
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        End If



    End Sub

End Class

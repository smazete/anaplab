﻿Imports System.IO
Imports clsSmart
Imports System.Windows
Imports System.IO.Compression

Partial Class acoes_judiciais
    Inherits System.Web.UI.Page

    Private Sub acoes_judiciais_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then

            Exit Sub
        End If

        divacaoaberta.Visible = False

        divinformativo.Visible = True
        carregarpt()
    End Sub

    'Dim xCaixapostal As New clscaixapostal

    'xCaixapostal.Nrseq = hdrseqacao.Value

    'If Not xCaixapostal.procurarassociados() Then
    '    sm("swal({title: 'Error!',text: '" & xCaixapostal.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
    '    Exit Sub
    'End If

    'txtassunto.Text = xCaixapostal.Assunto

    'Dim wcnomearq As String = Server.MapPath("~") & "arqshtml\cxpostal.html"
    'wcnomearq = alteranome(wcnomearq)
    'Dim arquivo As New StreamWriter(wcnomearq)
    'arquivo.Write(xCaixapostal.Texto)
    'arquivo.Close()

    'zframe.Attributes.Add("src", "/arqshtml/" & mARQUIVO(wcnomearq))
    'txtdtcad.Text = formatadatamysql(xCaixapostal.Dtcad, True, True)

    'divgrademenssagem.Visible = True

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub carregarpt()

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbacoesjudiciais where ativo = 1")

        rptacoes.DataSource = tb1
        rptacoes.DataBind()

    End Sub

    Private Sub rptacoes_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptacoes.ItemCommand
        Dim Nrseqacao As HiddenField = e.Item.FindControl("hdnrseqacao")
        Dim xAcao As New clsacoesjudiciais


        If e.CommandName = "abrir" Then

            xAcao.Nrseq = Nrseqacao.Value
            hdselecaoacao.Value = Nrseqacao.Value

            If Not xAcao.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAcao.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            If xAcao.Arquivotermo = "" Then
                btntermo.Visible = False
            Else
                btntermo.Visible = True
            End If

            If xAcao.Arquivocontrato = "" Then
                btncontrato.Visible = False
            Else
                btncontrato.Visible = True

            End If

            Dim wcnomearq As String = Server.MapPath("~") & "arqshtml\acoesjudiciais.html"
            wcnomearq = alteranome(wcnomearq)
            Dim arquivo As New StreamWriter(wcnomearq)

            arquivo.Write(xAcao.Descricao)
            arquivo.Close()

            zframe.Attributes.Add("src", "/arqshtml/" & mARQUIVO(wcnomearq))

            divacaoaberta.Visible = True
            divinformativo.Visible = False
            divrpt.Visible = False

        End If

    End Sub

    Private Sub btnvoltar_Click(sender As Object, e As EventArgs) Handles btnvoltar.Click
        hdselecaoacao.Value = ""
        divacaoaberta.Visible = False
        divinformativo.Visible = True
        divrpt.Visible = True
        carregarpt()
    End Sub

    Private Sub btncontrato_Click(sender As Object, e As EventArgs) Handles btncontrato.Click

        Dim xAcao As New clsacoesjudiciais

        xAcao.Nrseq = hdselecaoacao.Value

        If Not xAcao.procurar() Then
            sm("swal({title: 'Error!',text: '" & xAcao.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        Dim wcarquivo As String = xAcao.Arquivocontrato
        Response.ContentType = "application/octet-stream"
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment; filename=" & wcarquivo)
        Response.TransmitFile(Server.MapPath("~\downloadsacoes\contrato\") & wcarquivo)
        Response.End()

    End Sub




    Private Sub btntermo_Click(sender As Object, e As EventArgs) Handles btntermo.Click

        Dim xAcao As New clsacoesjudiciais

        xAcao.Nrseq = hdselecaoacao.Value

        If Not xAcao.procurar() Then
            sm("swal({title: 'Error!',text: '" & xAcao.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        Dim wcarquivo As String = xAcao.Arquivotermo
        Response.ContentType = "application/octet-stream"
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment; filename=" & wcarquivo)
        Response.TransmitFile(Server.MapPath("~\downloadsacoes\termo\") & wcarquivo)
        Response.End()

        sm("swal({title: 'Atenção!',text: 'Não foi possivel Baixar o documento',type: 'error',confirmButtonText: 'OK'})", "swal")


    End Sub



End Class

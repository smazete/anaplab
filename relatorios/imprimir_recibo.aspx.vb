﻿Imports System.Data
Imports System.IO
Imports clsSmart

Partial Class imprimir_recibo

    Inherits System.Web.UI.Page
    Dim tabmensalidades As New clsBanco
    Dim tbmensalidades As New Data.DataTable
    Dim sss As Decimal = 0

    Dim tabelaClientes As String = "tbclientes"
    Dim tabelaCaixaDth As String = "tbcaixasdth"
    Dim tableagendamedica As String = "tbagendamedica"


    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function

    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function

    Private Sub imprimir_recibo_Load(sender As Object, e As EventArgs) Handles Me.Load


        Dim nrseq As String = Request.QueryString("c")
        Dim nrseqdetalhe As String = Request.QueryString("e")
        Dim referente As String = Request.QueryString("r")
        '   Dim referente As String = Request.QueryString("t")

        If nrseq = "" Then
            Response.Redirect("/agendamedica.aspx")
        End If
        obterDados(nrseq, nrseqdetalhe, referente)
    End Sub

    Private Sub obterDados(nrseq As String, nrseqdetalhe As String, referente As String)
        Dim nomecliente As String
        Dim valortela As String
        Dim empresaenome As String 'para empresa
        Dim cidadeempresa As String

        Dim tb1 As New DataTable
        Dim tab1 As New clsBanco


        tb1 = tab1.conectar("SELECT * FROM " & tableagendamedica & " WHERE nrseq = '" & nrseq & "'")

        nomecliente = tb1.Rows(0)("paciente").ToString

        spnReferente.InnerText = "Consulta Médica"
        valortela = tb1.Rows(0)("valor").ToString
        spnNomeCliente.InnerText = nomecliente
        spnValor.InnerText = valortela
        spnValorExtenso.InnerText = NumeroToExtenso(valortela)

        tb1 = tab1.conectar("SELECT cidade, nomecliente FROM tbconfig where liberado = 1")


        empresaenome = tb1.Rows(0)("cidade").ToString()
        spnCidade.InnerText = empresaenome
        cidadeempresa = tb1.Rows(0)("nomecliente").ToString()
        spnNomeEmpresa.InnerText = cidadeempresa


    End Sub

End Class

﻿
Partial Class relatorios_aniversariantes
    Inherits System.Web.UI.Page

    Private Sub relatorios_aniversariantes_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack Then Exit Sub
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        tb1 = tab1.conectar(Session("sql"))

        gradeclientes.DataSource = tb1
        gradeclientes.DataBind()

    End Sub

    Private Sub gradeclientes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeclientes.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub

        e.Row.Cells(1).Text = FormatDateTime(e.Row.Cells(1).Text, DateFormat.ShortDate)
    End Sub
End Class

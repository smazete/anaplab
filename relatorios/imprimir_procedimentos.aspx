﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/relatorios/MasterPrint.master" CodeFile="imprimir_procedimentos.aspx.vb" Inherits="imprimir_procedimentos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css">
    <script src="../bower_components/jquery/dist/jquery.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" runat="Server">
    
    <asp:HiddenField ID="hdsql" runat="server" />

    <div class="row blue lighten-5 bordered-full">
        <div class="col-lg-12">
            <br />
            <div class="col s6 left-align">
                <h3 id="empresa" runat="server">{empresa}</h3>
            </div>
            <div class="col s6 right-align">
                <img src="../../img/logo.png">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <asp:UpdatePanel ID="updatepanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gdvListaProcedimentos" runat="server" CssClass="table-bordered"  AutoGenerateColumns="false" GridLines="Both">
                        <Columns>

                            <asp:BoundField DataField="nrseq" HeaderText="Código" />
                            <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                            <asp:BoundField DataField="valorcusto" HeaderText="Valor Custo" />
                            <asp:BoundField DataField="valorfinal" HeaderText="Valor Final" />
                            <asp:BoundField DataField="codigoamb" HeaderText="Código AMB" />
                            <asp:BoundField DataField="codigosus" HeaderText="Código TUSS" />
                            <asp:BoundField DataField="percobplano" HeaderText="% Cobertura Plano" />
                            <asp:BoundField DataField="valorcobplano" HeaderText="R$ Cobertura do Plano" />
                            <asp:BoundField DataField="naoincluircaixa" HeaderText="Incluído o valor no caixa" />
                            <asp:BoundField DataField="plano" HeaderText="Plano" />

                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

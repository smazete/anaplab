﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="caixa.aspx.vb" Inherits="restrito_relatorios_caixa"  MasterPageFile="~/relatorios/MasterPrint.master"  %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" runat="Server">
         
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <asp:HiddenField ID="hdsql" runat="server" />
   
              <div class="row">
                <div class="col s12 blue lighten-5 bordered-full">
                    <br />
                      <div class="col s6 left-align">
                          <h3 id="empresa" runat="server">Relatorio de Caixa</h3>
                      </div>
                      <div class="col s6 right-align">
                          <img src="../../img/logo.png">
                     </div>
                     <div class="col s6 right-align">
                         <strong>Relatorio De Caixa</strong>
                     </div>
               </div>
            </div>

                <div class="col s10" runat="server">
                     
                    <asp:GridView ID="gdvProcuraDeCaixas" runat="server" CssClass="bordered" AutoGenerateColumns="false" GridLines="None">
                        <Columns>
                            
                            <asp:BoundField DataField="codigo" HeaderText="ID Caixa" />
                            <asp:BoundField DataField="usuario" HeaderText="Funcionario" />
                            <asp:BoundField DataField="data" HeaderText="Data Caixa" />
                            <asp:BoundField DataField="total" HeaderText="Valor Final" />
                            <asp:BoundField DataField="fech" HeaderText="Status Caixa"  />

                        </Columns>
                    </asp:GridView>
                </div>
            
    
</asp:Content>


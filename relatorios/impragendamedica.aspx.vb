﻿
Imports System.Data
Imports System.IO
Imports SelectPdf
Imports clsSmart
Partial Class relatorios_impragendamedica
    Inherits System.Web.UI.Page

    Private Sub relatorios_impragendamedica_Load(sender As Object, ea As EventArgs) Handles Me.Load
        'teste
        'Dim tb1 As New Data.DataTable
        'Dim tab1 As New clsBanco

        'tb1 = tab1.conectar("select * from vwagendamedica_impressao where nrseqmedico='6'")

        'gdvagendamedica.DataSource = tb1
        'gdvagendamedica.DataBind() 

        Dim nm As String = Server.UrlDecode(Request.QueryString("nm")) 'nrseqmedico
        Dim e As String = Server.UrlDecode(Request.QueryString("e")) 'empresa
        Dim r As String = Server.UrlDecode(Request.QueryString("r")) 'rua
        Dim c As String = Server.UrlDecode(Request.QueryString("c")) 'cidade
        Dim t As String = Server.UrlDecode(Request.QueryString("t")) 'telefone
        Dim m As String = Server.UrlDecode(Request.QueryString("m")) 'medico
        Dim d As String = Server.UrlDecode(Request.QueryString("d")) 'data
        Dim raiz As String = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath

        Dim naorecarregar As String = Request.QueryString("nc") 'setar no request como 1 como converter para pdf
        If nm <> "" AndAlso Not IsPostBack Then 'se o nrseq do medico for diferente de 0 entao ele carrega as informacoes da pagina
            carregarListaCaixa(nm) 'carregar a grade da pagina
            preencheDados(e, r, c, t, m, d, nm) 'carrega o conteudo e seta os textos fora da grade informando os chars para popular as tags html
            If naorecarregar <> "1" Then 'habilitado no request da pagina
                converterArquivoParaPDF(e, r, c, t, m, d, nm) 'informa os chars para a funcao para popular o pdf com os chars que representam as strings
            End If
        End If
    End Sub

    Private Function converterCaracteres(str As String) As String
    End Function

    Private Sub preencheDados(e As String, r As String, c As String, t As String, m As String, d As String, nm As String)
        empresa.InnerText = e
        rua.InnerText = r
        cidade.InnerText = c
        telefone.InnerText = t
        profissional.InnerText = m
        data.InnerText = d
    End Sub

    Private Sub converterArquivoParaPDF(e As String, r As String, c As String, t As String, m As String, d As String, nm As String)
        Dim raiz As String = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
        Dim args As String = "/relatorios/impragendamedica.aspx?nc=1&nm=" & Server.UrlEncode(nm) & "&e=" & Server.UrlEncode(e) & "&r=" & Server.UrlEncode(r) & "&c=" & Server.UrlEncode(c) & "&t=" & Server.UrlEncode(t) & "&m=" & Server.UrlEncode(m) & "&d=" & Server.UrlEncode(d)

        Dim caminho = "\relatorios\arquivos\"
        Dim arquivo As String = "relatorio_agendamedica " & m & " " & DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString & ".pdf"
        Dim salvarEm As String = Server.MapPath("~/") & caminho & arquivo
        Dim FullPath = raiz & args

        Dim converter As New HtmlToPdf()
        Dim doc As PdfDocument = converter.ConvertUrl(FullPath)
        Dim pdf As Byte() = doc.Save()
        doc.Close()

        File.WriteAllBytes(salvarEm, pdf)

        Response.Redirect(raiz & caminho & arquivo)
    End Sub

    Private Sub carregarListaCaixa(nrseqmedico As String)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from vwagendamedica_impressao where nrseqmedico='" & nrseqmedico & "'")

        gdvagendamedica.DataSource = tb1
        gdvagendamedica.DataBind()

    End Sub


    Private Sub gdvagendamedica_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvagendamedica.RowDataBound



        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        Dim nrseqmedico As HiddenField = e.Row.FindControl("hdnnrseqmedico")
        If nrseqmedico IsNot Nothing Then
            tb1 = tab1.conectar("select * from vwagendamedica_impressao where nrseqmedico='" & nrseqmedico.Value & "'")
        End If
        'tb1 = tab1.conectar("select * from vwagendamedica_impressao where nrseqmedico='6'")'teste

        If e.Row.RowIndex < 0 Then Exit Sub
        Dim i As Integer = e.Row.RowIndex

        Dim lblmatricula As Label = e.Row.FindControl("lblmatricula")
        Dim lblnome As Label = e.Row.FindControl("lblnome")

        Dim hora As String = e.Row.Cells(0).Text
        Dim matriculapaciente As String = lblmatricula.Text
            Dim nomepaciente As String = lblnome.Text

            If logico(tb1.Rows(i)("atendido").ToString) = True Then
                e.Row.Cells(0).Text = "+" + e.Row.Cells(0).Text
            End If
            If logico(tb1.Rows(i)("retorno").ToString) = True Then
                e.Row.Cells(0).Text = e.Row.Cells(0).Text + "R"
            End If
        If logico(tb1.Rows(i)("ehtitular").ToString) = False Then
            e.Row.Cells(1).Text = matriculapaciente '+ "\ " + tb1.Rows(i)("nrseqpaciente").ToString
        End If
        If e.Row.Cells(5).Text = "0" OrElse e.Row.Cells(5).Text = "" Then
            e.Row.Cells(5).Text = "Não informado"
        End If

    End Sub

End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/relatorios/MasterPrint.master"  CodeFile="relatoriomensalidades.aspx.vb" Inherits="relatorios_relatoriomensalidades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" Runat="Server">

           <div class="row">
                <div class="col s12 blue lighten-5 bordered-full">
                    <br />
                      <div class="col s6 left-align">
                          <h3 id="empresa" runat="server">RELATÓRIO DE MENSALIDADES.</h3>
                          <br />
                          <asp:Label ID="txtnometitulo" Text="" runat="server" />
                      </div>
                      <div class="col s6 right-align">
                          <img src="../../img/logo.png">
                     </div>
                   <%--  <div class="col s6 right-align">
                         <strong>Relatório de Aniversariantes</strong>
                     </div>--%>
               </div>
            </div>  
 <%--   <div class="row">
                <div class="col s12 blue lighten-5 bordered-full">
                      <div class="col s4 ">
                                  <b>  <asp:Label Text="Matricula" runat="server"></asp:Label><span class=" text-danger"></b></span>
                                    <asp:TextBox ID="txtmatricula" runat="server" CssClass="form-control" ></asp:TextBox>
                          <br>
                      </div>
                      <div class="col s4 ">
                                   <b> <asp:Label Text="Nome" runat="server"></asp:Label><span class=" text-danger"><b/></span>
                                    <asp:TextBox ID="txtnome" runat="server" CssClass="form-control"></asp:TextBox>
                      </div>
                      <div class="col s4">
                                  <b>  <asp:Label Text="Nascimento" runat="server"></asp:Label><span class=" text-danger"></b>
                                    <asp:TextBox onkeyup="formataData(this,event);" MaxLength="10" ID="txtdata" runat="server" CssClass="form-control"></asp:TextBox>
                     </div>
                    <br />
               </div>
            </div>--%>

                <div class="col s10" runat="server">
                     
                  
                        <asp:GridView ID="grademensalidade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma mensalidade encontrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnrseqgrade" runat="server" Value='<%#Bind("nrseq")%>'></asp:HiddenField>
                                        <asp:HiddenField ID="hdvalor" runat="server" Value='<%#Bind("valor")%>'></asp:HiddenField>
                                        <asp:LinkButton ID="visualizar" runat="server" CssClass="btn btn-info " CommandName="visualizar" CommandArgument='<%#Container.DataItemIndex%>' Visible="false">
                            <i class="fa fa-eye"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                <asp:BoundField DataField="nome" HeaderText="Cliente" />
                                <asp:BoundField DataField="ativo" HeaderText="Ativo" Visible="false" />
                                <asp:BoundField DataField="statuspg" HeaderText="Status" />
                                <asp:BoundField DataField="mes" HeaderText="Mês" />
                                <asp:BoundField DataField="ano" HeaderText="Ano" />
                                <asp:BoundField DataField="Valor" HeaderText="Valor" />
                                <asp:BoundField DataField="dtpg" HeaderText="Data" />
                           
                            </Columns>
                        </asp:GridView>
                </div>
</asp:Content>


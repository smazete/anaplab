﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ocorrenciacpfl.aspx.vb" Inherits="restrito_relatorios_ocorrenciacpfl" %>

<!DOCTYPE html>
<html>
<head>
    <title>Orçamentos </title>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="assets/others.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/table.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/col.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/colors.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/style.css" media="screen,projection" />
    <meta charset="UTF-8">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <form id="formulario" runat="server">


        <div class="container">
            <div class="row">
                <div class="col s6 left-align">
                    <h3 id="empresa" runat="server">{Empresa}</h3>
                </div>
                <div class="col s6 right-align">
                    <img src="../../img/logo.png">
                </div>
            </div>
            <div class="row margin blue lighten-5">
                <div class="col s6 left-align sem-margin">
                    <h4 id="nomeEmpresaMenor" runat="server">{Empresa}</h4>
                </div>
                <div class="col s6 right-align sem-margin">
                    <h4 id="emitidopor" runat="server">{Emitido Por}</h4>
                </div>
                <div class="col s6 left-align sem-margin">
                    <h4 id="endereco" runat="server">{Endereço}</h4>
                </div>
                <div class="col s6 right-align sem-margin">
                    <h4 id="telefone" runat="server">{Telefone}</h4>
                </div>
            </div>
            <div class="row bordered">
                <!-- Linha -->
                <div class="col s1  bordered-last">
                    <strong>Tipo</strong>
                </div>
                <div class="col s7  bordered-last" id="tipoRelatorio" runat="server">
                    Relatório de Gerência de Caixa
                </div>
                <div class="col s2  bordered-last">
                    <strong>Emissão Em</strong>
                </div>
                <div class="col s2 bordered-right  bordered-last" id="emissao" runat="server">
                    dd/MM/yyyy hh:mm
                </div>
                <!-- Fim da Linha -->
            </div>
            <div class="row">
                <div class="col s12 blue lighten-5 bordered-full">
                    <div class="col s6 right-align">
                        <strong>Ocorrências</strong>
                    </div>
                    <div class="col s6 right-align" style="font-size: .8rem; margin-top: .2rem;">
                        Registros Impressos: <strong id="impressos" runat="server">0</strong>
                    </div>
                </div>
                <div class="col s12" runat="server">
                    <asp:GridView ID="gdvLancamentos" runat="server" CssClass="bordered" AutoGenerateColumns="false" GridLines="None">
                        <Columns>
                            <asp:BoundField DataField="nrseqcpfl" HeaderText="Código" />
                            <asp:BoundField DataField="data" HeaderText="Data" />
                            <asp:BoundField DataField="desctipo" HeaderText="Tipo da Ocorrência" />
                            <asp:BoundField DataField="ocorrencia" HeaderText="Ocorrência" />
                            <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                            <asp:BoundField DataField="cliente" HeaderText="Cliente" />
                            <asp:BoundField DataField="telefonefixo" HeaderText="Telefone Fixo" />
                            <asp:BoundField DataField="telefonecelular" HeaderText="Telefone Celular" />
                            <asp:BoundField DataField="email" HeaderText="Email" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>

    </form>
</body>
</html>

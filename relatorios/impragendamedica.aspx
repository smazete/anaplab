﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/relatorios/impragendamedica.aspx.vb" Inherits="relatorios_impragendamedica" %>

<!DOCTYPE html>
<html>
<head>
    <title>Impressao agenda medica</title>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="assets/others.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/table.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/col.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/colors.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/style.css" media="screen,projection" />

    <script src="../bower_components/jquery/dist/jquery.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../dist/js/adminlte.min.js"></script>
    <script src="../js/jquery.mask.js"></script>
    <script src="../js/JScriptmascara.js" type="text/javascript"></script>
    <script src="../assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link type="text/css" rel="stylesheet" href="../assets/css/style.css" />
    <script src="../dist/js/sweetalert2.all.min.js"></script>
    <meta charset="UTF-8">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <form id="formulario" runat="server">


        <div class="container">
            <div class="row">
                <div class="col s6 left-align">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 id="empresa" runat="server">{Empresa}</h3>
                        </div>
                        <div class="col-lg-12">
                            <h3 id="rua" runat="server">{Rua}</h3>
                        </div>
                        <div class="col-lg-12">
                            <h3 runat="server"><span runat="server" id="cidade">{cidade}</span> - <span runat="server" id="telefone">{telefone}</span></h3>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 right-align">
                    <img src="../../img/logo.png">
                </div>
            </div>

            <div class="row bordered-full text-align-center">
                <div class="col-lg-12" style="height: 100%; padding: 40px !important;">
                    <h2 id="titleagenda" style="font-weight: bold">AGENDA MÉDICA</h2>
                </div>
            </div>

            <div class="row">
                <div style="height: 100%;" class="col-lg-12" runat="server">
                    <h2 class="bordered-full bordered-right" style="margin: 0;" runat="server"><span class="left-align"  runat="server" id="profissional">Profissional:{medico}</span> <span runat="server" class="right-align" id="data">Data:{data}</span></h2>
                    <asp:GridView ID="gdvagendamedica" runat="server" AutoGenerateColumns="false" RowStyle-CssClass="bordered-full text-align-center" HeaderStyle-CssClass="text-align-center" GridLines="Horizontal">

                        <Columns>

                            <asp:BoundField DataField="horario" HeaderText="Horário" />
                            <asp:TemplateField HeaderText="Paciente">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblmatricula" Text='<%#Bind("matricula") %>'></asp:Label>
                                    <asp:Label runat="server" ID="lblnome" Text='<%#Bind("paciente") %>'></asp:Label>
                                    <asp:HiddenField runat="server" Value='<%#Eval("nrseqmedico") %>' ID="hdnnrseqmedico" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="celular" HeaderText="Celular" />
                            <asp:BoundField DataField="telefone" HeaderText="Telefone" />
                            <asp:BoundField DataField="valor" HeaderText="Valor" />
                            <asp:BoundField DataField="usermarcouconsulta" HeaderText="Funcionario" />
                            <asp:BoundField DataField="plano" HeaderText="Plano" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!-- Fim da Linha -->

        </div>

    </form>
</body>
</html>

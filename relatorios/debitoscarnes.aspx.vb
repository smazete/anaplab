﻿Imports clsSmart
Imports System.Data
Partial Class restrito_relatorios_debitoscarnes
    Inherits System.Web.UI.Page
    Dim tblista As New Data.DataTable
    Dim tablista As New clsBanco
    Dim tabelaEmpresa As String = " tbempresas"
    Dim tabelaUsuario As String = " tbusuarios"
    Dim tabelaClientes As String = " tbclientes_map"
    Dim tabelaCarnes As String = " tbcarnes"
    Dim tabelaCarnesParcela As String = " tbcarnes_parcelas"
    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function

    Public Function paraUTF8(str As String) As String
        Dim encode As New System.Text.UTF8Encoding()
        Dim strBytes() As Byte = encode.GetBytes(str)
        Return encode.GetString(strBytes)
    End Function
    Private Function carregaEmpresa() As Data.DataTable
        Dim nrseqempresa As String = Session("idempresaemuso")
        Dim sql = "SELECT * FROM " & tabelaEmpresa & " WHERE ativo = true AND nrseq = '" & nrseqempresa & "' "
        Return findSql(sql)
    End Function
    Public Function replaceForSqlMoedacomSum(campo As String) As String
        Return "CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(SUM(COALESCE(" & campo & ", 0)), 2), ',', ' '), '.', ','), ' ', '.'))"
    End Function

    Private Sub carregaGdv(filtro As String)
        Dim sqlGdv As String = "SELECT  sum(cp.valor) As cpvalor, IF(cp.baixa = TRUE, 'Sim', 'Não') AS cppago,    cli.nome AS clinome, cli.matricula AS climatricula  FROM  tbcarnes_parcelas As cp INNER JOIN  tbcarnes As cr ON cp.nrseqcarne = cr.nrseq INNER JOIN  tbclientes_map As cli ON cp.matricula = cli.matricula WHERE " & filtro & "  GROUP BY     cp.baixa,   cli.nome, cli.matricula order by cli.nome"


        gdvProcuraDeCarnes.DataSource = findSql(sqlGdv)
        gdvProcuraDeCarnes.DataBind()

        impressos.InnerText = gdvProcuraDeCarnes.Rows.Count





        '   montaGrafico(sqlGrafico)

    End Sub
    Private Sub restrito_relatorios_debitoscarnes_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsPostBack Then Exit Sub

        hdfiltro.Value = Session("consultasql")

        carregaGdv(hdfiltro.Value)

        emitidopor.InnerText = Session("Usuario") & " - " & DateTime.Now.ToString("dd/MM/yyyy hh:mm")
        Dim dt As DataTable = carregaEmpresa()
        empresa.InnerText = dt.Rows(0)("nome").ToString
        DataInicial.InnerText = Session("datainicial")
        DataFinal.InnerText = Session("datafinal")
        hddtinicial.Value = Session("datainicial")
        hddtfinal.Value = Session("datafinal")
        ' nomeEmpresaMenor.InnerText = dt.Rows(0)("nome").ToString
        ' endereco.InnerText = IIf(dt.Rows(0)("endereco").GetType IsNot Nothing AndAlso dt.Rows(0)("endereco").ToString <> "" AndAlso dt.Rows(0)("endereco").ToString <> " ", dt.Rows(0)("endereco").ToString, "Sem Endereço")
        ' telefone.InnerText = IIf(dt.Rows(0)("telefone").GetType IsNot Nothing AndAlso dt.Rows(0)("telefone").ToString <> "" AndAlso dt.Rows(0)("telefone").ToString <> " ", dt.Rows(0)("telefone").ToString, "Sem Telefone")
        tipoRelatorio.InnerHtml = "Relatório de Carnês"
    End Sub

    Private Sub gdvProcuraDeCarnes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvProcuraDeCarnes.RowDataBound

        If e.Row.RowType = DataControlRowType.Header Then

            tblista = tablista.conectar("select * from tbcarnes_parcelas where  (baixa = false or baixa is null) and ativo = true and dtvencimento >= '" & formatadatamysql(Session("datainicial")) & "' and dtvencimento <= '" & formatadatamysql(Session("datafinal")) & "' order by dtvencimento")
        End If
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim lblvencimentosdatas As Label = e.Row.FindControl("lblvencimentosdatas")

        'Dim tbtemp As New Data.DataTable
        'Dim tabtemp As New clsBanco
        Dim tbrow As Data.DataRow()
        ' tbtemp = tabtemp.conectar("select * from tbcarnes_parcelas where matricula = '" & e.Row.Cells(0).Text & "' and (baixa = false or baixa is null) and ativo = true and dtvencimento >= '" & formatadatamysql(DataInicial.InnerText) & "' and dtvencimento <= '" & formatadatamysql(DataFinal.InnerText) & "' order by dtvencimento")
        tbrow = tblista.Select(" matricula = '" & e.Row.Cells(0).Text & "'")
        For x As Integer = 0 To tbrow.Count - 1
            lblvencimentosdatas.Text &= FormatDateTime(tbrow(x)("dtvencimento").ToString, DateFormat.ShortDate) & " - "
        Next
        If tbrow.Count > 0 Then
            lblvencimentosdatas.Text &= " (" & tbrow.Count & " em aberto)."

        End If
        e.Row.Cells(3).Text = FormatCurrency(e.Row.Cells(3).Text)
    End Sub
End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="carnes.aspx.vb" Inherits="restrito_relatorios_dependentes" MasterPageFile="~/relatorios/MasterPrint.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="container">
        <div class="row">
            <div class="col s6 left-align">
                <h3 id="empresa" runat="server">{Empresa}</h3>
            </div>
            <div class="col s6 right-align">
                <img src="../../img/logo.png">
            </div>
        </div>
        <div class="row margin blue lighten-5">
            <div class="col s6 left-align sem-margin">
                <h4 id="nomeEmpresaMenor" runat="server">{Empresa}</h4>
            </div>
            <div class="col s6 right-align sem-margin">
                <h4 id="emitidopor" runat="server">{Emitido Por}</h4>
            </div>
            <div class="col s6 left-align sem-margin">
                <h4 id="endereco" runat="server">{Endereço}</h4>
            </div>
            <div class="col s6 right-align sem-margin">
                <h4 id="telefone" runat="server">{Telefone}</h4>
            </div>
        </div>
        <div class="row bordered">
            <!-- Linha -->
            <div class="col s1  bordered-last">
                <strong>Tipo</strong>
            </div>
            <div class="col s11 bordered-right   bordered-last" id="tipoRelatorio" runat="server">
                Relatório de Gerência de Caixa
            </div>
            <!-- Fim da Linha -->
        </div>
        <div class="row margin blue lighten-5 center-align">
            <div class="col s4">
                <div class="col s12">
                    <strong>Em aberto</strong>
                </div>
                <div class="col s12" id="emaberto" runat="server">
                    R$ 00,00
                </div>
            </div>
            <div class="col s4">
                <div class="col s12">
                    <strong>Pago</strong>
                </div>
                <div class="col s12" id="valorpago" runat="server">
                    R$ 00,00
                </div>
            </div>
            <div class="col s4">
                <div class="col s12">
                    <strong>Total</strong>
                </div>
                <div class="col s12" id="total" runat="server">
                    R$ 00,00
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 blue lighten-5 bordered-full">
                <div class="col s6 right-align">
                    <strong>Carnês</strong>
                </div>
                <div class="col s6 right-align" style="font-size: .8rem; margin-top: .2rem;">
                    Registros Impressos: <strong id="impressos" runat="server">0</strong>
                </div>
            </div>
            <div class="col s12" runat="server">
                <asp:GridView ID="gdvProcuraDeCarnes" runat="server" CssClass="bordered" AutoGenerateColumns="false" GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="cpnrseq" HeaderText="Código" />
                        <asp:BoundField DataField="cpnrseqcarne" HeaderText="Nr Carnê" />
                        <asp:BoundField DataField="climatricula" HeaderText="Matricula" />
                        <asp:BoundField DataField="clinome" HeaderText="Nome" />
                        <asp:BoundField DataField="cpdtvencimento" HeaderText="Dt Vencimento" />
                        <asp:BoundField DataField="cpdtemissao" HeaderText="Dt Emissão" />
                        <asp:BoundField DataField="cpvalor" HeaderText="Valor" />
                        <asp:BoundField DataField="cpemitido" HeaderText="Emitido" />
                        <asp:BoundField DataField="cppago" HeaderText="Pago" />
                        <asp:BoundField DataField="cpvalorpago" HeaderText="Valor Pago" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="gerenciacaixa.aspx.vb" Inherits="restrito_relatorios_gerenciacaixa" %>

<!DOCTYPE html>
<html>
<head>
    <title>Orçamentos </title>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="assets/others.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/table.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/col.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/colors.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="assets/style.css" media="screen,projection" />
    <meta charset="UTF-8">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <form id="formulario" runat="server">


        <div class="container">
            <div class="row">
                <div class="col s6 left-align">
                    <h3 id="empresa" runat="server">{Empresa}</h3>
                </div>
                <div class="col s6 right-align">
                    <img src="../../img/logo.png">
                </div>
            </div>
            
            <div class="row bordered">
                <!-- Linha -->
                <div class="col s1">
                    <strong>Tipo</strong>
                </div>
                <div class="col s7" id="tipoRelatorio" runat="server">
                    Relatório de Gerência de Caixa
                </div>
                <div class="col s2">
                    <strong>Emissão Em</strong>
                </div>
                <div class="col s2 bordered-right" id="emissao" runat="server">
                    dd/MM/yyyy hh:mm
                </div>
            </div>
            <div class="row bordered" style="margin-top: -20px;">
                <div class="col s1 bordered-last">
                    <strong>Caixa(s)</strong>
                </div>
                <div class="col s7 bordered-last" id="caixas" runat="server">
                    Rua do Cliente, X, Bairrista, São José dos Pinhais, PR
                </div>
                <div class="col s2 bordered-last">
                    <strong>Responsável</strong>
                </div>
                <div class="col s2 bordered-right bordered-last" id="responsavel" runat="server">
                    Usuario
                </div>
            </div>
            <!-- Fim da Linha -->
            <!-- Linha -->

            <!-- Fim da Linha -->

            <div class="row margin blue lighten-5 center-align" style="padding:0;">
                <div class="row">
                    <strong>Caixa</strong>
                </div>
                <div class="row sem-margin" style="padding:0;">

                    <div class="col s2">
                        <strong>Valor Inicial</strong>
                        <div class="row" id="valorinicial" runat="server">
                            R$ 420,00
                        </div>
                    </div>
                    <div class="col s2">
                        <strong>Valor Final</strong>
                        <div class="row" id="valorfinal" runat="server">
                            R$ 420,00
                        </div>
                    </div>
                    <div class="col s2">
                        <strong>Valor Débito</strong>
                        <div class="row" id="valorDébito" runat="server">
                            R$ 40,00
                        </div>
                    </div>
                    <div class="col s2">
                        <strong>Valor Crédito</strong>
                        <div class="row" id="valorCredito" runat="server">
                            R$ 40,00
                        </div>
                    </div>
                    <div class="col s4">
                        <strong>Total</strong>
                        <div class="row">
                            <strong id="total" runat="server">R$ 900,00</strong>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row margin blue lighten-5 center-align" style="height: 100%;">
                <div class="col s7 right-align">
                    <strong>Lançamentos</strong>
                </div>
                <div class="col s5 right-align" style="font-size: .8rem; margin-top: .2rem;">
                    Registros Impressos: <strong id="impressos" runat="server">0</strong>
                </div>
                <div style="height: 100%; padding: 0; margin: 0;" class="col s12" runat="server">
                    <asp:GridView ID="gdvLancamentos"  runat="server" CellPadding="0" CellSpacing="0"  AutoGenerateColumns="false" GridLines="None">
                        <Columns>
                            <asp:BoundField  DataField="nrseq" HeaderText="Código" />
                            <asp:BoundField DataField="dtcad" HeaderText="Data" />
                            <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                            <asp:BoundField DataField="operacao" HeaderText="Operação" />
                            <asp:BoundField DataField="valor" HeaderText="Valor" />
                            <asp:BoundField DataField="qtdparcelas" HeaderText="Parcelas" />
                            <asp:BoundField DataField="nrseqcaixa" HeaderText="Caixa" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="row blue lighten-5 bordered-full">
                    <div class="row center-align">
                        <strong>Resumo de Forma de Pagamento</strong>
                    </div>
                <div style="height: 100%;" class="col s12" runat="server">
                    <asp:GridView ID="gdvResumo" runat="server" CssClass="bordered" AutoGenerateColumns="false" GridLines="None">
                        <Columns>
                            <asp:BoundField DataField="Forma" HeaderText="Forma de pagto" />
                            <asp:BoundField DataField="soma" HeaderText="Valor" />
                        </Columns>
                    </asp:GridView>
                </div>


            </div>
        </div>

    </form>
</body>
</html>

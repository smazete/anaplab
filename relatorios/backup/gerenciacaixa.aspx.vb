﻿
Imports System.Data
Imports System.IO
Imports SelectPdf

Partial Class restrito_relatorios_gerenciacaixa
    Inherits System.Web.UI.Page
    Dim tabelaCaixas As String = " tbcaixas"
    Dim tabelaDetalhes As String = " tbcaixasdth"
    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function

    Public Function paraUTF8(str As String) As String
        Dim encode As New System.Text.UTF8Encoding()
        Dim strBytes() As Byte = encode.GetBytes(str)
        Return encode.GetString(strBytes)
    End Function

    Private Sub restrito_relatorios_gerenciacaixa_Load(sender As Object, ea As EventArgs) Handles Me.Load
        Dim c As String = Server.UrlDecode(Request.QueryString("c"))
        ' c = converterCaracteres(c)
        Dim e As String = Server.UrlDecode(Request.QueryString("e"))
        Dim i As String = Server.UrlDecode(Request.QueryString("i"))
        Dim r As String = Server.UrlDecode(Request.QueryString("r"))
        Dim t As String = Server.UrlDecode(Request.QueryString("t"))
        Dim u As String = Server.UrlDecode(Request.QueryString("u"))
        Dim lancamentos As String = Server.UrlDecode(Request.QueryString("lancamentos"))
        Dim raiz As String = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath

        Dim naorecarregar As String = Request.QueryString("nr")
        If i <> "" AndAlso Not IsPostBack Then
            carregarListaCaixa(i)
            preencheDados(c, e, i, r, t, u)
            If naorecarregar <> "1" Then
                converterArquivoParaPDF(c, e, i, r, t, u)
            End If
        End If
    End Sub

    Private Function converterCaracteres(str As String) As String
    End Function

    Private Sub preencheDados(c As String, e As String, i As String, r As String, t As String, u As String)
        empresa.InnerText = c
      
        tipoRelatorio.InnerText = "Relatório de Gerencia de Caixa"
        caixas.InnerText = i
        responsavel.InnerText = u
        emissao.InnerText = DateTime.Now.ToString("dd/MM/yyyy hh:mm")
    End Sub

    Private Sub converterArquivoParaPDF(c As String, e As String, i As String, r As String, t As String, u As String)
        Dim raiz As String = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
        Dim args As String = "/relatorios/gerenciacaixa.aspx?nr=1&c=" & Server.UrlEncode(c) & "&e=" & Server.UrlEncode(e) & "&i=" & Server.UrlEncode(i) & "&r=" & Server.UrlEncode(r) & "&t=" & Server.UrlEncode(t) & "&u=" & Server.UrlEncode(u)

        Dim caminho = "\relatorios\arquivos\"
        Dim arquivo As String = "relatorio_gerenciacaixa" & DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString & ".pdf"
        Dim salvarEm As String = Server.MapPath("~/") & caminho & arquivo
        Dim FullPath = raiz & args

        Dim converter As New HtmlToPdf()
        Dim doc As PdfDocument = converter.ConvertUrl(FullPath)
        Dim pdf As Byte() = doc.Save()
        doc.Close()

        File.WriteAllBytes(salvarEm, pdf)

        Response.Redirect(raiz & caminho & arquivo)
    End Sub

    Private Sub carregarListaCaixa(nrseq As String)
        Dim datatable As Data.DataTable = New Data.DataTable
        Dim sqlValorIncial As String = "SELECT SUM(vlinicial) AS soma FROM " & tabelaCaixas & " WHERE nrseq IN ( " & nrseq & " )"
        Dim sqlValorFinal As String = "SELECT SUM(vlfinal) AS soma FROM " & tabelaCaixas & " WHERE nrseq IN ( " & nrseq & " )"
        Dim sqlCreditos As String = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE operacao = 'C' AND nrseqcaixa IN ( " & nrseq & " )"
        Dim sqlDebitos As String = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE operacao = 'D' AND nrseqcaixa IN ( " & nrseq & " )"
        Dim sqlSaldoAtual As String = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & nrseq & " )"
        Dim sql As String = "SELECT * FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & nrseq & " )"

        datatable = findSql(sqlValorIncial)
        valorinicial.InnerText = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))
        datatable = findSql(sqlValorFinal)
        valorfinal.InnerText = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))
        datatable = findSql(sqlCreditos)
        valorCredito.InnerText = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))
        datatable = findSql(sqlDebitos)
        valorDébito.InnerText = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))
        datatable = findSql(sqlSaldoAtual)
        total.InnerText = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))

        datatable = findSql(sql)
        impressos.InnerText = datatable.Rows.Count

        gdvLancamentos.DataSource = datatable
        gdvLancamentos.DataBind()

        'Dim sqlResumo = "SELECT d.descricao AS descricao, SUM(c.valor) AS soma FROM tbcaixasdth AS c "
        Dim sqlResumo = "SELECT d.descricao AS descricao, operacao as Forma, sum(valor) AS soma FROM tbcaixasdth"
        'sqlResumo &= "LEFT JOIN tbdocumentos AS d ON c.nrseqdocumento = d.nrseq WHERE nrseqcaixa IN ( " & nrseq & ") GROUP BY nrseqdocumento;"

        sqlResumo &= " LEFT JOIN tbdocumentos AS d ON nrseqdocumento = d.nrseq WHERE nrseqcaixa IN (" & nrseq & ") group by Forma ;"
        datatable = findSql(sqlResumo)
        gdvResumo.DataSource = datatable
        gdvResumo.DataBind()

    End Sub


    Private Sub gdvLancamentos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvLancamentos.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim i As Integer = 4


        If e.Row.Cells(i).Text > 0 Then
            e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00c853")
        ElseIf e.Row.Cells(i).Text < 0 Then
            e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#d50000")
        End If

        i = 3


        If e.Row.Cells(i).Text = "C" Then
            e.Row.Cells(i).Text = "Crédito"
        End If

        If e.Row.Cells(i).Text = "DI" Then
            e.Row.Cells(i).Text = "Dinheiro"
        End If

        If e.Row.Cells(i).Text = "DE" Then
            e.Row.Cells(i).Text = "Debito"
        End If

        If e.Row.Cells(i).Text = "N" Then
            e.Row.Cells(i).Text = "Não informado"
        End If
    End Sub

    Private Sub gdvResumo_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvResumo.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim i As Integer = 1
        ' If e.Row.Cells(i).Text > 0 Then
        '  e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00c853")
        '  ElseIf e.Row.Cells(i).Text < 0 Then
        '  e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#d50000")
        '  End If

        i = 0
        If e.Row.Cells(i).Text = "" OrElse e.Row.Cells(i).Text = " " OrElse e.Row.Cells(i).Text = "&nbsp;" OrElse e.Row.Cells(i).Text Is Nothing Then
            e.Row.Cells(i).Text = "Não informado"
        End If

        If e.Row.Cells(i).Text = "C" Then
            e.Row.Cells(i).Text = "Crédito"
        End If

        If e.Row.Cells(i).Text = "DI" Then
            e.Row.Cells(i).Text = "Dinheiro"
        End If

        If e.Row.Cells(i).Text = "DE" Then
            e.Row.Cells(i).Text = "Debito"
        End If

        If e.Row.Cells(i).Text = "N" Then
            e.Row.Cells(i).Text = "Não informado"
        End If


    End Sub

End Class

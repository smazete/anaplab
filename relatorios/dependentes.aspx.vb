﻿
Imports System.Data
Imports System.IO
Imports SelectPdf
Imports clssessoes

Partial Class restrito_relatorios_dependentes
    Inherits System.Web.UI.Page
    Dim tabelaIrregulariedades As String = " tbirregularidades_map"
    Dim tabelaIrregulariedadesdth As String = " tbirregularidadesdth_map"
    Dim tabelaCPFL As String = " tbOcorrenciasCPFL"
    Dim tabelaPlanos As String = " tbplanos_map"
    Dim tabelaEmpresa As String = " tbempresas"
    Dim tabelaClientes As String = " tbclientes_map"
    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function

    Public Function paraUTF8(str As String) As String
        Dim encode As New System.Text.UTF8Encoding()
        Dim strBytes() As Byte = encode.GetBytes(str)
        Return encode.GetString(strBytes)
    End Function

    Private Sub restrito_relatorios_gerenciacaixa_Load(sender As Object, ea As EventArgs) Handles Me.Load

        versessao()
        Dim c As String = Server.UrlDecode(Request.QueryString("c"))
        Dim s As String = Server.UrlDecode(Request.QueryString("s"))
        Dim r As String = Server.UrlDecode(Request.QueryString("r"))
        Dim t As String = Server.UrlDecode(Request.QueryString("t"))
        Dim u As String = Server.UrlDecode(Request.QueryString("u"))

        Dim raiz As String = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath

        Dim naorecarregar As String = Request.QueryString("nr")
        If s <> "" AndAlso Not IsPostBack Then
            carregarListaCaixa(s)
            preencheDados(c, s, r, t, u)
            'If naorecarregar <> "1" Then
            '    converterArquivoParaPDF(c, s, r, t, u)
            'End If
        End If
    End Sub

    Private Function converteDatatableToString(datatable As DataTable, Optional campo As String = "nrseq") As String
        Dim palavra As String = ""

        For x As Integer = 0 To datatable.Rows.Count - 1
            If x > 0 Then
                palavra &= ", "
            End If
            palavra &= datatable.Rows(x)(campo)
        Next

        Return palavra
    End Function

    Private Sub preencheDados(c As String, s As String, r As String, t As String, u As String)
        empresa.InnerText = c
        nomeEmpresaMenor.InnerText = c
        emitidopor.InnerText = u
        endereco.InnerText = r
        telefone.InnerText = t
        tipoRelatorio.InnerText = "Relatório de Dependentes"

        emissao.InnerText = DateTime.Now.ToString("dd/MM/yyyy hh:mm")
    End Sub

    Private Sub converterArquivoParaPDF(c As String, s As String, r As String, t As String, u As String)
        Dim raiz As String = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
        Dim args As String = "restrito/relatorios/dependentes.aspx?nr=1&c=" & Server.UrlEncode(c) & "&s=" & Server.UrlEncode(s) & "&r=" & Server.UrlEncode(r) & "&t=" & Server.UrlEncode(t) & "&u=" & Server.UrlEncode(u)

        Dim caminho = "restrito\relatorios\arquivos\"
        Dim arquivo As String = "relatorio_dependentes" & DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString & ".pdf"
        Dim salvarEm As String = Server.MapPath("~/") & caminho & arquivo
        Dim FullPath = raiz & args

        Dim converter As New HtmlToPdf()
        converter.Options.MaxPageLoadTime = 500
        Dim doc As PdfDocument = converter.ConvertUrl(FullPath)
        Dim pdf As Byte() = doc.Save()
        doc.Close()

        File.WriteAllBytes(salvarEm, pdf)

        Response.Redirect(raiz & caminho & arquivo)
    End Sub

    Private Sub carregarListaCaixa(sql As String)
        'Dim myDelims As String() = New String() {"FROM"}
        'Dim split As String() = sql.Split(myDelims, StringSplitOptions.None)

        Dim datatable As Data.DataTable = findSql(sql)
        impressos.InnerText = datatable.Rows.Count
        gdvLancamentos.DataSource = datatable
        gdvLancamentos.DataBind()

        ' Dim valores As String = converteDatatableToString(datatable, "erro")
    End Sub

    Private Sub gdvLancamentos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvLancamentos.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim cel = 2
        e.Row.Cells(cel).Text = Left(e.Row.Cells(cel).Text, 10)
        cel = 3
        e.Row.Cells(cel).Text = Left(e.Row.Cells(cel).Text, 10)
        cel = 4
        Dim dtparse As DateTime
        DateTime.TryParse(e.Row.Cells(cel).Text.ToString, dtparse)
        e.Row.Cells(cel).Text = DateDiff(DateInterval.Year, dtparse, DateTime.Now).ToString

        cel = 6
        Dim ativo As String = e.Row.Cells(cel).Text
        cel = 0
        If ativo <> "" AndAlso ativo <> " " AndAlso ativo <> "&nbsp;" AndAlso ativo IsNot Nothing Then
            e.Row.Cells(cel).Text = e.Row.Cells(cel).Text & " - Ativo"
        Else
            e.Row.Cells(cel).Text = e.Row.Cells(cel).Text & " - Inativo"
        End If

    End Sub
End Class

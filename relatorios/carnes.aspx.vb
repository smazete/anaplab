﻿Imports clsSmart
Imports System.Data
Imports System.IO
Imports SelectPdf

Partial Class restrito_relatorios_dependentes
    Inherits System.Web.UI.Page

    Dim tabelaEmpresa As String = " tbempresas"
    Dim tabelaUsuario As String = " tbusuarios"
    Dim tabelaClientes As String = " tbclientes_map"
    Dim tabelaCarnes As String = " tbcarnes"
    Dim tabelaCarnesParcela As String = " tbcarnes_parcelas"
    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function

    Public Function paraUTF8(str As String) As String
        Dim encode As New System.Text.UTF8Encoding()
        Dim strBytes() As Byte = encode.GetBytes(str)
        Return encode.GetString(strBytes)
    End Function

    Private Function carregaEmpresa() As Data.DataTable
        Dim nrseqempresa As String = Session("idempresaemuso")
        Dim sql = "SELECT * FROM " & tabelaEmpresa & " WHERE ativo = true AND nrseq = '" & nrseqempresa & "' "
        Return findSql(sql)
    End Function

    Private Sub restrito_relatorios_gerenciacaixa_Load(sender As Object, ea As EventArgs) Handles Me.Load
        Dim ordernarpor As String = IIf(Request.QueryString("o") = "", 1, Request.QueryString("o"))
        Dim exibir As String = IIf(Request.QueryString("e") = "", 1, Request.QueryString("e"))
        Dim txtdtinicial As String = Request.QueryString("i")
        Dim txtdtfinal As String = Request.QueryString("f")
        Dim filtroDatas As String = IIf(Request.QueryString("d") = "", 1, Request.QueryString("d"))

        carregaGdv(ordernarpor, exibir, txtdtinicial, txtdtfinal, filtroDatas)

        emitidopor.InnerText = Session("Usuario") & " - " & DateTime.Now.ToString("dd/MM/yyyy hh:mm")
        Dim dt As DataTable = carregaEmpresa()
        empresa.InnerText = dt.Rows(0)("nome").ToString
        nomeEmpresaMenor.InnerText = dt.Rows(0)("nome").ToString
        endereco.InnerText = IIf(dt.Rows(0)("endereco").GetType IsNot Nothing AndAlso dt.Rows(0)("endereco").ToString <> "" AndAlso dt.Rows(0)("endereco").ToString <> " ", dt.Rows(0)("endereco").ToString, "Sem Endereço")
        telefone.InnerText = IIf(dt.Rows(0)("telefone").GetType IsNot Nothing AndAlso dt.Rows(0)("telefone").ToString <> "" AndAlso dt.Rows(0)("telefone").ToString <> " ", dt.Rows(0)("telefone").ToString, "Sem Telefone")
        tipoRelatorio.InnerHtml = "Relatório de Carnês"

    End Sub

    Private Sub carregaGdv(ordernarpor As String, exibir As String, txtdtinicial As String, txtdtfinal As String, filtroDatas As String)
        Dim sqlGdv As String = "SELECT cp.nrseq AS cpnrseq, DATE_FORMAT(cp.dtvencimento, '%d/%m/%Y') AS cpdtvencimento, DATE_FORMAT(cp.dtemissao, '%d/%m/%Y') AS cpdtemissao, COALESCE(FORMAT(cp.valor,2), 0) As cpvalor, "
        sqlGdv &= "cp.parcela AS cpparcela, IF(cp.emitido = TRUE, 'Sim', 'Não') As cpemitido, IF(cp.baixa = TRUE, 'Sim', 'Não') AS cppago, "
        sqlGdv &= " COALESCE(FORMAT(cp.valorpago,2), 0) AS cpvalorpago, cp.nrseqcarne AS cpnrseqcarne , cli.nome AS clinome, cli.matricula AS climatricula "
        Dim sqlFrom As String = " FROM " & tabelaCarnesParcela & " As cp "
        sqlFrom &= "INNER JOIN " & tabelaCarnes & " As cr ON cp.nrseqcarne = cr.nrseq "
        sqlFrom &= "INNER JOIN " & tabelaClientes & " As cli ON cp.matricula = cli.matricula "
        Dim sqlFiltros As String = "WHERE 1=1 AND cp.ativo = 1 AND cr.nrseqempresa = '" & Session("idempresaemuso") & "' "

        If exibir = 2 Then
            sqlFiltros &= "AND ( (cp.baixa = false or cp.baixa is null)  ) "
        ElseIf exibir = 3 Then
            sqlFiltros &= "AND cp.baixa = 1 "
        End If

        Dim dtinicial = ""
        Dim dtfinal = ""
        If txtdtfinal <> "" OrElse txtdtinicial <> "" Then
            If txtdtinicial <> "" Then
                dtinicial = txtdtinicial
                If txtdtfinal <> "" Then
                    dtfinal = txtdtfinal
                Else
                    dtfinal = DateTime.Now.ToString("yyyy/MM/dd")
                End If
            End If
            If txtdtfinal <> "" Then
                dtfinal = txtdtfinal
            Else
                If txtdtinicial <> "" Then
                    dtinicial = txtdtinicial
                    dtinicial = DateTime.Now.ToString("yyyy/MM/dd")
                End If
            End If
            sqlFiltros &= "AND "

            If filtroDatas = 3 Then
                sqlFiltros &= "cp.dtcad "
            ElseIf filtroDatas = 2 Then
                sqlFiltros &= "cp.dtvencimento "
            Else
                sqlFiltros &= "cp.dtbaixa "
            End If

            sqlFiltros &= "BETWEEN '" & formatadatamysql(dtinicial) & "' And '" & formatadatamysql(dtfinal) & "' "
        End If

        Dim sqlTodos As String = "SELECT DISTINCT(COALESCE(COUNT(cp.nrseq), 0)) AS cont, COALESCE(SUM(FORMAT(cp.valor,2)), 0) AS soma " & sqlFrom & sqlFiltros & ""
        Dim sqlPagos As String = "SELECT  DISTINCT(COALESCE(COUNT(cp.nrseq), 0)) AS cont, COALESCE(SUM(FORMAT(cp.valorpago,2)), 0) AS soma " & sqlFrom & sqlFiltros & "AND cp.baixa = 1"
        Dim sqlAberto As String = "SELECT  DISTINCT(COALESCE(COUNT(cp.nrseq), 0)) AS cont, COALESCE(SUM(FORMAT(cp.valor,2)), 0) AS soma " & sqlFrom & sqlFiltros & "AND ( cp.baixa = 0 OR cp.baixa IS NULL )"


        Dim sqlGroup As String = " GROUP BY cp.nrseq, cp.dtvencimento, cp.dtemissao, cp.valor, cp.parcela, cpemitido, cp.baixa, cp.valorpago, cp.nrseqcarne, cli.nome, cli.matricula;"

        sqlGdv = sqlGdv & sqlFrom & sqlFiltros & sqlGroup

        gdvProcuraDeCarnes.DataSource = findSql(sqlGdv)
        gdvProcuraDeCarnes.DataBind()

        impressos.InnerText = gdvProcuraDeCarnes.Rows.Count

    End Sub

    Public Function replaceForSqlMoedacomSum(campo As String) As String
        Return "CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(SUM(COALESCE(" & campo & ", 0)), 2), ',', ' '), '.', ','), ' ', '.'))"
    End Function

End Class

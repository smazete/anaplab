﻿
Partial Class relatorios_relatoriomensalidades
    Inherits System.Web.UI.Page



    Private Sub relatorios_relatoriomensalidades_Load(sender As Object, e As EventArgs) Handles Me.Load


        Dim Nrcliente As String = Server.UrlDecode(Request.QueryString("nrseqcliente")) ' numero sequencial do cliente

        carregamensalidades(Nrcliente)
        carregacliente(Nrcliente)



    End Sub

    Public Sub carregacliente(nrseqcliente)


        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select nome, dtnasc, matricula from tbAssociados where  nrseq =  '" & nrseqcliente & "'")

        ' txtdata.Text = tb1.Rows(0)("dtnasc").ToString
        '   txtnome.Text = tb1.Rows(0)("nome").ToString
        txtnometitulo.Text = tb1.Rows(0)("nome").ToString
        '  txtmatricula.Text = tb1.Rows(0)("matricula").ToString
    End Sub


    Public Sub carregamensalidades(nrseqcliente)


        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where  tbmensalidades.nrseqcliente =  " & nrseqcliente & "  order by tbmensalidades.ano, tbmensalidades.mes asc")

        grademensalidade.DataSource = tb1
        grademensalidade.DataBind()

    End Sub

    Private Sub grademensalidade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grademensalidade.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub
        '  Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        '    Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(4).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue
        Else
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If

        If e.Row.Cells(5).Text = "1" Then
            e.Row.Cells(5).Text = "Janeiro"
        ElseIf e.Row.Cells(5).Text = "2" Then
            e.Row.Cells(5).Text = "Fevereiro"
        ElseIf e.Row.Cells(5).Text = "3" Then
            e.Row.Cells(5).Text = "Março"
        ElseIf e.Row.Cells(5).Text = "4" Then
            e.Row.Cells(5).Text = "Abril"
        ElseIf e.Row.Cells(5).Text = "5" Then
            e.Row.Cells(5).Text = "Maio"
        ElseIf e.Row.Cells(5).Text = "6" Then
            e.Row.Cells(5).Text = "Junho"
        ElseIf e.Row.Cells(5).Text = "7" Then
            e.Row.Cells(5).Text = "Julho"
        ElseIf e.Row.Cells(5).Text = "8" Then
            e.Row.Cells(5).Text = "Agosto"
        ElseIf e.Row.Cells(5).Text = "9" Then
            e.Row.Cells(5).Text = "Setembro"
        ElseIf e.Row.Cells(5).Text = "10" Then
            e.Row.Cells(5).Text = "Outubro"
        ElseIf e.Row.Cells(5).Text = "11" Then
            e.Row.Cells(5).Text = "Novembro"
        ElseIf e.Row.Cells(5).Text = "12" Then
            e.Row.Cells(5).Text = "Dezembro"
        End If

        If e.Row.Cells(4).Text = "0" Then
            e.Row.Cells(4).Text = "Aberto"
        ElseIf e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Pago"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Estornado"
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.Cells(4).Text = "Isento"
        End If



        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If

    End Sub

End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="debitoscarnes.aspx.vb" Inherits="restrito_relatorios_debitoscarnes"  MasterPageFile="~/relatorios/MasterPrint.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID ="hdfiltro" runat="server" />
    <div class="container">
        <asp:HiddenField ID="hddtinicial" runat="server" />
        <asp:HiddenField ID="hddtfinal" runat="server" />
        <div class="row">
            <div class="col s6 left-align">
                <h3 id="empresa" runat="server">{Empresa}</h3>
            </div>
            <div class="col s6 right-align">
                <img src="../../img/logo.png">
            </div>
        </div>
        <div class="row margin blue lighten-5">
            <div class="col s6 left-align sem-margin">
                <h4 id="DataInicial" runat="server">{Data Inicial}</h4>
            </div>
            <div class="col s6 right-align sem-margin">
                <h4 id="emitidopor" runat="server">{Emitido Por}</h4>
            </div>
            <div class="col s6 left-align sem-margin">
                <h4 id="DataFinal" runat="server">{Data Final}</h4>
            </div>
        </div>
        <div class="row bordered">
            <!-- Linha -->
            <div class="col s1  bordered-last">
                <strong>Tipo</strong>
            </div>
            <div class="col s11 bordered-right   bordered-last" id="tipoRelatorio" runat="server">
                Relatório de Gerência de Caixa
            </div>
            <!-- Fim da Linha -->
        </div>
        <div class="row margin blue lighten-5 center-align">
            <div class="col s4">
                <div class="col s12">
                    <strong>Em aberto</strong>
                </div>
                <div class="col s12" id="emaberto" runat="server">
                    R$ 00,00
                </div>
            </div>
            <div class="col s4">
                <div class="col s12">
                    <strong>Pago</strong>
                </div>
                <div class="col s12" id="valorpago" runat="server">
                    R$ 00,00
                </div>
            </div>
            <div class="col s4">
                <div class="col s12">
                    <strong>Total</strong>
                </div>
                <div class="col s12" id="total" runat="server">
                    R$ 00,00
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 blue lighten-5 bordered-full">
                <div class="col s6 right-align">
                    <strong>Carnês</strong>
                </div>
                <div class="col s6 right-align" style="font-size: .8rem; margin-top: .2rem;">
                    Registros Impressos: <strong id="impressos" runat="server">0</strong>
                </div>
            </div>
            <div class="col s12" runat="server">
                <asp:GridView ID="gdvProcuraDeCarnes" runat="server" CssClass="bordered" AutoGenerateColumns="false" GridLines="None">
                    <Columns>
                        
                        <asp:BoundField DataField="climatricula" HeaderText="Matricula" />
                        <asp:BoundField DataField="clinome" HeaderText="Nome" />
                       <asp:TemplateField HeaderText ="Vencimentos">
                           <ItemTemplate >
                               <asp:Label ID="lblvencimentosdatas" runat="server"></asp:Label>
                           </ItemTemplate>
                       </asp:TemplateField>
                        <asp:BoundField DataField="cpvalor" HeaderText="Total Aberto" />
                     
                    </Columns>
                </asp:GridView>
            </div>
        </div>
       
    </div>
     <div class="row">
                <asp:Chart ID="charResumo" runat="server">
                    <Series>
                         <asp:Series ChartArea="chartAreaResumo" Name="seriePeriodo" XValueMember="periodo" YValueMembers="periodo" CustomProperties="DrawingStyle=Cylinder, PointWidth=1" IsValueShownAsLabel="True" Legend="legendResumo" Font="Microsoft Sans Serif, 12pt" LegendText="Período">
                        </asp:Series>
                        <asp:Series ChartArea="chartAreaResumo" Name="serieAberto" YValueMembers="abertos" CustomProperties="DrawingStyle=Cylinder, PointWidth=1" IsValueShownAsLabel="True" Legend="legendResumo" Font="Microsoft Sans Serif, 12pt" LegendText="Abertos">
                        </asp:Series>
                        <asp:Series ChartArea="chartAreaResumo" Name="seriesPago" YValueMembers="pagos" CustomProperties="DrawingStyle=Cylinder, PointWidth=1" IsValueShownAsLabel="True" Legend="legendResumo" Font="Microsoft Sans Serif, 12pt" LegendText="Pagos">
                        </asp:Series>
                        <asp:Series ChartArea="chartAreaResumo" Name="serieTotal" YValueMembers="totais" CustomProperties="DrawingStyle=Cylinder, PointWidth=1" IsValueShownAsLabel="True" Legend="legendResumo" Font="Microsoft Sans Serif, 12pt" LegendText="Totais">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="chartAreaResumo" AlignmentOrientation="Horizontal" BackColor="White">
                            <AxisX Interval="1">
                            </AxisX>
                            <Area3DStyle Enable3D="True" Inclination="10" IsClustered="True" LightStyle="Realistic" PointDepth="50" PointGapDepth="50" WallWidth="6" />
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Name="legendResumo">
                        </asp:Legend>
                    </Legends>
                    <Titles>
                        <asp:Title Name="titleResumo" Text="Resumos">
                        </asp:Title>
                    </Titles>
                </asp:Chart>

        </div>
</asp:Content>

﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/relatorios/MasterPrint.master" CodeFile="~/relatorios/reletorioretorno.aspx.vb" Inherits="relatorios_reletorioretorno" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">--%>
    <script src="../js/JScriptmascara.js" type="text/javascript"></script>

    <style type="text/css" media="all">
        .texto-titulo {
            font-size: 15pt;
            font-weight: bold;
        }
        .normal {
            font-weight: normal;
        }
        .negrito {
            font-weight: bold !important;
        }

        .negrito2 {
            font-weight: bold;
            color: #000;
        }
        hr{
          border-color:#aaa;
          box-sizing:border-box;
          width:100%;  
        }
       
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPagina" runat="Server">

    <div class="container">
        <div class="row">
         <strong>RECIBO</strong>
        </div>
        <div class="row">
            <p>
                <span id="spnNomeEmpresa" runat="server"></span> Recebeu de
                <span id="spnNomeCliente" runat="server"></span> a importância
                <span id="spnValor" runat="server"></span> (<span id="spnValorExtenso" runat="server"></span>) referente a 
                <span id="spnReferente" runat="server"></span>.
                <br /><br />
               <span id="spnCidade" runat="server"></span>, ______ de _____________________ de ____________.
             
            </p>
        </div>
    </div>



    <script>
</script>
</asp:Content>


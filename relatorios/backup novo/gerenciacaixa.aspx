﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeFile="gerenciacaixa.aspx.vb" Inherits="restrito_gerenciacaixa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">

        function fecharmodelo() {
            //$('.fechar').click(function (ev) {

            $(".windowstatusmodelo").hide();
            $(".windowstatusmodelo").closest;

            //}

        }

        function exibirmodelo() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);
            $('#enviaremail').show();
            $('#enviaremail').load();

        }

        function fecharmodalstatus() {
            //$('.fechar').click(function (ev) {
            $("#mascara").hide();
            $(".windowstatus").hide();
            $(".infoconsulta").hide();
            $(".windowstatus").closest;

            //}

        }
        function exibirnovostatus() {
            var alturaTela = $(document).height();
            var larguraTela = $(window).width();

            // colocando o fundo preto
            $('#mascara').css({ 'width': larguraTela, 'height': alturaTela });
            $('#mascara').fadeIn(1000);
            $('#mascara').fadeTo("slow", 0.8);

            //      alert('Registro gravado com sucesso2!');

            var left = ($(window).width() / 2) - ($('#alterarststatus').width() / 4);
            var top = ($(window).height() / 2) - ($('#alterarststatus').height() / 4);

            $('#alterarststatus').css({ 'left': left });
            //$('#embarquemaritimo').css({ 'margin-top': '430px' });
            $('#alterarststatus').show();
            $('#alterarststatus').load();
        }

        function fecharmodalmsg() {
            //$('.fechar').click(function (ev) {
            $("#mascara").hide();
            $(".windowmsg").hide();
            $(".infoconsulta").hide();
            $(".windowmsg").closest;

            //}

        }
        function exibirnovomsg() {
            var alturaTela = $(document).height();
            var larguraTela = $(window).width();

            // colocando o fundo preto
            $('#mascara').css({ 'width': larguraTela, 'height': alturaTela });
            $('#mascara').fadeIn(1000);
            $('#mascara').fadeTo("slow", 0.8);

            //      alert('Registro gravado com sucesso2!');

            var left = ($(window).width() / 2) - ($('#msg').width() / 4);
            var top = ($(window).height() / 2) - ($('#msg').height() / 4);

            $('#msg').css({ 'left': left });
            //$('#embarquemaritimo').css({ 'margin-top': '430px' });
            $('#msg').show();
            $('#msg').load();
        }

        function fecharenviado() {
            //$('.fechar').click(function (ev) {
            $("#mascara").hide();
            $(".windowstatus").hide();
            $(".infoconsulta").hide();
            $(".windowstatus").closest;

            //}

        }
        function exibirenviado() {
            var alturaTela = $(document).height();
            var larguraTela = $(window).width();

            // colocando o fundo preto
            $('#mascara').css({ 'width': larguraTela, 'height': alturaTela });
            $('#mascara').fadeIn(1000);
            $('#mascara').fadeTo("slow", 0.8);

            //      alert('Registro gravado com sucesso2!');

            var left = ($(window).width() / 2) - ($('#enviado').width() / 4);
            var top = ($(window).height() / 2) - ($('#enviado').height() / 4);

            $('#enviado').css({ 'left': left });
            //$('#embarquemaritimo').css({ 'margin-top': '430px' });
            $('#enviado').show();
            $('#enviado').load();
        }

        function fecharmodallogin() {
            //$('.fechar').click(function (ev) {
            $("#mascara").hide();
            $(".windowstatus").hide();
            $(".infoconsulta").hide();
            $(".windowstatus").closest;

            //}

        }
        function exibirlogin() {
            var alturaTela = $(document).height();
            var larguraTela = $(window).width();

            // colocando o fundo preto
            $('#mascara').css({ 'width': larguraTela, 'height': alturaTela });
            $('#mascara').fadeIn(1000);
            $('#mascara').fadeTo("slow", 0.8);

            //      alert('Registro gravado com sucesso2!');

            var left = ($(window).width() / 2) - ($('#alterarststatus').width() / 4);
            var top = ($(window).height() / 2) - ($('#alterarststatus').height() / 4);

            $('#loginnovo').css({ 'left': left });
            //$('#embarquemaritimo').css({ 'margin-top': '430px' });

            $('#loginnovo').load('loginmodal.aspx#loginnovo');
            $('#loginnovo').show();
        }
        function exibiralerta1() {

            $("html, body").animate({
                scrollTop: 0
            }, 1200);


            $('#alerta1').show();
            $('#alerta1').load();
        }
        function escondeexibiralerta1() {




            $('#alerta1').hide();
            $('#alerta1').hide();
        }

    </script>

    <style type="text/css">
        #btnadd {
            margin-top: 24px;
        }

        #btncep {
            margin-top: 24px;
        }

        #btnprocura {
            margin-top: 26px;
        }

        .lblmensagemupload {
            font-size: 11px;
            color: #1C5E55;
            font-weight: bold;
        }

        .frameemail {
            width: 1240px;
            height: 820px;
            border: none;
        }

        .windowstatusmodelo {
            border-width: 2px;
            border-style: solid;
            border-color: #800000;
            background: white;
            display: none;
            width: 1300px;
            height: 900px;
            position: absolute;
            margin-top: 10px;
            margin-left: 10px;
            left: 120px;
            top: 30px;
            z-index: 9999;
            text-align: left;
            -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
            box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);
        }

        .windowstatus {
            display: none;
            width: 40%;
            /*height:40px;*/
            position: absolute;
            left: 29px;
            top: 203px;
            background: #FFF;
            z-index: 9900;
            padding: 10px;
            border-radius: 10px;
            text-align: left;
            margin-top: 430px
        }

        .windowmsg {
            display: none;
            width: 40%;
            /*height:40px;*/
            position: absolute;
            left: 29px;
            top: 203px;
            background: #FFF;
            z-index: 9900;
            padding: 10px;
            border-radius: 10px;
            text-align: left;
            margin-top: 430px
        }

        #mascara {
            position: absolute;
            left: 0;
            top: 0;
            z-index: 9000;
            background-color: black;
            /*display:none;*/
            pointer-events: none;
        }

        .block {
            display: block;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="margem">
        <br />
        <div class="container-fluid">
            <div class="box box-primary">
                <div class="box-header center-block">
                    <center>Gerencia de Caixa</center>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#movimentacao" aria-controls="movimentacao" role="tab" data-toggle="tab">Movimentação</a>
                                </li>
                                <li role="presentation">
                                    <a href="#aberturadecaixa" aria-controls="aberturadecaixa" role="tab" data-toggle="tab">Abertura de Caixa</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="movimentacao">
                                    <div class="box box-primary">
                                        <div class="box-body">
                                            <div class="row">
                                                <br />
                                            </div>
                                            <div class="row">
                                                <asp:UpdatePanel ID="update1" runat="server">
                                                    <ContentTemplate>
                                                        <div class="col-lg-6">
                                                            <label for="ddlFuncionario">Nome do Funcionario</label>
                                                            <asp:DropDownList ID="ddlFuncionario" runat="server" AutoPostBack="true"
                                                                CssClass="form-control" ClientIDMode="Static">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label for="ddlNumCaixa">Nº Caixa</label>
                                                            <asp:DropDownList ID="ddlNumCaixa" runat="server" AutoPostBack="true"
                                                                CssClass="form-control" ClientIDMode="Static">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label for="ddlDtcaixa">Data do Caixa</label>
                                                            <asp:DropDownList ID="ddlDtcaixa" runat="server" AutoPostBack="true"
                                                                CssClass="form-control" ClientIDMode="Static">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-6">
                                                    <center>
                                        <asp:UpdatePanel ID="updateButtons" runat="server">
                                            <ContentTemplate>
                                                    <asp:LinkButton runat="server" class="btn btn-primary"
                                                    autopostback="true" id="btnAbrirLancamento">
                                                    Abrir Lançamento
                                                </asp:LinkButton>
                                                <asp:Button runat="server" class="btn btn-primary"
                                                    autopostback="true" id="btnDesbloquearProcura" Text="Desbloquear Procura"/>
                                                <button id="btnNovoLancmento" runat="server" class="btn btn-primary"
                                                    autopostback="true" data-spy="scroll" data-target="#collapseNovo" href="#collapseNovo" 
                                                    data-toggle="collapse" aria-expanded="false" aria-controls="collapseNovo">
                                                    Novo Lançamento
                                                </button>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </center>
                                                </div>
                                                <div class="col-lg-2"></div>

                                            </div>
                                            <div class="row">
                                                <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="updateButtons">
                                                    <ProgressTemplate>
                                                        <center>
                                              <img src="../img/loadred.gif" width="50px" height="50px" />
                                                </center>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box box-primary">
                                        <div class="box-header center-block">
                                            <center>Dados do Caixa</center>
                                        </div>
                                        <div class="box-body">
                                            <asp:UpdatePanel ID="updateInformacoesCaixa" runat="server">
                                                <ContentTemplate>

                                                    <div class="row">
                                                        <div class="form-group col-lg-2">
                                                            <label for="txtvalorinicial">Valor Inicial</label>
                                                            <input runat="server" type="text" class="form-control moeda" id="txtvalorinicial" clientidmode="Static" />
                                                        </div>
                                                        <div class="form-group col-lg-2">
                                                            <label for="txtcreditos">Valor Total de Créditos</label>
                                                            <input runat="server" type="text" class="form-control moeda" id="txtcreditos" clientidmode="Static" />
                                                        </div>
                                                        <div class="form-group col-lg-2">
                                                            <label for="txtdebitos">Valor Total de Débitos</label>
                                                            <input runat="server" type="text" class="form-control moeda" id="txtdebitos" clientidmode="Static" />
                                                        </div>
                                                        <div class="form-group col-lg-3">
                                                            <label for="txtsaldo">Lançamentos</label>
                                                            <input runat="server" type="text" class="form-control moeda" id="txtlancamentos" clientidmode="Static" />
                                                        </div>
                                                        <div class="form-group col-lg-3">
                                                            <label for="txtsaldo">Saldo Atual</label>
                                                            <input runat="server" type="text" class="form-control moeda" id="txtsaldo" clientidmode="Static" />
                                                        </div>
                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <br />
                                            <br />
                                            <div class="row" style="margin-left: .5rem; margin-right: 1rem;">
                                                <asp:UpdatePanel ID="updatepanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gdvLancamentos" runat="server" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None">
                                                            <Columns>
                                                                <asp:BoundField DataField="nrseq" HeaderText="Código" />
                                                                <asp:BoundField DataField="dtcad" HeaderText="Data" />
                                                                <asp:BoundField DataField="descricao" HeaderText="Descrição" />
                                                                <asp:BoundField DataField="formapagto" HeaderText="Forma de Pagto" />
                                                                <asp:BoundField DataField="operacao" HeaderText="Operação" />
                                                                <asp:BoundField DataField="valor" HeaderText="Valor" />
                                                                <asp:BoundField DataField="qtdparcelas" HeaderText="Parcelas" />
                                                                <asp:BoundField DataField="nrseqcaixa" HeaderText="Caixa" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="row">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Button runat="server" CssClass="btn btn-primary btn-danger center-block"
                                                            ID="btnImprimir" Text="Imprimir" AutoPostBack="true" ClientIDMode="Static" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collapse" id="collapseNovo">
                                        <div class="box box-primary">
                                            <div class="box-header center-block">
                                                <center>Novo Lançamento</center>
                                            </div>
                                            <div class="box-body">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <div class="row">
                                                            <div class="form-group col-lg-2">
                                                                <label for="txtaberto" class="col-lg-12">Caixa Aberto</label>
                                                                <input runat="server" type="text" step="1" class="form-control col-lg-12" id="txtaberto" clientidmode="Static" />
                                                            </div>
                                                            <div class="form-group col-lg-8">
                                                                <label for="txtdescricao" class="col-lg-12">Descrição</label>
                                                                <input runat="server" type="text" class="form-control col-lg-12" id="txtdescricao" clientidmode="Static" />
                                                            </div>
                                                            <div class="form-group col-lg-2">
                                                                <label for="ddlPagamento" class="col-lg-12">Forma de Pagamento</label>
                                                                <asp:DropDownList ID="ddlPagamento" runat="server"
                                                                    CssClass="form-control col-lg-12" ClientIDMode="Static">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-lg-2">
                                                                <label for="ddlOpercacao" class="col-lg-12">Operação</label>
                                                                <asp:DropDownList ID="ddlOperacao" runat="server"
                                                                    CssClass="form-control col-lg-12" ClientIDMode="Static">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="form-group col-lg-4">
                                                                <label for="txtvalor" class="col-lg-12">Valor</label>
                                                                <input runat="server" type="text" class="form-control col-lg-12 moedasemnegativo" id="txtvalor" clientidmode="Static" />
                                                            </div>
                                                            <div class="form-group col-lg-4">
                                                                <label for="txtsaldoatual" class="col-lg-12">Saldo Atual</label>
                                                                <input runat="server" type="text" class="form-control col-lg-12 moeda" id="txtsaldoatual" clientidmode="Static" />
                                                            </div>
                                                            <div class="form-group col-lg-2">
                                                                <label for="ddlCaixaFechado" class="col-lg-12">Caixa Fechado?</label>
                                                                <asp:DropDownList ID="ddlCaixaFechado" runat="server"
                                                                    CssClass="form-control col-lg-12" ClientIDMode="Static">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="box box-primary">
                                                                    <div class="box-body">
                                                                        <div class="row center-block">
                                                                            <asp:UpdatePanel ID="upBtnSalvar" runat="server">
                                                                                <ContentTemplate>
                                                                                    <asp:Button runat="server" CssClass="btn btn-primary btn-danger center-block"
                                                                                        ID="btnSalvar" Text="Salvar" AutoPostBack="true" ClientIDMode="Static" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                            <br />
                                                                        </div>
                                                                        <div class="row" id="rowError" runat="server" style="display: none">
                                                                            <br />
                                                                            <div class="col-lg-3"></div>
                                                                            <div class="alert alert-danger col-lg-6" role="alert">
                                                                                <p>
                                                                                    <center>
                                                                                    <strong>Aconteceu algo de errado<br /></strong> 
                                                                                     Os campos: <strong id="lblErro" runat="server"></strong><br /> são de preenchimento obrigatório!
                                                                                    </center>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="aberturadecaixa">
                                    <div class="box box-primary">
                                        <div class="box-body">
                                            <div class="row">
                                                <br />
                                            </div>
                                            <div class="row">
                                                <!-- <div class="row" style="margin-left: .5rem; margin-right: 1rem;"> -->
                                                <div class="col-lg-5">
                                                    <asp:UpdatePanel ID="updatepanel4" runat="server">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gdvFuncionarios" runat="server" CssClass="table table-striped" AutoGenerateColumns="false" GridLines="None">
                                                                <Columns>
                                                                    <asp:BoundField DataField="nrsequser" HeaderText="Código" />
                                                                    <asp:BoundField DataField="funcionario" HeaderText="Funcionario" />
                                                                    <asp:BoundField DataField="caixa" HeaderText="Caixa" />
                                                                    <asp:BoundField DataField="valoratual" HeaderText="Valor Atual" />
                                                                    <asp:BoundField DataField="cfechado" HeaderText="Fechado" HeaderStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                                                    <asp:TemplateField HeaderText="Ações" runat="server">
                                                                        <ItemTemplate runat="server">
                                                                            <asp:LinkButton ID="editar" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar">
                                                                         <i class="fas fa-pencil-alt"></i>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="col-lg-7">

                                                    <asp:UpdatePanel ID="updatesadas1" runat="server">
                                                        <ContentTemplate>
                                                            <div class="row">
                                                                <div class="form-group col-lg-12">
                                                                    <label for="txtnome">Funcionario Selecionado</label>
                                                                    <input runat="server" type="text" class="form-control" id="txtnome" clientidmode="Static" disabled />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group col-lg-4">
                                                                    <label for="txtnumcaixa">Número do Caixa</label>
                                                                    <input runat="server" type="text" class="form-control" id="txtnumcaixa" clientidmode="Static" disabled />
                                                                </div>
                                                                <div class="form-group col-lg-4">
                                                                    <label for="txtinicial">Valor Inicial</label>
                                                                    <input runat="server" type="text" class="form-control moedasemnegativo" id="txtinicial" clientidmode="Static" />
                                                                </div>
                                                                <div class="form-group col-lg-4">
                                                                    <label for="txttotal">Valor Total</label>
                                                                    <input runat="server" type="text" class="form-control moeda" id="txttotal" clientidmode="Static" />
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <div class="row">
                                                        <div class="col-lg-7">
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <center>
                                                                  <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                                      <ContentTemplate>
                                                                         <div class="col-lg-6">
                                                                               <asp:LinkButton ID="btnAbrirCaixa" runat="server" CssClass="btn btn-primary">Abrir Caixa</asp:LinkButton>
                                                                          </div>

                                                          <div class="col-lg-6">
                                                                <asp:LinkButton ID="btnFecharCaixa" runat="server" CssClass="btn btn-danger">Fechar Caixa</asp:LinkButton>
                                                          </div>
                                                                            
                                                                             
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                              </center>


                                                        </div>


                                                    </div>

                                                    <div class="row">
                                                        <asp:UpdatePanel ID="updateAberturaCaixaError" runat="server">
                                                            <ContentTemplate>
                                                                <div class="row" id="rowErrorAbertura" runat="server" style="display: none">
                                                                    <br />
                                                                    <div class="col-lg-3">
                                                                    </div>
                                                                    <div class="alert alert-danger col-lg-6" role="alert">
                                                                        <p>
                                                                            <center>
                                                                    <strong>Aconteceu algo de errado<br /></strong> 
                                                                        Os campos: <strong id="lblErrorAbertura" runat="server"></strong><br /> são de preenchimento obrigatório!
                                                                    </center>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>

                                                    </div>

                                                </div>
                                                <!-- </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <div id="enviaremail" class="windowstatusmodelo">
                <div class="btfecharholder">
                    <%--<div class="btfechar" onclick="fecharmodalfichaatendimento()">--%>
                    <a href="#" class="fechar" onclick="fecharmodelo()">X</a>
                    <br />
                    <br />
                </div>
                <div>
                    <iframe src="dependentes.aspx" class="frameemail"></iframe>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="/assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.maskMoney.min.js"></script>
    <script type="text/javascript" src="../assets/js/moeda.js"></script>
    <script type="text/javascript">
        carregaJS()
        function carregaJS() {
            $(function () {
                // Mascara

                $("#txtcep").mask("99999-999");
                $("#txtcpf").mask("999.999.999-99");
                $("#txtcpfconta").mask("999.999.999-99");
                $("#txttelresiden").mask("(99) 9999-9999");
                $("#txtelcelular").mask("(99) 99999-9999");
                $("#txtrg").mask("99999999999");
                $("#txttelpessoaref").mask("(99) 99999-9999");
                $(".dtmascara").mask("99/99/9999");

            });

        }

    </script>
</asp:Content>

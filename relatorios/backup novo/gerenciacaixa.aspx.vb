﻿Imports System.Data
Imports clsSmart
Partial Class restrito_gerenciacaixa
    Inherits System.Web.UI.Page

    Dim tabelaCaixas As String = " tbcaixas"
    Dim tabelaDetalhes As String = " tbcaixasdth"
    Dim tabelaEmpresa As String = " tbempresas"
    Dim tabelaUsuario As String = " tbusuarios"
    'Dim tabelaUsuario As String = " tbusuarios_map"
    Dim tabelaDocumentos As String = " tbdocumentos"

    Private Function findSql(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.conectar(sql)
    End Function

    Private Function persist(sql As String) As Data.DataTable
        Dim clsBanco As New clsBanco
        Return clsBanco.IncluirAlterarDados(sql)
    End Function

    Private Function carregaEmpresa() As Data.DataTable
        Dim nrseqempresa As String = Session("idempresaemuso")
        Dim sql = "SELECT * FROM " & tabelaEmpresa & " WHERE ativo = true AND nrseq = '" & nrseqempresa & "' "
        Return findSql(sql)
    End Function

    Private Sub carregaDocumentos()
        Dim datatable As New Data.DataTable
        Dim nrseqempresa As String = Session("idempresaemuso")
        Dim sql = "SELECT * FROM " & tabelaDocumentos & " WHERE ativo = true AND nrseqempresa = '" & nrseqempresa & "' "
        datatable = findSql(sql)

        If datatable.Rows.Count > 0 Then
            ddlPagamento.Items.Clear()
            ddlPagamento.Items.Add(New ListItem("Selecione a forma de pagamento", ""))
            For x As Integer = 0 To datatable.Rows.Count - 1
                ddlPagamento.Items.Add(New ListItem(datatable.Rows(x)("descricao").ToString, datatable.Rows(x)("nrseq").ToString))
            Next
        End If
    End Sub

    Public Function geraCodigo() As String
        Dim protocolo As String = DateTime.Now.ToString("yyyyMMddHHmmssfff").ToString
        Return protocolo
    End Function

    Private Sub inserirNaConta(nrseqcaixa As String)
        Dim sql As String = "SELECT * FROM tbcaixas WHERE nrseq = '" & nrseqcaixa & "' "
        Dim caixa As Data.DataTable = findSql(sql)

        Dim usuario As String = caixa.Rows(0)("usuario")
        Dim numcaixa As String = caixa.Rows(0)("nrseq")
        Dim valorinicial As String = caixa.Rows(0)("vlinicial")
        Dim valorfinal As String = caixa.Rows(0)("vlfinal")

        sql = "INSERT INTO tbcontas (ativo, usercad, dtcad, valor, valorpago, juros, multa, obs, descricao, operacao, qtdparcelas, parcela, desconto, dtvencimento, dtexecucao, nrseqdocumento, nrseqempresa) VALUES "
        sql &= "("
        sql &= "1, "
        sql &= "'" & Session("usuario") & "', "
        sql &= "NOW(), "
        sql &= " " & moeda(txttotal.Value.Replace("R$", "").Replace(" ", "") - txtinicial.Value.Replace("R$", "").Replace(" ", "")) & ", "
        sql &= " " & moeda(txttotal.Value.Replace("R$", "").Replace(" ", "") - txtinicial.Value.Replace("R$", "").Replace(" ", "")) & ", "
        sql &= "0, "
        sql &= "0, "
        sql &= "'Fechamento de caixa nº: " & numcaixa & " - " & usuario & "', "
        sql &= "'Fechamento de caixa nº: " & numcaixa & " - " & usuario & "', "
        sql &= "'C', "
        sql &= "1, "
        sql &= "1, "
        sql &= "0, "
        sql &= "NOW(), "
        sql &= "NOW(), "
        sql &= "'" & findSql("SELECT nrseq FROM tbdocumentos WHERE descricao = 'DINHEIRO'").Rows(0)("nrseq") & "', "
        sql &= Session("idempresaemuso") & " "
        sql &= ")"

        persist(sql)

    End Sub

    Public Function verificaCampo(input As String, campo As String, erro As String) As String
        If input = "" Then
            If erro <> "" Then
                erro &= ",<br>"
            Else
                erro &= "<br>"
            End If
            erro &= campo
        End If
        Return erro
    End Function

    Private Sub habilitar(habilitar As Boolean, Optional opcao As Integer = 0, Optional limpar As Boolean = False, Optional limparGdv As Boolean = False)

        ddlCaixaFechado.Enabled = False
        txtvalorinicial.Disabled = True
        txtcreditos.Disabled = True
        txtdebitos.Disabled = True
        txtsaldoatual.Disabled = True
        txtsaldo.Disabled = True
        txtaberto.Disabled = True

        If opcao = 1 Then

            txtdescricao.Disabled = Not habilitar
            ddlOperacao.Enabled = habilitar
            txtvalor.Disabled = Not habilitar

            btnAbrirLancamento.Enabled = Not habilitar
            btnImprimir.Enabled = habilitar
            btnDesbloquearProcura.Enabled = habilitar
            btnSalvar.Enabled = habilitar
            btnNovoLancmento.Disabled = Not habilitar
        Else
            ddlFuncionario.Enabled = habilitar
            ddlNumCaixa.Enabled = habilitar
            ddlDtcaixa.Enabled = habilitar
            btnSalvar.Enabled = habilitar
            If ddlNumCaixa.SelectedValue <> "" Then
                btnNovoLancmento.Disabled = habilitar
            End If
            btnDesbloquearProcura.Enabled = Not habilitar
            btnImprimir.Enabled = Not habilitar
            btnAbrirLancamento.Enabled = habilitar

        End If

        If limpar = True Then
            Me.limpar()
        End If

        If limparGdv = True Then
            gdvLancamentos.DataBind()
        End If
    End Sub

    Private Sub limpar()
        txtdescricao.Value = ""
        ddlOperacao.SelectedValue = ""
        txtvalor.Value = ""
    End Sub

    Private Sub carregaCaixa()
        If ddlFuncionario.SelectedValue <> "" Then
            Dim datatable As Data.DataTable = New Data.DataTable
            Dim sql As String = "SELECT * FROM " & tabelaCaixas & " WHERE usuario = '" & ddlFuncionario.SelectedValue & "' "
            If ddlDtcaixa.SelectedValue <> "" Then
                sql &= " AND data = '" & formatadatamysql(ddlDtcaixa.SelectedValue) & "' "
            End If
            If ddlNumCaixa.SelectedValue <> "" Then
                sql &= " AND nrseq = '" & ddlNumCaixa.SelectedValue & "' "
            End If
            datatable = findSql(sql)
            If datatable.Rows.Count > 0 Then
                Dim nrseq = datatable.Rows(0)("nrseq").ToString
                Session("nrseqcaixa") = nrseq
                If nrseq <> "" AndAlso nrseq <> "" Then
                    carregarListaCaixa(converteDatatableToString(datatable))
                    habilitar(False, 0)
                    If ddlNumCaixa.SelectedValue <> "" Then
                        carregaDadosDoCaixa(nrseq)
                        habilitar(True, 1)
                    End If
                End If
            End If
        End If
    End Sub

    Private Function converteDatatableToString(datatable As DataTable, Optional campo As String = "nrseq") As String
        Dim palavra As String = ""

        For x As Integer = 0 To datatable.Rows.Count - 1
            If x > 0 Then
                palavra &= ", "
            End If
            palavra &= datatable.Rows(x)(campo)
        Next

        Return palavra
    End Function

    Private Sub carregarListaCaixa(nrseq As String)
        Dim datatable As Data.DataTable = New Data.DataTable
        Dim sqlValorIncial As String = "SELECT SUM(vlinicial) AS soma FROM " & tabelaCaixas & " WHERE nrseq IN ( " & nrseq & " )"
        Dim sqlCreditos As String = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE operacao = 'C' AND nrseqcaixa IN ( " & nrseq & " )"
        Dim sqlDebitos As String = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE operacao = 'D' AND nrseqcaixa IN ( " & nrseq & " )"
        Dim sqlSaldoAtual As String = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & nrseq & " )"
        Dim sql As String = "SELECT * FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & nrseq & " )"

        datatable = findSql(sqlValorIncial)
        Dim valorInicial As Double = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))
        txtvalorinicial.Value = valorInicial
        datatable = findSql(sqlCreditos)
        txtcreditos.Value = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))
        datatable = findSql(sqlDebitos)
        txtdebitos.Value = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))
        datatable = findSql(sqlSaldoAtual)
        Dim lancamentos As Double = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma"))
        txtlancamentos.Value = lancamentos
        txtsaldo.Value = valorInicial + lancamentos
        'txtsaldoatual.Value = txtsaldo.Value

        datatable = findSql(sql)

        gdvLancamentos.DataSource = datatable
        gdvLancamentos.DataBind()
    End Sub

    Private Sub carregaDadosDoCaixa(nrseq As String)
        Dim datatable As Data.DataTable = New Data.DataTable
        Dim sql As String = "SELECT * FROM " & tabelaCaixas & " WHERE nrseq = " & nrseq

        datatable = findSql(sql)
        If datatable.Rows.Count > 0 Then
            txtaberto.Value = datatable.Rows(0)("nrseq")
            ddlCaixaFechado.SelectedValue = datatable.Rows(0)("fechado")
            Dim vlfinal As String = datatable.Rows(0)("vlfinal").ToString
            If vlfinal <> "" AndAlso vlfinal Then
                txtsaldoatual.Value = vlfinal
            Else
                Dim vlInicial As Double = IIf(datatable.Rows(0)("vlinicial").ToString = "", "0,00", datatable.Rows(0)("vlinicial"))
                Dim sqlSaldoAtual As String = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & nrseq & " )"
                datatable = findSql(sqlSaldoAtual)
                txtsaldoatual.Value = IIf(datatable.Rows(0)("soma").ToString = "", "0,00", datatable.Rows(0)("soma")) + vlInicial
            End If
        End If
    End Sub

    Private Sub carregarFuncionario()
        Dim datatable As Data.DataTable = New Data.DataTable
        datatable = carregaEmpresa()
        Dim sql As String = "SELECT * FROM " & tabelaUsuario & " WHERE usuario = '" & datatable.Rows(0)("nome") & "' OR usuario = '' OR usuario IS NULL"

        datatable = findSql(sql)
        If datatable.Rows.Count > 0 Then
            ddlFuncionario.Items.Clear()
            ddlFuncionario.Items.Add(New ListItem("Selecione o Funcionário", ""))
            For x As Integer = 0 To datatable.Rows.Count - 1
                ddlFuncionario.Items.Add(New ListItem(datatable.Rows(x)("usuario").ToString, datatable.Rows(x)("usuario").ToString))
            Next
        End If
    End Sub

    Private Sub carregarNumCaixas(Optional fromFuncionario As Boolean = False)
        Dim datatable As Data.DataTable = New Data.DataTable
        Dim sql As String = "SELECT * FROM " & tabelaCaixas & " WHERE usuario = '" & ddlFuncionario.SelectedValue & "' "
        If fromFuncionario = False Then
            If ddlDtcaixa.SelectedValue <> "" Then
                sql &= " AND data = '" & formatadatamysql(ddlDtcaixa.SelectedValue) & "' "
            End If
        End If
        sql &= " ORDER BY nrseq DESC"
        datatable = findSql(sql)
        If datatable.Rows.Count > 0 Then
            If ddlNumCaixa.Items.Count = 0 Then
                adicionaNumCaixa(datatable)
            Else
                Dim nrseq = datatable.Rows(0)("nrseq").ToString
                Dim ddlValue = ddlNumCaixa.Items(1).Value
                Dim ddlSelected = ddlNumCaixa.SelectedValue
                If IIf(ddlSelected = nrseq, ddlSelected <> nrseq, ddlValue <> nrseq) Then
                    adicionaNumCaixa(datatable)
                End If
            End If
        Else
            ddlNumCaixa.Items.Clear()
        End If
    End Sub

    Private Sub carregarDataCaixa(Optional fromFuncionario As Boolean = False)
        Dim datatable As Data.DataTable = New Data.DataTable
        Dim sql As String = "SELECT * FROM " & tabelaCaixas & " WHERE usuario = '" & ddlFuncionario.SelectedValue & "' "
        If fromFuncionario = False Then
            If ddlNumCaixa.SelectedValue <> "" Then
                sql &= " AND nrseq = '" & ddlNumCaixa.SelectedValue & "' "
            End If
        End If
        sql &= " ORDER BY data DESC"
        datatable = findSql(sql)
        If datatable.Rows.Count > 0 Then
            If ddlDtcaixa.Items.Count = 0 Then
                adicionaDataCaixa(datatable)
            Else
                Dim data = datatable.Rows(0)("data").ToString
                Dim ddlValue = ddlDtcaixa.Items(1).Value
                Dim ddlSelected = ddlDtcaixa.SelectedValue
                If IIf(ddlSelected = data, ddlSelected <> data, ddlValue <> data) Then
                    adicionaDataCaixa(datatable)
                End If
            End If
        Else
            ddlDtcaixa.Items.Clear()
        End If
    End Sub

    Private Sub adicionaNumCaixa(datatable As DataTable)
        ddlNumCaixa.Items.Clear()
        ddlNumCaixa.Items.Add(New ListItem("Selecione o Nº de Caixa", ""))
        For x As Integer = 0 To datatable.Rows.Count - 1
            Dim nrseq = datatable.Rows(x)("nrseq").ToString
            ddlNumCaixa.Items.Add(New ListItem(nrseq, nrseq))
        Next
    End Sub

    Private Sub adicionaDataCaixa(datatable As DataTable)
        ddlDtcaixa.Items.Clear()
        ddlDtcaixa.Items.Add(New ListItem("Selecione uma data", ""))
        For x As Integer = 0 To datatable.Rows.Count - 1
            ddlDtcaixa.Items.Add(New ListItem(Left(datatable.Rows(x)("data").ToString, 10), datatable.Rows(x)("data").ToString))
        Next
    End Sub

    Private Sub carregaOperacoes()
        ddlOperacao.Items.Add(New ListItem("Selecione a operação", ""))
        ddlOperacao.Items.Add(New ListItem("Crédito", "C"))
        ddlOperacao.Items.Add(New ListItem("Débito", "D"))
    End Sub

    Private Sub carregaFechamento()
        ddlCaixaFechado.Items.Add(New ListItem("Aberto", "0"))
        ddlCaixaFechado.Items.Add(New ListItem("Fechado", "1"))
    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub restrito_gerenciacaixa_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("usuario") = Nothing Then
            Response.Redirect("login.aspx")
        End If
        sm("carregaMaskMoney()", "carregaMaskMoney")
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "carregaJS", "carregaJS()", True)
        If Not IsPostBack Then
            carregarFuncionario()
            carregaDocumentos()
            carregaOperacoes()
            carregaFechamento()
            carregaFuncionarioGDV()
            habilitar(False, 1)
            btnAbrirCaixa.Enabled = False
            btnFecharCaixa.Enabled = False
        Else
            carregaFuncionarioGDV()
        End If
    End Sub

    Private Sub carregaFuncionarioGDV()
        Dim sql As String = "Select f.nrseq As nrsequser, f.usuario As funcionario, c.nrseq As caixa, COALESCE(SUM(d.valor),0) + c.vlinicial As valoratual, COALESCE(c.fechado, 1) As cfechado "
        sql &= " FROM " & tabelaUsuario & " As f "
        sql &= " LEFT JOIN " & tabelaCaixas & " As c On c.usuario = f.usuario "
        sql &= " LEFT JOIN " & tabelaDetalhes & " As d On d.nrseqcaixa = c.nrseq "
        sql &= " WHERE c.nrseq In (Select MAX(nrseq) FROM " & tabelaCaixas & " WHERE usuario = f.usuario) Or c.nrseq Is null GROUP BY f.nrseq, f.usuario, c.nrseq, c.fechado "
        gdvFuncionarios.DataSource = findSql(sql)
        gdvFuncionarios.DataBind()

    End Sub

    Private Sub btnAbrirLancamento_Click(sender As Object, e As EventArgs) Handles btnAbrirLancamento.Click
        carregaCaixa()
    End Sub

    Private Sub btnDesbloquearProcura_Click(sender As Object, e As EventArgs) Handles btnDesbloquearProcura.Click
        habilitar(False, 1, True, True)
        habilitar(True, 0)
    End Sub

    Private Sub ddlFuncionario_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFuncionario.SelectedIndexChanged
        carregarDataCaixa(True)
        carregarNumCaixas(True)
    End Sub

    Private Sub ddlNumCaixa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlNumCaixa.SelectedIndexChanged
        carregarDataCaixa()
    End Sub

    Private Sub ddlDtcaixa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDtcaixa.SelectedIndexChanged
        carregarNumCaixas()
    End Sub

    Private Sub gdvLancamentos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvLancamentos.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim i As Integer = 5
        If e.Row.Cells(i).Text > 0 Then
            e.Row.ForeColor = System.Drawing.Color.Green
        ElseIf e.Row.Cells(i).Text < 0 Then
            e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#d50000")
        End If
        i = 1
        e.Row.Cells(i).Text = Left(e.Row.Cells(i).Text, 10)
        i = 4
        If e.Row.Cells(i).Text = "C" Then
            e.Row.Cells(i).Text = "Crédito"
        Else
            e.Row.Cells(i).Text = "Débito"
        End If
        i = 5
        e.Row.Cells(i).Text = "R$ " & e.Row.Cells(i).Text
    End Sub

    Private Sub btnSalvar_Click(sender As Object, e As EventArgs) Handles btnSalvar.Click
        rowError.Style.Add("display", "none")
        If txtaberto.Value <> "" AndAlso txtdescricao.Value <> "" AndAlso ddlOperacao.SelectedValue <> "" AndAlso txtvalor.Value <> "" Then
            Dim datatable As Data.DataTable = New Data.DataTable
            Dim nrseqctrl As String = Session("usuario") & geraCodigo()
            If ddlOperacao.SelectedValue = "D" Then
                txtvalor.Value = txtvalor.Value * (-1)
            Else
            End If
            Dim sql As String = "INSERT INTO " & tabelaDetalhes & " (nrseqcaixa, descricao, operacao, valor, qtdparcelas, usercad, dtcad, nrseqctrl, ativo, nrseqdocumento) VALUES "
            sql &= " ('" & Session("nrseqcaixa") & "', '" & txtdescricao.Value & "', '" & ddlOperacao.SelectedValue & "', '" & moeda(txtvalor.Value) &
                "', '1','" & Session("usuario") & "', '" & formatadatamysql(DateTime.Now) & "', '" & nrseqctrl & "', 1, " &
                IIf(ddlPagamento.SelectedValue <> "", "'" & ddlPagamento.SelectedValue & "'", "Null") & ")"

            datatable = persist(sql)
            limpar()
            carregaCaixa()
            '  clssessoes.atualizarCookieTemporario()
        Else
            Dim erro As String = ""
            ' Quantas você quiser dessa coisa... pra cada campo
            erro = verificaCampo(txtaberto.Value, "Caixa em Aberto", erro)
            erro = verificaCampo(txtdescricao.Value, "Descrição", erro)
            erro = verificaCampo(ddlOperacao.SelectedValue, "Operação", erro)
            erro = verificaCampo(txtvalor.Value, "Valor", erro)
            If erro <> "" Then
                rowError.Style.Remove("display")
                lblErro.InnerHtml = erro
            End If
        End If
    End Sub

    Private Sub btnImprimir_Click(sender As Object, ea As EventArgs) Handles btnImprimir.Click
        If ddlFuncionario.SelectedValue <> "" Then
            Dim datatable As DataTable = carregaEmpresa()
            Dim c As String = datatable.Rows(0)("nome").ToString
            Dim e As String = Session("usuario")
            Dim i As String = ""
            Dim r As String = "Sem Endereço"
            Dim t As String = "Sem Telefone"
            Dim u As String = ddlFuncionario.SelectedValue

            Session("cRelatorio") = c
            Session("eRelatorio") = e
            Session("rRelatorio") = r
            Session("uRelatorio") = u

            Dim sql As String = "SELECT * FROM " & tabelaCaixas & " WHERE usuario = '" & ddlFuncionario.SelectedValue & "' "
            If ddlDtcaixa.SelectedValue <> "" Then
                sql &= " AND data = '" & formatadatamysql(ddlDtcaixa.SelectedValue) & "' "
            End If
            If ddlNumCaixa.SelectedValue <> "" Then
                sql &= " AND nrseq = '" & ddlNumCaixa.SelectedValue & "' "
            End If
            datatable = findSql(sql)
            If datatable.Rows.Count > 0 Then
                Dim nrseq = datatable.Rows(0)("nrseq").ToString
                Session("nrseqcaixa") = nrseq
                If nrseq <> "" AndAlso nrseq <> "" Then
                    i = converteDatatableToString(datatable)
                End If
            End If

            Response.Redirect("/restrito/relatorios/gerenciacaixa.aspx?c=" & Server.UrlEncode(c) & "&e=" & Server.UrlEncode(e) & "&i=" & Server.UrlEncode(i) & "&r=" & Server.UrlEncode(r) & "&t=" & Server.UrlEncode(t) & "&u=" & Server.UrlEncode(u))
        End If
    End Sub

    Private Sub gdvFuncionarios_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gdvFuncionarios.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gdvFuncionarios.Rows(index)
        Dim datatable As New Data.DataTable
        If e.CommandName = "editar" Then
            Dim i As Integer = 1
            txtnome.Value = HttpUtility.HtmlDecode(row.Cells(i).Text)
            i = 2
            txtnumcaixa.Value = row.Cells(i).Text

            If txtnumcaixa.Value <> "" AndAlso txtnumcaixa.Value <> "&nbsp;" AndAlso txtnumcaixa.Value <> 0 Then
                txtinicial.Disabled = True
                btnAbrirCaixa.Enabled = False
                btnFecharCaixa.Enabled = True
            Else
                txtinicial.Disabled = False
                btnAbrirCaixa.Enabled = True
                btnFecharCaixa.Enabled = False
            End If
            If txtnumcaixa.Value <> "" AndAlso txtnumcaixa.Value <> "&nbsp" Then

                Dim sql As String = "SELECT * FROM " & tabelaCaixas & " WHERE nrseq = '" & txtnumcaixa.Value & "' "

                datatable = findSql(sql)
                If datatable.Rows(0)("fechado").ToString <> "1" AndAlso datatable.Rows(0)("fechado").ToString <> True Then
                    txtinicial.Value = datatable.Rows(0)("vlinicial").ToString
                    sql = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & txtnumcaixa.Value & " )"
                    txttotal.Value = findSql(sql).Rows(0)("soma").ToString
                End If

            End If
        End If
        carregaFuncionarioGDV()
        '  clssessoes.atualizarCookieTemporario()
    End Sub

    Private Sub gdvFuncionarios_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gdvFuncionarios.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim i As Integer = 4
        If e.Row.Cells(i).Text = "1" Then
            i = 2
            e.Row.Cells(i).Text = ""
            i = 3
            e.Row.Cells(i).Text = ""
            e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00c853")
        Else
            i = 3
            e.Row.Cells(i).Text = "R$ " & HttpUtility.HtmlDecode(e.Row.Cells(i).Text)
            e.Row.ForeColor = System.Drawing.ColorTranslator.FromHtml("#d50000")
        End If

    End Sub

    Private Sub btnFecharCaixa_Click(sender As Object, e As EventArgs) Handles btnFecharCaixa.Click
        Dim tabela As New Data.DataTable
        Dim sql As String = ""
        Dim sqlSoma As String = ""
        If txtnome.Value <> "" AndAlso txtnome.Value <> "&nbsp;" Then
            If txtnumcaixa.Value <> "" AndAlso txtnumcaixa.Value <> 0 AndAlso txtnumcaixa.Value <> "&nbsp;" Then

                sqlSoma = "SELECT SUM(valor) AS soma FROM " & tabelaDetalhes & " WHERE nrseqcaixa IN ( " & txtnumcaixa.Value & " )"
                tabela = findSql(sqlSoma)
                If txtinicial.Value = "" OrElse txtinicial.Value = 0 OrElse txtinicial.Value = "&nbsp;" Then
                    txtinicial.Value = 0
                End If
                If txttotal.Value = "" OrElse txttotal.Value = 0 OrElse txttotal.Value = "&nbsp;" Then
                    txttotal.Value = IIf(tabela.Rows(0)("soma").ToString <> "", tabela.Rows(0)("soma").ToString, 0)
                End If
                ' Pegando valor pro lançamento final
                Dim vlLancamentoFinal As Double = txttotal.Value - txtinicial.Value - IIf(tabela.Rows(0)("soma").ToString <> "", tabela.Rows(0)("soma").ToString, 0)
                'Definindo Operação
                Dim operacao As String = "C"
                If vlLancamentoFinal < 0 Then
                    operacao = "D"
                ElseIf vlLancamentoFinal > 0 Then
                    ' Fazendo o lançamento Final
                    Dim nrseqctrl As String = Session("usuario") & geraCodigo()
                    sql = "INSERT INTO " & tabelaDetalhes & " (nrseqcaixa, descricao, operacao, valor, qtdparcelas, usercad, dtcad, nrseqctrl, ativo) VALUES "
                    sql &= " ('" & txtnumcaixa.Value & "', 'Lançamento via Gerencia de Caixa', '" & operacao & "', '" & vlLancamentoFinal &
                        "', '1','" & Session("usuario") & "', '" & formatadatamysql(DateTime.Now) & "', '" & nrseqctrl & "', 1)"
                    tabela = persist(sql)
                End If
                ' Finalmente realizando o fechamento 
                tabela = findSql(sqlSoma)
                Dim soma As Double = CType(IIf(tabela.Rows(0)("soma").ToString <> "", tabela.Rows(0)("soma").ToString, 0), Double) +
                 CType(IIf(txtinicial.Value <> "", txtinicial.Value, 0), Double)
                sql = "update tbcaixas set fechado = true, dtfechamento = '" & formatadatamysql(data) & "', userfechado = '" & Session("usuario") & "', vlfinal = '" & moeda(soma) & "', vlinicial = '" & moeda(txtinicial.Value) & "'  where nrseq = " & txtnumcaixa.Value
                tabela = persist(sql)

                inserirNaConta(txtnumcaixa.Value)

                txtnome.Value = ""
                txtinicial.Value = ""
                txttotal.Value = ""
                txtnumcaixa.Value = ""
                btnAbrirCaixa.Enabled = False
                btnFecharCaixa.Enabled = False

            End If
        End If
        carregaFuncionarioGDV()
        ' clssessoes.atualizarCookieTemporario()
    End Sub

    Private Sub btnAbrirCaixa_Click(sender As Object, e As EventArgs) Handles btnAbrirCaixa.Click
        rowErrorAbertura.Style.Add("display", "none")
        Dim sql As String = "select * from tbcaixas where usuario = '" & txtnome.Value & "' and fechado = false order by nrseq desc"
        Dim tabela As Data.DataTable = findSql(sql)
        If txtinicial.Value <> "" AndAlso txtnumcaixa.Value = "" AndAlso txtnome.Value <> "" Then
            Dim wcnrseqctrl As String = Session("usuario") & geraCodigo()
            sql = "insert into tbcaixas (usuario, data, ativo, fechado, vlinicial, vlfinal, nrseqctrl) values ('" & txtnome.Value & "','" & formatadatamysql(data) & "',true,false," & moeda(txtinicial.Value) & ",0,'" & wcnrseqctrl & "')"
            tabela = persist(sql)
            sql = "select * from tbcaixas where nrseqctrl = '" & wcnrseqctrl & "'"
            tabela = findSql(sql)
            If tabela.Rows.Count > 0 Then
                txtnumcaixa.Value = tabela.Rows(0)("nrseq").ToString
                btnFecharCaixa.Enabled = True
                btnAbrirCaixa.Enabled = False
                txtinicial.Disabled = True
            End If
        Else
            Dim erro As String = ""
            ' Quantas você quiser dessa coisa... pra cada campo
            erro = verificaCampo(txtnome.Value, "Funcionario Selecionado", erro)
            erro = verificaCampo(txtinicial.Value, "Valor Inicial", erro)
            ' erro = verificaCampo(txtnumcaixa.Value, "Número do Caixa", erro)
            If erro <> "" Then
                rowErrorAbertura.Style.Remove("display")
                lblErrorAbertura.InnerHtml = erro
            End If
        End If
        carregaFuncionarioGDV()
        '  clssessoes.atualizarCookieTemporario()
    End Sub
End Class

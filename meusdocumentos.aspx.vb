﻿Imports System.IO
Imports clsSmart
Imports System.Windows
Imports System.IO.Compression


Partial Class autoatendimento_meusdocumentos
    Inherits System.Web.UI.Page
    Private Sub meusdocumentos_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("idassociado") = "" Then

            Response.Redirect("loginassociados.aspx")
        End If

        If IsPostBack Then

            Exit Sub
        End If
        carregagrade()

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Public Sub carregagrade()

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        Dim consulta As String
        Dim where As String

        tb1 = tab1.conectar("select * from tbdocumentosassociados where matricula = '" & Session("idmatricula") & "'  and ativo = '1' ")

        grade.DataSource = tb1
        grade.DataBind()
    End Sub

    Private Sub grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grade.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim arquivo As HiddenField = row.FindControl("hdarquivo")

        If e.CommandName = "baixar" Then

            Dim wcarquivo As String = arquivo.Value
            Response.ContentType = "application/octet-stream"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment; filename=" & wcarquivo)
            Response.TransmitFile(Server.MapPath("~\documentosassociados\") & wcarquivo)
            Response.End()

            sm("swal({title: 'Atenção!',text: 'Não foi possivel Baixar o documento',type: 'error',confirmButtonText: 'OK'})", "swal")

        End If

        If e.CommandName = "link2" Then

            Dim wcarquivo As String = arquivo.Value
            Dim caminho As String = "~\documentosassociados\"
            Dim hlink As String


            hlink = caminho + wcarquivo

            Response.Write("<script> window.open('" & hlink & "','_blank') </script>")

        End If

    End Sub

End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="marcarconsulta.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="marcarconsulta" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="box box-info" style="-webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box-header with-border">
            <h3 class="box-title">Marcar Consulta</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-lg-6 ">

                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server" Text="Plano"></asp:Label>
                                        <br />
                                        <asp:DropDownList ID="cboplano" runat="server" AutoPostBack="true" CssClass="form-control" Enabled="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <%-- <div class="col-lg-5">
                        <div class="form-group">
                            <asp:Label ID="Label17" runat="server" Text="Serviço"></asp:Label>
                            <br />
                            <asp:DropDownList ID="cboservico" runat="server" CssClass="form-control" Enabled="true">
                                <asp:ListItem Text="Consulta" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Procedimentos" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>--%>

                                <div class="col-lg-2">
                                    <asp:CheckBox Text="Retorno" CssClass="checkbox-inline fix-check" runat="server" ID="chkretorno" />
                                </div>
                                <div class="col-lg-2">
                                    <asp:CheckBox Text="Primeira" CssClass="checkbox-inline fix-check" runat="server" ID="chkprimeiraconsulta" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 ">

                                    <asp:Label ID="Label2" runat="server" Text="Procurar Por"></asp:Label>
                                    <br />
                                    <asp:DropDownList ID="cboprocurarcliente" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Nome" Value="nome"></asp:ListItem>
                                        <asp:ListItem Text="Matricula" Value="matricula"></asp:ListItem>
                                        <asp:ListItem Text="CPF" Value="cpf"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                                <div class="col-lg-6">
                                    <asp:HiddenField ID="hdnrseqcliente" runat="server" />
                                    <asp:Label ID="Label3" runat="server" Text="Cliente: "></asp:Label>
                                    <asp:Label ID="lblretmatricula" runat="server" Text="" AutoPostBack="true"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtcliente" runat="server" CssClass="form-control" AutoPostBack="true">

                                    </asp:TextBox>
                                </div>
                                <div class="col-lg-1">
                                    <br />
                                    <asp:LinkButton ID="btnbuscacliente" runat="server" CssClass="btn btn-info"><i class="fa fa-search" title=""></i></asp:LinkButton>

                                </div>
                                <div class="col-lg-2">

                                    <%--    <asp:LinkButton ID="btnescolha1" runat="server" CssClass="btn btn-primary btn-sm" ><i class="fa fa-thumb-tack" title="selecionar"></i></asp:LinkButton>--%>

                                    <%--                                <asp:LinkButton ID="btncancelar1" runat="server" CssClass="btn btn-danger btn-sm" ><i class="fa fa-mail-reply"></i></asp:LinkButton>--%>
                                    <br />
                                    <asp:LinkButton ID="btnnovocliente" runat="server" CssClass="btn btn-primary" title="Cadastra novo cliente"><i class="fa fa-user-plus" title="novo"></i></asp:LinkButton>

                                    <%--    <asp:LinkButton ID="btnhabilitadependente" runat="server" CssClass="btn btn-primary btn-sm" ><i class="fa fa-users"></i></asp:LinkButton>--%>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                                        <ProgressTemplate>
                                            <center>
                                              <img src="../img/loadred.gif" style="height:3rem;width:3rem;" />
                                                    </center>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                            <div class="row" id="divmostragrade" runat="server">
                                <div class="col-lg-12">
                                    <asp:GridView ID="gradeclientes" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                        <AlternatingRowStyle BackColor="AntiqueWhite" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                                    <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("nrseq") %>' />
                                                    <asp:LinkButton ID="selecionar" runat="server" CssClass="btn btn-default btn-xs" CommandName="selecionar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Selecionar"> 
                                            <i class="fas fa-edit"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                            <asp:BoundField DataField="nome" HeaderText="Nome" />
                                            <asp:BoundField DataField="telcelular" HeaderText="Contato" />
                                            <asp:BoundField DataField="liberado" HeaderText="Ativo" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="updatedependente" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-10">
                                            <asp:CheckBox ID="chkusardependente" runat="server" Text="Selecionar dependente" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row" runat="server" id="mostradependente">
                                        <div class="col-lg-10">
                                            <asp:Label ID="Label5" runat="server" Text="Dependente"></asp:Label>
                                            <br />
                                            <asp:DropDownList ID="cbodependente" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <%--   <div class="col-lg-2 ">
                                            <br />
                                            <asp:LinkButton ID="btndependentefixa" runat="server" CssClass="btn btn-primary  btn-sm"><i class="fa fa-search"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnvoltadependente" runat="server" CssClass="btn btn-danger  btn-sm" title="Cancelar"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>--%>
                                    </div>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div class="row">
                                <div class="col-lg-10">
                                    <asp:Label ID="lblmedicos" runat="server" Text="Profissional">
                                        <asp:Label ID="soteste" Text="text" runat="server" /></asp:Label>
                                    <br />
                                    <asp:DropDownList ID="cbomedicos" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                              <%--  <div class="col-lg-2 ">
                                    <br />
                                    <asp:LinkButton ID="btnescolha2" runat="server" CssClass="btn btn-primary btn-sm"><i class="fa fa-thumb-tack" ></i></asp:LinkButton>
                                    <%--                        <asp:LinkButton ID="btncancelar2" runat="server" CssClass="btn btn-danger btn-sm" title="Cancelar" ><i class="fa fa-mail-reply"></i></asp:LinkButton>
                                </div>--%>
                            </div>

                            <div class="row">
                                <div class="col-lg-10">
                                    <asp:Label ID="lblespecialidades" runat="server" Text="Especialidade"></asp:Label>
                                    <br />
                                    <asp:DropDownList ID="cboespecialidades" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:HiddenField runat="server" ID="hdnvlrepasse" />
                                    <asp:HiddenField ID="hdchaveconsulta" runat="server" />
                                </div>
                                <div class="col-lg-2 ">
                                    <br />
                                    <asp:LinkButton ID="brnprocedimento" runat="server" CssClass="btn btn-primary btn-sm" title="Adicionar procedimento"><i class="fa fa-heartbeat"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btncancelar3" Visible="false" runat="server" CssClass="btn btn-danger btn-sm" title="Cancelar"><i class="fa fa-mail-reply" ></i></asp:LinkButton>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <asp:Label ID="Label6" runat="server" Text="Carteira Convênio"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="txtcaiteiraconv" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-6">
                                    <asp:Label ID="Label4" runat="server" Text="Plano Convênio"></asp:Label>
                                    <br />
                                    <asp:DropDownList ID="cboplanoconvenio" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 ">
                                    <asp:Label ID="Label7" runat="server" Text="Valor da consulta"></asp:Label>
                                    <br />

                                    <asp:TextBox runat="server" ID="txtvalor" ReadOnly="true" type="text" CssClass="form-control"> </asp:TextBox>

                                </div>
                                <div class="col-lg-4 ">
                                    <asp:Label ID="Label9" runat="server" Text="Valor do Procedimento"></asp:Label>
                                    <br />
                                    <asp:TextBox runat="server" ID="txtvalorprocedimentos" ReadOnly="true" CssClass="form-control"> </asp:TextBox>
                                </div>
                                <div class="col-lg-1 ">
                                    <br />
                                    <asp:Label ID="Label11" runat="server" Text="="></asp:Label>

                                </div>
                                <div class="col-lg-3 ">
                                    <asp:Label ID="Label10" runat="server" Text="Valor"></asp:Label>
                                    <br />
                                    <asp:TextBox runat="server" ID="txtsomadovalor" ReadOnly="true" type="text" CssClass="form-control"> </asp:TextBox>

                                </div>

                            </div>


                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <div class="col-lg-6 ">
                    <!-- /.agenda -->
                    <asp:HiddenField runat="server" ID="hdacao" />
                    <div class="box box-info" runat="server" id="divagendamedica">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-calendar"></i>Agenda Medica</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:UpdatePanel ID="updategrade" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <asp:TextBox data-date-format="dd/mm/yyyy" data-date-autoclose="true" ID="dtagendamedica" runat="server" ClientIDMode="Static" data-provide="datepicker" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                                        </div>


                                        <div class="col-lg-3 ">
                                            <asp:TextBox Type="time" runat="server" ID="txtencaixe" CssClass="form-control" />
                                        </div>

                                        <%-- <div class="col-lg-2 ">
                                <asp:LinkButton ID="btnencaixe" runat="server" CssClass="btn btn-warning btn-sm"><i class="fa fa-clock-o">Encaixe</i></asp:LinkButton>
                            </div>--%>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 text-center">
                                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updategrade">
                                                <ProgressTemplate>
                                                    <center>
                                              <img src="../img/loadred.gif" style="height:3rem;width:3rem;" />
                                                    </center>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <br />
                                            <asp:GridView ID="gradeohorarios" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none" EmptyDataText="Nenhum horário localizado para esse dia na agenda do profissional !">
                                                <AlternatingRowStyle BackColor="AntiqueWhite" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblitemgrade" runat="server" Text='<%#Container.DataItemIndex + 1 %>' CssClass="badge"></asp:Label>
                                                            <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("nrseq") %>' />
                                                            <asp:CheckBox ID="chkselgrade" runat="server" />
                                                            <%-- <asp:LinkButton ID="selecionar" runat="server" CssClass="btn btn-default btn-xs" CommandName="selecionar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Selecionar"> 
                                            <i class="fas fa-user-plus"></i>
                                                        </asp:LinkButton>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="hora" HeaderText="Hora" />
                                                    <asp:BoundField DataField="paciente" HeaderText="Paciente" />
                                                    <asp:TemplateField HeaderText="Opções">
                                                        <ItemTemplate runat="server">
                                                            <%-- <asp:LinkButton ID="agenda" runat="server" CssClass="btn btn-default btn-xs" CommandName="agenda" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Agenda">
                                           <i class="fas fa-calendar-plus-o"></i>
                                                        </asp:LinkButton>--%>
                                                            <asp:LinkButton ID="bloquear" runat="server" CssClass="btn btn-danger btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Bloquear ou Desbloquear">
                                           <i class="fa fa-user"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <!-- /.procedimentos -->
                    <div class="box box-info" runat="server" id="divprocedimento">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <i class="fa fa-heartbeat"></i>Lista de Procedimentos</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>

                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <br />
                                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                                <AlternatingRowStyle BackColor="AntiqueWhite" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                                            <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("cod") %>' />

                                                            <asp:LinkButton ID="Edit" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar"> 
                                            <i class="fas fa-edit"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="cod" HeaderText="Código" />
                                                    <asp:BoundField DataField="nome" HeaderText="Medico" />
                                                    <asp:BoundField DataField="celular" HeaderText="Celular" />
                                                    <asp:BoundField DataField="email" HeaderText="E-mail" />

                                                    <asp:TemplateField HeaderText="Opções">
                                                        <ItemTemplate runat="server">

                                                            <asp:LinkButton ID="envioagenda" runat="server" CssClass="btn btn-default btn-xs" CommandName="envioagenda" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Enviar agenda por e-mail">
                                           <i class="fas fa-envelope"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="horario" runat="server" CssClass="btn btn-default btn-xs" CommandName="horario" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Horario">
                                           <i class="fas fa-clock"></i>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="especialidades" runat="server" CssClass="btn btn-default btn-xs" CommandName="especialidades" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Especialidade">
                                           <i class="fas fa-heartbeat"></i>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="agenda" runat="server" CssClass="btn btn-default btn-xs" CommandName="agenda" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Agenda">
                                           <i class="fas fa-calendar-plus-o"></i>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="bloquear" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Bloquear ou Desbloquear">
                                           <i class="fa fa-user"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="suspenso" runat="server" CssClass="btn btn-default btn-xs" CommandName="suspenso" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Desativar usuário">
                                            <i class="fas fa-thumbs-down"></i> 
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="reativar" runat="server" CssClass="btn btn-default btn-xs" CommandName="reativar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reativar usuário">
                                            <i class="fas fa-thumbs-up"></i> 
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="resetsenha" runat="server" CssClass="btn btn-default btn-xs" CommandName="resetsenha" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reset de senha">
                                            <i class="fas fa-key"></i> 
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="senhapemail" runat="server" CssClass="btn btn-default btn-xs" CommandName="senhapemail" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Enviar senha por e-mail">
                                           <i class="fas fa-envelope"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <!-- /.dados financeiro do cliente -->
                    <div class="box box-info" runat="server" id="divdadosfim">
                        <div class="box-header with-border">
                            <h3 class="box-title">Dados Financeiro</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <br />
                                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                                <AlternatingRowStyle BackColor="AntiqueWhite" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                                            <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("cod") %>' />

                                                            <asp:LinkButton ID="Edit" runat="server" CssClass="btn btn-default btn-xs" CommandName="editar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Editar"> 
                                            <i class="fas fa-edit"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="cod" HeaderText="Código" />
                                                    <asp:BoundField DataField="nome" HeaderText="Medico" />
                                                    <asp:BoundField DataField="celular" HeaderText="Celular" />
                                                    <asp:BoundField DataField="email" HeaderText="E-mail" />

                                                    <asp:TemplateField HeaderText="Opções">
                                                        <ItemTemplate runat="server">

                                                            <asp:LinkButton ID="envioagenda" runat="server" CssClass="btn btn-default btn-xs" CommandName="envioagenda" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Enviar agenda por e-mail">
                                           <i class="fas fa-envelope"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="horario" runat="server" CssClass="btn btn-default btn-xs" CommandName="horario" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Horario">
                                           <i class="fas fa-clock"></i>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="especialidades" runat="server" CssClass="btn btn-default btn-xs" CommandName="especialidades" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Especialidade">
                                           <i class="fas fa-heartbeat"></i>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="agenda" runat="server" CssClass="btn btn-default btn-xs" CommandName="agenda" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Agenda">
                                           <i class="fas fa-calendar-plus-o"></i>
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="bloquear" runat="server" CssClass="btn btn-default btn-xs" CommandName="excluir" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Bloquear ou Desbloquear">
                                           <i class="fa fa-user"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="suspenso" runat="server" CssClass="btn btn-default btn-xs" CommandName="suspenso" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Desativar usuário">
                                            <i class="fas fa-thumbs-down"></i> 
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="reativar" runat="server" CssClass="btn btn-default btn-xs" CommandName="reativar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reativar usuário">
                                            <i class="fas fa-thumbs-up"></i> 
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="resetsenha" runat="server" CssClass="btn btn-default btn-xs" CommandName="resetsenha" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Reset de senha">
                                            <i class="fas fa-key"></i> 
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="senhapemail" runat="server" CssClass="btn btn-default btn-xs" CommandName="senhapemail" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Enviar senha por e-mail">
                                           <i class="fas fa-envelope"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-lg-12">
                    <hr style="border-color: steelblue" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <br />
                    <asp:LinkButton ID="btnsalvarconsulta" runat="server" CssClass="btn btn-warning "><i class="fa fa-calendar-plus-o" aria-hidden="true"></i>  Salvar a consulta</asp:LinkButton>
                </div>
            </div>
        </div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>


                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog" style="width: 80vw; height: 80vh;">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title text-center">
                                    <asp:Label ID="lblnrseqpropostamodal" runat="server" CssClass="label-success"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <center>
                                <div id="mdvisualizar" runat="server">
      <div class="row">
          <div class="col-lg-2">
              <asp:HiddenField runat="server" ID="hdnnrseqmodal" />
              Matricula
               <asp:TextBox ID="txtmatriculacliente" Enabled="false" runat="server" CssClass="form-control "></asp:TextBox>
          </div>
            <div class="col-lg-3">
                CPF
                <asp:TextBox ID="txtcpfcliente" MaxLength="14" onkeyup="formataCnpjCpf(this,event);" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-lg-5">
                Nome
                <asp:TextBox ID="txtnomecliente" runat="server" CssClass="form-control" ></asp:TextBox>
            </div>
            <div class="col-lg-2 text-center">
              
                <asp:LinkButton ID="btnprocurarcliente" runat="server" CssClass="btn btn-warning ">Localizar</asp:LinkButton>
            </div>

        </div>
   <div class="row">
                    <div class="col-lg-5">
                 Data de nascimento
              <asp:TextBox runat="server" onkeyup="formataData(this,event);" MaxLength="10" CssClass="form-control" ID='txtdtnascimentocliente' ClientIDMode="Static" />

                 </div>
                    <div class="col-lg-5">
                Plano
              <asp:DropDownList runat="server" CssClass="form-control" Enabled="false" ID="cboplanocliente" AutoPostBack="true" ClientIDMode="Static" ></asp:DropDownList>

                 </div>
   </div>
                        
         <div class="row">
            
            <div class="col-lg-5">
                Telefone
                
                <asp:TextBox ID="txttelcliente" onkeyup="formataTelefone(this,event);" MaxLength="14" runat="server" CssClass="form-control "></asp:TextBox>
            </div>
            <div class="col-lg-5">
                Celular
                
                <asp:TextBox ID="txtcelularcliente" onkeyup="formataTelefone(this,event);" MaxLength="15" runat="server" CssClass="form-control "></asp:TextBox>
            </div>

            <div class="col-lg-2">
                  <br />
                <asp:LinkButton ID="btnsalvarnovocliente" runat="server" CssClass="btn btn-success ">Salvar</asp:LinkButton>
            </div>

        </div>

                                   <div class="row">
                                <div class="col-lg-12">
                                    <asp:GridView ID="gradeclientesmodal" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                        <AlternatingRowStyle BackColor="AntiqueWhite" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                                    <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("nrseq") %>' />
                                                    <asp:LinkButton ID="selecionar" runat="server" CssClass="btn btn-default btn-xs" CommandName="selecionar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Selecionar"> 
                                            <i class="fas fa-edit"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="matricula" HeaderText="Matricula" />
                                            <asp:BoundField DataField="nome" HeaderText="Nome" />
                                            <asp:BoundField DataField="telcelular" HeaderText="Contato" />
                                            <asp:BoundField DataField="liberado" HeaderText="Ativo" />

                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>

                                    </div>
                                <div id="mdarquivos" runat="server">
                                       <div class="col-lg-12">
                               

                                </div>
                                   </div> 
              </center>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="txtnomecliente" EventName="TextChanged" />
                <asp:AsyncPostBackTrigger ControlID="txtcpfcliente" EventName="TextChanged" />
                <%--     <asp:AsyncPostBackTrigger ControlID="btnsalvarnovocliente" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnprocurarcliente" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>

        <asp:UpdatePanel runat="server">
            <ContentTemplate>


                <div id="modalprocedimentos" class="modal fade" role="dialog">
                    <div class="modal-dialog" style="width: 80vw; height: 80vh;">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title text-center">
                                    <asp:Label ID="Label8" runat="server" CssClass="label-success">Procedimentos Agenda Medica
                                        <asp:Label Text="text" runat="server" ID="lblrchave" /></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>

                                        <center>
                                <div id="Div1" runat="server">
      <div class="row">
          <div class="col-lg-3">
              <asp:HiddenField runat="server" ID="hdnnrseqmodalproc" />
              <asp:HiddenField runat="server" ID="hdnrseqplanomodal" />
              Plano
               <asp:TextBox ID="cbompplan" Enabled="false" runat="server" CssClass="form-control "></asp:TextBox>
          </div>
            <div class="col-lg-4">
                Convenio
                <asp:DropDownList ID="cbompconv" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
            </div>
       
          <div class="col-lg-4">
                              Procedimentos
                <asp:DropDownList ID="cbompproc" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
          </div>
                   <div class="col-lg-1 ">
              <br />
                <asp:LinkButton ID="btnmplimpar" runat="server" CssClass="btn btn-warning ">Limpar Tudo</asp:LinkButton>
            </div>
    <%--      campo procedimento
          valor
          grade
          valor total--%>     
        </div>
                                      <div class="row">
                                            <div class=" col-lg-2">
                                                 Código<br />
                                                <asp:textbox runat="server" type="text" class="form-control" id="txtmodalproccodigo" clientidmode="Static"  ReadOnly="true" />
                                            </div>
                                            <div class="col-lg-5">
                                                 Descrição <br />
                                                <asp:TextBox runat="server" type="text" class="form-control" id="txtmodalprocdescricao"   ReadOnly="true"  />
                                            </div>
                                            <div class="col-lg-2">
                                                 Valor de Custo<br />
                                                <asp:TextBox runat="server" type="text" class="form-control moedasemnegativo" id="txtmodalprocvalorcusto" onkeyup="formataValor(this,event);"   ReadOnly="true"    />
                                            </div>
                                            <div class="col-lg-2">
                                                 Valor Final<br />
                                                <asp:TextBox runat="server" class="form-control" ID="txtmodalprocvalorfinal" AutoPostBack="true" onkeyup="formataValor(this,event);"  ReadOnly="true"  />
                                            </div>
                                            <div class="col-lg-1 ">
              <br />
                <asp:LinkButton ID="btnaddmodalproc" runat="server" CssClass="btn btn-primary ">ADD</asp:LinkButton>
            </div>
                                        </div>
                                       <div class="row">
                                             <div class="col-lg-10">
                Obs
                <asp:TextBox ID="txtobs" TextMode="MultiLine" style="resize:none;" runat="server" CssClass="form-control" ></asp:TextBox>
            </div>
                             <div class="col-lg-2">
                                           Valor total
                                            <asp:TextBox runat="server" ID="txtmpvaltotal" CssClass="form-control"></asp:TextBox>
                                        </div>    

   </div>

                                 
                                    <br />
                                    <div class="row">
                                        <div class="col-lg-10"></div>
                                       
                                    </div>

                                   <div class="row">
                                <div class="col-lg-12">
                                    <asp:GridView ID="gradempproced" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="50" ShowHeaderWhenEmpty="True" CssClass="table" GridLines="none">
                                        <AlternatingRowStyle BackColor="AntiqueWhite" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblitemgrade" runat="server" CssClass="badge"></asp:Label>
                                                    <asp:HiddenField ID="hdnrseq" runat="server" Value='<%#Bind("nrseq") %>' />
                                                    <asp:LinkButton ID="deletar" runat="server" CssClass="btn btn-default btn-xs" CommandName="deletar" CommandArgument='<%#Container.DataItemIndex%>' data-toggle="tooltip" title="Deletar"> 
                                            <i class="fas fa-edit"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Convenio" HeaderText="Convenio" />
                                            <asp:BoundField DataField="Procedimento" HeaderText="Procedimento" />
                                            <asp:BoundField DataField="Valor" HeaderText="Valor" />
                                            
                                        </Columns>
                                        
                                    </asp:GridView>
                                </div>
                            </div>

                                    </div>
                                <div id="Div2" runat="server">
                                       <div class="col-lg-12">
                               

                                </div>
                                   </div> 
              </center>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>

                                <asp:LinkButton ID="btnmpsalvar" type="button" runat="server" CssClass="btn btn-success ">Confirmar</asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="txtnomecliente" EventName="TextChanged" />
                <asp:AsyncPostBackTrigger ControlID="txtcpfcliente" EventName="TextChanged" />
                <%--     <asp:AsyncPostBackTrigger ControlID="btnsalvarnovocliente" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnprocurarcliente" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>

        <div class="row">
            <div class="col-lg-4 ">
                <asp:Button ID="btnAlterar" runat="server" Text="Alterar" Width="127px" CssClass="btn btn-primary " Visible="false" />
            </div>
            <div class="col-lg-4 ">
                <asp:Button ID="btnExcluir" runat="server" Text="Excluir" Width="127px" CssClass="btn btn-danger" Visible="false" />
            </div>
            <div class="col-lg-4 ">
                <asp:Button ID="btnReativar" runat="server" Text="Reativar" Width="127px" CssClass="btn btn-primary " Visible="false" />
            </div>
        </div>
    </div>
  

</asp:Content>

﻿Imports clssessoes
Imports System.IO
Imports clsSmart

Partial Class Associados
    Inherits System.Web.UI.Page

    Private Sub associados_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            habilitar(False)
        End If

    End Sub

    'grade de mensalidade 

    Private Sub grademensalidade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grademensalidade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grademensalidade.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")


        If e.CommandName = "excluir" Then
            Dim xMensalidade As New clsmensalidades

            xMensalidade.Nrseq = nrseq.Value


            If Not xMensalidade.excluir() Then
                sm("Swal.fire({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
                Exit Sub
            End If

            If xMensalidade.Ativo = "1" Then

                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Reativada com Sucesso',  showConfirmButton: false,  timer: 2000})")
                carregamensalidades()
            Else
                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Cancelado com Sucesso',  showConfirmButton: false,  timer: 2000})")

                carregamensalidades()
            End If

        End If

        If e.CommandName = "aberto" Then
            Dim xMensalidade As New clsmensalidades

            Dim valor As HiddenField = row.FindControl("hdvalor")
            xMensalidade.Nrseq = nrseq.Value
            xMensalidade.Valorpg = valor.Value

            If Not xMensalidade.aberto() Then
                sm("Swal.fire({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade em Aberto',  showConfirmButton: false,  timer: 2000})")

            carregamensalidades()

        End If

        If e.CommandName = "pagar" Then
            Dim xMensalidade As New clsmensalidades

            Dim valor As HiddenField = row.FindControl("hdvalor")
            xMensalidade.Nrseq = nrseq.Value
            xMensalidade.Valorpg = valor.Value

            If Not xMensalidade.pagar() Then
                sm("Swal.fire({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Mensalidade Paga',  showConfirmButton: false,  timer: 2000})")

            carregamensalidades()

        End If

        If e.CommandName = "estornar" Then
            Dim xMensalidade As New clsmensalidades

            xMensalidade.Nrseq = nrseq.Value

            If Not xMensalidade.estornar() Then
                sm("Swal.fire({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Estorno Realizado com Sucesso',  showConfirmButton: false,  timer: 2000})")

            carregamensalidades()

        End If
        If e.CommandName = "isento" Then
            Dim xMensalidade As New clsmensalidades

            xMensalidade.Nrseq = nrseq.Value


            If Not xMensalidade.isento() Then
                sm("Swal.fire({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Isenção Realizada com Sucesso',  showConfirmButton: false,  timer: 2000})")

            carregamensalidades()

            btndesfiliar.Visible = False
            btnreativar.Visible = False


        End If
    End Sub

    Public Sub carregamensalidades(Optional exibirinativos As Boolean = False)


        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbmensalidades inner join tbAssociados on tbmensalidades.nrseqcliente = tbAssociados.nrseq where  tbmensalidades.nrseqcliente =  " & hdnrseq.Value & "  order by tbmensalidades.ano, tbmensalidades.mes asc")

        grademensalidade.DataSource = tb1
        grademensalidade.DataBind()

    End Sub

    Private Sub grademensalidade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grademensalidade.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(4).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue
        Else
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If

        If e.Row.Cells(5).Text = "1" Then
            e.Row.Cells(5).Text = "Janeiro"
        ElseIf e.Row.Cells(5).Text = "2" Then
            e.Row.Cells(5).Text = "Fevereiro"
        ElseIf e.Row.Cells(5).Text = "3" Then
            e.Row.Cells(5).Text = "Março"
        ElseIf e.Row.Cells(5).Text = "4" Then
            e.Row.Cells(5).Text = "Abril"
        ElseIf e.Row.Cells(5).Text = "5" Then
            e.Row.Cells(5).Text = "Maio"
        ElseIf e.Row.Cells(5).Text = "6" Then
            e.Row.Cells(5).Text = "Junho"
        ElseIf e.Row.Cells(5).Text = "7" Then
            e.Row.Cells(5).Text = "Julho"
        ElseIf e.Row.Cells(5).Text = "8" Then
            e.Row.Cells(5).Text = "Agosto"
        ElseIf e.Row.Cells(5).Text = "9" Then
            e.Row.Cells(5).Text = "Setembro"
        ElseIf e.Row.Cells(5).Text = "10" Then
            e.Row.Cells(5).Text = "Outubro"
        ElseIf e.Row.Cells(5).Text = "11" Then
            e.Row.Cells(5).Text = "Novembro"
        ElseIf e.Row.Cells(5).Text = "12" Then
            e.Row.Cells(5).Text = "Dezembro"
        End If

        If e.Row.Cells(4).Text = "0" Then
            e.Row.Cells(4).Text = "Aberto"
        ElseIf e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Pago"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Estornado"
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.Cells(4).Text = "Isento"
        End If

        If e.Row.Cells(3).Text = "0" Then
            '  e.Row.Cells(7).Text = "Ativar"
            div.Visible = False
            texto.Visible = True
        Else
            div.Visible = True
            texto.Visible = False
        End If

        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If

    End Sub

    Private Sub btnbaixaselect_Click(sender As Object, e As EventArgs) Handles btnbaixaselect.Click
        For Each row As GridViewRow In grademensalidade.Rows

            Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
            Dim chkok As CheckBox = row.FindControl("chkok")

            If chkok.Checked = "true" Then
                Dim xMensalidade As New clsmensalidades

                Dim valor As HiddenField = row.FindControl("hdvalor")
                xMensalidade.Nrseq = nrseq.Value

                If txtdtbaixa.Text <> "" Then
                    xMensalidade.Dtpg = txtdtbaixa.Text
                End If

                If Not xMensalidade.pagar() Then
                    sm("swal({title: 'Error!',text: '" & xMensalidade.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                    Exit Sub
                End If

                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Efetuada a baixa das mensalidades',  showConfirmButton: false,  timer: 2000})")

                carregamensalidades()

            End If
        Next
    End Sub

    Private Sub limpar()
        '  txtnrseq.text = ""
        txtmatricula.text = ""
        txtnome.text = ""
        txtnomemae.text = ""
        txtsituacaopb.text = ""
        txtsexo.text = ""
        txtdtnasc.text = ""
        txtcpf.text = ""
        txtrg.text = ""
        txtcep.text = ""
        txtendereco.text = ""
        txtnumero.text = ""
        txtcomplemento.text = ""
        txtbairro.text = ""
        txtcidade.text = ""
        txtestado.text = ""
        txtcelular.Text = ""
        txttelefone.text = ""
        txtemail.text = ""
        txtdtposse.text = ""
        txtdtaposentadoria.text = ""
        txtsenha.text = ""
        txtAgencia.text = ""
        txtcontacorrente.Text = ""
        'txtautorizacao.text = ""
        'txtdtcad.text = ""
        'txtusercad.text = ""
        'txtAtivo_Old.text = ""
        'txtAtivo.text = ""
        'txtmatriculactrl.text = ""
        'txtcodigo.text = ""
        'txtuserexcluir.text = ""
        'txtdtexcluido.text = ""
        'txtdddcelular.text = ""
        'txtcelular.text = ""
        'txtbloqueado_Old.text = ""
        'txtbloqueado.text = ""
        'txtdtbloqueio.text = ""
        'txtuserbloqueio.text = ""
        'txtdtdesbloqueio.text = ""
        'txtuserdesbloqueio.text = ""
        'txtip.text = ""
        'txtdtsolexc.text = ""
        'txtmotiexc.text = ""
        'txtdtinad.text = ""
        'txtfiliacao.text = ""
        'txtvivomorto.text = ""
        'txtnegociado.text = ""
    End Sub


    Private Sub salvar()
        Try

            Dim xclasse As New clsAssociado

            xclasse.Nrseq = txtnrseq.Text
            xclasse.Matricula = txtmatricula.Text
            xclasse.Nome = txtnome.Text
            xclasse.Nomemae = txtnomemae.Text
            xclasse.Situacaopb = txtsituacaopb.Text
            xclasse.Sexo = txtsexo.Text
            If txtdtnasc.Text = "" Then
                'informar para preenchimento
            Else
                xclasse.Dtnasc = txtdtnasc.Text
            End If
            xclasse.Cpf = txtcpf.Text
            xclasse.Rg = txtrg.Text
            xclasse.Cep = txtcep.Text
            xclasse.Endereco = txtendereco.Text
            xclasse.Numero = txtnumero.Text
            xclasse.Complemento = txtcomplemento.Text
            xclasse.Bairro = txtbairro.Text
            xclasse.Cidade = txtcidade.Text
            xclasse.Estado = txtestado.Text
            xclasse.Celular = txtcelular.Text
            xclasse.Telefone = txttelefone.Text
            xclasse.Email = txtemail.Text
            If txtdtposse.Text = "" Then
                'informar para preenchimento
            Else
                xclasse.Dtposse = txtdtposse.Text
            End If
            If txtdtaposentadoria.Text = "" Then
                'informar para preenchimento
            Else
                xclasse.Dtaposentadoria = txtdtaposentadoria.Text
            End If
            xclasse.Senha = txtsenha.Text
            xclasse.Agencia = txtAgencia.Text
            xclasse.Contacorrente = txtcontacorrente.Text
            '  xclasse.autorizacao = txtautorizacao.text
            ' xclasse.dtcad = txtdtcad.text
            '    xclasse.usercad = txtusercad.text
            'xclasse.ativo_old = txtativo_old.text
            'xclasse.ativo = txtativo.text
            'xclasse.matriculactrl = txtmatriculactrl.text
            'xclasse.codigo = txtcodigo.text
            'xclasse.userexcluir = txtuserexcluir.text
            'xclasse.dtexcluido = txtdtexcluido.text
            'xclasse.dddcelular = txtdddcelular.text
            xclasse.Celular = txtcelular.Text
            'xclasse.bloqueado_old = txtbloqueado_old.text
            'xclasse.bloqueado = txtbloqueado.text
            'xclasse.dtbloqueio = txtdtbloqueio.text
            'xclasse.userbloqueio = txtuserbloqueio.text
            'xclasse.dtdesbloqueio = txtdtdesbloqueio.text
            'xclasse.userdesbloqueio = txtuserdesbloqueio.text
            'xclasse.ip = txtip.text
            'xclasse.dtsolexc = txtdtsolexc.text
            'xclasse.motiexc = txtmotiexc.text
            'xclasse.dtinad = txtdtinad.text
            'xclasse.filiacao = txtfiliacao.text
            'xclasse.vivomorto = txtvivomorto.text
            'xclasse.negociado = txtnegociado.text

            If Not xclasse.salvar Then
                sm("Swal.fire({   type: 'warning',  title: 'Não foi possivel Salvar',  confirmButtonText: 'OK'})")
                Exit Sub
            End If

            '   limpar()
            '   habilitar(False)

        Catch ex As Exception
            sm("Swal.fire({   type: 'warning',  title: 'Não foi possivel efetuar esta ação',  confirmButtonText: 'OK'})")
        End Try


    End Sub



    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub efetuarbusca()

        limpar()
        habilitar(True)

        Dim xAssociados As New clsAssociado

        xAssociados.Valorbusca = txtbusca.Text
        xAssociados.Busca = cbobusca.Text

        If Not xAssociados.procurapor() Then
            sm("Swal.fire({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
            Exit Sub
        End If
        Dim seraativo As String

        txtnrseq.Text = xAssociados.Nrseq
        hdnrseq.Value = xAssociados.Nrseq
        hdmatricula.Value = xAssociados.Matricula
        txtmatricula.Text = xAssociados.Matricula
        txtnome.Text = xAssociados.Nome
        nomedoassociado.Text = xAssociados.Nome
        txtnomemae.Text = xAssociados.Nomemae
        txtsituacaopb.Text = xAssociados.Situacaopb
        txtsexo.Text = xAssociados.Sexo
        txtdtnasc.Text = xAssociados.Dtnasc
        txtcpf.Text = xAssociados.Cpf
        txtrg.Text = xAssociados.Rg
        txtcep.Text = xAssociados.Cep
        txtendereco.Text = xAssociados.Endereco
        txtnumero.Text = xAssociados.Numero
        txtcomplemento.Text = xAssociados.Complemento
        txtbairro.Text = xAssociados.Bairro
        txtcidade.Text = xAssociados.Cidade
        txtestado.Text = xAssociados.Estado
        txtcelular.Text = xAssociados.Celular
        txttelefone.Text = xAssociados.Telefone
        txtemail.Text = xAssociados.Email
        txtdtposse.Text = xAssociados.Dtposse
        txtdtaposentadoria.Text = xAssociados.Dtaposentadoria
        txtsenha.Text = xAssociados.Senha
        txtAgencia.Text = xAssociados.Agencia
        txtcontacorrente.Text = xAssociados.Contacorrente
        seraativo = xAssociados.Ativo

        If seraativo = True Then
            txtativodesativo.Text = "ATIVO"
        ElseIf seraativo = False Then
            txtativodesativo.Text = "DESFILIADO"
        Else


        End If

        If xAssociados.Ativo = True Then
            btndesfiliar.Visible = True
            btnreativar.Visible = False
        Else
            btnreativar.Visible = True
            btndesfiliar.Visible = False
        End If

        txtdatainclusao.Text = xAssociados.Dtcad
        txtcadastradopor.Text = xAssociados.Usercad

        divdadosassociado.Visible = True
        divdocumentos.Visible = False
        divjoias.Visible = False
        divjoias2.Visible = False
        divmensalidade.Visible = False
        divacoes.Visible = False


    End Sub
    Private Sub habilitar(valor As Boolean)
        'txtnrseq.enabled = valor
        txtmatricula.Enabled = valor
        txtnome.Enabled = valor
        txtnomemae.Enabled = valor
        txtsituacaopb.Enabled = valor
        txtsexo.Enabled = valor
        txtdtnasc.Enabled = valor
        txtcpf.Enabled = valor
        txtrg.Enabled = valor
        txtcep.Enabled = valor
        txtendereco.Enabled = valor
        txtnumero.Enabled = valor
        txtcomplemento.Enabled = valor
        txtbairro.Enabled = valor
        txtcidade.Enabled = valor
        txtestado.Enabled = valor
        txtcelular.Enabled = valor
        txttelefone.Enabled = valor
        txtemail.Enabled = valor
        txtdtposse.Enabled = valor
        txtdtaposentadoria.Enabled = valor
        txtsenha.Enabled = valor
        txtAgencia.Enabled = valor
        txtcontacorrente.Enabled = valor
        txtmatricula2.Enabled = valor
        'txtautorizacao.enabled = valor
        'txtdtcad.enabled = valor
        'txtusercad.enabled = valor
        'txtativo_old.enabled = valor
        'txtativo.enabled = valor
        'txtmatriculactrl.enabled = valor
        'txtcodigo.enabled = valor
        'txtuserexcluir.enabled = valor
        'txtdtexcluido.enabled = valor
        'txtdddcelular.enabled = valor
        'txtcelular.enabled = valor
        'txtbloqueado_old.enabled = valor
        'txtbloqueado.enabled = valor
        'txtdtbloqueio.enabled = valor
        'txtuserbloqueio.enabled = valor
        'txtdtdesbloqueio.enabled = valor
        'txtuserdesbloqueio.enabled = valor
        'txtip.enabled = valor
        'txtdtsolexc.enabled = valor
        'txtmotiexc.enabled = valor
        'txtdtinad.enabled = valor
        'txtfiliacao.enabled = valor
        'txtvivomorto.enabled = valor
        'txtnegociado.enabled = valor
    End Sub

    Public Sub carregaassociados(Optional exibirinativos As Boolean = False)
        'Dim xempresas As New clsEmpresas
        Dim xAssociados As New clsAssociado
        Dim consulta As String

        consulta = ""

        If txtbusca.Text = "" Then
            consulta = ""
            sm("Swal.fire({title: 'Error!',text: 'Essa Busca pode demorar',type: 'error',confirmButtonText: 'OK'})")
            Exit Sub
        Else
            consulta = "where " & cbobusca.SelectedValue & " like '%" & txtbusca.Text & "%'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbAssociados " & consulta & "")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        gradeassociados.DataSource = tb1
        gradeassociados.DataBind()

    End Sub

    Private Sub gradeassociados_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeassociados.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeassociados.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "editar" Then
            '    carregarNoFormulario(nrseq.Value)
            habilitar(True)
            '  txtidentificador.Enabled = False

            Dim xAssociados As New clsAssociado

            xAssociados.Nrseq = nrseq.Value

            If Not xAssociados.procurar() Then
                sm("Swal.fire({title: 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
                Exit Sub
            End If

            Dim seraativo As String

            txtnrseq.Text = xAssociados.Nrseq
            hdnrseq.Value = xAssociados.Nrseq
            hdmatricula.Value = xAssociados.Matricula
            txtmatricula.Text = xAssociados.Matricula
            txtnome.Text = xAssociados.Nome
            nomedoassociado.Text = xAssociados.Nome
            txtnomemae.Text = xAssociados.Nomemae
            txtsituacaopb.Text = xAssociados.Situacaopb
            txtsexo.Text = xAssociados.Sexo
            txtdtnasc.Text = xAssociados.Dtnasc
            txtcpf.Text = xAssociados.Cpf
            txtrg.Text = xAssociados.Rg
            txtcep.Text = xAssociados.Cep
            txtendereco.Text = xAssociados.Endereco
            txtnumero.Text = xAssociados.Numero
            txtcomplemento.Text = xAssociados.Complemento
            txtbairro.Text = xAssociados.Bairro
            txtcidade.Text = xAssociados.Cidade
            txtestado.Text = xAssociados.Estado
            txtcelular.Text = xAssociados.Celular
            txttelefone.Text = xAssociados.Telefone
            txtemail.Text = xAssociados.Email
            txtdtposse.Text = xAssociados.Dtposse
            txtdtaposentadoria.Text = xAssociados.Dtaposentadoria
            txtsenha.Text = xAssociados.Senha
            txtAgencia.Text = xAssociados.Agencia
            txtcontacorrente.Text = xAssociados.Contacorrente
            seraativo = xAssociados.Ativo

            If seraativo = True Then
                txtativodesativo.Text = "ATIVO"
            ElseIf seraativo = False Then
                txtativodesativo.Text = "DESFILIADO"
            Else


            End If

            If xAssociados.Ativo = True Then
                btndesfiliar.Visible = True
                btnreativar.Visible = False
            Else
                btnreativar.Visible = True
                btndesfiliar.Visible = False
            End If

            txtdatainclusao.Text = xAssociados.Dtcad
            txtcadastradopor.Text = xAssociados.Usercad

            divdadosassociado.Visible = True
            divdocumentos.Visible = False
            divjoias.Visible = False
            divjoias2.Visible = False
            divmensalidade.Visible = False
            divacoes.Visible = False

            divgradeassociado.Visible = False


        End If

        If e.CommandName = "deletar" Then
            Dim xplanos As New clsPlanos

            xplanos.Nrseq = nrseq.Value

            If Not xplanos.excluir() Then
                sm("Swal.fire({title: 'Error!',text: '" & xplanos.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})")
                Exit Sub
            End If

            carregaassociados()
        End If
    End Sub

    Private Sub gradeassociados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeassociados.RowDataBound


    End Sub

    Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click
        carregaassociados()
        divgradeassociado.Visible = True
        divdadosassociado.Visible = False
        divdocumentos.Visible = False
        divjoias.Visible = False
        divjoias2.Visible = False
        divmensalidade.Visible = False
        divacoes.Visible = False
        nomedoassociado.Text = ""
        txtativodesativo.Text = "Aguardando..."
        hdnrseq.Value = ""
        hdmatricula.Value = ""
        txtnrseq.Text = "0"


    End Sub

    Public Function validarconta(ByVal recebe As String) As Boolean

        If recebe <> "" Then
            Dim qtd As String = Len(recebe)
            Dim digitoinformado As String = Mid(recebe, qtd, 1)
            qtd = Len(recebe) - 1
            Dim valor As Integer
            Dim numero As Integer = 9
            Dim digitofinal As Integer

            recebe = Mid(recebe, 1, Len(qtd - 1))
            For x As Integer = 0 To qtd - 1
                valor = Mid(recebe, qtd, 1) * numero
                digitofinal = digitofinal + valor


                If numero = 0 Then
                    numero = 9
                End If
                numero -= 1
                qtd = qtd - 1

                If qtd = 0 Then
                    qtd += 1
                End If
            Next
            digitofinal = digitofinal Mod 11


            If digitofinal <> 10 Then

                If IsNumeric(digitoinformado) = False Then
                    Return False
                    Exit Function
                End If

                If CType(digitoinformado, Integer) = digitofinal Then
                    Return True
                Else
                    Return False
                End If

            Else

                If CType(digitoinformado, String) = "X" Or CType(digitoinformado, String) = "x" Then
                    Return True
                Else
                    Return False
                End If
            End If



        Else
            Return False
        End If
    End Function

    ' carrega joias e grade joias


    Private Sub gradejoias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradejoias.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(1).Text <> "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue
        End If

        If e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Janeiro"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Fevereiro"
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.Cells(4).Text = "Março"
        ElseIf e.Row.Cells(4).Text = "4" Then
            e.Row.Cells(4).Text = "Abril"
        ElseIf e.Row.Cells(4).Text = "5" Then
            e.Row.Cells(4).Text = "Maio"
        ElseIf e.Row.Cells(4).Text = "6" Then
            e.Row.Cells(4).Text = "Junho"
        ElseIf e.Row.Cells(4).Text = "7" Then
            e.Row.Cells(4).Text = "Julho"
        ElseIf e.Row.Cells(4).Text = "8" Then
            e.Row.Cells(4).Text = "Agosto"
        ElseIf e.Row.Cells(4).Text = "9" Then
            e.Row.Cells(4).Text = "Setembro"
        ElseIf e.Row.Cells(4).Text = "10" Then
            e.Row.Cells(4).Text = "Outubro"
        ElseIf e.Row.Cells(4).Text = "11" Then
            e.Row.Cells(4).Text = "Novembro"
        ElseIf e.Row.Cells(4).Text = "12" Then
            e.Row.Cells(4).Text = "Dezembro"
        End If

        If e.Row.Cells(3).Text = "0" Then
            e.Row.Cells(3).Text = "Aberto"
        ElseIf e.Row.Cells(3).Text = "1" Then
            e.Row.Cells(3).Text = "Pago"
        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.Cells(3).Text = "Estornado"
        Else
            e.Row.Cells(3).Text = "Aberto"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If

        If e.Row.Cells(2).Text = "0" Then
            e.Row.Cells(2).Text = "Excluido"
            e.Row.Cells(4).Text = "Excluido"
        ElseIf e.Row.Cells(2).Text = "3" Then
            e.Row.Cells(2).Text = "Ativo"
        End If

        If e.Row.Cells(2).Text = "Excluido" Then
            '  e.Row.Cells(7).Text = "Ativar"
            div.Visible = False
            texto.Visible = True
        Else
            div.Visible = True
            texto.Visible = False
        End If


        If e.Row.Cells(3).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray
        End If



    End Sub


    Public Sub carregajoias(Optional exibirinativos As Boolean = False)



        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.nrsequser = " & hdnrseq.Value & "")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        gradejoias.DataSource = tb1
        gradejoias.DataBind()

    End Sub

    Private Sub gradejoias_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradejoias.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradejoias.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim nrsequser As HiddenField = row.FindControl("hdnrsequser")

        If e.CommandName = "excluir" Then

            Dim xJoias As New clsmJoias

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value


            If Not xJoias.excluir() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel excluir a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias()

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")

        End If

        If e.CommandName = "estornar" Then

            Dim xJoias As New clsmJoias

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value


            If Not xJoias.estorno() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias()


            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")
        End If

        If e.CommandName = "aberto" Then

            Dim xJoias As New clsmJoias

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value

            If Not xJoias.aberto() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If


            carregajoias()

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")

        End If

        If e.CommandName = "pagar" Then

            Dim xJoias As New clsmJoias

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value

            If Not xJoias.baixar() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel dar baixa a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias()

        End If

    End Sub



    ' carrega joias 2


    Private Sub gradejoias2_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradejoias2.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divbuttons")
        Dim texto As HtmlGenericControl = e.Row.FindControl("divtexto")


        If e.Row.Cells(4).Text = "1" Then
            e.Row.Cells(4).Text = "Janeiro"
        ElseIf e.Row.Cells(4).Text = "2" Then
            e.Row.Cells(4).Text = "Fevereiro"
        ElseIf e.Row.Cells(4).Text = "3" Then
            e.Row.Cells(4).Text = "Março"
        ElseIf e.Row.Cells(4).Text = "4" Then
            e.Row.Cells(4).Text = "Abril"
        ElseIf e.Row.Cells(4).Text = "5" Then
            e.Row.Cells(4).Text = "Maio"
        ElseIf e.Row.Cells(4).Text = "6" Then
            e.Row.Cells(4).Text = "Junho"
        ElseIf e.Row.Cells(4).Text = "7" Then
            e.Row.Cells(4).Text = "Julho"
        ElseIf e.Row.Cells(4).Text = "8" Then
            e.Row.Cells(4).Text = "Agosto"
        ElseIf e.Row.Cells(4).Text = "9" Then
            e.Row.Cells(4).Text = "Setembro"
        ElseIf e.Row.Cells(4).Text = "10" Then
            e.Row.Cells(4).Text = "Outubro"
        ElseIf e.Row.Cells(4).Text = "11" Then
            e.Row.Cells(4).Text = "Novembro"
        ElseIf e.Row.Cells(4).Text = "12" Then
            e.Row.Cells(4).Text = "Dezembro"
        End If

        If e.Row.Cells(3).Text = "0" Then
            e.Row.Cells(3).Text = "Aberto"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        ElseIf e.Row.Cells(3).Text = "1" Then
            e.Row.Cells(3).Text = "Pago"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue
        ElseIf e.Row.Cells(3).Text = "2" Then
            e.Row.Cells(3).Text = "Estornado"
        Else
            e.Row.Cells(3).Text = "Aberto"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.White
        End If

        If e.Row.Cells(2).Text = "0" Then
            e.Row.Cells(2).Text = "Excluido"
            e.Row.Cells(4).Text = "Excluido"
        ElseIf e.Row.Cells(2).Text = "3" Then
            e.Row.Cells(2).Text = "Ativo"
        End If

        If e.Row.Cells(2).Text = "Excluido" Then
            '  e.Row.Cells(7).Text = "Ativar"
            div.Visible = False
            texto.Visible = True
        Else
            div.Visible = True
            texto.Visible = False
        End If





    End Sub


    Public Sub carregajoias2(Optional exibirinativos As Boolean = False)



        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbjoias_2 inner join tbAssociados on tbjoias_2.nrsequser = tbAssociados.nrseq where tbjoias_2.nrsequser = " & hdnrseq.Value & " order by  tbjoias_2.ano, tbjoias_2.mes asc")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        gradejoias2.DataSource = tb1
        gradejoias2.DataBind()

    End Sub

    Private Sub gradejoias2_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradejoias2.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradejoias2.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim nrsequser As HiddenField = row.FindControl("hdnrsequser")

        If e.CommandName = "excluir" Then

            Dim xJoias As New clsjoia2

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value


            If Not xJoias.excluir() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel excluir a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias2()

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")

        End If

        If e.CommandName = "estornar" Then

            Dim xJoias As New clsjoia2

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value


            If Not xJoias.estorno() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias2()


            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")
        End If

        If e.CommandName = "aberto" Then

            Dim xJoias As New clsjoia2

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value

            If Not xJoias.aberto() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If


            carregajoias2()

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Status joia atualizado',  showConfirmButton: false,  timer: 2000})")

        End If

        If e.CommandName = "pagar" Then

            Dim xJoias As New clsjoia2

            xJoias.Nrsequser = nrsequser.Value
            xJoias.Nrseq = nrseq.Value

            If Not xJoias.baixar() Then

                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel dar baixa a joia',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregajoias2()

        End If

    End Sub

    ' joias 2 fim


    Public Sub carregaacoes(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbacoes_detalhes inner join tbacoes on tbacoes_detalhes.nrseqacoes = tbacoes.nrseq where tbacoes_detalhes.ativo = 1 and tbacoes_detalhes.matricula = '" & hdmatricula.Value & "' and tbacoes.ativo = '1' order by tbacoes.nrseq desc")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        gradeaceos.DataSource = tb1
        gradeaceos.DataBind()

    End Sub



    Public Sub carregadocumentos(Optional exibirinativos As Boolean = False)
        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbdocumentosassociados where matricula = '" & hdmatricula.Value & "' and ativo = true")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        gradedocumentos.DataSource = tb1
        gradedocumentos.DataBind()

    End Sub

    Private Sub btnupload_Click(sender As Object, e As EventArgs) Handles btnupload.Click

        Dim sImageName As String = ""
        Dim descriarquivo As String = ""
        Dim codigo As String = ""
        Dim sImagePathImages As String = Server.MapPath("~\documentosassociados")

        Dim xDoc As New clsdocassociados

        If Not xDoc.novo Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel carregar os dados',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        codigo = xDoc.Nrseq

        If (upload01.HasFile) Then

            descriarquivo = "\MAT_" & hdmatricula.Value & txtnome.Text & codigo & Path.GetExtension(upload01.FileName)
            sImageName = sImagePathImages & descriarquivo

            ' Dim sFileExt As String = Path.GetExtension(upload01.FileName)
            ' m_sImageNameUserUpload = sImageName + sFileExt
            ' m_sImageNameGenerated = Path.Combine(sImagePathImages, m_sImageNameUserUpload)
            upload01.PostedFile.SaveAs(sImageName)
        Else


            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Carregar um arquivo valido',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        ' Início do salvar\

        xDoc.Nrseq = codigo
        xDoc.Matricula = hdmatricula.Value
        xDoc.Nome = txtdescridocassoc.Text
        xDoc.Arquivo = descriarquivo

        If Not xDoc.novosalvar Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel gravar o arquivo',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If


        sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Arquivo salva com sucesso',  showConfirmButton: false,  timer: 2000})")
        limpar()
        carregadocumentos()

    End Sub

    Private Sub gradedocumentos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradedocumentos.RowCommand

        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradedocumentos.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim arquivo As HiddenField = row.FindControl("hdarquivo")

        If e.CommandName = "baixar" Then

            Dim wcarquivo As String = arquivo.Value
            Response.ContentType = "application/octet-stream"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment; filename=" & wcarquivo)
            Response.TransmitFile(Server.MapPath("~\documentosassociados\") & wcarquivo)
            Response.End()

            sm("Swal.fire({title: 'Atenção!',text: 'Não foi possivel Baixar o documento',type: 'error',confirmButtonText: 'OK'})")

        End If

        If e.CommandName = "excluir" Then

            Dim xDocassociados As New clsdocassociados

            xDocassociados.Nrseq = nrseq.Value

            If Not xDocassociados.excluir() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel excluir',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregadocumentos()

        End If

    End Sub

    Private Sub gradeaceos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeaceos.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub

        Dim ativ As HtmlGenericControl = e.Row.FindControl("divretirar")
        Dim dsativ As HtmlGenericControl = e.Row.FindControl("divativar")

        If e.Row.Cells(2).Text = "1" Then

            e.Row.Cells(2).Text = "Vinculado"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue
            dsativ.Visible = True
            ativ.Visible = False
        ElseIf e.Row.Cells(2).Text = "0" Then
            e.Row.Cells(2).Text = "Desviculado"
            e.Row.ForeColor = System.Drawing.Color.White
            e.Row.BackColor = System.Drawing.Color.Gray
            ativ.Visible = False
            dsativ.Visible = True
        End If

        'If e.Row.Cells(5).Text = "0" Then
        '    e.Row.Cells(4).Text = "Excluido"
        'End If

    End Sub


    Private Sub gradedocumentos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradedocumentos.RowDataBound

        If e.Row.RowIndex < 0 Then Exit Sub
        Dim div As HtmlGenericControl = e.Row.FindControl("divmostrabtn")

        If e.Row.Cells(5).Text = "1" Then
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(5).Text = "0" Then
            e.Row.ForeColor = System.Drawing.Color.White
            e.Row.BackColor = System.Drawing.Color.Gray
            div.Visible = False
        End If

        If e.Row.Cells(5).Text = "0" Then
            e.Row.Cells(4).Text = "Excluido"
        End If

    End Sub




    Private Sub btndadosassociados_Click(sender As Object, e As EventArgs) Handles btndadosassociados.Click

        If hdnrseq.Value = "" Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Selecione um associado',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        divmensalidade.Visible = False
        divdadosassociado.Visible = True

        divdocumentos.Visible = False
        divjoias.Visible = False
        divjoias2.Visible = False
        divacoes.Visible = False
        carregamensalidades()

    End Sub

    Private Sub btnmensalidade_Click(sender As Object, e As EventArgs) Handles btnmensalidade.Click


        If hdnrseq.Value = "" Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Selecione um associado',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        divmensalidade.Visible = True
        divdadosassociado.Visible = False

        divdocumentos.Visible = False
        divjoias.Visible = False
        divjoias2.Visible = False
        divacoes.Visible = False
        carregamensalidades()
    End Sub


    Private Sub btnjoias_Click(sender As Object, e As EventArgs) Handles btnjoias.Click

        If hdnrseq.Value = "" Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Selecione um associado',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        divmensalidade.Visible = False
        divdadosassociado.Visible = False

        divdocumentos.Visible = False
        divjoias.Visible = True
        divjoias2.Visible = False
        divacoes.Visible = False
        carregajoias()
    End Sub


    Private Sub btnjoias2_Click(sender As Object, e As EventArgs) Handles btnjoias2.Click

        If hdnrseq.Value = "" Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Selecione um associado',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        divmensalidade.Visible = False
        divdadosassociado.Visible = False
        divdocumentos.Visible = False
        divjoias.Visible = False
        divjoias2.Visible = True
        divacoes.Visible = False
        carregajoias2()
    End Sub


    Private Sub btndocumentos_Click(sender As Object, e As EventArgs) Handles btndocumentos.Click
        If hdnrseq.Value = "" Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Selecione um associado',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        divmensalidade.Visible = False
        divdadosassociado.Visible = False
        divdocumentos.Visible = True
        divjoias.Visible = False
        divjoias2.Visible = False
        divacoes.Visible = False
        carregadocumentos()
    End Sub

    Private Sub btnacoes_Click(sender As Object, e As EventArgs) Handles btnacoes.Click
        If hdnrseq.Value = "" Then
            sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Selecione um associado',  showConfirmButton: false,  timer: 2000})")
            Exit Sub
        End If

        divmensalidade.Visible = False
        divdadosassociado.Visible = False
        divdocumentos.Visible = False
        divjoias.Visible = False
        divjoias2.Visible = False
        divacoes.Visible = True
        carregaacoes()
    End Sub

    Private Sub gradeaceos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeaceos.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeaceos.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdn")
        Dim matricula As HiddenField = row.FindControl("hdmatricula")

        If e.CommandName = "ativar" Then


            Dim xAcoes As New clsacoes
            Dim xAcoesDetalhes As New clsacoesdth

            xAcoesDetalhes.Matricula = matricula.Value
            xAcoesDetalhes.Nrseq = nrseq.Value

            If Not xAcoesDetalhes.ativar() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: '" & xAcoesDetalhes.Mensagemerro & "',  showConfirmButton: false,  timer: 3000})")
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: ' Matricula " & matricula.Value & " Recupedara com sucesso',  showConfirmButton: false,  timer: 3000})")

        End If

        If e.CommandName = "retirar" Then

            Dim xAcoes As New clsacoes
            Dim xAcoesDetalhes As New clsacoesdth

            xAcoesDetalhes.Matricula = matricula.Value
            xAcoesDetalhes.Nrseq = nrseq.Value

            If Not xAcoesDetalhes.remover() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: '" & xAcoesDetalhes.Mensagemerro & "',  showConfirmButton: false,  timer: 2000})")

                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: ' Matricula " & matricula.Value & " retirada com sucesso',  showConfirmButton: false,  timer: 2000})")

        End If
    End Sub

    Private Sub btnatualizar_Click(sender As Object, e As EventArgs) Handles btnatualizar.Click
        salvar()
    End Sub

    Private Sub btngradejoiasselect_Click(sender As Object, e As EventArgs) Handles btngradejoiasselect.Click
        For Each row As GridViewRow In gradejoias.Rows

            Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
            Dim chkok As CheckBox = row.FindControl("chkok")

            If chkok.Checked = "true" Then
                Dim xMensalidade As New clsmJoias

                Dim xJoias As New clsmJoias

                xJoias.Nrseq = nrseq.Value

                If Not xJoias.baixar() Then
                    sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia',  showConfirmButton: false,  timer: 2000})")
                    Exit Sub
                End If
                carregajoias()

                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Efetuada a baixa das joias',  showConfirmButton: false,  timer: 2000})")


            End If
        Next
    End Sub

    Private Sub btndesfiliar_Click(sender As Object, e As EventArgs) Handles btndesfiliar.Click

        Dim xclasse As New clsAssociado
        Dim xmensalidade As New clsmensalidades


        xclasse.Nrseq = hdnrseq.Value
        xmensalidade.Nrseqcliente = hdnrseq.Value


        If Not xclasse.desfiliar Then
            sm("Swal.fire({   type: 'warning',  title: 'erro durante a execução',  confirmButtonText: 'OK'})")
            Exit Sub
        End If
        If Not xmensalidade.desfiliar Then
            sm("Swal.fire({   type: 'warning',  title: 'erro durante a execução',  confirmButtonText: 'OK'})")
            Exit Sub
        End If


        efetuarbusca()
        sm("Swal.fire({   type: 'warning',  title: 'Associado desfiliado',  confirmButtonText: 'OK'})")

    End Sub

    Private Sub btnreativar_Click(sender As Object, e As EventArgs) Handles btnreativar.Click
        Dim xclasse As New clsAssociado
        Dim xmensalidade As New clsmensalidades


        xclasse.Nrseq = hdnrseq.Value

        xmensalidade.Nrseqcliente = hdnrseq.Value
        If Not xclasse.reativar Then
            sm("Swal.fire({   type: 'warning',  title: 'erro durante a execução',  confirmButtonText: 'OK'})")
            Exit Sub
        End If

        If Not xmensalidade.filiar Then
            sm("Swal.fire({   type: 'warning',  title: 'erro durante a execução',  confirmButtonText: 'OK'})")
            Exit Sub
        End If
        efetuarbusca()
        sm("Swal.fire({   type: 'warning',  title: 'Associado reativado',  confirmButtonText: 'OK'})")

    End Sub

    Private Sub btndarbaixaj2_Click(sender As Object, e As EventArgs) Handles btndarbaixaj2.Click
        For Each row As GridViewRow In gradejoias2.Rows

            Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
            Dim chkok As CheckBox = row.FindControl("chkok")

            If chkok.Checked = "true" Then
                '  Dim xMensalidade As New clsjoia2

                Dim xJoias As New clsjoia2

                xJoias.Nrseq = nrseq.Value

                If Not xJoias.baixar() Then
                    sm("Swal.fire({  position: 'top-end',  type: 'error',  title: 'Não foi possivel atualizar a joia 2',  showConfirmButton: false,  timer: 2000})")
                    Exit Sub
                End If
                carregajoias2()

                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Efetuada a baixa das joias 2',  showConfirmButton: false,  timer: 2000})")


            End If
        Next
    End Sub

    Private Sub btnpdfmensaldiade_Click(sender As Object, e As EventArgs) Handles btnpdfmensaldiade.Click
        Dim link As String = ""
        link = "irlink('" & Request.Url.OriginalString.ToString.Replace(Request.Url.PathAndQuery, "") & "/relatorios/relatoriomensalidades.aspx?nrseqcliente=" & hdnrseq.Value & "');"

        sm(link, link)
    End Sub
End Class

﻿Imports clssessoes
Imports clsSmart
Imports System.Web.UI.DataVisualization.Charting


Partial Class acoeshomepage
    Inherits System.Web.UI.Page

    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco



    Private Sub acoeshomepage_Load(sender As Object, e As EventArgs) Handles Me.Load
        DirectCast(DirectCast(Page.Master, MasterPage).FindControl("panelprincipal"), Panel).DefaultButton = ""
        If IsPostBack Then
            Exit Sub
        End If
        btncarregar.Visible = True
    End Sub


    Private Sub chkprocuracao_CheckedChanged(sender As Object, e As EventArgs) Handles chkprocuracao.CheckedChanged
        If chkprocuracao.Checked = True Then
            txtchkprocuracao.Enabled = True
            FileUpload1.Enabled = True
        Else
            txtchkprocuracao.Enabled = False
            FileUpload1.Enabled = False
        End If
    End Sub

    Private Sub chktermo_CheckedChanged(sender As Object, e As EventArgs) Handles chktermo.CheckedChanged
        If chktermo.Checked = True Then
            txtchtermo.Enabled = True
            FileUpload2.Enabled = True
        Else
            txtchtermo.Enabled = False
            FileUpload2.Enabled = False
        End If
    End Sub


    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub



    Private Sub carregagradeacoes()

        tb1 = tab1.conectar("select * from tbacoesjudiciais  order by nrseq desc")
        gradeacaojudicial.DataSource = tb1
        gradeacaojudicial.DataBind()


    End Sub

    Private Sub btncarregar_Click(sender As Object, e As EventArgs) Handles btncarregar.Click
        btncarregar.Visible = False
        gradeacaojudicial.Visible = True
        carregagradeacoes()
    End Sub

    Private Sub btninvisivel_Click(sender As Object, e As EventArgs) Handles btninvisivel.Click
        btncarregar.Visible = True
        gradeacaojudicial.Visible = False
    End Sub

    Private Sub gradeacaojudicial_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeacaojudicial.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeacaojudicial.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")

        If e.CommandName = "ativar" Then
            Dim xAcoes As New clsacoesjudiciais

            xAcoes.Nrseq = nrseq.Value

            If Not xAcoes.ativar() Then
                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Ativado com sucesso',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregagradeacoes()
        End If

        If e.CommandName = "cancelar" Then
            Dim xAcoes As New clsacoesjudiciais

            xAcoes.Nrseq = nrseq.Value

            If Not xAcoes.excluir() Then
                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Cancelado com sucesso',  showConfirmButton: false,  timer: 2000})")
                Exit Sub
            End If

            carregagradeacoes()
        End If

    End Sub

    Private Sub gradeacaojudicial_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeacaojudicial.RowDataBound


        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        Dim desativar As HtmlGenericControl = e.Row.FindControl("divdesativar")
        Dim ativar As HtmlGenericControl = e.Row.FindControl("divativar")


        If e.Row.Cells(3).Text = "1" Then
            e.Row.Cells(3).Text = "VISÍVEL"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(3).Text = "0" Then
            e.Row.Cells(3).Text = "CANCELADO"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray

        End If




        If e.Row.Cells(3).Text = "1" Or e.Row.Cells(3).Text = "VISÍVEL" Then
            ativar.Visible = False
            desativar.Visible = True

        ElseIf e.Row.Cells(3).Text = "CANCELADO" Or e.Row.Cells(3).Text = "0" Then
            ativar.Visible = True
            desativar.Visible = False
        End If

    End Sub
End Class

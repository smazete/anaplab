﻿Imports clssessoes
Imports clsSmart
Imports System.Web.UI.DataVisualization.Charting

Partial Class arrecadacaojoias
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    Private Sub arrecadacaojoias_Load(sender As Object, e As EventArgs) Handles Me.Load
        'cbomes.SelectedValue = Date.Now.Month
        'txtbuscajoias.Text = DateTime.Now.ToString("yyyy")


        carregaarrecadaçãojoias()
        carregaextratoconsolidado()
    End Sub

    Public Sub carregaextratoconsolidado(Optional exibirinativos As Boolean = False)
        Dim joiasmes As String = cbomes.SelectedValue
        Dim joiasano As String = txtbuscajoias.Text

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.statuspg =  1 and tbAssociados.ativo = 1 ")

        lblpagamentosextrato.Text = tb1.Rows.Count.ToString()

        lbltotalgeral.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 100)


        lblanaplab.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 5)
        lblstamato.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 65)
        lblnelson.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 30)
        'lblanaplab.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * (5 / 100))
        'lblstamato.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * (65 / 10))
        'lblnelson.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * (30 / 100))


        lblcomissao.Text = FormatCurrency(CDbl(lblanaplab.Text) + CDbl(lblstamato.Text) + CDbl(lblnelson.Text))
        lblgeralarrecadado.Text = FormatCurrency(numeros(lblpagamentosextrato.Text) * 100)

    End Sub

    Public Sub carregaarrecadaçãojoias(Optional exibirinativos As Boolean = False)
        Dim joiasmes As String = cbomes.SelectedValue
        Dim joiasano As String = txtbuscajoias.Text

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco

        'tb1 = tab1.conectar("select * from tbmensalidade_joias where ativo = '1'")

        'lblparticipantesgerais.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.ano =  '" & joiasano & "' and tbmensalidade_joias.mes = '" & joiasmes & "'  and tbAssociados.ativo = 1 ")

        lblparticipantes.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.ano =  '" & joiasano & "' and tbmensalidade_joias.mes = '" & joiasmes & "'  and tbmensalidade_joias.statuspg = 1 and tbAssociados.ativo = 1 ")

        lblarrecadajoias.Text = tb1.Rows.Count.ToString()


        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.ano =  '" & joiasano & "' and tbmensalidade_joias.mes = '" & joiasmes & "'  and tbmensalidade_joias.statuspg = 3 and tbAssociados.ativo = 1 ")

        lblarrecadaisentos.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.ano =  '" & joiasano & "' and tbmensalidade_joias.mes = '" & joiasmes & "'  and tbmensalidade_joias.statuspg = 0 and tbAssociados.ativo = 1 ")

        lblinadimplentes.Text = tb1.Rows.Count.ToString()

        tb1 = tab1.conectar("select * from tbmensalidade_joias inner join tbAssociados on tbmensalidade_joias.nrsequser = tbAssociados.nrseq where tbmensalidade_joias.ano =  '" & joiasano & "' and tbmensalidade_joias.mes = '" & joiasmes & "'  and tbmensalidade_joias.statuspg = 1 and tbAssociados.ativo = 0 ")

        lbldesfiliadosadiplentes.Text = tb1.Rows.Count.ToString()


        lblvalorfinal.Text = FormatCurrency(numeros(lblarrecadajoias.Text) * 100)

        'lblvalorfinal.Text = lblarrecadajoias.Text * 100


    End Sub


    Private Sub btnbuscandojoias_Click(sender As Object, e As EventArgs) Handles btnbuscandojoias.Click


        carregaarrecadaçãojoias()
    End Sub


End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="medicoespecialidades.aspx.vb" MasterPageFile="~/MasterPage.master" Inherits="medicoespecialidades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updategeral" runat="server">
        <ContentTemplate>
            <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
                <div class="box box-header">
                    <b>Especialidades do Profissional</b>
                </div>
                <div class="box-body">

                    <div class="box box-primary ">
                        <div class="box box-header "><b>Selecione o profissional desejado</b></div>
                        <div class="box-body ">
                            <asp:HiddenField runat="server" ID="hdnnrseq" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            Plano
                                    <asp:DropDownList CssClass="form-control dropdown" runat="server" ID="cboplano" AutoPostBack="true">
                                        <asp:ListItem Text="Selecione"></asp:ListItem>
                                    </asp:DropDownList>
                                        </div>
                                        <div class="col-lg-3">
                                            Profissional
                                    <asp:DropDownList CssClass="form-control dropdown" runat="server" ID="cbomedicos" AutoPostBack="true">
                                        <asp:ListItem Text="Selecione"></asp:ListItem>
                                    </asp:DropDownList>
                                        </div>

                                        <div class="col-lg-3">
                                            Especialidade
                                    <asp:DropDownList CssClass="form-control dropdown" runat="server" ID="cboespecialidades">
                                        <asp:ListItem Text="Selecione"></asp:ListItem>
                                    </asp:DropDownList>
                                        </div>

                                        <div class="col-lg-1 text-center">
                                            <asp:LinkButton ID="btnnovo" runat="server" CssClass="btn btn-warning fix-btn"> <i class="fas fa-plus-circle"></i> Abrir</asp:LinkButton>
                                        </div>
                                    </div>
                                </ContentTemplate>

                            </asp:UpdatePanel>
                            <br />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-2">
                            Valor Consulta
                            <asp:TextBox Enabled="false" ID="txtvlconsulta" runat="server" CssClass="form-control" onkeyup="formataValor(this,event);"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            Percentual Repasse
                            <asp:TextBox Enabled="false" ID="txtpercrepasse" runat="server" CssClass="form-control" onkeyup="formataValor(this,event);" AutoPostBack="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            Valor Repasse
                            <asp:TextBox Enabled="false" ID="txtvlrepasse" runat="server" CssClass="form-control" onkeyup="formataValor(this,event);"></asp:TextBox>
                        </div>

                        <div class="col-lg-4" style="display: none">
                            Enviadosite
                            <asp:TextBox Enabled="false" ID="txtenviadosite" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                    </div>
                    <div class="row text-align-center">
                        <div class="col-lg-12">
                            <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="updategeral">
                                <ProgressTemplate>
                                    <img src="../img/loadred.gif" style="height: 3rem; width: 3rem;" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>
                    <br />
                    
                    <div class="row">
                    <div class="col-lg-4"></div>
                        <div class="col-lg-2">
                            <asp:LinkButton Enabled="false" ID="btnsalvar" runat="server" CssClass="btn btn-success"> <i class="fas fa-floppy-o"></i> Salvar</asp:LinkButton>
                        </div>
                        <div class="col-lg-2">
                            <asp:LinkButton Enabled="True" ID="btncancelar" runat="server" CssClass="btn btn-danger"> <i class="fas fa-eraser"></i> Cancelar</asp:LinkButton>
                        </div>
                        <div class="col-lg-4"></div>

                        <div class="col-lg-12 text-center">
                            
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <asp:Repeater runat="server" ID="rptservicos">
                            <HeaderTemplate>
                                <table class="container-fluid form-control" style="border: 0; width: 100%; height: 100%;">

                                    <thead>
                                    </thead>
                                    <tbody class="tbody form-control" style="border: 0; height: 100%;">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <th class="item" style="padding: 1rem">
                                    <div class="info-box bg-green" style="border-radius: 5%">
                                        <div class="row" style="padding: 1rem;">
                                            <div class="col-lg-12 ">
                                                <div class="row">
                                                    <div class="col-lg-12 text-center">
                                                        <asp:HiddenField ID="hdnrseqrpt" runat="server" Value='<%#Bind("nrseq") %>'></asp:HiddenField>

                                                        <asp:Label ID="lblservicorpt" runat="server" Font-Bold="true" Text='<%#Bind("descricao") %>'></asp:Label>
                                                        <br />
                                                        <asp:Label ID="lblplanorpt" runat="server" Text='<%#Bind("plano") %>'></asp:Label>

                                                    </div>

                                                </div>
                                                <div class="row" style="padding: 1rem;">

                                                    <div class="col-lg-6 ">
                                                        Consulta
                                                        <asp:Label ID="lblvalorconsrpt" runat="server" Text='<%#Bind("valorespecialidades") %>'></asp:Label>
                                                    </div>
                                                    <div class="col-lg-6 ">
                                                        Repasse
                                                        <asp:Label ID="lblvalorrpt" runat="server" Text='<%#Bind("Vlrepasse") %>'></asp:Label>
                                                    </div>

                                                </div>
                                                <div class="row" style="padding: 1rem;">

                                                    <div class="col-lg-6 text-center  ">
                                                        <asp:LinkButton ID="btnremoverrpt" runat="server" CssClass="btn btn-danger" CommandName="excluir" Style="border-radius: 50%"><i class="fa fa-trash-o" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                    <div class="col-lg-6 text-center  ">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-warning" CommandName="editar" Style="border-radius: 50%"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> 
                              </div>
                              </table>
                           
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

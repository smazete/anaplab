﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="site.aspx.vb" MasterPageFile="~/masterpage.master"  Inherits="site" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server"><div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
<div class="box box-header">
<b> Informativo Home page

</b>
</div>
<div class="box-body">
<div class="row">
<div class="col-lg-12 text-center">
<asp:LinkButton ID = "btnnovo" runat="server" CssClass="btn btn-warning "> <i class="fas fa-plus-circle"></i>Novo</asp:LinkButton>
<br>
</div>
</div>
<div class="row">
<div class="col-lg-10">
<div class="col-lg-2">
Nrseq
<br>
<asp:TextBox id="txtnrseq" runat="server" cssclass="form-control" ></asp:TextBox>
</div>
<div class="col-lg-10">
Titulo
<br>
<asp:TextBox id="txtTitulo" runat="server" cssclass="form-control" ></asp:TextBox>
</div>
<div class="col-lg-12">
Descricao
<br> <asp:TextBox ID="txtdescricao" TextMode="MultiLine" Style="margin-right: 10px; margin-left: 10px;" Height="420px" runat="server" Width="100%" Rows="100">
                                            </asp:TextBox>
                                            <ajaxToolkit:HtmlEditorExtender ID="TextBox1_HtmlEditorExtender" runat="server" TargetControlID="txtdescricao" EnableSanitization="false">
                                            </ajaxToolkit:HtmlEditorExtender>
</div>
</div>
    
<div class="col-lg-2">
<div class="col-lg-12">
Assinatura
<br>
<asp:TextBox id="txtassinatura" runat="server" cssclass="form-control" ></asp:TextBox>
</div><br>
<div class="col-lg-12">
Usercad
<br>
<asp:TextBox id="txtusercad" runat="server" cssclass="form-control" ></asp:TextBox>
</div><br>
<div class="col-lg-12">
Dtcad
<br>
<asp:TextBox id="txtdtcad" runat="server" cssclass="form-control" ></asp:TextBox>
</div><br>
<div class="col-lg-12">
Validoate
<br>
<asp:TextBox id="txtvalidoate" runat="server" cssclass="form-control" ></asp:TextBox>
</div><br>
<div class="col-lg-12">
Ativo
<br>
<asp:TextBox id="txtativo" runat="server" cssclass="form-control" ></asp:TextBox>
</div><br>
</div>



</div>
<div class="row">
<div class="col-lg-12 text-center">
<br>
<asp:LinkButton ID = "btnsalvar" runat="server" CssClass="btn btn-success "> <i class="fas fa-floppy-o"></i>Salvar</asp:LinkButton>
</div>
</div>



<div class="row">
<br>
 <asp:GridView ID = "grade" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="Nenhuma doca cadastrada !" CssClass="table" GridLines="none" AutoGenerateColumns="false">
<columns>
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label runat="server" Text='<%#Container.DisplayIndex + 1 %>'></asp:Label>
<asp:HiddenField ID="hdnrseqgrade" runat="server" value='<%#Bind("nrseq")%>'></asp:HiddenField>
 </ItemTemplate>
</asp:TemplateField >
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label id="lblnrseqgrade" runat="server" Text='<%#Bind("nrseq")%>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label id="lbltitulograde" runat="server" Text='<%#Bind("titulo")%>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label id="lbldescricaograde" runat="server" Text='<%#Bind("descricao")%>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label id="lblassinaturagrade" runat="server" Text='<%#Bind("assinatura")%>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label id="lblusercadgrade" runat="server" Text='<%#Bind("usercad")%>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label id="lbldtcadgrade" runat="server" Text='<%#Bind("dtcad")%>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label id="lblvalidoategrade" runat="server" Text='<%#Bind("validoate")%>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
<asp:TemplateField >
 <ItemTemplate>
 <asp:Label id="lblativograde" runat="server" Text='<%#Bind("ativo")%>'></asp:Label>
 </ItemTemplate>
</asp:TemplateField >
 </columns>
</asp:GridView >
</div>
    </div>
</div>



</asp:Content>

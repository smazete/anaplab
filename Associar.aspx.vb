﻿Imports clsSmart
Imports System.Web.Mail
Imports System.Text


Partial Class Associar
    Inherits System.Web.UI.Page
    Dim sql1 As String
    Dim tbassociado As New clsBanco
    Dim temp As New Data.DataTable
    Dim nrseq As Integer
    Dim minhasql As String
    Dim newthread As New Threading.Thread(AddressOf aguarde)

    Private dadosArray() As String = {"111.111.111-11", "222.222.222-22", "333.333.333-33", "444.444.444-44",
                                              "555.555.555-55", "666.666.666-66", "777.777.777-77", "888.888.888-88", "999.999.999-99"}
    Private Const msgErro As String = "Dados Inválidos"
    Private bValida As Boolean
    Private sCPF As String
    Private sCNPJ As String
    Public mMensagemAguarde As String = ""
    Public Sub aguarde()
    End Sub
    Private Function ValidaCPF(ByVal CPF As String) As Boolean

        Dim i, x, n1, n2 As Integer

        CPF = CPF.Trim
        For i = 0 To dadosArray.Length - 1
            If CPF.Length <> 14 Or dadosArray(i).Equals(CPF) Then
                Return False
            End If
        Next
        'remove a maskara
        CPF = CPF.Substring(0, 3) + CPF.Substring(4, 3) + CPF.Substring(8, 3) + CPF.Substring(12)
        For x = 0 To 1
            n1 = 0
            For i = 0 To 8 + x
                n1 = n1 + Val(CPF.Substring(i, 1)) * (10 + x - i)
            Next
            n2 = 11 - (n1 - (Int(n1 / 11) * 11))
            If n2 = 10 Or n2 = 11 Then n2 = 0

            If n2 <> Val(CPF.Substring(9 + x, 1)) Then
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub voltarcor()
        txtNome.BackColor = Drawing.Color.White
        txtnomemae.BackColor = Drawing.Color.White
        txtMatricula.BackColor = Drawing.Color.White
        txtdtnasc.BackColor = Drawing.Color.White
        txttelddd.BackColor = Drawing.Color.White
        txtRG.BackColor = Drawing.Color.White
        txtcep.BackColor = Drawing.Color.White
        txtcomplemento.BackColor = Drawing.Color.White
        txtnumero.BackColor = Drawing.Color.White
        txtcomplemento.BackColor = Drawing.Color.White
        txtbairro.BackColor = Drawing.Color.White
        txtcidade.BackColor = Drawing.Color.White
        txtcep.BackColor = Drawing.Color.White
        txttelresidencial.BackColor = Drawing.Color.White
        txtemail.BackColor = Drawing.Color.White
        txtdtposse.BackColor = Drawing.Color.White
        txtdtaposentadoria.BackColor = Drawing.Color.White
        txtnragencia.BackColor = Drawing.Color.White
        TxtcontaCorrente.BackColor = Drawing.Color.White
        txtendereco.BackColor = Drawing.Color.White
        txtdtnasc.BackColor = Drawing.Color.White
    End Sub

    Private Sub enviar()
        voltarcor()

        Dim existe As Boolean = False
        If radiomasculino.Checked = False And Radiofeminino.Checked = False Then
            grupomf.BackColor = Drawing.Color.IndianRed
            existe = True
        End If

        If validarconta(txtMatricula.Text & txtMatricula_digito.Text) = False Then
            txtMatricula.BackColor = Drawing.Color.IndianRed
            existe = False
        End If

        If validarconta(txtnragencia.Text & txtagenciadigito.Text) = False Then
            txtnragencia.BackColor = Drawing.Color.IndianRed
            existe = False
        End If

        If validarconta(TxtcontaCorrente.Text & txtcontadigito.Text) = False Then
            TxtcontaCorrente.BackColor = Drawing.Color.IndianRed
            existe = False
        End If
        If txtendereco.Text = "" Then
            txtendereco.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtcidade.Text = "" Then
            txtcidade.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtagenciadigito.Text = "" Then
            txtagenciadigito.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtMatricula_digito.Text = "" Then
            txtMatricula_digito.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtdtnasc.Text = "" Then
            txtdtnasc.BackColor = Drawing.Color.IndianRed
            existe = True
        End If

        If txtsenha.Text = "" And txtconfirmasenha.Text = "" Then
        End If
        If txtsenha.Text <> txtconfirmasenha.Text Then
            txtsenha.BackColor = Drawing.Color.IndianRed
            txtconfirmasenha.BackColor = Drawing.Color.IndianRed
            lblmens.Text = "Senhas não Conferem"
            existe = True
        End If

        If txtMatricula.Text = "" Then
            txtMatricula.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtNome.Text = "" Then
            txtNome.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If ValidaCPF(txtCpf.Text) = False Then
            txtCpf.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txttelddd.Text = "" Then
            txttelddd.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtNome.Text = "" Then
            txtNome.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtRG.Text = "" Then
            txtRG.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtbairro.Text = "" Then
            txtbairro.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtcep.Text = "" Then
            txtcep.BackColor = Drawing.Color.IndianRed
            existe = True
        End If

        If existe = True Then
            lblmens.Visible = True
            lblmensver.Visible = True
            lblmens.Text = " * Os campos São invalidos"
            Exit Sub
        End If

        '----------------------------------------------------------
        If IsNumeric(txtnumero.Text) = False Then
            txtnumero.Text = 0
        End If

        tbassociado = Nothing
        temp = Nothing

        tbassociado = New clsBanco
        temp = New Data.DataTable

        tbassociado = Nothing
        temp = Nothing

        tbassociado = New clsBanco

        Dim matriculacrtl As String
        matriculacrtl = gerarnrseqcontrole()
        Dim datanasc As Date = txtdtnasc.Text
        '  sql1 = "insert into tbAssociados (matricula,nome,situacaopb,nomemae,sexo,dtnasc,cpf,rg,cep,endereco,numero,complemento,bairro,cidade,telefone,email,dtposse,dtaposentadoria,senha,agencia,contacorrente,autorizacao,usercad,matriculactrl,ativo) values('" & txtMatricula.Text & txtMatricula_digito.Text & "','" & tratatexto(txtNome.Text) & "','" & tratatexto(cbosituacao.Text) & "','" & tratatexto(txtnomemae.Text) & "','" & IIf(radiomasculino.Checked = True, "M", "F") & "','" & formatadatamysql(datanasc) & "','" & tratatexto(txtCpf.Text) & "','" & tratatexto(txtRG.Text) & "','" & tratatexto(txtcep.Text) & "','" & tratatexto(txtendereco.Text) & "','" & tratatexto(txtnumero.Text) & "','" & tratatexto(txtcomplemento.Text) & "','" & tratatexto(txtbairro.Text) & "','" & tratatexto(txtcidade.Text) & "','" & tratatexto(txttelresidencial.Text) & "','" & tratatexto(txtemail.Text) & "','" & formatadatamysql(txtdtposse.Text) & "','" & formatadatamysql(txtdtaposentadoria.Text) & "','" & tratatexto(txtsenha.Text) & "','" & tratatexto(txtnragencia.Text & txtagenciadigito.Text) & "','" & tratatexto(TxtcontaCorrente.Text & txtcontadigito.Text) & "','1','" & "Associado" & "','" & matriculacrtl & "',1)"
        'tbassociado.ExecutaSql = sql1
        'temp = tbassociado.incluiralterardados
        MsgBox("next save")

        ' limpar()

        Response.Redirect("conclusaocadastro.aspx?script=" & 0)
    End Sub


    Private Sub limpar()
        txtMatricula.Text = ""
        txtNome.Text = ""
        txtnomemae.Text = ""
        txtdtnasc.Text = ""
        txtCpf.Text = ""
        txtRG.Text = ""
        txtcep.Text = ""
        txtendereco.Text = ""
        txtnumero.Text = ""
        txtcomplemento.Text = ""
        txtbairro.Text = ""
        txtcidade.Text = ""
        ' cboestado.Text = ""
        txttelddd.Text = ""
        txttelresidencial.Text = ""
        txtemail.Text = ""
        txtdtaposentadoria.Text = ""
        txtdtposse.Text = ""
        txtsenha.Text = ""
        txtconfirmasenha.Text = ""
        txtnragencia.Text = ""
        TxtcontaCorrente.Text = ""
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim cultura As System.Globalization.CultureInfo = New System.Globalization.CultureInfo("pt-BR")
        Dim formato As System.Globalization.DateTimeFormatInfo = cultura.DateTimeFormat
        Dim dia As Integer = DateTime.Now.Day
        Dim ano As Integer = DateTime.Now.Year
        Dim Mes As String = cultura.TextInfo.ToTitleCase(formato.GetMonthName(DateTime.Now.Month))
        Dim DiadaSemana As String = cultura.TextInfo.ToTitleCase(formato.GetDayName(DateTime.Now.DayOfWeek))
        Dim dataporExtenso As String = DiadaSemana & ", " & dia & " de " & Mes & " de " & ano
        lbldata.Text = dataporExtenso

        Dim PostBackOptionsoptionsSubmit As New PostBackOptions(btnsalvar)
        btnsalvar.OnClientClick = "disableButtonOnClick(this, 'Aguarde...'); "
        btnsalvar.OnClientClick += ClientScript.GetPostBackEventReference(PostBackOptionsoptionsSubmit)

        temp = Nothing

        temp = New Data.DataTable

        If lblnrseqalterar.Text = "" Then
            lblnrseqalterar.Text = 0
        End If
        Dim y As Integer = Request.QueryString("Script")
        If CType(lblnrseqalterar.Text, Integer) = y Then
            Exit Sub
        End If
        lblnrseqalterar.Text = Request.QueryString("Script")

        If lblnrseqalterar.Text = "" Or lblnrseqalterar.Text = 0 Then
            Exit Sub
        End If

        sql1 = "Select * from tbclientes where nrseq = " & lblnrseqalterar.Text
        tbassociado.ExecutaSql = sql1
        temp = tbassociado.conectar

        If temp.Rows.Count = 0 Then
            Exit Sub
        End If
        txtMatricula.Text = temp.Rows(0)("matricula").ToString
        txtNome.Text = temp.Rows(0)("nome").ToString
        txtdtnasc.Text = tratadata(temp.Rows(0)("dtnasc").ToString, gravardata.recuperar)
        txtcomplemento.Text = temp.Rows(0)("endereco").ToString
        txtnumero.Text = temp.Rows(0)("numero").ToString
        txtcomplemento.Text = temp.Rows(0)("compl").ToString
        txtbairro.Text = temp.Rows(0)("bairro").ToString
        txtcomplemento.Text = temp.Rows(0)("uf").ToString
        txtcep.Text = temp.Rows(0)("cep").ToString
        txtcep.Text = temp.Rows(0)("cidade").ToString
        txtbairro.Text = temp.Rows(0)("tel1").ToString
        txtcep.Text = temp.Rows(0)("telcel").ToString()
        txttelddd.Text = temp.Rows(0)("cpf").ToString
        txtRG.Text = temp.Rows(0)("rg").ToString
    End Sub


    Protected Sub cbosituacao_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbosituacao.DataBound

    End Sub

    Protected Sub cbosituacao_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbosituacao.Disposed

    End Sub

    Protected Sub cbosituacao_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbosituacao.Load

    End Sub

    Protected Sub cbosituacao_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbosituacao.SelectedIndexChanged

    End Sub

    Protected Sub cbosituacao_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbosituacao.Unload
    End Sub

    Public Function validarconta(ByVal testo As String) As Boolean

        If testo <> "" Then
            Dim qtd As String = Len(testo)
            Dim digitoinformado As String = Mid(testo, qtd, 1)
            qtd = Len(testo) - 1
            Dim valor As Integer
            Dim numero As Integer = 9
            Dim digitofinal As Integer

            testo = Mid(testo, 1, Len(qtd - 1))
            For x As Integer = 0 To qtd - 1
                valor = Mid(testo, qtd, 1) * numero
                digitofinal = digitofinal + valor


                If numero = 0 Then
                    numero = 9
                End If
                numero -= 1
                qtd = qtd - 1

                If qtd = 0 Then
                    qtd += 1
                End If
            Next
            digitofinal = digitofinal Mod 11


            If digitofinal <> 10 Then

                If IsNumeric(digitoinformado) = False Then
                    Return False
                    Exit Function
                End If

                If CType(digitoinformado, Integer) = digitofinal Then
                    Return True
                Else
                    Return False
                End If

            Else

                If CType(digitoinformado, String) = "X" Or CType(digitoinformado, String) = "x" Then
                    Return True
                Else
                    Return False
                End If
            End If



        Else
            Return False
        End If
    End Function


    Protected Sub btnlocalizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlocalizar.Click
        Try

            txtcep.BackColor = Drawing.Color.White
            If Len(txtcep.Text) <> 9 Then
                txtcep.BackColor = Drawing.Color.IndianRed
                Exit Sub
            End If
            Dim wcprocuracep As Boolean = False
            If wcprocuracep Then

            Else
                If txtcep.Text = "" Then Exit Sub
                newthread = New Threading.Thread(AddressOf aguarde)
                mMensagemAguarde = "Procurando o cep na web..."
                newthread.Start()
                txtendereco.Text = BuscaCep(txtcep.Text)("tipologradouro") & " " & BuscaCep(txtcep.Text)("logradouro")
                txtcidade.Text = BuscaCep(txtcep.Text)("cidade")
                txtbairro.Text = BuscaCep(txtcep.Text)("bairro")
                Try
                    cboestado.Items.Clear()
                    cboestado.Items.Add(BuscaCep(txtcep.Text)("UF"))
                    newthread.Abort()
                Catch ex As Exception

                End Try

            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub txtNome_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNome.TextChanged
        txtNome.Text = UCase(txtNome.Text)
    End Sub


    Public Function enviarcopiaerro(ByVal mensagem As String) As Boolean
        Try

            Dim OA As String
            If radiomasculino.Checked = True Then
                OA = "o"
            Else
                OA = "a"
            End If


            listmensagem.Text = "                                                 Joinville (SC), " & Date.Now.Date
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "Prezad" & OA & " Colega " & txtNome.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Sentimo-nos honrados ao acolher o seu pedido de filiação"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "e faremos tudo que estiver ao nosso alcance para atender as"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "suas demandas judiciais, que também são as nossas."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Neste momento nosso sistema gerou a 	 " & txtsenha.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "personalizada. De posse desta senha você estará apt" & OA & " a"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "ingressar no autoatendimento da ANAPLAB."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "Caso ainda não tenha autenticado sua fiiação, clique no link "
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "abaixo e faça login para concluir o processo."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "ou copie e cole na barra de endereço do seu navegador."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "http://anaplab.com.br/autoatendimento/LoginConfirmarFiliacao.aspx"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Em até 2 (dois) dias úteis, nosso Departamento de"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "Tesouraria lhe enviará a CONFIRMAÇÃO DE SUA FILIAÇÃO,"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "juntamente com as instruções para a adesão ao débito"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "automático de suas mensalidades."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Sinta-se em casa."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Utilize nossos CANAIS DE ATENDIMENTO para emitir"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "sugestões e/ou críticas."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Sua participação é muito importante." '

            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Ajude-nos a construir uma Associação voltada para os"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "interesses dos associados."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Atenciosamente."
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Ari Zanella"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "  Presidente Administrativo."


            Dim mySMTP, myPORTA, myUSER, mySENHA, myEMAIL As String
            mySMTP = "smtp.anaplab.com.br"
            mySENHA = "sjp21071"
            myUSER = "atendimento@anaplab.com.br"
            myEMAIL = "ANAPLAB - Atendimento <atendimento@anaplab.com.br>"
            myPORTA = "587"
            Dim email As New clsEnvioEmail
            email.AdicionaAssunto = " CADASTRO ANAPLAB"
            email.AdicionaDestinatarios = txtemail.Text
            'email.AdicionaDestinatarios = "mariodelimadobrasil@gmail.com"

            email.AdicionaMensagem = listmensagem.Text


            email.ConfigPorta = myPORTA
            email.ConfigSMTP = mySMTP
            email.AdicionaRemetente = myEMAIL

            email.Credenciais(myUSER, mySENHA, mySMTP)

            email.EnviarEmail()

        Catch ex As Exception

        End Try

    End Function


    Public Function emailcadastro(ByVal mensagem As String) As Boolean
        Try

            listmensagem.Font.Size = 22
            listmensagem.ForeColor = Drawing.Color.Blue

            listmensagem.Text = ""

            Dim SEXO As String

            If radiomasculino.Checked = True Then
                SEXO = "MASCULINO"
            Else
                SEXO = "FEMININO"
            End If


            listmensagem.Text = "FICHA DE FILIAÇÃO"
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "MATRICULA: " & txtMatricula.Text & txtMatricula_digito.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "NOME: " & txtNome.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "NOME DA MAE : " & txtnomemae.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "SEXO: " & SEXO
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "DATA DE NASCIMENTO " & txtdtnasc.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "CPF : " & txtCpf.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "RG: " & txtRG.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "CEP: " & txtcep.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "ENDEREÇO: " & txtendereco.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "COMPLEMENTO: " & txtcomplemento.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "BAIRRO: " & txtcomplemento.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "CIDADE: " & txtcidade.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "ESTADO: " & cboestado.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "TELEFONE: (" & txttelddd.Text & ")" & txttelresidencial.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "EMAIL: " & txtemail.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "DATA DA POSSE: " & txtdtposse.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "DATA DA APOSENTADORIA: " & txtdtaposentadoria.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "AGENCIA: " & txtnragencia.Text & "-" & txtagenciadigito.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "CONTA CORRENTE: " & TxtcontaCorrente.Text & "-" & txtcontadigito.Text
            listmensagem.Text = listmensagem.Text & Environment.NewLine & ""
            listmensagem.Text = listmensagem.Text & Environment.NewLine & "DATA DE CADASTRO: " & Date.Now.Date


            Dim arqerro As String = "LogImagemErro"

            Dim mySMTP, myPORTA, myUSER, mySENHA, myEMAIL As String
            mySMTP = "smtp.anaplab.com.br"
            mySENHA = "sjp21071"
            myUSER = "atendimento@anaplab.com.br"
            myEMAIL = "ANAPLAB - Atendimento <atendimento@anaplab.com.br>"
            myPORTA = "587"
            Dim email As New clsEnvioEmail
            email.AdicionaAssunto = txtNome.Text
            email.AdicionaDestinatarios = "cadastro@anaplab.com.br"
            'email.AdicionaDestinatarios = "mariodelimadobrasil@gmail.com"


            email.AdicionaMensagem = listmensagem.Text


            email.ConfigPorta = myPORTA
            email.ConfigSMTP = mySMTP
            email.AdicionaRemetente = myEMAIL

            email.Credenciais(myUSER, mySENHA, mySMTP)

            email.EnviarEmail()

        Catch ex As Exception

        End Try
    End Function

    Public Function enviosql(ByVal mensagem As String) As Boolean
        Dim arqerro As String = "LogImagemErro"
        Dim mySMTP, myPORTA, myUSER, mySENHA, myEMAIL As String
        mySMTP = "smtp.anaplab.com.br"
        mySENHA = "sjp21071"
        myUSER = "atendimento@anaplab.com.br"
        myEMAIL = "ANAPLAB - Atendimento <atendimento@anaplab.com.br>"
        myPORTA = "587"

        Dim email As New clsEnvioEmail
        email.AdicionaAssunto = "SQl"
        email.AdicionaDestinatarios = "mariodelimadobrasil@gmail.com"

        email.AdicionaMensagem = minhasql

        email.ConfigPorta = myPORTA
        email.ConfigSMTP = mySMTP
        email.AdicionaRemetente = myEMAIL

        email.Credenciais(myUSER, mySENHA, mySMTP)

        email.EnviarEmail()

    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim existe As Boolean = False


        If validarconta(txtMatricula.Text & txtMatricula_digito.Text) = False Then
            txtMatricula.BackColor = Drawing.Color.IndianRed
            existe = False
        End If

        If validarconta(txtnragencia.Text & txtagenciadigito.Text) = False Then
            txtnragencia.BackColor = Drawing.Color.IndianRed
            existe = False
        End If

        If validarconta(TxtcontaCorrente.Text & txtcontadigito.Text) = False Then
            TxtcontaCorrente.BackColor = Drawing.Color.IndianRed
            existe = False
        End If

        If txtsenha.Text = "" And txtconfirmasenha.Text = "" Then
        End If

        If txtsenha.Text <> txtconfirmasenha.Text Then
            txtsenha.BackColor = Drawing.Color.IndianRed
            txtconfirmasenha.BackColor = Drawing.Color.IndianRed
            lblmens.Text = "Senhas não Conferem"
            existe = True
        End If


        If txtMatricula.Text = "" Then
            txtMatricula.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtNome.Text = "" Then
            txtNome.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If ValidaCPF(txtCpf.Text) = False Then
            txtCpf.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txttelddd.Text = "" Then
            txttelddd.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtNome.Text = "" Then
            txtNome.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtRG.Text = "" Then
            txtRG.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtbairro.Text = "" Then
            txtbairro.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtcep.Text = "" Then
            txtcep.BackColor = Drawing.Color.IndianRed
            existe = True
        End If


        If existe = True Then
            lblmens.Visible = True
            lblmensver.Visible = True
            lblmens.Text = " * Os campos São invalidos"
            Exit Sub
        End If

        '----------------------------------------------------------
        If IsNumeric(txtnumero.Text) = False Then
            txtnumero.Text = 0
        End If

        tbassociado = Nothing
        temp = Nothing

        tbassociado = New clsBanco
        temp = New Data.DataTable


        enviarcopiaerro("erro servidor")

        tbassociado = Nothing
        temp = Nothing

        tbassociado = New clsBanco

        Dim matriculacrtl As String
        matriculacrtl = gerarnrseqcontrole()
        Dim datanasc As Date = txtdtnasc.Text
        sql1 = "insert into tbAssociados (matricula,nome,situacaopb,nomemae,sexo,dtnasc,cpf,rg,cep,endereco,numero,complemento,bairro,cidade,telefone,email,dtposse,dtaposentadoria,senha,agencia,contacorrente,autorizacao,usercad,matriculactrl,ativo) values('" & txtMatricula.Text & txtMatricula_digito.Text & "','" & tratatexto(txtNome.Text) & "','" & tratatexto(cbosituacao.Text) & "','" & tratatexto(txtnomemae.Text) & "','" & IIf(radiomasculino.Checked = True, "M", "F") & "','" & formatadatamysql(datanasc) & "','" & tratatexto(txtCpf.Text) & "','" & tratatexto(txtRG.Text) & "','" & tratatexto(txtcep.Text) & "','" & tratatexto(txtendereco.Text) & "','" & tratatexto(txtnumero.Text) & "','" & tratatexto(txtcomplemento.Text) & "','" & tratatexto(txtbairro.Text) & "','" & tratatexto(txtcidade.Text) & "','" & tratatexto(txttelresidencial.Text) & "','" & tratatexto(txtemail.Text) & "','" & formatadatamysql(txtdtposse.Text) & "','" & formatadatamysql(txtdtaposentadoria.Text) & "','" & tratatexto(txtsenha.Text) & "','" & tratatexto(txtnragencia.Text & txtagenciadigito.Text) & "','" & tratatexto(TxtcontaCorrente.Text & txtcontadigito.Text) & "','1','" & "Associado" & "','" & matriculacrtl & "',1)"
        tbassociado.ExecutaSql = sql1
        temp = tbassociado.IncluirAlterarDados

        limpar()

        Response.Redirect("conclusaocadastro.aspx?script=" & 0)

    End Sub

    Protected Sub TextBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listmensagem.TextChanged

    End Sub

    Protected Sub btnsalvar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsalvar.Click

        Dim meuip As String
        meuip = getIP()


        voltarcor()
        Dim existe As Boolean = False

        If validarconta(txtMatricula.Text & txtMatricula_digito.Text) = False Then
            txtMatricula.BackColor = Drawing.Color.IndianRed
            existe = True
        End If

        If validarconta(txtnragencia.Text & txtagenciadigito.Text) = False Then
            txtnragencia.BackColor = Drawing.Color.IndianRed
            existe = True
        End If

        If validarconta(TxtcontaCorrente.Text & txtcontadigito.Text) = False Then
            TxtcontaCorrente.BackColor = Drawing.Color.IndianRed
            existe = True
        End If

        If txtsenha.Text = "" And txtconfirmasenha.Text = "" Then
            txtsenha.BackColor = Drawing.Color.IndianRed
            txtconfirmasenha.BackColor = Drawing.Color.IndianRed
            lblmens.Text = "Senhas não Conferem"
            existe = True
            ' txtsenha.Text = gerarsenhaautomatica(txtNome.Text)
            ' txtconfirmasenha.Text = txtsenha.Text
        End If
        If txtsenha.Text <> txtconfirmasenha.Text Then
            txtsenha.BackColor = Drawing.Color.IndianRed
            txtconfirmasenha.BackColor = Drawing.Color.IndianRed
            lblmens.Text = "Senhas não Conferem"
            existe = True
        End If


        If cbosituacao.Text = "Aposentadoria" Then
            If IsDate(txtdtaposentadoria) = False Then
                txtdtaposentadoria.BackColor = Drawing.Color.IndianRed
                existe = True
            End If

            If IsDate(txtdtposse.Text) = False Then
                txtdtposse.BackColor = Drawing.Color.IndianRed
                existe = True
            End If

        End If

        If txtMatricula.Text = "" Then
            txtMatricula.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtNome.Text = "" Then
            txtNome.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If ValidaCPF(txtCpf.Text) = False Then
            txtCpf.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txttelresidencial.Text = "" Then
            txttelresidencial.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtemail.Text = "" Then
            txtemail.BackColor = Drawing.Color.IndianRed
            existe = True
        End If

        If txttelddd.Text = "" Then
            txttelddd.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtnomemae.Text = "" Then
            txtnomemae.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtRG.Text = "" Then
            txtRG.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtbairro.Text = "" Then
            txtbairro.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtcep.Text = "" Then
            txtcep.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtcidade.Text = "" Then
            txtcidade.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtnumero.Text = "" Then
            txtnumero.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtagenciadigito.Text = "" Then
            txtnumero.BackColor = Drawing.Color.IndianRed
            existe = True
        End If
        If txtnragencia.Text = "" Then
            txtnumero.BackColor = Drawing.Color.IndianRed
            existe = True
        End If

        If existe = True Then
            btnsalvar.Enabled = True
            lblmens.Visible = True
            lblmensver.Visible = True
            '         Session("STATUS") = Nothing
            panelaviso.Visible = True
            lblAviso.Text = "Atenção:Preencha os campos corretamente."
            Exit Sub
        End If

        '----------------------------------------------------------
        If IsNumeric(txtnumero.Text) = False Then
            txtnumero.Text = 0
        End If

        tbassociado = Nothing
        temp = Nothing

        tbassociado = New clsBanco
        temp = New Data.DataTable


        sql1 = "select * from tbAssociados where cpf='" & txtCpf.Text & "'"
        tbassociado.ExecutaSql = sql1
        temp = tbassociado.conectar

        If temp.Rows.Count <> 0 Then
            '          Session("STATUS") = Nothing
            Response.Redirect("usuariocadastrado.aspx?script=" & txtCpf.Text)
            Exit Sub
        End If

        tbassociado = Nothing
        temp = Nothing

        tbassociado = New clsBanco
        temp = New Data.DataTable


        sql1 = "select * from tbAssociados where email='" & txtemail.Text & "'"
        tbassociado.ExecutaSql = sql1
        temp = tbassociado.conectar

        If temp.Rows.Count <> 0 Then
            '           Session("STATUS") = Nothing
            Response.Redirect("usuariocadastrado.aspx?script=" & txtCpf.Text)
            Exit Sub
        End If

        Dim codigoseq As Integer
        Try
            tbassociado = Nothing
            temp = Nothing

            tbassociado = New clsBanco
            temp = New Data.DataTable


            sql1 = "select * from tbAssociados order by codigo desc"
            tbassociado.ExecutaSql = sql1
            temp = tbassociado.conectar
            If temp.Rows.Count = 0 Then
                codigoseq = 1
            Else
                codigoseq = temp.Rows(0)("codigo") + 1
            End If
        Catch ex As Exception

        End Try

        tbassociado = Nothing
        temp = Nothing

        tbassociado = New clsBanco
        temp = New Data.DataTable

        Dim matriculacrtl As String
        matriculacrtl = gerarnrseqcontrole()

        Try
            emailcadastro("erro servidor") 'envio de email para cadastro@anaplab.com.br
            enviarcopiaerro("erro servidor") 'envio de email para confirmação cliente
        Catch ex As Exception

        End Try

        sql1 = "insert into tbAssociados (matricula,nome,situacaopb,nomemae,sexo,dtnasc,cpf,rg,cep,endereco,numero,complemento,bairro,cidade,telddd,telefone,dddcelular,celular,email,dtposse,dtaposentadoria,senha,agencia,contacorrente,autorizacao,usercad,dtcad,matriculactrl,ativo,estado,codigo,ip) values('" & txtMatricula.Text & txtMatricula_digito.Text & "','" & tratatexto(txtNome.Text) & "','" & tratatexto(cbosituacao.Text) & "','" & tratatexto(txtnomemae.Text) & "','" & IIf(radiomasculino.Checked = True, "M", "F") & "','" & formatadatamysql(txtdtnasc.Text) & "','" & tratatexto(txtCpf.Text) & "','" & tratatexto(txtRG.Text) & "','" & tratatexto(txtcep.Text) & "','" & tratatexto(txtendereco.Text) & "','" & tratatexto(txtnumero.Text) & "','" & tratatexto(txtcomplemento.Text) & "','" & tratatexto(txtbairro.Text) & "','" & tratatexto(txtcidade.Text) & "','" & tratatexto(txttelddd.Text) & "','" & tratatexto(txttelresidencial.Text) & "','" & tratatexto(txtdddcelular.Text) & "','" & tratatexto(txtcelular.Text) & "'   ,'" & tratatexto(txtemail.Text) & "','" & formatadatamysql(txtdtposse.Text) & "','" & formatadatamysql(txtdtaposentadoria.Text) & "','" & tratatexto(txtsenha.Text) & "','" & tratatexto(txtnragencia.Text & txtagenciadigito.Text) & "','" & tratatexto(TxtcontaCorrente.Text & txtcontadigito.Text) & "','1','" & "Associado" & "','" & formatadatamysql(Date.Now.Date) & "' ,'" & matriculacrtl & "',1,'" & cboestado.Text & "'," & codigoseq & ",'" & meuip & "')"
        minhasql = sql1

        tbassociado.ExecutaSql = sql1
        temp = tbassociado.IncluirAlterarDados

        Response.Redirect("conclusaocadastro.aspx?script=" & 0)

    End Sub

    Protected Sub cboestado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboestado.SelectedIndexChanged

    End Sub


    Public Function getIP() As String
        Dim ip As String
        ip = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")

        If ip = String.Empty Or ip = Nothing Then
            ip = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
        End If

        If ip = String.Empty Or ip = Nothing Then
            ip = HttpContext.Current.Request.UserHostAddress
        End If

        Return ip
    End Function

End Class

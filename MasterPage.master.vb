﻿Imports clsSmart
Imports clssessoes
Imports System.IO
Imports dllfrases

Partial Class restrito_MasterPage

    Inherits MasterPage

    Public conexao As String = ""
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco

    Private Sub restrito_MasterPage_Load(sender As Object, e As EventArgs) Handles Me.Load


        If IsPostBack Then Exit Sub
        'versessao()
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alteraposicao(Image6, 1)", "alteraposicao(Image6, 1)", True)

        If File.Exists(Server.MapPath("~") & "social\" & Session("logocliente")) Then

            Dim xred As New clsredimensiona
            xred.Arquivo = Server.MapPath("~") & "social\" & Session("logocliente")
            xred.Max = 10
            If xred.redimensionar Then
                imlogo.Style.Remove("Height")
                imlogo.Style.Remove("Width")
                imlogo.Style.Add("Height", xred.Novaaltura & "rem")
                imlogo.Style.Add("Width", xred.Novalargura & "rem")
            End If
            imlogo.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & Session("logocliente"))
        Else

            Dim xred As New clsredimensiona
            xred.Arquivo = Server.MapPath("~") & "img\logo.png"
            xred.Max = 10
            If xred.redimensionar Then
                imlogo.Style.Remove("Height")
                imlogo.Style.Remove("Width")
                imlogo.Style.Add("Height", xred.Novaaltura & "rem")
                imlogo.Style.Add("Width", xred.Novalargura & "rem")
            End If
        End If

        imlogo.Visible = False


        lblversao.Text = minhaversao
        Dim tbcarregaimagemm As New Data.DataTable
        Dim tabcarrega As New clsBanco

        lblbanco.Text = tabcarrega.Banco
        tbcarregaimagemm = tabcarrega.conectar("select * from tbusuarios where  usuario = '" & Session("usuario") & "' and ativo = true ")

        If tbcarregaimagemm.Rows.Count > 0 Then

            Session("imagemperfil") = tbcarregaimagemm.Rows(0)("imagemperfil").ToString
            lblusuariocompleto.Text = Session("usuario")
            If IsDate(tbcarregaimagemm.Rows(0)("dtcad").ToString) Then
                lbldtcaduser.Text = "Desde " & MesExtenso(CDate(tbcarregaimagemm.Rows(0)("dtcad").ToString).Month) & " de " & CDate(tbcarregaimagemm.Rows(0)("dtcad").ToString).Year
            End If
            If System.IO.File.Exists(Request.MapPath("~/social/" & tbcarregaimagemm.Rows(0)("imagemperfil").ToString)) Then
                imgperfil1.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & tbcarregaimagemm.Rows(0)("imagemperfil").ToString)
                imgperfilfixo.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & tbcarregaimagemm.Rows(0)("imagemperfil").ToString)
            Else
                imgperfil1.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/img/semimagem01.png")
                imgperfil1.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/img/semimagem01.png")

            End If
        Else
            imgperfil1.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/img/semimagem01.png")
            imgperfil1.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/img/semimagem01.png")
        End If

        Dim veremail As New clscaixamensagens(True)
        lblqtdmensagens.Text = veremail.novamensagem()
        If lblqtdmensagens.Text > 0 Then
            lblmensagensqtd.ForeColor = System.Drawing.Color.Red
            lblmensagensqtd.Text = "Você tem " & lblqtdmensagens.Text & IIf(lblqtdmensagens.Text = "1", " mensagem!", " mensagens !")
        Else
            lblmensagensqtd.Text = "Você não possui novas mensagens !"
        End If
        tb1 = tab1.conectar("select * from vwcaixaemails where nrseqdestinatario = " & Session("idusuario") & " and excluida = false order by  nrseq desc limit 10")
        rptmensagens.DataSource = tb1
        rptmensagens.DataBind()

        'Dim tbfrases As New Data.DataTable
        'Dim tabfrases As New clsBanco
        'tbfrases = tabfrases.conectar("select * from tbfrases")
        'Dim r As New Random()

        '' Se você quiser dar um valor minimo e maximo

        '' como intervalo...

        'Dim i As Integer = r.Next(0, tbfrases.Rows.Count - 1)
        Dim frase As New dllfrases.frases



        lblfrase.Text = Server.HtmlDecode(frase.Frase)

        lblusuario.Text = Session("usuario")

        Session("idempresaemuso") = 0
        imgnatal.Visible = False
        If ehnatal() Then
            imgnatal.Visible = True
        End If

        'Dim xaniversarios As New clsaniversarios

        'If Not xaniversarios.carregaraniversariosclientesdependentes() Then
        '    sm("swal.fire({title: 'Ops!',text: '" & xaniversarios.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        'If xaniversarios.Table.Rows.Count < 50 Then
        '    rptalertas.DataSource = xaniversarios.Table
        '    rptalertas.DataBind()

        '    lblqtdalertas.Text = xaniversarios.Contador
        'End If
        'lblmsgalerta.Text = "Importante !"

        Dim xlogin As New clslogin

        ''Dim tbnews As New Data.DataTable
        ''Dim tabnews As New clsbancosmartcode

        ''tbnews = tabnews.conectar("select * from vwprodutos_manualonline where chave = '" & xlogin.Produto & "' order by descricao")

        ''rptnews.DataSource = tbnews
        ''rptnews.DataBind()

        'lblqtdnews.Text = tbnews.Rows.Count
    End Sub

    Private Sub restrito_MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        clssessoes.versessao()
        panelprincipal.DefaultButton = "botaodefault"
    End Sub

    'Private Sub imgperfilmenu_Click(sender As Object, e As ImageClickEventArgs) Handles imgperfilmenu.Click
    '    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibirimagem()", "exibirimagem()", True)
    'End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub imgperfil1_Click(sender As Object, e As ImageClickEventArgs) Handles imgperfil1.Click
        Session("chave") = Session("idusuario")
        Session("origem") = "usuarios"
        Response.Redirect("/loadimage.aspx")
    End Sub

    Private Sub imgcorreios_Click(sender As Object, e As ImageClickEventArgs) Handles imgcorreios.Click
        Response.Redirect("/mail.aspx")
    End Sub

    Private Sub rptmensagens_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptmensagens.ItemDataBound
        If e.Item.ItemIndex < 0 Then Exit Sub
        Dim imgimagemremetente As Image = e.Item.FindControl("imgimagemremetente")
        Dim lblassunto As Label = e.Item.FindControl("lblassunto")
        Dim lbldiftempo As Label = e.Item.FindControl("lbldiftempo")
        Dim hdimagemremetente As HiddenField = e.Item.FindControl("hdimagemremetente")

        If lblassunto.Text = "" Then
            lblassunto.Text = "Sem Assunto..."
        End If
        Dim wcdata As Date = CDate(e.Item.DataItem("dtcad").ToString)
        lbldiftempo.Text = diferencatempo(wcdata, e.Item.DataItem("hrcad"), data(), hora())
        If e.Item.DataItem("lida").ToString = "0" Then
            lblassunto.Font.Bold = True

        Else

        End If
        If System.IO.File.Exists(Request.MapPath("~/social/" & hdimagemremetente.Value)) Then
            imgimagemremetente.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & hdimagemremetente.Value)

        Else
            imgimagemremetente.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/img/semimagem01.png")

        End If
        Dim xlogin As New clslogin
        Dim tbnews As New Data.DataTable
        Dim tabnews As New clsbancosmartcode



        '   tbnews = tabnews.conectar("select * from vwprodutos_manualonline where chave = '" & xlogin.Produto & "' order by descricao")

        rptnews.DataSource = tbnews
        rptnews.DataBind()

        lblqtdnews.Text = tbnews.Rows.Count
    End Sub

    Private Sub btnvermensagens_Click(sender As Object, e As EventArgs) Handles btnvermensagens.Click
        Response.Redirect("mail.aspx")
    End Sub
    Private Sub rptnews_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptnews.ItemCommand
        Dim index As Integer = e.Item.ItemIndex

        ' Retrieve the row that contains the button clicked 
        ' by the user from the Rows collection.
        Dim row As RepeaterItem = rptnews.Items(index)
        Dim lblnewrpt As Label = row.FindControl("lblnewrpt")
        Dim hdurl As HiddenField = row.FindControl("hdurl")
        If e.CommandName = "abrir" Then

            Session("manualurl") = hdurl.Value
            Response.Redirect("manual.aspx")

        End If

    End Sub

    Private Sub btnprocurarmanual_Click(sender As Object, e As EventArgs) Handles btnprocurarmanual.Click
        Session("manualurl") = ""
        Response.Redirect("manual.aspx")
    End Sub

    Private Sub rptalertas_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptalertas.ItemCommand

    End Sub

    Private Sub rptalertas_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptalertas.ItemDataBound

        Dim hdndata As Label = e.Item.FindControl("lblusuarioalerta")
        Dim hdmatricularpt As HiddenField = e.Item.FindControl("hdmatricularpt")
        Dim xaniversarios As New clsaniversarios

        If Not xaniversarios.carregaraniversariosclientesdependentes(hdmatricularpt.Value) Then
            sm("swal.fire({title: 'Ops!',text: '" & xaniversarios.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

            hdndata.Text = xaniversarios.Dtclientes

            'Dim imagemcom As String = "/img/padlogo.png"
            'Dim imgsituacao01rpt As Image = e.Item.FindControl("imgsituacao01rpt")
            'Dim imgsituacao02rpt As Image = e.Item.FindControl("imgsituacao02rpt")
            'Dim imgsituacao03rpt As Image = e.Item.FindControl("imgsituacao03rpt")
            'Dim imgsituacao04rpt As Image = e.Item.FindControl("imgsituacao04rpt")

            ''If sonumeros(e.Item.DataItem("totalavaliacoes1").ToString).Length > 0 AndAlso e.Item.DataItem("totalavaliacoes1") > 0 Then
            ''    imgsituacao01rpt.ImageUrl = "/img/av01.png"
            ''Else
            ''    imgsituacao01rpt.ImageUrl = "/img/av02.png"
            ''    imgsituacao01rpt.Visible = False
            ''End If
            ''If sonumeros(e.Item.DataItem("totalavaliacoes2").ToString).Length > 0 AndAlso e.Item.DataItem("totalavaliacoes2") > 0 Then

            ''    imgsituacao02rpt.ImageUrl = "/img/rm01.png"
            ''Else
            ''    imgsituacao02rpt.ImageUrl = "/img/rm02.png"
            ''    imgsituacao02rpt.Visible = False
            ''End If

            ''If sonumeros(e.Item.DataItem("totalavaliacoes3").ToString).Length > 0 AndAlso e.Item.DataItem("totalavaliacoes3") > 0 Then

            ''    imgsituacao03rpt.ImageUrl = "/img/rf01.png"
            ''Else
            ''    imgsituacao03rpt.ImageUrl = "/img/rf02.png"
            ''    imgsituacao03rpt.Visible = False
            ''End If
            ''If sonumeros(e.Item.DataItem("totalcompetencias").ToString).Length > 0 AndAlso e.Item.DataItem("totalcompetencias") > 0 Then

            ''    imgsituacao04rpt.ImageUrl = "/img/competencias01.png"
            ''Else
            ''    imgsituacao04rpt.ImageUrl = "/img/competencias02.png"
            ''    imgsituacao04rpt.Visible = False
            ''End If

        End If
    End Sub

    Private Sub qtcontato()

        Dim count As Integer = 0

        tb1 = tab1.conectar("select * from tbcontato where ativo = '1'")
        If tb1.Rows.Count > 0 Then
            lblqtdmensagens.Text = tb1.Rows.Count.ToString()
        End If


    End Sub
End Class


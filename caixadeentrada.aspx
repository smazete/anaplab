﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="caixadeentrada.aspx.vb" Inherits="caixadeentrada" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <title>Ceva CT-e - Painel de Controle</title>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="device-width, initial-scale=1">
        <title>Ceva CT-e</title>

        <script src="js/jquery-1.4.1.js" type="text/javascript"></script>

        <script src="js/JScriptmascara.js" type="text/javascript"></script>


        <script type="text/javascript" src="jquery-1.9.1.js"></script>
        <script type="text/javascript" src="core.js"></script>
        <script type="text/javascript" src="jquery.cookie.js"></script>
        <script src="js/jquery-1.4.1.js" type="text/javascript"></script>

        <script src="js/JScriptmascara.js" type="text/javascript"></script>
        <script src="js/painelcteview.js"></script>
        <link rel="stylesheet" href="emitir-b.css">
        <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet" />

        <link href="css/charisma-app.css" rel="stylesheet">
        <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
        <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
        <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
        <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
        <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
        <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
        <link href='css/jquery.noty.css' rel='stylesheet'>
        <link href='css/noty_theme_default.css' rel='stylesheet'>
        <link href='css/elfinder.min.css' rel='stylesheet'>
        <link href='css/elfinder.theme.css' rel='stylesheet'>
        <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='css/uploadify.css' rel='stylesheet'>
        <link href='css/animate.min.css' rel='stylesheet'>



        <script type="text/javascript">
            function fecharmodalstatus() {
                //$('.fechar').click(function (ev) {
                $("#mascara").hide();
                $(".windowstatus").hide();
                $(".infoconsulta").hide();
                $(".windowstatus").closest;

                //}

            }
            function exibirnovostatus() {
                var alturaTela = $(document).height() * 2;
                var larguraTela = $(window).width();

                // colocando o fundo preto


                //      alert('Registro gravado com sucesso2!');

                var left = ($(window).width() / 2) - ($('#alterarststatus').width() / 4);
                var top = 100;

                $('#alterarststatus').css({ 'left': 30 });
                $('#alterarststatus').css({ 'margin-top': 30 });

                $('#alterarststatus').show();
                //    $('#alterarststatus').load();
            }



        </script>
        <link rel="stylesheet" href="visual.css">
    </head>
    

   

            <div class="panel panel-primary" style="margin-top: 96px; margin-left: 5px; margin-right: 5px">

                <div class="panel-heading">
                    <center><asp:Label ID="lblnrlote" runat="server" Text="Caixa de Entrada" ></asp:Label></center>
                </div>
                <div class="panel-body">
                     <div class="row">
                             <div class="col-lg-12">
                                <div class="form-group">
                          <asp:GridView ID="Grade" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" ShowHeaderWhenEmpty="True" Width="536px">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Sel">
                            <EditItemTemplate>
                                <asp:CheckBox ID="sel0" runat="server" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="sel0" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="nrseq" HeaderText="NrSeq" />
                        <asp:BoundField DataField="data" HeaderText="Data" />
                        <asp:BoundField DataField="codigo" HeaderText="Codigo" />
                        <asp:BoundField DataField="remetente" HeaderText="Remetente" />
                        
                        <asp:ButtonField ButtonType="Button" CommandName="Reenviar" Text="  Reenviar Codigo  ">
                        <ControlStyle CssClass="botaoredondo" />
                        <ItemStyle CssClass="botaored" />
                        </asp:ButtonField>
                    </Columns>
                    <EditRowStyle BackColor="#7C6F57" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" BorderStyle="Solid" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" BorderStyle="Solid" BorderWidth="1px" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                                    </div>
                                 </div>
                        </div>
                    </div>
                </div>
    </div>
</asp:Content>


﻿Imports clsSmart
Imports clssessoes
Imports System.IO
Partial Class painel
    Inherits System.Web.UI.Page
    Dim tb1 As New Data.DataTable
    Dim tab1 As New clsBanco


    Private Sub painel_Load(sender As Object, e As EventArgs) Handles Me.Load


        If IsPostBack Then Exit Sub

        divexportar.Style.Remove("display")
        divbaixar.Style.Add("display", "none")
        divgrade.Style.Add("display", "none")
        carregagestores()
        If Session("master") = "Sim" Then
            carregarcidades()
            carregarcontratos()
        End If
        '  carregaravaliacoes()
        'If Not Session("master") = "Sim" Then
        '    txtano.ReadOnly = False
        'Else
        txtano.Text = Session("anobaseuso")
        txtano.ReadOnly = True

        '  End If

    End Sub
    Private Sub carregarcontratos()
        tb1 = tab1.conectar("select distinct contrato from vwcolaboradores order by contrato")
        cbocontrato.Items.Clear()
        cbocontrato.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbocontrato.Items.Add(tb1.Rows(x)("contrato").ToString)

        Next
    End Sub
    Private Sub carregarcidades()
        tb1 = tab1.conectar("select distinct cidade from vwcolaboradores order by cidade")
        cbocidade.Items.Clear()
        cbocidade.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbocidade.Items.Add(tb1.Rows(x)("cidade").ToString)

        Next
    End Sub
    Private Sub carregaravaliacoes()
        Dim xsql As String
        xsql = "select distinct descricao from tbresultados_finais where ativo = true  order by descricao "

        tb1 = tab1.conectar(xsql)
        '   cbo.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbogestor.Items.Add(tb1.Rows(x)("descricao").ToString)
        Next
    End Sub
    Private Sub carregagestores()
        Dim xsql As String
        xsql = "select distinct nome from vwcolaboradores where gestor = true   "
        If Not Session("master") = "Sim" Then
            xsql &= " And nrseqgestor = " & Session("nrseqcolaborador")
        End If
        xsql &= " order by nome"
        tb1 = tab1.conectar(xsql)
        cbogestor.Items.Add("")
        For x As Integer = 0 To tb1.Rows.Count - 1
            cbogestor.Items.Add(tb1.Rows(x)("nome").ToString)
        Next
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles btnProcurar.Click


        Dim xsql As String = "select * from vwpad where 1=1 and ativo = true "

        If Not Session("master") = "Sim" Then
            xsql &= " And nrseqgestor = " & Session("nrseqcolaborador")
        End If
        If cbogestor.SelectedItem.Text <> "" Then
            xsql &= " and nomegestor = '" & cbogestor.SelectedItem.Text & "'"
        End If
        If cbocidade.Text <> "" Then
            xsql &= " and empresa = '" & cbocidade.SelectedItem.Text & "'"
        End If
        If cbocontrato.Text <> "" Then
            xsql &= " and contrato = '" & cbocontrato.SelectedItem.Text & "'"
        End If
        If txtcolaborador.Text <> "" Then
            If sonumeros(txtcolaborador.Text).Length = txtcolaborador.Text.Trim.Length Then
                xsql &= " and matriculacolaborador like '%" & txtcolaborador.Text & "%'"
            Else
                xsql &= " and nomecolaborador like '%" & txtcolaborador.Text & "%'"
            End If


        End If
        If sonumeros(txtano.Text) <> "" Then
            xsql &= " and anobase = " & sonumeros(txtano.Text)
        End If
        xsql &= " order by nrseq"

        tb1 = tab1.conectar(xsql)
        If tb1.Rows.Count = 0 Then
            sm("swal({title: 'Atenção!',text: 'Desculpe, nenhum PAD foi encontrado !',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        grade.DataSource = tb1
        grade.DataBind()

        divgrade.Style.Remove("display")

    End Sub

    'Private Sub GradeLotes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GradeLotes.RowDataBound
    '    If e.Row.RowIndex < 0 Then Exit Sub
    '    Dim lblitem As Label = e.Row.FindControl("lblitem")
    '    Dim imgimagemerro As Image = e.Row.FindControl("imgimagemerro")
    '    Dim hdimagemusuario As HiddenField = e.Row.FindControl("hdimagemusuario")
    '    Dim imagemusuario As Image = e.Row.FindControl("imagemusuario")
    '    Dim lbldatacadgrade As Label = e.Row.FindControl("lbldatacadgrade")
    '    Dim lbldataprocessamento As Label = e.Row.FindControl("lbldataprocessamento")
    '    Dim lblnrseqgrade As Label = e.Row.FindControl("lblnrseqgrade")
    '    Dim lblloteempresa As Label = e.Row.FindControl("lblloteempresa")
    '    Dim btnbaixarfaturas As LinkButton = e.Row.FindControl("btnbaixarfaturas")
    '    Dim lblqtdarquivosfaturaslotes As Label = e.Row.FindControl("lblqtdarquivosfaturaslotes")

    '    Dim wccampostatus As Integer = 6
    '    Dim wcreponsavel As Integer = 7
    '    Dim btnSelecionar As LinkButton = e.Row.FindControl("btnSelecionar")
    '    imgimagemerro.Visible = False
    '    lblitem.Text = e.Row.RowIndex + 1
    '    'If IsDate(e.Row.Cells(7).Text) Then
    '    '    e.Row.Cells(7).Text = FormatDateTime(e.Row.Cells(7).Text.Replace("00:00:00", ""), DateFormat.ShortDate)
    '    'End If
    '    If lblqtdarquivosfaturaslotes.Text > 0 Then
    '        btnbaixarfaturas.Visible = True
    '    Else
    '        btnbaixarfaturas.Visible = False
    '    End If
    '    If hdimagemusuario.Value <> "" Then
    '        If System.IO.File.Exists(Request.MapPath("~/social/" & hdimagemusuario.Value)) Then
    '            imagemusuario.ImageUrl = Request.Url.OriginalString.Replace(Request.Path, "/social/" & hdimagemusuario.Value)
    '        End If
    '    End If
    '    If e.Row.DataItem("ativo").ToString = "1" Then
    '        If e.Row.DataItem("aprovado").ToString = "1" Then
    '            e.Row.ForeColor = System.Drawing.Color.White
    '            e.Row.BackColor = System.Drawing.Color.Green

    '            e.Row.Cells(wccampostatus).BackColor = System.Drawing.Color.DarkGreen
    '            e.Row.Cells(wccampostatus).ForeColor = System.Drawing.Color.White
    '            Session("aprovados") += 1

    '            e.Row.Cells(wccampostatus).Text = "Aprovado"

    '        Else
    '            If e.Row.DataItem("emanalise").ToString = "1" Then
    '                e.Row.ForeColor = System.Drawing.Color.DarkBlue
    '                e.Row.Cells(wccampostatus).BackColor = System.Drawing.Color.DarkBlue
    '                e.Row.Cells(wccampostatus).ForeColor = System.Drawing.Color.White
    '                e.Row.Cells(wccampostatus).Text = "Em análise"
    '                Session("analise") += 1
    '            End If

    '            If Not validacte(lblnrseqgrade.Text) Then
    '                btnSelecionar.Visible = True
    '                imgimagemerro.Visible = True
    '                e.Row.Cells(wccampostatus).Text = "LOTE INVÁLIDO"
    '            End If


    '        End If
    '        If e.Row.DataItem("reprovado").ToString = "1" OrElse e.Row.DataItem("status").ToString = "Reprovado" Then
    '            e.Row.ForeColor = System.Drawing.Color.DarkRed
    '            e.Row.BackColor = System.Drawing.Color.Red
    '            e.Row.ForeColor = System.Drawing.Color.White
    '            e.Row.Cells(wccampostatus).BackColor = System.Drawing.Color.DarkRed
    '            e.Row.Cells(wccampostatus).ForeColor = System.Drawing.Color.White
    '            e.Row.Cells(wccampostatus).Text = "Reprovado"
    '            Session("reprovados") += 1
    '        End If
    '    Else

    '        e.Row.BackColor = System.Drawing.Color.DarkGray
    '        e.Row.ForeColor = System.Drawing.Color.White
    '        e.Row.Cells(wccampostatus).BackColor = System.Drawing.Color.Black
    '        e.Row.Cells(wccampostatus).ForeColor = System.Drawing.Color.White
    '        e.Row.Font.Strikeout = True
    '        e.Row.Cells(wccampostatus).Text = "Excluído"
    '    End If
    '    If Server.HtmlDecode(e.Row.Cells(wccampostatus).Text) = "Em análise" Then
    '        e.Row.ForeColor = System.Drawing.Color.White
    '        e.Row.BackColor = System.Drawing.Color.Blue
    '        e.Row.ForeColor = System.Drawing.Color.White
    '        e.Row.Cells(wccampostatus).BackColor = System.Drawing.Color.DarkBlue
    '        e.Row.Cells(wccampostatus).ForeColor = System.Drawing.Color.White
    '        e.Row.Cells(wcreponsavel).Text = "Em análise"
    '        'e.Row.Cells(8).Text = ""
    '    End If
    '    If Server.HtmlDecode(e.Row.Cells(wccampostatus).Text) = "LOTE INVÁLIDO" Then
    '        e.Row.Cells(wccampostatus).BackColor = System.Drawing.Color.DarkRed
    '        e.Row.Cells(wcreponsavel).Text = "AÇÃO: Reprocesse o arquivo !"
    '        e.Row.ForeColor = System.Drawing.Color.White
    '        e.Row.BackColor = System.Drawing.Color.Red
    '    End If
    '    lbldatacadgrade.Text = e.Row.DataItem("dtcad").ToString
    '    lbldataprocessamento.Text = e.Row.DataItem("dtstatus").ToString
    '    If IsDate(lbldatacadgrade.Text) Then
    '        lbldatacadgrade.Text = FormatDateTime(lbldatacadgrade.Text, DateFormat.ShortDate)
    '    End If
    '    If IsDate(lbldatacadgrade.Text) Then
    '        lbldataprocessamento.Text = FormatDateTime(lbldatacadgrade.Text, DateFormat.ShortDate)
    '    End If

    'End Sub





    'Private Sub GradeLotes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GradeLotes.RowCommand
    '    alerta1.Style.Add("display", "none")
    '    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "escondeexibiralerta1()", "escondeexibiralerta1()", True)
    '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '    Dim row As GridViewRow = GradeLotes.Rows(index)
    '    Dim cb As CheckBox = CType(row.FindControl("sel1"), CheckBox)
    '    Dim lblnrseqgrade As Label = row.FindControl("lblnrseqgrade")
    '    Dim lblloteempresa As Label = row.FindControl("lblloteempresa")
    '    Dim naoexistemvalores As Boolean = False
    '    Select Case e.CommandName.ToLower
    '        Case Is = "email"

    '            If Not (Session("usertransp") = "" OrElse Session("usertransp") = "0") Then
    '                sm("swal({title: 'Atenção!',text: 'Desculpe, você não tem acesso a essa opção !',type: 'error',confirmButtonText: 'OK'})", "swal")
    '                Exit Sub
    '            End If
    '            Dim xlote As New clslotes
    '            lblxlote.Text = lblnrseqgrade.Text
    '            xlote.Nrlote = lblnrseqgrade.Text
    '            If Not xlote.enviaremail Then
    '                Select Case xlote.Mensagemerro.ToLower
    '                    Case Is = "LOTE INVÁLIDO".ToLower
    '                        sm("swal({title: 'Atenção!',text: 'O lote é inválido! Por favor, reprocesse!',type: 'error',confirmButtonText: 'OK'})", "swal")
    '                    Case Is = "Escolha um número sequencial válido !".ToLower
    '                        sm("swal({title: 'Atenção!',text: 'Nenhum lote localizado !',type: 'error',confirmButtonText: 'OK'})", "swal")
    '                    Case Is = "O lote informado não existe !".ToLower
    '                        sm("swal({title: 'Atenção!',text: 'Nenhum lote localizado !',type: 'error',confirmButtonText: 'OK'})", "swal")
    '                    Case Is = "Não existem valores gravados para o email. Verifique e tente novamente !".ToLower
    '                        sm("swal({title: 'Atenção!',text: 'Confirme os valores para enviar o e-mail !',type: 'error',confirmButtonText: 'OK'})", "swal")
    '                        carregaresult(lblnrseqgrade.Text)
    '                        divemail.Style.Remove("display")
    '                        divprincipal.Style.Add("display", "none")
    '                        Exit Sub
    '                End Select
    '            End If

    '            Session("minhapagina") = xlote.Pagina
    '            Session("emailpara") = xlote.Destinatarios

    '            Session("assunto") = xlote.Assunto
    '            Session("anexos") = xlote.Anexos
    '            If xlote.Urldestino <> "" Then
    '                Response.Redirect(xlote.Urldestino)
    '            End If


    '         '   Response.Redirect("envioporemail.aspx")

    '            'If row.Cells(6).Text = "LOTE INVÁLIDO" Then
    '            '    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "irpaineldados()", "irpaineldados()", True)
    '            '    lblerroconta1.Text = "Atenção: Você deve reprocessar os arquivos !"
    '            '    alerta1.Style.Remove("display")
    '            '    Exit Sub
    '            'End If
    '            'lblxlote.Text = row.Cells(1).Text
    '            'If Session("usertransp") = "" OrElse Session("usertransp") = "0" Then
    '            '    Session("consultalote") = row.Cells(1).Text
    '            '    Dim xresultado = row.Cells(6).Text
    '            '    Dim xsolicitante As Integer = 0

    '            '    Dim tblotesarqs As New Data.DataTable
    '            '    Dim tablotesarqs As New clsBanco
    '            '    Dim tbsalvar As New Data.DataTable
    '            '    Dim tabsalvar As New clsBanco
    '            '    Dim tbconfig As New Data.DataTable
    '            '    Dim tabconfic As New clsBanco
    '            '    tbconfig = tabconfic.conectar("select * from tbconfig")
    '            '    tblotesarqs = tablotesarqs.conectar("select * from tblotes where nrseq = " & row.Cells(1).Text)
    '            '    If tblotesarqs.Rows.Count > 0 Then
    '            '        If tblotesarqs.Rows(0)("nrseqsolicitante").ToString = "" Then
    '            '            tblotesarqs.Rows(0)("nrseqsolicitante") = 0

    '            '        End If
    '            '        xsolicitante = tblotesarqs.Rows(0)("nrseqsolicitante").ToString
    '            '    End If
    '            '    tblotesarqs = tablotesarqs.conectar("select * from tblotes_arquivos where tparquivo = 'XML CT-e' and nrseqlote = " & row.Cells(1).Text)

    '            '    Dim tb1 As New Data.DataTable
    '            '    Dim tab1 As New clsBanco
    '            '    tb1 = tab1.conectar("Select * from tbconfig ")
    '            '    Dim textoaprovado As String = IIf(xresultado = "Aprovado", tb1.Rows(0)("textoaprovado").ToString, tb1.Rows(0)("textoreprovado").ToString)
    '            '    Dim textotabela As String = "<table>"
    '            '    textotabela &= "<tr bgcolor=""#990033"" align=""center"" color=""#F8F8FF"">"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Data</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Filial</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Referência Ceva</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Dossie</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Despesa</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Moeda</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Valor</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Desconto</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Total a Pagar</font>"
    '            '    textotabela &= "<td><font color=""#F8F8FF"">Vencimento</font>"
    '            '    textotabela &= "</tr>"
    '            '    For x As Integer = 0 To tblotesarqs.Rows.Count - 1
    '            '        If tblotesarqs.Rows(x)("valorfinal").ToString = "" Then

    '            '            naoexistemvalores = True
    '            '            Exit For
    '            '        End If

    '            '        textotabela &= "<tr bgcolor=""#FFA07A"" align=""center"">"
    '            '        textotabela &= "<td>" & data()
    '            '        textotabela &= "<td>CPQ"
    '            '        '   textotabela &= ("<td>" & rowh.Cells(3).Text)
    '            '        textotabela &= ("<td>" & tblotesarqs.Rows(x)("refceva").ToString)
    '            '        textotabela &= ("<td>" & tblotesarqs.Rows(x)("refdossie").ToString)
    '            '        textotabela &= "<td>" & tblotesarqs.Rows(x)("despesa").ToString
    '            '        textotabela &= "<td>" & tblotesarqs.Rows(x)("moeda").ToString
    '            '        textotabela &= "<td>" & FormatNumber(tblotesarqs.Rows(x)("valorpeso").ToString, 2)
    '            '        textotabela &= "<td>" & FormatNumber(tblotesarqs.Rows(x)("desconto").ToString, 2)
    '            '        textotabela &= "<td>" & FormatNumber(tblotesarqs.Rows(x)("valorfinal").ToString, 2)
    '            '        If Not IsDate(tblotesarqs.Rows(x)("vencimento").ToString) Then tblotesarqs.Rows(x)("vencimento") = data.AddMonths(1)
    '            '        textotabela &= "<td>" & FormatDateTime(tblotesarqs.Rows(x)("vencimento").ToString, DateFormat.ShortDate)
    '            '        textotabela &= "</tr>"


    '            '    Next
    '            '    If naoexistemvalores Then
    '            '        divemail.Style.Remove("display")
    '            '        carregaresult(row.Cells(1).Text)
    '            '        lblresultadoemail.Text = row.Cells(6).Text
    '            '        If lblresultadoemail.Text.Contains("Aprovado") Then
    '            '            lblresultadoemail.BackColor = System.Drawing.Color.Green
    '            '            lblresultadoemail.ForeColor = System.Drawing.Color.White
    '            '        Else
    '            '            lblresultadoemail.BackColor = System.Drawing.Color.Red
    '            '            lblresultadoemail.ForeColor = System.Drawing.Color.White
    '            '        End If
    '            '        Exit Sub
    '            '    End If
    '            '    textotabela &= "</table>"
    '            '    textotabela &= "<br>"


    '            '    ' caso seja reprovado
    '            '    Dim textotabelaerro As String = "<table>"


    '            '    Dim tbresult As New Data.DataTable
    '            '    Dim tabresulta As New clsBanco
    '            '    tbresult = tabresulta.conectar("select * from tblotes_reprovacao where nrseqlote = " & row.Cells(1).Text & " and ativo = true")

    '            '    If tbresult.Rows.Count > 0 Then
    '            '        textotabelaerro &= "<tr bgcolor=""#990033"" align=""center"" color=""#F8F8FF"">"
    '            '        textotabelaerro &= "<td><font color=""#F8F8FF"">Motivo</font>"
    '            '        textotabelaerro &= "<td><font color=""#F8F8FF"">Valor XML</font>"
    '            '        textotabelaerro &= "<td><font color=""#F8F8FF"">Valor Tabela</font>"

    '            '        textotabelaerro &= "</tr>"
    '            '        For x As Integer = 0 To tbresult.Rows.Count - 1
    '            '            textotabelaerro &= "<tr bgcolor=""#FFA07A"" align=""center"">"

    '            '            textotabelaerro &= ("<td>" & tbresult.Rows(x)("motivo").ToString)
    '            '            textotabelaerro &= ("<td>" & tbresult.Rows(x)("valorxml").ToString)
    '            '            textotabelaerro &= ("<td>" & tbresult.Rows(x)("valortabela").ToString)

    '            '            textotabelaerro &= "</tr>"
    '            '        Next

    '            '    End If
    '            '    textotabelaerro &= "</table>"
    '            '    textotabelaerro &= "<br>"
    '            '    Dim textoresultadoap As String = "Prezado fornecedor, <br> Informamos que realizamos a conferência da sua fatura de fretes e a mesma está OK para pagamento. Para que o seu pagamento seja processado com sucesso, solicitamos que encaminha esta fatura para o e-mail: SH-AM-BR-Fiscal.FM@CevaLogistics.com <br> Atenção: O não envio da fatura para a o grupo de e-mails informado acima (após o OK deste departamento), implicará no não processamento da mesma e a CEVA não se responsabilizará por eventuais atrasos e/ou encargos decorrentes! <br> O e-mail SH-AM-BR-Fiscal.FM@CevaLogistics.com é exclusivo para envio de faturas, para realizar cobranças, composição de pagamentos recebidos, favor entrar em contato com o departamento de contas à pagar: CONTAS.PAGAR@Cevalogistics.com"
    '            '    If xresultado <> "Aprovado" Then
    '            '        textoaprovado &= ("<br>" & textotabelaerro)
    '            '        textoresultadoap = ""

    '            '    End If




    '            '    '  txtresultado.Text = Session("acao")
    '            '    Dim lermensagem As New StreamReader(Server.MapPath("~/cevaREL.html"))
    '            '    Dim htmlm As String = lermensagem.ReadToEnd
    '            '    lermensagem.Close()
    '            '    Session("minhapagina") = "mensagens/rel" & sonumeros(data) & sonumeros(hora(True)) & ".html"
    '            '    Session("arqemail") = Server.MapPath("~/" & Session("minhapagina"))
    '            '    Dim gravaremail As New StreamWriter(Session("arqemail").ToString)
    '            '    gravaremail.Write(htmlm.Replace("{lote}", Session("consultalote")).Replace("{resultado}", xresultado).Replace("{operadora}", "").Replace("{cidade}", "").Replace("{dataextenso}", dataExtenso()).Replace("{texto}", textoaprovado).Replace("{tabela}", textotabela).Replace("{mensagemaprovado}", textoresultadoap))
    '            '    gravaremail.Close()

    '            '    ' define os destinatarios

    '            '    Session("emailpara") = ""
    '            '    Session("anexos") = ""
    '            '    Session("assunto") = IIf(Session("acao") = "Reprovado", "REPROVADO - ", "APROVADO - ") & encontrachr(row.Cells(5).Text, " ", 1) & " - " & lblloteempresa.Text & " - " & encontrachr(row.Cells(4).Text, " ", 1)
    '            '    Dim tbsol As New Data.DataTable
    '            '    Dim tabsol As New clsBanco
    '            '    Dim tbarqs As New Data.DataTable
    '            '    Dim tabarqs As New clsBanco



    '            '    tbsol = tabsol.conectar("Select tbsolicitantes.*, tbsolicitantes_emails.email from tbsolicitantes inner join tbsolicitantes_emails on  tbsolicitantes.nrseq = tbsolicitantes_emails.nrseqsolicitante where tbsolicitantes.nrseq = " & xsolicitante & " and tbsolicitantes.ativo = true and tbsolicitantes_emails.ativo = true and " & IIf(xresultado = "Aprovado", "(operacao = 'Aprovação' or operacao = 'Ambas')", "(operacao = 'Reprovação' or operacao = 'Ambas')"))


    '            '    For x As Integer = 0 To tbsol.Rows.Count - 1
    '            '        Session("emailpara") &= (tbsol.Rows(x)("email").ToString & ";")
    '            '    Next

    '            '    tbsol = tabsol.conectar("select * from tblotes_arquivos where nrseqlote = " & Session("consultalote") & " and ativo = true")
    '            '    For x As Integer = 0 To tbsol.Rows.Count - 1
    '            '        If tbsol.Rows(x)("tparquivo").ToString.ToLower = "boleto" Then
    '            '            Session("anexos") &= (Server.MapPath("~\arquivoslotes\boletos\") & tbsol.Rows(x)("arquivo").ToString & ";")
    '            '            'Else
    '            '            '    novoemail.AdicionaAnexos = Server.MapPath("~\arquivoslotes\xml\") & tbsol.Rows(x)("arquivo").ToString
    '            '        End If

    '            '    Next



    '            '   Response.Redirect("envioporemail.aspx")








    '            'Else

    '            '    lblerroconta1.Text = "Você não tem permissão para abrir o processo !"
    '            '    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibiralerta1()", "exibiralerta1()", True)
    '            '    Exit Sub
    '            'End If
    '        Case Is = "abrir"
    '            If row.Cells(6).Text = "LOTE INVÁLIDO" Then
    '                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "irpaineldados()", "irpaineldados()", True)
    '                lblerroconta1.Text = "Atenção: Você deve reprocessar os arquivos !"
    '                alerta1.Style.Remove("display")
    '                Exit Sub
    '            End If
    '            If Session("usertransp") = "" OrElse Session("usertransp") = "0" Then
    '                Session("abriu") = True

    '                Session("novolote") = "Não"
    '                Session("consultalote") = lblnrseqgrade.Text
    '                ' Session("nrfatura") = row.Cells(2).Text
    '                Session("transportadora") = row.Cells(3).Text
    '                Response.Redirect("painelcte.aspx")
    '            Else

    '                lblerroconta1.Text = "Você não tem permissão para abrir o processo !"
    '                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibiralerta1()", "exibiralerta1()", True)
    '                Exit Sub
    '            End If
    '        Case Is = "baixar"
    '            divbaixararquivos.Style.Remove("display")
    '            divgrade.Style.Add("display", "none")
    '            Dim tbarq As New Data.DataTable
    '            Dim tabarq As New clsBanco

    '            tbarq = tabarq.conectar("select * from tblotes_arquivos where ativo = true and nrseqlote = " & lblnrseqgrade.Text & " and not tparquivo like '%XML%' order by nrseq desc")

    '            GradeArqs.DataSource = tbarq
    '            GradeArqs.DataBind()
    '        Case Is = "excluir"
    '            Dim tbexcluir As New Data.DataTable
    '            Dim tabexcluir As New clsBanco



    '            If "Aprovado" = row.Cells(5).Text Then
    '                lblerroconta1.Text = "Você não tem permissão para excluir um processo já aprovado !"
    '                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "exibiralerta1()", "exibiralerta1()", True)
    '                Exit Sub
    '            End If
    '            tbexcluir = tabexcluir.IncluirAlterarDados("update tblotes set ativo = false, dtexclui = '" & formatadatamysql(data) & "', userexclui = '" & Session("usuario") & "' where nrseq = " & row.Cells(1).Text)

    '            tbexcluir = tabexcluir.IncluirAlterarDados("update tblotes_arquivos set ativo = false, dtexclui = '" & formatadatamysql(data) & "', userexclui = '" & Session("usuario") & "' where nrseqlote = " & row.Cells(1).Text)
    '            carregarlotes()
    '        Case Is = "reprocessar"
    '            If row.Cells(6).Text <> "LOTE INVÁLIDO" Then
    '                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "irpaineldados()", "irpaineldados()", True)
    '                lblerroconta1.Text = "Atenção: Você pode processar apenas lotes inválidos !"
    '                alerta1.Style.Remove("display")
    '                Exit Sub
    '            End If

    '            If processarlotegravarcte(lblnrseqgrade.Text) Then
    '                If Session("usertransp") = "" OrElse Session("usertransp") = "0" Then
    '                    Session("abriu") = True

    '                    Session("novolote") = "Não"
    '                    Session("consultalote") = lblnrseqgrade.Text
    '                    ' Session("nrfatura") = row.Cells(2).Text
    '                    Response.Redirect("painelcte.aspx")

    '                End If
    '            End If

    '    End Select



    'End Sub


    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub

    Private Sub grade_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grade.RowDataBound
        If e.Row.RowIndex < 0 Then Exit Sub

        Dim lblitem As Label = e.Row.FindControl("lblitem")
        Dim imgfitagrade As Image = e.Row.FindControl("imgfitagrade")
        Dim lblresultadograde As Label = e.Row.FindControl("lblresultadograde")
        Dim divpontosgrade As Object = e.Row.FindControl("divpontosgrade")
        lblitem.Text = e.Row.RowIndex + 1

        If lblresultadograde.Text <> "" Then
            divpontosgrade.style.remove("display")
            If logico(e.Row.DataItem("maior").ToString) = "True" Then
                imgfitagrade.Visible = True
            Else
                imgfitagrade.Visible = False
            End If
        Else
            divpontosgrade.style.add("display", "none")
            imgfitagrade.Visible = False
        End If

        e.Row.Cells(5).Text = IIf(logico(e.Row.Cells(5).Text) = "True", "Sim", "Não")
    End Sub

    Private Sub grade_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grade.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = grade.Rows(index)
        Dim lblnrseqgrade As Label = row.FindControl("lblnrseqgrade")
        If e.CommandName.ToLower = "abrir" Then
            Response.Redirect("pad.aspx?pad=" & lblnrseqgrade.Text)
        End If
        If e.CommandName.ToLower = "imprimir" Then
            Dim xpad As New clspad

            xpad.ProcurarPADpor = "nrseq"
            xpad.Nrseq = lblnrseqgrade.Text

            If Not xpad.imprimirpad() Then
                sm("swal.fire({title: 'Ops!',text: '" & xpad.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            'Response.ContentType = "application/octet-stream"

            'Response.Clear()
            'Response.AddHeader("content-disposition", "attachment; filename=\arquivolotes\" & mARQUIVO(xpad.Arquivopdf))
            'Response.TransmitFile(xpad.Arquivopdf)
            'Response.End()

            Dim linkproposta As String = Request.Url.OriginalString.Replace(Request.Url.PathAndQuery, "/" & "arquivoslotes/" & xpad.Arquivopdf)

            sm("irlink('" & linkproposta & "')")

        End If

        If e.CommandName.ToLower = "email" Then
            Dim xpad As New clspad

            xpad.Nrseq = lblnrseqgrade.Text

            If Not xpad.imprimirpad() Then
                sm("swal.fire({title: 'Ops!',text: '" & xpad.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")

                Exit Sub
            End If

            Session("tabela") = ""
            Session("assunto") = "Formulário PAD - " & ""
            Session("texto") = ""
            Session("emails") = ""
            Session("anexos") = xpad.Arquivopdf

            Response.Redirect("mail.aspx?opera=nova")

        End If
    End Sub

    Private Sub chkseltodos_CheckedChanged(sender As Object, e As EventArgs) Handles chkseltodos.CheckedChanged
        For Each row As GridViewRow In grade.Rows
            Dim sel1 As CheckBox = row.FindControl("sel1")
            sel1.Checked = chkseltodos.Checked

        Next
    End Sub

    Private Sub btnexportartodos_Click(sender As Object, e As EventArgs) Handles btnexportartodos.Click
        Dim xpad As New clspad
        xpad.ListaBaixarPADs.Clear()
        For Each row As GridViewRow In grade.Rows
            Dim sel1 As CheckBox = row.FindControl("sel1")
            Dim lblnrseqgrade As Label = row.FindControl("lblnrseqgrade")
            If sel1.Checked = True Then
                xpad.ListaBaixarPADs.Add(New clspad With {.Nrseq = lblnrseqgrade.Text, .ProcurarPADpor = "nrseq"})
            End If

        Next

        If Not xpad.baixartodos() Then
            sm("swal({title: 'Atenção!',text: '" & xpad.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If
        hdnomearquivo.Value = xpad.Arquivopdf
        divbaixar.Style.Remove("display")
        divexportar.Style.Add("display", "none")
    End Sub

    Private Sub btnbaixararqs_Click(sender As Object, e As EventArgs) Handles btnbaixararqs.Click


        divexportar.Style.Remove("display")
        divbaixar.Style.Add("display", "none")

        Response.ContentType = "application/octet-stream"

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment; filename=\arquivolotes\diversos\" & hdnomearquivo.Value)
        Response.TransmitFile(Server.MapPath("~\arquivoslotes\diversos\") & hdnomearquivo.Value)
        'Response.TransmitFile(xpad.Arquivopdf)
        Response.End()
    End Sub

    Private Sub btnbaixararqs_Command(sender As Object, e As CommandEventArgs) Handles btnbaixararqs.Command

    End Sub

    Private Sub cbogestor_CallingDataMethods(sender As Object, e As CallingDataMethodsEventArgs) Handles cbogestor.CallingDataMethods

    End Sub
End Class
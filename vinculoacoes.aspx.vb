﻿Imports clsSmart


Partial Class vinculoacoes
    Inherits System.Web.UI.Page

    Private Sub vinculoacoes_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
        End If



        habilitar(False)
        habilitartela2(False)

        carregagradeacoes()
    End Sub

    Private Sub limpar()
        txtnrseq.Text = ""
        txtnome.Text = ""
        txtnumero.Text = ""
        'cbojustica.SelectedValue = ""
        ' txtconteudo.Text = ""
        txtdata.Text = ""
        ' txtusercad.Text = ""
        ' txtativo_old.text = ""
        chkativo.Text = ""
        ' txtnrseqctrl.text = ""
        txtlink.Text = ""
    End Sub
    Private Sub sm(funcao As String, Optional nome As String = "")
        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)
    End Sub
    Private Sub habilitar(valor As Boolean)
        txtnrseq.Enabled = valor
        txtnome.Enabled = valor
        txtnumero.Enabled = valor
        cbojustica.Enabled = valor
        'txtconteudo.Enabled = valor
        txtdata.Enabled = valor
        ' txtusercad.Enabled = valor
        '    txtativo_old.enabled = valor
        chkativo.Enabled = valor
        '     txtnrseqctrl.enabled = valor
        txtlink.Enabled = valor
    End Sub

    Private Sub habilitartela2(valor As Boolean)

        If valor = True Then
            divparticipantes.Visible = True
            divpadrão.Visible = False
        Else
            divparticipantes.Visible = False
            divpadrão.Visible = True
        End If

    End Sub

    Public Sub carregaassociados(Optional exibirinativos As Boolean = False)
        Dim xAssociados As New clsAssociado
        Dim consulta As String

        consulta = ""

        If txtbuscaassociado.Text = "" Then
            consulta = ""
            sm("swal({title: 'Error!',text: 'Essa Busca pode demorar',type: 'error',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        Else
            consulta = "where " & cbobuscaassociado.SelectedValue & " like '%" & txtbuscaassociado.Text & "%'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbAssociados " & consulta & "")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        gradeassociados.DataSource = tb1
        gradeassociados.DataBind()

    End Sub


    Private Sub btnsalvar_Click(sender As Object, e As EventArgs) Handles btnsalvar.Click
        Dim informar As String
        informar = ""
        Dim xAcoes As New clsacoes

        If txtnome.Text = "" Then
            informar = informar + " [Nome] "
        End If

        If txtnumero.Text = "" Then
            informar = informar + " [Numero] "
        End If

        If txtlink.Text = "" Then
            informar = informar + " [link] "
        End If
        If txtjustica.Text = "" Then
            informar = informar + " [Justiça] "
        End If
        If txtdata.Text = "" Then
            informar = informar + " [Data] "
        End If

        If informar <> "" Then
            sm("Swal.fire({   type: 'warning',  title: 'Preencha os campos " & informar & "',  confirmButtonText: 'OK'})")
            Exit Sub
        End If

        xAcoes.Nrseq = txtnrseq.Text
        xAcoes.Nome = txtnome.Text
        xAcoes.Numero = txtnumero.Text
        xAcoes.Justica = txtjustica.Text
        xAcoes.Data = txtdata.Text
        xAcoes.Ativo = chkativo.Checked
        xAcoes.Link = txtlink.Text

        If Not xAcoes.salvar Then
            sm("swal({title: 'Atenção!',text: 'Não foi possivel salvar esta ação preencha os campos assinalados',type: '',confirmButtonText: 'OK'})", "swal")
            Exit Sub
        End If

        sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Ação salva com sucesso',  showConfirmButton: false,  timer: 2000})")


        limpar()
        habilitar(False)
        carregagradeacoes()
    End Sub

    Public Sub carregagradeacoes(Optional exibirinativos As Boolean = False)
        Dim consulta As String

        consulta = ""

        If txtbusca.Text = "" Then
            consulta = " where ativo = 1"
        Else
            consulta = " where " & cbobusca.SelectedValue & " like '%" & txtbusca.Text & "%'"
        End If

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbacoes " & consulta & "")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        gradeacoes.DataSource = tb1
        gradeacoes.DataBind()

    End Sub

    Public Sub carregagradeacoesdetalhes(Optional exibirinativos As Boolean = False)

        Dim tb1 As New Data.DataTable
        Dim tab1 As New clsBanco
        'If Not xempresas.carregarempresa() Then
        '    sm("swal({title: 'Error!',text: '" & xempresas.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If

        tb1 = tab1.conectar("select * from tbacoes_detalhes as ad inner join tbAssociados as a on ad.matricula = a.matricula  where ad.nrseqacoes = " & hdnrseqacoesvinculos.Value & " and ad.ativo = true")

        'If Not xempresas.Contador > 0 Then
        '    Exit Sub
        'End If

        'xplanos.Cliente = xempresas.Nome

        'If Not xAssociados.carregatodos() Then
        '    sm("swal({title 'Error!',text: '" & xAssociados.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
        '    Exit Sub
        'End If


        gradeacoesvinculos.DataSource = tb1
        gradeacoesvinculos.DataBind()

    End Sub

    Private Sub gradeacoes_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeacoes.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeacoes.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        If e.CommandName = "editar" Then
            '    carregarNoFormulario(nrseq.Value)
            habilitar(True)
            '  txtidentificador.Enabled = False

            Dim xAcoes As New clsacoes

            xAcoes.Nrseq = nrseq.Value

            If Not xAcoes.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAcoes.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            txtnrseq.Text = xAcoes.Nrseq
            txtnome.Text = xAcoes.Nome
            txtnumero.Text = xAcoes.Numero
            chkativo.Checked = xAcoes.Ativo
            txtlink.Text = xAcoes.Link
            txtjustica.Text = xAcoes.Justica
            ' cbojustica.Text = xAcoes.Justica
            txtdata.Text = formatadatamysql(xAcoes.Data, True, True)

            'dados historico

            'txtdtcad.Text = formatadatamysql(xAcoes.Dtcad, True, True)
            'txtusercad.Text = xAcoes.Usercad

            carregagradeacoes()


        End If

        If e.CommandName = "escolha" Then

            Dim xAcoes As New clsacoes
            Dim xAcoesDetalhes As New clsacoesdth

            xAcoesDetalhes.Nrseqacoes = nrseq.Value
            xAcoes.Nrseq = nrseq.Value

            If Not xAcoes.procurar() Then
                sm("swal({title: 'Error!',text: '" & xAcoes.Mensagemerro & "',type: 'error',confirmButtonText: 'OK'})", "swal")
                Exit Sub
            End If

            If Not xAcoesDetalhes.canceladoscount() Then

            End If

            If Not xAcoesDetalhes.participantescount() Then

            End If
            If xAcoes.Ativo = True Then
                txtstatus.Text = "ATIVO"
            Else
                txtstatus.Text = "DESATIVADO"
            End If

            hdnrseqacoesvinculos.Value = xAcoes.Nrseq
            txtnomeacao2.Text = xAcoes.Nome
            txtnumeracao2.Text = xAcoes.Numero
            txtcancelados.Text = xAcoesDetalhes.Cancelados
            txtparticipantes.Text = xAcoesDetalhes.Participantes




            habilitartela2(True)
            carregagradeacoesdetalhes()

        End If
    End Sub



    Private Sub gradeacoesvinculos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gradeacoesvinculos.RowDataBound

        If e.Row.RowIndex < 0 Then
            Exit Sub
        End If
        Dim ativar As HtmlGenericControl = e.Row.FindControl("divativar")
        Dim retirar As HtmlGenericControl = e.Row.FindControl("divretirar")


        If e.Row.Cells(3).Text = "1" Then
            e.Row.Cells(3).Text = "ATIVO"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightBlue

        ElseIf e.Row.Cells(3).Text = "0" Then
            e.Row.Cells(3).Text = "CANCELADO"
            e.Row.ForeColor = System.Drawing.Color.Black
            e.Row.BackColor = System.Drawing.Color.LightGray

        End If


        If e.Row.Cells(3).Text = "ATIVO" Then
            ativar.Visible = False
            retirar.Visible = True
        Else
            ativar.Visible = True
            retirar.Visible = False
        End If



    End Sub

    Private Sub gradeacoesvinculos_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeacoesvinculos.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeacoesvinculos.Rows(index)
        Dim nrseq As HiddenField = row.FindControl("hdnrseqgrade")
        Dim matricula As HiddenField = row.FindControl("hdmatricula")

        If e.CommandName = "ativar" Then


            Dim xAcoes As New clsacoes
            Dim xAcoesDetalhes As New clsacoesdth

            xAcoesDetalhes.Nrseqacoes = hdnrseqacoesvinculos.Value
            xAcoesDetalhes.Matricula = matricula.Value
            xAcoesDetalhes.Nrseq = nrseq.Value


            If Not xAcoesDetalhes.ativar() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: '" & xAcoesDetalhes.Mensagemerro & "',  showConfirmButton: false,  timer: 3000})")
                carregagradeacoesdetalhes()
                habilitartela2(True)
                Exit Sub
            End If


            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: ' Matricula " & matricula.Value & " Recupedara com sucesso',  showConfirmButton: false,  timer: 3000})")

            If Not xAcoesDetalhes.canceladoscount() Then
            End If

            If Not xAcoesDetalhes.participantescount() Then
            End If


            txtcancelados.Text = xAcoesDetalhes.Cancelados
            txtparticipantes.Text = xAcoesDetalhes.Participantes




            carregagradeacoesdetalhes()
            habilitartela2(True)
        End If

        If e.CommandName = "retirar" Then

                Dim xAcoes As New clsacoes
                Dim xAcoesDetalhes As New clsacoesdth

                xAcoesDetalhes.Nrseqacoes = hdnrseqacoesvinculos.Value
                xAcoesDetalhes.Matricula = matricula.Value
                xAcoesDetalhes.Nrseq = nrseq.Value


                If Not xAcoesDetalhes.remover() Then
                    sm("Swal.fire({  position: 'top-end',  type: 'error',  title: '" & xAcoesDetalhes.Mensagemerro & "',  showConfirmButton: false,  timer: 2000})")
                    carregagradeacoesdetalhes()
                    habilitartela2(True)
                    Exit Sub
                End If


                sm("Swal.fire({  position: 'top-end',  type: 'success',  title: ' Matricula " & matricula.Value & " retirada com sucesso',  showConfirmButton: false,  timer: 2000})")





                If Not xAcoesDetalhes.canceladoscount() Then
                End If

            If Not xAcoesDetalhes.participantescount() Then
            End If

            txtcancelados.Text = xAcoesDetalhes.Cancelados
            txtparticipantes.Text = xAcoesDetalhes.Participantes

            carregagradeacoesdetalhes()
                habilitartela2(True)

            End If
    End Sub


    Private Sub btnbuscando_Click(sender As Object, e As EventArgs) Handles btnbuscando.Click
        carregagradeacoes()
    End Sub

    Private Sub btnbuscacliente_Click(sender As Object, e As EventArgs) Handles btnbuscacliente.Click
        carregaassociados()
        habilitartela2(True)
    End Sub

    Private Sub gradeassociados_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gradeassociados.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = gradeassociados.Rows(index)
        Dim matricula As HiddenField = row.FindControl("hdmatri")

        If e.CommandName = "selecionar" Then
            Dim xAcoesDetalhes As New clsacoesdth

            xAcoesDetalhes.Matricula = matricula.Value
            xAcoesDetalhes.Nrseqacoes = hdnrseqacoesvinculos.Value

            If Not xAcoesDetalhes.adicionar() Then
                sm("Swal.fire({  position: 'top-end',  type: 'error',  title: '" & xAcoesDetalhes.Mensagemerro & "',  showConfirmButton: false,  timer: 2000})")

                carregagradeacoesdetalhes()
                habilitartela2(True)
                Exit Sub
            End If

            sm("Swal.fire({  position: 'top-end',  type: 'success',  title: 'Adicionado Matricula " & matricula.Value & " com sucesso',  showConfirmButton: false,  timer: 2000})")


            carregagradeacoesdetalhes()

            If Not xAcoesDetalhes.canceladoscount() Then
            End If

            If Not xAcoesDetalhes.participantescount() Then
            End If


            txtcancelados.Text = xAcoesDetalhes.Cancelados
            txtparticipantes.Text = xAcoesDetalhes.Participantes

            habilitartela2(True)

        End If
    End Sub


End Class

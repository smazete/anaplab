﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="votos_eleicoes.aspx.vb" Inherits="votos_eleicoes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
    <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="/dist/js/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>

    <script src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" />

    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/JScriptmascara.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="box box-primary" style="padding: 1rem; -webkit-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); -moz-box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77); box-shadow: 9px 7px 5px rgba(50, 50, 50, 0.77);">
        <div class="box box-header">
            <b>Votos Eleições 2020</b>
        </div>
        <div class="box-body">
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total de filiados</span>
                                <span class="info-box-number">
                                    <asp:Label ID="lblassocativos" Text="0000" runat="server" /></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-check-circle"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total de votos</span>
                                <span class="info-box-number">
                                    <asp:Label ID="lblttvotos" Text="0000" runat="server" /></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-grey"><i class="fa fa-check-circle"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">votos brancos e nulos</span>
                                <span class="info-box-number">
                                    <asp:Label ID="lblvotosbrancos" Text="0000" runat="server" /></span>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div class="box-body">
                            <asp:Repeater ID="rptchapas" runat="server">
                                <ItemTemplate>
                                    <div class="col-md-12">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">Chapa número <%# DataBinder.Eval(Container.DataItem, "numero") %></div>
                                            <div class="info-box bg-green">
                                                <span class="info-box-icon"><i class="fa fa-users" aria-hidden="true"></i></span>
                                                <div class="info-box-content">
                                                    <div class="col-md-4">
                                                        <span class="info-box-text">Total de votos recebidos</span>
                                                        <span class="info-box-number" ><asp:Label runat="server" id="lblvotos" Text=<%# DataBinder.Eval(Container.DataItem, "votos") %>/></span>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="progress">
                                                            <div class="progress-bar" runat="server" id="divprogress"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <span class="progress-description"><%# DataBinder.Eval(Container.DataItem, "descricao") %></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>


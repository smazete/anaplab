﻿Imports System.IO
Imports clsSmart

Partial Class relatorioretornoporconv
    Inherits System.Web.UI.Page

    Private Sub relatorioretornoporconv_Load(sender As Object, e As EventArgs) Handles Me.Load
        carregaplano()
        carregaconv()
    End Sub

    Private Sub carregaplano()

        Dim tbplano As New Data.DataTable
        Dim tabplano As New clsBanco

        Dim cont As Integer

        tbplano = tabplano.conectar("select nrseq, descricao from tbplanos where ativo = true")
        ddlPlano.Items.Clear()
        ddlPlano.Items.Add("")

        cont = ddlPlano.Items.Count
        For x As Integer = 0 To tbplano.Rows.Count - 1
            ddlPlano.Items.Add(New ListItem(tbplano.Rows(x)("descricao").ToString, tbplano.Rows(x)("nrseq").ToString))
        Next

    End Sub
    Private Sub carregaconv()

        Dim tbconv As New Data.DataTable
        Dim tabconv As New clsBanco

        Dim cont As Integer

        tbconv = tabconv.conectar("select nrseq, nome from tbconvenios where ativo = true")
        ddlConvenio.Items.Clear()
        ddlConvenio.Items.Add("")

        cont = ddlConvenio.Items.Count
        For x As Integer = 0 To tbconv.Rows.Count - 1
            ddlConvenio.Items.Add(tbconv.Rows(x)("nome").ToString)
            ddlConvenio.Items(ddlConvenio.Items.Count - 1).Value = tbconv.Rows(x)("nrseq").ToString
        Next

    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        Dim informar As String
        informar = ""

        If txtdtinicial.Text = "" Then
            informar = informar + " [Data Inico] "
        End If
        If txtdtfinal.Text = "" Then
            informar = informar + " [Data Final] "
        End If

        If informar <> "" Then
            sm("Swal.fire({icon: 'error',title: 'O Campo " & informar & "',text: 'Preencha os campos que estão faltando ',})")
            Exit Sub
        End If

        Response.Redirect("/relatorios/reletorioretorno.aspx?plan=" & ddlPlano.SelectedValue & "&conven=" & ddlConvenio.SelectedValue & "&dtin=" & valordatamysql(txtdtinicial.Text) & "&dtfim=" & valordatamysql(txtdtfinal.Text))

    End Sub

    Private Sub sm(funcao As String, Optional nome As String = "")

        If nome = "" Then
            nome = funcao
        End If
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), nome, funcao, True)

    End Sub
End Class
